# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from weaver.function import ShellFunction
from jflow.component import Component
from jflow import utils as utils

class Breakdancer (Component):
    """
    :note negative distance not considered
    """    
    def define_parameters(self, input_bam, reference_genome, min_sup, min_mapq, max_len):
        self.add_input_file_list("input_bam", "One bam file by individual/condition", default=input_bam, file_format="bam", required=True)
        self.add_output_file("output_file", "Breakdancer output file", filename="breakdancer.ctx")
        self.add_parameter("min_sup", "Minimum number of read supporting a SV", type=int, default=min_sup)
        self.add_parameter("max_len", "Maximum length of a variant (8 500 000)", type=int, default=max_len)
        self.add_parameter("min_mapq", "min. paired-end mapping quality", type=int, default=min_mapq)

    def process(self):
        # create configuration file
        cfg_file_path = self.get_temporary_file(".bd.cfg")
        tmp_normal = self.get_temporary_file(".bd")
        tmp_translocation = self.get_temporary_file(".bd")
        bam2cfg = ShellFunction(self.get_exec_path("bam2cfg.pl") + " -q $2 " + utils.get_argument_pattern(self.input_bam, 3)[0] + " > $1 ", cmd_format='{EXE} {OUT} {ARG} {IN}')
        bam2cfg(outputs=cfg_file_path, arguments=1, inputs=self.input_bam) #TODO change 1
        # same as breakdancer but for translocation
        breakdancer = ShellFunction(self.get_exec_path("breakdancer-max") \
                                    + " -r {0} -m {1} -q {2} ".format(self.min_sup, self.max_len, self.min_mapq) \
                                    + " $1 > $2;"\
                                    + " if [ ! -e $2 ]; then echo #no_read_groups_found > $2; fi",
                                    cmd_format='{EXE} {IN} {OUT}')
        # launch
        breakdancer(cfg_file_path, tmp_normal)
        breakdancer(" -t "+cfg_file_path, tmp_translocation) # interchrom translo°
        concatenate_files = ShellFunction('cat $2 $3 > $1', cmd_format='{EXE} {OUT} {IN}')
        concatenate_files(outputs = self.output_file, inputs=[tmp_normal, tmp_translocation])