# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from weaver.function import PythonFunction
from weaver.abstraction import Map
from jflow.component import Component
from jflow.abstraction import MultiMap
from fileinput import filename

def cnvnator_func(output, ref, bin_size, bam): # Wrapper which handle complex in-out of cnvnator
    import shlex, subprocess
    with open(output,'w') as fOut:
        tmp = output+"_tmp.root"
        #Do the 4 steps needed for cnvnator, 1 by 1
        x = ["-tree {0}".format(bam), "-his {0}".format(bin_size), "-stat {0}".format(bin_size), "-partition {0}".format(bin_size)]
        for i in x:
            command = "cnvnator -root {0} -genome {1} {2} ".format(tmp, ref, i)
            subprocess.Popen(shlex.split(command)).wait()
        #Last step need to gather stdOut
        command = "cnvnator -root {0} -genome {1} -call {2} ".format(tmp, ref, bin_size)
        subprocess.Popen(shlex.split(command), stdout=fOut).wait()


class CnvNator (Component):
    
    def define_parameters(self, input_bam, reference_genome, bin_size):
        self.add_input_file_list("input_bam", "One bam file by individual/condition", default=input_bam, file_format="bam", required=True)
        self.add_input_file("reference_genome", "On which genome should the reads being aligned on", default=reference_genome, file_format="fasta", required=True)
        self.add_output_file_list("output_file", "CNV output file", pattern='{basename}_call', items=input_bam)
        self.add_parameter("bin_size", "Bin size for cnvnator: Depends on the deep of coverage.  X5:500, x10:250, x20-30:100, x100:30", default=bin_size, type=int)

    def process(self):
        #Real cnvnator run starts
        cnvnator = PythonFunction(cnvnator_func, cmd_format="{EXE} {OUT} {ARG} {IN}")
        for i,o in zip(self.input_bam, self.output_file):
            cnvnator(outputs=o, arguments=[self.reference_genome, self.bin_size], inputs=i)