# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from weaver.function import ShellFunction
from jflow.component import Component
from jflow import utils as utils
import os.path

class Delly (Component):
    
    def define_parameters(self, input_bam, reference_genome, min_mapq=30):
        self.add_input_file_list("input_bam", "One bam file by individual/condition", default=input_bam, file_format="bam", required=True)
        self.add_input_file("reference_genome", "On which genome should the reads being aligned on", default=reference_genome, required=True)
        self.add_output_file("output_file", "Delly output file", filename="delly.vcf")
        self.add_parameter("min_mapq", "min. paired-end mapping quality", type=int, default=min_mapq)

    def process(self):
        delly = ShellFunction(self.get_exec_path("delly") \
                              + " -g {0} -q {1}".format(self.reference_genome, self.min_mapq) \
                              + " -o $1 "\
                              + utils.get_argument_pattern(self.input_bam, 2)[0]\
                              + " > /dev/null;" \
                              + " if [ ! -e $1 ]; then touch $1; fi", 
                              cmd_format='{EXE} {OUT} {IN}')
        delly(inputs=self.input_bam, outputs=self.output_file)
            