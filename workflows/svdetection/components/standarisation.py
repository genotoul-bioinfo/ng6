# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from weaver.function import ShellFunction
from weaver.abstraction import Map
from jflow.component import Component
from jflow import utils as utils
from fileinput import filename

class Standardisation (Component):
    
    def define_parameters(self, input_file, mode, output_file, min_len, max_len, indiv_list, input_bam=[], p=0.7, li=1000):
        self.add_input_file_list("input_file", "An output file from a SV detection software", default=input_file, required=True) #List only for cnvnator
        self.add_input_file_list("input_bam", "One bam file by individual/condition", default=input_bam, file_format="bam", required=True) #Only useful for breakdancer
        self.add_parameter("mode", "Mode: name of the software which the file originates from", type=str,
                            choices=["breakdancer", "cnvnator", "delly", "pindel"], default=mode, required=True)
        self.add_output_file("output_file", "Input file after being filtered and translated in an uniform bed-like format",
                              filename="{0}.bed".format(self.mode))
        self.add_output_file("translocation_file", "translocations after being translated in an uniform bed-like format",
                              filename="trans_{0}.bed".format(self.mode))
        self.add_parameter("min_len", "Minimum length of a variant (50bp)", type=int, default=min_len)
        self.add_parameter("max_len", "Maximum length of a variant (8 500 000bp)", type=int, default=max_len) #TODO chr %
        self.add_parameter("overlap_percent", "Minimum threshold of overlap percent between 2 SV", type=float, default=p)
        self.add_parameter("li_range", "Range of localisation interval around breakpoints", type=int, default=li)
        self.add_parameter_list("indiv_list", "String list of studied individuals name", default=indiv_list, type=str, required=True)

    def process(self):
        sort = ShellFunction(self.get_exec_path("sort") + " -k1,1n -k2,2n $1 > $2", cmd_format='{EXE} {IN} {OUT}')
        unsorted_out = self.get_temporary_file(".bed")
        if self.mode != "cnvnator":
            #FIX
            breakdancer_fix = " ".join(self.input_bam)
            if breakdancer_fix:
                breakdancer_fix = " --alt_names " + breakdancer_fix
            else:
                pass
            #BEDIFY
            std = ShellFunction(self.get_exec_path("bedify.py") + "\
                                --input_file $1 \
                                --output_file $2 \
                                --translocation_file $3 \
                                --names " + utils.get_argument_pattern(self.indiv_list, 4)[0] + "\
                                --tag " + self.mode + "\
                                --software " + self.mode + "\
                                --min_length {0} --max_length {1} ".format(self.min_len, self.max_len) + \
                                breakdancer_fix,
                                cmd_format='{EXE} {IN} {OUT} {ARG}')
            std(inputs=self.input_file, outputs=[unsorted_out, self.translocation_file], arguments=self.indiv_list) # Here, the mode is also used as a tag for the SVs (ID prefix)
            sort(unsorted_out, self.output_file)
        else :
            tmp_out = [file_path + ".bed" for file_path in self.input_file]
            #CNV2BED
            std = ShellFunction(self.get_exec_path("cnv2bed.py") + "\
                                --input_file $1 \
                                --output_file $2 \
                                --tag $3 \
                                --min_length {0} --max_length {1}".format(self.min_len, self.max_len),
                                cmd_format='{EXE} {IN} {OUT} {ARG}')
            if len(self.input_file) > 1:
                for i in range(len(self.input_file)):
                    std(inputs=self.input_file[i], outputs=tmp_out[i], arguments=self.indiv_list[i])
                    #MERGE_SV
                    mrg = ShellFunction(self.get_exec_path("merge_sv.py") + \
                                        " --output $1 \
                                          --tag $2\
                                          --p $3\
                                          --li $4" \
                                        + " --names" + utils.get_argument_pattern(self.indiv_list, 5)[0] \
                                        + " --files" + utils.get_argument_pattern(tmp_out, len(tmp_out)+5)[0],
                                        cmd_format='{EXE} {OUT} {ARG} {IN}')
                mrg(outputs=unsorted_out, inputs=tmp_out, arguments=[self.mode, self.overlap_percent, self.li_range, self.indiv_list])
                sort(unsorted_out, self.output_file)
            else:
                std(inputs=self.input_file, outputs=tmp_out, arguments=self.indiv_list)
                sort(tmp_out, self.output_file)
