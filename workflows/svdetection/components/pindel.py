# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import os.path
from weaver.function import Pipeline
from weaver.function import ShellFunction
from jflow import utils as utils
from jflow.component import Component
# from fileinput import filename

class Pindel (Component):
    
    def define_parameters(self, input_bam, reference_genome, output_file, mean_insert, min_sup, indiv_list):
        self.add_input_file_list("input_bam", "One bam file by individual/condition", default=input_bam, file_format="bam", required=True)
        self.add_input_file("reference_genome", "Animal ref file", default=reference_genome, file_format="fasta", required=True)
        self.add_output_file("output_file", "Pindel concatenated output files [D/SI/TD/INV]", filename="pindel.concat.out")
        self.add_output_file("tra_file", "Pindel translocations", filename="pindel.tra.out")
        self.add_parameter("mean_insert", "mean insert size", type=int, default=mean_insert, required=True)
        self.add_parameter("min_sup", "Minimum number of read supporting a SV (3) [Not available for cnvnator]", type=int, default=min_sup)
        self.add_parameter_list("indiv_list", "String list of studied individuals name", default=indiv_list, type=str, required=True)
        #TODO minmapq

    def process(self):
        # create configuration file
        cfg_file_path = self.get_temporary_file(".pdl.cfg")
        with open(cfg_file_path, 'w') as config_file:
            n = len(self.input_bam)
            for n in range(n):
                line = [self.input_bam[n], str(self.mean_insert), self.indiv_list[n]]
                true_line = "\t".join(line) + "\n"
                config_file.write(true_line)
        output_prefix = os.path.splitext(os.path.basename(self.output_file))[0]
        output_list = [] #raw pindel output files list
        for type in ["D", "SI", "TD", "INV"]:
            output_pathway = output_prefix + "_" + type
            output_list.append(output_pathway)
        pindel = ShellFunction(self.get_exec_path("pindel") + \
                                " -f " + self.reference_genome + \
                                " -i $1 " + \
                                " -o "+ output_prefix + \
                                " -k false" + \
                                " > /dev/null " ,
                                cmd_format='{EXE} {ARG}')
        concatenate = ShellFunction(self.get_exec_path("cat") + \
                                " " + utils.get_argument_pattern(output_list, 2)[0] + \
                                " | grep -P \"^[0-9]\" " + \
                                " | sort -k 8,8 -k10,10n " + \
                                " > $1", cmd_format='{EXE} {OUT} {ARG}')
        chained_function = Pipeline(functions=[pindel,concatenate], separator="&&")
        chained_function(arguments=[cfg_file_path, output_list], outputs=self.output_file)
        link = ShellFunction(self.get_exec_path("ln") + " -s $1 $2", cmd_format='{EXE} {IN} {OUT}')
        link(inputs=output_pathway+"_INT_final", outputs=self.tra_file)
