# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from jflow.workflow import Workflow
from ng6.ng6workflow import NG6Workflow

class SVDetection (NG6Workflow):
    
    def get_description(self):
        return "This pipeline aims to detect structural variations in whole genomes."
    
    def define_parameters(self, function="process"):
        #TODO reference -> force fasta
        self.add_input_file("reference_genome", "On which genome should the reads being aligned on", required=True)
        self.add_parameter("bin_size", "Bin size for cnvnator: Depends on the deep of coverage.  X5:500, x10:250, x20-30:100, x100:30", default=100, type=int)
        self.add_parameter("mean_insert", "mean insert size (PINDEL only)", type=int, required=True) #TODO use sample insert
        self.add_parameter("min_sup", "Minimum number of read supporting a SV (3) [Not available for cnvnator]", type=int, default=3)
        self.add_parameter("min_len", "Minimum length of a variant (50)", type=int, default=50)
        self.add_parameter("max_len", "Maximum length of a variant (8 500 000)", type=int, default=8500000) #TODO: max calculed % chr size
        #TODO in future: minmapq -> just after aligment
        self.add_parameter("min_mapq", "min. paired-end mapping quality", type=int, default=30)
        self.add_parameter("overlap_percent", "Minimum threshold of overlap percent between 2 SV", type=float, default=0.7)
        self.add_parameter("li_range", "Range of localisation interval around breakpoints", type=int, default=1000)

    def process(self):
        
        # parameters
        options = "Filter options:<br/>\
                            - min size {0}<br/>\
                            - max size {1}<br/>\
                            - min supporting read : {2}<br/>\
                            - min mapping quality : {6}<br/>\
                        CNVnator options:<br/>\
                            - bin size {3}<br/>\
                        Merge parameters:<br/>\
                            - overlap percent threshold: {4}<br/>\
                            - localisation interval range: {5}<br/>\
                        The rest is default.".format(self.min_len, self.max_len, self.min_sup, self.bin_size, self.overlap_percent, self.li_range, self.min_mapq)
        
        input_bam = [spl.reads1[0] for spl in self.samples]
        indiv_list = [spl.name for spl in self.samples]
        
        delly = self.add_component("Delly", [input_bam, self.reference_genome])
        breakdancer = self.add_component("Breakdancer", [input_bam, self.reference_genome, self.min_sup, self.min_mapq, self.max_len])
        pindel = self.add_component("Pindel", kwargs={"input_bam":input_bam,"reference_genome": self.reference_genome,
                                                        "output_file":"pindel.concat.out",  "mean_insert":self.mean_insert,
                                                        "min_sup":self.min_sup, "indiv_list":indiv_list})
        cnvnator = self.add_component("CnvNator", [input_bam, self.reference_genome, self.bin_size])
         
        delly = self.add_component("Standardisation", [delly.output_file, "delly", "delly.bed", self.min_len, self.max_len, indiv_list])
        pindel_tra = self.add_component("Standardisation", [pindel.output_file, "pindel_translocation", "pindel.bed", self.min_len, self.max_len, indiv_list])
        pindel = self.add_component("Standardisation", [pindel.output_file, "pindel", "pindel.bed", self.min_len, self.max_len, indiv_list])
        breakdancer = self.add_component("Standardisation", [breakdancer.output_file, "breakdancer", "breakdancer.bed", self.min_len, self.max_len, indiv_list, input_bam])
        cnvnator = self.add_component("Standardisation", [cnvnator.output_file, "cnvnator", "cnvnator.bed", self.min_len, self.max_len, indiv_list])
        
#         #=====TEST PURPOSE===== translocation_file
#         results = ["/home/yguarr/DataTest/delly.bed", "/home/yguarr/DataTest/breakdancer.bed", "/home/yguarr/DataTest/cnvnator.bed", "/home/yguarr/DataTest/pindel.bed"]
#         translocations = ["/home/yguarr/DataTest/tra_breakdancer.bed"]
#         #=====TEST PURPOSE=====
        
        results=[delly.output_file, breakdancer.output_file, cnvnator.output_file, pindel.output_file]
        translocations = [delly.translocation_file, breakdancer.translocation_file, cnvnator.translocation_file, pindel_tra.translocation_file]
        arg_list = [results, self.overlap_percent, self.li_range, ["delly", "breakdancer","cnvnator","pindel"], options, translocations]
        self.add_component(component_name="Analyzebed", addto="project", args=arg_list)

    def post_process(self):
        pass
