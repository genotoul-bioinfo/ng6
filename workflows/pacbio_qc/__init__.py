#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import logging
from ng6.ng6workflow import NG6Workflow
from ng6.utils import Utils

class PacBioQualityCheck (NG6Workflow):
    
    def get_name(self):
        return 'pacbio_qc'
    
    def get_description(self):
        return "PacBio data loading and quality check"
    
    def define_parameters(self, function="process"):
        logging.getLogger("jflow").debug("Begin PacBioQualityCheck.define_parameters! pacbio_qc")
        self.add_parameter("nb_threads", "Number of threads to use for fastqc. Each thread will be allocated 250MB of memory.", default=3)
        self.add_parameter("min_subreads_length", "Subreads shorter than this value (in base pairs) are filtered out and excluded from analysis", default=0, type='int')
        self.add_parameter("polymerase_read_qual", "Polymerase reads with lower quality than this value are filtered out and excluded from analysis", default=0, type='float')
        self.add_parameter("polymerase_read_length", "Polymerase reads shorter than this value (in base pairs) are filtered out and excluded from analysis", default=0, type='int')
        
        self.add_input_file( "barcode_file", "Input barcode file", default=None)
        self.add_parameter("barcode_score", "Min identical base for barcode", default=22, type='int') 
    def process(self):
        logging.getLogger("jflow").debug("Begin PacBioQualityCheck.process! test pacbio_qc")
        sample_names = []
        infiles = []
        for sample in self.samples :
            sample_names.append( sample.name )
            infiles.append(sample.reads1[0])
        add_pacbio_raw_file = self.add_component("AddPacBioRawFiles", [self.runobj, self.get_all_reads()])
        h5tofastq =  self.add_component("H5toFastq", [sample_names, infiles])
        fastqc = self.add_component("FastQC", [h5tofastq.output_fastqs, False, False, "fastqc.tar.gz", self.nb_threads], parent = h5tofastq)
        self.add_component("RS_Subreads", [sample_names, infiles,self.min_subreads_length,self.polymerase_read_qual,self.polymerase_read_length,self.barcode_file,self.barcode_score ])
                    
        
