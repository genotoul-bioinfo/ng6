#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import pickle

from jflow.component import Component

from workflows.pacbio_qc.lib.pacbiolib import h5file

from weaver.function import PythonFunction
    
def add_pacbio_raw_files(run_dump_path, tempdir, stdoutfile):
    import pickle
    import os
    import gzip
    import sys
    
    from workflows.pacbio_qc.lib.pacbiolib import PacbioH5Reader
    from jflow.utils import display_info_message
    with open(stdoutfile, 'w') as fhout :
        # --- add_pacbio_raw_files ---
        my_run = pickle.load(open(run_dump_path, "rb"))
        files_to_save = []
        nb_seq, full_size = 0, 0
        
        for ifile in my_run.raw_files:
            analysisresults_dir = os.path.dirname(ifile)
            celldir = os.path.dirname(analysisresults_dir)
            
            if ifile not in files_to_save :
                # total sequence length
                reader = PacbioH5Reader(ifile)
                h5 = reader.bash5
                for name, description, sequence, qualities in reader :
                    nb_seq += 1
                    full_size += len(sequence)
        
                for partfile in h5.parts :
                    if partfile not in files_to_save :
                        files_to_save.append(partfile.filename)
                
                # it's a bas.h5
                if h5.filename and  h5.filename not in files_to_save:
                    files_to_save.append(h5.filename)
                
                # copy .metadata.xml 
                if reader.metadata : 
                    if reader.metadata not in files_to_save :
                        files_to_save.append(reader.metadata)
                else :
                    display_info_message("Warning : no metadata file found for input file : %s "%ifile)
        
        fhout.write("nb_seq : ")
        fhout.write(str(nb_seq)+"\n")
        fhout.write("full_size : ")
        fhout.write(str(full_size)+"\n")
        fhout.write("Files to save : \n")
        fhout.write("\n".join(files_to_save) )
        my_run.set_nb_sequences(nb_seq)
        my_run.set_full_size(full_size)
        my_run.archive_files(files_to_save, "none")
        my_run.sync()

class AddPacBioRawFiles (Component):
    def define_parameters(self, runobj, input_files):
        self.runobj = runobj
        self.add_input_file_list( "input_files", "File to be saved as raw files", default=input_files, file_format = h5file, required=True)
        self.add_output_file("stdout", "AddPacBioRawFiles stdout", filename="AddPacBioRawFiles.stdout")

    def process(self):
        self.runobj.raw_files = self.input_files
        run_dump_path = self.get_temporary_file(".dump")
        pickle.dump(self.runobj, open(run_dump_path, "wb"))
        addraw = PythonFunction(add_pacbio_raw_files, cmd_format='{EXE} {ARG} {OUT}')
        addraw(outputs=self.stdout, includes=self.input_files, arguments=[run_dump_path, self.config_reader.get_tmp_directory()])
        
        