#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from workflows.pacbio_qc.lib.pacbiolib import h5file

from jflow.abstraction import MultiMap

from ng6.analysis import Analysis

from weaver.function import PythonFunction
    
def h5_to_fastq(input_file, fastqfile):
    import gzip
    
    from workflows.pacbio_qc.lib.pacbiolib import PacbioH5Reader

    # generate fastq.gz file
    f_out = gzip.open(fastqfile, 'wb')
    
    reader = PacbioH5Reader(input_file)
    
    for name, description, sequence, qualities in reader :
        f_out.write(str("@"+name+"\n").encode())
        f_out.write(sequence+"\n".encode())
        f_out.write("+\n".encode())
        f_out.write(qualities+"\n".encode())
    f_out.close()

class H5toFastq (Analysis):
    
    def define_parameters(self, sample_names, input_files):
        self.add_parameter_list("sample_names", "sample names, each sample name must correspond to an input file", default=sample_names, required=True)
        self.add_input_file_list( "input_files", "Input pacbio bas.h5 files", default=input_files, file_format = h5file, required=True)
        self.add_output_file_list( 'output_fastqs', "Output fastq files", pattern="{basename_woext}.fastq.gz", file_format="fastq", items=self.sample_names)

    def process(self):
        convertion = PythonFunction(h5_to_fastq, cmd_format="{EXE} {IN} {OUT}")
        bwa = MultiMap(convertion, inputs=self.input_files, outputs=self.output_fastqs)
        
    def define_analysis(self):
        self.name = "H5toFastq"
        self.description = "Extract subreads of pacbio bas.h5 files to fastq files"
        self.software = "Python"
        self.options = ""
    
    def post_process(self):
        self._save_files(self.output_fastqs)
        
    def get_version(self):
        return "1.0"
        