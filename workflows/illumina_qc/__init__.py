#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import sys
import re
import logging

from ng6.ng6workflow import CasavaNG6Workflow
from ng6.utils import Utils


class IlluminaQualityCheck (CasavaNG6Workflow):
    
    def get_name(self):
        return 'illumina_qc'
    
    def get_description(self):
        return "illumina quality check pipeline"
    
    
    def define_parameters(self, function="process"):
        self.add_input_file("reference_genome", "Which genome should the read being align on")
        self.add_parameter("delete_bam", "The BAM are not stored", type=bool, default = False)
        self.add_parameter("histogram_width", "Explicitly sets the histogram width, overriding automatic truncation of histogram tail", type=int, default = 800, group="INSERTSIZE section")
        self.add_parameter("min_pct", "When generating the histogram, discard any data categories (out of FR, TANDEM, RF) that have"+
                           " fewer than this percentage of overall reads", type=float, default = 0.01, group="INSERTSIZE section")
        self.add_parameter("assignation_databank", "Blast databank to classify a subset of sequences")
        
    
    def process(self):
        logging.getLogger("IlluminaQC").debug("process. nglbi_run_code = "+self.nglbi_run_code)
        fastqilluminafilter, filtered_read1_files, filtered_read2_files, concat_files, concatenatefastq = self.illumina_process()
        
        if self.reference_genome:
            # index the reference genome if not already indexed
            indexed_ref = self.reference_genome
            if not os.path.exists( self.reference_genome + ".bwt" ):
                bwaindex = self.add_component("BWAIndex", [self.reference_genome])
                indexed_ref = bwaindex.databank
            
            # align reads against indexed genome
            sample_lane_prefixes = None
            if self.group_prefix != None :
                sample_lane_prefixes = list((Utils.get_group_basenames(filtered_read1_files+filtered_read2_files, "lane")).keys())
            if self.align_subset_reads:
                subset = self.add_component("SubsetSeqFiles", [filtered_read1_files, filtered_read2_files], parent = fastqilluminafilter)
                filtered_read1_files = subset.subset_read1
                filtered_read2_files = subset.subset_read2
                bwa = self.add_component("BWA", [indexed_ref, filtered_read1_files, filtered_read2_files, sample_lane_prefixes, "mem", not self.delete_bam], parent = subset)
            else:
                bwa = self.add_component("BWA", [indexed_ref, filtered_read1_files, filtered_read2_files, sample_lane_prefixes, "mem", not self.delete_bam], parent = fastqilluminafilter)
            
            # make some statistic on the alignement
            alignmentstats = self.add_component("AlignmentStats", [bwa.bam_files, self.is_paired_end(), True], parent = bwa)
            
            if self.is_paired_end():
                # process insert sizes
                insertssizes = self.add_component("InsertsSizes", [bwa.bam_files, self.histogram_width, self.min_pct, "LENIENT", "inserts_sizes.tar.gz"], parent = bwa)
        
        if self.assignation_databank:
            R1_prefix = self.group_prefix
            if self.group_prefix != None :
                R1_prefix = list((Utils.get_group_basenames(filtered_read1_files, "read")).keys())
            # subset assignation
            subset_assignation = self.add_component("SubsetAssignation", [filtered_read1_files, self.assignation_databank, R1_prefix], parent = fastqilluminafilter)
