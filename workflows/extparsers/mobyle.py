#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import re
import copy
import fnmatch
import xml.etree.ElementTree as ET

from jflow.extparser import ExternalParser
from jflow.parameter import OutputFile
import argparse

class MobyleParser(ExternalParser):
    
    def parse(self, component_file):
        parameters = []
        parameters_names = []
        tree = ET.parse(component_file)
        root = tree.getroot()
        
        # get command string
        command = root.findtext(".//head/command", None)
        
        # retrieve all parameters from xml file 
        for parameterNode in root.findall('.//parameters/parameter[name]'):
            attrs = parameterNode.attrib
            param = self._parseParameter(parameterNode)
            if param['name'] in parameters_names :
                raise Exception('Duplicated parameter (%s)'%param['name'])
            parameters.append(param)
            parameters_names.append(param['name'])

        def fn_get_command(self):
            if command is None:
                return self._command
            return command 
    
        def fn_get_abstraction(self):
            return
    
        def fn_define_parameters(self, **kwargs):
            for userarg in list(kwargs.keys()):
                if userarg not in parameters_names :
                    raise Exception("Invalid argument '%s' for %s"%(userarg, self.get_nameid()))
            
            filenames_parameters = []
            for param in parameters:
                pname = param['name']
                param['value'] = param.get('vdef')
                
                if pname in kwargs :
                    param['value'] = kwargs[pname]
                
                printval = param['value']
                if param['type'] == 'file' or type(param['value']) == str :
                     printval = "'" + str(param['value']) + "'"
                
                exec("%s = %s"%(pname, printval), globals(), locals())
                
                if param['isfilename'] :
                    filenames_parameters.append(param)
            
            # resolve format
            for param in parameters:
                pname = param['name']
                value = param.get('value')
                vdef = param.get('vdef')
                
                if param['format'] is not None :
                    param['format'] = eval( param['format'] )
                    param['format'] = re.sub(r""+str(value)+"", "", param['format'])
                    # TODO : do I need to remove the '=' ?????
                    param['format'] = re.sub(r"=$", "", param['format'])
                    
                    # empty format here means no value
                    if not param['format'].strip() :
                        param['format'] = None
                        param['value'] = None
                
                elif param['flist'] :
                    vlist = list(param['flist'].keys())
                    # custom string type depending on vlist for keys and convert to flist
                    
                    class flist_type(jflow.extparser._serializable_nested_function):
                        def __call__(self, vvv):
                            if vvv in vlist :
                                return  eval(param['flist'][vvv])
                            raise argparse.ArgumentTypeError("%s is an invalid argument, valid ones are (%s)"%(vvv, ', '.join( map(str,vlist) )) )
                    
                    param['type'] = flist_type
                    param['vlist'] = vlist
                    if param['value'] :
                        param['value'] = flist_type(param['value'])
                
            
            # add parameters
            for param in parameters:
                pname = param['name']
                if param['iscommand'] :
                    self._command = param['value'] or param['format']
                    continue
                
                arguments = {}
                arguments['required'] = param.get('required', False)
                if param['value'] :
                    arguments['default'] = param['value']
                if param['vlist'] :
                    arguments['choices'] = param['vlist'] 
                if param['format'] :
                    arguments['cmd_format'] = param['format']
                if param['argpos'] :
                    arguments['argpos'] = param['argpos']
                
                if param['preconditions'] :
                    all_precond_true = True
                    for precond in param['preconditions'] :
                        try :
                            eval(precond)
                        except :
                            all_precond_true = False
                            break
                    if not  all_precond_true :
                        arguments.pop('default', None)
                        if param['type'] == 'file' :
                            if not param['isoutput'] :
                                arguments.pop('choices', None)
                                self.add_input_file(pname, param['help'], **arguments)
                            # do not add output files
                        else :
                            self.add_parameter(pname, param['help'], **arguments)
                        continue # --> next parameter
                
                if param['isfilename'] :
                    arguments.pop('default', None)
                    arguments.pop('required', None)
                    arguments.pop('choices', None)    
                    arguments['filename'] = param['value']
                    self.add_output_file( pname, param['help'], **arguments )
                
                elif param['type'] == 'file' :
                    if not param['isoutput'] :
                        arguments.pop('choices', None)
                        self.add_input_file(pname, param['help'], **arguments)
                    # output file
                    else :
                        arguments.pop('choices', None)
                        arguments.pop('default', None)
                        arguments.pop('required', None) 
                        
                        nfilenames = []
                        for expression in param['filenames'] :
                            try :
                                expression = eval(expression)
                            except : pass
                            nfilenames.append(expression)
                        param['filenames'] =  nfilenames          

                        if len(param['filenames']) == 1 :
                            filename = self.get_outputs("{basename}", param['filenames'])[0]
                            in_parameters = False
                            for param_name in self.params_order :
                                obj = self.__getattribute__(param_name)
                                if isinstance(obj, OutputFile) :
                                    if obj.default == filename :
                                        in_parameters = True
                                        break
                            if in_parameters :
                                continue # next parameter, no need to add this one 
                        # regexp to match all filenames
                        regexp = "|".join([ fnmatch.translate(e) for e in param['filenames']])
                        print(">>regex for %s    %s"%(pname,regexp))
                        self.add_output_file_pattern( pname, param['help'], regexp, **arguments )
                        
                else :
                    arguments['type'] = param['type']
                    self.add_parameter(pname, param['help'], **arguments)
                
            # controls
            for param in parameters :
                pname = param['name']
                if param.get("controls") :
                    value = param.get('value')
                    vdef = param.get('vdef')
                    if value :
                        for ctrl in  param.get("controls") :
                            if not eval(ctrl[0]) :
                                raise Exception('The parameter %s does not respect the following condition : %s'%(pname, ctrl[1]))
                
                
        component_name = root.find(".//head/name").text.replace('-', ' ').replace('_', ' ')
        component_name = "".join(component_name.title().split())
        
        return self.build_component(component_name, fn_define_parameters, get_command = fn_get_command, get_abstraction = fn_get_abstraction)

    def _parseParameter(self, parameterNode):    
        attrs = parameterNode.attrib
        PYTHON_TYPES = {
            'String'    : { 'func' : str, 'str' : 'str' }, 
            'Integer'   : { 'func' : int, 'str' : 'int' },
            'Float'     : { 'func' : float, 'str' : 'float'}, 
            'Boolean'   : { 'func' : lambda x : bool(int(x)), 'str': 'bool'}
        }               
        
        SIMPLE_TYPES = [ 'Choice', 'MultipleChoice']
    
        param = {
          'name'            : parameterNode.findtext('./name'),
          'help'            : parameterNode.findtext('./prompt'),
          'iscommand'       : False,
          'format'          : None,
          'type'            : None,
          'isfilename'      : False,
          'required'        : False,
          'multiple'        : False,
          'isoutput'        : False,
          'isstdout'        : False,
          'vdef'            : None,
          'argpos'          : -1,
          'vlist'           : None, # list of choices
          'flist'           : None, # list of format
          'controls'        : None,
          'preconditions'   : None,
          'filenames'        : None
        }

        if  'iscommand' in attrs and attrs['iscommand'] in ["1","true"] :
            param['iscommand'] = True
        if  ('ismandatory' in attrs and  attrs['ismandatory'] in ["1","true"]) or \
            ('ismaininput' in attrs and  attrs['ismaininput'] in ["1","true"]) :
            param['required'] = True
        if 'isout' in attrs and  attrs[ 'isout' ] in ["1","true"]:
            param['isoutput'] = True
        if 'isstdout' in attrs and  attrs[ 'isstdout' ] in ["1","true"]:
            param['isstdout'] = True 
            param['isoutput'] = True
        
        casting = None
        klass = parameterNode.findtext("./type/datatype/class", '')
        klass = parameterNode.findtext("./type/datatype/superclass", '').strip() or klass.strip()
        
        if not klass :
            raise Exception('Mobyle parameter parsing error, Missing <type> in param %s '%param['name'])
        
        if klass in PYTHON_TYPES :
            casting = PYTHON_TYPES[klass]['func']
            param['type'] =  PYTHON_TYPES[klass]['str']
        elif klass == "Filename" :
            casting = PYTHON_TYPES['String']['func']
            param['type'] =  PYTHON_TYPES['String']['str']
            param['isfilename']  = True
        elif klass not in SIMPLE_TYPES :
            param['type'] = 'file'
        # TODO : missing the case of choice in SIMPLE_TYPES ....
        
        # format represent the flag of the command
        formatCode = parameterNode.findtext('./format/code[@proglang="python"]', '').strip() 
        if formatCode :
            param['format'] = formatCode 
        
        try :
            param['argpos'] = int( parameterNode.findtext( './argpos' ) )
        except : pass
            
        # default value    
        vdefs = [] 
        for vdefNode in parameterNode.findall( './vdef/value' ):
            vdefs.append( casting(vdefNode.text) if casting else vdefNode.text)
        if vdefs:
            if len(vdefs) == 1 :
                param['vdef'] =  casting(vdefs[0]) if casting else vdefs[0]
            else :
                param['vdef'] = list(map(casting, vdefs)) if casting else vdefs
        
        # vlist : choices
        if parameterNode.find( './vlist' ) is not None :
            vlist = []
            for velem in parameterNode.findall( './vlist/velem' ) :
                label = velem.findtext('./value' , '' ).strip()
                val = velem.findtext('./value' , '' ).strip()
                if velem.attrib.get('undef', '') in ['1', "true"] :
                    if param['vdef'] == label :
                        param['vdef'] = None
                else :
                    vlist.append( casting(val) if casting else val)
            if vlist :
                param['vlist'] = vlist

        # flist : list of format
        if parameterNode.find( './flist' ) is not None :
            flist = {}
            for felem in parameterNode.findall( './flist/felem' ) :
                label = felem.findtext('./value' , '' ).strip()
                format = felem.findtext('./code[@proglang="python"]' , '' ).strip()
                if felem.attrib.get('undef', '') in ['1', "true"] :
                    if param['vdef'] == label :
                        param['vdef'] = None 
                else :
                    flist[label] = format
            if flist:
                param['flist'] = flist
        
        # controls
        controls = []
        for ctrl in parameterNode.findall( './ctrl' ):
            message = ' '.join([ n.text for n in  ctrl.findall( './message/text' )])
            for codeNode in ctrl.findall( './code[@proglang="python"]'):
                code = codeNode.text
                controls.append( (code,message) )
        if controls :
            param['controls'] = controls
        
        # preconditions
        preconditions = []
        for precondNode in parameterNode.findall( './precond/code[@proglang="python"]' ) :
            precond = precondNode.text.strip() 
            if precond != '':
                preconditions.append(precond)
        if preconditions:
            param['preconditions'] = preconditions
        
        # paramfile, I still don't know what this is used for ....
        paramfile = parameterNode.find( './paramfile' )
        if paramfile is not None:
            param['paramfile'] = paramfile.text 
    
        # output filenames mask
        filenamesCode = parameterNode.findall( './filenames/code[@proglang="python"]')
        fnames = []
        for codeNode in filenamesCode :
            fnames.append(codeNode.text.strip())
        if fnames :
            param['filenames'] = fnames

       
        # another property I didn't get ... I think it is used to test the range of possible
        # values for a defined value ... (a custom type ?)
        scaleNode = parameterNode.find( './scale')
        if scaleNode is not None:
            minNode = scaleNode.find('./min' )
            maxNode = scaleNode.find('./max' )
            if minNode is not None or maxNode is not None:
                try:
                    inc = scaleNode.find('./inc' ).text
                except:
                    inc = None
                try:
                    max = maxNode.find( './value' ).text
                    min = minNode.find( './value' ).text
                    param['scale'] = ( min , max , inc )
                except:
                    try:
                        minCodes = {}
                        maxCodes = {}
                        max = maxNode.find( './code[@proglang="python"]' ).text
                        min = minNode.find( './code[@proglang="python"]' ).text
                        param['scale'] = ( min , max , inc )
                    except : pass
        
        return param  
    
