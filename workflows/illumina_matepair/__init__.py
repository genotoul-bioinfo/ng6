#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import sys
import re

from ng6.ng6workflow import CasavaNG6Workflow
from ng6.utils import Utils


class IlluminaMatePair (CasavaNG6Workflow):
    
    def get_name(self):
        return 'illumina_matepair'
    
    def get_description(self):
        return "illumina quality check pipeline for matepair analyse"
        
    def define_parameters(self, function="process"):
        self.add_input_file("reference_genome", "Which genome should the read being align on")
        self.add_parameter("delete_bam", "The BAM are not stored", type=bool, default = False)
        self.add_parameter("histogram_width", "Explicitly sets the histogram width, overriding automatic truncation of histogram tail", type=int, default = 10000, group="INSERTSIZE section")
        self.add_parameter("min_pct", "When generating the histogram, discard any data categories (out of FR, TANDEM, RF) that have"+
                           " fewer than this percentage of overall reads", type=float, default = 0.01, group="INSERTSIZE section")
         
    def process(self):
        fastqilluminafilter, filtered_read1_files, filtered_read2_files, concat_files, concatenatefastq = self.illumina_process()
        
        # mate_pair analyse 
        concatenate1 = self.add_component("ConcatenateFilesGroups", [self.runobj,filtered_read1_files, list((Utils.get_group_basenames(self.get_all_reads('read1'), "read")).keys())],component_prefix="read1")
        concatenate2 = self.add_component("ConcatenateFilesGroups", [self.runobj,filtered_read2_files, list((Utils.get_group_basenames(self.get_all_reads('read2'), "read")).keys())],component_prefix="read2")
        concatenate1.concat_files = sorted(concatenate1.concat_files)
        concatenate2.concat_files = sorted(concatenate2.concat_files)
        
        cutadapt = self.add_component("CutAdapt",[concatenate1.concat_files, {"b":["CTGTCTCTT","ATACACATCT","AGATCTAT","AAGAGACAG"]}, concatenate2.concat_files,
                                {"b":["CTGTCTCTT","ATACACATCT","AGATCTAT","AAGAGACAG"]}, 0.1,4,20 ],  parent= fastqilluminafilter)
        
        #reverse_complement
        
        revcom1 = self.add_component("FastxReverseComplement",[cutadapt.output_files_R1],component_prefix="read1")
        revcom2 = self.add_component("FastxReverseComplement",[cutadapt.output_files_R2],component_prefix="read2")
        revcom1.output_files = sorted(revcom1.output_files)
        revcom2.output_files = sorted(revcom2.output_files)
        
        # make some statistics on filtered file
        fastqc = self.add_component("FastQC", [revcom1.output_files+revcom2.output_files, (self.group_prefix is not None), True, "_fastqc.tar.gz"], parent = cutadapt, component_prefix="Trimmed_read")
        
        
        if self.reference_genome:
            # index the reference genome if not already indexed
            indexed_ref = self.reference_genome
            if not os.path.exists( self.reference_genome + ".bwt" ):
                bwaindex = self.add_component("BWAIndex", [self.reference_genome])
                indexed_ref = bwaindex.databank
            
            # align reads against indexed genome
            sample_lane_prefixes = None
            if self.group_prefix is not None :
                sample_lane_prefixes = list((Utils.get_group_basenames(revcom1.output_files+revcom2.output_files, "lane")).keys())
            #bwa = self.add_component("BWA", [indexed_ref, gunzip.fastq_R1 , gunzip.fastq_R2, sample_lane_prefixes], parent = cutadapt)
            bwa = self.add_component("BWA", [indexed_ref, revcom1.output_files , revcom2.output_files, sample_lane_prefixes, 'mem', not self.delete_bam], parent = cutadapt)
            # make some statistic on the alignement
            alignmentstats = self.add_component("AlignmentStats", [bwa.bam_files, self.is_paired_end(), False], parent = bwa)
            
            if self.is_paired_end():
                # process insert sizes
                insertssizes = self.add_component("InsertsSizes", [bwa.bam_files, 10000, self.min_pct, "LENIENT", "inserts_sizes.tar.gz"], parent = bwa)
