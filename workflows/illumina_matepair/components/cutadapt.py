#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import re
from subprocess import Popen, PIPE

import jflow.seqio as seqio

from ng6.analysis import Analysis

class CutAdapt (Analysis):
    def _get_length_table(self, input_file):
        """
          @param input_file  : the fastq file path
          @return            : [nb_seq, {size: nb, size:...}]
        """       
        nb_seq = 0
        reader = seqio.SequenceReader(input_file)
        sizes = {}
        for id, desc, seq, qualities in reader:
            nb_seq += 1
            if len(seq) in sizes:
                sizes[len(seq)] += 1
            else:
                sizes[len(seq)] = 1
        return [nb_seq, sizes]
    
    def define_parameters(self, input_files_R1, adapt_FWD, input_files_R2 = None, adapt_REV=None, error_rate=0.1, 
                          times=None, min_length=None, max_length=None, overlap_length=None):
        self.add_parameter("error_rate", "error_rate", default=error_rate, type='float')
        self.add_parameter("min_length", "min_length", default=min_length, type='int')
        self.add_parameter("max_length", "max_length", default=max_length, type='int')
        self.add_parameter("overlap_length", "overlap_length", default=overlap_length, type='int')
        self.add_parameter("times", "times", default=times, type='int')
        
        self.adapt_FWD = adapt_FWD
        self.add_input_file_list( "input_files_R1", "input files read1", default=input_files_R1, required=True, file_format='fastq')
        self.add_output_file_list( "output_files_R1", "output files read1", pattern='{basename_woext}_cut.fastq', items=self.input_files_R1)
        self.add_output_file_list( "log_files_R1", "log files read1", pattern='{basename_woext}.stderr', items=self.input_files_R1)
        
        self.adapt_REV = adapt_REV
        self.is_paired = False
        if input_files_R2 :
            self.is_paired = True
            self.add_input_file_list( "input_files_R2", "input files read2", default=input_files_R2, file_format='fastq')
            self.add_output_file_list( "output_files_R2", "output files read2", pattern='{basename_woext}_cut.fastq', items=self.input_files_R2)
            self.add_output_file_list( "log_files_R2", "log files read2", pattern='{basename_woext}.stderr', items=self.input_files_R2)
            
            if self.min_length or self.max_length or self.overlap_length :
                self.add_output_file_list( "tmp_files_R1", "tmp filefiles read1", pattern='{basename_woext}_tmp.fastq', items=self.input_files_R1)
                self.add_output_file_list( "tmp_files_R2", "tmp filefiles read2", pattern='{basename_woext}_tmp.fastq', items=self.input_files_R2)

        
    def define_analysis(self):
        self.name = "CutAdapt"
        self.description = "Remove adapters"
        self.software = "cutadapt"
        self.options = "MAIN "
        self.cmdline_options = []
        if self.error_rate:
            self.cmdline_options.extend(["--error-rate", str(self.error_rate)])
        if self.min_length:
            self.cmdline_options.extend(["--minimum-length", str(self.min_length)])
        if self.max_length:
            self.cmdline_options.extend(["--maximum-length", str(self.max_length)])
        if self.overlap_length:
            self.cmdline_options.extend(["--overlap", str(self.overlap_length)])
        if self.times:
            self.cmdline_options.extend(["--times", str(self.times) ])
        
        self.options += " ".join(self.cmdline_options)
        self.options += " ; FWD "
        self.options_FWD = []
        # adapter for forward
        for cle, valeur in list(self.adapt_FWD.items()) : 
            if cle in ['a', 'b', 'g'] :
                for adapt in valeur :
                    self.options_FWD.extend([ '-'+cle, adapt ])
                    self.options += " -%s %s "% (cle,adapt)
        
        self.options_REV = []
        if self.adapt_REV :
            self.options += " ; REV "
            for cle, valeur in list(self.adapt_REV.items()) : 
                if cle in ['a', 'b', 'g'] :
                    for adapt in valeur :
                        self.options_REV.extend([ '-'+cle, adapt ])
                        self.options += " -%s %s "% (cle,adapt)
        
    def post_process(self):
                  
        for output_file_R1 in self.log_files_R1:
            sample = os.path.splitext(os.path.basename(output_file_R1))[0]
                            
            # Parse results
            metrics = self.parse_metrics_file(output_file_R1)
            self._add_result_element(sample, "processedread", str(metrics["processedread"]))
            self._add_result_element(sample, "processedbase", str(metrics["processedbase"]))
            self._add_result_element(sample, "trimmedread", str(metrics["trimmedread"]))
            self._add_result_element(sample, "trimmedbase", str(metrics["trimmedbase"]))
            self._add_result_element(sample, "shortread", str(metrics["shortread"]))
            self._add_result_element(sample, "longread", str(metrics["longread"]))

        samples = {}
        # Process metrics from the final output reads 1
        for filepath in self.output_files_R1:
            [nb_seq, sizes] = self._get_length_table(filepath)
            x = []
            y = []
            for val in list(sizes.keys()):
                x.append(val)
            x = sorted(x)
            for i in x:
                y.append(sizes[i])
            if self.is_paired : 
                sample_name = os.path.basename(filepath).split("_cut")[0]
            else : 
                sample_name = os.path.splitext(os.path.basename(filepath))[0]
            if sample_name not in samples:
                samples[sample_name] = {}
            samples[sample_name]["final_read"] = str(nb_seq)
            samples[sample_name]["size"] = str(",".join([str(v) for v in x]))
            samples[sample_name]["nb_size"] = str(",".join([str(v) for v in y]))
            self._save_file(filepath, gzip=True)
                
        if self.is_paired : 
            
            for output_file_R2 in self.log_files_R2 : 
                sample = os.path.splitext(os.path.basename(output_file_R2))[0]
                # Parse results
                metrics = self.parse_metrics_file(output_file_R2)
                self._add_result_element(sample, "processedread", str(metrics["processedread"]))
                self._add_result_element(sample, "processedbase", str(metrics["processedbase"]))
                self._add_result_element(sample, "trimmedread", str(metrics["trimmedread"]))
                self._add_result_element(sample, "trimmedbase", str(metrics["trimmedbase"]))
                self._add_result_element(sample, "shortread", str(metrics["shortread"]))
                self._add_result_element(sample, "longread", str(metrics["longread"]))
          
            # Process metrics from the final output reads 2
            for filepath in self.output_files_R2:
                [nb_seq, sizes] = self._get_length_table(filepath)
                x = []
                y = []
                for val in list(sizes.keys()):
                    x.append(val)
                x = sorted(x)
                for i in x:
                    y.append(sizes[i])
                sample_name = os.path.basename(filepath).split("_cut")[0]
                if sample_name not in samples:
                    samples[sample_name] = {}
                samples[sample_name]["final_read"] = str(nb_seq)
                samples[sample_name]["size"] = str(",".join([str(v) for v in x]))
                samples[sample_name]["nb_size"] = str(",".join([str(v) for v in y]))
                self._save_file(filepath, gzip=True)
                
        for sample in samples:        
            self._add_result_element(sample, "final_read", samples[sample]["final_read"])
            self._add_result_element(sample, "size", samples[sample]["size"])
            self._add_result_element(sample, "nb_size", samples[sample]["nb_size"])
         
    def get_version(self):
        cmd = [self.get_exec_path("cutadapt"),"--version"]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stdout.split()[0]
                     
    def process(self):
        if self.is_paired :
            if self.min_length or self.max_length or self.overlap_length :
                params = " ".join(self.options_REV + self.cmdline_options)
                self.cmdline_options.extend(["--paired-output", "$3"])

                # cutadapt forward
                self.add_shell_execution(self.get_exec_path("cutadapt") + " " + " ".join(self.options_FWD + self.cmdline_options) 
                                         + " --paired-output $4 -o $3 $1 $2 > $5", cmd_format = '{EXE} {IN} {OUT}', map=True,
                                         inputs = [self.input_files_R1, self.input_files_R2], outputs=[self.tmp_files_R1, self.tmp_files_R2, self.log_files_R1])
                
                #cutadapt reverse
                self.add_shell_execution(self.get_exec_path("cutadapt") + " " + " ".join(self.options_REV + self.cmdline_options) 
                                         + " --paired-output $3 -o $4 $1 $2 > $5", cmd_format = '{EXE} {IN} {OUT}', map=True,
                                         inputs = [self.tmp_files_R2, self.tmp_files_R1], outputs = [self.output_files_R1, self.output_files_R2, self.log_files_R2])
            else :
                #forward read
                self.add_shell_execution(self.get_exec_path("cutadapt") + " " + " ".join(self.options_FWD + self.cmdline_options) 
                                         + " -o $2 $1 > $3", cmd_format = '{EXE} {IN} {OUT}', map=True,
                                         inputs = [self.input_files_R1], outputs=[self.output_files_R1, self.log_files_R1])
                
                # reverse read
                self.add_shell_execution(self.get_exec_path("cutadapt") + " " + " ".join(self.options_REV + self.cmdline_options) 
                                         + " -o $2 $1 > $3", cmd_format = '{EXE} {IN} {OUT}', map=True,
                                         inputs = [self.input_files_R2], outputs=[self.output_files_R2, self.log_files_R2])
                
        else :
            self.add_shell_execution(self.get_exec_path("cutadapt") + " " + " ".join(self.options_FWD + self.cmdline_options) 
                             + " -o $2 $1 > $3", cmd_format = '{EXE} {IN} {OUT}', map=True,
                             inputs = [self.input_files_R1], outputs=[self.output_files_R1, self.log_files_R1])
            
    def parse_metrics_file(self, input_file):
        """
          @param input_file  : the output metrics file path
          @return            : string with inserts sizes class, metrics dictionnary
        """
        stats = {}
        print(input_file)
        for line in open(input_file, 'r').readlines():
            if re.search("Total read pairs processed",line):
                stats["processedread"] = line.strip().split()[2]
            if re.search("Total basepairs processed:",line):
                stats["processedbase"] = line.strip().split()[2]
            if re.search("Trimmed reads",line):
                stats["trimmedread"] = line.strip().split()[2]
            if re.search("Trimmed bases",line):
                stats["trimmedbase"] = line.strip().split()[2]
            if re.search("Pairs that were too short",line):
                stats["shortread"] = line.strip().split()[3]
            if re.search("Too long reads",line):
                stats["longread"] = line.strip().split()[3]
        return stats


