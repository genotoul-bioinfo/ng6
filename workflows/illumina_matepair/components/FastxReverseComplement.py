#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.component import Component


class FastxReverseComplement (Component):

    def define_parameters(self, input_files, qual_offset=33):
        
        self.add_input_file_list( "input_files", "input_files", default=input_files, required=True)
        if( self.input_files[0].endswith("fastq.gz") or self.input_files[0].endswith(".fastq") ):
            self.add_output_file_list( "output_files", "output_files", pattern='{basename}', items=self.input_files, file_format='fastq')
        else:
            self.add_output_file_list( "output_files", "output_files", pattern='{basename}', items=self.input_files)
        self.add_output_file_list( "stderr", "stderr", pattern='{basename_woext}.stderr', items=self.input_files)
        self.add_parameter("qual_offset", "qual_offset", default=qual_offset, type="int")
        self.options = " -Q " + str(self.qual_offset)
    
    def process(self):
        if self.input_files[0].endswith(".gz"):
            self.add_shell_execution('zcat $1 2> $3 | ' + self.get_exec_path("fastx_reverse_complement") + ' ' + self.options + ' -z -i - -o $2 2>> $3', 
                                     cmd_format="{EXE} {IN} {OUT}", map=True,
                                     inputs=self.input_files, outputs=[self.output_files, self.stderr])
        else:
            self.add_shell_execution(self.get_exec_path("fastx_reverse_complement") + ' ' + self.options + ' -i $1 -o $2 2> $3',
                                     cmd_format="{EXE} {IN} {OUT}", map=True,
                                     inputs=self.input_files, outputs=[self.output_files, self.stderr])