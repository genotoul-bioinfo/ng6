#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os, subprocess

from ng6.analysis import Analysis

class Splitbc (Analysis):
    
    def define_parameters(self, fastqs1, barcodes, fastqs2 = None, 
                          mismatches = None , trim_barcode = False, trim_reads2 = False, 
                          bol = True, eol = False, partial = None, no_adapt = False):
        """
            @param fastq_file1: fastq_file1 path or list of fastq file path
            @param barcodes: barcode file path or list of barcode file path
            @param fastq_file2: fastq_file2 path or list of fastq file path 
        """
        self.add_input_file_list( "fastqs1", "Fastq read1 files paths", default=fastqs1, required=True, file_format = 'fastq')
        self.add_input_file_list( "barcodes", "barcodes files path", default=barcodes, required=True)
        self.add_input_file_list( "fastqs2", "Fastq read2 files paths", default=fastqs2, file_format = 'fastq')
        self.add_parameter("mismatches", "mismatches", default=mismatches, type=int)
        self.add_parameter("trim_barcode", "trim_barcode", default=trim_barcode, type=bool)
        self.add_parameter("trim_reads2", "trim_reads2", default=trim_reads2, type=bool)
        self.add_parameter("bol", "bol", default=bol, type=bool)
        self.add_parameter("eol", "eol", default=eol, type=bool)
        self.add_parameter("partial", "partial", default=partial, type=int)
        self.add_parameter("no_adapt", "no_adapt", default=no_adapt, type=bool)

        if self.trim_barcode and self.trim_reads2 :
                raise Exception("you must specify either trim_barcode or trim_reads2, but not both")

        if (self.bol == self.eol == True ) or ( self.bol == self.eol == False) :
            raise Exception("One of bol, eol must be specified, but not both")
            
        if self.fastqs2 :
            assert len(self.fastqs1) == len(self.fastqs2) , "fastq_file1 and fastq_file2 must be of the same length"
        assert len(self.fastqs1) == len(self.barcodes) , "fastq_file1 and fastq_file2 must be of the same length"
        
        self.matrix_read1 = []
        self.matrix_read2 = []
        self.output_read1 = []
        self.output_read2 = []
        self.stdouts = []
        self.pools_output_dirs = []
        self.bcfile = self.get_temporary_file()
        
        with open(self.bcfile, 'w') as bc_fh :
            for id in range(len(self.fastqs1)):
                pool_outdir =  "pool_" + str(id)
                self.pools_output_dirs.append(  os.path.join( self.output_directory , pool_outdir ) )
                inames = []
                with open(self.barcodes[id]) as fh :
                    for line in fh :
                        line = line.rstrip()
                        if line :
                            bc_fh.write(line + "\n")
                            inames.append(line.split()[0])
                self.add_output_file_list( "outr1_" + str(id), "Output fastq files", pattern=pool_outdir+'/{basename_woext}_1.fq', items=inames, file_format='fastq')
                self.matrix_read1.append(getattr(self,"outr1_" + str(id)))
                self.output_read1 += getattr(self,"outr1_" + str(id))
                
                if self.fastqs2 :
                    self.add_output_file_list( "outr2_" + str(id), "Output fastq files", pattern=pool_outdir+'/{basename_woext}_2.fq', items=inames, file_format='fastq')
                    self.matrix_read2.append(getattr(self,"outr2_" + str(id)))
                    self.output_read2 += getattr(self,"outr2_" + str(id))
        self.add_output_file_list( "stdouts", "stdouts", pattern='splitBC_pool{FULL}.stdout', items=list(range(len(self.fastqs1))))
    
    def get_version(self):
        proc = subprocess.Popen( self.get_exec_path("splitbc.pl") + ' --version' , shell=True, stdout = subprocess.PIPE  )
        stdout, stderr = proc.communicate()
        return stdout

    def define_analysis(self):
        self.name = "Demultiplexing"
        self.description = "demultiplexing samples"
        self.software = os.path.basename(self.get_exec_path("splitbc.pl"))
        self.options = [ "--bcfile", str(self.bcfile) ]
        
        if self.bol :
            self.options.append('--bol')
        elif self.eol :
            self.options.append('--eol')
        
        if self.mismatches != None :
            self.options.extend(["--mismatches", str(self.mismatches)])
        
        if self.partial != None :
            self.options.extend(["--partial", str(self.partial)])
        
        if self.trim_barcode :
            self.options.append( "--trim")
        elif self.trim_reads2 :
            self.options.append( "--trim2")
            
        if self.no_adapt != None :
            self.options.append("--no_adapt")
        
        self.options = ' '.join(self.options)
    
    def post_process(self):
        self._add_result_element("barcode_file", "barcode_file", self._save_file(self.bcfile, "barcode_file") )
        rkey = 'single'
        if self.output_read2 :
            rkey = 'paired'
        for id, filepath in enumerate(self.stdouts) :
            with open(filepath) as fp :
                for line in fp:
                    ar = line.split('\t')
                    name = ar[0]
                    val = ar[1].replace("(*2)", "")
                    
                    if name in ["Barcode","ambiguous","unmatched"] :
                    #if name in ["Barcode", "total"] or name.endswith("_2rad") :
                        if name in ["ambiguous"] :
                            self._add_result_element(name, rkey, val, "ambiguous")
                        elif name in ["unmatched"] :
                            self._add_result_element(name, rkey, val, "unmatched")
                        continue
                    
                    self._add_result_element(name, rkey, val)
    
    def process(self):
        command_base = [self.get_exec_path("splitbc.pl")]
        
        if self.bol :
            command_base.append('--bol')
        elif self.eol :
            command_base.append('--eol')
        
        if self.mismatches != None :
            command_base.extend(["--mismatches", str(self.mismatches)])
        
        if self.partial != None :
            command_base.extend(["--partial", str(self.partial)])
        
        if self.trim_barcode :
            command_base.append( "--trim")
        elif self.trim_reads2 :
            command_base.append( "--trim2")
            
        if self.no_adapt != None :
            command_base.append("--no_adapt")
        
        for id, fastq1 in enumerate(self.fastqs1):
            stdout = self.stdouts[id]
            barcode = self.barcodes[id]
            outputs_read1 = self.matrix_read1[id]
            pool_outdir = self.pools_output_dirs[id]
            command = ["mkdir", pool_outdir, ";"] + command_base
            command.extend(["--prefix-r1", os.path.join(pool_outdir,  "%_1.fq") , '--bcfile', barcode])
            
            if self.fastqs2 :
                #print ">>>fastq2", self.fastqs2
                fastq2 = self.fastqs2[id]
                outputs_read2 = self.matrix_read2[id]
                command.extend(["--prefix-r2", os.path.join(pool_outdir,  "%_2.fq") , "$1", "$2", '2>&1 >> $3'])
                command = ' '.join(command)
                self.add_shell_execution(command, cmd_format='{EXE} {IN} {OUT}',includes = [barcode] , inputs=[fastq1, fastq2], outputs=[stdout, outputs_read1, outputs_read2],map=False)
            else :
                command.extend([ "$1", '2>&1 >> $2' ])
                command = ' '.join(command)
                self.add_shell_execution(command, cmd_format='{EXE} {IN} {OUT}',includes = [barcode] , inputs=[fastq1], outputs=[stdout, outputs_read1],map=False)