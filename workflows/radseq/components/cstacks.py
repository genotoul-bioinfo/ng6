#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from subprocess import Popen, PIPE
from jflow.component import Component
from collections import Counter
import numpy as np
import re

def wrap_cstacks(exec_path, mismatch, batch_id, out_dir, inputs_tags_list, output_tags, output_alleles, output_snps, stderr_path):
    from subprocess import Popen, PIPE

    cmd = [exec_path, "-b", batch_id, "-n", mismatch, "-o", out_dir]
    
    with open(inputs_tags_list, 'r') as input_tags : 
        for i in input_tags:
            cmd.append("-s")
            cmd.append(os.path.abspath(i.strip()).replace(".tags"," ").split(" ")[0])
    
    p = Popen(cmd, stderr=PIPE)
    stderr = p.communicate()[1]
    # write down the stderr
    stdeh = open(stderr_path, "wb")
    stdeh.write(stderr)
    stdeh.close()

class Cstacks (Component):
    
    def define_parameters(self, alleles_files, snps_files, tags_files, samples_id, population_list, batch_id=1, mismatch=1):
        
        """
        @param alleles_files : list of individuals alleles files
        @param snps_files : list of individuals snps files
        @param tags_files : list of individuals clustering files
        @param samples_id : list of individuals id
        @param samples_id : list of individuals id
        @param population_list :which sample belong to which population, same order as sample_id
        @param batch_id (-b) : batch id
        @param mismatch (-n) : number of mismatches allowed between sample tags when generating the catalog. default 1
        """
        self.add_input_file_list( "alleles_files", "list of individuals alleles files", default=alleles_files, required=True)
        self.add_input_file_list( "snps_files", "list of individuals snps files", default=snps_files, required=True)
        self.add_input_file_list( "tags_files", "list of individuals clustering files", default=tags_files, required=True)
        if len(self.tags_files) != len(self.alleles_files) and len(self.tags_files)!= len(self.snps_files) :
            raise Exception("[ERROR] : number of ustacks input files are not equal. Please check the ustacks result")
        
        self.add_parameter_list( "samples_id", "individuals id list", default=samples_id, required=False)
        self.add_parameter_list( "population_list", "which sample belong to which population, same order as sample_id", default=population_list, required=False)
        if len(self.samples_id) != len(self.population_list) :
            raise Exception("[ERROR] : samples_id and population_list are not equal. Please check your inputs")
        
        self.add_parameter("batch_id", "batch_id", default=batch_id, type=int)
        self.add_parameter("mismatch", "number of mismatches allowed between sample tags when generating the catalog.", default=mismatch, type=int)
        
        self.add_output_file("catalog_tags", "catalog cluster files", filename="batch_"+repr(self.batch_id)+".catalog.tags.tsv")
        self.add_output_file("catalog_alleles", "catalog alleles files", filename="batch_"+repr(self.batch_id)+".catalog.alleles.tsv")
        self.add_output_file("catalog_snps", "catalog snps files", filename="batch_"+repr(self.batch_id)+".catalog.snps.tsv")
        self.add_output_file("stderr", "cstacks log", filename="cstacks_batch_"+repr(self.batch_id)+".stderr")
    
    def get_version(self):
        cmd = [self.get_exec_path("cstacks"), "--version"]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stderr.split()[1]  
    
            
    def process(self):
        
        tmp_list_file = self.get_temporary_file()
        with open(tmp_list_file, 'w') as tmp_file :
                for tag in self.tags_files:
                    tmp_file.write(tag+"\n")
                           
        self.add_python_execution(wrap_cstacks, cmd_format="{EXE} {ARG} {IN} {OUT}",inputs = [tmp_list_file], outputs = [self.catalog_tags, self.catalog_alleles, self.catalog_snps, self.stderr], \
                arguments = [self.get_exec_path("cstacks"), self.mismatch, self.batch_id,self.output_directory], \
                includes = [self.tags_files, self.alleles_files, self.snps_files], map=False )