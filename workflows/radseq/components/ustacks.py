#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import re
import numpy as np
from subprocess import Popen, PIPE
from jflow.component import Component


class Ustacks (Component):
    
    def define_parameters(self, samples_id, read1_files, min_cov=3, primary_mismatch=2, secondary_mismatch=4, uncall_hap=False, removal_algo=True, \
                          deleveraging_algo=True, max_locus=3, model_type="snp", alpha=0.05, bound_low=0.0, bound_high=1.0, bc_err_freq=0.0):
        """
        @param samples_id (-i): list of individuals id
        @param read1_files (-1): paths to reads 1
        @param min_cov (-m): Minimum depth of coverage required to create a stack (default 3).
        @param primary_mismatch (-M) : Maximum distance (in nucleotides) allowed between stacks (default 2).
        @param secondary_mismatch (-N) : Maximum distance allowed to align secondary reads to primary stacks (adviced: M + 2).
        @param uncall_hap (-H) : disable calling haplotypes from secondary reads.
        @param removal_algo (-r) : enable the Removal algorithm, to drop highly-repetitive stacks (and nearby errors) from the algorithm.
        @param deleveraging_algo (-d) : enable the Deleveraging algorithm, used for resolving over merged tags.
        @param max_locus (--max_locus_stacks) : maximum number of stacks at a single de novo locus (default 3).
        @param model_type (--model_type) : either 'snp' (default), 'bounded', or 'fixed'
        @param alpha (--alpha) : chi square significance level required to call a heterozygote or homozygote, either 0.1, 0.05 (default), 0.01, or 0.001.
        @param bound_low (--bound_low) : lower bound for epsilon, the error rate, between 0 and 1.0 (default 0).
        @param bound_high (--bound_high) : upper bound for epsilon, the error rate, between 0 and 1.0 (default 1).
        @param bc_err_freq (--bc_err_freq) : specify the barcode error frequency, between 0 and 1.0.
        """
        self.add_parameter_list( "samples_id", "individuals id list", default=samples_id, required=True)
        self.add_input_file_list( "read1_files", "paths to reads 1", default=read1_files, required=True)
        if len(self.read1_files) != len(self.samples_id):
            raise Exception("[ERROR] : the number of files and the number of id are not equal. Please check your inputs")
        
        self.add_parameter("min_cov", "Minimum depth of coverage required to create a stack (default 3).", default=min_cov, type=int)
        self.add_parameter("primary_mismatch", "Maximum distance (in nucleotides) allowed between stacks (default 2).", default=primary_mismatch, type=int)
        self.add_parameter("secondary_mismatch", "Maximum distance allowed to align secondary reads to primary stacks (adviced: M + 2).", default=secondary_mismatch, type=int)
        self.add_parameter("uncall_hap", "disable calling haplotypes from secondary reads.", default=uncall_hap, type=bool)
        self.add_parameter("removal_algo", "enable the Removal algorithm, to drop highly-repetitive stacks (and nearby errors) from the algorithm.", default=removal_algo, type=bool)
        self.add_parameter("deleveraging_algo", "enable the Deleveraging algorithm, used for resolving over merged tags.", default=deleveraging_algo, type=bool)
        self.add_parameter("max_locus", "maximum number of stacks at a single de novo locus (default 3).", default=max_locus, type=int)
        self.add_parameter("model_type", "either 'snp' (default), 'bounded', or 'fixed'", default=model_type, type=str)
        if self.model_type not in ['snp', 'bounded',  'fixed'] :
            raise Exception("[ERROR] : the ustacks model type must be either 'snp' (default), 'bounded', or 'fixed'")
        
        if self.model_type == "snp" or self.model_type =="bounded" :
            self.add_parameter("alpha", "chi square significance level required to call a heterozygote or homozygote, either 0.1, 0.05 (default), 0.01, or 0.001.", default=alpha, type=float)
        if self.model_type == "bounded" :
            self.add_parameter("bound_high", "upper bound for epsilon, the error rate, between 0 and 1.0 (default 1).", default=bound_high, type=float)
            self.add_parameter("bound_low", "lower bound for epsilon, the error rate, between 0 and 1.0 (default 0).", default=bound_low, type=float)
            if self.bound_high < self.bound_low:
                raise Exception("[ERROR] : lower bound for epsilon must be smaller than upper bound")
        if self.model_type == "fixed" :
            self.add_parameter("bc_err_freq", "specify the barcode error frequency, between 0 and 1.0.", default=bc_err_freq, type=float)
        
        self.add_output_file_list("tags", "cluster files", pattern='{basename_woext}.tags.tsv.gz', items=self.read1_files)
        self.add_output_file_list("alleles", "haplotype files", pattern='{basename_woext}.alleles.tsv.gz', items=self.read1_files)
        self.add_output_file_list("snps", "snps files", pattern='{basename_woext}.snps.tsv.gz', items=self.read1_files)
        self.add_output_file_list("stderrs", "ustacks_log files", pattern='{basename_woext}.stderrs', items=self.read1_files)
    
    def get_version(self):
        cmd = [self.get_exec_path("ustacks"), "--version"]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stderr.split()[1]
    
    def process(self):
        
        self.options = " -m " + str(self.min_cov) + " -M " + str(self.primary_mismatch) + " -N " + str(self.secondary_mismatch) + \
        " --max_locus_stacks " + str(self.max_locus)
        
        if self.uncall_hap:
            self.options += " -H "
        if self.removal_algo:
            self.options += " -r "
        if self.deleveraging_algo:
            self.options += " -d "    
            
        self.options += " --model_type " + str(self.model_type)
    
        if self.model_type == "snp" or self.model_type == "bounded" : 
            self.options += " --alpha " + str(self.alpha)
        
        if self.model_type == "bounded" :
            self.options += " --bound_low " + str(self.bound_low) + " --bound_high " + str(self.bound_high)
            
        if self.model_type == "fixed" :
            self.options+= " --bc_err_freq " + str(self.bc_err_freq)
        
        for idx, read1 in enumerate(self.read1_files):
            id=self.samples_id[idx]
            
            format="gzfastq" if read1.endswith(".gz") else "fastq"
            self.add_shell_execution(self.get_exec_path("ustacks") + " -t " + format + " -f $1 -o " + self.output_directory + " -i " + str(id) 
                                    + self.options + " 2> $2", cmd_format='{EXE} {IN} {OUT}',inputs = [read1], outputs = [self.stderrs[idx], self.tags[idx], self.alleles[idx], self.snps[idx]], map=False )