#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os, subprocess
import re
from subprocess import Popen, PIPE

from ng6.analysis import Analysis

from jflow.component import Component

from ng6.utils import Utils


list_enz=['apeKI', 'bamHI', 'claI', 'dpnII', 'eaeI', 'ecoRI','ecoT22I', 'hindIII', 'mluCI', 'mseI', 'mspI', 'ndeI','nheI', 'nlaIII',\
           'notI', 'nsiI', 'pstI', 'sau3AI','sbfI', 'sexAI', 'sgrAI', 'sphI', 'taqI', 'xbaI']

def recover_mate_discards (read1,read2, output_file):
     import jflow.seqio as seqio
     # enregistrement des ID de sequences 1
     id_R1=[]
     reader = seqio.SequenceReader(read1,fileformat="fastq")           
     for id, desc, seq, qual in reader :
         id_R1.append(id)
     
     # disctionnaire de sequences 2 correspondant aux ID de lecures 1
     dic_R2={}        
     reader = seqio.SequenceReader(read2,fileformat="fastq")
     for id, desc, seq, qual in reader :
         if id in id_R1:
             dic_R2[id]=desc+"\n"+seq+"\n+\n"+qual+"\n"

     # ecriture du fichier fastq de lectures 2 equivalent aux lectures 1
     handle=open(output_file,"w")    
     string=""
     i=0        
     for name in id_R1:
         string=string+"@"+dic_R2[name]
         i=i+4
         if i==4000:
             handle.write(string)
             string=""
             i=0
     if string !="":
         handle.write(string)
         string=""
         i=0
     handle.close()
 
def recover_mate_ok (read1,read2,output_file):
     import jflow.seqio as seqio
     # enregistrement des ID de sequences 1
     id_R1=[]
     reader = seqio.SequenceReader(read1,fileformat="fastq")           
     for id, desc, seq, qual in reader :
         id_R1.append(id[:-2])
     
     # disctionnaire de sequences 2 correspondant aux ID des lcures 1
     dic_R2={}
     reader = seqio.SequenceReader(read2,fileformat="fastq")
     for id, desc, seq, qual in reader :
         convert_id="_".join(id.split(":")[3:7])
         if convert_id in id_R1:
             dic_R2[convert_id]=convert_id+"_2\n"+seq+"\n+\n"+qual+"\n"  
     
     # ecriture du fichier fastq de lectures 2 equivalent aux lectures 1
     handle=open(output_file,"w")    
     string=""
     i=0        
     for name in id_R1:
         string=string+"@"+dic_R2[name]
         i=i+4
         if i==4000:
             handle.write(string)
             string=""
             i=0
     if string !="":
         handle.write(string)
         string=""
         i=0
     handle.close()
     

def wrap_clonefilter_PE(exec_path, out_dir, inputR1_fastq, inputR2_fastq, outputR1_fastq, outputR2_fastq, hist_file, stderr_file):
    from subprocess import Popen, PIPE
    import re,os
    
    # execute clone filter
    cmd = [exec_path, "-1",inputR1_fastq , "-2", inputR2_fastq, "-o", out_dir, "-i", "gzfastq", "-y", "fastq", "-D"]
    p = Popen(cmd, stdout=PIPE, stderr=PIPE)
    stdout, stderr =p.communicate()

    stderh = open(stderr_file, "wb")
    stderh.write(stderr)	
    stderh.close()
    hist = open(hist_file, "wb")
    hist.write(stdout)
    hist.close()
    
    # rename and compress
    for file in os.listdir(out_dir):
        match = re.search('.Fil.fq',file)
        if match :
            if file == ".fq_1":
                cmd = ["gzip", file, ";", "mv", file, outputR1_fastq]
                p = Popen(cmd, stderr=PIPE)		
            else : 
                cmd = ["gzip", file, ";", "mv", file, outputR2_fastq]
                p = Popen(cmd, stderr=PIPE)
        else :
            if file in inputR1_fastq:
                cmd = ["cp", inputR1_fastq, outputR1_fastq]
                p = Popen(cmd, stderr=PIPE)
            if file in inputR2_fastq:
                cmd = ["cp", inputR2_fastq, outputR2_fastq]
                p = Popen(cmd, stderr=PIPE)

class ProcessRadtag (Analysis):
          
    def define_parameters(self, read1_files, read2_files = None, dereplicate=False, enzyme_name=None, enzyme_name2=None, window_size=0.15, limit_score=10, quality_encode = 'phred33', max_length=None, uncall_remove=True, discard_low_qual=True, rescue_radtag=False, \
                           keep_discard_read=True,  archive_name=False):


        """
          @param read1_files (-1): paths to reads 1
          @param read2_files (-2): paths to reads 2
          @param dereplicate : si dereplicate alors lancer les 
          @param uncall_remove (-c): clean data, remove any read with an uncalled base.
          @param discard_low_qual (-q): discard reads with low quality scores.
          @param rescue_radtag (-r) : rescue barcodes and RAD-Tags.
          @param max_length (-t) : truncate final read length to this value. (default none)
          @param quality_encode (-E) : specify how quality scores are encoded, 'phred33' (Illumina 1.8+, Sanger, default) or 'phred64' (Illumina 1.3 - 1.5).
          @param keep_discard_read (-D): capture discarded reads to a file.
          @param window_size (-w) : set the size of the sliding window as a fraction of the read length, between 0 and 1 (default 0.15).
          @param limit_score (-s) : set the score limit. If the average score within the sliding window drops below this value, the read is discarded (default 10).
          @param enzyme_name (-e) : -e [enz], --renz_1 [enz]: provide the restriction enzyme used (cut site occurs on single-end read)
          @param enzyme_name2 (--renz_2) : --renz_2 [enz] : provide the second restriction enzyme used
          @param archive_name : name for the output archive
        """
        # Files
        self.add_input_file_list( "read1_files", "paths to reads 1", default=read1_files, required=True, file_format = 'fastq')
        self.add_input_file_list( "read2_files", "paths to reads 2", default=read2_files, required=True, file_format = 'fastq')
        if self.read2_files : 
            assert len(read1_files) == len(read2_files) , "[ERROR] : the number of files is not correct! (the number of files in read1_files and in read2_files must be the same)"
        
        #Parameters
        self.add_parameter("uncall_remove", "clean data, remove any read with an uncalled base.", default=uncall_remove, type=bool)
        self.add_parameter("dereplicate", "if make clone filter analysis", default=dereplicate, type=bool)
        self.add_parameter("discard_low_qual", "discard reads with low quality scores.", default=discard_low_qual, type=bool)
        self.add_parameter("rescue_radtag", "rescue barcodes and RAD-Tags.", default=rescue_radtag, type=bool)
        self.add_parameter("max_length", "truncate final read length to this value. (default none)", default=max_length, type=int)
        self.add_parameter("quality_encode", "specify how quality scores are encoded, 'phred33' (Illumina 1.8+, Sanger, default) or 'phred64' (Illumina 1.3 - 1.5).", default=quality_encode, type=str)
        self.add_parameter("keep_discard_read", "capture discarded reads to a file.", default=keep_discard_read, type=bool)
        self.add_parameter("window_size", "set the size of the sliding window as a fraction of the read length, between 0 and 1 (default 0.15).", default=window_size, type=float)
        self.add_parameter("limit_score", "set the score limit. If the average score within the sliding window drops below this value, the read is discarded (default 10).", default=limit_score, type=int)
        self.add_parameter("enzyme_name", "provide the restriction enzyme used (cut site occurs on single-end read)", default=enzyme_name, type=str)
        self.add_parameter("enzyme_name2", "provide the second restriction enzyme used", default=enzyme_name2, type=str)
        self.archive_name = archive_name
        
        self.prefixes = self.get_outputs('{basename_woext}', [read1_files, read2_files])
        #print self.prefixes
        self.add_output_file_list("out_process_read1_files", "process read1 files", pattern='{basename}.gz', items=self.read1_files)
        self.add_output_file_list("out_process_read2_files", "process read2 files", pattern='{basename}.gz', items=self.read2_files)
        self.add_output_file_list("discard_process_read1_files", "process discard read1 files", pattern='{basename}.discard.gz', items=self.read1_files)
        self.add_output_file_list("discard_process_read2_files", "process discard read2 files", pattern='{basename}.discard.gz', items=self.read2_files)        
        self.add_output_file_list("stderr_process_files", "stderr files", pattern='{basename}.stderr', items=self.prefixes)
        
        if(self.dereplicate == "True"):
            print(self.dereplicate)
            print("DeReplicate")
            self.add_output_file_list("out_clone_read1_files", "uniq read1 files", pattern='{basename_woext}_cloneFil.fq.gz', items=self.read1_files)
            self.add_output_file_list("stderr_clone_files", "clone_filter_log files", pattern='{basename_woext}.stderrs', items=self.read1_files)
            self.add_output_file_list("hist_clone_files", "clone_filter_histogramme files", pattern='{basename_woext}.hist', items=self.read1_files)

    def __parse_process_file (self, processradtag_file):
        """
        Parse the process radtag log file
          @param processradtag_file : the process radtag log file
          @return             : {"totalsequence" : "nb seq", ...}
        """
        stats_processradtag = {}
        for line in open(processradtag_file, 'r').readlines():
            parts = line.strip().split(" ")
            if re.search('total sequences;',line) :
                stats_processradtag["inputseq"] = parts[0]
            elif re.search('ambiguous barcode drops;',line) :
                stats_processradtag["ambigbarc"] = parts[0]
            elif re.search('low quality read drops;',line) :
                stats_processradtag["lowqual"] = parts[0]
            elif re.search('ambiguous RAD-Tag drops;',line) :
                stats_processradtag["ambigRAD"] = parts[0] 
            elif re.search('retained reads.',line) :
                stats_processradtag["outputseq"] = parts[0]
            elif re.search('Processing file',line) :
                ad = line.strip().split("[")
                stats_processradtag["sample"] = ad[1].replace("_1.fq]", "")
        return stats_processradtag
        
    def __parse_clone_file (self, clonefilter_file):
        """
        Parse the clone file log file
            @param clonefilter_file : the clone filte log file
            @return                 : {"totalsequence" : "nb seq", ...}
        """
        
        stats_clonefilter = {}
        for line in open(clonefilter_file, 'r').readlines():
            parts = line.strip().split(" ")
            if re.search('pairs of reads input.', line) :
                stats_clonefilter["inputseq"] = parts[0]
                stats_clonefilter["outputseq"] = parts[5]
                stats_clonefilter["discardseq"] = parts[11]
            elif re.search('_1.fq.gz and', line) :
                ad = parts[0].strip().split("/")
                stats_clonefilter["sample"] = ad[len(ad)-1].replace("_1.fq.gz", "")
                
        return stats_clonefilter
            
    def define_analysis(self):
        self.name = "Process radtag"
        self.description = "Correct fastq radtag"
        self.software = "process_radtags"
        self.options = ""
        self.parameters = ""
        if self.enzyme_name:
            self.options += " -e " + str(self.enzyme_name)
            self.parameters += " -e " + str(self.enzyme_name)
        if self.enzyme_name2:
            self.options += " --renz_2 " + str(self.enzyme_name2)
            self.parameters += " --renz_2 " + str(self.enzyme_name2)
        if self.limit_score:
            self.options += " -s " + str(self.limit_score)
            self.parameters += " -s " + str(self.limit_score)
        if self.quality_encode:
            self.options += " -E " + str(self.quality_encode)
            self.parameters += " -E " + str(self.quality_encode)
        if self.rescue_radtag:
            print("option r", self.rescue_radtag)
            self.options += " -r "
            self.parameters += " -r "
        if self.discard_low_qual:
            self.options += " -q "
            self.parameters += " -q "
        if self.keep_discard_read:
            self.options += " -D "
            self.parameters += " -D "
        if self.max_length:
            self.options += " -t " + str(self.max_length)
            self.parameters += " -t " + str(self.max_length)
        if self.window_size:
            self.options += " -w " + str(self.window_size)
            self.parameters += " -w " + str(self.window_size)
        if self.uncall_remove:
            self.options += " -c "
            self.parameters += " -c "
        if self.dereplicate== "TRUE":
            self.options += " --dereplicate"


    def post_process(self):
        print("post process analysis process radtags")
        
        ## PROCESS_RADTAG
        for id, filepath in enumerate(self.stderr_process_files) :
            
            print(filepath)
            processradtag_info = self.__parse_process_file(filepath)
            sample = str(processradtag_info["sample"])
            self._add_result_element(sample, "processinput", str(processradtag_info["inputseq"]), "result")
            self._add_result_element(sample, "processambibarc", str(processradtag_info["ambigbarc"]), "result")
            self._add_result_element(sample, "processlowqual", str(processradtag_info["lowqual"]), "result")
            self._add_result_element(sample, "processambiRAD", str(processradtag_info["ambigRAD"]), "result")
            self._add_result_element(sample, "processoutput", str(processradtag_info["outputseq"]), "result")

        ## CLONE FILTER
        if self.dereplicate :
            samples = {}
            for id, filepath in enumerate(self.stderr_clone_files) : 
                clonefilter_info = self.__parse_clone_file(filepath)
                sample = str(clonefilter_info["sample"])
                self._add_result_element(sample, "cloneinput", str(clonefilter_info["inputseq"]), "result")
                self._add_result_element(sample, "cloneoutput", str(clonefilter_info["outputseq"]), "result")
                self._add_result_element(sample, "clonediscard", str(clonefilter_info["discardseq"]), "result")
                    
            for id, filepath in enumerate(self.hist_clone_files) :
                sample_name = os.path.basename(filepath).split(".hist")[0].replace("_1","")
                x = []
                y = []
                numClone = {}
                for line in open(filepath, 'r').readlines():
                    if not re.search('Num Clones', line):
                        parts = line.strip().split("\t")
                        numClone[parts[0]] = parts[1]
                for val in list(numClone.keys()):
                    x.append(val)
                x = sorted(x)
                for i in x :
                    y.append(numClone[i])
                
                samples[sample_name] = {
                    "num_clone"   : str(",".join([str(v) for v in x])),
                    "count_clone" : str(",".join([str(v) for v in y]))
                }
                
            for sample in samples:       
                self._add_result_element(sample, "num_clone", samples[sample]["num_clone"],"result")
                self._add_result_element(sample, "count_clone", samples[sample]["count_clone"],"result")

    def get_version(self):        
        cmd = [self.get_exec_path("process_radtags"), "--version"]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stderr.split()[1]
   
    def process(self):

        # Tmp output
        # Creates list for temporary uncompressed files
        tmp_output_read_1 = self.get_outputs('{basename}',self.read1_files)
        tmp_discard_read_1 = self.get_outputs('{basename}.discards',self.read1_files)
        if(self.read2_files):
            tmp_output_read_2 = self.get_outputs('{basename}',self.read2_files)
            tmp_discard_read_2 = self.get_outputs('{basename}.discards',self.read2_files)

        # Process radtags read1 files
        for i in range(0, len(self.prefixes)):        
            print(self.parameters)
            self.add_shell_execution(self.get_exec_path("process_radtags") + " -f $1 " + self.parameters + " -o " + self.output_directory + " 2> $2 ", cmd_format = "{EXE} {IN} {OUT}", inputs = [self.read1_files[i]], outputs = [self.stderr_process_files[i], tmp_output_read_1[i], tmp_discard_read_1[i]], map=False)
            
        #print "apres boucle"
        if(self.read2_files):
            print("Recover")
            # Recover_mate and recover_discard
            self.add_python_execution(recover_mate_ok, cmd_format="{EXE} {IN} {OUT}", inputs = [tmp_output_read_1, self.read2_files], outputs=[tmp_output_read_2], map=True)
            
            self.add_python_execution(recover_mate_discards, cmd_format="{EXE} {IN} {OUT}", inputs = [tmp_discard_read_1, self.read2_files], outputs=[tmp_discard_read_2], map=True)

        # Compress 
        self.add_shell_execution(self.get_exec_path("gzip") + " -c $1  > $2 ", cmd_format="{EXE} {IN} {OUT}",inputs = tmp_output_read_1, outputs = self.out_process_read1_files, map=True )
        self.add_shell_execution(self.get_exec_path("gzip") + " -c $1  > $2 ", cmd_format="{EXE} {IN} {OUT}", inputs = tmp_discard_read_1, outputs = self.discard_process_read1_files, map=True )

        if (self.read2_files):
            self.add_shell_execution(self.get_exec_path("gzip") + " -c $1  > $2 ", cmd_format="{EXE} {IN} {OUT}",inputs = tmp_output_read_2, outputs = self.out_process_read2_files, map=True )
            self.add_shell_execution(self.get_exec_path("gzip") + " -c $1  > $2 ", cmd_format="{EXE} {IN} {OUT}",inputs = tmp_discard_read_2, outputs = self.discard_process_read2_files, map=True )
            
            
        # clone_filter
        if(self.dereplicate):
            #clone filter
            print(self.enzyme_name2,self.read2_files)
            
            if(self.read2_files and not self.enzyme_name2):
                self.add_output_file_list("out_clone_read2_files", "uniq read2 files", pattern='{basename_woext}_cloneFil.fq.gz', items=self.read2_files)
                for idx, read1 in enumerate(self.read1_files):
                    self.add_python_execution(wrap_clonefilter_PE, cmd_format="{EXE} {ARG} {IN} {OUT}",arguments=[self.get_exec_path("clone_filter"), self.output_directory], inputs=[self.out_process_read1_files[idx], self.out_process_read2_files[idx]], outputs=[self.out_clone_read1_files[idx], self.out_clone_read2_files[idx], self.hist_clone_files[idx], self.stderr_clone_files[idx]],map=False)
                    
            else :
                raise Exception("Error : no clone filter for single read, or double RAD")
