#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from ng6.ng6workflow import DownloadWorkflow
from ng6.config_reader import NG6ConfigReader


class DownloadSymlink (DownloadWorkflow):

    def get_description(self):
        return """This workflow allows you to create symbolic links of the selected data for the provided user in the destination directory on the server.
        Please be carefull that the provided user must have write permission on the specified directory on the server."""

    def define_parameters(self, function="process"):
        config = NG6ConfigReader()
        self.add_parameter("server",
                   "Server where to create symbolic links",
                   choices = ["genologin.toulouse.inra.fr","genobioinfo.toulouse.inrae.fr"],
                   default="genologin")
        self.add_parameter('username', 'A valid user username on the server', type = "localuser" ,required = True)
        self.add_parameter('password', 'The connection password for the username on the server', type="password", required = True)
        self.add_parameter('user_directory', 'The directory where the links will be created. This directory must exists on the server and the provided user must have write permission on this directory', default = config.get_user_base_directory(),
                           required = True)

    def process(self):
        self.add_component('CreateSymlink', [self.username, self.password, self.user_directory, self.server, self.data_id, self.run_id, self.analysis_id, self.admin_login])