#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import pickle
import logging

from jflow.component import Component
from ng6.utils import Utils

from weaver.function import PythonFunction

def create_symbolik_link (ids_dump_path, output_list, user_script, username, password,server, output_folder, ng6_username = None):
    import os
    import pickle
    import subprocess
    import logging
    import shlex

    from ng6.config_reader import NG6ConfigReader
    from ng6.utils import Utils, SSH
    from jflow.parameter import PasswordParameter
    logging.getLogger("create_symbolik_link").debug("Entering create_symbolik_link")
    config = NG6ConfigReader()
    data_folder = config.get_save_directory()
    password = PasswordParameter.decrypt(password)
    if ng6_username in ['', 'None', 'none', 'null', None] :
        ng6_username = None

    try:

        fh = open(ids_dump_path, "rb")
        prefixed_ids = pickle.load(fh)
        fh.close()
        prefixed_ids = ";".join(prefixed_ids)
        src_directories, dest_directories = Utils.get_directories_structure_and_content(ng6_username, data_folder, output_folder, prefixed_ids)
        destinations = []
        with open(user_script, "w") as us:

            with open(output_list, "w") as ol :
                for i, source_dir in enumerate(src_directories):
                    dest_dir = dest_directories[i]
                    if not os.path.isdir(dest_dir) :
                        us.write("mkdir -p -m 755 %s ; \n"%dest_dir)

                    for filename in os.listdir(source_dir):
                        source_file_path = os.path.join(source_dir, filename)
                        dest_file_path = os.path.join(dest_dir, filename)
                        destinations.append(dest_file_path)
                        if os.path.isfile(source_file_path) and filename not in [ "analyse.xml", "index.html", "run.cfg"] :
                            if not os.path.exists(dest_file_path):
                                us.write("ln -s %s %s ;\n"%(source_file_path,dest_file_path))
                            ol.write(dest_file_path+"\n")

        # execution of the script as the user
        server_params = config.get_server_parameters()
        #s = SSH(username, password, server_params[0])
        logging.getLogger("create_symbolik_link").debug("Before SSH(username, password, server) on server" + server)
        logging.getLogger("create_symbolik_link").debug("user_script = "+user_script)
        s = SSH(username, password, server)
        logging.getLogger("create_symbolik_link").debug("Before s.cmd(")
        logging.getLogger("create_symbolik_link").debug("user_script = "+user_script)
        #if server == "genobioinfo.toulouse.inrae.fr" :
        #    user_script = user_script.replace("/home/ng6/work/", "/oldwork/ng6/")
        #    user_script = user_script.replace("/work/ng6/", "/oldwork/ng6/")
        #    logging.getLogger("create_symbolik_link").debug("user_script = "+user_script)
        logging.getLogger("create_symbolik_link").debug("Copy " + user_script + " to " + server+output_folder+'/')
        cmd = "curl --insecure --user "+username+":"+password.replace("'","\\'")+" -T "+user_script+" sftp://"+server+output_folder+'/'
        args = shlex.split(cmd)
        process = subprocess.Popen(args, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        logging.getLogger("create_symbolik_link").debug("curl stdout = "+ str(stdout))
        logging.getLogger("create_symbolik_link").debug("curl stderr = "+ str(stderr))
        logging.getLogger("create_symbolik_link").debug("Execute bash "+os.path.join(output_folder,os.path.basename(user_script)) +" on " + server + " on behalf of " + username)
        s.cmd('bash ' + os.path.join(output_folder,os.path.basename(user_script)))
        logging.getLogger("create_symbolik_link").debug("Delete "+os.path.join(output_folder,os.path.basename(user_script)) +" on " + server + " on behalf of " + username)
        s.cmd('rm ' + os.path.join(output_folder,os.path.basename(user_script)))
        #s.cmd('bash %s'%user_script)
        logging.getLogger("create_symbolik_link").debug("After s.cmd(")

    except Exception as e:
        raise Exception('Unable to create link : %s'%e)


class CreateSymlink (Component):

    def define_parameters(self, username, password, user_directory, server, data_ids = [], run_ids = [], analysis_ids = [], login = None):
        self.add_parameter('login', 'The login of the user in ng6', type = 'ng6userlogin', default = login)
        self.add_parameter('username', 'Valid username on the server', required = True, default = username)
        self.add_parameter('server', 'Server where to create smlinks', required = True, default = server)
        self.add_parameter('password', 'A password for the usernmae on the server', required = True, default = password, type = "password")
        self.add_parameter('user_directory', 'Destination', required = False, default = user_directory)
        self.add_parameter_list('data_ids', 'Ids or a run from which rawdata will be retrieved', default = data_ids)
        self.add_parameter_list('run_ids', 'Ids of run from which all data will be retrieved', default = run_ids)
        self.add_parameter_list('analysis_ids', 'Ids of analysis to retrieve', default = analysis_ids)
        self.add_output_file( 'user_script', 'The bash script that will be executed by the user', filename = "userscript.sh")
        self.add_output_file( 'output_list', 'output_list', filename = "output_list.txt")

    def process(self):
        run_ids = [ "run_%s"%i for i in self.run_ids ]
        data_ids = [ "data_%s"%i for i in self.data_ids ]
        analysis_ids = [ "analyse_%s"%i for i in self.analysis_ids ]

        ids_dump_path = self.get_temporary_file(".dump")
        fh = open(ids_dump_path, "wb")
        pickle.dump( run_ids + data_ids + analysis_ids, fh)
        fh.close()

        createlinks = PythonFunction(create_symbolik_link, cmd_format = "{EXE} {IN} {OUT} {ARG} ")
        createlinks(arguments = [ self.username, self.password, self.server, self.user_directory, self.login ],
                    inputs = ids_dump_path , outputs = [self.output_list, self.user_script], local = True)