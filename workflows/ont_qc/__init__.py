#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import logging
import os
from glob import glob
from subprocess import Popen, PIPE
from ng6.ng6workflow import NG6Workflow
from ng6.utils import Utils
from _codecs import encode

class OntQualityCheck (NG6Workflow):
    
    def __init__(self, args={}, id=None, function= "process"):
        NG6Workflow.__init__(self, args, id, function)
        self.log_files = []
        
    def get_name(self):
        return 'ont_qc'
    
    def get_description(self):
        return "ONT data loading and quality check"
    
    def define_parameters(self, function="process"):
        logging.getLogger("jflow").debug("Begin OntQualityCheck.define_parameters! ont_qc")
        self.add_parameter("compression", "How should the data be compressed once archived", choices= [ "none", "gz", "bz2"], default = "gz")
        self.add_parameter("trimming", "use trimming with porechop or not",choices= [ "yes", "no"], default = "no")
        self.add_input_file("summary_file", "Input summary basecalling file", default=None)
        self.add_parameter("barcoded", "If barcoded run : yes", default = "no")
        self.add_parameter("fast5dir", "path of the fast5 directory", default = None)
        
    def process(self):
        logging.getLogger("jflow").debug("Begin OntQualityCheck.process! test ont_qc")
        sample_names = []
        infiles = []
        for sample in self.samples :
            sample_names.append( sample.name )
            infiles.append(sample.reads1[0])
        # add raw
        logging.getLogger("jflow").debug("OntQualityCheck.process! get_all_reads : "+",".join(self.get_all_reads()))
        logging.getLogger("jflow").debug("OntQualityCheck.process! sample_name : "+str(sample_names))
        logging.getLogger("jflow").debug("OntQualityCheck.process! summary_file : "+str(self.summary_file))
        
        ### check for log file self.fastq_files
        # get the dirname of reads folder, logs are in this dirname/jflow/
        logpath = os.path.dirname( self.get_all_reads()[0] ) + "/jflow/"
        logging.getLogger("jflow").debug("OntQualityCheck._process.logfile logpath = " + logpath)
        
        # find .log files
        for file in glob(logpath+"/*.log"):
            self.log_files.append(file)
        for file in glob(logpath+"/*.pdf"):
            self.log_files.append(file)
        for file in glob(logpath+"/*.html"):
            self.log_files.append(file)
        logging.getLogger("jflow").debug("OntQualityCheck._process.logfile self.log_files = " + ",".join(self.log_files))
        
        # add logs
        if len(self.log_files) > 0 :
            add_log = self.add_component("BasicAnalysis", [self.log_files,"Log Files","Log files generated during primary analysis","-","-","-","none", "","log.gz"])
        
        addrawfiles = self.add_component("AddRawFiles", [self.runobj, self.get_all_reads(), self.compression])
        ontstat = self.add_component("Run_stats", [self.summary_file, sample_names[0]])
        if (self.barcoded == "yes"):
            demultiplexont = self.add_component("Demultiplex_ONT", [self.get_all_reads()])
        if self.trimming == "yes":
            trim_porechop = self.add_component("Trim_porechop", [self.get_all_reads() , "discard_middle"])
        if self.fast5dir != None:
            fast5archive = self.add_component("Fast5archive", [self.fast5dir, "fast5archive.tar"])
        
        logging.getLogger("jflow").debug("OntQualityCheck._process.logfile exiting")
        

