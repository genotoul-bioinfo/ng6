#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import re, os
from subprocess import Popen, PIPE
import logging
import time
from ng6.analysis import Analysis
from ng6.utils import Utils
from jflow.utils import get_argument_pattern

class Demultiplex_ONT (Analysis):
    
    """
        This module demultiplexes the total fastq of a barcoded ONT run and produces stats
    """
    def __init__(self, args={}, id=None, function= "process"):
        Analysis.__init__(self, args, id, function)
        
    def define_parameters(self, fastq_files, archivename="DemultiplexONT_archive.tar"):
        self.add_input_file_list( "fastq_files", "fastq_files", default=fastq_files, required=True, file_format = 'fastq')
        #self.add_parameter("barcode_file", "Name of the barcode file", default=barcode_file, required=False , file_format = 'str')
        self.add_parameter("archive_name", "Name of the archive", default=archivename, type='str')
        #self.add_parameter( "run_name", "The name of the run (from total fastq file)", pattern='{basename_woext}', items=self.fastq_files, file_format = "fastq")
        self.add_output_file("output_file", "output_file", filename=os.path.join(self.output_directory, "DemultiplexONT.output") )
        
    def define_analysis(self):
        self.name = "DemultiplexONT"
        self.description = "Produces stats about demultiplex files from ONT Barcoder"
        self.software = "Seqkit"

    def __parse_stat_file (self, stat_file):
        logging.getLogger("demultiplexont.process").debug("DemultiplexONT.__parse_stat_file begins! file = "+str(stat_file))
        """
        Parse the stat file
          @param stat_file : the stdout porechop
          @return             : {"read_trim_start" : read_trim_start, ...}
        """
        #File parsing : recording into a list
        list_stats= []
        with open(stat_file, "r") as f_stat :
            for line in f_stat.readlines():
                list_stats.append(line.split())
                
        # Registering file's header into a list
        header = list_stats.pop(0)
        # Creating a dictionnary on this model
        # dico_stats[SampleName][parameterName] = Value
        dico_stats = {}
        for sample_number in range(len(list_stats)):
            logging.getLogger("demultiplexont.process").debug("DemultiplexONT.__parse_stat_file exe! Sample number : "+str(sample_number) )
            dico_stats[list_stats[sample_number][0]] = {}
            for parameter_idx in range(1, len(header)):
                dico_stats[list_stats[sample_number][0]][header[parameter_idx]] = list_stats[sample_number][parameter_idx]
        logging.getLogger("demultiplexont.process").debug("DemultiplexONT.__parse_stat_file ends! file = "+stat_file)
        return dico_stats
        
        
    def post_process(self):
        logging.getLogger("demultiplexont.process").debug("DemultiplexONT.post_process begins!")
        # Create dictionary : key = file name or prefix, value = files path
        stats_dico = self.__parse_stat_file(os.path.join(self.output_directory, "DemultiplexONT.output"))
        
        # add header of stats
        stats_names = ["format",'num_seqs','sum_len','avg_len','min_len','max_len',"N50"]
        #'Q2', , , 'N50', , , 'format', 'Q1', 'Q3', 'type', 'sum_gap', 'Q30(%)', , 'Q20(%)' 
        self._add_result_element("metrics", "headers", ','.join(stats_names),"stats_names")
        
        # Add stats metrics
        for fastq in stats_dico:
            if re.search("undetermined.", fastq):
                fastq_name = os.path.basename(fastq).split('.')[0].split('_')[-1]
            else:
                fastq_name = "_".join(os.path.basename(fastq).split('.')[0].split('_')[1:3])
                #fastq_name = "_".join(os.path.basename(fastq).split('.')[0].split('_')[-2:])
            logging.getLogger("demultiplexont.process").debug("DemultiplexONT.fast_name = "+fastq_name)
            
            for stat in stats_dico[fastq]:
                self._add_result_element("stats_metrics", stat, stats_dico[fastq][stat],fastq_name)

        logging.getLogger("demultiplexont.process").debug("DemultiplexONT.post_process ends!")
    
    def get_version(self):
        shell_script = self.get_exec_path("seqkit") + " version | head -n1"
        logging.getLogger("demultiplexont.process").debug("DemultiplexONT.get_version ! shell_script = " + str(shell_script))
        cmd = ["sh","-c",shell_script]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        logging.getLogger("demultiplexont.process").debug("DemultiplexONT.get_version result: " + str(stderr))
        return stdout
                     
    def process(self):
        logging.getLogger("demultiplexont.process").debug("DemultiplexONT.process begins!")
        
        iter = 1
        str_input = ""
        str_output = ""
        for fastq in self.fastq_files:
            str_input += ' ${' + str(iter) + '}'
            iter += 1
        str_output = ' ${'+ str(iter) + '}'
            
        # Create cmd
        self.add_shell_execution(self.get_exec_path("seqkit") +" stats --all " + str_input + ">" + str_output,
            cmd_format='{EXE} {IN} {OUT}',
            map=False,
            inputs = [self.fastq_files],
            outputs = os.path.join(self.output_directory, "DemultiplexONT.output"))

        #archive = self.output_directory + '/' + self.archive_name + '.tar.gz'
        #self.add_shell_execution('tar -czf $1 ' + self.output_directory + '/' + '*_trim.fastq ', cmd_format='{EXE} {OUT}', map=False, outputs = archive)

        logging.getLogger("demultiplexont.process").debug("DemultiplexONT.process ends!")

