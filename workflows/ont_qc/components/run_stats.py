#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import os
import json
import logging
import re

from ng6.analysis import Analysis

from subprocess import Popen, PIPE
from jflow.utils import get_argument_pattern
from ng6.utils import Utils

class Run_stats (Analysis):
    """
        This module make some statistic from ONT run with graphs
    """
    
    def define_parameters(self, sequencing_summary_file, sample_name="plot", archive_name="RunStats_archive.tar.gz"):
        logging.getLogger("jflow").debug("Begin Run_stats parameters")
        self.add_input_file( "sequencing_summary_file", "Input sequencing summary file from Basecaller", default=sequencing_summary_file, required=True)
        self.add_parameter("sample_name", "Sample name for prefix", default=sample_name, type='str')
        self.add_parameter("archive_name", "Archive name", default=archive_name)
        
        self.add_output_file_list("stderr", "stderr ouput file",pattern='Run_stats.stderr', items = self.sequencing_summary_file)
        
    def get_version(self):
        #cmd = [self.get_exec_path("Rscript")," /save/sbsuser/analyses_scripts/mmanno/graph_albacoresummary.R"]
        #p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        #stdout, stderr = p.communicate()
        #return stdout.split()[1]
        return "v2.0"
        
    def define_analysis(self):
        self.name = "RUNStats"
        self.description = "Statistics on reads and their qualities with R."
        self.software = "Rscript"
        self.options = "-"

    def __parse_stat_file (self, stat_file):
        """
        Parse the stat file
          @param stat_file : the runstatsR stats file
          @return             : {"" : "", ...}
        """
        stats = {}
        logging.getLogger("jflow").debug("Begin post_process  _parse_stat_file!")
        for line in open(stat_file, 'r').readlines():
            parts = line.strip().split("\t")
            
            if parts[0] == "nb_reads": stats["nb_reads"] = parts[1]
            if parts[0] == "total_bases": stats["total_bases"] = parts[1]
            if parts[0] == "mean_read_length": stats["mean_read_length"] = parts[1]
            if parts[0] == "N50_read_length": stats["N50_read_length"] = parts[1]
            if parts[0] == "N90_read_length": stats["N90_read_length"] = parts[1]
            if parts[0] == "N10_read_length": stats["N10_read_length"] = parts[1]
            if parts[0] == "mean_read_quality": stats["mean_read_quality"] = parts[1]
            if parts[0] == "mean_yield_per_sec": stats["mean_yield_per_sec"] = parts[1]
            if parts[0] == "longread1": stats["longread1"] = parts[1]
            if parts[0] == "longread2": stats["longread2"] = parts[1]
            if parts[0] == "longread3": stats["longread3"] = parts[1]
            if parts[0] == "longread4": stats["longread4"] = parts[1]
            if parts[0] == "longread5": stats["longread5"] = parts[1]

            if parts[0] == "nb_reads_Q7": stats["nb_reads_Q7"] = parts[1]
            if parts[0] == "total_bases_Q7": stats["total_bases_Q7"] = parts[1]
            if parts[0] == "mean_read_length_Q7": stats["mean_read_length_Q7"] = parts[1]
            if parts[0] == "N50_read_length_Q7": stats["N50_read_length_Q7"] = parts[1]
            if parts[0] == "N90_read_length_Q7": stats["N90_read_length_Q7"] = parts[1]
            if parts[0] == "N10_read_length_Q7": stats["N10_read_length_Q7"] = parts[1]
            if parts[0] == "mean_read_quality_Q7": stats["mean_read_quality_Q7"] = parts[1]
            if parts[0] == "mean_yield_per_sec_Q7": stats["mean_yield_per_sec_Q7"] = parts[1]
            if parts[0] == "longread1_Q7": stats["longread1_Q7"] = parts[1]
            if parts[0] == "longread2_Q7": stats["longread2_Q7"] = parts[1]
            if parts[0] == "longread3_Q7": stats["longread3_Q7"] = parts[1]
            if parts[0] == "longread4_Q7": stats["longread4_Q7"] = parts[1]
            if parts[0] == "longread5_Q7": stats["longread5_Q7"] = parts[1]
            
            if parts[0] == "nb_reads_Q9": stats["nb_reads_Q9"] = parts[1]
            if parts[0] == "total_bases_Q9": stats["total_bases_Q9"] = parts[1]
            if parts[0] == "mean_read_length_Q9": stats["mean_read_length_Q9"] = parts[1]
            if parts[0] == "N50_read_length_Q9": stats["N50_read_length_Q9"] = parts[1]
            if parts[0] == "N90_read_length_Q9": stats["N90_read_length_Q9"] = parts[1]
            if parts[0] == "N10_read_length_Q9": stats["N10_read_length_Q9"] = parts[1]
            if parts[0] == "mean_read_quality_Q9": stats["mean_read_quality_Q9"] = parts[1]
            if parts[0] == "mean_yield_per_sec_Q9": stats["mean_yield_per_sec_Q9"] = parts[1]
            if parts[0] == "longread1_Q9": stats["longread1_Q9"] = parts[1]
            if parts[0] == "longread2_Q9": stats["longread2_Q9"] = parts[1]
            if parts[0] == "longread3_Q9": stats["longread3_Q9"] = parts[1]
            if parts[0] == "longread4_Q9": stats["longread4_Q9"] = parts[1]
            if parts[0] == "longread5_Q9": stats["longread5_Q9"] = parts[1]
            
        #print(stats)
        return stats
        
    def post_process(self):
        logging.getLogger("jflow").debug("Begin Run_stats.post_process! "+self.output_directory)
        results_files = []
        metrics = []
        
        cmd = [self.get_exec_path("pwd")]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        print(stdout.split()[0])
        
        #sample = "ONT_sample"
        sample = self.sample_name
        
        self._add_result_element("metrics", "template_status", "2.0","analyse_info")
        self._add_result_element("metrics", "Sample_name",self.sample_name,"analyse_info")
        
        group = "Stats"
        self._add_result_element(sample, "sequencing_summary", self._save_file(self.sequencing_summary_file, sample + ".sequencing_summary.txt", True), group)
        
        #logging.getLogger("jflow").debug("Begin Nanoplot.post_process - sample "+file)
        # stat file
        statfile = os.path.join(self.output_directory,sample+"_stats.txt")
        for file in os.listdir(self.output_directory):
            full_file_path = os.path.join(self.output_directory, file)
            if file.endswith(".zip"):
                results_files.append(full_file_path)
        
        if os.path.isfile(statfile): 
            
            stat_info = self.__parse_stat_file(os.path.join(self.output_directory, sample+"_stats.txt"))
                    
            group = 'rawdata'
            metrics.append(group)
            metrics_names = ["nb_reads", 
                            "total_bases", 
                            "mean_read_length",
                            "N50_read_length",
                            "N90_read_length",
                            "N10_read_length",
                            "mean_read_quality",
                            "mean_yield_per_sec",
                            "longread1",
                            "longread2",
                            "longread3",
                            "longread4",
                            "longread5"]
            self._add_result_element("metrics", "headers", ','.join(metrics_names), group)
            for item in metrics_names:
                self._add_result_element(sample, item, str(stat_info[item]),group)
            
            group = 'Q7'
            metrics.append(group)
            metrics_names = ["nb_reads_Q7",
                            "total_bases_Q7",
                            "mean_read_length_Q7",
                            "N50_read_length_Q7",
                            "N90_read_length_Q7",
                            "N10_read_length_Q7",
                            "mean_read_quality_Q7",
                            "mean_yield_per_sec_Q7",
                            "longread1_Q7",
                            "longread2_Q7",
                            "longread3_Q7",
                            "longread4_Q7",
                            "longread5_Q7"]
            self._add_result_element("metrics", "headers", ','.join(metrics_names), group)
            for item in metrics_names:
                self._add_result_element(sample, item, str(stat_info[item]),group)
            
            group = 'Q9'
            metrics.append(group)
            metrics_names = ["nb_reads_Q9",
                            "total_bases_Q9",
                            "mean_read_length_Q9",
                            "N50_read_length_Q9",
                            "N90_read_length_Q9",
                            "N10_read_length_Q9",
                            "mean_read_quality_Q9",
                            "mean_yield_per_sec_Q9",
                            "longread1_Q9",
                            "longread2_Q9",
                            "longread3_Q9",
                            "longread4_Q9",
                            "longread5_Q9"]
            self._add_result_element("metrics", "headers", ','.join(metrics_names), group)
            for item in metrics_names:
                self._add_result_element(sample, item, str(stat_info[item]),group)
            
        group = 'plots'
        metrics.append(group)
        self._add_result_element("metrics", "headers", ','.join(["cumulyield","distriblength","distriblength_bybases","distribquality","seqrate","channelbases","channelreads"]), group)
        
        if os.path.isfile(os.path.join(self.output_directory, sample+"_cumulyield.png")):
            self._add_result_element(sample, "cumulyield", self._save_file(os.path.join(self.output_directory, sample+"_cumulyield.png"), sample+"_cumulyield.png"), group)
            results_files.append(os.path.join(self.output_directory, sample+"_cumulyield.png"))
        if os.path.isfile(os.path.join(self.output_directory, sample+"_distriblength.png")):
            self._add_result_element(sample, "distriblength", self._save_file(os.path.join(self.output_directory, sample+"_distriblength.png"), sample+"_distriblength.png"), group)
            results_files.append(os.path.join(self.output_directory, sample+"_distriblength.png"))
        if os.path.isfile(os.path.join(self.output_directory, sample+"_distriblength_bybases.png")):
            self._add_result_element(sample, "distriblength_bybases", self._save_file(os.path.join(self.output_directory, sample+"_distriblength_bybases.png"), sample+"_distriblength_bybases.png"), group)
            results_files.append(os.path.join(self.output_directory, sample+"_distriblength_bybases.png"))
        if os.path.isfile(os.path.join(self.output_directory, sample+"_distribquality.png")):
            self._add_result_element(sample, "distribquality", self._save_file(os.path.join(self.output_directory, sample+"_distribquality.png"), sample+"_distribquality.png"), group)
            results_files.append(os.path.join(self.output_directory, sample+"_distribquality.png"))
        if os.path.isfile(os.path.join(self.output_directory, sample+"_seqrate.png")):
            self._add_result_element(sample, "seqrate", self._save_file(os.path.join(self.output_directory, sample+"_seqrate.png"), sample+"_seqrate.png"), group)
            results_files.append(os.path.join(self.output_directory, sample+"_seqrate.png"))
        if os.path.isfile(os.path.join(self.output_directory, sample+"_channelbases.png")):
            self._add_result_element(sample, "channelbases", self._save_file(os.path.join(self.output_directory, sample+"_channelbases.png"), sample+"_channelbases.png"), group)
            results_files.append(os.path.join(self.output_directory, sample+"_channelbases.png"))
        if os.path.isfile(os.path.join(self.output_directory, sample+"_channelreads.png")):
            self._add_result_element(sample, "channelreads", self._save_file(os.path.join(self.output_directory, sample+"_channelreads.png"), sample+"_channelreads.png"), group)
            results_files.append(os.path.join(self.output_directory, sample+"_channelreads.png"))

        # Finaly create and add the archive to the analysis
        self._create_and_archive(results_files,self.archive_name)

    def process(self):
        logging.getLogger("jflow").debug("Begin Run_stats.process! ont_qc")

        self.add_shell_execution(self.get_exec_path("Rscript") + " " + self.get_exec_path("graphe_summary") +' '+ '$1' +' ' + self.output_directory + " "+self.sample_name+" 2> " +' $2', 
                                cmd_format='{EXE} {IN} {OUT}' ,
                                map=False, 
                                inputs = self.sequencing_summary_file,
                                outputs = self.stderr)

        logging.getLogger("jflow").debug("End Run_stats.process! ")
