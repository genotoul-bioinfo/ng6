#
# Copyright (C) 2015 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import logging
from jflow.component import Component
from workflows.types import ng6space,existingprojectid
from ng6.t3MySQLdb import t3MySQLdb

def migrate_project (project_id, new_space_id, output):
    import os
    from subprocess import call
    import logging

    from ng6.config_reader import NG6ConfigReader
    from ng6.analysis import Analysis
    from ng6.project import Project
    from ng6.run import Run
    
    def log_to_output_files (log_text, log_file):
        fh = open(log_file, "a")        
        fh.write(log_text)
        fh.close()
        logging.getLogger("move_project.py").debug(log_text)
    
    ng6conf = NG6ConfigReader()
    base_path = ng6conf.get_save_directory()
        
    project = Project.get_from_id(project_id)
    old_space_id = project.space_id        
       
    str_log="MIGRATE PROJECT: '" + str(project.name) + "' (" + str(project.id) + ") from " + old_space_id + " to " + str(new_space_id) + "\n"
    log_to_output_files(str_log, output)
       
    runs = project.get_runs()
    for run in runs :
        str_log="\t -  ##RUN '" + run.name + "' (" + str(run.id) + ") - " + run.directory + "\n"
        log_to_output_files(str_log, output)
        
        (ret_code, ret_command) = run.change_space(new_space_id) 
        str_log = "\t   RET CODE: " + str(ret_code) + " CMD : " +  ret_command + "\n"
        log_to_output_files(str_log, output)
       
        run_analyzes = run.get_analysis()
        for analysis in run_analyzes :
            str_log = "\t\t - ###ANALYSE " + analysis.name+ "' (" + str(analysis.id) + ") - " + analysis.directory + "\n"
            log_to_output_files(str_log, output)
            
            (ret_code, ret_command) = analysis.change_space(new_space_id)
            str_log = "\t\t   RET CODE: " + str(ret_code) + " CMD : " +  ret_command + "\n"
            log_to_output_files(str_log, output)
        
    analyzes = project.get_analysis()
    for analysis in analyzes :        
        str_log="\t - ##ANALYSE PROJECT '" + analysis.name + "' (" + str(analysis.id) + ") - " + analysis.directory + "\n"
        log_to_output_files(str_log, output)

        (ret_code, ret_command) = analysis.change_space(new_space_id)
        str_log = "\tRET CODE: " + str(ret_code) + " CMD : " +  ret_command + "\n"
        log_to_output_files(str_log, output)
            
    project.update_space_id(new_space_id)
    str_log = "project.update_space_id DONE\n"
    log_to_output_files(str_log, output)


class MoveProject (Component):
    
    def define_parameters(self, project_id, space_id,):
        self.add_parameter('project_id', 'The project id', type = existingprojectid, required = True, default = project_id )
        self.add_parameter('space_id', 'The new space_id', type = ng6space, required = True, default = space_id)
        self.add_output_file("stdout", "MoveProject stdout", filename="MoveProject.stdout")
        
    def process(self):
        logging.getLogger("MoveProject.process").debug(" Begin MoveProject Component.\n")
        logging.getLogger("MoveProject.process").debug(" self.space_id="+str(self.space_id)+"\n")
        logging.getLogger("MoveProject.process").debug(" self.project_id="+str(self.project_id)+"\n")
        logging.getLogger("MoveProject.process").debug(" stdout = "+str(self.stdout)+"\n")
        
        self.add_python_execution(migrate_project, cmd_format="{EXE} {ARG} {OUT}",
                                      outputs = self.stdout, arguments = [self.project_id, self.space_id])
        
