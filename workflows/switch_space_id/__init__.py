#
# Copyright (C) 2012 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import sys
import logging

from ng6.ng6workflow import BasicNG6Workflow
from ng6.project import Project
from jflow.utils import  display_error_message

from workflows.types import ng6space,existingprojectid
from ng6.config_reader import NG6ConfigReader

class SwitchSpaceId (BasicNG6Workflow):

    def get_description(self):
        return """This workflow allows you to migrate a project from a space_id to another.  
        It updates the project's space_id in DB, moves the runs files, updates the runs and analyzes's files paths, and updates the corresponding retentions."""

    def define_parameters(self, function="process"):
        ng6conf = NG6ConfigReader()
        space_choices = ng6conf.get_available_spaces()
        
        self.add_parameter('project_id', 'The project id', type=existingprojectid, required = True )
        self.add_parameter('space_id', 'The new space_id', required = True, type=ng6space, choices = space_choices)

    def process(self):
        
        self.project_id = int(self.project_id)
        project = Project.get_from_id(self.project_id)

        if project is not None :

            old_space_id = project.space_id
            
            #We log a message if the project already has the given space_id
            if str(old_space_id) == str(self.space_id) :
                logging.getLogger("SwitchProjectSpaceId.process").debug("The project " + project.name + " already belongs to the space_id '" + str(self.space_id) + "'\n" )

        add = self.add_component("MoveProject", [ self.project_id, self.space_id] )
