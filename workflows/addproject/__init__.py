#
# Copyright (C) 2012 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from jflow.workflow import Workflow
from ng6.project import Project
from ng6.config_reader import NG6ConfigReader
from workflows.types import ng6adminlogin, uniqproject, ng6space

class AddProject (Workflow):

    def get_description(self):
        return "Add a brand new project"

    def define_parameters(self, function="process"):
        ng6conf = NG6ConfigReader()
        space_choices = ng6conf.get_available_spaces()
        
        self.add_parameter("admin_login", "The login of a ng6 administrator", required = True, type = ng6adminlogin)
        self.add_parameter("project_name", "Give a name to your project (has to be unique)", flag = "--name", required = True, type = uniqproject)
        self.add_parameter("project_description", "Give a description to your project", flag = "--description", required = True)
        self.add_parameter("space", "Where to store data", flag = "--space", default="shortterm", type=ng6space, choices = space_choices)

    def process(self):
        project = Project(self.project_name, self.project_description, self.admin_login, None, self.space)
        self.metadata.append("project_id="+str(project.id))
