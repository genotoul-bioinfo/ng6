#! /usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use File::Basename;
use feature 'say';
use lib (`ktGetLibPath`);
use KronaTools;

setOption('name', 'root');
setOption('out', 'mothur.krona.html');

my @options =
qw(
	out
	name
	combine

);
	# minConfidence
	# depth
	# hueBad
	# hueGood
	# local
	# url
	# postUrl

getKronaOptions(@options);

if
(
	@ARGV < 1
)
{
	printUsage
	(
		'Create a Krona chart from Mothur classifications.',
		'mothur_details',
		'Option 1 : Take 1 argument : FILE.taxonomy
		or Option 2 : Take 3 arguments : FILE.taxonomy, FILE.groups and FILE.names.',
		0,
		1,
		\@options
	);
	
	exit 0;
}

my $tree = newTree();
my @datasetNames;
my $set = 0;

sub getExtension{
	my $fileName = $_[0];
	my ($name,$path,$suffix) = fileparse($fileName,qr"\..[^.]*$");
	return ($path,$name);
}

sub getNames {
	#create a hash of file.names : key = ID Seq and value = array with all ID seq with same seq 
	my $filename = $_[0];
	my $path = $_[1];
	open INFILE, "${path}${filename}.names" or die $!;
	my %hashNames;
	while (my $line = <INFILE>) 
	{
		my ($key, $value) = split /\s+/, $line;
		push @{$hashNames{$key}}, split /\s*,\s*/,$value;
	}
	return \%hashNames;
}

sub getGroups {
	#create a hash of file.groups : key = Num sample and value = array with all ID seq
	my $filename = $_[0];
	my $path = $_[1];
	open INFILE, "${path}${filename}.groups" or die $!;
	my %hashGroups;
	while (my $line = <INFILE>) 
	{
		my ($value, $key) = split /\t/, $line;
		push @{ $hashGroups{$key} }, $value;
	}
	return %hashGroups;
}

sub getIdNames {
	#retrieve the general ID seq 
	my $idGroups = $_[0];
	my $hashNames = $_[1];
	foreach my $key ( keys %$hashNames )  
	{
		foreach my $values ( @{ $hashNames->{$key} } )  
		{
		if ($idGroups eq $values)
			{
				return $key;
			}
		}
	}
}


sub getTaxonomy {
	#create two hashs of file.taxonomy : 
	#taxonomyLineage : key = ID seq and value = array of taxons
	#taxonomyScore : key = ID seq and value = array of pourcent
	#taxonomyCount : key = ID seq and value = number of sequences
	my %taxonomyLineage;
	my %taxonomyScores;
	my %taxonomyCount;
	my $filename = $_[0];
	my $path = $_[1];
	say "${path}${filename}.taxonomy";
	open INFILE, "${path}${filename}.taxonomy" or die $!;
	while (my $line = <INFILE>) 
	{
		next if ($line =~ m/OTU\tSize\tTaxonomy/);
		my $root;
		my @lineage;
		my @ranks;
		my @scores;
		my $allRanks;
		my $count = 1 ;
		
		my @fields = split /[\t;]/, $line;
		my $queryID = $fields[0];
		for ( my $i = 1; $i < $#fields; $i++ )
		{
			if( ($i == 1) && $fields[$i] =~ /^(\d+)$/ ) {
				$count = $1 ;
			} else {
				my ($val, $int) = $fields[$i] =~ /\"?(\w+)\"?\(*(\d*)\)*/;
				push @lineage, $val;
				push @scores, $int;
			}
		}
		push(@{ $taxonomyLineage{$queryID} }, @lineage);
		push(@{ $taxonomyScores{$queryID} }, @scores);
		$taxonomyCount{$queryID} = $count;
	}

	return (\%taxonomyLineage, \%taxonomyScores, \%taxonomyCount);
}

sub getLineage{
	# retrun the good lineage for one ID seq
	my $hashLineage = $_[0];
	my $idNames = $_[1];
	my @lineage = @{ $hashLineage->{$idNames} };
	return @lineage;
}

sub getScores{
	# retrun the good score for one ID seq
	my $hashScores = $_[0];
	my $idNames = $_[1];
	my @scores = @{ $hashScores->{$idNames} };
	return @scores;
}

sub getListFiles{
	#check if all files exists
	my $filename=$_[0];
	my $path=$_[1];
	opendir(DIR, $path);
	my @files = grep(/^$filename\.(taxonomy|groups|names|count_table)/,readdir(DIR));
	closedir(DIR);
	return \@files;
}

sub count_table_parse {
	#return for each sample, the count seqs for each unique sequence
	my $filename = $_[0];
	my $path = $_[1];
	say "${path}${filename}.count_table";
	open INFILE, "${path}${filename}.count_table" or die $!;
	my %hashCount;
	my @firstline = split('\t',<INFILE>);
	my @samples = @firstline[2..$#firstline-1];
	shift(@firstline);
	while (my $line = <INFILE>) 
	{
		my %temp;
		chomp($line);
		my @line = split('\t', $line);
		foreach my $cpt (1..$#line) {
			$temp{$firstline[$cpt-1]}=$line[$cpt];
		}
		$hashCount{$line[0]}={%temp};
	}
	return (\@samples,\%hashCount);
}

foreach my $input(@ARGV)
{
	say "MY INPUT                       $input";
	my ($fileName, $magFile, $name) = parseDataset($input);
	say "Importing $fileName...";
	open INFILE, "<$fileName" or die $!;
	my $format;
	my $line = <INFILE>;
	if ( $line =~ /\t/ )
	{
		$format = 1;
		say "File taxonomy found.";
		
		while ( $line !~ /\t/ )
		{
			$line = <INFILE>;
			
			if ( ! $line )
			{
				ktDie("Classifications not found in \"$fileName\". Is it an \"Assignment detail\" file?");
			}
		}
	}
	else
	{
		say "Command line format detected.";
	}
	my ($path,$filewe) = getExtension($input);
	#~ say "my name                $name";
	#~ say "my file                $filewe";
	#~ say "my path                $path";
	#~ say "my fullname                ${path}${filewe}";
	my $listFiles = getListFiles($filewe,$path);
	my $size = $#$listFiles + 1;
	my ($hashLineage, $hashScores, $hashCount) = getTaxonomy($filewe,$path);
	#if i have multiple samples (FILE.names and FILE.groups)
	if ($size eq 3)
	{
		my %samples = getGroups($filewe,$path);    # {'sample1:["seq_1", "seq_2"]', 'sample2':["seq_8"]}
		my $hashNames = getNames($filewe,$path);   # {'seq_3:["seq_4", "seq_5"]', 'seq_8':[]}
		foreach my $sample (keys %samples)
		{
			say "My sample      ",$sample;
			foreach my $seq (@{$samples{$sample}})
			{
				#
				my $idNames = getIdNames($seq,$hashNames);
				my @lineage = getLineage($hashLineage,$idNames);
				my @scores = getLineage($hashScores,$idNames);
				my $allRanks;
				my $root = $lineage[0];
				for ( my $i = 1; $i < @lineage; $i ++ )
				{
					if (! getOption('combine'))
					{
						if ( $lineage[$i] eq $root)
						{
							setOption('name',$root );
							$allRanks = 1;
							next;
						}
					}
				}
				addByLineage
				(
				$tree,
				$set,#rank for the different samples
				\@lineage,
				$seq,#name of id sequence
				undef,
				undef,
				\@scores #pourcent
				# $format ? ($allRanks ? undef : \@webRanks) : \@ranks
				);
			}

			if ( ! getOption('combine') )
			{
				$set++;
				push @datasetNames, $sample;
			}
		}
		close INFILE;
	}
	elsif ($size eq 2)
	{
		my ($samples,$counTable) = count_table_parse($filewe,$path);
		foreach my $sample (@$samples)
		{
			say "My sample      ",$sample;
			foreach my $seq (keys %$hashLineage)
			{				
				my $count = $counTable->{$seq}->{$sample};
				if ($count != "0")
				{
					my $total = $count;
					my $allRanks;
					my @lineage = @{ $hashLineage->{$seq} };
					my @scores = @{ $hashScores->{$seq} };
					my $root = $lineage[0];
					for ( my $i = 1; $i < @lineage; $i ++ )
					{
						if (! getOption('combine'))
						{
							if ( $lineage[$i] eq $root)
							{
								setOption('name',$root );
								$allRanks = 1;
								next;
							}
						}
					}
					addByLineage
					(
					$tree,
					$set,#rank for the different samples
					\@lineage,
					$seq,#name of id sequence
					$total,
					undef,
					\@scores #pourcent
					# $format ? ($allRanks ? undef : \@webRanks) : \@ranks
					);
				}
			}

			if ( ! getOption('combine') )
			{
				$set++;
				push @datasetNames, $sample;
			}
		}
		close INFILE;
	}
	#if i have just FILE.taxonomy (OTU)
	elsif($size eq 1)
	{
		foreach my $seq (keys %$hashLineage)
		{
			if ($hashCount->{$seq} != "0")
			{
				my $allRanks;
				my @lineage = @{ $hashLineage->{$seq} };
				my @scores = @{ $hashScores->{$seq} };
				my $root = $lineage[0];
				for ( my $i = 1; $i < @lineage; $i ++ )
				{
					if ( $lineage[$i] eq $root)
					{
						setOption('name',$root );
						$allRanks = 1;
						next;
					}
				}
				addByLineage
				(
				$tree,
				$set,
				\@lineage,
				$seq,
				$hashCount->{$seq},
				undef,
				\@scores
				# $format ? ($allRanks ? undef : \@webRanks) : \@ranks
				);
			}
		}
		if ( ! getOption('combine') )
			{
				$set++;
				push @datasetNames, $filewe;
			}
	}
	else
	{
		say "There are not all files required. Please check if you have opt1 : FILE.taxonomy or opt2 : FILE.taxonomy, FILE.groups and FILE.names in your current directory.";
		exit 0;
	}
} 
say "Done";

my @attributeNames =
(
	'count',
	'magnitude',
	'unassigned',
	'score'
);

my @attributeDisplayNames =
(
	'Count',
	'Total',
	'Unassigned',
	'Avg. % Confidence',
);

writeTree
(
	$tree,
	\@attributeNames,
	\@attributeDisplayNames,
	\@datasetNames,
	# getOption('hueBad'),
	# getOption('hueGood')
);
