#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import sys
import re
import logging

from ng6.ng6workflow import CasavaNG6Workflow
from ng6.utils import Utils


class IlluminaDiversityQC (CasavaNG6Workflow):
    
    def get_name(self):
        return 'illumina_diversity_qc'
    
    def get_description(self):
        return "Illumina diversity quality check pipeline. This pipeline needs to use paired-end reads with overlap."
    
    
    def define_parameters(self, function="process"):
        self.add_input_file("reference_genome", "Which genome should the read being align on")
        self.add_parameter("assignation_databank", "Blast databank to classify a subset of sequences")
        self.add_parameter("mismatch_ratio", "Maximum allowed ratio between the number of mismatched base pairs and the overlap length.", default = 0.1, type = float, group="JOIN section")
        self.add_parameter("min_overlap", "The minimum required overlap length between two reads to provide a confident overlap.", default = 20, type = int, group="JOIN section")
        self.add_parameter("max_overlap", "Maximum overlap length expected in approximately 90 percent of read pairs.", default = 55, type = int, group="JOIN section")
    
    def process(self):
        fastqilluminafilter, filtered_read1_files, filtered_read2_files, concat_files, concatenatefastq = self.illumina_process()      
        logging.getLogger("IlluminaDiversityQC").debug("process Utils.concat_files = " + ",".join(concat_files))
        logging.getLogger("IlluminaDiversityQC").debug("process concatenatefastq.r1_files = " + ",".join(concatenatefastq.r1_files))
        logging.getLogger("IlluminaDiversityQC").debug("process concatenatefastq.r2_files = " + ",".join(concatenatefastq.r2_files))
        
        # merge overlapping pair
        join_pairs = self.add_component("Flash", [concatenatefastq.r1_files, concatenatefastq.r2_files, self.mismatch_ratio, self.min_overlap, self.max_overlap], parent=fastqilluminafilter)
        
        if self.assignation_databank != None:
            # subset assignation
            subset_assignation = self.add_component("SubsetAssignation", [join_pairs.extended_frags, self.assignation_databank], parent=join_pairs)
