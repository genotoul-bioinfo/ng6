#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os, re, sys

from ng6.ng6workflow import NG6Workflow
from ng6.utils import Utils


class GeneDiversity (NG6Workflow):

    def _get_clean_list( self, split_inputs, split_outputs, samples_names = [] ):
        """
         @summary : Returns two lists. The sorted list of split component outputs. 
                     The sorted and completed list of samples names which correspond to the sorted list of split component outputs.
          @param split_inputs : [list] The list of inputs for split. It is used to find the initial order of files.
          @param split_outputs : [list] The list to sort.
          @param samples_names : [list] The list of samples names to sort and complete.
         @return : [list, list] The sorted list of split outputs and the sorted and completed list of samples names which correspond.
        """
        new_samples_names = list()
        new_split_outputs = list()
        for idx in range( len(split_inputs) ):
            sample = samples_names[idx] if samples_names else None
            basename = os.path.basename(split_inputs[idx])
            basename_woext = basename.split(".")[0]
            extensions = ".".join( basename.split(".")[1:] )
            for file in split_outputs:
                out_split_basename = os.path.basename( file )
                if re.match(basename_woext + "_\d+." + extensions, out_split_basename) is not None:
                    new_samples_names.append( sample )
                    new_split_outputs.append( file )
        if not samples_names :
            new_samples_names = None
        return new_split_outputs, new_samples_names
    
    def _load_merge_arg(self, samples_names, merge_args):
        """
         @summary : Parse and check the merge parameter to return the dictionnary of merge groups.
          @param samples_names : [list] All the samples names.
          @param merge_args: multiple parameter list (list of dictionary)
         @return : [dict] The merge groups (key:group name ; values:list of samples).
        """
        merge_groups = {}
        for merge_arg in merge_args :
            merge_groups[merge_arg['name']] = merge_arg['sample_name']
        self._check_merge_groups( merge_groups, samples_names )
        if merge_groups :
            return merge_groups
        return None
    
    def _check_merge_groups(self, merge_groups, samples_names):
        """
         @summary : Raise an exception if the merge parameter is wrong.
          @param merge_groups : [dict] The merge groups (key:group name ; values:list of samples).
          @param samples_names : [list] All the samples names.
        """
        merged_samples = list()
        wrong_samples = list()
        duplicated_samples = list()
        # Check
        for group_samples in list(merge_groups.values()):
            for sample in group_samples:
                if not sample in samples_names: # The sample does not exist
                    wrong_samples.append( sample )
                else:  # The sample exists
                    if sample in merged_samples: # The sample is already used in other merge
                        duplicated_samples.append( sample )
                    else: # The sample is in any other merge
                        merged_samples.append( sample )
        # Error message
        error_msg = ""
        if len(wrong_samples) > 0:
            error_msg += "The sample(s) '" + "', '".join(wrong_samples) + "' do(es)n't exist."
        if len(duplicated_samples) > 0:
            error_msg += "The sample(s) '" + "',  '".join(duplicated_samples) + "' is/are duplicated."
        if error_msg != "":
            raise ValueError( "Error in merge rules : " + error_msg )
    
    
    def get_name(self):
        return 'gene_diversity'
    
    def get_description(self):
        return "Analysis the composition and function of a microbial community from a functional gene."
    
    def define_parameters(self, function="process"):
        
        self.add_multiple_parameter_list("merge", "The list of samples to merge")
        self.add_parameter("name", "Merge group name", required=False, add_to="merge")
        self.add_parameter_list("sample_name", "The name of the sample", required=False, add_to="merge")
        
        self.add_input_file("database", "The reference set contains protein representative sequences of the gene target" + 
                            "and should be compiled to have a good coverage of diversity of the gene family", required=True, group="Gene section")
        self.add_input_file("taxonomy", "The gene taxonomy. Format : 'GENE_ID<tab>TAX; TAX; TAX;'.", required=True, group="Gene section")
        
        self.add_parameter("trim_read_1", "Maximum length for reads 1.", default=500, type='int', group="1- TRIM section")
        self.add_parameter("trim_read_2", "Maximum length for reads 2.", default=500, type='int', group="1- TRIM section")
        
        self.add_parameter("mismatch_ratio", "Maximum allowed ratio between the number of mismatched base pairs and the overlap length.", default=0.1, type='float', group="2- JOIN section")
        self.add_parameter("min_overlap", "The minimum required overlap length between two reads to provide a confident overlap.", default=20, type='int', group="2- JOIN section")
        self.add_parameter("max_overlap", "Maximum overlap length expected in approximately 90 percent of read pairs.", default=55, type='int', group="2- JOIN section")
        
        self.add_parameter("min_length", "The minimum length of a fragment after join.", type='int', group="3- FILTER section")
        self.add_parameter("max_length", "The maximum length of a fragment after join.", type='int', group="3- FILTER section")
        self.add_parameter("max_n", "The maximum number of N in a fragment after join.", type='int', group="3- FILTER section", default=1)
        
        self.add_parameter("protein_min_length", "The protein minimal length..", type='int', group="4- TRADUCTION section", default=80)
        
        self.add_parameter("otu_identity_threshold", "Sequence identity threshold. Calculated as : number of "+
                           "identical amino acids in alignment divided by the full length of the shorter sequence.", type='float', group="5- OTU section", default=0.95)
        
        self.add_parameter("otu_length_diff_cutoff", "Maximum length difference between shorter end representative sequence of the cluster. "+
                           "If set to 0.9, the shorter sequences need to be at least 90 percent length of the representative of the cluster.", 
                           flag = '--otu-lg-diff-cutoff', type='float', group="5- OTU section", default=0.8)
        self.add_parameter("otu_cluster_most_similar", "Change the clustering method. True : the program will cluster it into the most similar "+
                           "cluster that meet the threshold (accurate but slow mode). False : the sequence can be clustered to the first cluster "+
                           "that meet the threshold (fast cluster). Either won't change the representatives of final clusters.", 
                           flag = '--otu-most-similar', type='bool', group="5- OTU section", default=True)
        
        self.add_parameter("discard", "The number of discarded sequences before each round of random sampling.", type='int', group="6- SAMPLING section", default=0)
        self.add_parameter("select", "The number of selected sequences with replacement in each round of random sampling.", type='int', group="6- SAMPLING section", default=2000)
        self.add_parameter("round", "The number of round for the random sampling.", type='int', group="6- SAMPLING section", default=100)
        self.add_parameter("obs_min", "The minimum number of sequences to keep an observation. This filter is applied before sampling.", type='int', group="6- SAMPLING section", default=2)
            
    def process(self):
        # Manage samples
        merge_groups = self._load_merge_arg( self.samples_names, self.merge )  if self.samples_names else None
        
        # Add raw files
        addrawfiles = self.add_component( "AddRawFiles", [self.runobj, self.get_all_reads(), "none"] )
            
        # Trim sequences
        trim_R1 = self.add_component("Trimmer", [self.get_all_reads('read1'), 1, self.trim_read_1], component_prefix="R1")
        trim_R2 = self.add_component("Trimmer", [self.get_all_reads('read2'), 1, self.trim_read_2], component_prefix="R2")
             
        # Make some statistics on raw file
        #fastqc = self.add_component("FastQC", [trim_R1.output_files + trim_R2.output_files, False, True])
             
        # Merge overlapping pair
        join_pairs = self.add_component("Flash", [trim_R1.output_files, trim_R2.output_files, self.mismatch_ratio, self.min_overlap, self.max_overlap])
        
        # Filter sequences
        filter = self.add_component("FilterSeq", [join_pairs.extended_frags, self.min_length, self.max_length, self.max_n], parent=join_pairs)
        
        # Fastq to fasta
        fastq2fasta = self.add_component("Fastq2fasta", [filter.output_files])
              
        # Dereplicates sequences
        dereplicate = self.add_component("UsearchDereplication", [fastq2fasta.output_files])
        
        # Remove chimeric sequences
        chimera = self.add_component("UsearchChimera", [dereplicate.output_files, 6], parent=filter)
        
        # Sequence traduction
        split = self.add_component("SplitSeq", [chimera.nonchimeras, 6000])
        
        split_outputs, new_samples_names = self._get_clean_list( chimera.nonchimeras, split.output_files, self.samples_names )
        
        framebot = self.add_component("Framebot", [split_outputs, self.database, self.protein_min_length, False])

        # Rename the pre-clusters to provide traceback after merge and cd-hit
        rename_clusters = self.add_component("AddSamplesNames", [framebot.corrected_proteins, new_samples_names])
            
        # Merge sequences
        merge = self.add_component("ConcatenateFiles", [rename_clusters.output_files, "all_trad_sequences.fasta"])
             
        # Create OTU
        cdhit = self.add_component("Cdhit", [merge.output_file, self.otu_identity_threshold, self.otu_length_diff_cutoff, 
                                             self.otu_cluster_most_similar, 5, 'euclidean', 'average'], parent=chimera)
       
        # Sampling
        groups = merge_groups if merge_groups else None
        sampling = self.add_component("BiomSampling", [cdhit.biom_files, self.discard, self.select, self.round, 
                                                       self.obs_min, cdhit.output_files, 'euclidean', 'average', groups], parent=cdhit)
         
        # Stat on OTU
        blast_index = self.add_component("BlastIndex", [self.database, "prot"])
        otu_classify = self.add_component("GeneOTUClassify", [cdhit.biom_files+sampling.output_files, cdhit.output_files+sampling.output_fasta, 
                                                        self.taxonomy, blast_index.databank], parent=sampling)