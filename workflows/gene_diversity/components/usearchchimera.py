#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os, re

from subprocess import Popen, PIPE

from ng6.analysis import Analysis

from jflow.abstraction import MultiMap

from weaver.function import ShellFunction, PythonFunction


def filter_count( chimeras_file, non_chimeras_file, output_file ):
    """
     @summary : Writes file with the non-chimeras and chimeras count.
      @param size_separator : [str] The number of sequences represented by each sequence/pre-cluster must be stored at
                              the end of each sequence ID. The separator must be size_separator.
                              Example : size_sep=';' where sequence ID = 'seq10001;83'
      @param chimeras_file : [str] path to the fasta file off chimeras.
      @param non_chimeras_file : [str] path to the fasta file off non-chimeras.
      @param output_file : [str] path to the output file.
    """
    import jflow.seqio as seqio
    size_separator = ';size='
    chimeras_count = 0
    non_chimeras_count = 0
    
    # Chimeras
    reader = seqio.SequenceReader( chimeras_file )
    for id, desc, seq, qual in reader :
        count = id.split( size_separator )[-1]
        # Patch for Usearch dereplication
        if count.endswith( ';' ):
            count = count[0:-1]
        chimeras_count += int(count)
        
    # Non chimeras
    reader = seqio.SequenceReader( non_chimeras_file )
    for id, desc, seq, qual in reader :
        count = id.split( size_separator )[-1]
        # Patch for Usearch dereplication
        if count.endswith( ';' ):
            count = count[0:-1]
        non_chimeras_count += int(count)
        
    # Write
    out_fh = open( output_file, "w" )
    out_fh.write( str(chimeras_count) + " chimeras" + "\n" )
    out_fh.write( str(non_chimeras_count) + " non-chimeras" + "\n" )
    out_fh.close()

class UsearchChimera (Analysis):
    """
     @summary: Remove chimeric sequences.
     @see : http://drive5.com/usearch/manual/uchime_algo.html
     @see : http://drive5.com/usearch/manual/chimera_formation.html
    """
    
    def define_parameters(self, fasta_list, min_diffs=3):
        """
          @param fasta_list : [list] path of files that will be processed.
          @param min_diffs : [int] minimum number of diffs in a segment. Must be > 0.
        """
        self.add_parameter("min_diffs", "inimum number of diffs in a segment. Must be > 0.", default=min_diffs, type='int')
        self.add_input_file_list( "input_fasta", "path of files that will be processed.", default=fasta_list, file_format='fasta' )
        self.add_output_file_list("stdout", "stdout", pattern='{basename_woext}.stdout', items=self.input_fasta)
        self.add_output_file_list("stderr", "stderr", pattern='{basename_woext}.stderr', items=self.input_fasta)
        self.add_output_file_list("log", "log", pattern='{basename_woext}.log', items=self.input_fasta)
        self.add_output_file_list("chimeras", "chimeras", pattern='{basename_woext}_chimeras.fasta', items=self.input_fasta, file_format='fasta')
        self.add_output_file_list("nonchimeras", "nonchimeras", pattern='{basename_woext}_nonchimeras.fasta', items=self.input_fasta, file_format='fasta')
        self.add_output_file_list("stat", "stat", pattern='{basename_woext}.stat', items=self.input_fasta)
    
    def define_analysis(self):
        self.name = "RemoveChimera"
        self.description = "Remove chimeric sequences."
        self.software = "usearch"
        self.options = "-mindiffs " + str(self.min_diffs) + " -uchime_denovo"
        
    def _parse_stat(self, file_path):
        """
         @summmary : Return the number of chimeras and the number of non-chimeras from the usearch uchime_denovo stderr or stat file.
          @param file_path : [string] the stderr or stat filepath.
         @note : stderr example  
           00:00 2.2Mb Reading /work/ng6/jflow/gene_diversity/wf000908/UsearchDereplication_default/dsrB-ADNc-3-FM3-C0-Jour_CAGTAT_L001_R.extendedFrags_nr.fasta, 11Mb
           00:00  13Mb 24362 (24.4k) seqs, min 251, avg 377, max 430nt
           01:46 131Mb  100.0% Search 3226/24362 chimeras found (13.2%)
           01:47 131Mb  100.0% Writing alignments                      
           01:47 131Mb  100.0% Writing hits      
           01:47 131Mb  100.0% Writing 3226 chimeras
           01:47 131Mb  100.0% Writing 21136 non-chimeras
        """
        nb_chimeras = 0
        nb_non_chimeras = 0
        
        stderr_fh = open(file_path)
        for line in stderr_fh:
            line = line.strip()
            matches = re.search("(\d+) chimeras$", line)
            if matches is not None:
                nb_chimeras = matches.group(1)
            else:
                matches = re.search("(\d+) non-chimeras$", line)
                if matches is not None:
                    nb_non_chimeras = matches.group(1)
        
        stderr_fh.close()
        return nb_chimeras, nb_non_chimeras

    def post_process(self):
        self._create_and_archive(self.nonchimeras + self.log, "usearch_chimera.gz")
        
        for filepath in self.stat:
            sample = os.path.basename(filepath).split(".")[0]
            nb_chimeras, nb_non_chimeras = self._parse_stat( filepath )
            self._add_result_element( sample, "nb_chimeras", str(nb_chimeras) )
            self._add_result_element( sample, "nb_non_chimeras", str(nb_non_chimeras) )
    
    def get_version(self):
        cmd = [self.get_exec_path("usearch"), "--version"]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stdout.split()[1]    
    
    def process(self):
        # Process
        chimera = ShellFunction( self.get_exec_path("usearch") + " -mindiffs " + str(self.min_diffs) + " -uchime_denovo $1 -uchimeout $2 -uchimealns $3 -chimeras $4 -nonchimeras $5 2> $6", cmd_format='{EXE} {IN} {OUT}' )
        chimera = MultiMap(chimera, inputs=self.input_fasta, outputs=[self.stdout, self.log, self.chimeras, self.nonchimeras, self.stderr])
        
        # Statistics
        stat = PythonFunction( filter_count, cmd_format='{EXE} {IN} {OUT}' )
        stat = MultiMap(stat, inputs=[self.chimeras, self.nonchimeras], outputs=[self.stat])
            