#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.component import Component
from jflow.abstraction import MultiMap

from weaver.function import ShellFunction


class UsearchDereplication (Component):
    """
     @summary: Dereplication is the removal of duplicated sequences.
     @see : http://www.drive5.com/usearch/manual/dereplication.html
    """
    
    def define_parameters(self, fasta_list, method="fulllength", strand="both",  minuniquesize=None):
        """
          @param files_list : path of files that will be processed.
          @param method : algorithm to search replicates.
              'fulllength' => If two or more sequences are identical, all except one are kept.
              'prefixes'   => A sequence A is discarded if it is a prefix of some other sequence B in the set.
          @param strand : Search for hits on the forward or forward and reverse-complemented strands.
              'plus' => Search for hits on the forward (plus) strand only. This is faster, so is recommended when the query sequences are known to be oriented on the same strand as the database.
              'both' => Search for hits on both the forward and reverse-complemented strands.
          @param minuniquesize : may be used to set a minimum size for a cluster; unique sequences with smaller clusters are not included in the output file.
        """
        
        self.add_parameter("method", "algorithm to search replicates.", default=method, choices=["prefixes", "fulllength"])
        self.add_parameter("strand", "Search for hits on the forward or forward and reverse-complemented strands.", default=strand, choices=["plus", "both"])
        self.add_parameter("minuniquesize", "may be used to set a minimum size for a cluster; " +
                           "unique sequences with smaller clusters are not included in the output file.", default=minuniquesize)
        
        # Check
        if self.method == "prefixes" and self.strand == "both":
            raise "Dereplication method 'prefixes' does not support 'strand=both'."      
        
        # Parameters
        self.options = " -threads 1 -sizeout"
        self.options += " -strand " + self.strand
        if self.minuniquesize != None:
            self.options += " -minuniquesize " + str(self.minuniquesize)
        
        self.add_input_file_list( "input_fasta", "path of files that will be processed.", default=fasta_list, required=True, file_format='fasta' )
        self.add_output_file_list("output_files", "output_files", pattern='{basename_woext}_nr.fasta', items=self.input_fasta, file_format='fasta')
        self.add_output_file_list("stderr", "stderr", pattern='{basename_woext}.stderr', items=self.input_fasta)
        
    def process(self):
        dereplicate = ShellFunction( self.get_exec_path("usearch") + " -derep_" + self.method + " $1 -fastaout $2 " + self.options + " 2> $3", cmd_format='{EXE} {IN} {OUT}' )
        dereplicate = MultiMap(dereplicate, inputs=self.input_fasta, outputs=[self.output_files, self.stderr])