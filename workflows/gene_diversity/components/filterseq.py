#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from ng6.analysis import Analysis

from jflow.abstraction import MultiMap

from weaver.function import PythonFunction
  
  
def filter_seq( input_file, output_file, log_file, min_length="-1", max_length="-1", max_N="-1", max_homopolymer="-1" ):
    """
     @summary: Filters sequences on length, number of N and number of homopolymer.
      @param input_file : [str] Path to the processed sequence file.
      @param output_file : [str] Path to the output file.
      @param log_file : [str] Path to the log file. It contains the numeric results of each filters.
      @param min_length : [str] The minimum length to keep a sequence. The value "-1" cancels the filter on this criterion. 
      @param max_length : [str] The maximum length to keep a sequence. The value "-1" cancels the filter on this criterion.
      @param max_N : [str] The maximum number of N to keep a sequence. The value "-1" cancels the filter on this criterion.
      @param max_homopoly : [str] The maximum number of homopolymer to keep a sequence. The value "-1" cancels the filter on this criterion.
    """
    import jflow.seqio as seqio
    
    def is_true( *val ):
        return True
    
    def check_min_length( length ):
        return length >= int(min_length)
    
    def check_max_length( length ):
        return length <= int(max_length)
    
    def check_length( length ):
        return( length >= int(min_length) and length <= int(max_length) )
    
    def check_N_number( sequence ):
        return( (sequence.count('N') + sequence.count('n')) <= int(max_N) )
    
    def check_homopolymer( sequence ):
        previous = ''
        homopolymer_length = 0
        sequence_length = len( sequence )
        idx = 0
        while homopolymer_length < int(max_homopolymer) and idx < sequence_length:
            if sequence[idx] == previous:
                homopolymer_length += 1
            else:
                homopolymer_length = 0
            previous = sequence[idx]
            idx += 1
        return homopolymer_length < int(max_homopolymer)
    
    def write_fasta( fh, seq_record  ):
        """
         @summary : Writes a sequence record in FASTA output file.
          @param fh : [File] The file handler to the output file.
          @param seq_record : [list] The id, description, sequence and quality of the record.
        """
        seqio.writefasta(out_fh, [seq_record])
        
    def write_fastq( fh, seq_record  ):
        """
         @summary : Writes a sequence record in FASTQ output file.
          @param fh : [File] The file handler to the output file.
          @param seq_record : [list] The id, the description, the sequence and the quality of the record.
        """
        seqio.writefastq(out_fh, [seq_record])
        
    # Load suitable function to check length
    length_is_ok = is_true # no check
    if min_length != "-1" and max_length != "-1": # check min and max length
        length_is_ok = check_length
    elif min_length != "-1" and max_length == "-1": # check only min length
        length_is_ok = check_min_length
    elif min_length == "-1" and max_length != "-1": # check only max length
        length_is_ok = check_min_length
    
    # Load suitable function to check the number of N
    N_number_is_ok = is_true # no check
    if max_N != "-1":
        N_number_is_ok = check_N_number
    
    # Load suitable function to check the number homopolymer
    homopolymer_is_ok = is_true # no check
    if max_homopolymer != "-1":
        homopolymer_is_ok = check_homopolymer
    
    # Open output
    if input_file.endswith(".gz"):   
        out_fh = seqio.xopen( output_file, "w" )
    else:
        out_fh = open( output_file, "w" )
    
    # Load suitable function to write sequences
    reader = seqio.SequenceReader( input_file )
    if reader.__class__.__name__ == "FastqReader": # If sequences file is a FASTQ          
        write = write_fastq   
    elif reader.__class__.__name__ == "FastaReader": # If sequences file is a FASTA
        write = write_fasta
    
    # Process
    nb_seq = 0
    filter_on_length = 0
    filter_on_N = 0
    filter_on_homopoly = 0
    for id, desc, seq, qual in reader:
        nb_seq += 1
        if not length_is_ok( len(seq) ):
            filter_on_length += 1
        elif not N_number_is_ok( seq ):
            filter_on_N += 1
        elif not homopolymer_is_ok( seq ):
            filter_on_homopoly += 1
        else:
            write( out_fh, [id, desc, seq, qual] )
    
    # Write log
    log_fh = open( log_file, "w" )
    log_fh.write( "Nb seq processed               : " + str(nb_seq) + "\n" )
    log_fh.write( "Nb seq filtered on length      : " + str(filter_on_length) + "\n" )
    log_fh.write( "Nb seq filtered on N (after length filter) : " + str(filter_on_N) + "\n" )
    log_fh.write( "Nb seq filtered on homopolymer (after N filter) : " + str(filter_on_homopoly) + "\n" )
    log_fh.close()


class FilterSeq (Analysis):
    """
     @summary: Filters sequences on length, number of N and number of homopolymer.
    """

    def define_parameters( self, files_list, min_length=None, max_length=None, max_N=None, max_homopoly=None ):
        """
          @param files_list : [str] path to sequences files that will be processed.
          @param min_length : [int] The minimum length to keep a sequence.
          @param max_length : [int] The maximum length to keep a sequence.
          @param max_N : [int] The maximum number of N to keep a sequence.
          @param max_homopoly : [int] The maximum number of homopolymer to keep a sequence.     
        """
        
        self.add_parameter("min_length", "The minimum length to keep a sequence.", default=(-1, min_length)[min_length!=None], type='int')
        self.add_parameter("max_length", "The maximum length to keep a sequence.", default=(-1, max_length)[max_length!=None], type='int')
        self.add_parameter("max_N", "The maximum number of N to keep a sequence.", default=(-1, max_N)[max_N!=None], type='int')
        self.add_parameter("max_homopoly", "The maximum number of homopolymer to keep a sequence.     ", default=(-1, max_homopoly)[max_homopoly!=None], type='int')
        
        self.add_input_file_list( "files_list", "path to sequences files that will be processed.", default=files_list, required=True )
        self.add_output_file_list("output_files", "output_files", pattern='{basename}', items=self.files_list, file_format=self.files_list.file_format)
        self.add_output_file_list("log_files", "log_files", pattern='{basename_woext}.log', items=self.files_list)
        self.stderr = os.path.join(self.output_directory, 'filter.stderr')

    def define_analysis( self ):
        self.name = "Filter"
        self.description = "Filter sequences."
        self.software = "-"
        self.options = ""
        if self.min_length != -1:
            self.options += "min_length:" + str(self.min_length) + ";"
        if self.max_length != -1:
            self.options += "max_length:" + str(self.max_length) + ";"
        if self.max_N != -1:
            self.options += "max_N:" + str(self.max_N) + ";"
        if self.max_homopoly != -1:
            self.options += "max_homopoly:" + str(self.max_homopoly) + ";"
        if self.options != "":
            self.options = self.options[:-1]
    
    def get_version( self ):
        return "-"
    
    def _parse_log( self, log_file ):
        """
         @summary : Returns information about results of filters from a log file.
          @param log_file : [str] path to the log file.
         @return : [list] The number of processed sequences, 
                           the number of sequences filtered on length,
                           the number of sequences filtered on number of N,
                           the number of sequences filtered on number of homopolymer
        """
        log_fh = open( log_file )
        nb_seq_processed = log_fh.readline().split(':')[1].strip()
        nb_filtered_on_length = log_fh.readline().split(':')[1].strip()
        nb_filtered_on_N = log_fh.readline().split(':')[1].strip()
        nb_filtered_on_homopoly = log_fh.readline().split(':')[1].strip()
        log_fh.close()
        return nb_seq_processed, nb_filtered_on_length, nb_filtered_on_N, nb_filtered_on_homopoly
    
    def post_process( self ):
        for current_file in self.log_files:
            sample_name = os.path.basename( current_file ).split(".")[0]
            nb_seq_processed, nb_filtered_on_length, nb_filtered_on_N, nb_filtered_on_homopoly = self._parse_log( current_file )
            self._add_result_element( sample_name, "nb_seq_processed", str(nb_seq_processed) )
            self._add_result_element( sample_name, "nb_filtered_length", str(nb_filtered_on_length) )
            self._add_result_element( sample_name, "nb_filtered_N", str(nb_filtered_on_N) )
            self._add_result_element( sample_name, "nb_filtered_homopolymer", str(nb_filtered_on_homopoly) )
       
    def process( self ):
        filter = PythonFunction( filter_seq, cmd_format='{EXE} {IN} {OUT} ' + str(self.min_length) + ' ' + str(self.max_length) + ' ' + \
                                 str(self.max_N) + ' ' + str(self.max_homopoly) + " 2>> " + self.stderr )
        MultiMap( filter, inputs=self.files_list, outputs=[self.output_files, self.log_files] )