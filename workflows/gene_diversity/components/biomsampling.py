#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os, pickle

from workflows.gene_diversity.lib.biomstat import *

from ng6.analysis import Analysis

from jflow.abstraction import MultiMap

from weaver.function import PythonFunction
from weaver.abstraction import Map   


def merge_samples( input_biom, merge_dump, output_biom, min_evidence_nb=None ):
    """
     @summary : Adds counts and merges metadata of a list of samples.
      @param input_biom : [str] path to the biom file processed.
      @param merge_dump : [str] path to the dump file with the rules to merge.
      @param min_evidence_nb : [int] the minimun number of replicates with the 
                                observation (@see Biom.reset_count_by_replicates_evidence).
    """
    import pickle
    from jflow.featureio import BiomIO
    
    # Retrieve merge rules
    merge = open(merge_dump, "rb")
    merge_groups = pickle.load(merge)
    merge.close()
    
    biom = BiomIO.from_json( input_biom )
    # Filter on evidence
    if min_evidence_nb is not None:
        for merged_sample in list(merge_groups.keys()):
            biom.reset_count_by_replicates_evidence( merge_groups[merged_sample], int(min_evidence_nb) )
        biom.filter_OTU_by_count( 1 )
    # Merge
    for merged_sample in list(merge_groups.keys()):
        biom.merge_samples( merge_groups[merged_sample], merged_sample )
    BiomIO.write( output_biom, biom )

def filter_and_bootstrap( input_biom, output_biom, observation_threshold, nb_deleted, nb_selected, nb_round ):
    """
     @summary :
    """
    from jflow.featureio import BiomIO
    
    biom = BiomIO.from_json( input_biom )
    observation_threshold = int(observation_threshold)
    nb_selected = int(nb_selected)
    nb_deleted = int(nb_deleted)
    nb_round = int(nb_round)
    
    # Filter
    if observation_threshold > 0:
        biom.filter_OTU_by_count( observation_threshold )
    # Normalisation
    biom.bootstrap_by_sample( nb_selected, nb_deleted, nb_round )
    # Removed the empty observations
    biom.filter_OTU_by_count( 1 )
    # Write
    BiomIO.write( output_biom, biom )
    
def filter_seq( input_fasta, input_biom, output_fasta ):
    """
     @summary : Writes a new fasta with only the sequences corresponding of the observations.
      @param input_fasta : [str] path to the file to filter (fasta file).
      @param input_biom : [str] path to the file with the IDs of kept sequences (biom file).
      @param output_fasta : [str] path to the filtered fasta.
    """
    from jflow.featureio import BiomIO
    import jflow.seqio as seqio
    
    biom = BiomIO.from_json( input_biom )
    # Find the ids for kept sequences
    kept_sequences = dict()
    for observation_id in biom.get_observations_names():
        kept_sequences[observation_id] = 1
        
    # Filter sequences
    reader = seqio.SequenceReader( input_fasta )
    out_fh = open( output_fasta, "w" )
    for id, desc, seq, qual in reader :
        if id in kept_sequences:
            seqio.writefasta( out_fh, [[id, desc, seq, qual]] )
    out_fh.close()


class BiomSampling (Analysis):
    """
     @summary : Sampling the count of clusters in Biom file.
                [1- Merge specified samples]
                2- Filter observation on minimum depth
                3- Sampling
                   For each round :
                    a- Discard several sequences (replacement at the end of the current round)
                    b- Select several sequences with replacement
                    c- Add selected sequences in sampling
                4- Delete observation without sequences.
    """
    
    def define_parameters(self, biom, nb_deleted, nb_selected, nb_round, observation_threshold=0, fasta_files=None, distance_method='euclidean', linkage_method='average', merge_groups=None):
        """
          @param biom : [str] path to the biom file processed.
          @param nb_deleted : [int] the number of elements randomly removed at each round from the initial file.
          @param nb_selected : [int] the number of elements randomly selected at each round from the file without the deleted elements.
          @param nb_round : [int] the number of execution of the process "initial biom -> delete X elements -> select Y elements -> add elements to final biom".
          @param observation_threshold : [int]
          @param distance_method : [str] distance method for the hierarchical clustering. Accepted values @see biomstat.samples_hclassification.
          @param linkage_method : [str] linkage method for the hierarchical clustering. Accepted values @see biomstat.samples_hclassification.
          @param merge_groups : [list] the list of samples to merge. 
                                       Example : [
                                                  { "sample_a" : ["sample_a_1", "sample_a_2"],                 # Biom_A
                                                    "sample_b" : ["sample_b_1", "sample_b_2", "sample_b_3"] }
                                                  { "sample_a" : ["sample_a_1", "sample_a_2"],                 # Biom_B
                                                    "sample_b" : ["sample_b_1", "sample_b_2", "sample_b_3"] }
                                                 ]
        """
        self.add_input_file_list('input_bioms', 'path to the biom file processed.', default=biom, required=True)
        self.add_parameter('nb_deleted', 'the number of elements randomly removed at each round from the initial file', default=nb_deleted, required=True, type='int')
        self.add_parameter('nb_selected', 'the number of elements randomly selected at each round from the file without the deleted elements', default=nb_selected, required=True, type='int')
        self.add_parameter('nb_round', 'the number of execution of the process '+
                           '"initial biom -> delete X elements -> select Y elements -> add elements to final biom".', default=nb_round, required=True, type='int')
        self.add_parameter('observation_threshold', 'observation_threshold.', default=observation_threshold, type='int')
        self.add_parameter('distance_method', 'distance_method.', default=distance_method)
        self.add_parameter('linkage_method', 'linkage_method.', default=linkage_method)
        self.merge_groups = merge_groups
        self.min_evidence_nb = 2
        
        self.add_input_file_list('input_fasta', 'input_fasta.', default=fasta_files)
        self.add_output_file_list("output_fasta", "output_fasta", pattern='{basename}', items=self.input_fasta)
        
        self.add_output_file_list("output_files", "output_fasta", pattern='{basename_woext}_norm.biom', items=self.input_bioms)
        self.add_output_file_list("depth_files", "depth_files", pattern='{basename_woext}_norm_depth.tsv', items=self.input_bioms)
        self.add_output_file_list("hclust_files", "depth_files", pattern='{basename_woext}_norm_hclust.json', items=self.input_bioms)
        self.stderr = os.path.join( self.output_directory, 'normalisation.stderr' )
        
#        # Parameters
#        self.nb_deleted = nb_deleted
#        self.nb_selected = nb_selected
#        self.nb_round = nb_round
#        self.observation_threshold = observation_threshold
#        self.distance_method = distance_method
#        self.linkage_method = linkage_method
#        self.merge_groups = merge_groups
#        self.min_evidence_nb = 2
#        
#        # Files
#        self.input_bioms = InputFileList( biom )
#        self.output_fasta = list()
#        if fasta_files is not None:
#            self.input_fasta = InputFileList( fasta_files )
#            self.output_fasta = OutputFileList( self.get_outputs('{basename}', fasta_files) )
#        self.output_files = OutputFileList( self.get_outputs('{basename_woext}_norm.biom', self.input_bioms) )
#        self.depth_files = OutputFileList( self.get_outputs('{basename_woext}_norm_depth.tsv', self.input_bioms) )
#        self.hclust_files = OutputFileList( self.get_outputs('{basename_woext}_norm_hclust.json', self.input_bioms) )
#        self.stderr = os.path.join( self.output_directory, 'normalisation.stderr' )
        
    def get_template(self):
        return "ClustersStats"

    def define_analysis(self):
        self.name = "Sampling"
        self.description = "Filter and random sampling."
        self.software = "-"
        self.options = "sampling deleted=" + str(self.nb_deleted) + " selected=" + str(self.nb_selected) + " round=" + str(self.nb_round) + " obs_min=" + str(self.observation_threshold) +\
                       ";hierarchical_clustering distance=" + self.distance_method + " linkage=" + self.linkage_method
        if self.merge_groups != None:
            self.options = "replicate_filter min_evidence=" + str(self.min_evidence_nb) + ";" + self.options 

    def get_version(self):
        return "-"
  
    def post_process(self):
        self._save_files( self.output_files + self.depth_files + self.hclust_files + self.output_fasta )
        # Parse depths
        for filepath in self.depth_files:
            [depths, counts, nb_observations, nb_seq, upper_quartile, median, lower_quartile] = observations_depth_to_stat(filepath)
            sample_name = os.path.basename(filepath).split("_norm_depth.tsv")[0]
            self._add_result_element( sample_name, "depths", ",".join(map(str, depths)) )
            self._add_result_element( sample_name, "counts", ",".join(map(str, counts)) )
            self._add_result_element( sample_name, "nb_observations", str(nb_observations) )
            self._add_result_element( sample_name, "nb_sequences", str(nb_seq) )
            self._add_result_element( sample_name, "upper_quartile", str(upper_quartile) )
            self._add_result_element( sample_name, "median", str(median) )
            self._add_result_element( sample_name, "lower_quartile", str(lower_quartile) )
        # Parse JSON
        for filepath in self.hclust_files:
            json_fh = open( filepath )
            hierarchical_clustering = ' '.join( line.strip() for line in json_fh )
            json_fh.close()
            sample_name = os.path.basename(filepath).split("_norm_hclust.json")[0]
            self._add_result_element( sample_name, "linkage", hierarchical_clustering )
    
    def process(self): 
        # Merge samples
        tmp_files = self.input_bioms
        if self.merge_groups is not None:
            tmp_files = self.get_outputs( '{basename_woext}_tmp.biom', self.input_bioms )
            merge_dumps = self.get_outputs( '{basename_woext}.dump', self.input_bioms )
            for idx in range(len(self.merge_groups)):
                merge_dump_path = merge_dumps[idx]
                merge_dump = open( merge_dump_path, "wb" )
                pickle.dump( self.merge_groups[idx], merge_dump )
                merge_dump.close()
            merge = PythonFunction( merge_samples, cmd_format='{EXE} {IN} {OUT} ' + str(self.min_evidence_nb) + ' 2>> ' + self.stderr )
            MultiMap( merge, inputs=[self.input_bioms, merge_dumps], outputs=tmp_files )
        
        # Process filter and normalisation
        normalisation = PythonFunction( filter_and_bootstrap, cmd_format='{EXE} {IN} {OUT} ' + str(self.observation_threshold) + ' '  + str(self.nb_deleted) + ' ' + str(self.nb_selected) + ' ' + str(self.nb_round) + ' 2>> ' + self.stderr )
        Map( normalisation, inputs=tmp_files, outputs=self.output_files )
        
        # Update fasta
        if len(self.output_fasta) > 0:
            filter = PythonFunction( filter_seq, cmd_format='{EXE} {IN} {OUT} 2>> ' + self.stderr )
            filter = MultiMap( filter, inputs=[self.input_fasta, self.output_files], outputs=self.output_fasta )            
        
        # Depths stats
        depth = PythonFunction( observations_depth, cmd_format='{EXE} {IN} {OUT} 2>> ' + self.stderr )
        Map( depth, inputs=self.output_files, outputs=self.depth_files )
        
        # Linkage stats
        dendrogram = PythonFunction( samples_hclassification, cmd_format='{EXE} {IN} {OUT} ' + self.distance_method + ' ' + self.linkage_method + ' 2>> ' + self.stderr )
        Map( dendrogram, inputs=self.output_files, outputs=self.hclust_files )