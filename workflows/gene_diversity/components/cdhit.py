#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from subprocess import Popen, PIPE

from workflows.gene_diversity.lib.biomstat import *

from ng6.analysis import Analysis

from jflow.abstraction import MultiMap

from weaver.function import ShellFunction, PythonFunction
from weaver.abstraction import Map


def to_biom( output_biom, clusters_file):
    """
    @summary : Write a biom file from cdhit results.
    @param output_biom : [str] path to the output file.
    @param clusters_file : [str] path to the '.clstr' file.
    """
    from jflow.featureio import Biom, BiomIO
    precluster_size_sep = ';size='
    precluster_sample_sep = '|'
    samples_seen = dict()
    biom = Biom( generated_by='cdhit', matrix_type="sparse" )
    
    # Process count
    cluster_name = None
    for line in open(clusters_file).readlines():
        # New cluster
        if line.startswith(">"): # Line example : '>Cluster 0' => 'Cluster_0'
            if cluster_name is not None:
                # Add previous cluster on biom
                biom.add_observation( cluster_name )
                for sample_name in cluster_count:
                    if sample_name not in samples_seen:
                        biom.add_sample( sample_name )
                        samples_seen[sample_name] = 1
                    biom.add_count( cluster_name, sample_name, cluster_count[sample_name] )
            # Start new cluster
            cluster_name = line.strip()[1:].replace(" ", "_")
            cluster_count = dict()
        # Additional information on current cluster
        else: # Line example : '60    130aa, >c6104.0;1... at 99.23%'
            pre_cluster_id = line.strip().split()[2][1:-3] # c6104.0;1
            # Count
            pre_cluster_count = 1
            if precluster_size_sep != "none":
                pre_cluster_count = int( pre_cluster_id.split(precluster_size_sep)[-1] )
                pre_cluster_id = precluster_size_sep.join( pre_cluster_id.split(precluster_size_sep)[0:-1] ) # trim cluster size from ID
            # Sample
            pre_cluster_sample = "all"
            if precluster_sample_sep != "none":
                pre_cluster_sample = pre_cluster_id.split(precluster_sample_sep)[-1]
            # Store
            if pre_cluster_sample in cluster_count:
                cluster_count[pre_cluster_sample] += pre_cluster_count
            else:
                cluster_count[pre_cluster_sample] = pre_cluster_count
    if cluster_name is not None:
        # Add last cluster on biom
        biom.add_observation( cluster_name )
        for sample_name in cluster_count:
            if sample_name not in samples_seen:
                biom.add_sample( sample_name )
                samples_seen[sample_name] = 1
            biom.add_count( cluster_name, sample_name, cluster_count[sample_name] ) 
    # Write
    BiomIO.write( output_biom, biom )

def rename_seq( input_fasta, clusters_file, renamed_fasta ):
    """
     @summary: Write a renamed_fasta where the representative sequences ID will be replaced by the ID of the cluster.
      @param input_fasta : [str] path to the fasta to process.
      @param clusters_file : [str] path to the '.clstr'.
      @param renamed_fasta : [str] path to the fasta after process.
    """
    import jflow.seqio as seqio
    
    # Retrieve new names
    cluster_representative = dict()
    cluster_id = None
    clusters_fh = open( clusters_file )
    for line in clusters_fh:
        line = line.strip()
        if line.startswith('>'): # Line example : '>Cluster 0'
            cluster_id = line[1:].replace(" ", "_")
        if line.endswith('*'): # Line example : '0 2799aa, >PF04998.6|RPOC2_CHLRE/275-3073... *'
            representative_name = line.split()[2][1:-3]
            cluster_representative[representative_name] = cluster_id
    clusters_fh.close()
    
    # Rename sequences
    reader = seqio.FastaReader( input_fasta )
    out_fh = open( renamed_fasta, "w" )
    for id, desc, seq, qual in reader :
        seqio.writefasta( out_fh, [[cluster_representative[id], desc, seq, qual]] )


class Cdhit (Analysis):
    """
     @summary: Produces a set of representative sequences.
     After the clustering step some statistics are produced : 
         - Number of cluster by depth.
         - Hierarchical clustering between samples. This clustering is based on clusters's normalised expression.
    """
    
    def define_parameters(self, input_fasta, identity_threshold=0.95, length_diff_cutoff=0.8, cluster_most_similar=True, word_length=5, 
                          distance_method='euclidean', linkage_method='average'):
        """
         @param input_fasta : [str] fasta list to process
         @param identity_threshold : [float] sequence identity threshold. Calculated as : number of identical amino acids in alignment divided by the full length of the shorter sequence.
         @param length_diff_cutoff : [float] Maximum length difference between shorter end representative sequence of the cluster. If set to 0.9, the shorter sequences need to be at least 90% length of the representative of the cluster.
         @param cluster_most_similar : [bool] False => the sequence can be clustered to the first cluster that meet the threshold (fast cluster).
             True => the program will cluster it into the most similar cluster that meet the threshold (accurate but slow mode).
             Either won't change the representatives of final clusters.
         @param word_length : [int] word length.
         @param distance_method : [str] distance method for the hierarchical clustering. Accepted values @see biomstat.samples_hclassification.
         @param linkage_method : [str] linkage method for the hierarchical clustering. Accepted values @see biomstat.samples_hclassification.
        """
        
        self.add_parameter("identity_threshold", "sequence identity threshold. Calculated as : number of "+
                           "identical amino acids in alignment divided by the full length of the shorter sequence.", default=identity_threshold, type='float')
        self.add_parameter("length_diff_cutoff", "Maximum length difference between shorter end representative "+
                           "sequence of the cluster. If set to 0.9, the shorter sequences need to be at least 90% "+
                           "length of the representative of the cluster.", default=length_diff_cutoff, type='float')
        self.add_parameter("cluster_most_similar", """
            False => the sequence can be clustered to the first cluster that meet the threshold (fast cluster).
             True => the program will cluster it into the most similar cluster that meet the threshold (accurate but slow mode).
             Either won't change the representatives of final clusters.
        """, default=cluster_most_similar, type='bool')
        
        self.add_parameter("word_length", "word length.", default=word_length, type='int')
        self.add_parameter("distance_method", "distance method for the hierarchical clustering. Accepted values @see biomstat.samples_hclassification.", default=distance_method)
        self.add_parameter("linkage_method", "linkage method for the hierarchical clustering. Accepted values @see biomstat.samples_hclassification.", default=linkage_method)
        
        # Parameters
        self.cdhit_options = ""
        if self.cluster_most_similar:
            self.cdhit_options += " -g 1"
        else:
            self.cdhit_options += " -g 0"
        self.cdhit_options += " -c " + str(self.identity_threshold)
        self.cdhit_options += " -s " + str(self.length_diff_cutoff)
        self.cdhit_options += " -n " + str(self.word_length)
        
        # Files
        self.add_input_file_list( "input_fasta", "fasta list to process", default=input_fasta, required=True, file_format='fasta')
        self.add_output_file_list("output_files", "output_files", pattern='{basename_woext}_cdhit.fasta', items=self.input_fasta, file_format='fasta')
        self.add_output_file_list("cluster_files", "cluster_files", pattern='{basename_woext}.clstr', items=self.input_fasta)
        self.add_output_file_list("biom_files", "biom_files", pattern='{basename_woext}_cdhit.biom', items=self.input_fasta)
        self.add_output_file_list("depth_files", "depth_files", pattern='{basename_woext}_depth.tsv', items=self.input_fasta)
        self.add_output_file_list("hclust_files", "hclust_files", pattern='{basename_woext}_hclust.json', items=self.input_fasta)
        self.stderr = os.path.join(self.output_directory, 'cdhit.stderr')

    def get_template(self):
        return "ClustersStats"

    def define_analysis(self):
        self.name = "Cluster"
        self.description = "Cluster sequences."
        self.software = "Cd-Hit"
        self.options = "cdhit " + self.cdhit_options + ";hierarchical_clustering distance=" + self.distance_method + " linkage=" + self.linkage_method

    def get_version(self):
        cmd = [self.get_exec_path("cd-hit")]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stdout.split()[3]
  
    def post_process(self):
        self._save_files( self.biom_files + self.output_files + self.depth_files + self.hclust_files )
        # Parse depths
        for filepath in self.depth_files:
            [depths, counts, nb_observations, nb_seq, upper_quartile, median, lower_quartile] = observations_depth_to_stat(filepath)
            sample_name = os.path.basename(filepath).split("_depth.tsv")[0]
            self._add_result_element( sample_name, "depths", ",".join(map(str, depths)) )
            self._add_result_element( sample_name, "counts", ",".join(map(str, counts)) )
            self._add_result_element( sample_name, "nb_observations", str(nb_observations) )
            self._add_result_element( sample_name, "nb_sequences", str(nb_seq) )
            self._add_result_element( sample_name, "upper_quartile", str(upper_quartile) )
            self._add_result_element( sample_name, "median", str(median) )
            self._add_result_element( sample_name, "lower_quartile", str(lower_quartile) )
        # Parse JSON
        for filepath in self.hclust_files:
            json_fh = open( filepath )
            hierarchical_clustering = ' '.join( line.strip() for line in json_fh )
            json_fh.close()
            sample_name = os.path.basename(filepath).split("_hclust.json")[0]
            self._add_result_element( sample_name, "linkage", hierarchical_clustering )
    
    def process(self):
        tmp_fasta_files = self.get_outputs('{basename_woext}', self.input_fasta)
        
        # Build clusters
        cdhit = ShellFunction( self.get_exec_path("cd-hit") + " -i $1 -o $2 -d 0 " + self.cdhit_options + " 2>> " + self.stderr, cmd_format='{EXE} {IN} {OUT}' )
        cdhit = MultiMap( cdhit, inputs=self.input_fasta, outputs=[tmp_fasta_files, self.cluster_files] )
        
        # Rename cluster's sequences
        rename = PythonFunction( rename_seq, cmd_format='{EXE} {IN} {OUT} 2>> ' + self.stderr )
        rename = MultiMap( rename, inputs=[tmp_fasta_files, self.cluster_files], outputs=self.output_files )
        
        # Build biom
        biom = PythonFunction( to_biom, cmd_format='{EXE} {OUT} {IN} 2>> ' + self.stderr )
        biom = Map( biom, inputs=self.cluster_files, outputs=self.biom_files )
        
        # Depths stats
        depth = PythonFunction( observations_depth, cmd_format='{EXE} {IN} {OUT} 2>> ' + self.stderr )
        Map( depth, inputs=self.biom_files, outputs=self.depth_files )
        
        # Linkage stats
        dendrogram = PythonFunction( samples_hclassification, cmd_format='{EXE} {IN} {OUT} ' + self.distance_method + ' ' + self.linkage_method + ' 2>> ' + self.stderr )
        Map( dendrogram, inputs=self.biom_files, outputs=self.hclust_files )