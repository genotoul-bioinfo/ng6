#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.component import Component

from weaver.function import PythonFunction


def add_sample_name( sample_name, input_file, output_file ):
    """
     @summary : Adds the sample name to each sequence IDs.
      @param sample_name : [str] the sample name to add.
      @param input_file : [str] path to the fasta processed.
      @param output_file : [str] path to the output.
    """    
    import jflow.seqio as seqio
    sequence_name_sep = '|'
    sequence_count_sep = ';size='
    
    reader = seqio.SequenceReader(input_file)
    out_fh = open(output_file, "w")
    for id, desc, seq, qual in reader :
        new_id = id + sequence_name_sep + sample_name
        if sequence_count_sep != "none":
            split_id = id.split( sequence_count_sep )
            new_id = sequence_count_sep.join(split_id[0:-1]) + sequence_name_sep + sample_name + sequence_count_sep + split_id[-1]
            # patch
            if new_id.endswith(';'):
                new_id = new_id[:-1]
        seqio.writefasta( out_fh, [[new_id, desc, seq, qual]] )
    out_fh.close()

class AddSamplesNames (Component):
    """
     @summary : Adds the sample name to each sequence IDs.
    """
    
    def define_parameters(self, input_fasta, samples_names=None):
        """
         @param input_fasta : [list] fasta processed.
         @param samples_names : [list] the sample name for each input fasta.
        """
        self.add_input_file_list( "input_fasta", "fasta processed.", default=input_fasta, required=True, file_format='fasta' )
        self.add_output_file_list("output_files", "The BWA bam files.", pattern='{basename}', items=self.input_fasta, file_format="fasta")
        self.add_output_file("stderr", "stderr", filename='addSample.stderr')
        
        if samples_names == None:
            samples_names = list()
            for current_input in self.input_fasta:
                basename = os.path.basename(current_input)
                samples_names.append( ".".join(basename.split('.')[:-1]) )
        self.add_parameter_list("samples_names", "the sample name for each input fasta", default = samples_names)
        
    def process(self):
        # Rename files
        for file_idx in range( len(self.input_fasta) ):       
            rename = PythonFunction( add_sample_name, cmd_format='{EXE} ' + self.samples_names[file_idx] + ' {IN} {OUT} 2>> ' + self.stderr )
            rename( inputs=self.input_fasta[file_idx], outputs=self.output_files[file_idx] )