#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.abstraction import MultiMap

from weaver.function import ShellFunction, PythonFunction
from weaver.abstraction import Map

from ng6.analysis import Analysis

def biom_to_count( biom_file, count_file ):
    """
     @summary : Writes a count table with taxonomy from biom file.
      @param biom_file : [str] path to the biom to process.
      @param count_file : [str] path to the output file.
    """
    from jflow.featureio import BiomIO
    
    biom = BiomIO.from_json( biom_file )
    out_fh = open( count_file, "w" )
    obs_idx = -1
    for obs in biom.to_count_table():
        display = "\t".join(map(str, obs)) + "\t"
        if obs_idx == -1:
            display += "Evalue\tIdentity\tAln_length\tKingdom\tPhylum\tClass\tOrder\tFamily\tGenus\tSpecie"
        else:
            display += biom.rows[obs_idx]["metadata"]["evalue"] + "\t"
            display += biom.rows[obs_idx]["metadata"]["identity"] + "\t"
            display += biom.rows[obs_idx]["metadata"]["aln_length"] + "\t"
            display += "\t".join( biom.rows[obs_idx]["metadata"]["taxonomy"] )
            i = 1 
            while i <= (7 - len(biom.rows[obs_idx]["metadata"]["taxonomy"])):
                display += "\t"
                i += 1
        out_fh.write( display + "\n" )
        obs_idx += 1
    out_fh.close()

def biom_to_krona( exec_path, stem_path_krona, biom_file, log_file ):
    """
     @summary : Creates the krona charts for the taxonomic data of each samples in the biom file.
      @param exec_path : [str] path to the software ktImportText.
      @param stem_path_krona : [str] stem of the path for all krona charts.
                               The function creates one file by sample with this path : <stem_path_krona><sample_name>.html
      @param biom_file : [str] path to the biom to process.
      @param log_file : [str] path to the log file.
    """
    from jflow.featureio import BiomIO
    from subprocess import Popen, PIPE
    import sys
    
    biom = BiomIO.from_json( biom_file )
    log_fh = open( log_file, "wb" )
    for sample in biom.get_samples_names():
        krona_text_file = stem_path_krona + sample.replace( " ", "_") + ".text"
        krona_html_file = stem_path_krona + sample.replace( " ", "_") + ".html"
        BiomIO.write_krona_table_by_sample( krona_text_file, biom, sample )
        # Build krona
        cmd = [exec_path, "-n", "root", "-o", krona_html_file, krona_text_file]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        log_fh.write(stdout)
        # Write down the stderr
        sys.stderr.write(stderr.decode())
    log_fh.close()

def add_tax_metadata( biom_file, blast_file, taxonomy_file, output_file ):
    """
     @summary : Add taxonomy metadata on biom file from a blast result.
      @param biom_file : [string] the path to the Biom file to process.
      @param blast_file : [string] the path to the blast result in tabular format (outfmt 6 with NCBI Blast+).
      @param taxonomy_file : [string] the path to the taxonomy file.
      @param output_file : [string] the path to the output file.
    """
    import os
    from jflow.featureio import BiomIO
    
    def init_cluster_count(sample_names):
        samples_count = {}
        for sample in sample_names:
            samples_count[sample] = 0
        return samples_count
    
    # Build an hash with the taxonomy for each gene (key=gene_id ; value=gene_taxonomy)
    tax = {}
    for line in open(taxonomy_file).readlines():
        parts = line.strip().split("\t") # Line example : 'ADC62191    Bacteria; Proteobacteria; Gammaproteobacteria; Chromatiales; Chromatiaceae; Allochromatium.; Allochromatium vinosum DSM 180'
        tax[parts[0]] = parts[1]
    
    # Retrieve clusters annotations
    cluster_annot = dict()
    for line in open(blast_file).readlines():
        parts = line.strip().split()
        query_id = parts[0].split(";")[0]
        cluster_annot[query_id] = dict()
        subject_id = parts[1].split("#")[0]    # Subject field : <ID>#<PARTIAL_DESC>
        cluster_annot[query_id]['tax'] = list(map(str.strip, tax[subject_id].split(";")))
        cluster_annot[query_id]['evalue'] = parts[10]
        cluster_annot[query_id]['identity'] = parts[2]
        cluster_annot[query_id]['aln_length'] = parts[3]
    
    # Add metadata to biom
    biom = BiomIO.from_json( biom_file )
    for cluster in biom.rows:
        cluster_id = cluster['id']
        taxonomy   = [""]
        evalue     = ""
        identity   = ""
        aln_length = ""
        if cluster_id in cluster_annot:
            taxonomy   = cluster_annot[cluster_id]['tax']
            evalue     = cluster_annot[cluster_id]['evalue']
            identity   = cluster_annot[cluster_id]['identity']
            aln_length = cluster_annot[cluster_id]['aln_length']
        biom.add_metadata( cluster_id, "taxonomy", taxonomy, "observation")
        biom.add_metadata( cluster_id, "evalue", evalue, "observation")
        biom.add_metadata( cluster_id, "identity", identity, "observation")
        biom.add_metadata( cluster_id, "aln_length", aln_length, "observation")
    BiomIO.write( output_file, biom )


class GeneOTUClassify( Analysis ):
    
    def define_parameters(self, biom_files, input_fasta, taxonomy_file, databank, blast_used="blastp", evalue="1e-5"):
        """
         @param biom_files : [list] the Biom files with OTU to process.
         @param input_fasta : [list] the fasta file containing the sequences of OTUs. Each biom file correspond to one input fasta.
         @param taxonomy_file : [str] path to the taxonomy files.
         @param databank : [str] path to the databank used for retrieve taxonomy by best hit search.
         @param blast_used : [str] the type of blast used ("blastx", "blastp", ...).
         @param evalue : [str] the maximum e-value to keep the best hit in taxonomy step.
        """
        # Files
        self.add_input_file_list( "biom_files", "the Biom files with OTU to process.", default=biom_files, required=True )
        self.add_input_file_list( "input_fasta", "the fasta file containing the sequences of OTUs. Each biom file correspond to one input fasta.", default=input_fasta, required=True )
        self.add_input_file("taxonomy_file", " path to the taxonomy files.", default=taxonomy_file, required=True)
        self.add_output_file_list("output_files", "output_files.", pattern='{basename}', items=self.biom_files)
        self.add_output_file_list("count_tax", "count_tax.", pattern='{basename_woext}.csv', items=self.biom_files)
        self.add_output_file_list("krona_log_files", "krona_log_files.", pattern='{basename_woext}_krona.log', items=self.biom_files)
        self.stderr = os.path.join(self.output_directory, 'geneOTUclassify.stderr')
        
        # Parameters
        self.add_parameter("blast_used", 'the type of blast used ("blastx", "blastp", ...).', default=blast_used)
        self.add_parameter("databank", 'path to the databank used for retrieve taxonomy by best hit search.', default=databank, required = True)
        self.add_parameter("evalue", 'the maximum e-value to keep the best hit in taxonomy step.', default=evalue)
        
        self.blast_options = " -max_target_seqs 1 -evalue " + str(self.evalue) + " -outfmt 6"
        self.biom_basename_woext = [os.path.basename(elt) for elt in self.get_outputs('{basename_woext}_', self.biom_files)]
        self.stem_path_krona = self.get_outputs('{basename_woext}_', self.biom_files) # <output_dir>/<biom>_<sample>.html

    def define_analysis(self):
        self.name = "GeneOTUClassify"
        self.description = "Operational Taxon Unit analysis."
        self.software = "NCBI Blast+"
        self.options = self.blast_used + self.blast_options
    
    def get_version(self):
        return "-"
        
    def post_process(self):
        self._save_files( self.output_files + self.count_tax )
        
        # Add the krona files
        for krona_file in os.listdir( self.output_directory ):
            self.biom_basename_woext.sort(key = lambda s: len(s))
            for current_biom in reversed(self.biom_basename_woext):
                if krona_file.startswith( current_biom ) and krona_file.endswith( ".html" ):
                    sample = krona_file.split( current_biom )[1][:-5] # krona_file = <current_biom><sample>.html
                    fullpath = os.path.join( self.output_directory, krona_file )
                    self._add_result_element( sample, "html", self._save_file( fullpath ), current_biom[:-1] )
                    break
      
    def process(self):
        blast_files = self.get_outputs( '{basename_woext}_blast.tsv', self.biom_files )
        
        # Blast
        blast = ShellFunction( self.get_exec_path(self.blast_used) + " " + self.blast_options + " -query $1 -out $2 -db " + self.databank + " 2>> " + self.stderr, cmd_format='{EXE} {IN} {OUT}' )
        Map( blast, inputs=self.input_fasta, outputs=blast_files, includes=self.databank )
        
        # Process metadata
        metadata = PythonFunction( add_tax_metadata, cmd_format='{EXE} {IN} ' + self.taxonomy_file + ' {OUT} 2>> ' + self.stderr )
        MultiMap( metadata, inputs=[self.biom_files, blast_files], outputs=self.output_files, includes=self.taxonomy_file )
        
        # Biom to krona
        krona_data_files = self.get_outputs( '{basename_woext}_krona.tsv', self.biom_files )
        for idx in range(len(self.biom_files)):
            krona = PythonFunction( biom_to_krona, cmd_format='{EXE} ' + self.get_exec_path("ktImportText") + ' ' + self.stem_path_krona[idx] + ' {IN} {OUT} 2>> ' + self.stderr )
            krona( inputs=self.output_files[idx], outputs=self.krona_log_files[idx] )
            
        # Biom to count_tax
        biom2count = PythonFunction( biom_to_count, cmd_format='{EXE} {IN} {OUT} 2>> ' + self.stderr )
        Map( biom2count, inputs=self.output_files, outputs=self.count_tax )