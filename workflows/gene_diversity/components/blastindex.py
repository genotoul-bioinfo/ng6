#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.component import Component

from weaver.function import PythonFunction


def blast_index(exec_path, databank_type, input_fasta, databank, stdout_path, stderr_path):
    """
     @param exec_path : path to makeblasdb software.
     @param databank_type : molecule type of target db ('nucl' or 'prot').
     @param input_fasta : databank fasta file.
     @param databank : name of BLAST database to be created.
     @param stdout_path : path to stdout.
     @param stderr_path : path to stderr.
    """
    from subprocess import Popen, PIPE
    # first make the symbolic link
    os.symlink(input_fasta, databank)
    # then execute blast index
    cmd = [exec_path, "-dbtype", databank_type, "-in", databank, "-out", databank]
    p = Popen(cmd, stdout=PIPE, stderr=PIPE)
    stdout, stderr = p.communicate()
    # write down the stdout
    stdoh = open(stdout_path, "wb")
    stdoh.write(stdout)
    stdoh.close()
    # write down the stderr
    stdeh = open(stderr_path, "wb")
    stdeh.write(stderr)
    stdeh.close()
        
class BlastIndex (Component):
    """
     @summary: Build index for NCBI Blast+.
    """
    
    def define_parameters(self, input_fasta, databank_type):
        """
         @param input_fasta : databank fasta file.
         @param databank_type : molecule type of target db ('nucl' or 'prot').
        """
        self.add_input_file('input_fasta', 'databank fasta file', file_format="fasta", default=input_fasta)
        self.add_parameter("databank_type", " molecule type of target db ('nucl' or 'prot').", choices=['nucl','prot'], default=databank_type, required=True)
        self.add_output_file('databank', 'databank', filename=os.path.basename(self.input_fasta))
        self.add_output_file('stdout', 'stdout', filename="blastindex.stdout")
        self.add_output_file('stderr', 'stderr', filename="blastindex.stderr")
        
    def process(self):        
        blastindex = PythonFunction(blast_index, cmd_format="{EXE} {ARG} {IN} {OUT}")
        blastindex(inputs=self.input_fasta, outputs=[self.databank, self.stdout, self.stderr], arguments=[self.get_exec_path("makeblastdb"), self.databank_type])