#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.component import Component

from weaver.function import ShellFunction


class Framebot (Component):
    """
     @summary: frameshift correction and nearest neighbor classification tool for use with high-throughput amplicon sequencing.
     It uses a dynamic programming algorithm to align each query DNA sequence against a set of target protein sequences, produces 
     frameshift-corrected protein and DNA sequences and an optimal global or local protein alignment.
    """
    
    def define_parameters(self, input_fasta, bank, length_cutoff=80, is_index=True):
        """
         @param input_fasta : [list] files to align.
         @param bank : [string] databank framebot index or fasta databank.
         @param length_cutoff : [int] length cutoff in number of amino acids.
         @param is_index : [bool] true if bank is a framebot index. False if bank is a fasta databank.
        """
        self.add_input_file_list( "input_fasta", "files to align.", default=input_fasta, required=True, file_format='fasta' )
        self.add_input_file("bank", "databank framebot index or fasta databank.", default=bank, required=True)
        self.add_parameter("length_cutoff", "length cutoff in number of amino acids.", default=length_cutoff, type='int')
        self.add_parameter("is_index", "true if bank is a framebot index. False if bank is a fasta databank.", default=is_index, type='boll')
        
        # Parameters
        self.options = " -l " + str(self.length_cutoff)
        if self.is_index == False:
            self.options += " -N "
        
        # Files
        self.stem_name = list()
        for file_path in self.input_fasta:
            stem = os.path.basename(file_path).split('.')[0]
            stem_with_path = os.path.join( self.output_directory, stem )
            self.stem_name.append( stem_with_path )
        
        self.add_output_file_list("ok_aln", "ok_aln", pattern='{fullpath}_framebot.txt', items=self.stem_name)
        self.add_output_file_list("discarded_aln", "discarded_aln", pattern='{fullpath}_failed_framebot.txt', items=self.stem_name)
        self.add_output_file_list("corrected_nucleotids", "corrected_nucleotids", pattern='{fullpath}_corr_nucl.fasta', items=self.stem_name, file_format='fasta')
        self.add_output_file_list("failed_nucleotids", "failed_nucleotids", pattern='{fullpath}_failed_nucl.fasta', items=self.stem_name, file_format='fasta')
        self.add_output_file_list("corrected_proteins", "corrected_proteins", pattern='{fullpath}_corr_prot.fasta', items=self.stem_name, file_format='fasta')
        self.add_output_file_list("stderr", "stderr", pattern='{basename_woext}.stderr', items=self.stem_name)
       
    def process(self):
        for idx in range(len(self.input_fasta)):
            framebot = ShellFunction( "java -Xmx4g -jar " + self.get_exec_path("framebot") + " framebot " + self.options + " -o $1 " + self.bank + " $2 2> $3", cmd_format='{EXE} {ARG} {IN} {OUT}' )
            framebot( arguments=self.stem_name[idx], 
                      inputs=self.input_fasta[idx], 
                      outputs=[self.stderr[idx], self.ok_aln[idx], self.discarded_aln[idx], self.corrected_nucleotids[idx], self.failed_nucleotids[idx], self.corrected_proteins[idx]], 
                      includes=self.bank )