#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.component import Component

from weaver.function import ShellFunction


class FramebotIndex (Component):
    """
     @summary: Build an index for Framebot.
    """
    
    def define_parameters(self, bank):
        """
         @param bank : Reference fasta file.
        """
        self.add_input_file("bank", "Reference fasta file.", default=bank, required=True)
        self.add_output_file("index", "index", filename = os.path.basename(self.bank) + ".index" )
        self.add_output_file("stderr", "stderr", filename = "framebotindex.stderr")

    def process(self):
        framebotIndex = ShellFunction( "java -Xmx4g -jar " + self.get_exec_path("framebot") + " index $1 $2 2> $3", cmd_format='{EXE} {IN} {OUT}' )
        framebotIndex(inputs=self.bank, outputs=[self.index, self.stderr])