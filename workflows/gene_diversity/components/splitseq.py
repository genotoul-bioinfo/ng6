#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

import jflow.seqio as seqio

from jflow.component import Component

from weaver.function import PythonFunction
from weaver.abstraction import Map


def split_seq (sequences_file, outdir, nb_seq_by_file):
    """
    @summary : Split a sequences file in sequences files with 'nb_seq_by_file' sequences per file.
      @param sequences_file : file before cut.
      @param outdir: output directory.
      @param nb_seq_by_file: number of sequences per file.
    """
    import os
    import jflow.seqio as seqio
    
    basename = os.path.basename(sequences_file)
    basename_woext = basename.split(".")[0]
    extensions = ".".join( basename.split(".")[1:] )
    
    current_nb_seq_on_file = 0
    current_file_id = 0
    out_fh = None
    # For each sequence
    reader = seqio.SequenceReader( sequences_file )
    for id, desc, seq, qual in reader:
        current_nb_seq_on_file += 1
        # If current file is complete
        if int(current_nb_seq_on_file) > int(nb_seq_by_file) or out_fh is None:
            # Close current file
            if out_fh is not None:
                out_fh.close()
            # Next output file
            current_nb_seq_on_file = 1
            current_file_id += 1
            current_file_path = os.path.join(outdir, basename_woext + '_' + str(current_file_id) + '.' + extensions)
            if sequences_file.endswith(".gz"):   
                out_fh = seqio.xopen( current_file_path, "w" )
            else:
                out_fh = open( current_file_path, "w" )
        # If sequences file is a FASTQ
        if reader.__class__.__name__ == "FastqReader":          
            seqio.writefastq(out_fh, [[id, desc, seq, qual]])
        # If sequences file is a FASTA   
        elif reader.__class__.__name__ == "FastaReader":
            seqio.writefasta(out_fh, [[id, desc, seq, qual]])
    out_fh.close()
    

class SplitSeq (Component):
    """
     @summary : Split a sequences file in sequences files with 'nb_seq_by_file' sequences by file.
    """

    def define_parameters(self, sequences_files, nb_seq_by_file=200):
        """
         @param sequences_files : [list] files before cut.
         @param nb_seq_by_file : number of sequences per file before cut.
        """
        self.add_parameter("nb_seq_by_file", "number of sequences per file before cut.", default=nb_seq_by_file, type='int')
        self.add_input_file_list( "input_files", "files before cut..", default=sequences_files, required=True)
        self.add_output_file_list("stderr", "stderr", pattern='{basename_woext}.stderr', items=self.input_files)
        self.add_output_file_endswith("output_files", "output_files", pattern=".stderr", behaviour="exclude", file_format=self.input_files.file_format)
      
    def process(self):
        split = PythonFunction( split_seq, cmd_format='{EXE} {IN} ' + self.output_directory + ' ' + str(self.nb_seq_by_file) + ' 2> {OUT}' )
        Map(split, inputs=self.input_files, outputs=self.stderr)