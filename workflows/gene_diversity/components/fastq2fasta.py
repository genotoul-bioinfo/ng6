#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.component import Component
  
from weaver.function import PythonFunction
from weaver.abstraction import Map
  
  
def fastq_2_fasta( fastq_file, fasta_file ):
    """
     @summary: Transforms a FASTQ file into a FASTA file
      @param fastq_file : file to transform.
      @param fasta_file : result of the transformation.     
    """
    import jflow.seqio as seqio
  
    out_fh = open( fasta_file, "w" )
    reader = seqio.SequenceReader( fastq_file )
    for id, desc, seq, qual in reader:
        seqio.writefasta(out_fh, [[id, desc, seq, qual]])


class Fastq2fasta (Component):
    """
     @summary: Transforms FASTQ files into FASTA files
    """
    
    def define_parameters(self, files_list):
        """
          @param files_list : path of files that will be processed.
        """
        self.add_input_file_list( "files_list", "path of files that will be processed.", default=files_list, required=True, file_format='fastq' )
        self.add_output_file_list("output_files", "output_files", pattern='{basename_woext}.fasta', items=self.files_list, file_format='fasta')

    def process(self):
        fastq2fasta = PythonFunction( fastq_2_fasta, cmd_format='{EXE} {IN} {OUT}' )
        Map(fastq2fasta, inputs=self.files_list, outputs=self.output_files)