#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

def observations_depth( input_biom, output_depth ):
    """
    @summary : Write the depths of the observation in file.
    @param input_biom : [str] path to the biom file processed .
    @param output_depth : [str] path to the output file. 
    @note : Example of one output file 
                #Depth<TAB>Nb_Observ_concerned<TAB>Prct_Observ_concerned
                1<TAB>65<TAB>65.000
                2<TAB>30<TAB>30.000
                3<TAB>0<TAB>0.000
                4<TAB>5<TAB>5.000
    """
    from jflow.featureio import BiomIO
    
    obs_depth = list()
    nb_observ = 0
    # Process depth calculation
    biom = BiomIO.from_json( input_biom )
    for observation_id, observation_count in biom.observations_counts():
        while len(obs_depth) <= observation_count:
            obs_depth.append(0)
        obs_depth[observation_count] += 1
        if observation_count != 0:
            nb_observ += 1
    del biom
    # Write output
    out_fh = open( output_depth, 'w' )
    out_fh.write( "#Depth\tNb_Observ_concerned\tPrct_Observ_concerned\n" )
    for depth in range(1, len(obs_depth)):
        prct = (float(obs_depth[depth])/ nb_observ)*100
        out_fh.write( str(depth) + "\t" + str(obs_depth[depth]) + "\t" + ("%.3f" % prct) + "\n" )
    out_fh.close()
 
def samples_hclassification( input_biom, output_dendrogram, distance_method, linkage_method ):
    """
    @todo : test
    """
    import json
    from scipy.spatial.distance import pdist, squareform
    from scipy.cluster.hierarchy import linkage, dendrogram
    import scipy.cluster.hierarchy
    from jflow.featureio import BiomIO
    from functools import reduce
        
    def add_node( node, parent ):
        """
        @summary : Create a nested dictionary from the ClusterNode's returned by SciPy.
        """
        # First create the new node and append it to its parent's children
        new_node = dict( node_id=node.id, children=[], dist=node.dist )
        parent["children"].append( new_node )
        # Recursively add the current node's children
        if node.left: add_node( node.left, new_node )
        if node.right: add_node( node.right, new_node )
         
    def label_tree( node, id_2_name ):
        """
        @summary : Label each node with the names of each leaf in its subtree.
        """
        # If the node is a leaf
        if len(node["children"]) == 0:
            leaf_names = [ id_2_name[node["node_id"]] ]
        # If not, flatten all the leaves in the node's subtree
        else:
            leaf_names = reduce(lambda ls, c: ls + label_tree(c, id_2_name), node["children"], [])
        # Labeling
        if len(leaf_names) == 1:
            node["name"] = str(leaf_names[0])
        else:
            node["name"] = "%.3f" % float(node["dist"])
        # Delete useless information
        del node["node_id"]
        del node["dist"]
        return leaf_names
     
    data_array = list()
    samples_names = list()
     
    # Normalisation on count by sample
    biom = BiomIO.from_json( input_biom )
    for col_idx, current_sample in enumerate(biom.columns):
        samples_names.append( current_sample['id'] )
        sum_on_sample = biom.data.get_col_sum( col_idx )
        OTUs_norm = list()
        for row_idx in range(len(biom.rows)):
            OTUs_norm.append( biom.data.nb_at(row_idx, col_idx)/float(sum_on_sample) )
        data_array.append( OTUs_norm )
    del biom
    
    if len(samples_names) == 1 :
        # Write JSON
        out_tree = dict( children=[], name=samples_names[0], dist="0.000" )
        json.dump( out_tree, open(output_dendrogram, "w"), sort_keys=True, indent=4 )
    else:
        # Computing the distance and linkage
        data_dist = pdist( data_array, distance_method )
        data_link = linkage( data_dist, linkage_method )
         
        # Write JSON
        scipy_hc_tree = scipy.cluster.hierarchy.to_tree( data_link , rd=False )
        id_2_name = dict( list(zip(list(range(len(samples_names))), samples_names)) )
        out_tree = dict( children=[], name="", dist=None )
        add_node( scipy_hc_tree, out_tree )
        label_tree( out_tree["children"][0], id_2_name )
        json.dump( out_tree, open(output_dendrogram, "w"), sort_keys=True, indent=4 )
 
def observations_depth_to_stat( file ):
    """
    @todo : test
    """
    import numpy as np
    
    def count_to_list( values, counts ):
        """
        @summary : Builds a sorted list where each element represent the depth of one cluster.
        @param values : [list] the depths. Each element is linked with the element at the same index in counts.
        @param counts : [list] the number of clusters concerned by a depth. Each element is linked with the element at the same index in values.
        @example : 
                   values : [1, 2, 3, 10, 25]
                   counts : [9, 5, 3, 1, 1]
                   results : [1,1,1,1,1,1,1,1,1,2,2,2,2,2,3,3,3,10,25]
        """
        final_list = list()
        for i in range(len(values)):
            for count in range(counts[i]):
                final_list.append( values[i] )
        final_list.sort()
        return final_list

    # Retrieve count by depth
    depths = list()
    counts = list()
    nb_observations = 0
    nb_seq = 0
    depth_fh = open( file )
    for line in depth_fh:
        if not line.startswith('#'):
            line_fields = line.strip().split()
            if int(line_fields[1]) > 0:
                depths.append( int(line_fields[0]) )
                counts.append( int(line_fields[1]) )
                nb_observations += int(line_fields[1])
                nb_seq += int(line_fields[1]) * int(line_fields[0])
    depth_fh.close()
    # Process dispersion statistics
    list_depths = count_to_list( depths, counts )
    upper_quartile = np.percentile( list_depths, 75 )
    median = np.percentile( list_depths, 50 )
    lower_quartile = np.percentile( list_depths, 25 )
     
    return depths, counts, nb_observations, nb_seq, upper_quartile, median, lower_quartile