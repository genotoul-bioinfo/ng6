#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import datetime
import os
import pwd
import re

from ng6.t3MySQLdb import t3MySQLdb

from jflow.config_reader import JFlowConfigReader
from ng6.config_reader import NG6ConfigReader

def date(datestr):
    try:
        return datetime.datetime.strptime(datestr, JFlowConfigReader().get_date_format())
    except:
        raise argparse.ArgumentTypeError("'" + datestr + "' is an invalid date!")

def casavadir (dir):
    if os.path.isdir(dir) & os.path.isfile(os.path.join(dir,"SampleSheet.mk")):
        return os.path.abspath(dir)
    else:
        raise argparse.ArgumentTypeError("Local directory'" + dir + "'does not exists or it's a wrong casava directory structure! Please provide a valid directory path!")

def localdirectory(dir):
    if not os.path.isdir(dir):
        raise argparse.ArgumentTypeError("The local directory'" + dir + "'does not exists !")
    return dir

def ng6adminlogin(login):
    t3mysql = t3MySQLdb()
    if not t3mysql.is_ng6admin(login) :
        raise argparse.ArgumentTypeError("Login '" + login + "' is not a ng6 administrator! Please provide a valid login!")
    return login

def ng6userlogin(login):
    t3mysql = t3MySQLdb()
    try:
        t3mysql.get_user_id(login)
        return login
    except:
        raise argparse.ArgumentTypeError("Login '" + login + "' does not exists! Please provide a valid login!")

def localuser(username):
    try:
        pwd.getpwnam(username)
        return username
    except :
        raise argparse.ArgumentTypeError("The username '" + username + "' does not exists in localhost")


def uniqproject(name):
    t3mysql = t3MySQLdb()
    if t3mysql.project_exists(name) :
        raise argparse.ArgumentTypeError("The project name '" + name + "' exists already")
    return name

def existingproject(name):
    try:
        t3mysql = t3MySQLdb()
        [uid, description, space] = t3mysql.select_project_from_name(name)
        if uid :
            return name
        raise Exception
    except :
        raise argparse.ArgumentTypeError("The project name '%s' does not exists" % name)

def existingprojectid(id):
    try:
        t3mysql = t3MySQLdb()
        [name, description, space_id] = t3mysql.select_project(id)
        if name :
            return id
        raise Exception
    except :
        raise argparse.ArgumentTypeError("The project id '%s' does not exists" % id)
        
def ng6space(val):
    try :
        ng6conf = NG6ConfigReader()
        ng6conf.get_space_directory(val)
    except :
        raise argparse.ArgumentTypeError("The space name '%s' is not define in application.properties" % val)
    return val
    
def existingrun(id):
    try:
        t3mysql = t3MySQLdb()
        t3mysql.select_run(id)
        return id
    except :
        raise argparse.ArgumentTypeError("The run associated with id '%s' does not exists" % id)


def existinganalysis(id):
    try:
        t3mysql = t3MySQLdb()
        t3mysql.select_analysis(id)
        return id
    except :
        raise argparse.ArgumentTypeError("The analysis associated with id '%s' does not exists" % id)

def password(val):
    return val

def email(val):
    if not re.match( r'[^@]+@[^@]+\.[^@]+', val) :
        raise argparse.ArgumentTypeError("Invalid email '%s' " % val)

    return val

def nospacestr(val):
    if re.search(r"\s+", val):
        raise argparse.ArgumentTypeError("The string value '%s' must not contain spaces" % val)
    return val

def samplemetadata(metadata):
    key, val =  metadata.split(':', 2)
    if not (key and val) :
        raise argparse.ArgumentTypeError("invalid sample metadata format '%d'. Format must be key:value" % metadata)
    return metadata
