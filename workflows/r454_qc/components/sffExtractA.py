#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from subprocess import Popen, PIPE

from jflow.abstraction import MultiMap
from ng6.analysis import Analysis

from weaver.function import ShellFunction

from workflows.formats import fastq, sff

class SFFExtractA (Analysis):
    
    def define_parameters(self, input_files, archive_name=None):
        self.add_input_file_list( "input_files", "input sff files", default=input_files, required=True, file_format = sff)
        self.add_parameter("archive_name", "Archive name", default=archive_name)
        self.add_output_file_list( "fastq_files", "Outputed fastq files.", pattern='{basename_woext}.fastq', items=self.input_files, file_format='fastq')
        self.add_output_file_list( "stdouts", "SFFExtractA stdout files.", pattern='{basename_woext}.stdout', items=self.input_files)
        
    def define_analysis(self):
        self.name = "Convert"
        self.description = "Convert sff file in fastq format."
        self.software = "sff_extract.py"
        self.options = "-c"
        
    def post_process(self):
        import re
        extract_regex = re.compile("Converted (.*) reads into (.*) sequences.")
        clips_regexp = re.compile("After applying left clips, (.*) sequences \(=(.*)%\) start with these bases:")
        for stdout in self.stdouts:
            found = False
            nb_reads, nb_seq, cliped, perc_cliped, seq_cliped = 0, 0, 0, 0, ""
            for line in open(stdout).readlines():
                mr = extract_regex.match(line)
                cr = clips_regexp.match(line)
                if found:
                    seq_cliped = line.strip()
                    found = False
                if mr != None:
                    nb_reads, nb_seq = mr.group(1), mr.group(2)
                if cr != None:
                    cliped, perc_cliped = cr.group(1), cr.group(2)
                    found = True
            self._add_result_element(os.path.basename(stdout), "nb_reads", nb_reads)
            self._add_result_element(os.path.basename(stdout), "nb_seq", nb_seq)
            self._add_result_element(os.path.basename(stdout), "cliped", cliped)
            self._add_result_element(os.path.basename(stdout), "perc_cliped", perc_cliped)
            self._add_result_element(os.path.basename(stdout), "seq_cliped", seq_cliped)
        self._create_and_archive(self.fastq_files, self.archive_name)
        
    def get_version(self):
        cmd = [self.get_exec_path("sff_extract.py"), "--version"]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stdout.split()[1]
    
    def process(self):
        sff_extract = ShellFunction(self.get_exec_path("sff_extract.py") + " -c $1 -s $2 > $3", cmd_format='{EXE} {IN} {OUT}')
        sff_extract = MultiMap(sff_extract, self.input_files, [self.fastq_files, self.stdouts])
