#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import re
from subprocess import Popen, PIPE
import numpy as np

from jflow.abstraction import MultiMap
from ng6.analysis import Analysis

from weaver.function import ShellFunction
from weaver.abstraction import Map

from workflows.formats import sff
from workflows.r454_qc.lib import ace

class RunAssembly (Analysis):
    
    def define_parameters(self, input_files, large=False, ace=True, cdna=False, iterate=True, archive_name=None):
        self.add_input_file_list( "input_files", "input sff files", default=input_files, required=True, file_format = sff)
        self.add_parameter("large", "large", default=large, type = bool)
        self.add_parameter("ace", "ace", default=ace, type = bool)
        self.add_parameter("cdna", "cdna", default=cdna, type = bool)
        self.add_parameter("iterate", "iterate", default=iterate, type = bool)
        self.add_parameter("archive_name", "archive_name", default=archive_name)
        
        ace_ext = '.454Contigs.ace'
        large_contig_fasta_ext =  '.454LargeContigs.fna'
        large_contig_qual_ext = '.454LargeContigs.qual'
        if self.cdna:
            ace_ext = '.454Isotigs.ace'
            large_contig_fasta_ext =  '.454Isotigs.fna'
            large_contig_qual_ext = '.454Isotigs.qual'

        self.add_output_file_list( "output_dirs", "output_dirs", pattern='{basename_woext}', items=self.input_files)
        self.add_output_file_list( "all_contig_fasta_files", "all_contig_fasta_files", pattern='{basename_woext}/{basename_woext}.454AllContigs.fna', items=self.input_files)
        self.add_output_file_list( "all_contig_qual_files", "all_contig_qual_files", pattern='{basename_woext}/{basename_woext}.454AllContigs.qual', items=self.input_files)
        self.add_output_file_list( "metrics_files", "metrics_files", pattern='{basename_woext}/{basename_woext}.454NewblerMetrics.txt', items=self.input_files)
        self.add_output_file_list( "ace_files", "ace_files", pattern='{basename_woext}/{basename_woext}'+ace_ext, items=self.input_files)
        self.add_output_file_list( "large_contig_fasta_files", "large_contig_fasta_files", pattern='{basename_woext}/{basename_woext}'+large_contig_fasta_ext, items=self.input_files)
        self.add_output_file_list( "large_contig_qual_files", "large_contig_qual_files", pattern='{basename_woext}/{basename_woext}'+large_contig_qual_ext, items=self.input_files)
        self.add_output_file_list( "stdouts", "stdouts", pattern='{basename_woext}.stdout', items=self.input_files)
        
    def define_analysis(self):
        self.name = "RunAssembly"
        self.description = "De novo assembly."
        self.software = "RunAssembly"
        self.options = "-force "
        if self.large: self.options += "-large "
        if self.cdna: self.options += "-cdna "
        if self.ace: self.options += "-ace "
        
    def __parse_ace_file (self, ace_file):
        """
        Parse the newbler ace file
          @param ace_file : the ace file path
          @return         : {"contigs": {...}, ...}
        """
        ace_iterator = ace.parse(open(ace_file, 'r'))
        contig_len = []
        prof = []
        
        #Collect values
        for contig in ace_iterator:
            read_sum = 0
            # For each read
            for i in range(len(contig.reads)):
                # Compute the read length involved in the contig
                read_within = 0
                read_length = len(contig.reads[i].rd.sequence)
                # If the read starts after the contig start or right at the start
                if contig.af[i].padded_start > 0:
                    read_within = min( read_length, (contig.nbases - contig.af[i].padded_start + 1) )
                # Otherwise
                else:
                    read_within = min( (read_length + contig.af[i].padded_start - 1), contig.nbases )
                # Add the length to the length sum
                read_sum += read_within
            # Append the length to the table
            contig_len.append(contig.nbases)
            # And the calculated depth
            prof.append(read_sum/contig.nbases)
        
        #Set statistical values
        min_length    = "/"
        max_length    = "/"
        median_length = "/"
        mean_length   = "/"
        min_depth     = "/"
        max_depth     = "/"
        median_depth  = "/"
        mean_depth    = "/"
        
        if len(contig_len)!= 0:
            min_length    = np.min(contig_len)
            max_length    = np.max(contig_len)
            median_length = round(np.median(contig_len),2)
            mean_length   = round(np.mean(contig_len),2)
            min_depth     = np.min(prof)
            max_depth     = np.max(prof)
            median_depth  = round(np.median(prof),2)
            mean_depth    = round(np.mean(prof),2)
        
        #Retrieve the number of reads and contigs
        ace_handle = open(ace_file, 'r')
        ace_header = re.split('\s+', ace_handle.readline())
        ace_handle.close() 
        
        return {"contigs": {"nbcontigs": ace_header[1], "nbreads": ace_header[2], "minlength": min_length, 
                            "maxlength": max_length, "medlength": median_length, "meanlength": mean_length},
                "depth": {"mindepth": min_depth, "maxdepth": max_depth, "meddepth": median_depth,
                          "meandepth": mean_depth}}


    def __parse_metrics_file (self, metrics_file):
        """
        Parse the newbler metrics file
          @param metrics_file : the metrics file path
          @return             : {"data": {...}, ...}
        """
        metrics = {}
        handle = iter(open(metrics_file, 'r'))
        line = next(handle)
        while True:
            try :
                line = next(handle)
            except :
                break
            # If this is the runData section
            if line.startswith('runData'):
                metrics["data"] = {}
                while not(line.startswith("\t}")):
                    line = next(handle)
                    if line.startswith("\t\tpath ="):
                        m = re.search("\t\tpath = \".*/(.*)\";", line)
                        metrics["data"][m.group(1)] = {}
                        while True:
                            line = next(handle)
                            # Define all regexp we want to catch
                            rr = re.search(".*numberOfReads.*= (\d+), (\d+);", line)
                            br = re.search(".*numberOfBases.*= (\d+), (\d+);", line)
                            # Then fill the parsing result table 
                            if rr != None:
                                metrics["data"][m.group(1)]["numberOfReads"] = [rr.group(1), rr.group(2)]
                            if br != None:
                                metrics["data"][m.group(1)]["numberOfBases"] = [br.group(1), br.group(2)]
                            if line.startswith("\t}"):
                                break
            
            # If this is the consensus section
            if line.startswith("consensusResults"):
                while True:
                    try :
                        line = next(handle)
                    except :
                        break
                    
                    # If this is the readstatus subsection
                    if line.startswith("\treadStatus"):
                        line = next(handle)
                        metrics["readStatus"] = {}
                        while True :
                            line = next(handle)
                            # Define all regexp we want to catch
                            rr = re.search("\s*([^\s]+)\s*= (.*);", line)
                            if rr != None:
                                key = rr.group(1)
                                values = rr.group(2).split(", ")
                                two_values_fields = ["numAlignedReads", "numAlignedBases", "inferredReadError"]
                                one_value_fields = ["numberAssembled", "numberPartial", "numberSingleton", "numberRepeat", "numberOutlier", "numberTooShort"]    
                                
                                # Then fill the parsing result table                       
                                if key in two_values_fields:
                                    metrics["readStatus"][key] = [values[0], values[1]]
                                if key in one_value_fields:
                                    metrics["readStatus"][key] = values[0]
                            if line.startswith("\t}"):
                                break

                    # If this is the isogroupMetrics subsection
                    if line.startswith("\tisogroupMetrics"):
                        line = next(handle)
                        metrics["isogroupMetrics"] = {}
                        while True :
                            line = next(handle)
                            # Define all regexp we want to catch
                            nir = re.search(".*numberOfIsogroups.*= (\d+);", line)
                            accr = re.search(".*avgContigCnt.*= (.*);", line)                        
                            lcr = re.search(".*largestContigCnt.*= (\d+);", line)  
                            nwocr = re.search(".*numberWithOneContig.*= (\d+);", line)  
                            aicr = re.search(".*avgIsotigCnt.*= (.*);", line)  
                            licr = re.search(".*largestIsotigCnt.*= (\d+);", line)  
                            nwoir = re.search(".*numberWithOneIsotig.*= (\d+);", line)    
                            # Then fill the parsing result table                       
                            if nir != None:
                                metrics["isogroupMetrics"]["numberOfIsogroups"] = nir.group(1)
                            if accr != None:
                                metrics["isogroupMetrics"]["avgContigCnt"] = accr.group(1)
                            if lcr != None:
                                metrics["isogroupMetrics"]["largestContigCnt"] = lcr.group(1)
                            if nwocr != None:
                                metrics["isogroupMetrics"]["numberWithOneContig"] = nwocr.group(1)
                            if aicr != None:
                                metrics["isogroupMetrics"]["avgIsotigCnt"] = aicr.group(1)
                            if licr != None:
                                metrics["isogroupMetrics"]["largestIsotigCnt"] = licr.group(1)
                            if nwoir != None:
                                metrics["isogroupMetrics"]["numberWithOneIsotig"] = nwoir.group(1)
                            if line.startswith("\t}"):
                                break

                    # If this is the isotigMetrics subsection
                    if line.startswith("\tisotigMetrics"):
                        line = next(handle)
                        metrics["isotigMetrics"] = {}
                        while True :
                            line = next(handle)
                            # Define all regexp we want to catch
                            nir = re.search(".*numberOfIsotigs.*= (\d+);", line)
                            accr = re.search(".*avgContigCnt.*= (.*);", line)                        
                            lcr = re.search(".*largestContigCnt.*= (\d+);", line)  
                            nwocr = re.search(".*numberWithOneContig.*= (\d+);", line)
                            nor = re.search(".*numberOfBases.*= (\d+);", line)   
                            aicr = re.search(".*avgIsotigSize.*= (.*);", line)   
                            n50ir = re.search(".*N50IsotigSize.*= (\d+);", line)    
                            lisr = re.search(".*largestIsotigSize.*= (\d+);", line) 
                            # Then fill the parsing result table                       
                            if nir != None:
                                metrics["isotigMetrics"]["numberOfIsotigs"] = nir.group(1)
                            if accr != None:
                                metrics["isotigMetrics"]["avgContigCnt"] = accr.group(1)
                            if lcr != None:
                                metrics["isotigMetrics"]["largestContigCnt"] = lcr.group(1)
                            if nwocr != None:
                                metrics["isotigMetrics"]["numberWithOneContig"] = nwocr.group(1)
                            if nor != None:
                                metrics["isotigMetrics"]["numberOfBases"] = nor.group(1)
                            if aicr != None:
                                metrics["isotigMetrics"]["avgIsotigSize"] = aicr.group(1)
                            if n50ir != None:
                                metrics["isotigMetrics"]["N50IsotigSize"] = n50ir.group(1)
                            if lisr != None:
                                metrics["isotigMetrics"]["largestIsotigSize"] = lisr.group(1)
                            if line.startswith("\t}"):
                                break

                    # If this is the largeContigMetrics subsection
                    if line.startswith("\tlargeContigMetrics"):
                        line = next(handle)
                        metrics["largeContigMetrics"] = {}
                        while True :
                            line = next(handle)
                            # Define all regexp we want to catch
                            cr = re.search(".*numberOfContigs.*= (\d+);", line)  
                            br = re.search(".*numberOfBases.*= (\d+);", line)  
                            acsr = re.search(".*avgContigSize.*= (\d+);", line)  
                            n50r = re.search(".*N50ContigSize.*= (\d+);", line)  
                            lcsr = re.search(".*largestContigSize.*= (\d+);", line)                          
                            q40r = re.search(".*Q40PlusBases.*= (\d+), (.*)%;", line)  
                            q39r = re.search(".*Q39MinusBases.*= (\d+), (.*)%;", line)
                            # Then fill the parsing result table 
                            if cr != None:
                                metrics["largeContigMetrics"]["numberOfContigs"] = cr.group(1)
                            if br != None:
                                metrics["largeContigMetrics"]["numberOfBases"] = br.group(1)
                            if acsr != None:
                                metrics["largeContigMetrics"]["avgContigSize"] = acsr.group(1)
                            if n50r != None:
                                metrics["largeContigMetrics"]["N50ContigSize"] = n50r.group(1)
                            if lcsr != None:
                                metrics["largeContigMetrics"]["largestContigSize"] = lcsr.group(1)
                            if q40r != None:
                                metrics["largeContigMetrics"]["Q40PlusBases"] = [q40r.group(1), q40r.group(2)]
                            if q39r != None:
                                metrics["largeContigMetrics"]["Q39MinusBases"] = [q39r.group(1), q39r.group(2)]
                            if line.startswith("\t}"):
                                break
    
                    # If this is the allContigMetrics subsection
                    if line.startswith("\tallContigMetrics"):
                        line = next(handle)
                        metrics["allContigMetrics"] = {}
                        while True :
                            line = next(handle)
                            # Define all regexp we want to catch
                            cr = re.search(".*numberOfContigs.*= (\d+);", line)  
                            br = re.search(".*numberOfBases.*= (\d+);", line)  
                            # Then fill the parsing result table  
                            if cr != None:
                                metrics["allContigMetrics"]["numberOfContigs"] = cr.group(1)
                            if br != None:
                                metrics["allContigMetrics"]["numberOfBases"] = br.group(1)
                            if line.startswith("\t}"):
                                break
                            
        return metrics
        
    def post_process(self):
        
        for ace_file in self.ace_files:
            
            contig_len = []
            sum_len_read = []
            for contig in ace.read(open(ace_file, 'r')).contigs:
                read_sum=0
                for read in contig.reads:
                    read_sum += len(read.rd.sequence)
                contig_len.append(contig.nbases)
                sum_len_read.append(read_sum)
            
            sample = os.path.splitext(os.path.splitext(os.path.basename(ace_file))[0])[0]
            ace_infos = self.__parse_ace_file(ace_file)
            self._add_result_element(sample, "nbcontigs", str(ace_infos["contigs"]["nbcontigs"]), "aceContigs")
            self._add_result_element(sample, "nbreads", str(ace_infos["contigs"]["nbreads"]), "aceContigs")
            self._add_result_element(sample, "minlength", str(ace_infos["contigs"]["minlength"]), "aceContigs")
            self._add_result_element(sample, "maxlength", str(ace_infos["contigs"]["maxlength"]), "aceContigs")
            self._add_result_element(sample, "medlength", str(ace_infos["contigs"]["medlength"]), "aceContigs")
            self._add_result_element(sample, "meanlength", str(ace_infos["contigs"]["meanlength"]), "aceContigs")

            hist, edges = np.histogram(contig_len, bins=30)
            self._add_result_element(sample, "lengthpng_x", ",".join(map(str, edges)), "aceContigs")
            self._add_result_element(sample, "lengthpng_y", ",".join(map(str, hist)), "aceContigs")
            
            self._add_result_element(sample, "mindepth", str(ace_infos["depth"]["mindepth"]), "aceDepth")
            self._add_result_element(sample, "maxdepth", str(ace_infos["depth"]["maxdepth"]), "aceDepth")
            self._add_result_element(sample, "meddepth", str(ace_infos["depth"]["meddepth"]), "aceDepth")
            self._add_result_element(sample, "meandepth", str(ace_infos["depth"]["meandepth"]), "aceDepth")
            self._add_result_element(sample, "depthpng_x", ",".join(map(str, sum_len_read)), "aceDepth")
            self._add_result_element(sample, "depthpng_y", ",".join(map(str, contig_len)), "aceDepth")
            
        for metrics_file in self.metrics_files:
            
            sample = os.path.splitext(os.path.splitext(os.path.basename(metrics_file))[0])[0]
            metrics = self.__parse_metrics_file(metrics_file)
            # If the readStatus information is present
            if "readStatus" in metrics :
                self._add_result_element(sample, "numAlignedReads", metrics["readStatus"]["numAlignedReads"][0] + ' (' + metrics["readStatus"]["numAlignedReads"][1] + ')', "readStatus")
                self._add_result_element(sample, "numAlignedBases", metrics["readStatus"]["numAlignedBases"][0] + ' (' + metrics["readStatus"]["numAlignedBases"][1] + ')', "readStatus")
                self._add_result_element(sample, "inferredReadError", metrics["readStatus"]["inferredReadError"][1] + ' (' + metrics["readStatus"]["inferredReadError"][0] + ')', "readStatus")
                self._add_result_element(sample, "numberAssembled", metrics["readStatus"]["numberAssembled"], "readStatus")
                self._add_result_element(sample, "numberPartial", metrics["readStatus"]["numberPartial"], "readStatus")
                self._add_result_element(sample, "numberSingleton", metrics["readStatus"]["numberSingleton"], "readStatus")
                self._add_result_element(sample, "numberRepeat", metrics["readStatus"]["numberRepeat"], "readStatus")
                self._add_result_element(sample, "numberOutlier", metrics["readStatus"]["numberOutlier"], "readStatus")
                self._add_result_element(sample, "numberTooShort", metrics["readStatus"]["numberTooShort"], "readStatus")

            # If the isogroupMetrics information is present
            if "isogroupMetrics" in metrics :
                self._add_result_element(sample, "numberOfIsogroups", metrics["isogroupMetrics"]["numberOfIsogroups"], "isogroupMetrics")
                self._add_result_element(sample, "avgContigCnt", metrics["isogroupMetrics"]["avgContigCnt"], "isogroupMetrics")
                self._add_result_element(sample, "largestContigCnt", metrics["isogroupMetrics"]["largestContigCnt"], "isogroupMetrics")
                self._add_result_element(sample, "numberWithOneContig", metrics["isogroupMetrics"]["numberWithOneContig"], "isogroupMetrics")
                self._add_result_element(sample, "avgIsotigCnt", metrics["isogroupMetrics"]["avgIsotigCnt"], "isogroupMetrics")
                self._add_result_element(sample, "largestIsotigCnt", metrics["isogroupMetrics"]["largestIsotigCnt"], "isogroupMetrics")
                self._add_result_element(sample, "numberWithOneIsotig", metrics["isogroupMetrics"]["numberWithOneIsotig"], "isogroupMetrics")
    
            # If the isotigMetrics information is present
            if "isotigMetrics" in metrics :
                self._add_result_element(sample, "numberOfIsotigs", metrics["isotigMetrics"]["numberOfIsotigs"], "isotigMetrics")
                self._add_result_element(sample, "largestContigCnt", metrics["isotigMetrics"]["largestContigCnt"], "isotigMetrics")
                self._add_result_element(sample, "numberWithOneContig", metrics["isotigMetrics"]["numberWithOneContig"], "isotigMetrics")
                self._add_result_element(sample, "numberOfBases", metrics["isotigMetrics"]["numberOfBases"], "isotigMetrics")
                self._add_result_element(sample, "avgIsotigSize", metrics["isotigMetrics"]["avgIsotigSize"], "isotigMetrics")
                self._add_result_element(sample, "largestIsotigSize", metrics["isotigMetrics"]["largestIsotigSize"], "isotigMetrics")
                self._add_result_element(sample, "N50IsotigSize", metrics["isotigMetrics"]["N50IsotigSize"], "isotigMetrics")

            # If the largeContigMetrics and allContigMetrics information are present but nor isogroupMetrics
            if "largeContigMetrics" in metrics and "allContigMetrics" in metrics :
                self._add_result_element(sample, "numberOfContigs", metrics["allContigMetrics"]["numberOfContigs"], "allContigMetrics")
                self._add_result_element(sample, "numberOfBases", metrics["allContigMetrics"]["numberOfBases"], "allContigMetrics")
                self._add_result_element(sample, "numberOfContigs", metrics["largeContigMetrics"]["numberOfContigs"], "largeContigMetrics")
                self._add_result_element(sample, "numberOfBases", metrics["largeContigMetrics"]["numberOfBases"], "largeContigMetrics")
                self._add_result_element(sample, "avgContigSize", metrics["largeContigMetrics"]["avgContigSize"], "largeContigMetrics")
                self._add_result_element(sample, "N50ContigSize", metrics["largeContigMetrics"]["N50ContigSize"], "largeContigMetrics")
                self._add_result_element(sample, "largestContigSize", metrics["largeContigMetrics"]["largestContigSize"], "largeContigMetrics")
                self._add_result_element(sample, "Q40PlusBases", metrics["largeContigMetrics"]["Q40PlusBases"][0] + ' (' + metrics["largeContigMetrics"]["Q40PlusBases"][1] + ')', "largeContigMetrics")
                self._add_result_element(sample, "Q39MinusBases", metrics["largeContigMetrics"]["Q39MinusBases"][0] + ' (' + metrics["largeContigMetrics"]["Q39MinusBases"][1] + ')', "largeContigMetrics")

        # Finaly create and add the archive to the analyse
        result_files = []
        result_files.extend(self.ace_files)
        result_files.extend(self.metrics_files)
        result_files.extend(self.large_contig_fasta_files)
        result_files.extend(self.large_contig_qual_files)
        result_files.extend(self.all_contig_fasta_files)
        result_files.extend(self.all_contig_qual_files)
        self._create_and_archive(result_files, self.archive_name)
        
    def get_version(self):
        cmd = [self.get_exec_path("runAssembly"), "--version"]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stdout.split()[1]
    
    def process(self):
        # defines outputs
        o_metrics_files = self.get_outputs('{basename_woext}/454NewblerMetrics.txt', self.input_files)
        o_all_contig_fasta_files = self.get_outputs('{basename_woext}/454AllContigs.fna', self.input_files)
        o_all_contig_qual_files = self.get_outputs('{basename_woext}/454AllContigs.qual', self.input_files)
        if self.cdna:
            o_ace_files = self.get_outputs('{basename_woext}/454Isotigs.ace', self.input_files)
            o_large_contig_fasta_files = self.get_outputs('{basename_woext}/454Isotigs.fna', self.input_files)
            o_large_contig_qual_files = self.get_outputs('{basename_woext}/454Isotigs.qual', self.input_files)
        else:
            o_ace_files = self.get_outputs('{basename_woext}/454Contigs.ace', self.input_files)
            o_large_contig_fasta_files = self.get_outputs('{basename_woext}/454LargeContigs.fna', self.input_files)
            o_large_contig_qual_files = self.get_outputs('{basename_woext}/454LargeContigs.qual', self.input_files)
        # define command line
        run_assembly = ShellFunction(self.get_exec_path("runAssembly") + " " + self.options + " -o $2 $1 > $3", 
                                     cmd_format='{EXE} {IN} {OUT}')
        run_assembly = MultiMap(run_assembly, self.input_files, [self.output_dirs, self.stdouts, 
                                o_ace_files, o_metrics_files, o_large_contig_fasta_files, 
                                o_large_contig_qual_files, o_all_contig_fasta_files, o_all_contig_qual_files])
        rename_from, rename_to = [], []
        rename_from.extend(o_ace_files)
        rename_from.extend(o_metrics_files)
        rename_from.extend(o_large_contig_fasta_files)
        rename_from.extend(o_large_contig_qual_files)
        rename_from.extend(o_all_contig_fasta_files)
        rename_from.extend(o_all_contig_qual_files)
        rename_to.extend(self.ace_files)
        rename_to.extend(self.metrics_files)
        rename_to.extend(self.large_contig_fasta_files)
        rename_to.extend(self.large_contig_qual_files)
        rename_to.extend(self.all_contig_fasta_files)
        rename_to.extend(self.all_contig_qual_files)
        mv = ShellFunction("cp $1 $2", cmd_format='{EXE} {IN} {OUT}')
        mv = Map(mv, inputs=rename_from, outputs=rename_to)
