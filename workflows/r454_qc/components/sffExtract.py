#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.component import Component
from jflow.abstraction import MultiMap

from weaver.function import ShellFunction

from workflows.formats import sff, fastq

class SFFExtract (Component):
    
    def define_parameters(self, input_files):
        self.add_input_file_list( "input_files", "input sff files paths", default=input_files, required=True, file_format = sff)
        self.add_output_file_list( "fastq_files", "Outputed fastq files.", pattern='{basename_woext}.fastq', items=input_files, file_format=fastq)
        self.add_output_file_list( "stdouts", "SFFExtract stdout files.", pattern='{basename_woext}.stdout', items=input_files)
    
    def process(self):
        sff_extract = ShellFunction(self.get_exec_path("sff_extract.py") + " -c $1 -s $2 > $3", cmd_format='{EXE} {IN} {OUT}')
        sff_extract = MultiMap(sff_extract, self.input_files, [self.fastq_files, self.stdouts])
