#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from subprocess import Popen, PIPE

from ng6.analysis import Analysis

from weaver.function import ShellFunction

class SFFfile (Analysis):
    
    def define_parameters(self, sff_file, files_prefix, midscheme, mids_config_file):
        files_prefix = os.path.join(self.output_directory, files_prefix)
        
        self.add_input_file("sff_file", "Input sff file", default=sff_file, required=True, file_format = 'sff')
        self.add_parameter("files_prefix", "Prefix for output files", default=files_prefix, required=True)
        self.add_parameter("midscheme", "midscheme", default=midscheme, required=True)
        self.add_input_file("mids_config_file", "mids_config_file path", default=mids_config_file, required=True)
        self.add_output_file_endswith( "splited_sffs", "Output sff file",  pattern = '.sff', file_format='sff')
        self.add_output_file( "stdout", "SFFfile stdout", filename=os.path.splitext(os.path.basename(sff_file))[0]+".stdout")
        
    def define_analysis(self):
        self.name = "Demultiplexing"
        self.description = "Demultiplex sequences per sample."
        self.software = "sfffile"
        basename = os.path.splitext(os.path.basename(self.sff_file))[0]
        self.options = "-s " + self.midscheme + " -mcf " + basename + ".cfg " + \
            " -o " + os.path.basename(self.files_prefix)
        
    def post_process(self):
        import re
        results_files = []
        for file in os.listdir(self.output_directory):
            if file.endswith(".sff"):
                results_files.append(os.path.join(self.output_directory, file))
        mids_regex = re.compile("  (.*):  (.*) reads.*")
        for line in open(self.stdout, 'r').readlines():
            mr = mids_regex.match(line)
            if mr != None:
                self._add_result_element(os.path.basename(self.stdout), mr.group(1), mr.group(2))
        # Figure out what is the mid config file path from the parameters             
        basename = os.path.splitext(os.path.basename(self.sff_file))[0]   
        self._add_result_element(os.path.basename(self.stdout), "config_file", self._save_file(self.mids_config_file, basename+".cfg"))
        # Finaly create and add the archive to the analysis
        self._create_and_archive(results_files, basename+".tar.gz")
        
    def get_version(self):
        cmd = [self.get_exec_path("sfffile"), "--version"]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stdout.split()[1]

    def process(self):
        sfffile = ShellFunction(self.get_exec_path("sfffile") + " -s $1 -mcf $2 -o $3 $4 > $5", cmd_format='{EXE} {ARG} {IN} {OUT}')
        sfffile(inputs=self.sff_file, outputs=self.stdout, arguments=[self.midscheme, self.mids_config_file, self.files_prefix])
        