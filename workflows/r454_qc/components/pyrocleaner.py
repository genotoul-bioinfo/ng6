#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from subprocess import Popen, PIPE

from jflow.abstraction import MultiMap
from ng6.analysis import Analysis

from weaver.function import ShellFunction

from workflows.formats import sff

class Pyrocleaner (Analysis):
    
    def define_parameters(self, input_files, format="sff", clean_pairends=False, clean_length_std=True, clean_length_win=False,
                          clean_ns=True, clean_duplicated_reads=True, clean_complexity_win=True, clean_complexity_full=False,
                          clean_quality=False, recursion_limit=1000, border_limit=100, missmatch=10, std=2, min_length=200,
                          max_length=600, ns_percent=4, duplication_limit=40, window=100, step=5, complexity=40, quality_threshold=35,
                          archive_name=None):
        
        
        self.add_input_file_list( "input_files", "input sff files", default=input_files, required=True, file_format = sff)
        self.add_parameter("format", "format", default=format)
        self.add_parameter("clean_pairends", "clean_pairends", default=clean_pairends, type=bool)
        self.add_parameter("clean_length_std", "clean_length_std", default=clean_length_std, type=bool)
        self.add_parameter("clean_length_win", "clean_length_win", default=clean_length_win, type=bool)
        self.add_parameter("clean_ns", "clean_ns", default=clean_ns, type=bool)
        self.add_parameter("clean_duplicated_reads", "clean_duplicated_reads", default=clean_duplicated_reads, type=bool)
        self.add_parameter("clean_complexity_win", "clean_complexity_win", default=clean_complexity_win, type=bool)
        self.add_parameter("clean_complexity_full", "clean_complexity_full", default=clean_complexity_full, type=bool)
        self.add_parameter("clean_quality", "clean_quality", default=clean_quality, type=bool)
        self.add_parameter("recursion_limit", "recursion_limit", default=recursion_limit, type=int)
        self.add_parameter("border_limit", "border_limit", default=border_limit, type=int)
        self.add_parameter("missmatch", "missmatch", default=missmatch, type=int)
        self.add_parameter("std", "std", default=std, type=int)
        self.add_parameter("min_length", "min_length", default=min_length, type=int)
        self.add_parameter("max_length", "max_length", default=max_length, type=int)
        self.add_parameter("ns_percent", "ns_percent", default=ns_percent, type=int)
        self.add_parameter("duplication_limit", "duplication_limit", default=duplication_limit, type=int)
        self.add_parameter("window", "window", default=window, type=int)
        self.add_parameter("step", "step", default=step, type=int)
        self.add_parameter("complexity", "complexity", default=complexity, type=int)
        self.add_parameter("quality_threshold", "quality_threshold", default=quality_threshold, type=int)
        self.add_parameter("archive_name", "archive_name", default=archive_name)
        
        
        self.cleaning_options = ""
        if self.clean_pairends: self.cleaning_options += "--clean-pairends "
        if self.clean_length_std: self.cleaning_options += "--clean-length-std "
        if self.clean_length_win: self.cleaning_options += "--clean-length-win "
        if self.clean_ns: self.cleaning_options += "--clean-ns "
        if self.clean_duplicated_reads: self.cleaning_options += "--clean-duplicated-reads "
        if self.clean_complexity_win: self.cleaning_options += "--clean-complexity-win "
        if self.clean_complexity_full: self.cleaning_options += "--clean-complexity-full "
        if self.clean_quality: self.cleaning_options += "--clean-quality "
        
        self.add_output_file_list( "clean_files", "clean_files", pattern='{basename_woext}/{basename_woext}.clean.'+self.format, items=self.input_files, file_format=sff)
        self.add_output_file_list( "output_dirs", "output_dirs", pattern='{basename_woext}', items=self.input_files)
        self.add_output_file_list( "log_files", "log_files", pattern='{basename_woext}/{basename_woext}.log', items=self.input_files)
        self.add_output_file_list( "stdouts", "stdouts", pattern='{basename_woext}.stdout', items=self.input_files)
        self.add_output_file_list( "stderrs", "stderrs", pattern='{basename_woext}.stderr', items=self.input_files)
        
    def define_analysis(self):
        self.name = "PyroCleaner"
        self.description = "Clean reads produced by the 454 pyrosequencer."
        self.software = "pyrocleaner.py"
        self.options = "--format " + self.format + " " + self.cleaning_options + " --recursion " + \
            str(self.recursion_limit) + " --border-limit " + str(self.border_limit) + " --missmatch " + \
            str(self.missmatch) + " --std " + str(self.std) + " --min " + str(self.min_length) + " --max " + \
            str(self.max_length) + " --ns_percent " + str(self.ns_percent) + " --duplication_limit " + \
            str(self.duplication_limit) + " --window " + str(self.window) + " --step " + str(self.step) + \
            " --complexity " + str(self.complexity) + " --quality-threshold " + str(self.quality_threshold)
        
    def __parse_log_file(self, log_file):
        """
        Parse the pyrocleaner log file
          @param log_file    : the log file path
          @return            : a log hash map
        """
        logs = {}
        for line in open(log_file, 'r').readlines():
            if line.startswith("## summary (global)"):
                logs["stats"] = line.rstrip().split()[4:]
            if line.startswith("## header (duplicated)"):
                logs["duplicated_header"] = line.rstrip().split()[4:]
            if line.startswith("## summary (duplicated)"):
                logs["duplicated_stats"] = line.rstrip().split()[4:]
                    
        return logs
        
    def post_process(self):
        result_files = []
        for log_file in self.log_files:
            sample = os.path.splitext(os.path.basename(log_file))[0]
            logs = self.__parse_log_file(log_file)
            self._add_result_element(sample, "nb_reads_begining", logs['stats'][0])
            self._add_result_element(sample, "nb_reads_end", logs['stats'][1])
            self._add_result_element(sample, "del_length", logs['stats'][2])
            self._add_result_element(sample, "del_ns", logs['stats'][3])
            self._add_result_element(sample, "del_complexity", logs['stats'][4])
            self._add_result_element(sample, "del_duplicat", logs['stats'][5])
            self._add_result_element(sample, "nb_pe", logs['stats'][6])
            self._add_result_element(sample, "del_quality", logs['stats'][7])
            # Add sequences duplication profil information
            try :
                self._add_result_element(sample, "profile_x", ",".join(logs["duplicated_header"]))
                self._add_result_element(sample, "profile_y", ",".join(logs["duplicated_stats"]))
            except :
                pass
            result_files.append(log_file)
        result_files.extend(self.clean_files)
        # Finaly create and add the archive to the analyse
        self._create_and_archive(result_files, self.archive_name)
        
    def get_version(self):
        cmd = ["python3", self.get_exec_path("pyrocleaner.py"), "--version"]
        print(cmd)
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stdout.split()[1]
    
    def process(self):
        # then process the command
        pyrocleaner = ShellFunction("python3 " + self.get_exec_path("pyrocleaner.py") + " " + self.options + " --in $1 --out $2" + \
                                    " --log $3 > $4 2> $5", cmd_format='{EXE} {IN} {OUT}')
        pyrocleaner = MultiMap(pyrocleaner, self.input_files, [self.output_dirs, self.log_files, self.stdouts, self.stderrs, self.clean_files])
