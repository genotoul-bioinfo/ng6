#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys, tempfile

from ng6.ng6workflow import NG6Workflow
from ng6.config_reader import NG6ConfigReader

class R454QualityCheck (NG6Workflow):
    
    def export_mids_to_newbler_cfg_file(self, error=2, midscheme="454MIDS"):
        """
        Export the config file in newbler format
        the script add_raw_files.
          @param files: the files to archive
          @param mode: can be none, gz, bz2, tar.gz and tar.bz2    
        """
        mids_cfg_path = tempfile.NamedTemporaryFile(suffix=".cfg").name
        mid_file = open(mids_cfg_path, "w")
        mid_file.write(midscheme+"\n")
        mid_file.write("{\n")
        # MID1:desc_ex1;MID2:desc_ex2;MID3,MID4:
        cfg_reader = NG6ConfigReader()
        mids = cfg_reader.get_454_mids()
        # First write the run mids config file
        for sample in self.runobj.get_samples() :
            for mid_name in sample.sample_id.split(',') :
                try:
                    mid_name = mid_name.strip()
                    seq = mids[mid_name.lower()].split(",")
                    seq_val = ''
                    if len(seq) > 1:
                        seq_val += '"' + seq[0] + '", ' + str(error) + ', "' + seq[1] + '";'
                    else:
                        seq_val += '"' + seq[0] + '", ' + str(error) + ';'
                    mid_file.write('\tmid = "' + sample.name + '", ' + seq_val +"\n")
                except:
                    pass
        mid_file.write("}")
        mid_file.close()
        return [mids_cfg_path, midscheme]
    
    
    def has_454_mids(self):
        cfg_reader = NG6ConfigReader()
        mids = cfg_reader.get_454_mids()
        mids_key = list(mids.keys())
        res = False
        for sample in self.runobj.get_samples():
            if str(sample.sample_id).lower() in mids_key :
                res = True
                break
        return res
    
    def get_name(self):
        return 'r454_qc'
    
    def get_description(self):
        return "Roche 454 quality check pipeline"
    
    def define_parameters(self, function="process"):
        self.add_parameter("compression", "How should the data be compressed once archived", choices= [ "none", "gz", "bz2"], default = "none")
        self.add_parameter("demux_error", "How many error allowed when demultiplexing", type = int , default = 2)
        self.add_input_file_list("databank", "Which databank should be used to seek assignation (as to be nr databank)")
    
    
    def process(self):
        raw_files = []
        for f in self.get_all_reads():
            if f not in raw_files :
                raw_files.append(f)
        if len(raw_files) > 1 :
            sys.exit("Every sample must have the same read1 sff file ")
        
        raw_files = raw_files[0]
        # handle if run name have spaces
        run_name = "_".join(self.runobj.name.split())
        # add raw files to the run
        addrawfiles = self.add_component("AddRawFiles", [self.runobj, raw_files, self.compression])
        # extract raw files and store as analysis
        raw_extract = self.add_component("SFFExtractA", [raw_files, run_name+"_sff_extract.tar.gz"])
        # make some statistics on raw file
        fastqc = self.add_component("FastQC", [raw_extract.fastq_files, False, False, run_name+"_fastqc.tar.gz"], component_prefix="raw", parent=raw_extract)
        
        # if it's a run with some index, let's split raw file
        if self.has_454_mids() :
            [mids_config_file, midscheme] = self.export_mids_to_newbler_cfg_file(self.demux_error)
            # extract sff per mids
            sfffile = self.add_component("SFFfile", [raw_files , run_name, midscheme, mids_config_file])
            sff_to_process = sfffile.splited_sffs
            # extract the files to process
            sff_extract = self.add_component("SFFExtract", [sff_to_process], parent=sfffile)
            fastq_to_process = sff_extract.fastq_files
            # make some statistics on all samples
            fastqc = self.add_component("FastQC", [fastq_to_process, False, False, run_name+"_fastqc.tar.gz"], component_prefix="samples", parent=sfffile)
            parent_src = sfffile
        else:
            sff_to_process = [raw_files]
            fastq_to_process = raw_extract.fastq_files
            parent_src = None
        
        # contamination_search
        if self.databank:
            subset_assignation = self.add_component("SubsetAssignation", [sff_to_process, self.databank], parent=parent_src)
        
        # clean reads using pyrocleaner
        pyrocleaner = self.add_component("Pyrocleaner", kwargs={"input_files": sff_to_process, "archive_name": run_name+"_pyrocleaner.tar.gz"}, parent=parent_src)
        # assemble the cleaned reads
        run_assembly = self.add_component("RunAssembly", kwargs={"input_files": pyrocleaner.clean_files, "archive_name": run_name+"_run_assembly.tar.gz"}, parent=pyrocleaner)
        