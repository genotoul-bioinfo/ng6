#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from ng6.ng6workflow import CasavaNG6Workflow
from ng6.utils import Utils


class PhiXCheck (CasavaNG6Workflow):
    def get_name(self):
        return 'phiX'
    
    def get_description(self):
        return "phiX spike-in quality check pipeline"
    
    def define_parameters(self, function="process"):
        
        self.add_input_file("phix_genome", "Which genome should be used to retrieve phiX reads", required = True)
        self.add_parameter("delete_bam", "The BAM are not stored", type='bool', default = False)
        self.add_parameter("histogram_width", "Explicitly sets the histogram width, overriding automatic truncation of histogram tail", type='int', default = 800, group="INSERTSIZE section")
        self.add_parameter("min_pct", "When generating the histogram, discard any data categories (out of FR, TANDEM, RF) that have"+
                           " fewer than this percentage of overall reads", type='float', default = 0.01, group="INSERTSIZE section")
        
        self.add_parameter("trim_start", "To identify phiX reads the pipeline extracts a subset of each read and aligns" + 
                           "this subset on the phiX databank. 'trim start' is the position of the kept first nucleotide.", type='int', default = 10)
        self.add_parameter("trim_end", "To identify phiX reads the pipeline extracts a subset of each read and aligns this" + 
                           " subset on the phiX databank. 'trim end' is the position of the kept last nucleotide..", type='int', default = 80)
    
    def process(self):      
        # index the phiX genome if not already indexed
        indexed_phiX = self.phix_genome
        if not os.path.exists( self.phix_genome + ".bwt" ):
            bwaindex = self.add_component("BWAIndex", [self.phix_genome])
            indexed_phiX = bwaindex.databank
        
        # trim reads to facilitate alignment
        trim = self.add_component("Trimmer", [self.get_all_reads()+ self.undetermined_reads1 + self.undetermined_reads2, self.trim_start, self.trim_end])
        
        # list trimmed files
        if self.is_paired_end() :
            # split read 1 and read 2 from filtered files list
            [trimmed_read1_files, trimmed_read2_files] = Utils.split_pair(trim.output_files, (self.group_prefix != None))
        else:
            trimmed_read1_files = trim.output_files
            trimmed_read2_files = []

        # align reads against phiX
        R2 = None
        if len( trimmed_read2_files ) != 0:
             R2 = trimmed_read2_files
        bwa = self.add_component("BWA", [indexed_phiX, trimmed_read1_files, R2, None, "aln", False])
        
        # filter reads
        filterByBam = self.add_component("FilterByBam", [bwa.bam_files, self.get_all_reads("read1") + self.undetermined_reads1, 
                                                         self.get_all_reads("read2") + self.undetermined_reads2, "aln"])
        # merge files
        phiX_R1 = []
        if self.undetermined_reads1 :
            concatenatefiles_R1_deter = self.add_component("ConcatenateFiles", [filterByBam.output_R1_files[:len(self.get_all_reads("read1"))], "phiX_determined_R1"], component_prefix="phiX_determined_R1" )
            concatenatefiles_R1_undeter = self.add_component("ConcatenateFiles", [filterByBam.output_R1_files[len(self.get_all_reads("read1")):], "phiX_undetermined_R1"], component_prefix="phiX_undetermined_R1" )
            phiX_R1 = [concatenatefiles_R1_deter.output_file, concatenatefiles_R1_undeter.output_file]
        else :
           concatenatefiles_R1_deter = self.add_component("ConcatenateFiles", [filterByBam.output_R1_files, "phiX_all_R1"], component_prefix="phiX_all_R1")
           phiX_R1 = [concatenatefiles_R1_deter.output_file] 
        
        phiX_R2 = []
        if self.is_paired_end() :
            if self.undetermined_reads2 :
                concatenatefiles_R2_deter = self.add_component("ConcatenateFiles", [filterByBam.output_R2_files[:len(self.get_all_reads("read2"))], "phiX_determined_R2"], component_prefix="phiX_determined_R2")
                concatenatefiles_R2_undeter = self.add_component("ConcatenateFiles", [filterByBam.output_R2_files[len(self.get_all_reads("read2")):], "phiX_undetermined_R2"], component_prefix="phiX_undetermined_R2")
                phiX_R2 = [concatenatefiles_R2_deter.output_file, concatenatefiles_R2_undeter.output_file]
            else :
                concatenatefiles_R2_deter = self.add_component("ConcatenateFiles", [filterByBam.output_R2_files, "phiX_all_R2"], component_prefix="phiX_all_R2")
                phiX_R2 = [concatenatefiles_R2_deter.output_file] 
            
        # archive the files
        addrawfiles = self.add_component("AddRawFiles", [self.runobj, phiX_R1+phiX_R2, self.compression])
        
        # fastq illumina filter
        fastqilluminafilter_phiX = self.add_component("FastqIlluminaFilter", [self.runobj,phiX_R1+phiX_R2, "pass_illumina_filters", None], component_prefix="phiX")
        # list filtered files
        if self.is_paired_end() :
            # split read 1 and read 2 from filtered files list
            [filtered_phiX_R1, filtered_phiX_R2] = Utils.split_pair(fastqilluminafilter_phiX.fastq_files_filtered)
        else:
            filtered_phiX_R1 = fastqilluminafilter_phiX.fastq_files_filtered
            filtered_phiX_R2 = []
                 
        # make some statistics on raw file
        fastqc_phiX = self.add_component("FastQC", [filtered_phiX_R1+filtered_phiX_R2, False, True, "phiX_fastqc.tar.gz"], parent = fastqilluminafilter_phiX, component_prefix="phiX") 
        
        # align reads against phiX
        R2 = None
        if len( filtered_phiX_R2 ) != 0:
             R2 = filtered_phiX_R2
        bwa_phiX = self.add_component("BWA", [indexed_phiX, filtered_phiX_R1, R2, None, "aln", False], parent = fastqilluminafilter_phiX, component_prefix="phiX")
         
        # make some statistic on the alignement
        alignmentstats = self.add_component("AlignmentStats", [bwa_phiX.bam_files, self.is_paired_end(), False], parent = bwa_phiX)
        
        if self.is_paired_end():
            # process insert sizes
            insertssizes = self.add_component("InsertsSizes", [bwa_phiX.bam_files, self.histogram_width, self.min_pct, "LENIENT", "inserts_sizes.tar.gz"], parent = bwa_phiX)
            