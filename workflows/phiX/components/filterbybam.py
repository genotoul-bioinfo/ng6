#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from weaver.abstraction import Map
from weaver.function import PythonFunction, ShellFunction

from jflow.component import Component
from jflow.abstraction import MultiMap

def filter(reads_file, id_list_file, output_file):
    """
      @param reads_file : path to file with sequences
      @param id_list_file : path to file with ids of the kept reads
      @param output_file : path to filtered file
    """
    import jflow.seqio as seqio
    keep_list = {}
    
    # Build kept list
    id_fh = open(id_list_file)
    for id in id_fh :
        id = id.strip()
        keep_list[id] = 1
    id_fh.close()
    
    # Open output
    out_fh = ""
    if output_file.endswith(".gz"):   
        out_fh = seqio.xopen(output_file, "w")
    else:
        out_fh = open(output_file, "w")
    
    # Filter reads
    reader = seqio.SequenceReader(reads_file)
    if reader.__class__.__name__ == "FastqReader" :            
        for id, desc, seq, qual in reader :
            if id in keep_list :
                seqio.writefastq(out_fh, [[id, desc, seq, qual]])           
    else:
        for id, desc, seq, qual in reader :
            if id in keep_list :
                seqio.writefasta(out_fh, [[id, desc, seq, qual]])
    

class FilterByBam (Component):

    def define_parameters(self, bam_files, reads_1_files, reads_2_files=[], keep_type="aln"):
        """
          @param bam_files : path to bam. These files will be used to build the id list of kept reads. 
          @param reads_1_files : path to R1 files
          @param reads_2_files : path to R2 files
          @param keep_type : with "aln", pairs with one aligned reads are kept after filter on reads_1_files and reads_2_files. Otherwise, pairs without alignment for each reads are kept
        """
        self.add_input_file_list( "bam_files", "path to bam. These files will be used to build the id list of kept reads. ", default=bam_files, required=True )
        self.add_input_file_list( "reads_1_files", "path to R1 files.", default=reads_1_files, required=True )
        self.add_input_file_list( "reads_2_files", "path to R2 files", default=reads_2_files)
        self.add_output_file_list("output_R1_files", "output_R1_files", pattern='{basename}', items=reads_1_files)
        self.add_output_file_list("output_R2_files", "output_R2_files", pattern='{basename}', items=reads_2_files)
        self.add_output_file_list("phiX_ids_files", "phiX_ids_files", pattern='{basename_woext}_phixIds.txt', items=bam_files)
        self.add_parameter("keep_type", "with 'aln', pairs with one aligned reads are kept after" + 
                           "filter on reads_1_files and reads_2_files. Otherwise, pairs without alignment for each reads are kept", default=keep_type)
    
    def process(self):
        # Build kept list from bam
        if self.keep_type == "aln" :
            filter_args = "-F 4"
        else:
            filter_args = "-f 12"
        reads_list = ShellFunction( self.get_exec_path("samtools") + " view " + filter_args + " $1 | cut -f1 - | sort - > $2", cmd_format="{EXE} {IN} {OUT}")
        reads_list = Map(reads_list, inputs=self.bam_files, outputs=self.phiX_ids_files)
        
        # Filter reads
        filter_by_bam = PythonFunction( filter, cmd_format="{EXE} {IN} {OUT}")
        filter_by_bam = MultiMap(filter_by_bam, inputs=[self.reads_1_files, self.phiX_ids_files], outputs=self.output_R1_files)
        
        if len(self.reads_2_files) != 0 :
            filter_by_bam = PythonFunction( filter, cmd_format="{EXE} {IN} {OUT}")
            filter_by_bam = MultiMap(filter_by_bam, inputs=[self.reads_2_files, self.phiX_ids_files], outputs=self.output_R2_files)