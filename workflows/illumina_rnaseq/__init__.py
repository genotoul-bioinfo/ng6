#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import glob
import sys
import logging

from ng6.ng6workflow import CasavaNG6Workflow
from ng6.utils import Utils


class RnaSeqQualityCheck (CasavaNG6Workflow):
    
    def get_name(self):
        return 'illumina_rnaseq'
    
    def get_description(self):
        return "rnaseq quality check pipeline"

    def define_parameters(self, function="process"):
        self.add_parameter("delete_bam", "The BAM are not stored", type=bool, default = False)
        self.add_parameter("n_threads", "Number of threads to use for the alignment with STAR", type='int', default = 8)
        self.add_input_file("reference_genome", "Which genome should the read being align on")
        self.add_input_file("reference_transcriptome", "Which transcriptome should the read being align on")
        self.add_input_file_list("annotation", "Which annotation file should be used for processing RNAseq quality")
    
    def process(self):
        fastqilluminafilter, filtered_read1_files, filtered_read2_files, concat_files, concatenatefastq = self.illumina_process()
        logging.getLogger("RnaSeqQualityCheck").debug("process. filtered_read1_files = "+",".join(filtered_read1_files))
        logging.getLogger("RnaSeqQualityCheck").debug("process. filtered_read2_files = "+",".join(filtered_read2_files))
        logging.getLogger("RnaSeqQualityCheck").debug("process. concat_files = "+",".join(concat_files))
        
        if self.reference_transcriptome != None :
            # index the reference transcriptome if not already indexed
            indexed_ref = self.reference_transcriptome
            if not os.path.exists( self.reference_transcriptome + ".bwt" ):
                bwaindex = self.add_component("BWAIndex", [self.reference_transcriptome])
                indexed_ref = bwaindex.databank
             
            # align reads against indexed genome
            sample_lane_prefixes = None
            
            if self.group_prefix != None :
                sample_lane_prefixes = list((Utils.get_group_basenames(filtered_read1_files+filtered_read2_files, "lane")).keys())
            
            if self.align_subset_reads:
                subset = self.add_component("SubsetSeqFiles", [filtered_read1_files, filtered_read2_files], parent = fastqilluminafilter)
                filtered_read1_files = subset.subset_read1
                filtered_read2_files = subset.subset_read2
                bwa = self.add_component("BWA", [indexed_ref, filtered_read1_files, filtered_read2_files, sample_lane_prefixes, "mem", not self.delete_bam], parent = subset)
            else:
                bwa = self.add_component("BWA", [indexed_ref, filtered_read1_files, filtered_read2_files, sample_lane_prefixes, "mem", not self.delete_bam], parent = fastqilluminafilter)
       
            # make some statistic on the alignement
            alignmentstats = self.add_component("AlignmentStats", [bwa.bam_files, self.is_paired_end()], parent = bwa, component_prefix="bwa")
            
        if self.reference_genome != None:
            reference_genome = self.reference_genome
            index_dir = os.path.dirname(self.reference_genome)
            if not os.path.exists(os.path.join(index_dir, 'SAindex' ))  :
                starindex = self.add_component("STARIndex", [self.reference_genome])
                index_dir =  starindex.index_directory
                reference_genome = starindex.normalized_fasta_file
            
            # spliced alignment of reads against indexed genome
            if self.is_paired_end() and (self.group_prefix != None):
                logging.getLogger("RnaSeqQualityCheck").debug("process. Dans self.reference_genome != None > if self.is_paired_end() and (self.group_prefix != None):")
                # split read 1 and read 2 from filtered files list
                [concat_read1_files, concat_read2_files] = Utils.split_pair(concat_files, (self.group_prefix != None))
                if self.align_subset_reads:
                    subset = self.add_component("SubsetSeqFiles", [concat_read1_files, concat_read2_files], component_prefix="star", parent = fastqilluminafilter)
                    concat_read1_files = subset.subset_read1
                    concat_read2_files = subset.subset_read2
            elif self.group_prefix != None:
                logging.getLogger("RnaSeqQualityCheck").debug("process. Dans self.reference_genome != None > elif self.group_prefix != None:")
                concat_read1_files = concat_files
                concat_read2_files = []
                if self.align_subset_reads:
                    subset = self.add_component("SubsetSeqFiles", [concat_files],component_prefix="star", parent = fastqilluminafilter)
                    concat_read1_files = subset.subset_read1
                    concat_read2_files = subset.subset_read2
            else:
                logging.getLogger("RnaSeqQualityCheck").debug("process. Dans self.reference_genome != None > else")
                concat_read1_files = filtered_read1_files
                concat_read2_files = filtered_read2_files
                if self.align_subset_reads:
                    subset = self.add_component("SubsetSeqFiles", [concat_read1_files,concat_read2_files], component_prefix="star", parent = fastqilluminafilter)
                    concat_read1_files = subset.subset_read1
                    concat_read2_files = subset.subset_read2
            concat_read1_files = sorted(concat_read1_files)
            concat_read2_files = sorted(concat_read2_files)
             
            sample_lane_prefixes = None
            if self.group_prefix != None :
                sample_lane_prefixes = sorted(list((Utils.get_group_basenames(concat_read1_files+concat_read2_files, "lane")).keys()))
                 
            if self.align_subset_reads:
                star = self.add_component("STAR", [reference_genome, index_dir, concat_read1_files, concat_read2_files,sample_lane_prefixes, not self.delete_bam, self.n_threads], parent = subset)
            else:
                star = self.add_component("STAR", [reference_genome, index_dir, concat_read1_files, concat_read2_files,sample_lane_prefixes, not self.delete_bam, self.n_threads], parent = fastqilluminafilter)
            # make some statistic on the alignment
            alignmentstats = self.add_component("AlignmentStats", [star.output_bams, self.is_paired_end()], parent = star, component_prefix="star")
             
            #Quality RNA Seq analysis  
#            02/08/2018 Audrey Gibert (était-ce un boulet?)
#            C'est commenté parce qu'on a un bug à cause du script inner_distance.py "start must be smaller than end..." c'est embetant, on ne s'en sert jamais voilà voilà 
#             if self.annotation:
#                 rseqc = self.add_component("RSeQC", [star.output_bams, self.annotation], parent = star)
