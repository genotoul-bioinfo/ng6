#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>. 
#

import os

from jflow.component import Component

class STARIndex (Component):

    def define_parameters(self, input_fasta):
        self.add_input_file("input_fasta", "Which fasta file should be indexed", file_format="fasta", default=input_fasta, required=True)
        self.add_output_file("index_directory", "index_directory", filename="index")
        self.add_output_file("normalized_fasta_file", "Fasta file normalized using picard tools", filename=os.path.basename(input_fasta))
        
        self.memory = '4G'
        if self.get_memory() != None :
            self.memory=self.get_memory_per_cpu()
        self.cpu = "8"
        if self.get_cpu() != None :
            self.n_threads=self.get_cpu()
    def process(self):
        xmx="-Xmx" + self.memory.lower()
        # normalize fasta
        self.add_shell_execution( self.get_exec_path("javaPICARD")+" " + xmx + " -jar " + self.get_exec_path("Picard") + " NormalizeFasta I=$1 O=$2  ", 
                                  cmd_format="{EXE} {IN} {OUT}", map=False,
                                  inputs = self.input_fasta, outputs = self.normalized_fasta_file)
        
        # index for star
        self.add_shell_execution( "mkdir $2 ; " + self.get_exec_path("STAR") + " --runMode genomeGenerate --genomeFastaFiles $1 "+
                                  "--genomeDir $2 --runThreadN " + str(self.cpu),
                                  cmd_format='{EXE} {IN} {OUT}' , map=False, inputs = self.normalized_fasta_file, outputs = self.index_directory)
        
