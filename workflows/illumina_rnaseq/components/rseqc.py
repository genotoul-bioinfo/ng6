#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os, re, math, json, functools

from subprocess import Popen, PIPE

from ng6.analysis import Analysis
from ng6.utils import Utils

def rpkm_saturation( exec_path_rpkm_saturation, prefix, bam_file, annotation_bed, infer_experiment_stdout, r_file_output):
    import os
    import re
    import subprocess
    from decimal import Decimal

    strand = 'none'
    percent = 0
    with open(infer_experiment_stdout) as infh :
        for line in infh:
            m = re.match( r'Fraction of reads explained by\s+"(.+)":\s+(.+)', line)
            m2 = re.match( r'Unknown\s+Data\s+type', line)
            if m :
                rstrand , rpercent = m.groups()
                if rstrand and rpercent and Decimal(rpercent) > percent :
                    strand = rstrand
            elif m2 :
                open(r_file_output, "w").close()
                return
    
    # run rpkm saturation
    proc = subprocess.Popen(
        [exec_path_rpkm_saturation, '-i', bam_file, '-r', annotation_bed, '-o', prefix, '--strand', strand, '--rpkm-cutoff', '0.000001'], 
        stdout = subprocess.PIPE, 
        stderr = subprocess.PIPE
    )
    stdout, stderr = proc.communicate()


def percentile(N, percent):
    """
    Find the percentile of a list of values.
    @param  N : is a list of values. Note N MUST BE already sorted.
    @param percent : a float value from 0.0 to 1.0.
    @return - the percentile of the values
    """
    if not N:
        return None
    k = (len(N)-1) * percent
    f = math.floor(k)
    c = math.ceil(k)
    if f == c:
        return N[int(k)]
    
    d0 = N[int(f)] * (c-k)
    d1 = N[int(c)] * (k-f)
    
    return d0+d1

median   = functools.partial(percentile, percent = 0.5)
lowquart = functools.partial(percentile, percent = 0.25)
upquart  = functools.partial(percentile, percent = 0.75)

       
class RSeQC (Analysis):
    
    def define_parameters(self, bam_files, bed_files, sample_size=200000, min_intron_size_junc_ann=50,  min_intron_size_junc_sat=50 ):
        self.add_input_file_list( "bam_files", "bam_files", default=bam_files, required=True, file_format = 'bam')
        self.add_input_file_list( "bed_files", "bed_files", default=bed_files, required=True)
        self.add_parameter("sample_size", "sample_size", default=sample_size, type=int)
        self.add_parameter("min_intron_size_junc_ann", "min_intron_size_junc_ann", default=min_intron_size_junc_ann, type=int)
        self.add_parameter("min_intron_size_junc_sat", "min_intron_size_junc_sat", default=min_intron_size_junc_sat, type=int)
        
        #only mapped reads
        self.add_output_file_list( "mapped_bams", "only mapped reads", pattern='{basename_woext}.mapped.bam', items=self.bam_files)
        
        #InferExperiment Output Files
        self.add_output_file_list( "stderr_infer", "stderr_infer", pattern='{basename_woext}.infer.stderr', items=self.bam_files)
        self.add_output_file_list( "stdout_infer", "stdout_infer", pattern='{basename_woext}.infer.stdout', items=self.bam_files)
        #InnerDistance Output Files
        self.add_output_file_list( "stderr_inner", "stderr_inner", pattern='{basename_woext}.inner.stderr', items=self.bam_files)
        self.add_output_file_list( "stdout_inner", "stdout_inner", pattern='{basename_woext}.inner.stdout', items=self.bam_files)
        self.add_output_file_list( "r_files_inner", "r_files_inner", pattern='{basename_woext}.inner_distance_plot.r', items=self.bam_files)
        #Junction Annotation Output Files
        self.add_output_file_list( "stderr_junc_ann", "stderr_junc_ann", pattern='{basename_woext}.junc_ann.stderr', items=self.bam_files)
        self.add_output_file_list( "stdout_junc_ann", "stdout_junc_ann", pattern='{basename_woext}.junc_ann.stdout', items=self.bam_files)
        self.add_output_file_list( "r_files_junc_ann", "r_files_junc_ann", pattern='{basename_woext}.junction_plot.r', items=self.bam_files)
        #Junction Saturation Output Files
        self.add_output_file_list( "stderr_junc_sat", "stderr_junc_sat", pattern='{basename_woext}.junc_sat.stderr', items=self.bam_files)
        self.add_output_file_list( "stdout_junc_sat", "stdout_junc_sat", pattern='{basename_woext}.junc_sat.stdout', items=self.bam_files)
        self.add_output_file_list( "r_files_junc_sat", "r_files_junc_sat", pattern='{basename_woext}.junctionSaturation_plot.r', items=self.bam_files)
        #GeneBody Coverage Output Files
        self.add_output_file_list( "stderr_gbc", "stderr_gbc", pattern='{basename_woext}_gbc.stderr', items=self.bam_files)
        self.add_output_file_list( "stdout_gbc", "stdout_gbc", pattern='{basename_woext}_gbc.stdout', items=self.bam_files)
        self.add_output_file_list( "cov_files", "cov_files", pattern='{basename_woext}.geneBodyCoverage.txt', items=self.bam_files)
        #RPKM Saturation Output Files
        self.add_output_file_list( "r_files_rpkm_sat", "r_files_rpkm_sat", pattern='{basename_woext}.saturation.r', items=self.bam_files)
        
    def get_version(self):
        cmd = [self.get_exec_path("infer_experiment.py"), "--version"]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stdout.split()[1]


    def define_analysis(self):
        self.name = "RNA-Seq quality"
        self.description = "Reads coverage over genes, Strand rules for reads, Insert sizes, Junction annotations, Junction saturation, RPKM saturation."
        self.software = "RSeQC package"
        self.options =  ' '.join( [ '--sample-size', str(self.sample_size), '--min-intron', str(self.min_intron_size_junc_ann), '--min-intron', str(self.min_intron_size_junc_sat)] ) 


    def process(self):
        output_prefixes = self.get_outputs('{basename_woext}', self.bam_files)
        for index, bam_file in enumerate(self.bam_files) :
            bed_file = self.bed_files[index]
            output_prefix = output_prefixes[index]
            mapped_bam = self.mapped_bams[index]
            
            # get mapped bams only
            self.add_shell_execution( "{0} view -h -F4 -b $1 > $2 ".format(self.get_exec_path("samtools") ) ,  cmd_format='{EXE} {IN} {OUT}',
                                      map=False, inputs = bam_file, outputs = mapped_bam)
            
            #InferExperiment
            self.add_shell_execution( self.get_exec_path("infer_experiment.py") + " -s $1 -i $2 -r $3  1> $4 2> $5", cmd_format="{EXE} {ARG} {IN} {OUT}",
                                      map=False, arguments = self.sample_size, inputs=[mapped_bam, bed_file], outputs=[self.stdout_infer[index], self.stderr_infer[index]] )
            
            #Inner distance
            self.add_shell_execution( self.get_exec_path('inner_distance.py') + " -o $1 -i $2 -r $3  1> $4 2> $5 " , cmd_format="{EXE} {ARG} {IN} {OUT}", 
                                      map=False, arguments = output_prefix, inputs = [mapped_bam, bed_file] , outputs = [self.stdout_inner[index], self.stderr_inner[index], self.r_files_inner[index]])
            
            #Junction Annotation
            self.add_shell_execution( self.get_exec_path("junction_annotation.py") + " -i $1 -r $2 -o " + output_prefix + " -m " + 
                                      str(self.min_intron_size_junc_ann) + " > $3 2> $4", cmd_format="{EXE} {IN} {OUT}", map=False,
                                      inputs=[mapped_bam, bed_file], outputs=[ self.stdout_junc_ann[index], self.stderr_junc_ann[index], self.r_files_junc_ann[index]])
            
            #Junction Saturation
            self.add_shell_execution( self.get_exec_path("junction_saturation.py") + " -i $1 -r $2 -o " + output_prefix + 
                                     " -m " + str(self.min_intron_size_junc_sat) + " > $3 2> $4", 
                                     cmd_format="{EXE} {IN} {OUT}", map=False,
                                     inputs=[mapped_bam, bed_file], outputs=[ self.stdout_junc_sat[index], self.stderr_junc_sat[index], self.r_files_junc_sat[index]])
    
            #GeneBody Coverage
            self.add_shell_execution( self.get_exec_path("geneBody_coverage.py") + " -i $1 -o " + output_prefix + " -r $2 2> $3 > $4", 
                                     cmd_format="{EXE} {IN} {OUT}", map=False,
                                     inputs=[mapped_bam, bed_file], outputs=[ self.stderr_gbc[index], self.stdout_gbc[index], self.cov_files[index]])
                
            #RPKM Saturation
            self.add_python_execution( rpkm_saturation, cmd_format="{EXE} {ARG} {IN} {OUT}", map=False,
                                       arguments = [ self.get_exec_path('RPKM_saturation.py'), output_prefix], 
                                       inputs = [ mapped_bam, bed_file, self.stdout_infer[index] ],
                                       outputs = self.r_files_rpkm_sat[index] )

    def post_process(self):
        
        for index, bam_file in enumerate(self.bam_files) :
            sample = os.path.splitext(os.path.basename(bam_file))[0]
            
            #InferExperiment
            orientation, type, unknown = self._parse_inferexp_file(self.stdout_infer[index])
            
            if not unknown :
                self._add_result_element(sample, "reads_orientation", orientation, 'inferexp')
                self._add_result_element(sample, "reads_type", type, 'inferexp')
            
                #InnerDistance
                if re.match( r'Pair', type) :
                    inser_sizes , frequences = self._parse_r_file_inner(self.r_files_inner[index])
                    if sum(frequences) > 0 :
                        self._add_result_element(sample, "frequences",  ','.join([ str(i) for i in frequences]), 'innerdist')
                        self._add_result_element(sample, "insert_sizes", ','.join([ str(i) for i in inser_sizes]) , 'innerdist')
    
                #Junction Annotation
                splicing_junctions , splicing_events, categories = self._parse_r_file_junc_ann(self.r_files_junc_ann[index])
                if splicing_junctions or splicing_events  and ( len(splicing_junctions) == len(splicing_events) ) :
                    self._add_result_element(sample, "splicing_junctions",  ";".join([ str(i) + "," + str(j) for i,j in zip(categories, splicing_junctions)]), 'juncannot')
                    self._add_result_element(sample, "splicing_events", ";".join([ str(i) + "," + str(j) for i,j in zip(categories, splicing_events)]), 'juncannot')
                    
                #Junction Saturation
                known_junctions, novel_junctions, all_juctions = self._parse_r_file_junc_sat(self.r_files_junc_sat[index])
                if sum(known_junctions + novel_junctions + all_juctions) > 0 :
                    self._add_result_element(sample, "known_junctions", ','.join(  [str(i) for i in  known_junctions]), 'juncsat' )
                    self._add_result_element(sample, "all_junctions", ','.join( [str (i) for i in all_juctions ]), 'juncsat')
                    self._add_result_element(sample, "novel_junctions",','.join([str(i) for i in novel_junctions]), 'juncsat')
                    self._add_result_element(sample, "found_junc", str(novel_junctions[-1]), 'juncsat')
        
                #GeneBody Coverage
                total_reads, fragment_number, counts = self._parse_covstat_file(self.cov_files[index])
                if sum(counts) > 0 :
                    data = self._parse_covstat_file(self.cov_files[index])
                    self._add_result_element(sample, "count",  ",".join([ str(i) for i in counts ])  , 'gbc')
                    self._add_result_element(sample, "total_reads", str(total_reads), 'gbc')
                    self._add_result_element(sample, "fragment_number", str(fragment_number), 'gbc')
                
                #RPKM Saturation
                data = self._parse_r_file_rpkm_sat( self.r_files_rpkm_sat[index])
                for quartile, qdata in data.items() :
                    if qdata :
                        self._add_result_element(sample, quartile, json.dumps(qdata), 'rpkm_saturation')
                 
    def _parse_covstat_file(self, infile):
        counts = []
        total_reads = 0
        fragment_number = 0
        #summary = {'total_reads': 0 , 'fragment_number': 0}
        with open(infile) as cov:
            header1 = cov.readline()
            header2 = cov.readline()
            header3 = cov.readline()
            
            total = re.match( r'Total\s*reads\:*\s*(\d+)', header1)
            fragment = re.match( r'Fragment\s*number\:*\s*(\d+)', header2)
            
            for line in cov:
                counts.append( int(line.strip().split('\t')[1].rstrip("\n")) )

            #summary["count"] = ";".join(count)
            if total:
                total_reads =  total.group(1)
            if fragment:
                fragment_number = fragment.group(1)
                
        return total_reads, fragment_number, counts
    
    def _parse_inferexp_file(self, stdout_file):
        orientation , type, unknown = "None", "None", False
        with open(stdout_file, 'r') as infer_data:
            infer_infos = []
            for line in infer_data.readlines():
                m1 = re.search( r'by ["]?([- +\w,]{1,})["]?:\s([0-9.]{1,})', line)
                m2 = re.search( r'This is (\w+) Data', line)
                m3 = re.search( r'Unknown\s+Data\s+type', line)
                if m1 :
                    combi = m1.group(1)
                    percentage = float(m1.group(2)) * 100
                    if re.match( r'other', combi) :
                        combi = 'other'
                    infer_infos.append(combi +  ":" + str(percentage) )
                if m2 :
                    type = m2.group(1)
                if m3 :
                    unknown = True
                    break
            orientation = ";".join(infer_infos)
        return orientation, type, unknown
    
    def _parse_r_file_inner(self, infile):
        inser_sizes , frequences = [], []
        with open(infile) as fh:
            freq_size_regex = re.compile('^fragsize=rep\(c\(([\d,-]+)\),times=c\(([\d,-]+)')
            for line in fh:
                freq_size = freq_size_regex.search(line)
                if freq_size:
                    inser_sizes = [ int(i) for i in freq_size.group(1).split(',') ]
                    frequences = [ int(i) for i in freq_size.group(2).split(',') ]
        return inser_sizes, frequences
    
    def _parse_r_file_junc_ann(self, infile):
        splicing_junctions , splicing_events = [], [] 
        categories = ["partial novel","complete novel","known"]
        
        with open(infile) as fh:
            events_regex = re.compile('^events=c\(([\d,\.]*)')
            junctions_regex = re.compile('^junction=c\(([\d,\.]*)')
            
            for line in fh:
                events = events_regex.search(line)
                junctions = junctions_regex.search(line)
                
                if junctions:
                    splicing_junctions = [ float(i) for i in junctions.group(1).split(',') ]
                if events:
                    splicing_events = [ float(i) for i in  events.group(1).split(',') ]

        return splicing_junctions , splicing_events, categories

    
    def _parse_r_file_junc_sat(self, infile):
        known_junctions, novel_junctions, all_juctions  = [], [], []
        
        with open(infile) as fh:
            known_regex = re.compile('^y=c\(([\d,]+)\)')
            novel_regex = re.compile('^w=c\(([\d,]+)\)')
            all_regexp  = re.compile('^z=c\(([\d,]+)\)')

            for line in fh:
                known = known_regex.search(line)
                novel = novel_regex.search(line)
                all   = all_regexp.search(line)
                
                if known and not known_junctions:
                    known_junctions = [ int(i) for i in known.group(1).split(',') ] 
                if novel and not novel_junctions:
                    novel_junctions = [ int(i) for i in novel.group(1).split(',') ] 
                if all and not all_juctions:
                    all_juctions = [ int(i) for i in all.group(1).split(',') ]
        
        return known_junctions, novel_junctions, all_juctions

    
    def _parse_r_file_rpkm_sat(self, r_file):
        summary = {"Q1":{},"Q2":{},"Q3":{},"Q4":{}}
        quartiles = sorted(summary.keys())
        qindex = 0
        with open(r_file, 'r') as r:
            
            rpkm_regex = re.compile('^(S[\d]+)=c\(([\d,\.]*)')
            boxplot_regex = re.compile('^boxplot')
            
            for line in r.readlines():
                boxplot = boxplot_regex.search(line)
                rpkm = rpkm_regex.search(line)
                
                if rpkm:
                    rpkm_data=rpkm.group(2)
                    percent_group = rpkm.group(1)
                    if percent_group == "S5":
                        percent_group = "S05"
                    if rpkm_data :
                        rpkm_data = [ float( i) for i in rpkm_data.split(",")]
                        mini = round(min(rpkm_data), 3) * 100
                        maxi = round(max(rpkm_data), 3) * 100
                        med = round(median(rpkm_data), 3) * 100
                        lq = round(lowquart(rpkm_data), 3) * 100
                        uq = round(upquart(rpkm_data), 3) * 100
                        summary[quartiles[qindex]][percent_group] = [ mini, lq, med, uq, maxi]
                elif boxplot_regex.search(line):
                    qindex += 1
                      
        return summary
