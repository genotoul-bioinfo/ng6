#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from ng6.analysis import Analysis

class STAR (Analysis):
    
    def define_parameters(self, fasta_file, genome_dir, read1, read2 = None, group_prefix=None, keep_bam = True, n_threads = 4 ):
        self.add_input_file("fasta_file", "fasta_file", default=fasta_file, required=True)
        self.add_input_file("genome_dir", "STAR genome index directory", default=genome_dir, required=True)
        self.add_input_file_list( "read1", "read1", default=read1, required=True)
        self.add_input_file_list( "read2", "read2", default=read2)
        self.add_parameter("keep_bam", "keep_bam", default=keep_bam, type=bool)
        self.add_parameter("n_threads", "n_threads", default=n_threads, type='int')
        if self.get_cpu() != None :
            self.n_threads=self.get_cpu()
        items = group_prefix if group_prefix != None else self.read1
        
        self.add_output_file("fasta_file_link", "fasta_file_link", filename=os.path.basename(self.fasta_file))
        
        self.add_output_file_list("output_prefixes", "output_prefixes", pattern='{basename_woext}', items=items)
        self.add_output_file_list("output_sorted_bams", "output sorted bam files", pattern='{basename_woext}/{basename_woext}_sorted.bam', items=items, file_format = 'bam')
        self.add_output_file_list("output_bams", "output bam files", pattern='{basename_woext}/{basename_woext}.bam', items=items, file_format = 'bam')
        self.add_output_file_list("output_sams_no_md", "Output sam files from STAR, no MD score calculated", pattern='{basename_woext}/Aligned.out.sam', items=items)
        
    def get_version(self):
        return "2.3.0"    
        
    def define_analysis(self):
        self.name = "STAR Alignment"
        self.description = "Reads Alignment using STAR software"
        self.software = "STAR"
        self.options = " ".join( [ "--readFilesCommand", "zcat", "--runThreadN", str(self.n_threads)])

    def process(self):
        
        #link fasta
        self.add_shell_execution("ln -s $1 $2" , cmd_format="{EXE} {IN} {OUT}",inputs = self.fasta_file, outputs = self.fasta_file_link)
        
        # run star
        if self.read2 :
            command = [ "mkdir $3 ; " , self.get_exec_path("STAR"), "--outFileNamePrefix ","$3/", 
                       "--readFilesCommand", "zcat",  
                       "--genomeDir", self.genome_dir,
                       "--runThreadN", str(self.n_threads),
                       "--outSAMunmapped Within",
                       "--readFilesIn", "$1", "$2" ]
            self.add_shell_execution( ' '.join(command),  cmd_format='{EXE} {IN} {OUT}', map=True,
                                      inputs = [self.read1 , self.read2], outputs = [self.output_prefixes, self.output_sams_no_md], 
                                      includes = self.genome_dir)
        else :
            command = [ "mkdir $2 ; " , self.get_exec_path("STAR"), "--outFileNamePrefix ","$2/", 
                       "--readFilesCommand", "zcat",  
                       "--genomeDir", self.genome_dir,
                       "--outSAMunmapped Within",
                       "--runThreadN", str(self.n_threads),  
                       "--readFilesIn", "$1" ]
            self.add_shell_execution( ' '.join(command),  cmd_format='{EXE} {IN} {OUT}', map=True,
                                      inputs = [self.read1], outputs = [self.output_prefixes, self.output_sams_no_md], includes = self.genome_dir)
        
        # sort
        self.add_shell_execution( self.get_exec_path("samtools") + ' view -Sb $1 | ' + self.get_exec_path("samtools") + 
                                 ' sort -@ ' + str(self.n_threads) + ' - -o $2 ' ,  cmd_format='{EXE} {IN} {OUT}', map=True,
                                 inputs = [self.output_sams_no_md], outputs = [self.output_sorted_bams])
        
        #calmd and convert to bam
        self.add_shell_execution( self.get_exec_path("samtools") + ' calmd -b $1 ' + self.fasta_file_link + ' > $2' ,  
                                  cmd_format='{EXE} {IN} {OUT}', map=True,
                                  inputs = [self.output_sorted_bams], outputs = [self.output_bams])
    
    def post_process(self):
        if self.keep_bam:
            self._save_files(self.output_bam)
        

    
    
    
    
  
