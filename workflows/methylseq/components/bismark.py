#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import logging

from subprocess import Popen, PIPE

from ng6.analysis import Analysis

class Bismark (Analysis):
    """
    By default bismark launch 2 process of bowtie. In production book nb_proc x 2 processors.
    @param reference_genome : [str] Path to the reference genome (it must be indexed).
    @param input_files_R1 : [list] Paths to reads 1.
    @param input_files_R2 : [list] Paths to reads 2.
    @param samples_names : [list] The component produces one bam by samples_names. 
    @param non_directional : [bool] does library is directionnal 
    @param bowtie1 : [bool] set true to use bowtie1 instead of bowtie 1
    @param alignment_mismatch : [int]
    @param max_insert_size : [int] default 800
    @param description : [str] analysis description
    @param nb_proc : [int] nb proc to use for bowtie thread.
    """
    def define_parameters(self, reference_genome, input_files_R1, input_files_R2=None, samples_names=None, non_directional = False, bowtie1 = False, alignment_mismatch=1, max_insert_size=800, description = None, nb_proc=8):
        self.add_input_file( "reference_genome", "input reference genome", default=reference_genome, required=True, file_format = 'fasta')
        self.reference_directory = os.path.dirname(reference_genome)
        self.add_input_file_list( "input_files_R1", "input files read1", default=input_files_R1, required=True, file_format='fastq')

        self.add_parameter_list( "samples_names", "The component produces one bam by prefix.", default=samples_names )
        self.add_parameter("non_directional", "Does library is non directional ? ", default=non_directional, type="bool")
        self.add_parameter("bowtie1", "Use bowtie1 ? ", default=bowtie1, type="bool")
        self.add_parameter("alignment_mismatch", "alignment_mismatch", default=alignment_mismatch, type="int")
        self.add_parameter("max_insert_size", "max_insert_size", default=max_insert_size, type="int")
        self.add_parameter("nb_proc", "nb_proc", default=nb_proc, type="int")
        self.add_parameter("description", "description", default=description)
        if self.get_cpu() != None :
            self.nb_proc=self.get_cpu()

        self.source_file = self.reference_genome + "_source"
        extention_bowtie=""        

        if not self.bowtie1 :
            extention_bowtie="_bt2"
        if input_files_R2 :
            self.is_paired = True
            self.add_input_file_list( "input_files_R2", "input files read2", default=input_files_R2, file_format='fastq')
            self.add_output_file_list( "output_files", "output alignment file", pattern='{basename_woext}_bismark'+extention_bowtie+'_pe.bam', items=self.input_files_R1, file_format='bam')
            self.add_output_file_list( "output_report", "output report file", pattern='{basename_woext}_bismark'+extention_bowtie+'_PE_report.txt', items=self.input_files_R1)
        else :
            self.is_paired = False
            self.add_output_file_list( "output_files", "output alignment file", pattern='{basename_woext}_bismark'+extention_bowtie+'.bam', items=self.input_files_R1, file_format='bam')
            self.add_output_file_list( "output_report", "output report file", pattern='{basename_woext}_bismark'+extention_bowtie+'_SE_report.txt', items=self.input_files_R1)
        
        base_output_names=[]
        if self.samples_names == None or len(self.samples_names) == 0:
            for file in input_files_R1 :
                dir = os.path.dirname(file)
                fname_woext = str(os.path.splitext(os.path.basename(file))[0]).replace("_trimmed","")
                base_output_names.append(os.path.join(dir, fname_woext ))
        else :
            for file in input_files_R1 :
                for s in self.samples_names :
                    if os.path.basename(file).startswith(s+"_") :
                        base_output_names.append(s)
        self.add_output_file_list( "output_bam", "renamed output alignment file", pattern='{basename_woext}.bam', items=base_output_names, file_format='bam')
        self.add_output_file_list("stderrs" , "stderrs files", pattern='{basename}.stderr', items=base_output_names)
        
        
    def define_analysis(self):     
        self.name = "Alignment" 
        if (self.description == None):
            self.description = "Reads Alignment"
        self.software = "bismark"
        self.args = " --gzip --bam -q"   

        if self.alignment_mismatch :
            self.args += " -N "+ str(self.alignment_mismatch)
        if self.max_insert_size :
            self.args += " --maxins "+ str(self.max_insert_size)
        if self.non_directional :
            self.args += " --non_directional"
        if not self.bowtie1: 
            if self.nb_proc :
                # 2 bowtie process are launch in directional mode so divided allocated cpu for each bowtie
                self.args += " --p "+ str(int(self.nb_proc/2))
            self.args += " --bowtie2"
            #if not os.path.dirname(self.get_exec_path("bowtie2")) == "" :
            #    self.args += " --path_to_bowtie " + os.path.dirname(self.get_exec_path("bowtie2")) 
             
        
        self.options = self.args + " | Databank " + os.path.basename(self.reference_genome)
        
        #################################PATCH############################################
        if os.path.exists(self.source_file) :
            source = open(self.source_file,'r')
            reference_desc = source.readline();
            source.close()
            self.options += " " + reference_desc
        ##################################################################################    

    def process(self):
        #to do merge bam per samples!
            if self.is_paired:
                logging.getLogger("ng6").debug("bismark.process ")
                logging.getLogger("ng6").debug("bismark.process self.output_files = " + ",".join(self.output_files))
                logging.getLogger("ng6").debug("bismark.process self.stderrs = " + ",".join(self.stderrs)) 
                logging.getLogger("ng6").debug("bismark.process self.output_report = " + ",".join(self.output_report)) 
                logging.getLogger("ng6").debug("bismark.process self.output_bam = " + ",".join(self.output_bam)) 
                self.add_shell_execution( self.get_exec_path("bismark") + " " + self.args + " -o " + self.output_directory + " --temp_dir " + self.output_directory + \
                                " " + self.reference_directory  + " -1 $1 -2 $2 2> $4 ; ln -s $3 $6 2>> $4;" , 
                                cmd_format='{EXE} {IN} {OUT}', map=True,
                                inputs=[self.input_files_R1, self.input_files_R2], 
                                outputs=[self.output_files,self.stderrs,self.output_report,self.output_bam], includes=self.reference_genome)
            else:
                self.add_shell_execution( self.get_exec_path("bismark") + " " + self.args + " -o " + self.output_directory + " --temp_dir " + self.output_directory + \
                                " " + self.reference_directory  + " $1 2> $3 ; ln -s $2 $5 2>> $3;" , cmd_format='{EXE} {IN} {OUT}', map=True,
                                inputs=self.input_files_R1, outputs=[self.output_files,self.stderrs,self.output_report,self.output_bam], includes=self.reference_genome)

    def post_process(self):
        #self._save_files(self.output_bam)
        results_files = self.output_report
        logging.getLogger("ng6").debug("bismark.post_process self.output_report = " + ",".join(self.output_report)) 
        # Finally create and add the archive to the analysis
        for file in self.output_report:
                # faire un test si contient _trimmed prendre ce qu'il y a avant comme nom d'echantillon.
                file_woext=os.path.splitext(os.path.basename(file))[0]
                if "_trimmed" in file_woext :
                    sample = file_woext.split("_trimmed")[0]
                else :
                    sample = file_woext.split("_bismark")[0]
                
                report_info = self.__parse_report_file(file)
                if self.is_paired :
                    self._add_result_element(sample, "totalPairs", str(report_info["totalPairs"])) 
                    self._add_result_element(sample, "nbrPairsBestHit", str(report_info["nbrPairsBestHit"]))
                    self._add_result_element(sample, "nbrPairsNoAlignment", str(report_info["nbrPairsNoAlignment"]))
                    self._add_result_element(sample, "nbrPairsNoUniqAlignment", str(report_info["nbrPairsNoUniqAlignment"]))
                    self._add_result_element(sample, "nbrPairsCoordinateExtractionProblem", str(report_info["nbrPairsCoordinateExtractionProblem"]))
                else:
                    self._add_result_element(sample, "totalReads", str(report_info["totalReads"])) 
                    self._add_result_element(sample, "nbrReadsBestHit", str(report_info["nbrReadsBestHit"]))
                    self._add_result_element(sample, "nbrReadsNoAlignment", str(report_info["nbrReadsNoAlignment"]))
                    self._add_result_element(sample, "nbrReadsNoUniqAlignment", str(report_info["nbrReadsNoUniqAlignment"]))
                    self._add_result_element(sample, "nbrReadsCoordinateExtractionProblem", str(report_info["nbrReadsCoordinateExtractionProblem"]))
                self._add_result_element(sample, "mappingEfficiency", str(report_info["mappingEfficiency"]))
                self._add_result_element(sample, "CinCpG", str(report_info["CinCpG"]))
                self._add_result_element(sample, "CinCHG", str(report_info["CinCHG"]))
                self._add_result_element(sample, "CinCHH", str(report_info["CinCHH"]))
                if "CinUnknown" in report_info:
                    self._add_result_element(sample, "CinUnknown", str(report_info["CinUnknown"]))
                self._add_result_element(sample, "totalC", str(report_info["totalC"]))
                self._add_result_element(sample, "totalmCinCpG", str(report_info["totalmCinCpG"]))
                self._add_result_element(sample, "totalmCinCHG", str(report_info["totalmCinCHG"]))
                self._add_result_element(sample, "totalmCinCHH", str(report_info["totalmCinCHH"]))
                if "totalmCinUnknown" in report_info :
                    self._add_result_element(sample, "totalmCinUnknown", str(report_info["totalmCinUnknown"]))
                self._add_result_element(sample, "totalCinCpG", str(report_info["totalCinCpG"]))
                self._add_result_element(sample, "totalCinCHG", str(report_info["totalCinCHG"]))
                self._add_result_element(sample, "totalCinCHH", str(report_info["totalCinCHH"]))
                if "totalCinUnknown" in report_info :
                    self._add_result_element(sample, "totalCinUnknown", str(report_info["totalCinUnknown"]))
                
        # Finaly create and add the archive to the analysis
        self._create_and_archive(results_files, "bismarkLogResult.tar.gz")
      
    def __parse_report_file (self, report_file):
        """
        Parse the data file
          @param data_file : the fastqc data file
          @return          : {"nbseq" : x, ...}
        """
        stats = {}
        logging.getLogger("ng6").debug("bismark.__parse_report_file report_file = " + report_file) 
        for line in open(report_file, 'r').readlines():
            logging.getLogger("ng6").debug("bismark.__parse_report_file line = " + line + ", line.strip = " + ",".join(line.strip().split())) 
            #if paired
            if line.startswith("Sequence pairs analysed in total:"):
                stats["totalPairs"] = line.strip().split()[-1]
            if line.startswith("Number of paired-end alignments with a unique best hit"):
                stats["nbrPairsBestHit"] = line.strip().split()[-1]
            if line.startswith("Sequence pairs with no alignments under any condition:"):
                stats["nbrPairsNoAlignment"] = line.strip().split()[-1]
            if line.startswith("Sequence pairs did not map uniquely:"):
                stats["nbrPairsNoUniqAlignment"] = line.strip().split()[-1]
            if line.startswith("Sequence pairs which were discarded because genomic sequence could not be extracted:"):
                stats["nbrPairsCoordinateExtractionProblem"] = line.strip().split()[-1]
                            
            #if single 
            if line.startswith("Sequences analysed in total:"):
                stats["totalReads"] = line.strip().split()[-1]
            if line.startswith("Number of alignments with a unique best hit from the different alignments:"):
                stats["nbrReadsBestHit"] = line.strip().split()[-1]
            if line.startswith("Sequences with no alignments under any condition:"):
                stats["nbrReadsNoAlignment"] = line.strip().split()[-1]
            if line.startswith("Sequences did not map uniquely:"):
                stats["nbrReadsNoUniqAlignment"] = line.strip().split()[-1]
            if line.startswith("Sequences which were discarded because genomic sequence could not be extracted:"):
                stats["nbrReadsCoordinateExtractionProblem"] = line.strip().split()[-1]
            
            
            if line.startswith("Mapping efficiency:"):
                stats["mappingEfficiency"] = line.strip().split()[-1]
            if line.startswith("C methylated in CpG context:"):
                stats["CinCpG"] = line.strip().split()[-1]
            if line.startswith("C methylated in CHG context:"):
                stats["CinCHG"] = line.strip().split()[-1]
            if line.startswith("C methylated in CHH context:"):
                stats["CinCHH"] = line.strip().split()[-1]
            if line.startswith("C methylated in unknown context (CN or CHN):"):
                stats["CinUnknown"] = line.strip().split()[-1]
            if line.startswith("Can't determine percentage of methylated Cs in CpG context if value was 0"):
                stats["CinCpG"] = 'NA'
            if line.startswith("Can't determine percentage of methylated Cs in CHG context if value was 0"):
                stats["CinCHG"] = 'NA'
            if line.startswith("Can't determine percentage of methylated Cs in CHH context if value was 0"):
                stats["CinCHH"] = 'NA'
            if line.startswith("Can't determine percentage of methylated Cs in unknown context (CN or CHN) if value was 0"):
                stats["CinUnknown"] = 'NA'
            if line.startswith("Total number of C's analysed:"):
                stats["totalC"] = line.strip().split()[-1]
                logging.getLogger("ng6").debug("bismark.__parse_report_file totalC line.strip = " + line.strip().split()[-1]) 
            if line.startswith("Total methylated C's in CpG context:"):
                stats["totalmCinCpG"] = line.strip().split()[-1]
            if line.startswith("Total methylated C's in CHG context:"):
                stats["totalmCinCHG"] = line.strip().split()[-1]
            if line.startswith("Total methylated C's in CHH context:"):
                stats["totalmCinCHH"] = line.strip().split()[-1]
            if line.startswith("Total methylated C's in Unknown context:"):
                stats["totalmCinUnknown"] = line.strip().split()[-1]
            if line.startswith("Total unmethylated C's in CpG context:"):
                stats["totalCinCpG"] = line.strip().split()[-1]
            if line.startswith("Total unmethylated C's in CHG context:"):
                stats["totalCinCHG"] = line.strip().split()[-1]
            if line.startswith("Total unmethylated C's in CHH context:"):
                stats["totalCinCHH"] = line.strip().split()[-1]
            if line.startswith("Total unmethylated C's in Unknown context:"):
                stats["totalCinUnknown"] = line.strip().split()[-1]

        return stats

    def get_version(self):
        cmd = [self.get_exec_path("bismark"),"--version"]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stdout.split()[9]
