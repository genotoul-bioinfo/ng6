#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import logging

from subprocess import Popen, PIPE
 
from ng6.analysis import Analysis

class TrimGalore (Analysis):
    
    def define_parameters(self, input_files_R1, input_files_R2=None, non_directional=False, rrbs=False, quality=20, phred=33, adapter=None, stringency=1, error_rate=0.1, length=20):
        self.is_paired = False
        self.add_input_file_list( "input_files_R1", "input files read1", default=input_files_R1, required=True, file_format = 'fastq')
        self.add_output_file_list( "report_files_R1", "report files read1", pattern='{basename}_trimming_report.txt', items=self.input_files_R1)
        if input_files_R2 :
            self.add_output_file_list( "output_files_R1", "output files read1", pattern='{basename_woext}_val_1.fq.gz', items=self.input_files_R1)
            self.is_paired = True
            self.add_input_file_list( "input_files_R2", "input files read2", default=input_files_R2, required=True, file_format = 'fastq')
            self.add_output_file_list( "output_files_R2", "output files read2", pattern='{basename_woext}_val_2.fq.gz', items=self.input_files_R2)
            self.add_output_file_list( "report_files_R2", "report files read2", pattern='{basename}_trimming_report.txt', items=self.input_files_R2)
        else :
            self.add_output_file_list( "output_files_R1", "output files read1", pattern='{basename_woext}_trimmed.fq.gz', items=self.input_files_R1)
            
        self.add_output_file_list( "stderrs", "stderr files", pattern='{basename_woext}.stderr', items=self.input_files_R1)
        self.add_output_file_list( "stdouts", "stdout files", pattern='{basename_woext}.stdout', items=self.input_files_R1)        
        self.add_parameter("quality", "Instructs Cutadapt to use ASCII+33 ", default=quality,type='int')
        self.add_parameter("phred", "Instructs Cutadapt to use ASCII+33 or ASCII+64", default=phred, type="int", choices=[33,64])
        self.add_parameter("adapter", "Adapter sequence to be trimmed", default=adapter)
        self.add_parameter("stringency", "Overlap with adapter sequence required to trim a sequence.", default=stringency, type="int")
        self.add_parameter("error_rate", "Maximum allowed error rate ", default=error_rate, type="float")
        self.add_parameter("length", "Discard reads that became shorter than length", default=length, type="int")
                        
        self.add_parameter("rrbs", "is rrbs data", default=rrbs, type="bool")
        self.add_parameter("non_directional", "Selecting this option for non-directional RRBS", default=non_directional, type="bool")
        
    def define_analysis(self):
        self.name = "Cleaning"
        self.description = "Adapter and quality trimming"
        self.software = "TrimGalore"
        
        self.options =  " --quality "+str(self.quality)+" --stringency "+ str(self.stringency) +" -e " + str(self.error_rate) +" --length " + str(self.length)
        if self.adapter != None and self.adapter != "" :
            self.options += " --adapter "+self.adapter
            
        if self.is_paired :
            self.options += " --paired"
        
        if self.rrbs :
            self.options += " --rrbs"
            if self.non_directional :
                self.options += " --non_directional"
            if self.is_paired :
                 self.options += " --trim1"
        
        if self.phred == 33 : 
            self.options += " --phred33"
        else : 
            self.options += " --phred64"    
        
        #cutadapt path
        #cutadapt_path = self.get_exec_path("cutadapt")
        #self.options += " --path_to_cutadapt "+cutadapt_path
        
    def process(self):
        
        if self.is_paired: 
            self.add_shell_execution( "module load bioinfo/Cutadapt/1.8.3;module load bioinfo/TrimGalore/0.4.5;"+ self.get_exec_path("trim_galore") + " " + self.options + \
                            " -o " + self.output_directory + " $1 $2 >$3 2>> $4 ", 
                            cmd_format='{EXE} {IN} {OUT}', map=True, inputs=[self.input_files_R1, self.input_files_R2], 
                            outputs=[self.stderrs,self.stdouts,self.output_files_R1,self.output_files_R2,self.report_files_R1,self.report_files_R2])
        else : 
            self.add_shell_execution( "module load bioinfo/Cutadapt/1.8.3;module load bioinfo/TrimGalore/0.4.5;"+ self.get_exec_path("trim_galore") + " " + self.options + \
                            " -o " + self.output_directory + " $1 > $2 2>> $3 ", cmd_format='{EXE} {IN} {OUT}', map=True,
                            inputs=[self.input_files_R1], outputs=[self.stderrs,self.stdouts,self.output_files_R1,self.report_files_R1])
 
    def post_process(self):
        
        for file in self.report_files_R1:
            sample = os.path.basename(file).replace(".fastq.gz_trimming_report.txt", "")
            report_info = self.__parse_report_file(file, "R1")
            for key,val in list(report_info.items()) :
                    self._add_result_element(sample, key, str(val))
        # Finally add report files
        self._save_files(self.report_files_R1)
        if self.is_paired :
            for file in self.report_files_R2:
                sample = os.path.basename(file).replace(".fastq.gz_trimming_report.txt", "")
                report_info = self.__parse_report_file(file, "R2")
                for key,val in list(report_info.items()) :
                    self._add_result_element(sample, key, str(val))
            # Finally add report files
            self._save_files(self.report_files_R2)
        
        
        
        
    def __parse_report_file (self, report_file, read):
        """
        Parse the data file
          @param data_file : the trim_galore report file
          @return          : {"nbseq" : x, ...}
        """
        stats = {}
        #Trimgalore can use cutadapt 1.7 or 1.8
        for line in open(report_file, 'r').readlines():
            
            #cutadapt 1.7
            if line.startswith("   Processed reads:"):
                stats["total_reads"] = line.strip().split()[-1]
            if line.startswith("   Processed bases:"):
                stats["total_bases"] = line.strip().split()[-4]
            if line.startswith("     Trimmed reads:"):
                stats["trimmed_reads"] = line.strip().split()[-2]
            if line.startswith("   Quality-trimmed:"):
                stats["qual_trimmed_bases"] = line.strip().split()[-7]
            if line.startswith("     Trimmed bases"):
                stats["trimmed_bases"] = line.strip().split()[-7]
            if line.startswith("   Too short reads"):
                stats["too_short_reads"] = line.strip().split()[-5]
            
            #cutadapt 1.8
            if line.startswith("Total reads processed:"):
                stats["total_reads"] = line.strip().split()[-1].replace(",","")
            if line.startswith("Total basepairs processed:"):
                stats["total_bases"] = int(line.strip().split()[-2].replace(",",""))
            if line.startswith("Reads with adapters:"):
                stats["trimmed_reads"] = line.strip().split()[-2].replace(",","")
            if line.startswith("Quality-trimmed:"):
                stats["qual_trimmed_bases"] = line.strip().split()[-3].replace(",","")
            if line.startswith("Total written (filtered):"):
                stats["trimmed_bases"] = stats["total_bases"]-int(line.strip().split()[-3].replace(",",""))
                       
            #trimgalore values 
            if line.startswith("Number of sequence pairs removed because at least"):
                stats["pairs_removed"] = line.strip().split()[-2]
            if line.startswith("RRBS reads trimmed by additional 2 bp when adapter contamination was detected:"):
                stats["rrbs_2bp_apdater"] = line.strip().split()[-2]
            if line.startswith("RRBS reads trimmed by 2 bp at the start when read started with"):
                stats["rrbs_2bp_read_CAA"] = line.strip().split()[-8].replace('(','').replace(')','')
                stats["rrbs_2bp_read_CGA"] = line.strip().split()[-5].replace('(','').replace(')','')
        return stats
    
    def get_version(self):
        cmd = [self.get_exec_path("trim_galore"),"--version"]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stdout.split()[5]
