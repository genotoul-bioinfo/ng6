#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import logging

from subprocess import Popen, PIPE
from ng6.analysis import Analysis

import re

class BismarkMethylationExtractor (Analysis):
    
    def define_parameters(self, reference_genome, bams, type="single", no_overlap=True, large_genome=False, compression="tar.gz"):
        
        self.add_parameter("type", "type", default=type)
        self.add_parameter("no_overlap", "no_overlap", default=no_overlap, type="bool")
        self.add_parameter("large_genome", "large_genome", default=large_genome, type="bool")
        self.add_parameter("compression", "compression", default=compression)
        self.add_input_file( "reference_genome", "input reference genome", default=reference_genome, required=True, file_format = 'fasta')
        self.add_input_file_list( "bams", "input bams files", default=bams, required=True, file_format='bam')
        self.add_output_file_list( "stderrs", "stderr files", pattern='{basename_woext}.stderr', items=self.bams)
        self.add_output_file_list( "output_directories", "output directories", pattern='{basename_woext}', items=self.bams)
        
    def define_analysis(self):
        self.name = "Methylation Extraction"
        self.description = "Methylation extraction from Bismark output"
        self.software = "bismark_methylation_extractor"
        #TO DO add parameter to handle memory option
        self.options = " --gzip --report --counts --cytosine_report --zero_based --CX_context --buffer_size 8G" 
        if self.type == "paired":
            self.options += " --paired-end"
            if self.no_overlap :
                self.options += " --no_overlap"
        elif self.type == "single" :
            self.options += " --single-end"
        if self.large_genome :
            self.options += " --scaffolds"

    def process(self):
        self.add_shell_execution("mkdir $2; cd $2; " + self.get_exec_path("bismark_methylation_extractor") + " $1 " + self.options + \
                                " --genome_folder " + os.path.dirname(self.reference_genome) + " -o $2 2>> $3;", 
                                cmd_format='{EXE} {IN} {OUT}', map=True,
                                inputs=self.bams, outputs=[self.output_directories,self.stderrs], includes=self.reference_genome)
    
         

    def post_process(self):
        logging.getLogger("ng6").debug("BismarkMethylationExtractor.post_process "  +", ".join(self.output_directories))
        for directory in self.output_directories :
           methylationExtractorResultFiles = []
           sample = os.path.basename(directory)
           for file in os.listdir(directory):
               logging.getLogger("ng6").debug("BismarkMethylationExtractor.file "  +file + " in " + directory)
               if file.endswith("M-bias.txt"):
                   graph=self.__parse_mbias_file(os.path.join(directory,file))
                   for read in list(graph.keys()):
                       for context in list(graph[read].keys()):
                           self._add_result_element(sample, "labels", ",".join(graph[read][context]["read_pos"]), read+"_"+context )
                           self._add_result_element(sample, "methylated", ",".join(graph[read][context]["Cmeth"]), read+"_"+context )
                           self._add_result_element(sample, "unmethylated", ",".join(graph[read][context]["Cunmeth"]), read+"_"+context )
                   
               
               if file.endswith("splitting_report.txt"):
                   logging.getLogger("ng6").debug("BismarkMethylationExtractor. file "  +file + " is splitting_report.txt")
                   report_info = self.__parse_report_file(os.path.join(directory, file))
                   self._add_result_element(sample, "totalC", str(report_info["totalC"]))
                   self._add_result_element(sample, "totalMethylatedCinCpG", str(report_info["totalMethylatedCinCpG"]))
                   self._add_result_element(sample, "totalMethylatedCinCHG", str(report_info["totalMethylatedCinCHG"]))
                   self._add_result_element(sample, "totalMethylatedCinCHH", str(report_info["totalMethylatedCinCHH"]))
                   self._add_result_element(sample, "totalUnmethylatedCinCpG", str(report_info["totalUnmethylatedCinCpG"]))
                   self._add_result_element(sample, "totalUnmethylatedCinCHG", str(report_info["totalUnmethylatedCinCHG"]))
                   self._add_result_element(sample, "totalUnmethylatedCinCHH", str(report_info["totalUnmethylatedCinCHH"]))
                   
               if not file.endswith(".bam") :
                   methylationExtractorResultFiles.append(os.path.join(directory, file))
           #Not necesary
           #self._archive_files(methylationExtractorResultFiles,self.compression,os.path.basename(directory)+".tar") 
           
    def __parse_report_file (self, report_file):
        logging.getLogger("ng6").debug("BismarkMethylationExtractor.__parse_report_file "  +report_file)
        """
        Parse the data file
          @param data_file : the fastqc data file
          @return          : {"nbseq" : x, ...}
        """
        stats = {}
        for line in open(report_file, 'r').readlines():
            logging.getLogger("ng6").debug("BismarkMethylationExtractor.__parse_report_file line"  +line)
            if line.startswith("Total number of C's analysed:"):
                stats["totalC"] = line.strip().split()[-1]
                logging.getLogger("ng6").debug("BismarkMethylationExtractor.__parse_report_file totalC = "  +stats["totalC"])
            if line.startswith("Total methylated C's in CpG context:"):
                stats["totalMethylatedCinCpG"] = line.strip().split()[-1]
            if line.startswith("Total methylated C's in CHG context:"):
                stats["totalMethylatedCinCHG"] = line.strip().split()[-1]
            if line.startswith("Total methylated C's in CHH context:"):
                stats["totalMethylatedCinCHH"] = line.strip().split()[-1]
            if line.startswith("Total C to T conversions in CpG context:"):
                stats["totalUnmethylatedCinCpG"] = line.strip().split()[-1]
            if line.startswith("Total C to T conversions in CHG context:"):
                stats["totalUnmethylatedCinCHG"] = line.strip().split()[-1]
            if line.startswith("Total C to T conversions in CHH context:"):
                stats["totalUnmethylatedCinCHH"] = line.strip().split()[-1]
        return stats
    
    def __parse_mbias_file (self, mbias_file):
        """
        Parse the file containing mbias graph values
          @param data_file : mbias graph data file
          @return          : {"R1" : {"CpG" : {read_pos : [1,2,3...] ,
                                               Cmeth : [0,10,2,..],
                                               Cunmeth : [5,3,4,..]},
                                        "CHH" : {...},
                                        }
                              "R2" : {}}
        """
        graph = {}
        prog = re.compile('(\w{3}) context\s?\(?(R\d)?\)?')
        fin = open(mbias_file, 'r')
        read="R1"
        context="CpG"
        for line in fin:
            tab = line.split ("\t")
            if prog.match(line) :
                m = prog.match(line)
                context=m.group(1)
                if m.group(2) is not None :
                    read=m.group(2)
                else :
                    read="single"
                if not (read in graph) :  graph[read]={}
                graph[read][context]={"read_pos":[],"Cunmeth":[],"Cmeth":[]}
            elif len(tab) >= 5 and tab[0] != "position"  :
                graph[read][context]["read_pos"].append(tab[0])
                graph[read][context]["Cmeth"].append(tab[1])
                graph[read][context]["Cunmeth"].append(tab[2])
        return graph
    def get_version(self):
        cmd = [self.get_exec_path("bismark_methylation_extractor"),"--version"]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stdout.split()[6]
