#
# Copyright (C) 2015 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from ng6.analysis import Analysis
from subprocess import Popen, PIPE
import re
import os
class RemoveDuplicate (Analysis):

    def define_parameters(self, bams, is_paired=True, mem="5G", cpu=2):
        self.add_input_file_list( "bam", "SORTED bam files.", default=bams, required=True )
        names=[]
        self.temp_sorted1=[]
        self.temp_sorted2=[]
        self.temp_rmdup=[]
        for file in bams :
            f=os.path.splitext(os.path.basename(file))[0]
            names.append(os.path.splitext(os.path.basename(file))[0])
            self.temp_sorted1.append(os.path.join(self.output_directory,f+"_sort1.bam"))
            self.temp_sorted2.append(os.path.join(self.output_directory,f+"_sort2.bam"))
            self.temp_rmdup.append(os.path.join(self.output_directory,f+"_rmdup.bam"))
            
        self.add_parameter("is_paired", "Does library is paired ? ", default=is_paired, type="bool")
        self.add_parameter("mem", "Memory for samtools ", default=mem, type="string")
        self.add_parameter("cpu", "Cpu for samtools ", default=cpu, type="int")
        if self.get_cpu() != None :
            self.cpu=self.get_cpu()
        if self.get_memory() != None :
            self.mem=self.get_memory()
        self.add_output_file_list("flagstat_init", "Flagstat initialy", pattern='{basename_woext}.init_flagstat', items=self.bam)
        self.add_output_file_list("flagstat_finally", "Flagstat result after removing singleton", pattern='{basename_woext}.finally_flagstat', items=self.bam)
        if self.is_paired:
            self.add_output_file_list("flagstat_rmdup", "Flagstat result after rmdup", pattern='{basename_woext}.rmdup_flagstat', items=self.bam)
            self.add_output_file_list("rmdup_stderr", "The error trace file", pattern='{basename_woext}_rmdup.stderr', items=self.bam)       
        self.add_output_file_list("output", "The bam with removed duplicates (and singleton if paired)", pattern='{basename_woext}_clean.bam', items=self.bam)
        
        self.add_output_file_list("rmsinglet_stderr","The error trace file", pattern='{basename_woext}_rmsinglet.stderr', items=self.bam)
    
    def define_analysis(self):
        self.name = "Remove duplicate"
        self.description = "Remove PCR duplicate with samtools"
        self.software = "samtools"
        #TO DO add parameter to handle memory option
        self.options = "samtools sort|samtools flagstat|samtools rmdup" 
        if self.is_paired:
            self.description += " and remove singleton reads"
            self.options += "|samtools flagstat|remove singleton|samtools flagstat"
        else :
            self.options +=" -s |samtools flagstat"
    def get_version(self):
        cmd = [self.get_exec_path("samtools_old")]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return str(stderr).split("\\n")[2].replace("Version: ","")
         
    def process(self):
        
        self.add_shell_execution(self.get_exec_path("samtools") + " flagstat $1 > $2", cmd_format='{EXE} {IN} {OUT}',
                                inputs=[self.bam], outputs=[self.flagstat_init], map=True)
        self.add_shell_execution(self.get_exec_path("samtools") + " sort -m "+self.mem+" -@"+str(self.cpu)+" $1 -o $2",
                                cmd_format='{EXE} {IN} {OUT}', inputs=[self.bam], outputs=[self.temp_sorted1], map=True)
        if self.is_paired :
            #samtools rmdup
            self.add_shell_execution(self.get_exec_path("samtools_old") + " rmdup $1 $2 2>> $3", cmd_format='{EXE} {IN} {OUT}',
                                inputs=[self.temp_sorted1], outputs=[self.temp_rmdup,self.rmdup_stderr], map=True)
            self.add_shell_execution(self.get_exec_path("samtools") + " flagstat $1 > $2", cmd_format='{EXE} {IN} {OUT}',
                    inputs=[self.temp_rmdup], outputs=[self.flagstat_rmdup], map=True)
            #remove singleton after rmdup
            
            self.add_shell_execution(self.get_exec_path("samtools") + " sort -n -m "+self.mem+" -@"+str(self.cpu)+" $1 -o $2 | "+ \
                                 self.get_exec_path("samtools") + " view -@"+str(self.cpu)+" -h - | "+\
                                 "awk '/^@/{print;next}$1==id{print l\"\\n\"$0;next}{id=$1;l=$0}' | "  + \
                                 self.get_exec_path("samtools") + " view -@"+str(self.cpu)+" -bS - > $2 2>>$3", 
                                 cmd_format='{EXE} {IN} {OUT}',
                                 inputs=[self.temp_rmdup], outputs=[self.output,self.rmsinglet_stderr], map=True)

            
        else :
            #samtools rmdup
            self.add_shell_execution(self.get_exec_path("samtools_old") + " rmdup -s $1 $2 2>> $3", cmd_format='{EXE} {IN} {OUT}',
                                inputs=[self.temp_sorted1], outputs=[self.output,self.rmdup_stderr], map=True)
           
        self.add_shell_execution(self.get_exec_path("samtools") + " flagstat $1 > $2", cmd_format='{EXE} {IN} {OUT}',
                    inputs=[self.output], outputs=[self.flagstat_finally], map=True)
        
    def post_process(self):
        bismark_analysis = Analysis.get_from_id(self.parent.id)
        bismark_analysis._save_files(self.output)
        #parse flagstats
        for file in self.flagstat_init :
            sample=os.path.splitext(os.path.basename(file))[0]
            self._add_result_element(sample, "input", str(self.get_mapped_flagstat(file)))
        if self.is_paired :
            for file in self.flagstat_rmdup :
                sample=os.path.splitext(os.path.basename(file))[0]
                self._add_result_element(sample, "rmdup", str(self.get_mapped_flagstat(file)))
        
        for file in self.flagstat_finally :
            sample=os.path.splitext(os.path.basename(file))[0]
            self._add_result_element(sample, "cleaned", str(self.get_mapped_flagstat(file)))    
               
    def get_mapped_flagstat(self , file):
        pattern=re.compile("(\d+) \+ \d+ mapped ")

        fh=open(file)
        for line in fh :
            if pattern.match(line) :
                m=pattern.match(line)
                return m.group(1)
                
        
        