#
# Copyright (C) 2012 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import sys

from jflow.utils import  display_error_message

from ng6.ng6workflow import BasicNG6Workflow
from ng6.project import Project
from ng6.run import Run
from ng6.analysis import Analysis

class AddAnalysis (BasicNG6Workflow):

    def get_description(self):
        return "Add a brand new analysis to a run or a project"

    def define_parameters(self, function="process"):
        self.add_input_file_list("data_file", " Which data files should be added to the analysis", required = True)
        self.add_parameter("compression", "How should data be compressed once archived", choices = ['none', 'gz', 'bz2'], default = 'none')
        self.add_parameter("delete", "Should the input data be deleted once the process is over", type = bool , default = False)
        self.add_parameter("archive_name", "Give a name to the final archive", default = 'ng6_archive')
        self.add_parameter("project_id", "The project id the analysis belongs to", type = 'existingprojectid', rules="Exclude=run_id;RequiredIf?ALL[run_id=None]")
        self.add_parameter("run_id", "The run id the run belongs to", type = 'existingrun')
        self.add_parameter("analysis_name", "Give a name to your analysis", required = True)
        self.add_parameter("analysis_description", "Give a description to your analysis", required = True)
        self.add_parameter("analysis_software", "Which software was used for this analysis", required = True)
        self.add_parameter("analysis_software_options", "Which software options were used for this analysis", required = True)
        self.add_parameter("analysis_software_version", "analysis_software_version", required = True)
        self.add_parameter("parent_analysis", "The id of an analysis to be used as a parent analysis", type= 'int')


    def process(self):
        self.runobj = None
        self.project = None
        # First check if files provided exists
        files_to_save = []
        for file in self.data_file:
            if os.path.isfile(file):
                files_to_save.append(file)
            else:
                display_error_message("error: %s file does not exists\n" % (file))

        # Check if user provided a project or a run
        space_id="default"
        if self.project_id :
            self.project = Project.get_from_id(self.project_id)
            self.metadata.append("project_id="+str(self.project_id))
            space_id = self.project.space_id
            # if user is not allowed to add data on project (is not admin)
            if self.project is not None and not self.project.is_admin(self.admin_login):
                display_error_message( "The user login '" + self.admin_login + "' is not allowed to add data on project '" + self.project.name + "'.\n" )

        elif self.run_id :
            self.runobj = Run.get_from_id(self.run_id)
            project = Project.get_from_run_id(self.run_id)
            space_id = project.space_id
            self.metadata.append("run_id="+str(self.run_id))
        else :
            display_error_message("One of run_id or project_id parameter is required!\n")

        addto = "project" if self.project else "run"
        parent = None
        if self.parent_analysis :
            parent = Analysis.get_from_id(self.parent_analysis)
            if self.project :
                analyses_ids = [ o.id for o in self.project.get_analysis()]
            else :
                analyses_ids = [ o.id for o in self.runobj.get_analysis()]
            if parent.id not in analyses_ids :
                    display_error_message("The parent analysis associated with id '%s' does not belong to the %s id '%s' " % (parent.id, addto, self.project_id or self.run_id) )

        basicanalysis = self.add_component("BasicAnalysis", [files_to_save, self.analysis_name,
                        self.analysis_description, self.analysis_software, self.analysis_software_options,
                        self.analysis_software_version, self.compression, self.delete,
                        self.archive_name, space_id], addto=addto, parent = parent)

    def post_process(self):
        if self.runobj:
            self.runobj.sync()
        elif self.project:
            self.project.sync()
