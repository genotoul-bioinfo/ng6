#
# Copyright (C) 2015 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import pickle

from jflow.component import Component

from weaver.function import PythonFunction

def get_urls( output_url, ids_dump_path, ng6_username = None):
    import os
    import pickle
    import subprocess
    import sys
    import re
    
    from ng6.utils import Utils
    from ng6.config_reader import NG6ConfigReader
    
    ng6config = NG6ConfigReader()
    data_directory = ng6config.get_save_directory()
    if ng6_username in ['', 'None', 'none', 'null', None] :
        ng6_username = None
        
    fh = open(ids_dump_path, "rb")
    prefixed_ids = pickle.load(fh)
    fh.close()
    
    prefixed_ids = ";".join(prefixed_ids)
    src_directories, dest_directories = Utils.get_directories_structure_and_content(ng6_username, data_directory, '/fake/output/', prefixed_ids)
    
    files = []
    for i, source_dir in enumerate(src_directories):
        
        for filename in os.listdir(source_dir):
            source_file_path = os.path.join(source_dir, filename)
            if os.path.isfile(source_file_path) and filename not in [ "analyse.xml", "index.html", "run.cfg"] :
                files.append(re.sub( r"\/+", "/", source_file_path.replace(data_directory, "")))
    
    with open(output_url, "w") as fh :
        for f in files :
            fh.write(f+"\n")

class GetURLS (Component):
    
    def define_parameters(self, data_ids = [], run_ids = [], analysis_ids = [], login = None):
        self.add_parameter('login', 'The login of the user in ng6', type = 'ng6userlogin', default = login)
        self.add_parameter_list('data_ids', 'Ids of a run from which rawdata will be retrieved', default = data_ids)
        self.add_parameter_list('run_ids', 'Ids of run from which all data will be retrieved', default = run_ids)
        self.add_parameter_list('analysis_ids', 'Ids of analysis to retrieve', default = analysis_ids)
        
        self.add_output_file("output_url", "A file with a list of created urls", filename = "output_urls.txt")
        
    def process(self):
        run_ids = [ "run_%s"%i for i in self.run_ids ]
        data_ids = [ "data_%s"%i for i in self.data_ids ]
        analysis_ids = [ "analyse_%s"%i for i in self.analysis_ids ]
        
        ids_dump_path = self.get_temporary_file(".dump")
        fh =  open(ids_dump_path, "wb")
        pickle.dump( run_ids + data_ids + analysis_ids, fh)
        fh.close()
        
        fn = PythonFunction(get_urls, cmd_format = "{EXE} {IN} {OUT} {ARG} ")
        fn(arguments = [ ids_dump_path, self.login], outputs = self.output_url, local = True)
    
    