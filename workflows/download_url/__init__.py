#
# Copyright (C) 2015 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from ng6.ng6workflow import DownloadWorkflow

class DownloadURL (DownloadWorkflow):

    def get_description(self):
        return """This workflow will create a list of url of each file of your project"""
    
    def define_parameters(self, function="process"):
        pass
    
    def process(self):
        self.add_component('GetURLS', [self.data_id, self.run_id, self.analysis_id, self.admin_login])
        
