#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import pickle
import logging

from jflow.component import Component

def extract_stats_from_seq_file(input_file, output_file_stat):
    import jflow.seqio as seqio
    import logging
    import os
    logging.getLogger("AddRawFiles").debug("extract_stats_from_seq_files. Entering, working on " + input_file)
    nb_seq, full_size = 0, 0
    try:
        # Get nb_seq and full_size values
        reader = seqio.SequenceReader(input_file)
        for id, desc, seq, qualities in reader:
             nb_seq += 1
             full_size += len(seq)
        f = open(output_file_stat, 'w')
        f.write(str(nb_seq)+":"+str(full_size))  # python will convert \n to os.linesep
        f.close()
    except:
        logging.getLogger("AddRawFiles").debug(file + " seems to not be a seq file")

    logging.getLogger("AddRawFiles").debug("extract_stats_from_seq_files. finished")

def md5sum(md5_file_out, files_to_md5sum):    
    import jflow.seqio as seqio
    import logging
    import os
    from subprocess import Popen, PIPE
    logging.getLogger("AddRawFiles").debug("md5sum. entering")
    logging.getLogger("AddRawFiles").debug("md5sum. md5sum in this directory : "+os.path.dirname(md5_file_out))
    logging.getLogger("AddRawFiles").debug("md5sum. Generating the md5sum.txt with this command : " +
                                           "find " + os.path.dirname(files_to_md5sum)  + " -maxdepth 1 -type f -not -name " + os.path.basename(md5_file_out) +
                                           " -exec md5sum {} \; | awk -F/ {'print $1$NF'} > "+ md5_file_out)
    p = Popen("find " + os.path.dirname(files_to_md5sum)  + " -maxdepth 1 -type f -not -name " + os.path.basename(md5_file_out) + " -exec md5sum {} \;" + 
              "| awk -F/ {'print $1$NF'} > "+ md5_file_out, shell = True, stdout = PIPE, stderr = PIPE, universal_newlines = True)
    stdout,stderr = p.communicate()
    logging.getLogger("AddRawFiles").debug("md5sum. finished")
    
def add_stats_to_run (run_dump_path, file):
    import pickle
    import logging
    total_nb_seq = 0
    total_size = 0
    logging.getLogger("AddRawFiles").debug("add_stats_to_run. Working on files of the directory containing this one : " + file)
    file_list = [ f for f in os.listdir(os.path.dirname(file)) if f.endswith('.count')]
    logging.getLogger("AddRawFiles").debug("add_stats_to_run. Length on the file_list : " + str(len(file_list)))
    logging.getLogger("AddRawFiles").debug("add_stats_to_run. Files on the file_list : " + str(file_list))
    for curr_stat_file in file_list:
        logging.getLogger("AddRawFiles").debug("add_stats_to_run. Content of " + os.path.dirname(file) + "/" + curr_stat_file)
        with open(os.path.dirname(file) + "/" + curr_stat_file, 'r') as myfile:
            nb_seq,size=myfile.read().split(':')
            logging.getLogger("AddRawFiles").debug("add_stats_to_run. Content of " + curr_stat_file + " nb_seq = " + str(nb_seq) + ", size = " + str(size))
        size= int(size)
        nb_seq= int(nb_seq)
        total_size+=size
        total_nb_seq+=nb_seq
    # load the analysis object
    run_dump = open(run_dump_path, "rb")
    my_run = pickle.load(run_dump)
    run_dump.close()
    logging.getLogger("AddRawFiles").debug("add_stats_to_run. Before my_run.process_raw_files total_nb_seq = " + str(total_nb_seq) + ", total_size = " + str(total_size))
    logging.getLogger("AddRawFiles").debug("add_stats_to_run. work_directory of the run "+ my_run.get_work_directory())
    my_run.process_raw_files(total_size,total_nb_seq)
    logging.getLogger("AddRawFiles").debug("add_stats_to_run. after my_run.process_raw_files, does the work dir ("+my_run.get_work_directory()+") exist? "+ str(os.path.isdir(my_run.get_work_directory())))
    logging.getLogger("AddRawFiles").debug("add_stats_to_run. Content of the folder ("+my_run.get_work_directory()+" = " + ",".join(os.listdir(my_run.get_work_directory())))

def copy_file (file,output):
    import os
    import logging
    from shutil import copyfile
    logging.getLogger("AddRawFiles").debug("copy_file. copy " + str(file) +" to " + str(output))
    
    try:
        logging.getLogger("AddRawFiles").debug("copy_file. Does work_directory of the run ("+os.path.dirname(output)+")exists?" + str(os.path.isdir(os.path.dirname(output))))
        copyfile(file, output)
        logging.getLogger("AddRawFiles").debug("copy_file. Content of the folder ("+os.path.dirname(output)+" = " + ",".join(os.listdir(os.path.dirname(output))))
    except:
        logging.getLogger("AddRawFiles").error("copy_file. Error raised while copying "+file +" to " + output)
        #raise Exception('Could not ')

def zip_file (file,output):
    import os
    import logging
    from ng6.utils import Utils
    
    logging.getLogger("AddRawFiles").debug("zip_file. zip " + str(file) +" to " + os.path.dirname(output))
    Utils.gzip(file, os.path.dirname(output))
    
def bzip2_file (file,output):
    import os
    import logging
    from ng6.utils import Utils
    
    logging.getLogger("AddRawFiles").debug("bzip2_file. zip " + str(file) +" to " + os.path.dirname(output))
    Utils.bz2(file, os.path.dirname(output))
    
def tar_zip_files (output,files):
    import os
    import logging
    from ng6.utils import Utils
    
    logging.getLogger("AddRawFiles").debug("tar_zip_files.")
    Utils.tar_files(files, os.path.join(os.path.dirname(output), "ng6_archive.tar"), delete)
    Utils.gzip(os.path.join(os.path.dirname(output), archive_name), os.path.dirname(output), False)

    
def tar_bz2_files (output, files):
    import os
    import logging
    from ng6.utils import Utils
    
    logging.getLogger("AddRawFiles").debug("tar_bz2_files.")
    Utils.tar_files(files, os.path.join(self.__get_work_directory(), archive_name), delete)
    Utils.bz2(os.path.join(self.__get_work_directory(), archive_name), self.__get_work_directory(), False)
     
class AddRawFiles (Component):
    
    def define_parameters(self, runobj, files_to_save, compression):
        self.runobj = runobj
        self.add_input_file_list( "files_to_save", "File to be saved as raw files", default=files_to_save, required=True)
        self.add_parameter("compression", "How should data be compressed once archived", default=compression, required = True)
        self.add_output_file("stdout", "AddRawfiles stdout", filename="addRawFiles.stdout")
        self.add_output_file("total_stat_file", "total_stat", filename="total_stat.stdout")
        self.add_output_file_list("files_to_save_stats", "Files containing stat of the corresponding read file", items=self.files_to_save)
        self.add_output_file_list("files_to_sync", "Files in the work directory of the run, to be synchronized", items=self.files_to_save)
        self.add_output_file_list("files_to_md5", "Files in the work directory for md5sum", items=self.files_to_save)
    
    def process(self):
        self.runobj.raw_files = self.files_to_save
        logging.getLogger("AddRawFiles").debug("process. work_directory of the run "+ self.runobj.get_work_directory())
        run_dump_path = self.get_temporary_file(".dump")
        run_dump = open(run_dump_path, "wb")
        pickle.dump(self.runobj, run_dump)
        run_dump.close()
        logging.getLogger("AddRawFiles").debug("process. begin does the work dir ("+self.runobj.get_work_directory()+") exist? "+ str(os.path.isdir(self.runobj.get_work_directory())))
        
        files_to_save_stats = self.get_outputs( '{basename_woext}.count', self.files_to_save)
        logging.getLogger("AddRawFiles").debug("process. Before self.add_python_execution(extract_stats_from_seq_file")
        #count number of reads and total length in base for each seq file
        for i,o in zip(self.files_to_save, files_to_save_stats ):
                self.add_python_execution(extract_stats_from_seq_file,cmd_format="{EXE} {IN} {OUT}",
                                      inputs = i, outputs = o, map=False)

        logging.getLogger("AddRawFiles").debug("process. Before self.add_stats_to_run(extract_stats_from_seq_file")
        logging.getLogger("AddRawFiles").debug("process. Dirname of files_to_save_stats[0] : "+ os.path.dirname(files_to_save_stats[0]))
        #Add number of reads and total length in base for each seq file and add these data to the run in the database
        self.add_python_execution(add_stats_to_run, cmd_format='{EXE} {ARG} > {OUT}', map=False,
                                 outputs = self.stdout, includes = files_to_save_stats, arguments=[run_dump_path, files_to_save_stats[0]])
        
        #archive the files in the work folder of the run to be rsynced at the end
        logging.getLogger("AddRawFiles").debug("process. Before copying/archiving files with compression = " + self.compression )
        files_to_sync_ori = []
        files_to_md5_ori = []
        if self.compression=="none":
            for file in self.files_to_save:
                files_to_md5_ori.append(os.path.join(self.runobj.get_work_directory(),os.path.basename(file)))
                if os.path.dirname(file) != self.runobj.get_work_directory():
                    files_to_sync_ori.append(os.path.join(self.runobj.get_work_directory(),os.path.basename(file)))
                    
            files_to_md5 = files_to_md5_ori
            files_to_sync = files_to_sync_ori
            for idx, file in enumerate(files_to_sync):
                self.add_python_execution(copy_file,cmd_format="{EXE} {IN} {OUT}",
                                      inputs = self.files_to_save[idx], outputs = file, map=False)
        #TODO: possible inconsistancy : if the filename ends with one extension in Utils.UNCOMPRESS_EXTENSION, output file name won't be suffixed with gz in Utils.gzip
        elif self.compression=="gz":
            for file in self.files_to_save:
                files_to_sync_ori.append(os.path.join(self.runobj.get_work_directory(),os.path.basename(file)+".gz"))
                files_to_md5_ori.append(os.path.join(self.runobj.get_work_directory(),os.path.basename(file)+".gz"))
            files_to_md5 = files_to_md5_ori
            files_to_sync = files_to_sync_ori
            for idx, file in enumerate(self.files_to_save):
                self.add_python_execution(zip_file,cmd_format="{EXE} {IN} {OUT}",
                                      inputs = file, outputs = files_to_sync[idx], map=False)
        #TODO: possible inconsistancy : if the filename ends with one extension in Utils.UNCOMPRESS_EXTENSION, output file name won't be suffixed with bz2 in Utils.bz2
        elif self.compression=="bz2":
            for file in self.files_to_save:
                files_to_md5_ori.append(os.path.join(self.runobj.get_work_directory(),os.path.basename(file)))
                files_to_sync_ori.append(os.path.join(self.runobj.get_work_directory(),os.path.basename(file)))
            files_to_md5 = files_to_md5_ori
            files_to_sync = files_to_sync_ori
            for idx, file in enumerate(self.files_to_save):
                self.add_python_execution(bzip2_file,cmd_format="{EXE} {IN} {OUT}",
                                      inputs = file, outputs = files_to_sync[idx], map=False)
        elif mode == "tar.gz":
            files_to_md5_ori.append(os.path.join(self.runobj.get_work_directory(),"ng6_archive.tar.gz"))
            files_to_sync_ori.append(os.path.join(self.runobj.get_work_directory(),"ng6_archive.tar.gz"))
            files_to_md5 = files_to_md5_ori
            files_to_sync = files_to_sync_ori
            
            self.add_python_execution(tar_zip_files,cmd_format="{EXE} {OUT} {IN}",
                                      inputs = self.files_to_save, outputs = files_to_sync[idx], map=False)

        elif mode == "tar.bz2":
            files_to_md5_ori.append(os.path.join(self.runobj.get_work_directory(),"ng6_archive.tar.bz2"))
            files_to_sync_ori.append(os.path.join(self.runobj.get_work_directory(),"ng6_archive.tar.bz2"))
            files_to_md5 = files_to_md5_ori
            files_to_sync = files_to_sync_ori
            self.add_python_execution(tar_bz2_files,cmd_format="{EXE} {OUT} {IN}",
                                      inputs = self.files_to_save, outputs = files_to_sync[idx], map=False)
        logging.getLogger("AddRawFiles").debug("process. after self.compression.")    
        
        if (len(files_to_md5_ori)>0):
            md5_file = os.path.join(self.runobj.get_work_directory(), "md5sum.txt")
            self.add_python_execution(md5sum, cmd_format="{EXE} {OUT} {IN}",
                                      inputs = files_to_md5[0], outputs = md5_file, map=False)       
        
               