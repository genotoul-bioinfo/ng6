#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import re
import logging
from jflow.utils import get_argument_pattern
from jflow.component import Component

from ng6.utils import Utils


class ConcatenateFilesGroups (Component):
    """
      @summary : Concatenates files according to filename prefixes.
    """
    
    def define_parameters(self, runobj, files_list, group_prefix):
        """
          @param files_list : path of files that will be concatenated
          @param group_prefix : list of filename prefixes
        """
        logging.getLogger("ConcatenateFilesGroups").debug("define_parameters. group_prefix = " + ",".join(group_prefix))
        self.runobj = runobj
        self.add_input_file_list( "files_list", "files_list", default=files_list, required=True)
        self.add_parameter_list("group_prefix", "group_prefix", default=group_prefix)
        extensions = os.path.basename(self.files_list[0]).split(".")[1:]
        output_list = []
        for prefix in self.group_prefix:
            output_list.append( os.path.join(self.runobj.get_work_directory(),prefix+"."+".".join(extensions)))
        logging.getLogger("ConcatenateFilesGroups").debug("define_parameters. extensions = " + ",".join(extensions))
        self.add_output_file_list( "concat_files", "concat_files", pattern='{fullpath}', items= output_list)
        self.add_output_file_pattern( "r1_files", "r1_files", pattern='*_R1.fastq.gz')
        self.add_output_file_pattern( "r2_files", "r2_files", pattern='*_R2.fastq.gz')
        #self.add_output_file_list( "concat_files", "concat_files", items= self.group_prefix)
        
    def process(self):
        logging.getLogger("ConcatenateFilesGroups").debug("process. work dir ("+self.runobj.get_work_directory()+") exist? "+ str(os.path.isdir(self.runobj.get_work_directory())))
        logging.getLogger("ConcatenateFilesGroups").debug("process. self.files_list = " + ", ".join(self.files_list))
        logging.getLogger("ConcatenateFilesGroups").debug("process. self.concat_files = " + ", ".join(self.concat_files))
        # Create dictionary : key = prefix and value = list of files to concatenate
        concat_groups = Utils.get_filepath_by_prefix(self.files_list, self.group_prefix)
        files_to_concatenate = []
        for file_to_concatenate in self.group_prefix:
              files_to_concatenate.append(os.path.join(self.runobj.get_work_directory(),os.path.basename(file_to_concatenate)+"."+".".join(os.path.basename(self.files_list[0]).split(".")[1:])))
        logging.getLogger("ConcatenateFilesGroups").debug("process. files_to_concatenate = " + ", ".join(files_to_concatenate))
        self.concat_files  = self.get_outputs('{fullpath}', files_to_concatenate)
        R1files = []
        R2files = []
        for file in self.concat_files:
            if re.search("_R1.",file):
                R1files.append(file)
            if re.search("_R2.",file):
                R2files.append(file)
        logging.getLogger("ConcatenateFilesGroups").debug("process R1files = " + ",".join(R1files))
        self.r1_files = sorted(R1files)
        self.r2_files = sorted(R2files)
        # self.concat_files = files_to_concatenate  
        logging.getLogger("ConcatenateFilesGroups").debug("process. self.concat_files bis = " + ", ".join(self.concat_files))
        # Create dictionary : key = prefix and value = the output file
        outputs_path = Utils.get_filepath_by_prefix(files_to_concatenate, self.group_prefix)
                
        out_files = []
        for prefix in self.group_prefix:
            logging.getLogger("ConcatenateFilesGroups").debug("process. prefix = " + prefix)
            logging.getLogger("ConcatenateFilesGroups").debug("process. outputs_path[prefix] = " + ", ".join(outputs_path[prefix]))
            logging.getLogger("ConcatenateFilesGroups").debug("process. concat_groups[prefix] = " + ", ".join(concat_groups[prefix]))
            # Sort list of files to concatenate
            concat_groups[prefix].sort()

            [cmd_inputs_pattern, next_arg_number] = get_argument_pattern(concat_groups[prefix], 1)

            # If the file is not zip
            if not self.files_list[0].endswith(".gz"):
                self.add_shell_execution('cat ' + cmd_inputs_pattern + ' > ${' + str(next_arg_number) + '}', 
                                         cmd_format='{EXE} {IN} {OUT}', map=False,
                                         inputs = concat_groups[prefix], outputs = outputs_path[prefix])
            # If the file is zip
            else:
                logging.getLogger("ConcatenateFilesGroups").debug("process. command = "  + 'zcat ' + cmd_inputs_pattern + ' | '+self.get_exec_path("gzip")+' - > ${' + str(next_arg_number) + '}' + ", ".join(concat_groups[prefix]) + ", and ouput = "+", ".join(outputs_path[prefix]))
                output = os.path.join(self.runobj.get_work_directory(),prefix+"."+".".join(os.path.basename(self.files_list[0]).split(".")[1:]))
                out_files.append(output)
                if len(concat_groups[prefix])==1:
                    self.add_shell_execution('cp ' + cmd_inputs_pattern + ' ${' + str(next_arg_number) + '}',
                                         cmd_format='{EXE} {IN} {OUT}', map=False,
                                         inputs = concat_groups[prefix], outputs = outputs_path[prefix])
                else:
                    self.add_shell_execution('zcat ' + cmd_inputs_pattern + ' | '+self.get_exec_path("gzip")+' - > ${' + str(next_arg_number) + '}', 
                                         cmd_format='{EXE} {IN} {OUT}', map=False,
                                         inputs = concat_groups[prefix], outputs = outputs_path[prefix])
#         self.concat_files  = out_files
