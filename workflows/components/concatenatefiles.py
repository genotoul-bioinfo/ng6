#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import logging
from jflow.component import Component
from jflow.utils import get_argument_pattern


class ConcatenateFiles (Component):
    
    def define_parameters(self, files_list, output_name_woext):
        """
            Concatenate a list of file. Output extension is the extension of the first file
        """
        self.add_input_file_list( "files_list", "files_list", default=files_list, required=True )
        if files_list : 
            self.add_parameter("output_name_woext", "Output name without extension", default = output_name_woext, required = True)
            self.extension = '.'.join(os.path.basename(self.files_list[0]).split(".")[1:])
            self.add_output_file("output_file", "output_file", filename = self.output_name_woext + '.' + self.extension )
        else :
            self.add_output_file("output_file", "output_file", filename=output_name_woext )
            
    def process(self):
        arg_pattern, next_number =  get_argument_pattern(self.files_list, start_number=2)
        if self.extension.endswith(".gz") :
            self.add_shell_execution('zcat ' + arg_pattern + ' | '+self.get_exec_path("gzip")+' - > $1', cmd_format='{EXE} {OUT} {IN}', 
                                     map=False, outputs = self.output_file, inputs=self.files_list)
        else :
            self.add_shell_execution('cat ' + arg_pattern + ' > $1', cmd_format='{EXE} {OUT} {IN}', 
                                     map=False, outputs = self.output_file, inputs=self.files_list)
      