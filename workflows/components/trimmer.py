#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from weaver.abstraction import Map
from weaver.function import PythonFunction

from jflow.component import Component

def trim(input_file, output_file, start, end):
    """
     Trim all sequences by start and end position.
      @param input_file : [string] path
      @param output_file : [string] path
      @param start : [int] index of the first nucleotide kept after trim (1 to N with included boundary)
      @param end : [int] index of the last nucleotide kept after trim (1 to N with included boundary)
    """
    import jflow.seqio as seqio 
    reader = seqio.SequenceReader(input_file)
    
    out_fh = ""
    if output_file.endswith(".gz"):
        out_fh = seqio.xopen(output_file, "w")
    else:
        out_fh = open(output_file, "w")
                 
    for id, desc, seq, qual in reader :
        local_end = len(seq)
        if end is not None and local_end > int(end) :
            local_end = int(end)
        if reader.__class__.__name__ == "FastqReader" :
            # trim seq
            seq = seq[int(start)-1:local_end]
            qual = qual[int(start)-1:local_end]
            # write seq
            seqio.writefastq(out_fh, [[id, desc, seq, qual]])
        else:
            # trim seq
            seq = seq[int(start)-1:local_end]
            # write seq
            seqio.writefasta(out_fh, [[id, desc, seq, qual]])
    

class Trimmer (Component):
    """
     @summary : Trim all sequences by start and end position.
    """

    def define_parameters(self, input_files, start=1, end=None):
        """
          @param input_files : [list] list of path
          @param start : [int] index of the first nucleotide kept after trim (1 to N with included boundary)
          @param end : [int] index of the last nucleotide kept after trim (1 to N with included boundary)
        """
        self.add_input_file_list( "input_files", "list of path", default=input_files, required=True )
        self.add_output_file_list("output_files", "Output files.", pattern='{basename}', items=input_files)
        self.add_output_file("stderr", "The trimmer stderr file", filename="trimmer.stderr")
        self.add_parameter("start", "index of the first nucleotide kept after trim (1 to N with included boundary)", default=start, type='int')
        self.add_parameter("end", "index of the last nucleotide kept after trim (1 to N with included boundary)", default=end, type='int')
    
    def process(self):
        trimmer = PythonFunction( trim, cmd_format="{EXE} {IN} {OUT} " + str(self.start) + " " + str(self.end) + " 2>> " + self.stderr )
        trimmer = Map( trimmer, inputs=self.input_files, outputs=self.output_files )