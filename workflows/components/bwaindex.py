#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from subprocess import Popen, PIPE

from jflow.component import Component

def bwa_index(exec_path, algorithm, input_fasta, databank, stdout_path, stderr_path):
    from subprocess import Popen, PIPE
    # first make the symbolic link
    os.symlink(input_fasta, databank)
    # then execute bwa index
    cmd = [exec_path, "index", "-a", algorithm, databank]
    p = Popen(cmd, stdout=PIPE, stderr=PIPE)
    stdout, stderr = p.communicate()
    # write down the stdout
    stdoh = open(stdout_path, "wb")
    stdoh.write(stdout)
    stdoh.close()
    # write down the stderr
    stdeh = open(stderr_path, "wb")
    stdeh.write(stderr)
    stdeh.close()


class BWAIndex (Component):

    def define_parameters(self, input_fasta, algorithm="bwtsw"):
        self.add_input_file("input_fasta", "Which fasta file should be indexed", file_format="fasta", default=input_fasta, required=True)
        self.add_parameter("algorithm", "Which algorithm should be used to index the fasta file", default=algorithm, choices=["bwtsw", "div", "is"])
        self.add_output_file("databank", "The indexed databank to use with BWA", filename=os.path.basename(input_fasta), file_format="bam")
        self.add_output_file("stdout", "The BWAIndex stdout file", filename="bwaindex.stdout")
        self.add_output_file("stderr", "The BWAIndex stderr file", filename="bwaindex.stderr")
    
    def get_version(self):
        cmd = [self.get_exec_path("bwa")]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stderr.split()[7]

    def process(self):
        self.add_python_execution(bwa_index, 
                                 cmd_format="{EXE} {ARG} {IN} {OUT}",
                                 inputs=self.input_fasta, outputs=[self.databank, self.stdout, self.stderr], arguments=[self.get_exec_path("bwa"), self.algorithm], 
                                 map=False)
        