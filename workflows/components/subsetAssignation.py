#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os, sys
import logging
from subprocess import Popen, PIPE

from jflow.utils import get_argument_pattern

from ng6.analysis import Analysis
from ng6.utils import Utils

def nb_seq_from_file(sequence_file):
    import jflow.seqio as seqio
    reader = seqio.SequenceReader(sequence_file)
    nb_seq = 0
    for id, desc, seq, qual in reader :
        nb_seq += 1
    return nb_seq

def extract_random_seq(extract_rate, min_nb_seq, max_nb_seq, *files):
    import jflow.seqio as seqio   
    # Check parameters 
    extract_rate = float(extract_rate)
    min_nb_seq = int(min_nb_seq)
    max_nb_seq = int(max_nb_seq)
    if extract_rate > 1 or extract_rate <= 0:
        raise Exception("[ERROR] : the extract_rate value is not correct! (Should be between 0 and 1)")
    if int(min_nb_seq) > int(max_nb_seq):
        raise Exception("[ERROR] : the threshold values are not correct ! (Minimum threshold is bigger than Maximum threshold)")
    nb_files_pair = len(files)/2
    if int(nb_files_pair) != nb_files_pair:
        raise Exception("[ERROR] : the number of files is not correct! (Each sequence_files should correspond to an sub_sequence_files : [file1, file2, sub_file1, sub_file2])")
    nb_files_pair = int(nb_files_pair)
    sequence_files = files[:nb_files_pair]
    outputs_fasta = files[nb_files_pair:]
    
    # Compute min_nb_seq, max_nb_seq, nbsubseq, totalseq
    nb_seq = 0
    for curr_sequence_file in sequence_files:
        reader = seqio.SequenceReader(curr_sequence_file)
        for id, desc, seq, qual in reader :
            nb_seq += 1       
    nb_seq_to_extract = int(nb_seq * float(extract_rate))
    
    # Setting the thresholds if necessary
    if nb_seq_to_extract > max_nb_seq:
        nb_seq_to_extract = max_nb_seq
    elif nb_seq_to_extract < min_nb_seq:
        nb_seq_to_extract = min_nb_seq
    if nb_seq < nb_seq_to_extract :
        nb_seq_to_extract = nb_seq 
    step = int(nb_seq / nb_seq_to_extract)

    # Create sub_sequence_files
    count = 0
    for i in range(0, nb_files_pair):
        # Writes result in fasta file
        reader = seqio.SequenceReader(sequence_files[i])
        with open(outputs_fasta[i], "w") as output:
            for id, desc, seq, qual in reader :
                count += 1
                if count == step:
                    count = 0
                    seqio.writefasta(output, [[id, desc, seq, qual]])

class SubsetAssignation (Analysis):
    
    def define_parameters(self, sequence_files, databank, group_prefix=None, max_target_seqs=10, 
                          extract_rate=0.05, min_nb_seq=20000, max_nb_seq=1e6):
        self.add_input_file_list( "sequence_files", "Input sequence files", default=sequence_files, required=True, file_format = 'fastq')
        self.add_parameter("databank", "blastn databank", default=databank, required = True)
        self.add_parameter("max_target_seqs", "max_target_seqs", type='int', default=max_target_seqs)
        self.add_parameter("extract_rate", "extract_rate", type='float', default=extract_rate)
        self.add_parameter("min_nb_seq", "min_nb_seq", type='int', default=min_nb_seq)
        self.add_parameter("max_nb_seq", "max_nb_seq", type='int', default=max_nb_seq)

        self.add_output_file_list( "sub_fasta_files", "sub fasta files", pattern='{basename_woext}_sub.fasta', items=self.sequence_files, file_format = 'fasta')
        self.add_output_file_list( "blast_files", "sub fasta files", pattern='{basename_woext}.blast', items=self.sequence_files)
        self.nb_threads=1
        if self.get_cpu() != None :
            self.nb_threads=self.get_cpu()
        self.use_index = "true"
        if not os.path.exists( self.databank + ".00.idx" ) :
            self.use_index = "false"
            logging.getLogger("SubsetAssignation").warn("would work more quickly with megablast index")
        
        if group_prefix == None :
            self.add_parameter_list("group_prefixes", "group_prefix", default=list(map(os.path.basename, self.get_outputs('{basename_woext}', self.sequence_files))))
            self.add_output_file_list( "html_files", "sub html_files files", pattern='{basename_woext}.html', items=self.sequence_files)
            self.add_output_file_list( "krona_stdout", "sub krona_stdout files", pattern='{basename_woext}.stdout', items=self.sequence_files)
        else :
            self.add_parameter_list("group_prefixes", "group_prefix", default=group_prefix)
            self.add_output_file_list( "html_files", "sub html_files files", pattern='{basename_woext}.html', items=self.group_prefixes)
            self.add_output_file_list( "krona_stdout", "sub krona_stdout files", pattern='{basename_woext}.stdout', items=self.group_prefixes)
            

    def define_analysis(self):
        self.name = "SubsetAssignation"
        self.description = "Assignation on a subset of sequences."
        self.software = "blastn"
        self.options = str(self.extract_rate)+" "+str(self.min_nb_seq)+" "+str(self.max_nb_seq)+" "+self.databank
        
    def post_process(self):        
        # Add the number of sequences in the subset
        nb_seq_by_groups = {}
        for sub_fasta_file in self.sub_fasta_files:
            for prefix in self.group_prefixes:
                if( os.path.basename(sub_fasta_file).startswith(prefix) ):
                    if prefix not in nb_seq_by_groups :
                        nb_seq_by_groups[prefix] = 0
                    nb_seq_by_groups[prefix] = nb_seq_by_groups[prefix] + nb_seq_from_file(sub_fasta_file)
        
        for prefix in self.group_prefixes:
            self._add_result_element(prefix, "nb_seq_extracted", nb_seq_by_groups[prefix])
            
        # Add the krona file
        for html_file in self.html_files:
            sample = os.path.splitext(os.path.basename(html_file))[0]
            self._add_result_element(sample, "html", self._save_file(html_file))
            if os.path.isdir(html_file+".files"):
                self._save_directory(html_file+".files")

    def get_version(self):
        cmd = [self.get_exec_path("blastn"),"-version"]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stdout.split()[1]
                     
    def process(self):
        # Create subsets files 
        input_groups = Utils.get_filepath_by_prefix( self.sequence_files, self.group_prefixes )
        sub_fasta_groups = Utils.get_filepath_by_prefix( self.sub_fasta_files, self.group_prefixes )
        for prefix in self.group_prefixes:
            self.add_python_execution(extract_random_seq,cmd_format="{EXE} " + 
                                     " ".join([str(self.extract_rate),str(self.min_nb_seq), str(self.max_nb_seq)]) + " {IN} {OUT}",
                                     inputs = input_groups[prefix], outputs = sub_fasta_groups[prefix], map=False)
                                                                                        
        # Align on databank
        self.add_shell_execution(self.get_exec_path("blastn") + " -max_target_seqs " + str(self.max_target_seqs) + " -num_threads " +  str(self.nb_threads) +
                                " -use_index " + str(self.use_index) + " -outfmt 7 -db " + str(self.databank) + " -query $1 -out $2", 
                               cmd_format='{EXE} {IN} {OUT}', map=True,
                               inputs=self.sub_fasta_files, outputs=self.blast_files)
        # Create files groups
        blast_groups = Utils.get_filepath_by_prefix( self.blast_files, self.group_prefixes )
        blast_merges_groups = Utils.get_filepath_by_prefix( self.get_outputs('{basename}_merge.blast', self.group_prefixes), self.group_prefixes )
        krona_groups = Utils.get_filepath_by_prefix( self.krona_stdout, self.group_prefixes )
        html_groups = Utils.get_filepath_by_prefix( self.html_files, self.group_prefixes )
        # For each group
        for prefix in self.group_prefixes:
            # Merge blast
            [cmd_inputs_pattern, next_arg_number] = get_argument_pattern(blast_groups[prefix], 1)
            self.add_shell_execution('cat ' + cmd_inputs_pattern + ' > ${' + str(next_arg_number) + '}', 
                                    cmd_format='{EXE} {IN} {OUT}', map=False,
                                    inputs = blast_groups[prefix], outputs = blast_merges_groups[prefix])
            # Create krona
            self.add_shell_execution(self.get_exec_path("ktImportBLAST") + " -i -b $1 -o $2 > $3", 
                                    cmd_format='{EXE} {IN} {OUT}', map=False,
                                    inputs = blast_merges_groups[prefix], outputs = [html_groups[prefix], krona_groups[prefix]]) 
            
