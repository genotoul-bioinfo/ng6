#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from subprocess import Popen, PIPE

from jflow.utils import get_argument_pattern

from ng6.analysis import Analysis
from ng6.utils import Utils

class ContaminationSearch (Analysis):
    
    def define_parameters(self, sequence_files, databanks, group_prefix=None, archive_name=None):
        self.add_input_file_list( "sequence_files", "sequence_files", default=sequence_files, required=True)
        self.add_input_file_list( "databanks", "databanks", default=databanks, required=True)
        self.add_parameter_list("group_prefix", "group_prefix", default=group_prefix)
        self.add_parameter("archive_name", "Archive name", default=archive_name)
        self.name_files = {}
        self.conta_stderr = {}
        all_name_files = []
        for current_databank in self.databanks:
            if not os.path.exists( current_databank + ".bwt" ):
                raise ValueError(current_databank + " must be a databank indexed for bwa.")
            databank_basename = os.path.basename(current_databank).split(".")[0]
            self.conta_stderr[databank_basename] = self.get_outputs('{basename_woext}_names.' + databank_basename + '.name_stderr', self.sequence_files)
            if group_prefix != None:
                self.name_files[databank_basename] = self.get_outputs('{basename}.conta_' + databank_basename + '.txt', group_prefix)
            else:
                self.name_files[databank_basename] = self.get_outputs('{basename_woext}.conta_' + databank_basename + '.txt', self.sequence_files)
            all_name_files = all_name_files + self.name_files[databank_basename]
        
        self.add_output_file_list( "out", "out", pattern='{basename}', items=all_name_files)
        
    def define_analysis(self):
        self.name = "ContaminationSearch"
        self.description = "Contamination search."
        self.software = "bwa"
        self.options = ""
        
    def post_process(self):        
        for current_databank in self.databanks:
            databank_basename = os.path.basename(current_databank).split(".")[0]
            # Count contamination
            for name_file in self.name_files[databank_basename]:
                sample = os.path.basename(name_file).split(".conta_")[0]
                nb_contamination = sum(1 for _ in open(name_file))
                self._add_result_element(sample, "nb_conta", str(nb_contamination), databank_basename)

        # Finaly create and add the archive to the analyse
        self._create_and_archive(self.out, self.archive_name)

    def get_version(self):
        cmd = [self.get_exec_path("bwa")]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stderr.split()[7]
                     
    def process(self):
        for current_databank in self.databanks:
            databank_basename = os.path.basename(current_databank).split(".")[0]
            unmerged_name_files = self.get_outputs('{basename_woext}_names.' + databank_basename + '.txt', self.sequence_files)
            self.add_shell_execution(self.get_exec_path("bwa") + ' mem ' + current_databank + ' $1 2>> $3 | ' +
                                     self.get_exec_path("samtools") + ' view -SF 260 - 2>> $3 | cut -f1 - 2>> $3 | sort - > $2 2>> $3', 
                                     cmd_format='{EXE} {IN} {OUT}', map=True,
                                     inputs=self.sequence_files, outputs=[unmerged_name_files, self.conta_stderr[databank_basename]])
            
            if self.group_prefix :
                # Create dictionary : key = prefix and value = list of files to merge
                groups_path = Utils.get_filepath_by_prefix( unmerged_name_files, self.group_prefix)
                     
                # Create dictionary : key = prefix and value = the output
                outputs_path = Utils.get_filepath_by_prefix( self.name_files[databank_basename], self.group_prefix)

                # Merges tmp name files in output
                for prefix in self.group_prefix:
                    [cmd_inputs_pattern, next_arg_number] = get_argument_pattern(groups_path[prefix], 1)
                    self.add_shell_execution('cat ' + cmd_inputs_pattern + ' > ${' + str(next_arg_number) + '}', 
                                             cmd_format='{EXE} {IN} {OUT}', map=False, 
                                             inputs=groups_path[prefix], outputs=outputs_path[prefix])
            else:
                # Rename tmp to output
                self.add_shell_execution('mv $1 $2', cmd_format='{EXE} {IN} {OUT}', map=False,
                                         inputs=unmerged_name_files, outputs=self.name_files[databank_basename])