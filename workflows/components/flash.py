#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from subprocess import Popen, PIPE

from ng6.analysis import Analysis
from ng6.utils import Utils
import jflow.seqio as seqio

class Flash (Analysis):
    def _get_length_table(self, input_file):
        """
          @param input_file : [str] the fastq file path
          @return : [list] the number of sequences and the number of fragment by size. 
                     Format : [nb_seq, {size_x: nb_fragment, size_j:...}]
        """       
        nb_seq = 0
        reader = seqio.SequenceReader(input_file)
        sizes = {}
        for id, desc, seq, qualities in reader:
            nb_seq += 1
            if len(seq) in sizes:
                sizes[len(seq)] += 1
            else:
                sizes[len(seq)] = 1
        return [nb_seq, sizes]
    
    def define_parameters(self, read1_files, read2_files, mismatch_ratio=None, min_overlap=None, max_overlap=None, avg_reads_length=None, avg_fragment_length=None, 
                          standard_deviation=None, phred_offset=None, nb_thread=1, archive_name=None):
        """
          @param read1_files : [list] paths to reads 1
          @param read2_files : [list] paths to reads 2
          @param mismatch_ratio : [float] maximum allowed ratio between the number of mismatched base pairs and the overlap length. Two reads will not be combined with a given overlap if that overlap results in a mismatched base density higher than this value
          @param min_overlap : [int] the minimum required overlap length between two reads to provide a confident overlap
          @param max_overlap : [int] maximum overlap length expected in approximately 90% of read pairs
          @param avg_reads_length : [int] average read length
          @param avg_fragment_length : [int] average fragment length
          @param standard_deviation : [int] average fragment standard deviation
          @param phred_offset : [int] the smallest ASCII value of the characters used to represent quality values of bases in FASTQ files
          @param nb_thread : [int] set the number of worker threads
          @param archive_name : [str] name for the output archive
        """
        
        self.add_input_file_list( "read1_files", "read1_files", default=read1_files, required=True)
        self.add_input_file_list( "read2_files", "read2_files", default=read2_files, required=True)
        self.add_parameter("nb_thread", "nb_thread", default=nb_thread, type=int)
        self.add_parameter("mismatch_ratio", "mismatch_ratio", default=mismatch_ratio, type=float)
        self.add_parameter("min_overlap", "min_overlap", default=min_overlap, type=int)
        self.add_parameter("max_overlap", "max_overlap", default=max_overlap, type=int)
        self.add_parameter("avg_reads_length", "avg_reads_length", default=avg_reads_length, type=int)
        self.add_parameter("avg_fragment_length", "avg_fragment_length", default=avg_fragment_length, type=int)
        self.add_parameter("standard_deviation", "standard_deviation", default=standard_deviation, type=int)
        self.add_parameter("phred_offset", "phred_offset", default=phred_offset, type=int)
        self.add_parameter("archive_name", "archive_name", default=archive_name)
        if self.get_cpu() != None :
            self.nb_thread=self.get_cpu()
        if len(self.read1_files) != len(self.read2_files):
            raise Exception("[ERROR] : the number of files is not correct! (the number of files in read1_files and in read2_files must be the same)")

        self.prefixes = self.get_outputs('{basename_woext}', [self.read1_files, self.read2_files])
        self.add_output_file_list( "extended_frags", "extended_frags", pattern='{basename_woext}.extendedFrags.fastq.gz', items=self.prefixes, file_format = 'fastq')
        self.add_output_file_list( "not_combined_read_1", "not_combined_read_1", pattern='{basename_woext}.notCombined_1.fastq.gz', items=self.prefixes, file_format = 'fastq')
        self.add_output_file_list( "not_combined_read_2", "not_combined_read_2", pattern='{basename_woext}.notCombined_2.fastq.gz', items=self.prefixes, file_format = 'fastq')
        self.add_output_file_list( "stderrs", "stderrs", pattern='{basename_woext}.stderr', items=self.prefixes)
        
    def define_analysis(self):
        self.name = "JoinPairs"
        self.description = "Combines the overlapping pairs."
        self.software = "flash"
        self.options = ""
        if self.nb_thread:
            nb_worker_threads = 1
            if self.nb_thread > 2:
                nb_worker_threads = int(round(self.nb_thread / 2))
                                                    
            self.options += " -t " + str(nb_worker_threads)
        if self.mismatch_ratio:
            self.options += " -x " + str(self.mismatch_ratio)
        if self.min_overlap:
            self.options += " -m " + str(self.min_overlap)
        if self.max_overlap:
            self.options += " -M " + str(self.max_overlap)
        if self.avg_reads_length:
            self.options += " -r " + str(self.avg_reads_length)
        if self.avg_fragment_length:
            self.options += " -f " + str(self.avg_fragment_length)
        if self.standard_deviation:
            self.options += " -s " + str(self.standard_deviation) 
        if self.phred_offset:
            self.options += " -p " + str(self.phred_offset)

    def post_process(self):
        samples = {}
        # Save files
        for filepath in self.extended_frags:
            self._save_file(filepath)
        # Process metrics from the extended fragments
        for filepath in self.extended_frags:
            [nb_seq, sizes] = self._get_length_table(filepath)
            x = []
            y = []
            for val in list(sizes.keys()):
                x.append(val)
            x = sorted(x)
            for i in x:
                y.append(sizes[i])
            sample_name = os.path.basename(filepath).split(".extendedFrags")[0]
            if sample_name not in samples:
                samples[sample_name] = {}
            samples[sample_name]["nb_extended"] = str(nb_seq)
            samples[sample_name]["size_extended"] = str(",".join([str(v) for v in x]))
            samples[sample_name]["nb_size_extended"] = str(",".join([str(v) for v in y]))
        # Process metrics from the not combined reads 1
        for filepath in self.not_combined_read_1:
            [nb_seq, sizes] = self._get_length_table(filepath)
            sample_name = os.path.basename(filepath).split(".notCombined_1")[0]
            if sample_name not in samples:
                samples[sample_name] = {}
            samples[sample_name]["nb_notcombined1"] = str(nb_seq)
        # Process metrics from the not combined reads 2
        for filepath in self.not_combined_read_2:
            [nb_seq, sizes] = self._get_length_table(filepath)
            sample_name = os.path.basename(filepath).split(".notCombined_2")[0]
            if sample_name not in samples:
                samples[sample_name] = {}
            samples[sample_name]["nb_notcombined2"] = str(nb_seq)
        # Save metrics
        for sample in samples:        
            self._add_result_element(sample, "nb_extended", samples[sample]["nb_extended"])
            self._add_result_element(sample, "size_extended", samples[sample]["size_extended"])
            self._add_result_element(sample, "nb_size_extended", samples[sample]["nb_size_extended"])
            self._add_result_element(sample, "nb_notcombined1", samples[sample]["nb_notcombined1"])
            self._add_result_element(sample, "nb_notcombined2", samples[sample]["nb_notcombined2"])
    
    def get_version(self):
        cmd = [self.get_exec_path("flash"), "--version"]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        return stdout.split()[1]

    def process(self):
        for i in range(0, len(self.prefixes)):
            self.add_shell_execution(self.get_exec_path("flash") + " -z $1 $2 " + self.options + 
                                     " -o " + os.path.basename(self.prefixes[i]) + " -d " + self.output_directory + " 2> $3", 
                                     cmd_format='{EXE} {IN} {OUT}', map=False,
                                     inputs=[self.read1_files[i], self.read2_files[i]], 
                                     outputs=[self.stderrs[i], self.extended_frags[i], self.not_combined_read_1[i], self.not_combined_read_2[i]] )