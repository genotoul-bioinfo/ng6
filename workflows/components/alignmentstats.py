#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os,re,sys

from ng6.analysis import Analysis

class AlignmentStats (Analysis):
    
    def define_parameters(self, bam_files, split_reads=True, search_dupl=False, optical_duplicate_pixel_distance=10, 
                          read_name_regex=None, assume_sorted=True, validation_stringency="LENIENT", 
                          max_file_handles=100, sorting_collection_size_ratio=0.15, archive_name=None):
        """
          @param bam_files : paths for bams
          @param split_reads : if True, one cigarlineGraph will be created for read 1 and one for read 2
          @param search_dupl : if True, the analysis processes metrics about duplications
          @param optical_duplicate_pixel_distance : the maximum offset between two duplicte clusters in order to consider them optical duplicates
          @param assume_sorted : True assumes that the input file is coordinate sorted even if the header says otherwise
          @param validation_stringency : Validation stringency for all SAM files read by this program. Setting stringency to SILENT can improve performance when processing a BAM file in which variable-length data (read, qualities, tags) do not otherwise need to be decoded. Default value: STRICT. This option can be set to 'null' to clear the default value. Possible values: {STRICT, LENIENT, SILENT}
          @param max_file_handles : maximum number of file handles to keep open when spilling read ends to disk. Set this number a little lower than the per-process maximum number of file that may be open
          @param sorting_collection_size_ratio : this number, plus the maximum RAM available to the JVM, determine the memory footprint used by some of the sorting collections
          @param archive_name : name for the output archive
        """
        self.add_input_file_list( "bam_files", "bam_files", default=bam_files, required=True, file_format = 'bam')
        self.add_parameter("split_reads", "split_reads", default=split_reads, type=bool)
        self.add_parameter("search_dupl", "search_dupl", default=search_dupl, type=bool)
        self.add_parameter("optical_duplicate_pixel_distance", "optical_duplicate_pixel_distance", default=optical_duplicate_pixel_distance, type=int)
        self.add_parameter("read_name_regex", "read_name_regex", default=read_name_regex)
        self.add_parameter("assume_sorted", "assume_sorted", default=assume_sorted, type=bool)
        self.add_parameter("validation_stringency", "validation_stringency", default=validation_stringency)
        self.add_parameter("max_file_handles", "max_file_handles", default=max_file_handles, type=int)
        self.add_parameter("sorting_collection_size_ratio", "sorting_collection_size_ratio", default=sorting_collection_size_ratio, type=float)
        self.add_parameter("archive_name", "archive_name", default=archive_name)
        self.memory = '4G'
        if self.get_memory() != None :
            self.memory=self.get_memory()
        
        self.add_output_file_list( "stat_files", "stat_files", pattern='{basename_woext}.stat', items=self.bam_files)
        self.add_output_file_list( "cigar_stderrs", "cigar_stderrs", pattern='{basename_woext}.cigar_stderr', items=self.bam_files)
        self.add_output_file_list( "csv_files_r1", "csv_files_r1", pattern='{basename_woext}.csv', items=self.bam_files)
        self.csv_files_r2 = None
        
        if self.split_reads :
            self.add_output_file_list( "csv_files_r1", "csv_files_r1", pattern='{basename_woext}Read1.csv', items=self.bam_files)
            self.add_output_file_list( "csv_files_r2", "csv_files_r2", pattern='{basename_woext}Read2.csv', items=self.bam_files)
        
        self.cigar_options = ""
        if self.split_reads:
            self.cigar_options = "--readsplit"
        
        self.duplication_options = ""
        if self.search_dupl :
            self.dupl_stderrs = self.get_outputs('{basename_woext}.dupl_stderr', self.bam_files)
            self.del_stderrs = self.get_outputs('{basename_woext}.del_stderr', self.bam_files)
            self.add_output_file_list( "duplication_files", "duplication_files", pattern='{basename_woext}.dupl', items=self.bam_files)
            if assume_sorted:
                self.duplication_options += " ASSUME_SORTED=true"
            else:
                self.duplication_options += " ASSUME_SORTED=false"
            self.duplication_options += " MAX_FILE_HANDLES=" + str(self.max_file_handles)
            self.duplication_options += " SORTING_COLLECTION_SIZE_RATIO=" + str(self.sorting_collection_size_ratio)      
            self.duplication_options += " VALIDATION_STRINGENCY=" + self.validation_stringency
            if self.read_name_regex != None:
                self.duplication_options += " READ_NAME_REGEX=" + self.read_name_regex
            if self.optical_duplicate_pixel_distance != None:
                self.duplication_options += " OPTICAL_DUPLICATE_PIXEL_DISTANCE=" + str(self.optical_duplicate_pixel_distance)

    def define_analysis(self):
        self.name = "AlignmentStats"
        self.description = "Alignment Statistics"
        self.software = "-"
        self.options = ""
        if self.split_reads:
            self.options += "cigarline " + self.cigar_options
        if self.search_dupl:
            self.options += ";duplication " + self.duplication_options
        
    def post_process(self):
        
        for stat_file in self.stat_files:
            sample = os.path.splitext(os.path.basename(stat_file))[0]
            summary_info_flagstat = self.__parse_flagstat_file(stat_file)

            self._add_result_element(sample, "total", str(summary_info_flagstat["total"]))
            self._add_result_element(sample, "qcfailure", str(summary_info_flagstat["qcfailure"]))
            self._add_result_element(sample, "mapped", str(summary_info_flagstat["mapped"][0]) + ' (' + str(summary_info_flagstat["mapped"][1]) + ')')
            self._add_result_element(sample, "paired", str(summary_info_flagstat["paired"]))
            self._add_result_element(sample, "read1", str(summary_info_flagstat["read1"]))
            self._add_result_element(sample, "read2", str(summary_info_flagstat["read2"]))
            self._add_result_element(sample, "properlypaired", str(summary_info_flagstat["properlypaired"][0]) + ' (' + str(summary_info_flagstat["properlypaired"][1]) + ')')
            self._add_result_element(sample, "matemapped", str(summary_info_flagstat["matemapped"]))
            self._add_result_element(sample, "singletons", str(summary_info_flagstat["singletons"][0]) + ' (' + str(summary_info_flagstat["singletons"][1]) + ')')
            self._add_result_element(sample, "mapch1", str(summary_info_flagstat["mapch1"]))
            self._add_result_element(sample, "supplementary" , str(summary_info_flagstat["supplementary"]))
        
        all_csv_files = self.csv_files_r1
        if  self.csv_files_r2:
            all_csv_files = self.csv_files_r1+self.csv_files_r2
            
        for csv_file in all_csv_files:        
            sample = os.path.splitext(os.path.basename(csv_file))[0]
            summary_info_csv = self.__parse_csv_file(csv_file)
            
            read="all"
            if sample.endswith("Read2"):
                read="read2"
                sample=sample.replace("Read2","")
            elif sample.endswith("Read1"):
                read="read1"
                sample=sample.replace("Read1","")
            
            self._add_result_element(sample, "match", summary_info_csv["match"],read)
            self._add_result_element(sample, "mismatch", summary_info_csv["mismatch"],read)
            self._add_result_element(sample, "clipping", summary_info_csv["clipping"],read)
            self._add_result_element(sample, "insertion", summary_info_csv["insertion"],read)
            self._add_result_element(sample, "readsLength", summary_info_csv["readsLength"],read)
        
        if self.search_dupl:
            for dupl_file in self.duplication_files:
                sample = os.path.splitext(os.path.basename(dupl_file))[0]
                dupl_info = self.__parse_dupl_file(dupl_file)           
                self._add_result_element(sample, "pairDuplicates", dupl_info["Unknown Library"]["READ_PAIR_DUPLICATES"])
                self._add_result_element(sample, "unpairDuplicates", dupl_info["Unknown Library"]["UNPAIRED_READ_DUPLICATES"])
                self._add_result_element(sample, "pairOpticalDuplicates", dupl_info["Unknown Library"]["READ_PAIR_OPTICAL_DUPLICATES"])
                self._add_result_element(sample, "percentDuplication", dupl_info["Unknown Library"]["PERCENT_DUPLICATION"])
            
            
        # Finaly create and add the archive to the analyse
        results_files = []
        results_files.extend(self.stat_files)
        results_files.extend(self.csv_files_r1)
        if self.csv_files_r2:
            results_files.extend(self.csv_files_r2)
        if self.search_dupl:
            results_files.extend(self.duplication_files)
        self._create_and_archive(results_files, self.archive_name)
    
    def get_version(self):
        return "-"
                 
    def process(self):
        # Duplication stats
        xmx="-Xmx"+self.memory.lower()
        if self.search_dupl:
            self.tmp_bam = self.get_outputs('{basename_woext}_noDupl.bam', self.bam_files)
            self.add_shell_execution(self.get_exec_path("javaPICARD")+ " "+ xmx +" -jar " + self.get_exec_path("Picard") + " MarkDuplicates INPUT=$1 METRICS_FILE=$2 OUTPUT=$3" + self.duplication_options + " 2> $4", 
                                     cmd_format='{EXE} {IN} {OUT}', map=True,
                                     inputs=self.bam_files, outputs=[self.duplication_files, self.tmp_bam, self.dupl_stderrs])
        
            self.add_shell_execution("rm $1 2> $2", cmd_format='{EXE} {IN} {OUT}', map=True,
                                     inputs=self.tmp_bam, outputs=self.del_stderrs)
        
        # Alignment quality stats
        if self.csv_files_r2:
            self.add_shell_execution(self.get_exec_path("samtools") + " view -F0x0100 $1 | " + self.get_exec_path("python3") + " " + self.get_exec_path("cigarlineGraph.py") + " -i - -t $2 $3 " + self.cigar_options + " 2> $4", 
                                     cmd_format='{EXE} {IN} {OUT}', map=True,
                                     inputs=self.bam_files, outputs=[self.csv_files_r1, self.csv_files_r2, self.cigar_stderrs])
        else:
            self.add_shell_execution(self.get_exec_path("samtools") + " view -F0x0100 $1 | " + self.get_exec_path("python3") + " " + self.get_exec_path("cigarlineGraph.py") + " -i - -t $2 " + self.cigar_options + " 2> $3", 
                                     cmd_format='{EXE} {IN} {OUT}', map=True,
                                     inputs=self.bam_files, outputs=[self.csv_files_r1, self.cigar_stderrs])
             
        # Alignment summary
        self.add_shell_execution(self.get_exec_path("samtools") + " view -F0x0100 -bh $1 | " + self.get_exec_path("samtools") + " flagstat - > $2", 
                                 cmd_format='{EXE} {IN} {OUT}', map=True,
                                 inputs=self.bam_files, outputs=self.stat_files)
    
        
    def __parse_flagstat_file (self, flagstat_file):
        """
    Parse the flagstat file
          @param flagstat_file : the flagstat file
          @return              : {"total": x, ...}
        """
        # total regexp
        total_regex = re.compile("(\d+) .*in total")
        # qcfailure regexp
        qcfailure_regex = re.compile("(\d+ \+ (\d+) in total)|((\d+) QC failure)")
        # duplicates regexp
        duplicates_regex = re.compile("(\d+) .*duplicates")
        # mapped regexp
        mapped_regex = re.compile("(\d+) .*mapped \(([^:]*).*\)")
        # paired regexp
        paired_regex = re.compile("(\d+) .*paired in sequencing")
        # read1 regexp
        read1_regex = re.compile("(\d+) .*read1")
        # read2 regexp
        read2_regex = re.compile("(\d+) .*read2")
        # matemapped regexp
        matemapped_regex = re.compile("(\d+) .*with itself and mate mapped")
        # properlypaired regexp
        properlypaired_regex = re.compile("(\d+) .*properly paired \(([^:]*).*\)")
        # singletons regexp
        singletons_regex = re.compile("(\d+) .*singletons \(([^:]*).*\)")
        # mapch1 regexp
        mapch1_regex = re.compile("(\d+) .*with mate mapped to a different chr")
        # supplementary regexp
        supplementary_regex = re.compile("(\d+).*supplementary")

        summary = {}
        for line in open(flagstat_file, 'r').readlines():
            tr = total_regex.match(line)
            qcfr = qcfailure_regex.match(line)
            dr = duplicates_regex.match(line)
            mr = mapped_regex.match(line)
            pr = paired_regex.match(line)
            r1r = read1_regex.match(line)
            r2r = read2_regex.match(line)
            mmr = matemapped_regex.match(line)
            ppr = properlypaired_regex.match(line)
            sr = singletons_regex.match(line)
            mc1r = mapch1_regex.match(line)
            sur = supplementary_regex.match(line)
            if tr != None :
                summary["total"] = tr.group(1)
            if qcfr != None :
                if qcfr.group(2) != None :
                    summary["qcfailure"] = qcfr.group(2)
                else:
                    summary["qcfailure"] = qcfr.group(4)
            if dr != None :
                summary["duplicates"] = dr.group(1)
            if mr != None :
                summary["mapped"] = [mr.group(1), mr.group(2)]
            if pr != None :
                summary["paired"] = pr.group(1)
            if r1r != None :
                summary["read1"] = r1r.group(1)
            if r2r != None :
                summary["read2"] = r2r.group(1)
            if mmr != None :
                summary["matemapped"] = mmr.group(1)
            if ppr != None :
                summary["properlypaired"] = [ppr.group(1), ppr.group(2)]
            if sr != None :
                summary["singletons"] = [sr.group(1), sr.group(2)]
            if mc1r != None :
                summary["mapch1"] = mc1r.group(1)
            if sur != None : 
                summary["supplementary"] = sur.group(1)

        return summary

    def __parse_csv_file(self, csv_file):
        match ,mismatch , clipping , insertion = [],[],[],[]
        match_pos ,mismatch_pos ,clipping_pos ,insert_pos = "","","",""
        line_count=0

        summary = {}
        with open(csv_file, 'r') as csv:
            header=csv.readline().split("\t")
            for index, status in enumerate(header):
                if re.search("^match$",status):
                    match_pos=index
                elif re.search("^mismatch$",status):
                    mismatch_pos=index                  
                elif re.search("^clipping$",status):
                    clipping_pos=index                 
                elif re.search("^insertion$",status):
                    insert_pos=index                  
                    
            if match_pos == "" or mismatch_pos == "" or clipping_pos == "" or insert_pos == "":
                sys.stderr.write("[ERROR] : Missing fields in CSV file ! Program will now abort")
                sys.exit()
            else:
                for line in csv.readlines():
                    line_count+=1
                    ligne=line.split("\t")
                    
                    clipping.append(ligne[clipping_pos].rstrip("\n"))
                    insertion.append(ligne[insert_pos].rstrip("\n"))
                    match.append(ligne[match_pos].rstrip("\n"))
                    mismatch.append(ligne[mismatch_pos].rstrip("\n"))
            
            summary["match"] = ";".join(match)
            summary["mismatch"] = ";".join(mismatch)
            summary["clipping"] = ";".join(clipping)
            summary["insertion"] = ";".join(insertion)        
            summary["readsLength"] = line_count
        
        return summary
    
    def __parse_dupl_file(self, dupl_file):
        title = False
        metrics_section = False
        metrics = {}
        metrics_titles = []
        
        fh_file = open(dupl_file, "r")
         
        for line in fh_file:
            line.rstrip('\s\n\r')
             
            if metrics_section:
                if not line.strip():
                    metrics_section = False
                else :
                    if title:
                        metrics_titles = line.split()
                        library_name_index = metrics_titles.index('LIBRARY') 
                        title = False
                    else :
                        metrics_fields = line.split("\t")
                        line_metrics = {}
                        for index in range(len(metrics_fields)):
                            if not metrics_fields[index].strip():
                                metrics_fields[index] = ""
                            line_metrics[metrics_titles[index]] = metrics_fields[index]
                        metrics[metrics_fields[library_name_index]] = line_metrics
            elif line.startswith("## METRICS CLASS"):
                metrics_section = True
                title = True
                
        fh_file.close()

        return metrics
