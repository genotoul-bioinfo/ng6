#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import logging

from jflow.component import Component
from jflow.utils import get_argument_pattern


class MD5sum (Component):
    
    def define_parameters(self, files_list, output_filename):
        """
            calculate the MD5 sum of the files and generates a file
        """
        self.add_input_file_list( "files_list", "files_list", default=files_list, required=True )
        self.add_output_file("output_file", "output_file", filename=output_filename )
            
    def process(self):
        filenamesToMd5sum = ""
        for filename in self.files_list:
            filenamesToMd5sum += filename + " "
        logging.getLogger("MD5sum").debug("process " + 'md5sum ' + filenamesToMd5sum + ' > ' + self.output_file)
        self.add_shell_execution('md5sum ' + filenamesToMd5sum + ' > $1', cmd_format='{EXE} {OUT} {IN}', 
                                     map=False, outputs = self.output_file, inputs=self.files_list)
      
