#
# Copyright (C) 2012 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from ng6.analysis import Analysis

class BasicAnalysis (Analysis):

    def define_parameters(self, files, name, description, software, options, version, compression="none",
                          delete=False, archive_name=None, space_id="default"):

        self.add_parameter_list( "input_files", "The files to be archived.", default=files, required=True )
        self.add_parameter( "analysis_name", "The analysis name", default=name, required = True )
        self.add_parameter( "analysis_description", "Analysis description", default=description, required = True )
        self.add_parameter( "analysis_software", "The analysis software", default=software, required = True )
        self.add_parameter( "analysis_software_options", "The analysis software options", default=options, required = True )
        self.add_parameter( "analysis_software_version", "The analysis software version", default=version, required = True )
        self.add_parameter( "data_compression", "File compression once archived", default=compression)
        self.add_parameter( "delete", "Should the input data be deleted once the process is over", type = bool, default=delete)
        self.add_parameter( "archive_name", "Give a name to the final archive", default=archive_name)
        self.add_parameter( "space_id", "Space identifier", default=space_id)
        self.is_editable = True

    def define_analysis(self):
        self.name = self.analysis_name
        self.description = self.analysis_description
        self.software = self.analysis_software
        self.options = self.analysis_software_options
        self.version = self.analysis_software_version

    def process(self): pass

    def get_version(self): return ''

    def post_process(self):
        self._archive_files(self.input_files, self.data_compression, self.archive_name, self.delete)
