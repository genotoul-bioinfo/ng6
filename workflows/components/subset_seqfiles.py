#
# Copyright (C) 2012 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import logging
from subprocess import Popen, PIPE
from jflow.utils import get_argument_pattern
from weaver.function import ShellFunction
from weaver.function import PythonFunction

from jflow.abstraction import MultiMap
from ng6.analysis import Analysis
from ng6.utils import Utils

def extract_random_seq(extract_rate,min_nb_seq,max_nb_seq,input_file,output_file):
    import jflow.seqio as seqio
    import logging
    import os
    import gzip
    logging.getLogger("SubsetSeqFiles").debug("extract_random_seq. Entering")
    # Check parameters
    extract_rate = float(extract_rate)
    min_nb_seq = int(min_nb_seq)
    max_nb_seq = int(max_nb_seq)
    logging.getLogger("SubsetSeqFiles").debug("extract_random_seq. extract_rate = " + str(extract_rate))
    logging.getLogger("SubsetSeqFiles").debug("extract_random_seq. min_nb_seq = " + str(min_nb_seq))
    logging.getLogger("SubsetSeqFiles").debug("extract_random_seq. max_nb_seq = " + str(max_nb_seq))
    logging.getLogger("SubsetSeqFiles").debug("extract_random_seq. working on filename = " + input_file)
    if extract_rate > 1 or extract_rate <= 0:
        raise Exception("[ERROR] : the extract_rate value is not correct! (Should be between 0 and 1)")
    if int(min_nb_seq) > int(max_nb_seq):
        raise Exception("[ERROR] : the threshold values are not correct ! (Minimum threshold is bigger than Maximum threshold)")
    #with open(info_file, 'r') as myfile:
    #        step,nb_seq_to_extract=myfile.read().split(':')
    #        logging.getLogger("SubsetSeqFiles").debug("extract_random_seq. Content of " + info_file + " step = " + step + ", nb_seq_to_extract = " + nb_seq_to_extract)
    nLines = 0
    nb_seq = 0
    nb_seq_to_extract = 0
    #determine number of reads in the current file
    with gzip.open(input_file) as fh:
        for line in fh:
            nLines = nLines + 1
    nb_seq = int(nLines / 4)
    #set the theorical nb of reads to extract
    nb_seq_to_extract = int(nb_seq * float(extract_rate))
    logging.getLogger("SubsetSeqFiles").debug("extract_random_seq. nb_seq = " + str(nb_seq) + ", nb_seq_to_extract="+str(nb_seq_to_extract) )
    #adapt the number of reads to extract to be in the min and max values, except if file does not have enough reads, in that case use all reads 
    if nb_seq_to_extract > max_nb_seq:
        nb_seq_to_extract = max_nb_seq
    elif nb_seq_to_extract < min_nb_seq:
        nb_seq_to_extract = min_nb_seq
    if nb_seq < nb_seq_to_extract :
        nb_seq_to_extract = nb_seq

    step = int(nb_seq / nb_seq_to_extract)
    logging.getLogger("SubsetSeqFiles").debug("extract_random_seq. subsetting info : "+str(nb_seq_to_extract)+" reads with step " + str(step) + " (The original file contains " + str(nb_seq) + " reads" )
    #step= int(step)
    count = 0
    countExtractedReads = 0
    # Writes result in seq file
    reader = seqio.SequenceReader(input_file)
    logging.getLogger("SubsetSeqFiles").debug("extract_random_seq. creating  =  " + output_file + ", from = " + input_file)
    with open(output_file, "w") as output:
        for id, desc, seq, qual in reader :
            count += 1

            if count % step == 0 and nb_seq_to_extract > countExtractedReads :
                countExtractedReads += 1
                seqio.writefastq(output, [[id, desc, seq, qual]])
            if nb_seq_to_extract <= countExtractedReads:
                break
    logging.getLogger("SubsetSeqFiles").debug("extract_random_seq. finished")

class SubsetSeqFiles (Analysis):

    def define_parameters(self, read1, read2=None, max_target_seqs=10,
                          extract_rate=0.05, min_nb_seq=20000, max_nb_seq=1e6):
        """

        """
        # Parameters
        self.add_parameter("max_target_seqs", "max_target_seqs", type='int', default=max_target_seqs)
        self.add_parameter("extract_rate", "extract_rate", type='float', default=extract_rate)
        self.add_parameter("min_nb_seq", "min_nb_seq", type='int', default=min_nb_seq)
        self.add_parameter("max_nb_seq", "max_nb_seq", type='int', default=max_nb_seq)
        self.add_parameter("number_of_reads_in_subset", "number_of_reads_in_subset", type='int')
        # Files
        self.add_input_file_list( "read1", "Which read1 files should be used.", default=read1, required=True )
        self.add_input_file_list( "read2", "Which read2 files should be used.", default=read2 )
        self.add_output_file_list("subset_read1", "Subset of the read1 file", pattern='{basename_woext}_subset.fastq.gz', items=self.read1)
        self.add_output_file_list("subset_read2", "Subset of the read1 file", pattern='{basename_woext}_subset.fastq.gz', items=self.read2)
        #self.add_output_file("output_file", "output_file", filename="reference_number_of_reads_to_extract.txt" )
    def get_version(self):
        return '-'

    def define_analysis(self):
        self.name = "SubSet reads file"
        self.description = "Extract a subset ot the total reads"
        self.software = "bash"
        self.options = ""

    def post_process(self):
        self.options = "Number of reads in the subset files  = "

    def process(self):

        subset_read1 = self.get_outputs( '{basename_woext}_subset.fastq', self.read1 )
        subset_read1_gz = self.get_outputs( '{basename_woext}_subset.fastq.gz', self.read1 )
        logging.getLogger("SubsetSeqFiles").debug("before subset of reads")
        #self.add_python_execution(get_number_of_reads_to_extract, cmd_format='{EXE} {ARG} {OUT} {IN}',
        #                            map=False, outputs = self.output_file, inputs=self.read1, arguments=[self.extract_rate,self.min_nb_seq, self.max_nb_seq])
        for i,o in zip(self.read1,subset_read1 ):
            self.add_python_execution(extract_random_seq,cmd_format="{EXE} {ARG} {IN} {OUT}",
                                      inputs = [i], outputs = o, map=False, arguments=[self.extract_rate,self.min_nb_seq, self.max_nb_seq])
        if self.read2:
            subset_read2 = self.get_outputs( '{basename_woext}_subset.fastq', self.read2 )
            subset_read2_gz = self.get_outputs( '{basename_woext}_subset.fastq.gz', self.read2 )

            for i,o in zip(self.read2,subset_read2 ):
                self.add_python_execution(extract_random_seq,cmd_format="{EXE} {ARG} {IN} {OUT}",
                                      inputs = [i], outputs = o, map=False,arguments=[self.extract_rate,self.min_nb_seq, self.max_nb_seq])

        gzip = ShellFunction( self.get_exec_path("gzip")+' $1', cmd_format='{EXE} {IN} {OUT}')
        MultiMap(gzip, inputs=[subset_read1], outputs=[subset_read1_gz])
        self.read1 = subset_read1_gz

        if self.read2:
            MultiMap(gzip, inputs=[subset_read2], outputs=[subset_read2_gz])
            self.read2 = subset_read2_gz