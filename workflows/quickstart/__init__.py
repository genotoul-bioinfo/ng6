#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from ng6.ng6workflow import NG6Workflow

class QuickStart(NG6Workflow):
    
    def get_name(self):
        return 'quickstart'
    
    def get_description(self):
        return "Quick start pipeline"
    
    def define_parameters(self, function="process"):
        self.add_parameter( "keep", "keep option for fastq_illumina_filter", default="N", choices=["N", "Y"])
    
    def process(self):
        illumina_filter = self.add_component("QIlluminaFilter", [self.get_all_reads()])
        fastqc = self.add_component("QFastQC", [illumina_filter.fastq_files_filtered], parent = illumina_filter)
        
        