#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import subprocess
import os
import re

from weaver.function import ShellFunction

from jflow.abstraction import MultiMap

from ng6.analysis import Analysis
    
class QFastQC (Analysis):
        
    def define_parameters(self, input_files):
        self.add_input_file_list("input_files", "Fastq file", default=input_files, required=True)
        self.add_output_file_list( "output_fastqc", "outputs", pattern='{basename_woext}_fastqc', items=input_files)
        self.add_output_file_list( "stdouts", "stdout", pattern='{basename_woext}.stdout', items=input_files)
        self.add_output_file_list( "stderrs", "stderr", pattern='{basename_woext}.stderr', items=input_files)

    def get_version(self):
        p = subprocess.Popen([self.get_exec_path("fastqc"), "--version"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        return stdout.split()[1]
    
    def define_analysis(self):
        self.name = "QFastQC"
        self.description = "Statistics on reads and their qualities"
        self.software = "fastqc"
        self.options = ""
    
    def __parse_summary_file (self, summary_file):
        per_base_quality = ""
        per_base_sequence_content = ""
        for line in open(summary_file, 'r').readlines():
            parts = line.strip().split("\t")
            if parts[1] == "Per base sequence quality": per_base_quality = parts[0]
            if parts[1] == "Per base sequence content": per_base_sequence_content = parts[0]
        return per_base_quality, per_base_sequence_content
        
    def process(self):
        fastqc = ShellFunction( self.get_exec_path("fastqc") + ' --extract --outdir ' + self.output_directory + '  $1 > $2 2>> $3 ', cmd_format='{EXE} {IN} {OUT}')
        MultiMap(fastqc, inputs=[self.input_files], outputs=[self.stdouts, self.stderrs, self.output_fastqc])

    def post_process(self):
        for output_fastqc_dir in self.output_fastqc :
            sample = re.sub( "_fastqc$", "", os.path.basename(output_fastqc_dir))
            per_base_quality, per_base_sequence_content = self.__parse_summary_file (os.path.join( output_fastqc_dir, "summary.txt" ))
            
            self._add_result_element(sample, "img", self._save_file(os.path.join(output_fastqc_dir, "Images", "per_base_quality.png"), sample + ".per_base_quality.png"), "pbqpng")
            self._add_result_element(sample, "result", str(per_base_quality), "pbqpng")

            self._add_result_element(sample, "img", self._save_file(os.path.join(output_fastqc_dir, "Images", "per_base_sequence_content.png"), sample + ".per_base_sequence_content.png"), "pbspng")     
            self._add_result_element(sample, "result", str(per_base_sequence_content), "pbspng")