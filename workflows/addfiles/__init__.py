#
# Copyright (C) 2012 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import sys

from ng6.ng6workflow import BasicNG6Workflow
from ng6.project import Project
from ng6.run import Run
from ng6.analysis import Analysis

class AddFiles (BasicNG6Workflow):

    def get_description(self):
        return "Add files to an existing analysis or run"

    def define_parameters(self, function="process"):
        self.add_input_file_list("data_file", " Which data files should be added to the analysis or the run", required = True)
        self.add_parameter("compression", "How should data be compressed once archived", choices = ['none', 'gz', 'bz2'], default = 'none')
        self.add_parameter("delete", "Should the input data be deleted once the process is over", type = bool , default = False)
        self.add_parameter("analysis_id", "The id of the analysis", type = int, rules="Exclude=run_id;RequiredIf?ALL[run_id=None]")
        self.add_parameter("run_id", "The run id the run belongs to", type = 'int')

    def process(self):
        # check if files provided exists

        files_to_save = []
        for file in self.data_file:
            if os.path.isfile(file):
                files_to_save.append(file)
            else:
                sys.exit("error: %s file does not exists\n" % (file))

        self.runobj = None
        self.analysisobj = None
        self.project = None

        if self.run_id :
            self.runobj = Run.get_from_id(self.run_id)
            self.project = Project.get_from_run_id(self.run_id)
            if self.project is not None and not self.project.is_admin(self.admin_login):
                sys.stderr.write( "The user login '" + self.admin_login + "' is not allowed to add data on project '" + self.project.name + "'.\n" )
                sys.exit()

            self.metadata.append("run_id="+str(self.run_id))
            add = self.add_component("AddRawFiles", [ self.runobj, files_to_save, self.compression ] )

        elif self.analysis_id :
            self.analysisobj = Analysis.get_from_id(self.analysis_id)
            self.project = self.analysisobj.project
            if self.project is not None and not self.project.is_admin(self.admin_login):
                sys.stderr.write( "The user login '" + self.admin_login + "' is not allowed to add data on project '" + self.project.name + "'.\n" )
                sys.exit()

            self.metadata.append("analysis_id="+str(self.analysis_id))
            add = self.add_component("AddAnalysisFiles", [ self.analysisobj, files_to_save, self.compression == "gz" ] )
        else :
            sys.stderr.write("One of --run-id or --analysis-id parameter is required!\n")
            sys.exit()

    def post_process(self):
        # once everything done, sync directories
        if self.runobj:
            self.runobj.sync()
        elif self.analysisobj:
            self.analysisobj.sync()
