#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import pickle

from jflow.component import Component

from weaver.function import PythonFunction

from ng6.analysis import Analysis


def add_analysis_file (analysis_dump_path, files_dump_path, gzip):
    import pickle
    # load the analysis object
    analysis_dump = open(analysis_dump_path, "rb")
    my_analysis = pickle.load(analysis_dump)
    analysis_dump.close()
    files_dump = open(files_dump_path, "rb")
    files_to_save = pickle.load(files_dump)
    files_dump.close()
    my_analysis._save_files(files_to_save, gzip)

class AddAnalysisFiles (Component):
    
    def define_parameters(self, analysisobj, files_to_save, gzip = True):
        self.analysisobj = analysisobj
        self.add_input_file_list( "files_to_save", "File to be saved as analysis files", default=files_to_save, required=True)
        self.add_parameter("gzip", "Should the data be gziped", default=gzip)
        self.add_output_file("stdout", "AddAnalysisFiles stdout", filename="AddAnalysisFiles.stdout")
        
    def process(self):
        self.analysisobj.raw_files = self.files_to_save
        analysis_dump_path = self.get_temporary_file(".dump")
        analysis_dump = open(analysis_dump_path, "wb")
        pickle.dump(self.analysisobj, analysis_dump)
        analysis_dump.close()
        
        files_dump_path = self.get_temporary_file(".dump")
        files_dump = open(files_dump_path, "wb")
        pickle.dump(self.files_to_save, files_dump)
        files_dump.close()
        self.add_python_execution(add_analysis_file, 
                                 cmd_format='{EXE} {ARG} > {OUT}',
                                 outputs=self.stdout, includes=self.files_to_save, arguments=[analysis_dump_path, files_dump_path, self.gzip], map=False)
            
    