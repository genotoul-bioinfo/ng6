{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

<div id="error_dialog" title=""></div>

<div class="sub-content sc-top-bottom">
	<input type="hidden" id="is_project_admin" value="{$projects[key($projects)].is_admin}" />
	
	<div class="ng6-content-header-left project">
		<h2>Installation steps</h2>
		Please provide the following information in order to finalize the installation. The Administrator user is the first 
		user able to run workflows, but he can also give administrator access to others so they can run workflows by their
		own. 
	</div>
	
	<div class="ng6-content-header-right-white">
		<legend>Define the administrator user</legend>
		
	    <form class="form-horizontal" id="install_form">
			<input type="hidden" id="server_name" value="{$server_name}" />
			<input type="hidden" id="server_url" value="{$server_url}" />
			<input type="hidden" id="pid" value="{$pid}" />
	
			<div class="form-group">
			    <label class="col-sm-2 control-label">Last name</label>
			    <div class="col-sm-10">
			        <input type="text" id="last_name_val" class="form-control" name="last_name_val" placeholder="Last name"/>  
			    </div>
			</div>
	
		    <div class="form-group">
		        <label class="col-sm-2 control-label">First name</label>
		        <div class="col-sm-10">
		          <input type="text" id="first_name_val" class="form-control" name="first_name_val" placeholder="First name"/>
		        </div>
		    </div>
		    
		    <div class="form-group">
		        <label class="col-sm-2 control-label">Login</label>
		        <div class="col-sm-10">
		          <input type="text" id="username_val" class="form-control" name="username_val" placeholder="Login"/>
		        </div>
		    </div>
	
		    <div class="form-group">
		        <label class="col-sm-2 control-label">Password</label>
		        <div class="col-sm-10">
		          <div class="input-append">
		            <input class="form-control" type="password" id="user_password_pwd_val" name="user_password_pwd_val" placeholder="Password"/> 
		          </div>
		        </div>
		    </div>
		    
		    <div class="form-group">
		        <label class="col-sm-2 control-label">Retype password</label>
		        <div class="col-sm-10">
		          <div class="input-append">
		            <input class="form-control" type="password" id="user_password_pwd2_val" name="user_password_pwd2_val" placeholder="Password"/> 
		          </div>
		        </div>
		    </div>
		    
		    <div class="form-group">
		        <label class="col-sm-2 control-label">Email</label>
		        <div class="col-sm-10">
		          <input type="text" id="email_val" class="form-control" name="email_val" placeholder="Email"/>
		        </div>
		    </div>
		    
			<div class="form-group">
				<label class="col-sm-2 control-label">Laboratory</label>
				<div class="col-sm-10">
					<input type="text" id="title_val" class="form-control" name="title_val" placeholder="Laboratory"/>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label">Organism</label>
				<div class="col-sm-10">
					<input type="text" id="organism_val" class="form-control" name="organism_val" placeholder="Organism"/>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label">Location</label>
				<div class="col-sm-10">
					<input type="text" id="location_val" class="form-control" name="location_val" placeholder="Location"/>
				</div>
			</div>

	    </form>
		<div style="float:right;margin-top:10px">
		    	<button class="btn btn-default" id="raz_form_install"><i class="glyphicon glyphicon-repeat"></i> Clear form</button>
				<button id="btn-install" class="btn btn-primary"> Install NG6</button>
		</div>
	</div>
	<div style="clear:both"></div>
</div>
