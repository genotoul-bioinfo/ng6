{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}
{* Recursive function to display an analysis hierarchy *}
{foreach from=$element item=new_element}
	{if ($new_element.data.hidden == 1 && $new_element.data.is_admin) || $new_element.data.hidden == 0}
		{assign var="classes" value=""}
		{if $new_element.data.hidden == 1}
			{$classes = $classes|cat:" <span class='label label-info'><i class='glyphicon glyphicon-eye-close'></i></span> "}
		{/if}
		<tr id="tr_analysis_{$new_element.data.id}">
			{assign var="editable" value=""}
			{if $new_element.data.is_admin}
				<td><center><input type="checkbox" id="chk_analysis_{$new_element.data.id}" value="analysis_{$new_element.data.id}"></center></td>
				<td class="editable" nowrap>
					{assign var="analyze_tree" value=""}
					{section name=customer loop=$prof*3}
						{assign var="analyze_tree" value="$analyze_tree &nbsp;"}
					{/section}
					{if $prof != 0}
						{assign var="analyze_tree" value="$analyze_tree |-- "}
					{/if}
					{$analyze_tree} {$classes} <span class="editable" data-type="text" data-pk="{$new_element.data.id}" data-url="index.php?eID=tx_nG6&type=update_db_field&table=tx_nG6_analyze&field=name" data-original-title="Enter name">{$new_element.data.href}</span>
				</td>
				<td class="editable"> 
					<span class="editable" data-type="textarea" data-pk="{$new_element.data.id}" data-url="index.php?eID=tx_nG6&type=update_db_field&table=tx_nG6_analyze&field=description" data-original-title="Enter description">{$new_element.data.description}</span>
				</td>
			{else}
				<td nowrap>
					{assign var="analyze_tree" value=""}
					{section name=customer loop=$prof*3}
						{assign var="analyze_tree" value="$analyze_tree &nbsp;"}
					{/section}
					{if $prof != 0}
						{assign var="analyze_tree" value="$analyze_tree |-- "}
					{/if}
					{$analyze_tree}{$new_element.data.href}
				</td>
				<td>{$new_element.data.description}</td>
			{/if}
			{if $new_element.data.is_admin}
				{if $new_element.data.is_editable}
					<td class="editable"> 
						<span class="editable-date" data-type="date" data-pk="{$new_element.data.id}" data-url="index.php?eID=tx_nG6&type=update_db_field&table=tx_nG6_analyze&field=crdate" data-original-title="Select date">{$new_element.data.date|date_format:"%d/%m/%Y"}</span>
					</td>
					<td class="editable"> 
					<span class="editable" data-type="textarea" data-pk="{$new_element.data.id}" data-url="index.php?eID=tx_nG6&type=update_db_field&table=tx_nG6_analyze&field=software" data-original-title="Enter software name">{$new_element.data.software}</span>
					</td>
					<td class="editable"> 
					<span class="editable" data-type="textarea" data-pk="{$new_element.data.id}" data-url="index.php?eID=tx_nG6&type=update_db_field&table=tx_nG6_analyze&field=version" data-original-title="Enter software version">{$new_element.data.version}</span>
					</td>
				{else}
					<td>{$new_element.data.date|date_format:"%d/%m/%Y"}</td>
					<td>{$new_element.data.software}</td>
					<td>{$new_element.data.version}</td>
				{/if}
			{else}
				<td>{$new_element.data.date|date_format:"%d/%m/%Y"}</td>
				<td>{$new_element.data.software}</td>
				<td>{$new_element.data.version}</td>
			{/if}
			<td>{if $new_element.data.data_state eq "stored"}<i title="Data stored until {$new_element.data.retention_date|date_format:"%d/%m/%y"}" class="glyphicon glyphicon-saved"></i>
						{elseif $new_element.data.data_state eq "purged"}<i title="Data purged since {$new_element.data.purged_date|date_format:"%d/%m/%y"}" class="glyphicon glyphicon-trash"></i>
						{elseif $new_element.data.data_state eq "excluded"}<i title="Data purged since {$new_element.data.purged_date|date_format:"%d/%m/%y"}" class="glyphicon glyphicon-trash"></i>
						{elseif $new_element.data.data_state eq "extended"}<i title="Data stored until {$new_element.data.retention_date|date_format:"%d/%m/%y"}" class="glyphicon glyphicon-repeat"></i>{/if}
						{if $new_element.data.mail_sent_date != 0}<i title="Mail sended since {$new_element.data.mail_sent_date|date_format:"%d/%m/%y"}" class="glyphicon glyphicon-envelope"></i>{/if}
			</td>
		</tr>
	{/if}
	{* recall *}
	{if $new_element.child}
		{$new_prof = $prof + 1}
		{include file="recursive_list_analysis_display.tpl" element=$new_element.child prof=$new_prof}
	{/if}
{/foreach}
