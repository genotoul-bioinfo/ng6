{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{* Modal initialization *}
{* Global tmpl *}
<div id="ng6modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-label-tmpl" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		        <h3 id="modal-label-tmpl">Modal header</h3>
	        </div>
	        <div id="modal-body-tmpl" class="modal-body"></div>
	        <div id="modal-foot-tmpl" class="modal-footer"></div>
        </div>
    </div>
</div>

{* setAndRunModal *}
<div id="setAndRunModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="setAndRunModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
	        <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		        <h3 id="setAndRunModalLabel"></h3>
	        </div>
	        <div id="setAndRunModalBody" class="modal-body"></div>
	        <div id="setAndRunModalFooter" class="modal-footer">
	        </div>
        </div>
    </div>
</div>
