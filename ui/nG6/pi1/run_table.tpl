{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}
<table class="table table-striped table-bordered dataTable" id="run_data_table">
	<thead>
		<tr>
		    {assign var="data_col" value=0}
		    {assign var="rcolspan" value=10}
			{if $is_at_least_admin_of_1_run && $login_user}
			    <th data-column="{$data_col}"><center><input type="checkbox" id="chk_all_run"></center></th>
			    {assign var="rcolspan" value= $rcolspan + 1}
			    {$data_col = $data_col+1}
			{/if}
			<th data-column="{$data_col}">Run Name</th>
			{$data_col = $data_col+1}
			<th data-column="{$data_col}" class="full-run-view">Project Name</th>
			{$data_col = $data_col+1}
			<th data-column="{$data_col}">Date</th>
			{$data_col = $data_col+1}
			<th data-column="{$data_col}">Species</th>
			{$data_col = $data_col+1}
			<th data-column="{$data_col}">Data nature</th>
			{$data_col = $data_col+1}
			<th data-column="{$data_col}">Type</th>
			{$data_col = $data_col+1}
			<th data-column="{$data_col}">Number of sequences</th>
			{$data_col = $data_col+1}
			<th data-column="{$data_col}">Full sequences length</th>
			{$data_col = $data_col+1}
			<th data-column="{$data_col}" class="full-run-view">Description</th>
			{$data_col = $data_col+1}
			<th data-column="{$data_col}">Sequencer</th>
			{$data_col = $data_col+1}
			{if $is_at_least_admin_of_1_run && $login_user}
			    <th data-column="{$data_col}" id="wf_run_all">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
			    {assign var="rcolspan" value= $rcolspan + 1}
			    {$data_col = $data_col+1}
			{/if}
			<th data-column="{$data_col}">Data state</th>
			{$data_col = $data_col+1}
		</tr>
	</thead>
	<tbody>
		{foreach from=$runs key=run_id item=run_values}
			{if ($run_values.hidden == 1 && $run_values.is_admin) || $run_values.hidden == 0}
				{assign var="classes" value=""}
				{if $run_values.hidden == 1}
					{$classes = $classes|cat:" <span class='label label-info'><i class='glyphicon glyphicon-eye-close'></i></span> "}
				{/if}
				<tr id="tr_run_{$run_values.id}">
					{if $run_values.is_admin}
						<td><center><input type="checkbox" id="chk_run_{$run_values.id}" value="run_{$run_values.id}"></center></td>
						<td class="editable" nowrap> 
							{$classes} <span class="editable" data-type="text" data-pk="{$run_values.id}" data-url="index.php?eID=tx_nG6&type=update_db_field&table=tx_nG6_run&field=name" data-original-title="Enter name">{$run_values.href}</span>
						</td>
						<td>{$run_values.project_name}</td>
						<td class="editable"> 
							<span class="editable-date" data-type="date" data-pk="{$run_values.id}" data-url="index.php?eID=tx_nG6&type=update_db_field&table=tx_nG6_run&field=date" data-original-title="Select date">{$run_values.date|date_format:"%d/%m/%Y"}</span>
						</td>
						<td class="editable"> 
							<span class="editable" data-type="text" data-pk="{$run_values.id}" data-url="index.php?eID=tx_nG6&type=update_db_field&table=tx_nG6_run&field=species" data-original-title="Enter species">{$run_values.species}</span>
						</td>
						<td class="editable"> 
							<span class="editable" data-type="text" data-pk="{$run_values.id}" data-url="index.php?eID=tx_nG6&type=update_db_field&table=tx_nG6_run&field=data_nature" data-original-title="Enter data nature">{$run_values.data_nature}</span>
						</td>
						<td class="editable"> 
							<span class="editable" data-type="text" data-pk="{$run_values.id}" data-url="index.php?eID=tx_nG6&type=update_db_field&table=tx_nG6_run&field=type" data-original-title="Enter type">{$run_values.type}</span>
						</td>	
						<td>{$run_values.nb_sequences|number_format:0:' ':' '}</td>
						<td>{$run_values.full_seq_size|number_format:0:' ':' '}</td>
						<td class="editable"> 
							<span class="editable" data-type="textarea" data-pk="{$run_values.id}" data-url="index.php?eID=tx_nG6&type=update_db_field&table=tx_nG6_run&field=description" data-original-title="Enter description">{$run_values.description}</span>
						</td>
						<td class="editable"> 
							<span class="editable" data-type="text" data-pk="{$run_values.id}" data-url="index.php?eID=tx_nG6&type=update_db_field&table=tx_nG6_run&field=sequencer" data-original-title="Enter sequencer">{$run_values.sequencer}</span>
						</td>
						<td id="wf_run_{$run_values.id}"></td>
					{else}
						{if $is_at_least_admin_of_1_run && $login_user}
							<td></td>
						{/if}
						<td>{$run_values.href}</td>
						<td>{$run_values.project_name}</td>
						<td>{$run_values.date|date_format:"%d/%m/%y"}</td>
						<td>{$run_values.species}</td>
						<td>{$run_values.data_nature}</td>
						<td>{$run_values.type}</td>
						<td>{$run_values.nb_sequences|number_format:0:' ':' '}</td>
						<td>{$run_values.full_seq_size|number_format:0:' ':' '}</td>
						<td>{$run_values.description}</td>
						<td>{$run_values.sequencer}</td>
						{if $is_at_least_admin_of_1_run && $login_user}
							<td></td>
						{/if}
					{/if}
						{* icone change if data stored/purged  *}
                        <td>{if $run_values.data_state eq "stored"}<i title="Data stored until {$run_values.retention_date|date_format:"%d/%m/%y"}" class="glyphicon glyphicon-saved"></i>
                        	{elseif $run_values.data_state eq "purged"}<i title="Data purged the {$run_values.purged_date|date_format:"%d/%m/%y"}" class="glyphicon glyphicon-trash"></i>
                        	{elseif $run_values.data_state eq "extended"}<i title="Data stored until {$run_values.retention_date|date_format:"%d/%m/%y"}" class="glyphicon glyphicon-repeat"></i>{/if}
                        	{if $run_values.mail_sent_date != 0}<i title="Mail sent the {$run_values.mail_sent_date|date_format:"%d/%m/%y"}" class="glyphicon glyphicon-envelope"></i>{/if}
                        </td>
				</tr>
			{/if}
		{/foreach}
	</tbody>
	{if ($is_at_least_admin_of_1_run && $login_user) || ($is_project_admin)}
	<tfoot>
		<tr>
			<th align="left" colspan="{$rcolspan}">
				With selection :
				<div  class="btn-group">
					<button id="hide_run" type="button" class="btn multipler-selection-btn btn-sm btn-default"><i class="glyphicon glyphicon-eye-close"></i> hide</button>
					<button id="unhide_run" type="button" class="btn multipler-selection-btn btn-sm btn-default"><i class="glyphicon glyphicon-eye-open"></i> unhide</button>
				</div>
				<div  class="btn-group">
					<button id="add_run" type="button" class="btn nor-selection-btn btn-sm btn-default"><i class="glyphicon glyphicon-plus"></i> add</button>
					<button id="delete_run" type="button" class="btn multipler-selection-btn btn-sm btn-default"><i class="glyphicon glyphicon-minus"></i> delete</button>
				</div>
				<div class="btn-group">
				    <button id="toggle_runtable_display" type="button" class="btn btn-info btn-sm" >
				        Full view
				    </button>
				</div>
			</th>
		</tr>
	</tfoot>
	{else}
	<tfoot>
		<tr>
			<th align="left" colspan="{$rcolspan}">
				<div id="toggle_runtable_display" class="btn-group">
				    <button id="toggle_runtable_display" type="button" class="btn btn-info btn-sm" >
				        Full view
				    </button>
				</div>
			</th>
		</tr>
	</tfoot>
	{/if}
</table>
