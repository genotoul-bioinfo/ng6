{*
Copyright (C) 2009 INRA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

<div id="user_information_dialog" title=""></div>
<div id="error_dialog" title=""></div>

<input type="hidden" id="server_name" value="{$server_name}" />
<input type="hidden" id="from_email" value="{$from_email}" />
<input type="hidden" id="envelope_sender_address" value="{$envelope_sender_address}" />
<input type="hidden" id="user_id" value="{$user_id}" />
<input type="hidden" id="user_login" value="{$user_login}" />
<input type="hidden" id="pid" value="{$pid}" />
<input type="hidden" id="data_folder" value="{$data_folder}" />
<input type="hidden" id="view" value="project" />
<input type="hidden" id="ids" value="{$project_ids}" />
<input type="hidden" id="login_user" value="{$login_user}" />
<input type="hidden" id="server_url" value="{$server_url}" />

{* If no project can be displayed *}
{if $projects|@count == 0 || ($projects|@count == 1 && $projects[key($projects)].hidden == 1 && !$projects[key($projects)].is_admin)}
	<div class="sub-content" >
		<br/>
		<div class="alert alert-info">Sorry no results to display</div>
	</div>

{* If a single project has to be displayed *}
{elseif $single_project_display}
	<div class="sub-content sc-top">
		<input type="hidden" id="is_project_admin" value="{$projects[key($projects)].is_admin}" />

		<div class="ng6-content-header-left project">
			<h2>Projet <small>{$projects[key($projects)].name}</small></h2>
			<div><p>Current space: <b><span id="current_space_id">{$projects[key($projects)].space_id}</span></b></p></div>
			<div><p>{$space_purge_msg}</p></div>
			{if $projects[key($projects)].is_admin}
                                <div id="migration_form"></div>
			{/if}
		</div>
		<div class="ng6-content-header-right">
			{assign var="manag_values" value="{$managment_purged_data[$projects[key($projects)].id]}"}
                        {$projects[key($projects)].description} <br /> <br />
			<strong>{$manag_values.nb_runs}</strong> run(s) and
			<strong>{$manag_values.nb_analyses}</strong> analyse(s) have been done on the project {$projects[key($projects)].name}.
			<br />
			
			{* We don't need to display the following part anymore. *}
			{*
			{assign var="project_size" value="<span id='size' class='tx-nG6-mini-wait'></span>"}
			Raw data and analysis results use <strong>{$project_size}</strong> on the hard drive for the whole project.<br />
			*}
			
			<!-- New data added to this project will be kept<b>{$retention_policy}</b>. <br /> -->
			<br><label>Data overview</label><br>

			<table class="table table-striped table-bordered dataTable" id="manag_purged_data_table">
				<thead>
					<tr>
						<th rowspan="1">Data</th>
						<th rowspan="1">Purged</th>
						<th rowspan="1">Stored</th>
						<th rowspan="1">Extended</th>
						<th rowspan="1">All</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th rowspan="1"># Runs</th>
						<th rowspan="1">{if $manag_values.state.purged.nb_run}{$manag_values.state.purged.nb_run} ({$tx_nG6_utils->get_octet_string_representation($manag_values.state.purged.size_run)}){else}-{/if}</th>
						<th rowspan="1">{if $manag_values.state.stored.nb_run}{$manag_values.state.stored.nb_run} ({$tx_nG6_utils->get_octet_string_representation($manag_values.state.stored.size_run)}){else}-{/if}</th>
						<th rowspan="1">{if $manag_values.state.extended.nb_run}{$manag_values.state.extended.nb_run} ({$tx_nG6_utils->get_octet_string_representation($manag_values.state.extended.size_run)}){else}-{/if}</th>
						<th rowspan="1">{$manag_values.nb_runs}</th>
					</tr>
					<tr>
						<th rowspan="1"># Analysis</th>
						<th rowspan="1">{if $manag_values.state.purged.nb_analyze}{$manag_values.state.purged.nb_analyze} ({$tx_nG6_utils->get_octet_string_representation($manag_values.state.purged.size_analyze)}){else}-{/if}</th>
						<th rowspan="1">{if $manag_values.state.stored.nb_analyze}{$manag_values.state.stored.nb_analyze} ({$tx_nG6_utils->get_octet_string_representation($manag_values.state.stored.size_analyze)}){else}-{/if}</th>
						<th rowspan="1">{if $manag_values.state.extended.nb_analyze}{$manag_values.state.extended.nb_analyze} ({$tx_nG6_utils->get_octet_string_representation($manag_values.state.extended.size_analyze)}){else}-{/if}</th>
						<th rowspan="1">{$manag_values.nb_analyses}</th>
					</tr>
				</tbody>
			</table>
			<br />
				<div class="alert alert-info" name="Adding_user">
					<p style="text-align: center;">Your role is defined on the <strong>Users tab</strong> below. If you are manager or owner, you can add a collaborator yourself. Please visit the <a href="https://ng6.toulouse.inra.fr/faq" target="_blank"><strong>FAQ</strong></a> > "<strong>What about user management</strong>" & "<strong>How to add a user to my project</strong>" to know more.</p>
				</div>


		</div>
		<div style="clear:both"></div>
	</div>

	<div class="sub-content sc-bottom">
		<ul id="myTab" class="nav nav-tabs">
			<li class="active"><a href="#runs" data-toggle="tab">Runs</a></li>
	 		<li><a href="#analyses" data-toggle="tab">Analyses</a></li>
	 		{if $login_user}
	 			<li><a href="#users" data-toggle="tab">Users</a></li>
	 		{/if}
	 		{assign var="comment_tab" value="project_comments"}
 		    <li><a href="#{$comment_tab}" data-toggle="tab">Comments</a></li>
		</ul>

	 	<div id="myTabContent" class="tab-content">
	 		<div class="tab-pane fade in active" id="runs">
	 			{* Find out if the current user is super user of a run *}
	 			{if $project_runs|@count == 0}
	 				{assign var="is_at_least_admin_of_1_run" value=false}
				{elseif $projects[key($projects)].is_admin}
					{assign var="is_at_least_admin_of_1_run" value=true}
				{else}
					{assign var="is_at_least_admin_of_1_run" value=false}
				{/if}
				{assign var="runs" value=$project_runs}
				{assign var="is_project_admin" value=$projects[key($projects)].is_admin}
				{include file='run_table.tpl'}
			</div>

			<div class="tab-pane fade" id="analyses">
				{* Find out if an analysis table should be displayed *}
				{assign var="display_analysis_result" value=false}
				{foreach from=$h_analysis key=analysis_id item=analysis_values}
					{if $analysis_values.is_admin && $analysis_values.hidden == 1}
						{assign var="display_analysis_result" value=true}
					{elseif $analysis_values.hidden == 0}
						{assign var="display_analysis_result" value=true}
					{/if}
				{/foreach}
				{assign var="is_admin" value=$projects[key($projects)].is_admin}
				{include file='analysis_table.tpl'}
			</div>

			{if $login_user}
			<div class="tab-pane fade" id="users">
				<input type="hidden" id="current_project_id" value="{$projects[key($projects)].id}" />
				<input type="hidden" id="current_project_name" value="{$projects[key($projects)].name}" />
				<p>
				{if $projects[key($projects)].is_admin}
					In the table below are listed all users with an access to the project.
				{elseif $projects[key($projects)].is_manager}
					In the table below are listed all users with an access to the project, the administrator is the user who had run the pipeline. As a manager, you are in charge of managing access to the project. To manage users access, you can use the add and the delete buttons on the bottom of the table. The add button will display a form so you can seek for a user or add a brand new one. 2 access levels are available: the manager who has the right to create new users and to modify users' information he had created, and member who has only the right to browse the project.
				{elseif $projects[key($projects)].is_member}
					In the table below are listed all users with an access to the project, the administrator is the user who had run the pipeline and the manager is the user in charge of managing access to the project. As a project member you can browse all the data related to the project. For further information, send an email to the project manager(s).
				{/if}
				</p>
				<table class="table table-striped table-bordered dataTable" id="users_data_table">
					<thead>
						<tr>
							<th><center><input type="checkbox" id="chk_all_user"></center></th>
							<th nowrap>Login</th>
							<th>Last login</th>
							<th>Creator</th>
							<th>Last name</th>
							<th>First name</th>
							<th>Email</th>
							<th>Right</th>
						</tr>
					</thead>
					{assign var=current_user_values value=""}
					<tbody>
						{foreach from=$project_users key=c_user_id item=user_values}

							{* highlight user personnal line *}
							{assign var="emphasis" value=""}
							{if $user_values.id==$user_id}
							    {assign var=current_user_values value=$user_values}
								{assign var="emphasis" value="tx-nG6-pi1-line-emphasis"}
							{/if}

							<tr id="tr_user_{$user_values.id}" class="{$emphasis}">

								{if ($user_values.cruser_id==$user_id)
								    || $projects[key($projects)].is_admin
								    || $projects[key($projects)].is_manager
								    || ($user_values.id == $user_id )
								    || $is_ng6_admin }
									<td><center><input type="checkbox" id="chk_user_{$user_values.id}" value="user_{$user_values.id}" /></center>
									{* If the current user is superadmin, it is allowed to update anyone's password. *}
									{if $is_ng6_superadmin}
										<input type="hidden" id="chk_cruser_{$user_values.id}" value="{$user_id}"/></td>
										<input type="hidden" id="chk_updatablepwd_{$user_values.id}" value="1"/></td>
									{else}
										<input type="hidden" id="chk_cruser_{$user_values.id}" value="{$user_values.cruser_id}"/></td>
										<input type="hidden" id="chk_updatablepwd_{$user_values.id}" value="{$user_values.updatable_password}"/></td>
									{/if}
								{else}
									<td></td>
								{/if}

								<td>{$user_values.username}</td>
								<td>{$user_values.lastlogin}</td>
								<td>{$user_values.cruser_name}</td>

								{if $user_values.id==$user_id || $user_values.cruser_id==$user_id }
									<td class="editable">
										<span class="editable" data-type="text" data-pk="{$user_values.id}" data-url="index.php?eID=tx_nG6&type=update_db_field&table=fe_users&field=last_name" data-original-title="Last name">{$user_values.last_name}</span>
									</td>
									<td class="editable">
										<span class="editable" data-type="text" data-pk="{$user_values.id}" data-url="index.php?eID=tx_nG6&type=update_db_field&table=fe_users&field=first_name" data-original-title="First name">{$user_values.first_name}</span>
									</td>
									<td class="editable">
										<span class="editable" data-type="text" data-pk="{$user_values.id}" data-url="index.php?eID=tx_nG6&type=update_db_field&table=fe_users&field=email" data-original-title="Email">{$user_values.email}</span>
									</td>
								{else}
									<td>{$user_values.last_name}</td>
									<td>{$user_values.first_name}</td>
									<td>{$user_values.email}</td>
								{/if}

								<td>
									{if $user_values.right_id=='2' && !$projects[key($projects)].is_admin}
										<select class="form-control" id="cbb_right_level_{$user_values.id}" name="right_level" disabled>
											<option value="2" selected="selected">Administrator</option>
										</select>
									{else}
										{if ($projects[key($projects)].is_admin && $user_values.id!=$user_id)
											|| ($projects[key($projects)].is_manager && $user_values.id!=$user_id && $user_values.right_id!='2')}
											{assign var="disabled" value=""}
										{else}
											{assign var="disabled" value="disabled"}
										{/if}
										<select  class="form-control" id="cbb_right_level_{$user_values.id}" name="right_level" {$disabled}>
											{if $projects[key($projects)].is_admin}
												<option value="2" selected="selected">Administrator</option>
											{/if}

											{if $user_values.right_id=='1'}
										    	<option value="1" selected="selected">Manager</option>
										    {else}
										    	<option value="1">Manager</option>
										    {/if}

										    {if $user_values.right_id=='0'}
										    	<option value="0" selected="selected">Member</option>
										    {else}
										    	<option value="0">Member</option>
										    {/if}
										</select>
									{/if}
								</td>

							</tr>
						{/foreach}
					</tbody>
					{assign var="disabled" value=""}
                    {assign var="nou_selection_btn" value="nou-selection-btn"}
                    {assign var="multipleu_selection_btn" value="multipleu-selection-btn"}
                    {if $projects[key($projects)].is_admin || $projects[key($projects)].is_manager}

                    {elseif $current_user_values.id==$user_id}
	                    {assign var="disabled" value="disabled"}

	                    {if $current_user_values.right_id == '0'}
	                        {assign var="nou_selection_btn" value=""}
	                    {/if}

	                    {assign var="multipleu_selection_btn" value=""}
	                {/if}
					<tfoot>
						<tr>
							<th align="left" colspan="8">
								With selection :
								<div  class="btn-group">
									<button id="add_user" type="button" class="btn {$nou_selection_btn} btn-sm btn-default" {$disabled}><i class="glyphicon glyphicon-plus"></i> add user</button>
									<button id="del_user" type="button" class="btn {$multipleu_selection_btn} btn-sm btn-default" {$disabled}><i class="glyphicon glyphicon-minus"></i> delete user</button>
									<button id="update_user" type="button" class="btn singleu-selection-btn btn-sm btn-default"><i class="glyphicon glyphicon-refresh"></i> update user</button>
								</div>
							</th>
						</tr>
					</tfoot>
				</table>
			</div>
			{/if}

			{include file='comments.tpl' comments=$comments  tab_id=$comment_tab  add_new_comment=$projects[key($projects)].is_admin user_id=$user_id }

		</div>
	</div>

{* If multiple projects have to be displayed *}
{else}
	{* Find out if the current user is super user of a project *}
	{assign var="is_at_least_admin_of_1_project" value=false}
	{foreach from=$projects key=project_id item=project_values}
		{if $project_values.is_admin && !$is_at_least_admin_of_1_project}
			{assign var="is_at_least_admin_of_1_project" value=true}
		{elseif !$is_at_least_admin_of_1_project}
			{assign var="is_at_least_admin_of_1_project" value=false}
		{/if}
	{/foreach}
	{* Then display the table *}
	<div class="sub-content sc-top">
		<div class="ng6-content-header-left project">
			<h2>Projects list <small> you can access </small></h2>

		</div>
		<div class="ng6-content-header-right">
			{if $is_ng6_superadmin}
				You have access to <strong>{$projects|@count}</strong> projects.
			{else}
				You have access to <strong>{$visible_projects|@count}</strong> projects.
			{/if}

			<br />
			{* We don't need to display the following part anymore. *}
			{*
	    	{assign var="project_size" value="<span id='size' class='tx-nG6-mini-wait'></span>"}
			Raw data and analysis results use <strong>{$project_size}</strong> on the hard drive for all projects.
			*}
		</div>
		<div style="clear:both"></div>
	</div>
	<div id="projects" class="sub-content sc-bottom">
		{include file='project_table.tpl'}
	</div>
{/if}
{include file='../template/modals.tpl'}
