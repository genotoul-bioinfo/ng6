{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=results_title}Methylation calling results {/block}
{block name=results}
	{assign var="type" value="paired"}
	{if $analyse_results|@count > 0}
		{assign var="firstelt" value=array_shift(array_slice($analyse_results, 0, 1))}
		{if 'single'|array_key_exists:$firstelt["default"] }
			{$type="single"}
		{/if}
	{/if}
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th><center><input type="checkbox" id="chk_all_sample"></center></th>
				<th>Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
				<th class="numeric-sort" ># C analysed</th>
				<th class="numeric-sort" ># mC in CpG</th>
				<th class="numeric-sort" ># C in CpG</th>
				<th class="numeric-sort" >% methylation in CpG</th>
				<th class="numeric-sort" ># mC in CHG</th>
				<th class="numeric-sort" ># C in CHG</th>
				<th class="numeric-sort" >% methylation in CHG</th>
				<th class="numeric-sort" ># mC in CHH</th>
				<th class="numeric-sort" ># C in CHH</th>
				<th class="numeric-sort" >% methylation in CHH</th>
			</tr>
		</thead>
		<tbody>
		{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
		{assign var="totalC" value=0}
		{assign var="totalMethylatedCinCpG" value=0}
		{assign var="totalUnmethylatedCinCpG" value=0}
		{assign var="totalMethylatedCinCHG" value=0}
		{assign var="totalUnmethylatedCinCHG" value=0}
		{assign var="totalMethylatedCinCHH" value=0}
		{assign var="totalUnmethylatedCinCHH" value=0}
		{assign var="numberOfSampleWithoutMappedReads" value=0}
		{assign var="messageToExplainMessagesWithoutMappedReads" value=""}
		{assign var=i value=1}
		{foreach from=$analyse_results_sorted key=sample item=sample_results}
		
			{$totalC=$totalC+$sample_results["default"].totalC}
			{$totalMethylatedCinCpG=$totalMethylatedCinCpG+$sample_results["default"].totalMethylatedCinCpG}
			{$totalUnmethylatedCinCpG=$totalUnmethylatedCinCpG+$sample_results["default"].totalUnmethylatedCinCpG}
			{$totalMethylatedCinCHG=$totalMethylatedCinCHG+$sample_results["default"].totalMethylatedCinCHG}
			{$totalUnmethylatedCinCHG=$totalUnmethylatedCinCHG+$sample_results["default"].totalUnmethylatedCinCHG}
			{$totalMethylatedCinCHH=$totalMethylatedCinCHH+$sample_results["default"].totalMethylatedCinCHH}
			{$totalUnmethylatedCinCHH=$totalUnmethylatedCinCHH+$sample_results["default"].totalUnmethylatedCinCHH}
			
			{$PercentCinCPG=round(($sample_results["default"].totalMethylatedCinCpG/($sample_results["default"].totalMethylatedCinCpG + $sample_results["default"].totalUnmethylatedCinCpG ))*100,2)}
			{$PercentCinCHG=round(($sample_results["default"].totalMethylatedCinCHG/($sample_results["default"].totalMethylatedCinCHG + $sample_results["default"].totalUnmethylatedCinCHG ))*100,2)}
			{$PercentCinCHH=round(($sample_results["default"].totalMethylatedCinCHH/($sample_results["default"].totalMethylatedCinCHH + $sample_results["default"].totalUnmethylatedCinCHH ))*100,2)}

			{if $sample_results["default"].totalC > 0}
				<tr>
					<input type="hidden" id="PercentCinCPG_{$i}" value="{$PercentCinCPG}"/>
					<input type="hidden" id="PercentCinCHG_{$i}" value="{$PercentCinCHG}"/>
					<input type="hidden" id="PercentCinCHH_{$i}" value="{$PercentCinCHH}"/>
					<input type="hidden" id="nb_C_{$i}" value="{$sample_results["default"].totalC}"/>
					<!--<input type="hidden" id="descrip_{$i}" value="{$sample|get_description:$descriptions}"/>-->
					<td><center><input type="checkbox" id="chk_sample_{$i}" value="sample"/></center></td>
					<td id="sample_{$i}_col_1">{$sample|get_description:$descriptions}</td>
					<td id="sample_{$i}_col_2">{$sample_results["default"].totalC|number_format:0:' ':' '}</td>
					<td id="sample_{$i}_col_3">{$sample_results["default"].totalMethylatedCinCpG|number_format:0:' ':' '}</td>
					<td id="sample_{$i}_col_4">{$sample_results["default"].totalUnmethylatedCinCpG|number_format:0:' ':' '}</td>
					<td id="sample_{$i}_col_5">{round(($sample_results["default"].totalMethylatedCinCpG/($sample_results["default"].totalMethylatedCinCpG + $sample_results["default"].totalUnmethylatedCinCpG ))*100,2)}</td>
					<td id="sample_{$i}_col_6">{$sample_results["default"].totalMethylatedCinCHG|number_format:0:' ':' '}</td>
					<td id="sample_{$i}_col_7">{$sample_results["default"].totalUnmethylatedCinCHG|number_format:0:' ':' '}</td>
					<td id="sample_{$i}_col_8">{round(($sample_results["default"].totalMethylatedCinCHG/($sample_results["default"].totalMethylatedCinCHG + $sample_results["default"].totalUnmethylatedCinCHG ))*100,2)}</td>
					<td id="sample_{$i}_col_9">{$sample_results["default"].totalMethylatedCinCHH|number_format:0:' ':' '}</td>
					<td id="sample_{$i}_col_10">{$sample_results["default"].totalUnmethylatedCinCHH|number_format:0:' ':' '}</td>
					<td id="sample_{$i}_col_11">{round(($sample_results["default"].totalMethylatedCinCHH/($sample_results["default"].totalMethylatedCinCHH + $sample_results["default"].totalUnmethylatedCinCHH ))*100,2)}</td>
	   			</tr>
	   			{$i = $i + 1}
	   		{else}
	   			{$numberOfSampleWithoutMappedReads = $numberOfSampleWithoutMappedReads + 1}
	   			{$messageToExplainMessagesWithoutMappedReads = $messageToExplainMessagesWithoutMappedReads|cat:'<li>'|cat:$sample|cat:' has no read mapping the reference, no methylation calling can be done.</li>'}
	   		{/if}
	   	{/foreach}
	   	</tbody>
		{if $analyse_results|@count > 1 }
	   	<tfoot>
			<tr>
				<th></th>
				<th>Total</th>
				<th>{$totalC|number_format:0:' ':' '}</th>
				<th>{$totalMethylatedCinCpG|number_format:0:' ':' '}</th>
				<th>{$totalUnmethylatedCinCpG|number_format:0:' ':' '}</th>
				<th></th>
				<th>{$totalMethylatedCinCHG|number_format:0:' ':' '}</th>
				<th>{$totalUnmethylatedCinCHG|number_format:0:' ':' '}</th>
				<th></th>
				<th>{$totalMethylatedCinCHH|number_format:0:' ':' '}</th>
				<th>{$totalUnmethylatedCinCHH|number_format:0:' ':' '}</th>
				<th></th>
			</tr>
			<tr>
				<th></th>
				<th>Mean</th>
				<th>{($totalC/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{($totalMethylatedCinCpG/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{($totalUnmethylatedCinCpG/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th></th>
				<th>{($totalMethylatedCinCHG/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{($totalUnmethylatedCinCHG/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th></th>
				<th>{($totalMethylatedCinCHH/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{($totalUnmethylatedCinCHH/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th></th>
			</tr>

			<tr>
				<th align="left" colspan="12">
					With selection :
					<button type="button" class="btn btn-default multiple-selection-btn length-view-btn"><i class="glyphicon glyphicon-signal"></i> # C analysed</button>
					<button type="button" class="btn btn-default multiple-selection-btn length-view-btn4" id="percent_graph"><i class="glyphicon glyphicon-signal"></i> % C in all context</button>
				</th>
			</tr>
		</tfoot>
		{/if}
	</table>
	{if $numberOfSampleWithoutMappedReads >0 }
		<p class="information">Warning, {$numberOfSampleWithoutMappedReads} sample(s) not displayed in the previous table : </b><br /><ul>{$messageToExplainMessagesWithoutMappedReads}</ul>
	{/if}
{/block}
