/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */

$(function () {
	
    /* 
    * Define functions in charge to visualize venn charts
    */
	$(".contigs-bar-view-btn").click(function() {
		
		var max_xvalues = 30;
		
    	if ($(":checked[id^=chk_sample_]").size() > 0) {
			//Set dialog window
			$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
    		$("#modal-body-tmpl").html('<div id="highcharts_container"></div>');
    		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
    		$("#highcharts_container").css('width', '845px');
    		resize_center_btmodal('#ng6modal', 845);
    		
    		//Set graph
    		
            var min = 1000000,
            max = 0;
            $(":checked[id^=chk_sample_]").each(function(){
                    var index = $(this).attr("id").split("_")[2];
                    var val_array =	$("#contigs_length_"+index).val().split(",");
                    var tmax = Math.max.apply( Math, val_array),
                        tmin = Math.min.apply( Math, val_array);
                    if (min > tmin)	{ min = tmin; }
                    if (max < tmax)	{ max = tmax; }
           	});
            
            var x_table = new Array();
            var x_table_labels = new Array();
            var y_table = new Array();
            var step = Math.ceil((max-min)/max_xvalues);
            for (var i=min; i<=max; i += step) {
            	x_table.push(String(i));
            }
            for (var i=0; i<x_table.length-1; i++) {
            	x_table_labels.push(x_table[i] + "-" + String(parseInt(x_table[i+1]) -1));
            }            
            $(":checked[id^=chk_sample_]").each(function(){
            	var index = $(this).attr("id").split("_")[2];
            	var c_table = new Array();
                for (var i=min; i<=max; i += step) {
                	c_table.push(1);
                }
                var index_array = $("#contigs_length_"+index).val().split(","),
                	value_array = $("#contigs_length_count_"+index).val().split(",");
                for (var i=0; i<x_table.length-1; i++) {
                	
                	var c_val = 0;
                	for (var j=0; j<index_array.length; j++) {
                		if (parseInt(index_array[j]) >= x_table[i] && parseInt(index_array[j]) < x_table[i+1]) {
                			c_table[i] += parseInt(value_array[j]);
                		}
                	}
                }
                console.log(c_table)
                c_table = c_table.slice(0, -1);
            	y_table.push({
            		name: $("#sample_id_"+index).html(),
            		data: c_table
            	});
            });            
            
            console.log(y_table)
            
	        chart = new Highcharts.Chart({
	            chart: {
	               renderTo: 'highcharts_container',
	               defaultSeriesType: 'line'
	            },
	            title: {
	               text: "Distribution of sequence lengths.",
	               x: -20 //center
	            },
	            xAxis: {
	               categories: x_table_labels,
			       title: {
			    	   text: "Sequence length (bp)"
				   },
				   labels: {
					   rotation: -45,
					   align: 'right'
				   }
	            },
	            yAxis: {
	               title: {
	                  text: "Number of sequences"
	               },
	               min: 1,
	               labels: {
	            	   formatter: function() {
	            		   if (this.value ==1) {
	            			   return this.value-1;
	            		   } else {
	            			   return this.value;
	            		   }
	            	   }
	               },
	               type: 'logarithmic',
	               minorTickInterval: 0.1,
	               plotLines: [{
	                  value: 0,
	                  width: 1,
	                  color: '#808080'
	               }]
	            },
	            tooltip: {
	               formatter: function() {
	            	   	var real_nb_seq = parseInt(this.y) -1;
	            	   	return '<b>'+ this.series.name +'</b><br/>'+
	            	   		this.x +'bp : '+ real_nb_seq +'sequences';
	               }
	            },
	            credits: { enabled: false },
	            legend: {
	               layout: 'vertical',
	               align: 'right',
	               verticalAlign: 'top',
	               x: -10,
	               y: 100,
	               borderWidth: 0
	            },
	            series: y_table
	         });
			
			//Display
			$("#ng6modal").modal();
    	}
	});
});
