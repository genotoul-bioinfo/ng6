{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=params_title} Filtering options {/block}
{block name=params_content}
	{assign var="params" value=";"|explode:$analyse.params}
	<p>In the current analysis only the following filters was used : </p>
	<ul>
		{foreach from=$params item=current_param}
		{assign var="current_param_fields" value=":"|explode:$current_param}
		{if $current_param_fields[0] == "min_length"}
		<li class="parameter">Keep reads with a length >= {$current_param_fields[1]}.</li>
		{elseif $current_param_fields[0] == "max_length"}
		<li class="parameter">Keep reads with a length <= {$current_param_fields[1]}.</li>
		{elseif $current_param_fields[0] == "max_N"}
		<li class="parameter">Keep reads with a number of N <= {$current_param_fields[1]}.</li>
		{elseif $current_param_fields[0] == "max_homopoly"}
		<li class="parameter">Keep reads with an great homopolymer length <= {$current_param_fields[1]}.</li>
		{/if}
		{/foreach}
	</ul>
{/block}

{block name=results_title} Filtering results {/block}
{block name=results}
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th class="string-sort">Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
				<th class="numeric-sort">Before filtering</th>
				<th class="numeric-sort">After filtering</th>
				<th class="numeric-sort">Deleted (%)</th>
				<th class="numeric-sort">Filtered on length</th>
				<th class="numeric-sort">Filtered on N</th>
				<th class="numeric-sort">Filtered on homopolymer</th>
			</tr>
		</thead>
		{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
		{assign var="total_processed" value=0}
		{assign var="total_deleted" value=0}
		<tbody>
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
		   	<tr>
				<td>{$sample|get_description:$descriptions}</td>
				{$total_processed = $sample_results["default"].nb_seq_processed + $total_processed}
				{assign var="nb_deleted_reads" value=($sample_results["default"].nb_filtered_length + $sample_results["default"].nb_filtered_N + $sample_results["default"].nb_filtered_homopolymer)}
				{assign var="nb_kept_reads" value=($sample_results["default"].nb_seq_processed - $nb_deleted_reads)}
				{$total_deleted = $nb_deleted_reads + $total_deleted}
				<td>{$sample_results["default"].nb_seq_processed|number_format:0:' ':' '}</td>
				<td>{$nb_kept_reads|number_format:0:' ':' '}</td>
				<td>{(($nb_deleted_reads/$sample_results["default"].nb_seq_processed)*100)|number_format:2:'.':' '}</td>
				<td>{$sample_results["default"].nb_filtered_length|number_format:0:' ':' '}</td>
				<td>{$sample_results["default"].nb_filtered_N|number_format:0:' ':' '}</td>
				<td>{$sample_results["default"].nb_filtered_homopolymer|number_format:0:' ':' '}</td>
		   	</tr>
		   	{/foreach}
	   	</tbody>
		{if $analyse_results|@count > 1 }
	   	<tfoot>
			<tr>
				<th>Total</th>
				<th>{$total_processed|number_format:0:' ':' '}</th>
				<th>{($total_processed-$total_deleted)|number_format:0:' ':' '}</th>
				<th>{(($total_deleted/$total_processed)*100)|number_format:2:'.':' '}</th>
				<th colspan=3></th>
			</tr>
			<tr>
				<th>Mean</th>
				<th>{($total_processed/$analyse_results|@count)|number_format:0:' ':' '}</th>
				<th>{(($total_processed-$total_deleted)/$analyse_results|@count)|number_format:0:' ':' '}</th>
				<th>-</th>
				<th colspan=3></th>
			</tr>
		</tfoot>
		{/if}
	</table>
{/block}

{block name=download}{/block}
