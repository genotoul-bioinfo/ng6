{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=params_title} Mothur commands and parameters used {/block}
{block name=params_content}
	{assign var="args_cmd" value=","|explode:$analyse.params}
	<ul>
		{foreach from=$args_cmd item=arg_cmd}
			{assign var="params_cmd" value="="|explode:$arg_cmd}
			{if in_array("qaverage", $params_cmd)}
			{assign var="qaverage_index" value=$params_cmd|@array_keys:"qaverage"}
			<li class="parameter">Remove sequences with a quality average below {$params_cmd[$qaverage_index[0]+1]}.</li>
			{/if}
			{if in_array("maxambig", $params_cmd)}
			{assign var="maxambig_index" value=$params_cmd|@array_keys:"maxambig"}
			<li class="parameter">Discard sequences with more than {$params_cmd[$maxambig_index[0]+1]} ambiguous bases.</li>
			{/if}
			{if in_array("maxhomop", $params_cmd)}
			{assign var="maxhomop_index" value=$params_cmd|@array_keys:"maxhomop"}
			<li class="parameter">Clean reads with homopolymers with a size higher than {$params_cmd[$maxhomop_index[0]+1]}.</li>
			{/if}
			{if in_array("minlength" , $params_cmd)}
			{assign var="minlength_index" value=$params_cmd|@array_keys:"minlength"}
			<li class="parameter">Clean reads with a length smaller than {$params_cmd[$minlength_index[0]+1]}</li>
			{/if}
			{if in_array("maxlength" , $params_cmd)}
			{assign var="maxlength_index" value=$params_cmd|@array_keys:"maxlength"}
			<li class="parameter">Clean reads with a length higher than {$params_cmd[$maxlength_index[0]+1]}</li>
			{/if}
			{if in_array("pdiffs", $params_cmd)}
			{assign var="pdiffs_index" value=$params_cmd|@array_keys:"pdiffs"}
			<li class="parameter">The maximum number of differences allowed with the primer is {$params_cmd[$pdiffs_index[0]+1]|replace:')':''}.</li>
			{/if}
		{/foreach}
		<br />
		<li class="parameter">{$analyse.params}</li>
	</ul>
{/block}

{block name=results_title} Cleaning results {/block}
{block name=results}

	<input type="hidden" id="data_folder" value="{$data_folder}"/>
	<input type="hidden" id="analysis_folder" value="{$data_folder|cat:$analyse.directory}"/>

	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th class="string-sort">Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
				<th class="numeric-sort">Number of sequences</th>
				<th class="numeric-sort">Primers</th>
				<th class="numeric-sort">Too short / too long </th>
				<th class="numeric-sort">Homopolymers</th>
				<th class="numeric-sort">Number of sequences after cleaning</th>
				<th class="numeric-sort">% deleted</th>
			</tr>
		</thead>
		<tbody>
			{assign var="nb_reads_begining" value=0}
			{assign var="primers_failed" value=0}
			{assign var="length_failed" value=0}
			{assign var="homopolymers_failed" value=0}
			{assign var="total_failed" value=0}
			{assign var="nb_reads_end" value=0}
			{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
			{assign var="i" value=0}
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
			   	<tr>
				<td id="sample_id_{$i}">{$sample|get_description:$descriptions}</td>
				{$nb_reads_begining=$nb_reads_begining+$sample_results["default"].nb_seq_after_demultiplexage}
				{$primers_failed=$primers_failed+$sample_results["default"].f+$sample_results["default"].fl}
				{$length_failed=$length_failed+$sample_results["default"].l}
				{$homopolymers_failed=$homopolymers_failed+$sample_results["default"].h}
				{$total_failed=$primers_failed+$length_failed+$homopolymers_failed}
				{$nb_reads_end=$nb_reads_begining-$total_failed}
				<td>{$sample_results["default"].nb_seq_after_demultiplexage|number_format:0:' ':' '}</td>
				<td>{$sample_results["default"].f+$sample_results["default"].fl|number_format:0:' ':' '}</td>
				<td>{$sample_results["default"].l|number_format:0:' ':' '}</td>
				<td>{$sample_results["default"].h|number_format:0:' ':' '}</td>
				<td>{$sample_results["default"].nb_seq_after_demultiplexage-($sample_results["default"].f+$sample_results["default"].fl+$sample_results["default"].l+$sample_results["default"].h)|number_format:0:' ':' '}</td>
				<td>{(100.00-((($sample_results["default"].nb_seq_after_demultiplexage-($sample_results["default"].f+$sample_results["default"].fl+$sample_results["default"].l+$sample_results["default"].h))*100)/$sample_results["default"].nb_seq_after_demultiplexage))|number_format:2:'.':' '}</td>
		   	</tr>
		   	{$i = $i +1}
		   	{/foreach}
		</tbody>
		<tfoot>
			{if $analyse_results|@count > 1 }
			<tr>
				<th>Total</th>
				<th>{$nb_reads_begining|number_format:0:' ':' '}</th>
				<th>{$primers_failed|number_format:0:' ':' '}</th>
				<th>{$length_failed|number_format:0:' ':' '}</th>
				<th>{$homopolymers_failed|number_format:0:' ':' '}</th>
				<th>{$nb_reads_end|number_format:0:' ':' '}</th>
				<th>{(100.00-((($nb_reads_end)*100)/$nb_reads_begining))|number_format:2:'.':' '}</th>			
			</tr>
			<tr>
				<th>Mean</td>
				<th>{($nb_reads_begining/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{($primers_failed/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{($length_failed/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{($homopolymers_failed/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{($nb_reads_end/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{(100.00-((($nb_reads_end)*100)/$nb_reads_begining))|number_format:2:'.':' '}</th>
			</tr>

			{/if}
		</tfoot>
	</table>
{/block}
