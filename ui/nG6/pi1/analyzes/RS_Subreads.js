/***************************************************************
*  Copyright notice
*
*  (c) 2015 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

$(function () {

	var updateTableStatus = function($elmt){
		var table = $("#rs_subreads_datatable").DataTable();
		var column = table.column($elmt.attr('data-column')) ;
		column.visible($elmt[0].checked );
	};
	
	$('#rs_subreads_datatable').on( 'draw.dt', function () {
		$(':checkbox.toogle-vis').each(function(e){
			updateTableStatus($(this));
		});
	});
	
	$(':checkbox.toogle-vis').change(function(e) {
		updateTableStatus($(this));
	});
	
	
	
	/*Check all metrics*/
	$('#check_all_metrics').click(function(){
		var state = this.checked;
		$("[id^='chk_col_']").each(function(){
			this.checked = state;
		});
	
	});
	
	/* Opens a graph will all the element in the selected column */
	$("#create_graph").click(function(){
		
		var samples = new Array() ;
		$("input[id^='chk_sample_']:checked").each( function() {
    		samples.push( parseInt($(this).attr("id").split('_')[2]) ) ;
    	}) ;
    	var columns = new Array() ;
    	$("input[id^='chk_col_']:checked").each( function() {
    		columns.push( parseInt($(this).attr("id").split('_')[2]) ) ;
    	}) ;
		
		if( samples.length != 0 ) {
			if( $('#sample_1_col_1') !== 'undefined' ) {
				columns.push( 1 ) ;
			}
			columns.sort(function(a,b){return a-b}) ;
			samples.sort(function(a,b){return a-b}) ;
		
		
			// which columns are checked
			var column_id = new Array();
			if (columns.length == 2) {
				column_id.push($('#th_id_' + columns[1]).html());
			} else if (columns.length == 3) {
				column_id.push($('#th_id_' + columns[1]).html());
				column_id.push($('#th_id_' + columns[2]).html());
			}
		
			
		
			// Set dialog window
			$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
    		$("#modal-body-tmpl").html('<div id="highcharts_container"></div>');
    		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
    		$("#highcharts_container").css({'width' :  '1045px','height' : '545px'});

    		// Set graph
			var description = new Array();
			var valeur1 = new Array();
			var valeur2 = new Array();
            $(":checked[id^=chk_sample_]").each(function(){
            	var index = $(this).attr("id").split("_")[2] ;
            	
                var index_description = $("#sample_" + index+"_col_1").html() ;
                description.push(index_description);
                
                var value = $("#sample_" + index+"_col_"+columns[1]+"").html();
                valeur1.push(parseInt(value.replace(/ /g,"")));
                var value = $("#sample_" + index+"_col_"+columns[2]+"").html();
                if (value){
					valeur2.push(parseInt(value.replace(/ /g,"")));
				}
            });
	       	
	       	if (columns.length == 3) {
            var options = {
				chart: {
					renderTo: 'highcharts_container', 
					type: 'column',
					margin: [80, 80, 150, 100]
				},
				title: { 
					text: 'Barcode number values :'
				},
				xAxis: [{
					categories: description,
					labels: {
						rotation: -45,
						align: 'right',
						x: 5,
						y: 5,
						style: { fontSize: '13px', fontFamily: 'Verdana, sans-serif' }
					}
				}],
				yAxis: [
					{ min: 0, title: { text: '# '+column_id[0] } },
					{ min: 0, title: { text: '# '+column_id[1] }, opposite: true}
				],
				legend: { enabled: false },
				tooltip: { shared: true },
				series: [
					{
						name: column_id[0],
						data: valeur1,
						dataLabels: {
							enabled: false,
						}
					},
					{
						name: column_id[1],
						data: valeur2,
						yAxis: 1,
						dataLabels: {
							enabled: false,
						}
					}],
			};
			} else {
				var options = {
				chart: {
					renderTo: 'highcharts_container', 
					type: 'column',
					margin: [60, 60, 150, 80]
				},
				title: { text: 'Barcode number values :' },
				xAxis: [{
					categories: description,
					labels: {
						rotation: -45,
						align: 'right',
						x: 5,
						y: 5,
						style: { fontSize: '13px', fontFamily: 'Verdana, sans-serif' }
					}
				}],
				yAxis: [
					{ min: 0, title: { text: '# '+column_id[0] } }
				],
				legend: { enabled: false },
				tooltip: { shared: true },
				series: [
					{
						name: column_id[0],
						data: valeur1,
						dataLabels: {
							enabled: false,
						}
					}]
			};
			}

			
			
			
            // Draw graph
			var chart = new Highcharts.Chart(options);
			resize_center_btmodal('#ng6modal', chart.chartWidth + 50);
			
			//Display
			$("#ng6modal").modal();
    	}
	
	 }) ;
    
    $(':checkbox.toogle-vis').each(function(e){
		updateTableStatus($(this));
	});
	
});


