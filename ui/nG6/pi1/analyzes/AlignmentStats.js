/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */


$(function () {
	//Executed if the corresponding element of DOM is clicked
    $('#read1-view-btn').click(function(){ process_highcharts("read1") });
    $('#read2-view-btn').click(function(){ process_highcharts("read2") });
    $('#merge-view-btn').click(function(){ process_highcharts("all") });
    $('#cigarlinegraph-view-btn').click(function(){ process_img() });
});  

//Displaying cigarlinegraph image
function process_img() {
   	var index = $(":checked[id^=chk_sample_]").attr("id").split("_")[2];
	var img_link = $('#cigarlinegraph_img_' + index).val() ;

    // Autoload image size
    var imgLoad = $("<img />")
	.attr("src", img_link)
	.attr('alt', 'Cannot display')
	.unbind("load")
	.bind("load", function () {
		var imgwidth = ( this.width > 900 ) ? 900 : this.width ;
		resize_center_btmodal('#ng6modal', imgwidth + 50);
		$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
		$("#modal-body-tmpl").html('<div id="img_container" style="overflow-x : auto"><img src="' + img_link + '" alt="Cannot display"></div>');
		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
		$("#ng6modal").modal();
	});
}

//Displaying Cigargraph with corresponding dataset
function process_highcharts(read) {
	$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
	$("#modal-body-tmpl").html('<div id="highcharts_container"></div>');
	$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
	$("#highcharts_container").css('width', '845px');
	
   	var index = $(":checked[id^=chk_sample_]").attr("id").split("_")[2];
   	
   	// Highcharts graph initialisation //
   	var options = {
	          chart: { renderTo: 'highcharts_container', 
					   zoomType: 'x', 
					   type: 'line'
					 },
	          title: { text: 'Reads Alignment Percent By Position' },
		      xAxis: { tickPosition: 'inside' , gridLineWidth: 2, gridLineDashStyle: 'shortdot' },
	          yAxis: { title: {text: 'Read %' }, gridLineWidth: 2, gridLineDashStyle: 'shortdot' },
	          series: [],
	          tooltip: { shared: true, crosshairs: true },
	          credits: { enabled: false }
	    };
	    
	var statuts = ["match","mismatch","clipping","insertion"];
	var calcul = [];
	
	//MATCH
	var match_values = $("#match_"+read+"_"+index).val();
	match_values = match_values.split(";");
	for (var j in match_values) {
		calcul[j]= parseInt(match_values[j]);
	}
	
	//MISMATCH
	var mismatch_values = $("#mismatch_"+read+"_"+index).val();
	mismatch_values = mismatch_values.split(";");
	for (var j in mismatch_values) {
		calcul[j] += parseInt(mismatch_values[j]);
	}
	
	//CLIPPING
	var clipping_values = $("#clipping_"+read+"_"+index).val();
	clipping_values = clipping_values.split(";");
	for (var j in clipping_values) {
		calcul[j] += parseInt(clipping_values[j]);
	}
	
	//INSERTION
	var insertion_values = $("#insertion_"+read+"_"+index).val();
	insertion_values = insertion_values.split(";");
	for (var j in insertion_values) {
		calcul[j] += parseInt(insertion_values[j]);
	}
	 
	for (var statut in statuts) {
		// Getting input values for the selected sample //
		var cigar_values = $(":input[id='"+statuts[statut]+"_"+read+"_"+index+"']").val();
  		var series = {
  					name : '', 
  					data: [] 
  					};
  	  		
  		cigar_values = cigar_values.split(";");
  		var result = [];
  	    for (var j in cigar_values) {
  	    	cigar_values[j] = (parseInt(cigar_values[j])*100)/parseInt(calcul[j]);
  	    	result[j] = Math.round(cigar_values[j]*100)/100;
  	    	//cigar_values[j] = parseInt(cigar_values[j]);
  	    }
  		series.data=result;
  		series.name=statuts[statut];
  		series.pointStart = 1;
  		 
  		if (!series.name) {
  			console.log("Warning : Empty serie names !");
  		}
  		if (series.data==[]) {
  			console.log("Warning : Empty data in the series !");
  		}
  		
  		// Adding data to highcharts graph //
  		options.series.push(series);
  	}
	// Create the chart //
	var chart = new Highcharts.Chart(options);
	resize_center_btmodal('#ng6modal', chart.chartWidth + 50);
	$("#ng6modal").modal();
};