{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=results_title} Contamination Results {/block}

{block name=results}
{assign var="samples" value=($analyse_results|@array_keys)}

{assign var="database" value=array()}
{foreach from=$analyse_results key=sample item=sample_results}
	{foreach from=$sample_results key=group item=value}
		{if !in_array($group, $database) }
			{$database[]=$group}
		{/if}
	{/foreach}
{/foreach}

{$null = sort($database)}
<table class="table table-striped table-bordered dataTable analysis-result-table">
	<thead>
		<tr>
			<th class = "string-sort">Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
			{foreach $database as $group}
			{if $group != "total"}
			<th class = "numeric-sort"> {$group} </th>
			{/if}
			{/foreach}
		</tr>
	</thead>
	<tbody>
		{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
		{foreach from=$analyse_results_sorted key=sample item=sample_results}
		<tr>
			<td>{$sample|get_description:$descriptions}</td>
			{foreach $database as $group}
			{if $group != "total"}
			<td> {$sample_results[$group].nb_conta|number_format:0:' ':' '} </td>
			{/if}
			{/foreach}
		</tr>
		{/foreach}
	</tbody>
</table>
{/block}
