/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */

$(function () {

    /* 
    * Define functions in charge to visualize venn diagramms
    */
	$(".venn-view-btn").click(function() {
    	if ($(":checked[id^=chk_sample_]").size() <= 5) {
    		// Define the venny update function
    		function updateVenn() {
    			var series_table = new Array();
    			$(":checked[id^=chk_sample_]").each(function(){
    				var index = $(this).attr("id").split("_")[2];
    				var data_values = new Array();
    				var data_table_values = new Array();
    				var data_table = $("#otus_"+index+"_"+$("#dist_select").val()).val().split(";");
    				for (var d in data_table) {
    	       			var parts = data_table[d].split("=");
	       				data_values.push(parts[0]);
	       				data_table_values.push(parts[1]);
    	       		}
	       			series_table.push({
		            	name: $("#sample_id_"+index).html(),
		            	data: data_values,
		            	values: data_table_values
		            });
    			});
    			if ($("#seq_otu_select").val() == "otu") {
	    	       	$("#venn_container").venny({
	    	            series: series_table,
	    	            disableClick: true
	    	        });
    			} else if ($("#seq_otu_select").val() == "seq") {
	    	       	$("#venn_container").venny({
	    	            series: series_table,
	    	            disableClick: true,
	    	            useValues: true
	    	        });
    			}
    		}
			$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
	       	var labels = new Array();
	       	$(":checked[id^=chk_sample_]").each(function(){
	       		var index = $(this).attr("id").split("_")[2];
	       		$("*[id^=otus_"+index+"_]").each(function(){
	       			var label = $(this).attr("id").split("_").slice(2).join("_").replace("_", ".");
	       			if (labels.indexOf(label) === -1) {
	       				labels.push(label);
	       			}
	       		});
    		});
	        var seq_otu_select = "<select class='input-medium' id='seq_otu_select'><option value='otu'>OTUs</option><option value='seq'>sequences</option></select>";
	        var dist_select = "<select class='input-medium' id='dist_select'>";
	        for (var l in labels) {
	        	dist_select += "<option value='"+labels[l].replace(".","_")+"'>"+labels[l]+"</option>";
	        }
	        dist_select += "</select>";
	       	var nav_html = 'Display ' + seq_otu_select + ' repartition for ' + dist_select + ' as distance.';
	       	$("#modal-body-tmpl").html('<blockquote>'+nav_html+'<small>Change values to update the venn diagram</small></blockquote><br/><div align="center" id="venn_container"></div>');
            $("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
    		resize_center_btmodal('#ng6modal', 750);
            $("#ng6modal").modal();
	       	updateVenn();
	       	$("#dist_select").change(function(){ updateVenn(); });
	       	$("#seq_otu_select").change(function(){ updateVenn(); });
    	}
	});
	
    /* 
     * Define functions in charge to visualize phylogenic bar charts
     */
    $(".rarefaction-line-view-btn").click(function() {
    	var max_xvalues = 30;
    	if ($(":checked[id^=chk_sample_]").size() == 1) {

    		$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
	       	$("#modal-body-tmpl").html('<div id="highcharts_container"></div>');
            $("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
    		resize_center_btmodal('#ng6modal', 750);
            $("#ng6modal").modal();
	       	
	       	var index = $(":checked[id^=chk_sample_]").attr("id").split("_")[2];
	       	var groups = new Array();
	       	$("*[id^=rarefaction_curve_x_]").each(function(){
	       		if ($(this).attr("id").split("_").length == 5) {
	       			var group = $(this).attr("id").split("_")[4];
	       		} else {
	       			var group = $(this).attr("id").split("_").slice(4,$(this).attr("id").split("_").length).join("_");
	       		}
	       		if (groups.indexOf(group) == -1) {
	       			groups.push(group); 
	       		}
	       	});
	       	var categories_table = new Array();
	       	var series_table = new Array();
	       	for (group in groups) {
	       		var data_table = new Array();
	       		var vals = $("#rarefaction_curve_y_"+index+"_"+groups[group]).val().split(",");
	       		var catvals = $("#rarefaction_curve_x_"+index+"_"+groups[group]).val().split(",");
	       		var graph_vals = new Array();
	       		if (vals.length > max_xvalues) {
	       			var step = Math.ceil(vals.length/max_xvalues);
	       			var istep = 0;
	       			for (value in vals){
	       				if (istep == step-1) {
	       					istep = 0;
	       					graph_vals.push(vals[value]);
	       					categories_table.push(catvals[value])
	       				}
	       				istep ++;
	       			}
	       		} else {
	       			graph_vals = vals;
	       			categories_table = $("#rarefaction_curve_x_"+index+"_"+groups[group]).val().split(",");
	       		}
	       		for (value in graph_vals){
	       			data_table.push(parseFloat(graph_vals[value]));
	       		}
	       		series_table.push({name:groups[group].replace("_","."),data:data_table})
	       	}
	       	
		   chart = new Highcharts.Chart({
		      chart: {
		         renderTo: 'highcharts_container',
		         defaultSeriesType: 'line'
		      },
		      title: {
		         text: 'Rarefaction curves for sample ' + $("#sample_id_"+index).html() + '.',
		         x: -20 //center
		      },
		      xAxis: {
		         categories: categories_table,
				 labels: {
					rotation: -45,
					align: 'right'
				 },
		         title: {
		            text: 'Number of Tags Sampled'
		         }
		      },
		      yAxis: {
		         title: {
		            text: 'OTUs'
		         },
		         min: 0,
		         plotLines: [{
		            value: 0,
		            width: 1,
		            color: '#808080'
		         }]
		      },
		      tooltip: {
		         formatter: function() {
		               return '<b>'+ this.series.name +'</b><br/>'+
		               this.x + ' require to sample ' + this.y + ' OTUs.';
		         }
		      },
 			  credits: { enabled: false },
		      legend: {
		         layout: 'vertical',
		         align: 'right',
		         verticalAlign: 'top',
		         x: -10,
		         y: 100,
		         borderWidth: 0
		      },
		      series: series_table
		   });
		   chart.setSize(720, 450);
    	}
	});

    // If there is some tree to display
    if ($("[id^=tre_]").size() > 0) {
        var dataObject = { newick: $("#tre_thetayc_unique").val() };        
        phylocanvas = new Smits.PhyloCanvas(
        	dataObject,     // Newick or XML string
        	'svgCanvas',    // Div Id where to render
        	800, 800,      // Height, Width in pixels
        	'circular'
        );
    }

});
