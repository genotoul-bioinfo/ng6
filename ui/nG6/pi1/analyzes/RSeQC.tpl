{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=params}
	{assign var="params" value=" "|explode:$analyse.params}
	<ul>
    	<li> 
	    {foreach from=$params key=k item=v}
	        {$v}  
	    {/foreach}
	    </li>
	</ul>
{/block}

{block name=results_title} Reads and quality statistics {/block}
{block name=results}

<table class="table table-striped table-bordered dataTable analysis-result-table">
	<thead>
		<tr>
			<th><center><input type="checkbox" id="chk_all_sample"></center></th>
			<th class = "string-sort">Samples ({$analyse_results|@count})</th>
			<th class = "numeric-sort">Type</th>
			<th class = "numeric-sort">GBC total reads</th>
			<th class = "numeric-sort">Splicing reads found</th>
			<th class = "numeric-sort">Novel junctions</th>
			<th class = "numeric-sort">Known junctions</th>
		</tr>
	</thead>
	<tbody>
		{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
		{assign var=options value=" "|explode:$analyse.params}
		{assign var="i" value=0}
		{foreach from=$analyse_results_sorted key=sample item=sample_results}
			<tr>
				<td>
				    <center>
					    <input type="checkbox" id="chk_sample_{$i}" class="rseqc-data-check" >
					    <input type="hidden" id="reads_orientation_{$i}" value="{$sample_results['inferexp'].reads_orientation}">
					    <input type="hidden" id="inferexp_read_type_{$i}" value="{$sample_results['inferexp'].reads_type}">
					    <input type="hidden" id="inferexp_read_orientation_{$i}" value="{$sample_results['inferexp'].reads_orientation}">
					    <input type="hidden" id="innerdist_frequences_{$i}" value="{$sample_results['innerdist'].frequences}">
					    <input type="hidden" id="innerdist_insert_sizes_{$i}" value="{$sample_results['innerdist'].insert_sizes}">
					    <input type="hidden" id="juncannot_splicing_junctions_{$i}" value="{$sample_results['juncannot'].splicing_junctions}">
					    <input type="hidden" id="juncannot_splicing_events_{$i}" value="{$sample_results['juncannot'].splicing_events}">
					    <input type="hidden" id="juncsat_all_junctions_{$i}" value="{$sample_results['juncsat'].all_junctions}">
					    <input type="hidden" id="juncsat_known_junctions_{$i}" value="{$sample_results['juncsat'].known_junctions}">
					    <input type="hidden" id="juncsat_novel_junctions_{$i}" value="{$sample_results['juncsat'].novel_junctions}">
					    <input type="hidden" id="gbc_count_{$i}" value="{$sample_results['gbc'].count}">
					    {foreach from=$sample_results['rpkm_saturation'] key=quartile item=data}
						    <input type="hidden" id="quartile_{$quartile}_{$i}" value='{$data}'/>
					    {/foreach}
				    </center>
				</td>
				
				<td id="sample_id_{$i}">{$sample|get_description:$descriptions}</td>
				<input type="hidden" id="sample_{$i}" value="{$sample|get_description:$descriptions}">
				
				<td>{$sample_results['inferexp'].reads_type}</td>
				
				{assign var="novel_j" value=","|explode:$sample_results["juncsat"].novel_junctions}
				{assign var="known_j" value=","|explode:$sample_results["juncsat"].known_junctions}
				<td>{$sample_results["gbc"].total_reads|number_format:0:' ':' '}</td>
                <td>{$sample_results["juncsat"].found_junc|number_format:0:' ':' '}</td>
				<td>{$novel_j[19]|number_format:0:' ':' '}</td>
				<td>{$known_j[19]|number_format:0:' ':' '}</td>
			</tr>
			{$i = $i +1}
		{/foreach}
	</tbody>
	<tfoot>
		<tr>
			<th align="left" colspan="7">
				With selection :
				<button type='button' id="infer-view-btn" class='btn  btn-default  single-selection-btn' ><i class="glyphicon glyphicon-signal"></i> Read orientations</button>
				<button type='button' id="inn_dist-view-btn" class='btn  btn-default multiple-selection-btn' ><i class="glyphicon glyphicon-signal"></i> Inner distance</button>
				<button type='button' id="junc_ann-view-btn" class='btn  btn-default  single-selection-btn' ><i class="glyphicon glyphicon-signal"></i> Junction Annotation</button>
				<button type='button' id="junc_sat-view-btn" class='btn  btn-default  single-selection-btn' ><i class="glyphicon glyphicon-signal"></i> Junction Saturation</button>
				<button type='button' id="gbc-view-btn" class='btn  btn-default multiple-selection-btn' ><i class="glyphicon glyphicon-signal"></i> Gene Coverage</button>
				<button type='button' id="rpkm_sat-view-btn" class='btn  btn-default single-selection-btn' ><i class="glyphicon glyphicon-signal"></i> RPKM Saturation</button>
			</th>
		</tr>
	</tfoot>
</table>

{/block}
