{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=params_title} Mothur commands and parameters used {/block}
{block name=params_content}
	{assign var="params" value=";"|explode:$analyse.params}
	{foreach from=$analyse_results key=sample item=sample_results}
	{assign var="sample" value=$sample}
	{/foreach}
	<ul>
		{foreach from=$params item=param}
			<li class="parameter">{$param}</li>
			{assign var="args_cmd" value=","|explode:$param}
			{foreach from=$args_cmd item=arg_cmd}
				{assign var="params_cmd" value="="|explode:$arg_cmd}
				{if in_array("bdiffs", $params_cmd)}
				{assign var="bdiffs" value=$params_cmd[$bdiffs_index[0]+1]|replace:')':''}
				{/if}
			{/foreach}
		{/foreach}
		<br />
		<li class="parameter">The maximum number of differences allowed with the barcode is {$bdiffs}.
			With <strong> barcodes.oligos </strong> containing: 
			<br /> 
			<div class="file-display">{$analyse_results["barcodes.oligos"]["default"]["barcodes_file"]}</div> 
		</li>
	</ul>

{/block}

{block name=results_title} Cleaning results {/block}
{block name=results}

	<input type="hidden" id="data_folder" value="{$data_folder}"/>
	<input type="hidden" id="analysis_folder" value="{$data_folder|cat:$analyse.directory}"/>
	
	<ul>
		<li class="parameter">There are {$analyse_results["global"]["default"].ids_failed_trim_seqs_b} sequences deleted because of a problem with the barcode.</li>
		<li class="parameter">There are {$analyse_results["global"]["default"].ids_failed_trim_seqs_lb} sequences deleted because of a wrong length and a problem with the barcode.</li>
		<li class="parameter">There are {$analyse_results["global"]["default"].ids_failed_trim_seqs_l} sequences deleted because of a wrong length.</li>
	</ul>
	<br />	
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
			<tr>
				{if $analyse_results|@count > 1}
					{assign var="nb_sample" value=$analyse_results|@count}
					<th class="string-sort">Samples ({$nb_sample-2})</th>
				{/if}
				<th class="numeric-sort">Number of sequences</th>
			</tr>
		</thead>
		<tbody>
			{assign var="nb_reads_begining" value=0}
			{assign var="i" value=0}
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
				{if $sample != "global" and $sample != "barcodes.oligos"}
				   	<tr>
					{if $analyse_results|@count > 1 }
						<td id="sample_id_{$i}">{$sample|get_description:$descriptions}</td>
					{/if}
					{$nb_reads_begining=$nb_reads_begining+$sample_results["default"].nb_seq}
					<td>{$sample_results["default"].nb_seq|number_format:0:' ':' '}</td>
			   		</tr>
		   			{$i = $i +1}
		   		{/if}
		   	{/foreach}
		</tbody>
		<tfoot>
			{if $analyse_results|@count > 1 }
			<tr>
				<th>Total</th>
				<th>{$nb_reads_begining|number_format:0:' ':' '}</th>
			</tr>
			<tr>
				<th>Mean</td>
				<th>{($nb_reads_begining/($analyse_results|@count))|number_format:0:' ':' '}</th>
			</tr>
			{/if}
		</tfoot>
	</table>
{/block}