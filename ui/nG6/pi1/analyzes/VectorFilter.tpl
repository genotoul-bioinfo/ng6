{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=results_title} Analysis summary {/block}
{block name=results}
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th class="string-sort">Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
				<th class="numeric-sort">Number of contigs</th>
				<th class="string-sort">Contig names</th>
				<th class="numeric-sort">Extremity contigs</th>
				<th class="numeric-sort">Full size</th>
			</tr>
		</thead>
		<tbody>
			{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
		   	<tr>
				<td>{$sample|get_description:$descriptions}</td>
				<td>{$sample_results["default"].nb_contigs|number_format:0:' ':' '}</td>
				<td>{$sample_results["default"].contig_names}</td>
				<td>{$sample_results["default"].extremity_contigs}</td>
				<td>{$sample_results["default"].full_size|number_format:0:' ':' '}</td>
		   	</tr>
		   	{/foreach}
		</tbody>
	</table>
{/block}
