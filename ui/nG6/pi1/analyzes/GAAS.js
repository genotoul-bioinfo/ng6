/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */

$(function () {

	$(".krona-view-btn").click(function() {
		if ($(":checked[id^=chk_sample_]").size() == 1) {
			$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
            var webPAth = $("#analysis_directory").val() + $(":checked[id^=chk_sample_]").attr("id").split("_").slice(2).join("_") + ".krona.html";            
            $("#modal-body-tmpl").html('<iframe width="820px" frameborder="0" height="550px" src="' + webPAth + '"></iframe>');
            $("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
    		resize_center_btmodal('#ng6modal', 850);
            $("#ng6modal").modal();
    	}
	});

});
