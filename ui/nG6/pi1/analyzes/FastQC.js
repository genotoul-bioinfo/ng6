/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */
	
/* 
 * Resize the HTML element.
 * @param HTMLElement - The element to resize.
 * @param float - The percentage of the initial size of the element.
 */
function resize(elt, ratio) {
	elt.width = ratio*elt.width ;
}

$(function () {
	/*Check all metrics*/
	$('#check_all_metrics').click(function(){
		var state = this.checked;
		$("[id^='chk_col_']").each(function(){
			this.checked = state;
		});
	
	});
	
    /* Opens a window with all the element in the selected column */
    $("#columns_extract").click(function() {
    	var samples = new Array() ;
		$("input[id^='chk_sample_']:checked").each( function() {
    		samples.push( parseInt($(this).attr("id").split('_')[2]) ) ;
    	}) ;
    	var columns = new Array() ;
    	$("input[id^='chk_col_']:checked").each( function() {
    		columns.push( parseInt($(this).attr("id").split('_')[2]) ) ;
    	}) ;
		
		if( samples.length != 0 ) {
			if( $('#sample_1_col_1') !== 'undefined' ) {
				columns.push( 1 ) ;
			}
			columns.sort(function(a,b){return a-b}) ;
			samples.sort(function(a,b){return a-b}) ;
			
			//Build thead
			var thead = '<tr>' ;
			for ( var int = 0; int < columns.length; int++) {
				var id = columns[int];
				if( jQuery.inArray( id, columns) != -1 && id == 1 ) {
					thead += '<th>Samples</th>' ;
				}
				else {
					thead += '<th><center>'+$("#th_id_"+id).text()+'</center></th>' ;
				}
			}
			thead += '</tr>' ;	
			thead = '<thead>' + thead + '</thead>' ;
			
			//Build tbody
			var tbody = "" ; 
			
			var pattern_regexp = new RegExp( '<a[^>]+class="[^"]*imglink[^"]*"[^>]+href="(\/*fileadmin\/data[_]*[a-z]*\/analyze\/[^"]+)' ) ;
			for( var i = 0 ; i < samples.length ; i++ ) {
				tbody += '<tr>' ; 
				for( var j = 0 ; j < columns.length ; j++ ) {
					var new_html = $('#sample_' + samples[i] + '_col_' + columns[j]).html() ;
					var inner_exp = pattern_regexp.exec( new_html ) ;
					// If the cell match the pattern
					if( inner_exp != null )
					{
						if( inner_exp[1] === undefined ) {
							inner_exp[1] = "" ;
						}
						new_html = '<img src="' + inner_exp[1] + '" onload="resize(this, 0.5)">' ;
					}	
					tbody += '<td>' + new_html + '</td>' ;
				}	
				tbody += '</tr>' ; 
			}
			tbody = '<tbody>' + tbody + '</tbody>' ;
	
			// Display images table
			$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
			$("#modal-body-tmpl").html('<div id="table_container"></div>');
			$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
			$("#table_container ").css({ 
				'max-height' : modal_height(300, 50)+'px', 
				'overflow-x' : 'auto',
				'overflow-y' : 'auto'
			});
			$("#ng6modal .modal-content").css('width', '870px');
			$("#ng6modal").css('margin-left', '-135px');
			$("#table_container").append( '<table class="table table-striped table-bordered ">' + thead + tbody + '</table>' );
			
			$("#ng6modal").modal();
		}
    }) ;
});