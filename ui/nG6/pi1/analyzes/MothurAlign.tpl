{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}
{extends file='AnalysisTemplate.tpl'}

{block name=params_title} Alignment options {/block}
{block name=params_content}
	{assign var="params" value=";"|explode:$analyse.params}
	<ul>
		{foreach from=$params item=param}
			{assign var="args_cmd" value=","|explode:$param}
			
			{if in_array("#align.seqs(fasta=FILE.fasta", $args_cmd)}
			{foreach from=$args_cmd item=arg_cmd}
				{assign var="params_cmd" value="="|explode:$arg_cmd}
				{if in_array("search", $params_cmd)}
				{assign var="search_index" value=$params_cmd|@array_keys:"search"}
				<li class="parameter">The method of finding the template sequence is by <strong>{$params_cmd[$search_index[0]+1]}</strong>.</li>
				{/if}
				{if in_array("ksize", $params_cmd)}
				{assign var="ksize_index" value=$params_cmd|@array_keys:"ksize"}
				<li class="parameter">The size of kmers is {$params_cmd[$ksize_index[0]+1]}.</li>
				{/if}
			{/foreach}
			<br />
			<li class="parameter">{$param}</li>
			{/if}
			
			{if in_array("screen.seqs(fasta=FILE.align", $args_cmd)}
			<li class="parameter">{$param}</li>
			{/if}
		{/foreach}
	</ul>
{/block}

{block name=results_title} Alignment results {/block}
{block name=results}
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th class="string-sort">Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
				<th class="numeric-sort">Aligned reads</th>
				<th class="numeric-sort">After cleaning the alignment</th>
				<th class="numeric-sort">Aligned out of the area</th>
				<th class="numeric-sort">% deleted</th>
			</tr>
		</thead>
		<tbody>
			{assign var="nb_reads_begining" value=0}
			{assign var="nb_reads_end" value=0}
			{assign var="uncorrect" value=0}
			{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
		   	<tr>
				<td>{$sample|get_description:$descriptions}</td>
				{$nb_reads_begining=$nb_reads_begining+$sample_results["default"].nb_aligned_reads}
				{$nb_reads_end=$nb_reads_end+$sample_results["default"].nb_after_screen}
				{$uncorrect=$uncorrect+($sample_results["default"].nb_aligned_reads-$sample_results["default"].nb_after_screen)}
				<td>{$sample_results["default"].nb_aligned_reads|number_format:0:' ':' '}</td>
				<td>{$sample_results["default"].nb_after_screen|number_format:0:' ':' '}</td>
				<td>{($sample_results["default"].nb_aligned_reads-$sample_results["default"].nb_after_screen)|number_format:0:' ':' '}</td>
				<td>{(100.00-((($sample_results["default"].nb_after_screen)*100)/$sample_results["default"].nb_aligned_reads))|number_format:2:'.':' '}</td>
		   	</tr>
		   	{/foreach}
		</tbody>
		<tfoot>
			{if $analyse_results|@count > 1 }
			<tr>
				<th>Total</th>
				<th>{$nb_reads_begining|number_format:0:' ':' '}</th>
				<th>{$nb_reads_end|number_format:0:' ':' '}</th>
				<th>{$uncorrect|number_format:0:' ':' '}</th>
				<th>{(100.00-((($nb_reads_end)*100)/$nb_reads_begining))|number_format:2:'.':' '}</th>
			</tr>
			<tr>
				<th>Mean</td>
				<th>{($nb_reads_begining/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{($nb_reads_end/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{($uncorrect/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{(100.00-((($nb_reads_end)*100)/$nb_reads_begining))|number_format:2:'.':' '}</th>
			</tr>
			{/if}
		</tfoot>
	</table>
{/block}
