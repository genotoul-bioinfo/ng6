{*
Copyright (C) 2009 INRA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=params_title} Mothur commands and parameters used {/block}
{block name=params_content}
	{assign var="params" value=";"|explode:$analyse.params}
	{foreach from=$analyse_results key=sample item=sample_results}
	{assign var="sample" value=$sample}
	{/foreach}
	<ul>
		{foreach from=$params item=param}
			{assign var="args_cmd" value=","|explode:$param}

			{if in_array("#make.shared(list=FILE.an.list", $args_cmd)}
			{foreach from=$args_cmd item=arg_cmd}
				{assign var="params_cmd" value="="|explode:$arg_cmd}
				{if in_array("label", $params_cmd)}
				{assign var="label_index" value=$params_cmd|@array_keys:"label"}
				<li class="parameter">To know how many sequences are in each OTU we used these levels {$params_cmd[$label_index[0]+1]|replace:')':''}.</li>
				{/if}
			{/foreach}
			<ul>
				<li class="parameter">{$param}</li>
			</ul>
			{/if}

			{if in_array("#summary.single(shared=FILE.shared", $args_cmd)}
			{foreach from=$args_cmd item=arg_cmd}
				{assign var="params_cmd" value="="|explode:$arg_cmd}
				{if in_array("calc", $params_cmd)}
				{assign var="calc_index" value=$params_cmd|@array_keys:"calc"}
				<li class="parameter">The calculator used value for each line in the OTU data and for all possible comparisons between the different groups in the group file :</li>
					{assign var="calcs_index" value="-"|explode:$params_cmd[$calc_index[0]+1]}
					<ul>
					{foreach from=$calcs_index item=calc}
						<li>{$calc}</li>
					{/foreach}
					</ul>
				{/if}
			{/foreach}
			<ul>
				<li class="parameter">{$param}</li>
			</ul>
			{/if}

			{if in_array("#rarefaction.single(shared=FILE.shared", $args_cmd)}
			{foreach from=$args_cmd item=arg_cmd}
				{assign var="params_cmd" value="="|explode:$arg_cmd}
				{if in_array("freq", $params_cmd)}
				{assign var="freq_index" value=$params_cmd|@array_keys:"freq"}
				<li class="parameter">We interested in obtaining all of the data for the number of sequences sampled with a frequency of : {$params_cmd[$freq_index[0]+1]|replace:')':''}.</li>
				{/if}
			{/foreach}
			<ul>
				<li class="parameter">{$param}</li>
			</ul>
			{/if}

			{if in_array("#tree.shared(shared=FILE.shared", $args_cmd)}
				<li class="parameter">{$param}</li>
			{/if}

			{if in_array("#sub.sample(fasta=FILE.fasta", $args_cmd)}
				<li class="parameter">{$param}</li>
			{/if}

			{if in_array("#classify.otu(list=FILE.list", $args_cmd)}
			{foreach from=$args_cmd item=arg_cmd}
				{assign var="params_cmd" value="="|explode:$arg_cmd}
				{if in_array("label", $params_cmd)}
				{assign var="label_index" value=$params_cmd|@array_keys:"label"}
				<li class="parameter">To get a consensus taxonomy for an OTU we used these levels {$params_cmd[$label_index[0]+1]|replace:')':''}.</li>
				{/if}
			{/foreach}
			<ul>
				<li class="parameter">{$param}</li>
			</ul>
			{/if}
		{/foreach}
	</ul>
{/block}

{block name=content}

	{* First find out all levels used *}
	{assign var="levels" value=array()}
	{foreach from=$analyse_results key=sample item=sample_results}
		{foreach from=$sample_results key=group item=value}
			{if !in_array($group, $levels) and $group != "default"}
				{$levels[]=$group}
			{/if}
		{/foreach}
	{/foreach}
	{$null = rsort($levels)}
	{* Then find out all estimators used *}
	{assign var="estimators" value=array()}
	{assign var="tree_avail" value=false}
	{foreach from=$analyse_results key=sample item=sample_results}
		{if $sample == "all"}
			{$tree_avail=true}
		{/if}
		{foreach from=$sample_results key=group item=value}
			{foreach from=$value key=estimator item=est_value}
				{if !in_array($estimator, $estimators) and $estimator != "tre_thetayc" and $estimator != "tre_jclass" and $estimator != "rarefaction_x" and $estimator != "rarefaction_y" and $estimator != "nb_seq" and $estimator != "otus"}
					{$estimators[]=$estimator}
				{/if}
			{/foreach}
		{/foreach}
	{/foreach}
	{assign var="beta_done" value=false}

	{* Init tre values *}
	{if $tree_avail}
		{foreach $levels as $group}
			<input type="hidden" id="tre_thetayc_{$group|replace:".":"_"}" value="{$analyse_results["all"][$group].tre_thetayc}"/>
			<input type="hidden" id="tre_jclass_{$group|replace:".":"_"}" value="{$analyse_results["all"][$group].tre_jclass}"/>
		{/foreach}
	{/if}

	<input type="hidden" id="analyse_name" value="{$analyse.name}"/>
	<ul id="myTab" class="nav nav-tabs">
		<li class="active"><a href="#alpha" data-toggle="tab">Alpha diversity</a></li>
		{if $tree_avail}
		<li><a href="#beta" data-toggle="tab">Beta diversity</a></li>
		{/if}
		<li><a href="#parameters" data-toggle="tab">Parameters</a></li>
		<li><a href="#downloads" data-toggle="tab">{block name=downloads_title} Downloads {/block}</a></li>
	</ul>

	<div id="myTabContent" class="tab-content">
		<div class="tab-pane fade in active" id="alpha">
			<table class="table table-striped table-bordered dataTable analysis-result-table">
				<thead>
					<tr>
						<th rowspan=2><center><input type="checkbox" id="chk_all_sample"></center></th>
						{if $analyse_results|@count > 1 }
						<th class="string-sort" rowspan=2>Samples ({$analyse_results|@count})</th>
						{$beta_done=true}
						{/if}
						<th class="numeric-sort" rowspan=2>Number of sequences</th>
					    {foreach $levels as $group}
					    	<th colspan={$estimators|@count}> {$group} </th>
					    {/foreach}
					</tr>
					<tr>
						{foreach $levels as $group}
							<th class="numeric-sort">OTUs</th>
							{foreach $estimators as $estimator}
								{if $estimator != "sobs" and $estimator != "krona"}
								<th class="numeric-sort">{$estimator}</th>
								{/if}
							{/foreach}
						{/foreach}
					</tr>
				</thead>
				<tbody>
					{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
					{assign var="i" value=0}
					{assign var="krona_done" value=0}
					{foreach from=$analyse_results_sorted key=sample item=sample_results}
					{if $sample != "all"}
						<tr>
							<td><center>
								<input type="checkbox" id="chk_sample_{$i}" value="sample">
								{foreach $levels as $group}
									<input type="hidden" id="rarefaction_curve_x_{$i}_{$group|replace:".":"_"}" value="{$sample_results[$group].rarefaction_x}"/>
									<input type="hidden" id="rarefaction_curve_y_{$i}_{$group|replace:".":"_"}" value="{$sample_results[$group].rarefaction_y}"/>
									{if $sample_results['default'].krona}
										{$krona_done=$krona_done+1}
										<input type="hidden" id="krona_file_{$i}" value="{$sample_results['default'].krona}"/>
									{/if}
									{if $sample_results[$group].otus != "" }
									<input type="hidden" id="otus_{$i}_{$group|replace:".":"_"}" value="{$sample_results[$group].otus}"/>
									{/if}
								{/foreach}
							</center></td>
							{if $analyse_results|@count > 1 }
							<td id="sample_id_{$i}">{$sample|get_description:$descriptions}</td>
							{/if}
							<td>{$sample_results["default"]["nb_seq"]|number_format:0:' ':' '}</td>
							{foreach $levels as $group}
								<td><center>{$sample_results[$group]["sobs"]|number_format:0:' ':' '}</center></td>
								{foreach $estimators as $estimator}
									{if $estimator != "sobs" and $estimator != "krona"}
										{assign var="display_value" value=";"|explode:$sample_results[$group][$estimator]}
										{if $display_value|@count == 3 }
										<td nowrap="nowrap"><center>{$display_value[0]|number_format:2:'.':' '} <br /> ({$display_value[1]|number_format:2:'.':' '}:{$display_value[2]|number_format:2:'.':' '})</center></td>
										{else}
										<td nowrap="nowrap"><center>{$sample_results[$group][$estimator]|number_format:2:'.':' '}</center></td>
										{/if}
									{/if}
								{/foreach}
							{/foreach}
						</tr>
						{$i = $i +1}
					{/if}
					{/foreach}
				</tbody>
				<tfoot>
					<tr>
						<th align="left" colspan="10">
							With selection :
							{if $beta_done}
							<button type="button" class="btn btn-default btn-sm rarefaction-line-view-btn single-selection-btn"><i class="glyphicon glyphicon-signal"></i> Rarefaction curves</button>
							<button type="button" class="btn btn-default btn-sm venn-view-btn multiple5-selection-btn"><i class="glyphicon glyphicon-search"></i> Venn diagram</button>
							{if $krona_done>0}
								<button type="button" class="btn btn-default btn-sm phylogeny-krona-view-btn single-selection-btn"><i class="glyphicon glyphicon-search"></i> Krona view</button>
							{/if}
							{else}
							<button type="button" class="btn btn-default btn-sm rarefaction-line-view-btn single-selection-btn"><i class="glyphicon glyphicon-signal"></i> Rarefaction curves</button>
							{/if}
						</th>
					</tr>
				</tfoot>
			</table>
		</div>

		{if $tree_avail}
		<div class="tab-pane fade" id="beta">
			<div id="svgCanvas"></div>
		</div>
		{/if}

		<div class="tab-pane fade" id="parameters">
			{block name=params}
				{block name=params_content}
				<ul>
					<li class="parameter">{$analyse.params}</li>
				</ul>
				{/block}
			{/block}
		</div>
		<div class="tab-pane fade" id="downloads">
			{block name=download}
				{assign var="nb_files" value=0}
				{$dir=$data_folder|cat:$analyse.directory}
				{foreach $dir|scandir as $file}
					{assign var="link" value=(('fileadmin'|cat:$analyse.directory)|cat:'/')|cat:$file}
					{if $file != "." and $file != "" and $file != ".." and ($file|substr:-strlen(".png")) != ".png" and !is_dir($link)}
						{$nb_files = $nb_files + 1}
					{/if}
				{/foreach}
				{if $nb_files == 0}
					<div class="alert alert-info">
						Results folder not synchronized yet...
					</div>
				{else}
					<ul>
						{foreach $dir|scandir as $file}
						{assign var="link" value=(('fileadmin'|cat:$analyse.directory)|cat:'/')|cat:$file}
						{if $file != "." and $file != "" and $file != ".." and ($file|substr:-strlen(".png")) != ".png" and !is_dir($link)}
							<li class="filelist"><a href="{$link}">{$file}</a> </li>
						{/if}
						{/foreach}
					</ul>
				{/if}
			{/block}
		</div>
	</div>

{/block}
