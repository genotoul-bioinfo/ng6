{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=results_title} Clustering results {/block}
{block name=results}
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th><center><input type="checkbox" id="chk_all_sample"></center></th>
				<th class="string-sort">Clustering ({$analyse_results|@count})</th>
				<th class="numeric-sort">Nb observations</th>
				<th class="numeric-sort">Nb seq</th>
				<th class="numeric-sort">Min depth</th>
				<th class="numeric-sort">Lower quartile depth</th>
				<th class="numeric-sort">Median depth</th>
				<th class="numeric-sort">Upper quartile depth</th>
				<th class="numeric-sort">Max depth</th>
			</tr>
		</thead>
		<tbody>
			{assign var="i" value=0}
			{assign var="totale" value=0}
			{assign var="total1" value=0}
			{assign var="prct_extend_total" value=0}
			{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
			<tr>
				<td><center>
					<input type="checkbox" id="chk_sample_{$i}" value="sample">
					<input type="hidden" id="depths_{$i}" value="{$sample_results["default"].depths}"/>
					<input type="hidden" id="counts_{$i}" value="{$sample_results["default"].counts}"/>
					<input type="hidden" id="linkage_{$i}" value='{$sample_results["default"].linkage}'/>
				</center></td>			
				<td id="sample_id_{$i}">{$sample|get_description:$descriptions}</td>
				{assign var="array_depths" value=","|explode:$sample_results["default"].depths}
				{assign var="min" value=$array_depths[0]}
				{assign var="max" value=$array_depths[count($array_depths)-1]}
				<td id="count_{$i}">{$sample_results["default"].nb_observations|number_format:0:' ':' '}</td>
				<td id="seq_{$i}">{$sample_results["default"].nb_sequences|number_format:0:' ':' '}</td>
				<td id="min_{$i}">{$min}</td>
				<td id="lq_{$i}">{$sample_results["default"].lower_quartile}</td>
				<td id="median_{$i}">{$sample_results["default"].median}</td>
				<td id="uq_{$i}">{$sample_results["default"].upper_quartile}</td>
				<td id="max_{$i}">{$max|number_format:0:' ':' '}</td>
			</tr>
			{$i = $i + 1}
			{/foreach}
		</tbody>
		<tfoot>
			<tr>
				<th align="left" colspan="9">
					With selection :
					<button type="button" class="btn btn-default multiple-selection-btn depth-stat-view-btn"><i class="glyphicon glyphicon-signal"></i> Dispersion</button>
					<button type="button" class="btn btn-default multiple-selection-btn depth-hist-view-btn"><i class="glyphicon glyphicon-signal"></i> Depths</button>
					<button type="button" class="btn btn-default multiple-selection-btn reads-curve-view-btn"><i class="glyphicon glyphicon-signal"></i> Reads</button>
					<button type="button" class="btn btn-default single-selection-btn linkage-tree-view-btn"><i class="glyphicon glyphicon-signal"></i> Linkages</button>
				</th>
			</tr>
		</tfoot>
	</table>
{/block}
