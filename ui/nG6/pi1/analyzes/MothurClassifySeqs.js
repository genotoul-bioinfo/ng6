/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */

$(function () {
	
    /* 
    * Define functions in charge to visualize venn charts
    */
	$(".venn-view-btn").click(function() {
    	if ($(":checked[id^=chk_sample_]").size() <= 5) {
			$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
            $("#modal-body-tmpl").html('<div id="navigation_container" style="margin-right:20px; float: left; width: 520px;"><blockquote>Species repartition throught the different samples. To display the shared species, click on the corresponding number.</blockquote><br /><div id="venn_container"></div></div><div style="width: 1160px;"><textarea style="resize:none; height:560px; width: 600px;" id="venn_text"></textarea></div>');
            $("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
    		resize_center_btmodal('#ng6modal', 1200);
            $("#ng6modal").modal();
        	var ajax = new Array();
        	var samples_desc = new Array();
        	var samples = new Array();
            $(":checked[id^=chk_sample_]").each(function(){
	            var index = $(this).attr("id").split("_")[2];
	            ajax.push($.ajax({
	            	url: $("#phylogeny_file_"+index).val(),
	            	dataType: "text"
	            }));
	            samples.push("phylogeny_file_"+index);
	            samples_desc["phylogeny_file_"+index] = $("#sample_id_"+index).html();
    		});
            $.when.apply($, ajax).done(function(){
            	var values = {};
            	if (samples.length == 1) {
                	values[samples[0]] = new Array();
                	var current_level = -1;
                	var current_tax = "";
            		var lines = arguments[0].split("\n");
            		for (j=0; j<lines.length; j++) {
    	        		parts = lines[j].split("\t");
    	        		if (parts[0] != "taxlevel" && parts[0] != "0") {
    	        			if (parts[0] == "6") {
    	        				current_tax += parts[2];
    	        				values[samples[0]][current_tax] = parts[4];
    	        			} else if (parseInt(parts[0]) > current_level) {
    	        				current_tax += parts[2] + ";";
    	        			} else {
    	        				var table = current_tax.split(";");
    	        				current_tax = table.slice(0, table.length-(current_level-parseInt(parts[0]))-1).join(";") + ";" + parts[2] + ";";
    	        			}
    	        		}
    	        		current_level = parseInt(parts[0]);
    	        	}
            	} else {
	                for( var i = 0; i < arguments.length; i++ ) {
	                	values[samples[i]] = new Array();
	                	var current_level = -1;
	                	var current_tax = "";
	                	var lines = arguments[i][0].split("\n");
	    	        	for (j=0; j<lines.length; j++) {
	    	        		parts = lines[j].split("\t");
	    	        		if (parts[0] != "taxlevel" && parts[0] != "0") {
	    	        			if (parts[0] == "6") {
	    	        				current_tax += parts[2];
	    	        				values[samples[i]][current_tax] = parts[4];
	    	        			} else if (parseInt(parts[0]) > current_level) {
	    	        				current_tax += parts[2] + ";";
	    	        			} else {
	    	        				var table = current_tax.split(";");
	    	        				current_tax = table.slice(0, table.length-(current_level-parseInt(parts[0]))-1).join(";") + ";" + parts[2] + ";";
	    	        			}
	    	        		}
	    	        		current_level = parseInt(parts[0]);
	    	        	}
	                }
            	}
                var series_table = new Array();
                for (val in values) {
                	var data_table = new Array();
                	for (v in values[val]) {
                		data_table.push(v);
                	}
                	series_table.push({
                    	name: samples_desc[val],
                    	data: data_table
                    });
                }
                $("#venn_container").venny({
    	            series: series_table,
    	            fnClickCallback: function() {
	                	var list_elt = "";
	                	for (name in this.listnames) {
	                		list_elt += this.listnames[name] + " ";
	                	}
	                	var value = ""
	                	if (this.listnames.length == 1) {
	                		value += 'Elements only in ' + list_elt + ' :';
	                	} else {
	                		value += 'Common elements in ' + list_elt + ' :';
	                	}
	                	value += "\n";
	                	for (val in this.list) {
	                		value += this.list[val] + "\n";
	                	}
	                	$("#venn_text").val(value);
	                }
    	        });
            });
    	}
	});

    /* 
     * Define functions in charge to visualize phylogenic bar charts
     */
    $(".phylogeny-bar-view-btn").click(function() {
    	if ($(":checked[id^=chk_sample_]").size() == 1) {
			$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
            $("#modal-body-tmpl").html('<div id="highcharts_container"></div>');
            $("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
    		resize_center_btmodal('#ng6modal', 750);
            $("#ng6modal").modal();
	       	var index = $(":checked[id^=chk_sample_]").attr("id").split("_")[2];
	        $.get($("#phylogeny_file_"+index).val(), function(data) {
	        	// First convert mothur_phylo to hash table
	        	var lines = data.split("\n");
	        	var current_level = 0;
	        	var full_phylo = "";
	        	var taxonomy = new Array(); 
	        	level_1_keys = new Array();
	        	level_1_values = new Array();
	        	for (i=0; i<lines.length; i++) {
	        		parts = lines[i].split("\t");
	        		if (parts[0] != "taxlevel" && parts[0] != "0") {
	        			// If this is a 1st level
	        			if (parts[0] == "1") {
	        				level_1_keys.push(parts[2]);
	        				level_1_values.push(parseInt(parts[4]));
	        			}
	        			// If it's a sub level: children
	        			if (parseInt(parts[0]) > current_level) {
	        				// If it's a leaf
	                        if (parseInt(parts[3]) == 0) {
	                        	full_phylo += parts[2];
	                        	taxonomy.push(full_phylo+"\t"+parts[3]+"\t"+parts[4]);
	                        	full_phylo +=  ";";
	                        // It's a node 
	                        } else {
	                        	full_phylo += parts[2];
	                        	taxonomy.push(full_phylo+"\t"+parts[3]+"\t"+parts[4]);
	                        	full_phylo +=  ";";
	                        }
		        		// If it's the same level: siblings
		        		} else if (parseInt(parts[0]) == current_level) {
		        			var phylo_tab = full_phylo.split(";");
		        			full_phylo = "";
		        			var indice = phylo_tab.length-2;
		        			for (j=0;j<indice;j++) {
		        				full_phylo += phylo_tab[j] + ";";
		        			}
	        				// If it's a leaf
	                        if (parseInt(parts[3]) == 0) {
	                        	full_phylo += parts[2];
	                        	taxonomy.push(full_phylo+"\t"+parts[3]+"\t"+parts[4]);
	                        	full_phylo +=  ";";
	                        // It's a node 
	                        } else {
	                        	full_phylo += parts[2];
	                        	taxonomy.push(full_phylo+"\t"+parts[3]+"\t"+parts[4]);
	                        	full_phylo +=  ";";
	                        }
		        		// If it's a higher level
		        		} else {
		        			var phylo_tab = full_phylo.split(";");
		        			full_phylo = "";
		        			var indice = phylo_tab.length-(current_level-parseInt(parts[0]))-2;
		        			for (j=0;j<indice;j++) {
		        				full_phylo += phylo_tab[j] + ";";
		        			}
	        				// If it's a leaf
	                        if (parseInt(parts[3]) == 0) {
	                        	full_phylo += parts[2];
	                        	taxonomy.push(full_phylo+"\t"+parts[3]+"\t"+parts[4]);
	                        	full_phylo +=  ";";
	                        // It's a node 
	                        } else {
	                        	full_phylo += parts[2];
	                        	taxonomy.push(full_phylo+"\t"+parts[3]+"\t"+parts[4]);
	                        	full_phylo +=  ";";
	                        }
		        		}
	        			current_level = parseInt(parts[0]);
	        		}
	        	}
	        	
	        	var colors = new Array("#4572A7","#AA4643","#89A54E","#80699B","#3D96AE","#DB843D","#92A8CD","#A47D7C","#B5CA92", "#FFF718", "#ED00D4", "#D5B0FF", "#91E2F9", "#FFB87E", "#FFFCA3", "#FFA0F5");//Highcharts.getOptions().colors;
	        	var categories = level_1_keys;
	        	var name = '';
	        	data = []
	        	for (i=0; i<level_1_keys.length; i++) {
	        		var sub_taxonomy = new Array();
	        		for (j=0; j<taxonomy.length; j++){
	        			// Produce the sub table
	        			var value = taxonomy[j].split("\t")[0];
	        			if (taxonomy[j].indexOf(level_1_keys[i]) === 0 && level_1_keys[i] != value) {
	        				sub_taxonomy.push(taxonomy[j]);
	        			}
	        		}
	        		data.push({
	        			y: level_1_values[i],
	        			color: colors[i],
	        			drilldown: {
	        				name: level_1_keys[i],
	        				categories: get_categories(sub_taxonomy, 1),
	        				data: get_data(sub_taxonomy, 1, colors),
	        				color: colors[i]
	        			}
	        		});
	        	}
	
	        	function setChart(name, categories, data, color) {
					chart.xAxis[0].setCategories(categories);
					chart.series[0].remove();
					chart.addSeries({
						name: name,
						data: data,
						color: 'white'
					});
				}
	        	
				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'highcharts_container', 
						type: 'column'
					},
					title: {
						text: 'Taxonomic affiliation against ' + $("#phylogeny_file_"+index).val().split(".").slice(-3)[0] + ' database.'
					},
					subtitle: {
						text: ''
					},
					xAxis: {
						categories: categories,
						labels: {
							rotation: -45,
							align: 'right'
						}
					},
					yAxis: {
						title: {
							text: "Number of sequences"
						}
					},
                    legend: {
                        enabled: true
					},
					credits: { enabled: false },
					plotOptions: {
						column: {
							cursor: 'pointer',
							point: {
								events: {
									click: function() {
										var drilldown = this.drilldown;
										if (drilldown) { // drill down
											setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
										} else { // restore
											setChart(name, categories, data);
										}
									}
								}
							},
							dataLabels: {
								enabled: true,
								color: colors[0],
								style: {
									fontWeight: 'bold'
								},
								formatter: function() {
									return this.y;
								}
							}					
						}
					},
					tooltip: {
						formatter: function() {
							var point = this.point,
								s = this.x +':<b>'+ this.y +' sequences</b><br/>';
							if (point.drilldown) {
								s += 'Click to view' + point.category;
							} else {
								s += 'Click to return to first graph';
							}
							return s;
						}
					},
					series: [{
						name: name,
						data: data,
						color: 'white'
					}]
				});
				chart.setSize(720, 450);
				
	        }, "text");
    	} else {
    		/* Function in charge to create the chart */
            function updateChart(rankid) {
            	var ajax = new Array();
                var samples = new Array();
                var index = "";
                $(":checked[id^=chk_sample_]").each(function(){
    	            index = $(this).attr("id").split("_")[2];
    	            ajax.push($.ajax({
    	            	url: $("#phylogeny_file_"+index).val(),
    	            	dataType: "text"
    	            }));
    	            samples.push($("#sample_id_"+index).html());
        		});
                $.when.apply($, ajax).done(function(){
                	var taxonomy = new Array();
                	var taxonomy_val = new Array();
                	var samples_taxonomy = new Array();
                	var taxlevel = rankid.split(".").length - 1;
                	// for each taxonomy files
                    for( var i = 0; i < arguments.length; i++ ) {
                    	samples_taxonomy.push(new Array());
                    	var rank_found = false;
                    	var lines = arguments[i][0].split("\n");
        	        	for (j=0; j<lines.length; j++) {
        	        		parts = lines[j].split("\t");
        	        		if (rank_found && parseInt(parts[0]) == taxlevel) {
        	        			break;
        	        		}
        	        		if (rank_found && parseInt(parts[0]) == taxlevel + 1) {
        	        			if (taxonomy.indexOf(parts[2].replace(/"/g, '')) === -1) {
        	        				taxonomy.push(parts[2].replace(/"/g, ''));
        	        				taxonomy_val.push(parts[1]);
        	        			}
        	        			samples_taxonomy[i][parts[1]] = parts[4];
        	        		}
        	        		if (parts[1] == rankid) {
        	        			rank_found = true;
        	        		}
        	        	}
                    }
                    var series_table = new Array();
                    for (i in samples) {
                    	data_table = new Array();
                    	for (j in taxonomy_val) {
                    		if (taxonomy_val[j] in samples_taxonomy[i]) {
                    			data_table.push(parseInt(samples_taxonomy[i][taxonomy_val[j]]))
                    		} else {
                    			data_table.push(0);
                    		}
                    	}
                    	series_table.push({name:samples[i], data:data_table});
                    }
                    chart = new Highcharts.Chart({
    					chart: {
    						renderTo: 'highcharts_container',
    						defaultSeriesType: 'column'
    					},
    					credits: { enabled: false },
    					title: {
    						text: 'Taxonomic affiliation against ' + $("#phylogeny_file_"+index).val().split(".").slice(-3)[0] + ' database.'
    					},
    					subtitle: {
    						text: ''
    					},
    					xAxis: {
    						categories: taxonomy,
    						labels: {
    							rotation: -45,
    							align: 'right'
    						}
    					},
    					yAxis: {
    						min: 0,
    						title: {
    							text: "Number of sequences"
    						}
    					},
    					legend: {
    						layout: 'vertical',
    						backgroundColor: '#FFFFFF',
    						align: 'left',
    						verticalAlign: 'top',
    						x: 100,
    						y: 70,
    						floating: true,
    						shadow: true
    					},
    					tooltip: {
    						formatter: function() {
    							return ''+
    								this.x +': '+ this.y +' sequences';
    						}
    					},
    					plotOptions: {
    						column: {
    							pointPadding: 0.2,
    							borderWidth: 0
    						}
    					},
    				        series: series_table
    				});
                });
            }


			$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
            $("#modal-body-tmpl").html('<div id="navigation_container" style="float: left; width: 280px;"></div><div id="highcharts_container" style="width: 900px;"></div>');
            $("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
    		resize_center_btmodal('#ng6modal', 1200);
            $("#ng6modal").modal();
            var ajax = new Array();
            var samples = new Array();
            $(":checked[id^=chk_sample_]").each(function(){
	            var index = $(this).attr("id").split("_")[2];
	            ajax.push($.ajax({
	            	url: $("#phylogeny_file_"+index).val(),
	            	dataType: "text"
	            }));
	            samples.push($("#phylogeny_file_"+index).val());
    		});            
            // Init the navigation
            $.when.apply($, ajax).done(function(){
               	$("#navigation_container").html('<blockquote>Click on a taxonomic level to display the sequences distribution at this level.</blockquote><br /><div id="jstree_navigation"></div>');
                var html_data = $("<ul></ul>");
                var current_level = -1;
                var parentl = ""
                for( var i = 0; i < arguments.length; i++ ) {
                	var lines = arguments[i][0].split("\n");
    	        	for (j=0; j<lines.length; j++) {
    	        		if (lines[j] != "") {
        	        		mparts = lines[j].split("\t");
        	        		if (mparts[0] != "taxlevel" && mparts[0] != "9") {
        	        			// If this guy doesnt exist yet
        	        			if (html_data.find("#li_"+mparts[1].replace(/\./g, '_')).length == 0) {
        	        				parentl = mparts[1].split(".").slice(0,-1).join("_");
	        	        			// If no parent yet, this is the root
	        	        			if (html_data.find("#li_"+parentl).length == 0) {
	        	        				html_data.append('<li id=li_'+mparts[1].replace(/\./g, '_')+'><a class="taxonomy_click" id="'+mparts[1].replace(/\./g, '_')+'">'+mparts[2]+'</a><ul></ul></li>');
	        	        			// Otherwise append it to the parent
	        	        			} else {
	        	        				if (mparts[0] != "8") {
	        	        					html_data.find("#li_"+parentl+" > ul").append('<li id=li_'+mparts[1].replace(/\./g, '_')+'><a class="taxonomy_click" id="'+mparts[1].replace(/\./g, '_')+'">'+mparts[2]+'</a><ul></ul></li>');
	        	        				} else {
	        	        					html_data.find("#li_"+parentl+" > ul").append('<li id=li_'+mparts[1].replace(/\./g, '_')+'><a class="taxonomy_click" id="'+mparts[1].replace(/\./g, '_')+'">'+mparts[2]+'</a></li>');
	        	        				}
	        	        			}
        	        			}

        	        		}
    	        		}
    	        	}
                }
                $("#jstree_navigation").jstree({
            		"themes" : {
            			"theme" : "default",
            			"icons" : false
            		},
            		"ui" : {
            			"select_limit" : 1,
            			"selected_parent_close" : "select_parent",
            			"initially_select" : [ "li_0" ]
            		},
                    "html_data" : {
                        "data" : html_data
                    },
                    "plugins" : [ "themes", "html_data", "ui" ]
                });
                
                $("a.taxonomy_click").click(function(){
                	updateChart($(this).attr("id").replace(/_/g, '.'));
                });                    
            });
            // Init the chart
            updateChart("0");
    	}
    });
    function get_categories(lines, depth) {
    	var cathegories = new Array();
    	for (var x=0;x<lines.length;x++) {
    		parts = lines[x].split(";");
    		// If this is the right depth
    		if (depth+1 == parts.length) {
    			var values = lines[x].split("\t");
    			var final_value = values[0].split(";");
    			cathegories.push(final_value[final_value.length-1].replace(/"/g, ''));
    		}
    	}
    	return cathegories;
    }
    function get_data(lines, depth, colors) {
    	var data = new Array(); 
    	var current_level = new Array();
    	for (var i=0;i<lines.length;i++) {
    		parts = lines[i].split(";");
    		// If this is the right depth
    		if (depth+1 == parts.length) {
    			current_level.push(lines[i]);
    		}
    	}
    	for (var i=0;i<current_level.length;i++) {
    		parts = current_level[i].split("\t");
    		// If it's a leaf
    		if (parts[1] == "0") {
    			data.push({y:parseInt(parts[2]), color: colors[i]});			
    		// It's a node
    		} else {
        		var sub_taxonomy = new Array();
        		for (j=0; j<lines.length; j++){
        			// Produce the sub table
        			var value = lines[j].split("\t")[0];
        			if (lines[j].indexOf(parts[0]) === 0 && parts[0] != value) {
        				sub_taxonomy.push(lines[j]);
        			}
        		}
        		var final_name = parts[0].replace(/"/g, '');
        		data.push({y:parseInt(parts[2]), color: colors[i], drilldown:{name:final_name, categories:get_categories(sub_taxonomy, depth+1), data:get_data(sub_taxonomy, depth+1, colors)}});
    		}
    	}
    	return data;
    }

    /* 
     * Define functions in charge to visualize phylogenic stacked bar charts
     */
    $(".phylogeny-stackedbar-view-btn").click(function() {
    	if ($(":checked[id^=chk_sample_]").size() == 1) {
			$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
            $("#modal-body-tmpl").html('<div id="highcharts_container"></div>');
            $("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
    		resize_center_btmodal('#ng6modal', 750);
            $("#ng6modal").modal();
	       	var index = $(":checked[id^=chk_sample_]").attr("id").split("_")[2];
	        $.get($("#phylogeny_file_"+index).val(), function(data) {
	        	// First convert mothur_phylo to hash table
	        	var lines = data.split("\n");
	        	var current_level = 0;
	        	var full_phylo = "";
	        	var taxonomy = new Array(); 
	        	level_1_keys = new Array();
	        	level_1_values = new Array();
	        	for (i=0; i<lines.length; i++) {
	        		parts = lines[i].split("\t");
	        		if (parts[0] != "taxlevel" && parts[0] != "0") {
	        			// If this is a 1st level
	        			if (parts[0] == "1") {
	        				level_1_keys.push(parts[2]);
	        				level_1_values.push(parseInt(parts[4]));
	        			}
	        			// If it's a sub level: children
	        			if (parseInt(parts[0]) > current_level) {
	        				// If it's a leaf
	                        if (parseInt(parts[3]) == 0) {
	                        	full_phylo += parts[2];
	                        	taxonomy.push(full_phylo+"\t"+parts[3]+"\t"+parts[4]);
	                        	full_phylo +=  ";";
	                        // It's a node 
	                        } else {
	                        	full_phylo += parts[2];
	                        	taxonomy.push(full_phylo+"\t"+parts[3]+"\t"+parts[4]);
	                        	full_phylo +=  ";";
	                        }
		        		// If it's the same level: siblings
		        		} else if (parseInt(parts[0]) == current_level) {
		        			var phylo_tab = full_phylo.split(";");
		        			full_phylo = "";
		        			var indice = phylo_tab.length-2;
		        			for (j=0;j<indice;j++) {
		        				full_phylo += phylo_tab[j] + ";";
		        			}
	        				// If it's a leaf
	                        if (parseInt(parts[3]) == 0) {
	                        	full_phylo += parts[2];
	                        	taxonomy.push(full_phylo+"\t"+parts[3]+"\t"+parts[4]);
	                        	full_phylo +=  ";";
	                        // It's a node 
	                        } else {
	                        	full_phylo += parts[2];
	                        	taxonomy.push(full_phylo+"\t"+parts[3]+"\t"+parts[4]);
	                        	full_phylo +=  ";";
	                        }
		        		// If it's a higher level
		        		} else {
		        			var phylo_tab = full_phylo.split(";");
		        			full_phylo = "";
		        			var indice = phylo_tab.length-(current_level-parseInt(parts[0]))-2;
		        			for (j=0;j<indice;j++) {
		        				full_phylo += phylo_tab[j] + ";";
		        			}
	        				// If it's a leaf
	                        if (parseInt(parts[3]) == 0) {
	                        	full_phylo += parts[2];
	                        	taxonomy.push(full_phylo+"\t"+parts[3]+"\t"+parts[4]);
	                        	full_phylo +=  ";";
	                        // It's a node 
	                        } else {
	                        	full_phylo += parts[2];
	                        	taxonomy.push(full_phylo+"\t"+parts[3]+"\t"+parts[4]);
	                        	full_phylo +=  ";";
	                        }
		        		}
	        			current_level = parseInt(parts[0]);
	        		}
	        	}
	        	
	        	var colors = new Array("#4572A7","#AA4643","#89A54E","#80699B","#3D96AE","#DB843D","#92A8CD","#A47D7C","#B5CA92", "#FFF718", "#ED00D4", "#D5B0FF", "#91E2F9", "#FFB87E", "#FFFCA3", "#FFA0F5");//Highcharts.getOptions().colors;
	        	var categories = level_1_keys;
	        	var name = '';
	        	data = []
	        	for (i=0; i<level_1_keys.length; i++) {
	        		var sub_taxonomy = new Array();
	        		for (j=0; j<taxonomy.length; j++){
	        			// Produce the sub table
	        			var value = taxonomy[j].split("\t")[0];
	        			if (taxonomy[j].indexOf(level_1_keys[i]) === 0 && level_1_keys[i] != value) {
	        				sub_taxonomy.push(taxonomy[j]);
	        			}
	        		}
	        		data.push({
	        			y: level_1_values[i],
	        			color: colors[i],
	        			drilldown: {
	        				name: level_1_keys[i],
	        				categories: get_categories(sub_taxonomy, 1),
	        				data: get_data(sub_taxonomy, 1, colors),
	        				color: colors[i]
	        			}
	        		});
	        	}
	
	        	function setChart(name, categories, data, color) {
					chart.xAxis[0].setCategories(categories);
					chart.series[0].remove();
					chart.addSeries({
						name: name,
						data: data,
						color: 'white'
					});
				}
	        	
				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'highcharts_container', 
						type: 'column'
					},
					title: {
						text: 'Taxonomic affiliation against ' + $("#phylogeny_file_"+index).val().split(".").slice(-3)[0] + ' database.'
					},
					subtitle: {
						text: ''
					},
					xAxis: {
						categories: categories,
						labels: {
							rotation: -45,
							align: 'right'
						}
					},
					yAxis: {
						title: {
							text: "Number of sequences"
						},

                        stackLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold',
                                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                            }
                        }
					},
                    legend: {
                        enabled: true
					},
					credits: { enabled: false },
					plotOptions: {
						column: {
                            stacking: 'normal',
							cursor: 'pointer',
							point: {
								events: {
									click: function() {
										var drilldown = this.drilldown;
										if (drilldown) { // drill down
											setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
										} else { // restore
											setChart(name, categories, data);
										}
									}
								}
							},
							dataLabels: {
								enabled: true,
								color: colors[0],
								style: {
									fontWeight: 'bold'
								},
								formatter: function() {
									return this.y;
								}
							}					
						}
					},
					tooltip: {
						formatter: function() {
							var point = this.point,
								s = this.x +':<b>'+ this.y +' sequences</b><br/>';
							if (point.drilldown) {
								s += 'Click to view' + point.category;
							} else {
								s += 'Click to return to first graph';
							}
							return s;
						}
					},
					series: [{
						name: name,
						data: data,
						color: 'white'
					}]
				});
				chart.setSize(720, 450);
				
	        }, "text");
    	} else {
    		/* Function in charge to create the chart */
            function updateChart(rankid) {
            	var ajax = new Array();
                var samples = new Array();
                var index = "";
                $(":checked[id^=chk_sample_]").each(function(){
    	            index = $(this).attr("id").split("_")[2];
    	            ajax.push($.ajax({
    	            	url: $("#phylogeny_file_"+index).val(),
    	            	dataType: "text"
    	            }));
    	            samples.push($("#sample_id_"+index).html());
        		});
                $.when.apply($, ajax).done(function(){
                	var taxonomy = new Array();
                	var taxonomy_val = new Array();
                	var samples_taxonomy = new Array();
                	var taxlevel = rankid.split(".").length - 1;
                	// for each taxonomy files
                    for( var i = 0; i < arguments.length; i++ ) {
                    	samples_taxonomy.push(new Array());
                    	var rank_found = false;
                    	var lines = arguments[i][0].split("\n");
        	        	for (j=0; j<lines.length; j++) {
        	        		parts = lines[j].split("\t");
        	        		if (rank_found && parseInt(parts[0]) == taxlevel) {
        	        			break;
        	        		}
        	        		if (rank_found && parseInt(parts[0]) == taxlevel + 1) {
        	        			if (taxonomy.indexOf(parts[2].replace(/"/g, '')) === -1) {
        	        				taxonomy.push(parts[2].replace(/"/g, ''));
        	        				taxonomy_val.push(parts[1]);
        	        			}
        	        			samples_taxonomy[i][parts[1]] = parts[4];
        	        		}
        	        		if (parts[1] == rankid) {
        	        			rank_found = true;
        	        		}
        	        	}
                    }

                    // Formatting the data.
                    // Rows: samples
                    // Cols: values
                    var series_table = new Array(); 
                    for (j in taxonomy_val) {
                    	data_table = new Array();
                    	for (i in samples) {
                    		if (taxonomy_val[j] in samples_taxonomy[i]) {
                    			data_table.push(parseInt(samples_taxonomy[i][taxonomy_val[j]]))
                    		} else {
                    			data_table.push(0);
                    		}
                    	}
                    	series_table.push({name:taxonomy[j], data:data_table});
                    }


                    chart = new Highcharts.Chart({
    					chart: {
    						renderTo: 'highcharts_container',
    						defaultSeriesType: 'column'
    					},
    					credits: { enabled: false },
    					title: {
    						text: 'Taxonomic affiliation against ' + $("#phylogeny_file_"+index).val().split(".").slice(-3)[0] + ' database.'
    					},
    					subtitle: {
    						text: ''
    					},
    					xAxis: {
    						categories: samples,
    						labels: {
    							rotation: -45,
    							align: 'right'
    						}
    					},
    					yAxis: {
    						min: 0,
    						title: {
    							text: "Number of sequences"
    						},
                            stackLabels: {
                                enabled: true,
                                style: {
                                    fontWeight: 'bold',
                                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                                }
                            }
    					},
    					legend: {
    						layout: 'vertical',
    						backgroundColor: '#FFFFFF',
    						align: 'right',
    						verticalAlign: 'top',
    						x: -70,
    						y: 20,
    						floating: false,
    						shadow: true
    					},
    					tooltip: {
    						formatter: function() {
                                return '<b>'+ this.x +'</b><br/>'+
                                this.series.name +': '+ this.y +'<br/>'+
                                'Total: '+ this.point.stackTotal;
                        }
    					},
    					plotOptions: {
    						column: {
                                stacking: 'normal',
    							pointPadding: 0.2,
    							borderWidth: 0
    						}
    					},
    				        series: series_table
    				});
                });
            }


			$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
            $("#modal-body-tmpl").html('<div id="navigation_container" style="float: left; width: 280px;"></div><div id="highcharts_container" style="width: 900px;"></div>');
            $("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
            resize_center_btmodal('#ng6modal', 1200);
            $("#ng6modal").modal();
            var ajax = new Array();
            var samples = new Array();
            $(":checked[id^=chk_sample_]").each(function(){
	            var index = $(this).attr("id").split("_")[2];
	            ajax.push($.ajax({
	            	url: $("#phylogeny_file_"+index).val(),
	            	dataType: "text"
	            }));
	            samples.push($("#phylogeny_file_"+index).val());
    		});            
            // Init the navigation
            $.when.apply($, ajax).done(function(){
               	$("#navigation_container").html('<blockquote>Click on a taxonomic level to display the sequences distribution at this level.</blockquote><br /><div id="jstree_navigation"></div>');
                var html_data = $("<ul></ul>");
                var current_level = -1;
                var parentl = ""
                for( var i = 0; i < arguments.length; i++ ) {
                	var lines = arguments[i][0].split("\n");
    	        	for (j=0; j<lines.length; j++) {
    	        		if (lines[j] != "") {
        	        		mparts = lines[j].split("\t");
        	        		if (mparts[0] != "taxlevel" && mparts[0] != "9") {
        	        			// If this guy doesnt exist yet
        	        			if (html_data.find("#li_"+mparts[1].replace(/\./g, '_')).length == 0) {
        	        				parentl = mparts[1].split(".").slice(0,-1).join("_");
	        	        			// If no parent yet, this is the root
	        	        			if (html_data.find("#li_"+parentl).length == 0) {
	        	        				html_data.append('<li id=li_'+mparts[1].replace(/\./g, '_')+'><a class="taxonomy_click" id="'+mparts[1].replace(/\./g, '_')+'">'+mparts[2]+'</a><ul></ul></li>');
	        	        			// Otherwise append it to the parent
	        	        			} else {
	        	        				if (mparts[0] != "8") {
	        	        					html_data.find("#li_"+parentl+" > ul").append('<li id=li_'+mparts[1].replace(/\./g, '_')+'><a class="taxonomy_click" id="'+mparts[1].replace(/\./g, '_')+'">'+mparts[2]+'</a><ul></ul></li>');
	        	        				} else {
	        	        					html_data.find("#li_"+parentl+" > ul").append('<li id=li_'+mparts[1].replace(/\./g, '_')+'><a class="taxonomy_click" id="'+mparts[1].replace(/\./g, '_')+'">'+mparts[2]+'</a></li>');
	        	        				}
	        	        			}
        	        			}

        	        		}
    	        		}
    	        	}
                }
                $("#jstree_navigation").jstree({
            		"themes" : {
            			"theme" : "default",
            			"icons" : false
            		},
            		"ui" : {
            			"select_limit" : 1,
            			"selected_parent_close" : "select_parent",
            			"initially_select" : [ "li_0" ]
            		},
                    "html_data" : {
                        "data" : html_data
                    },
                    "plugins" : [ "themes", "html_data", "ui" ]
                });
                
                $("a.taxonomy_click").click(function(){
                	updateChart($(this).attr("id").replace(/_/g, '.'));
                });                    
            });
            // Init the chart
            updateChart("0");
    	}
    });
    function get_categories(lines, depth) {
    	var cathegories = new Array();
    	for (var x=0;x<lines.length;x++) {
    		parts = lines[x].split(";");
    		// If this is the right depth
    		if (depth+1 == parts.length) {
    			var values = lines[x].split("\t");
    			var final_value = values[0].split(";");
    			cathegories.push(final_value[final_value.length-1].replace(/"/g, ''));
    		}
    	}
    	return cathegories;
    }
    function get_data(lines, depth, colors) {
    	var data = new Array(); 
    	var current_level = new Array();
    	for (var i=0;i<lines.length;i++) {
    		parts = lines[i].split(";");
    		// If this is the right depth
    		if (depth+1 == parts.length) {
    			current_level.push(lines[i]);
    		}
    	}
    	for (var i=0;i<current_level.length;i++) {
    		parts = current_level[i].split("\t");
    		// If it's a leaf
    		if (parts[1] == "0") {
    			data.push({y:parseInt(parts[2]), color: colors[i]});			
    		// It's a node
    		} else {
        		var sub_taxonomy = new Array();
        		for (j=0; j<lines.length; j++){
        			// Produce the sub table
        			var value = lines[j].split("\t")[0];
        			if (lines[j].indexOf(parts[0]) === 0 && parts[0] != value) {
        				sub_taxonomy.push(lines[j]);
        			}
        		}
        		var final_name = parts[0].replace(/"/g, '');
        		data.push({y:parseInt(parts[2]), color: colors[i], drilldown:{name:final_name, categories:get_categories(sub_taxonomy, depth+1), data:get_data(sub_taxonomy, depth+1, colors)}});
    		}
    	}
    	return data;
    }

    $(".phylogeny-krona-view-btn").click(function() {
    	if ($(":checked[id^=chk_sample_]").size() >= 1) {
	    	$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
	        $("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
			resize_center_btmodal('#ng6modal', 1200);
	        $("#ng6modal").modal();
	    	var samples_select = new Array();
	    	var samples = new Array();
	    	var url_val = $("#krona_file_0").val();
	        $(":checked[id^=chk_sample_]").each(function(){
	            var index = $(this).attr("id").split("_")[2];
	            samples.push("krona_file_"+index);
	            samples_select.push($("#sample_id_"+index).text());
			});
	        var size_samples = samples_select.length;
	    	$("#modal-body-tmpl").html('<div id="krona_container" style="width:1180px; height:700px;"><iframe id ="KronaFile" frameborder="0" src="'+url_val+'" style="width:100%; height:100%;"></iframe></div>');
	    	  $('#KronaFile').load(function(){
	  	    	$('#KronaFile').contents().find("option").each(function() {
	    	    	var found = $.inArray($(this).text(), samples_select) > -1;
	    	    	if (found == false){
	    	    		$(this).hide();
	    	    	}
	    	    });
    	    	$('#KronaFile').contents().find("#datasets").attr("size",size_samples);
    	    	$('#KronaFile').contents().find("#datasets").val("#datasets option:selected").val(samples_select[0]).change();

	    	  });
    	}
    });
});