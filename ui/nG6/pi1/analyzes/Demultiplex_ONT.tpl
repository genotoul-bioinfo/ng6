{*
Copyright (C) 2009 INRA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=params}

{/block}


{block name=results_title} Reports {/block}
{block name=results}

        {* find if user is CTD *}
        <input type="hidden" id="user_login" value="{$user_login}" />
        {if $user_login == "CTD"}
                {assign var="isCTD" value=true}
        {else}
                {assign var="isCTD" value=false}
        {/if}

        {assign var="stats_metrics" value=$analyse_results["stats_metrics"]|@ksort}
        {assign var="stats_headers" value=','|explode:$analyse_results["metrics"].stats_names.headers}
        {assign var="stats_names" value=['format' => 'Format','num_seqs' => 'NB Sequences','sum_len' => 'Total Length','avg_len' => 'Mean Length','min_len' => 'Min Length','max_len' => 'Max Length','N50' => 'N50']}
        {assign var="totalNbSequences" value=0}
        {assign var="totalLength" value=0}
        {assign var="totalAvgLength" value=0}
        {assign var="totalMinLength" value=0}
        {assign var="totalMaxLength" value=0}
        {assign var="totalN50" value=0}
        {assign var="totalNbSequenceswithoutBarcode" value=0}
        {assign var="totalLengthwithoutBarcode" value=0}

        <table id="ont_stats_datatable" class="table table-striped table-bordered dataTable analysis-result-table" style="white-space:nowrap;">
                <thead>
                        <th>Sample</th>
                        {foreach from=$stats_headers key=k item=stat}
                                <th>{$stats_names.$stat}</th>
                        {/foreach}
                </thead>
                <tbody>
                        {foreach from=$stats_metrics key=fastq item=fastq_metrics}
                                {assign var=samplenameVar value="_"|explode:$fastq}
                                <tr><th>{$samplenameVar[1]} - {$samplenameVar[0]}</th>
                                {foreach from=$stats_headers key=k item=stat}
                                        {if $fastq|regex_replace:"/unclassified/":"" !== $fastq}
                                                {if $stat == "num_seqs"}{$totalNbSequenceswithoutBarcode=$totalNbSequenceswithoutBarcode+$fastq_metrics[$stat]|replace:',':''|replace:'.':','}{/if}
                                                {if $stat == "sum_len"}{$totalLengthwithoutBarcode=$totalLengthwithoutBarcode+$fastq_metrics[$stat]|replace:',':''|replace:'.':','}{/if}
                                        {else}
                                                {if $stat == "num_seqs"}{$totalNbSequences=$totalNbSequences+$fastq_metrics[$stat]|replace:',':''|replace:'.':','}{/if}
                                                {if $stat == "sum_len"}{$totalLength=$totalLength+$fastq_metrics[$stat]|replace:',':''|replace:'.':','}{/if}
                                                {if $stat == "avg_len"}{$totalAvgLength=$totalAvgLength+$fastq_metrics[$stat]|replace:',':''|replace:'.':','}{/if}
                                                {if $stat == "min_len"}{$totalMinLength=$totalMinLength+$fastq_metrics[$stat]|replace:',':''|replace:'.':','}{/if}
                                                {if $stat == "max_len"}{$totalMaxLength=$totalMaxLength+$fastq_metrics[$stat]|replace:',':''|replace:'.':','}{/if}
                                                {if $stat == "N50"}{$totalN50=$totalN50+$fastq_metrics[$stat]|replace:',':''|replace:'.':','}{/if}
                                        {/if}
                                                {if $stat == "format"}
                                                        <td>{$fastq_metrics[$stat]}</td>
                                                {else}
                                                        <td>{$fastq_metrics[$stat]|replace:',':''|replace:'.':','|number_format:0:' ':' '}</td>
                                                {/if}
                                {/foreach}
                                </tr>
                        {/foreach}
                </tbody>
                {if $analyse_results|@count > 1 }
                <tfoot>
                        <tr>
                                <th></th>
                                <th>Total Classified</th>
                                <th>{$totalNbSequences|number_format:0:' ':' '}</th>
                                <th>{$totalLength|number_format:0:' ':' '}</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                        </tr>
                        <tr>
                                <th></th>
                                <th>Mean Classified</th>
                                <th>{($totalNbSequences/($stats_metrics|@count))|number_format:0:' ':' '}</th>
                                <th>{($totalLength/($stats_metrics|@count))|number_format:0:' ':' '}</th>
                                <th>{($totalAvgLength/($stats_metrics|@count))|number_format:0:' ':' '}</th>
                                <th>{($totalMinLength/($stats_metrics|@count))|number_format:0:' ':' '}</th>
                                <th>{($totalMaxLength/($stats_metrics|@count))|number_format:0:' ':' '}</th>
                                <th>{($totalN50/($stats_metrics|@count))|number_format:0:' ':' '}</th>
                        </tr>
                        <tr>
                                <th></th>
                                <th>Total Unclassified</th>
                                <th>{$totalNbSequenceswithoutBarcode|number_format:0:' ':' '} ({(($totalNbSequenceswithoutBarcode/($totalNbSequences+$totalNbSequenceswithoutBarcode))*100)|number_format:0}%)</th>
                                <th>{$totalLengthwithoutBarcode|number_format:0:' ':' '} ({(($totalLengthwithoutBarcode/($totalLength+$totalLengthwithoutBarcode))*100)|number_format:1}%)</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                        </tr>
                </tfoot>

                {/if}
        </table>

{* Help block *}
        <div class="tx-nG6-pi1-help">
                <img src="" alt="" class="img" />
                <p>Help for ONT metrics report :</p>
                <span class="meta">
                        <ul>
                                <li><strong>Format</strong> :
                                        The format of the sample file.
                                </li>
                                <li><strong>NB Sequences</strong> :
                                        The total number of reads/sequences for this sample.
                                </li>
                                <li><strong>Total Length</strong> :
                                        The number of bases for this sample.
                                </li>
                                <li><strong>Mean Length</strong> :
                                        The mean sequence length for this sample (bases).
                                </li>
                                <li><strong>Min Length</strong> :
                                        The minimum sequence length for this sample (bases).
                                </li>
                                <li><strong>Max Length</strong> :
                                        The maximum sequence length for this sample (bases).
                                </li>
                                <li><strong>N50</strong> :
                                        50% of all bases come from reads/sequences longer than this value.
                                <li><strong>Total/Mean Classified</strong> :
                                        Only reads with barcodes are used.
                                <li><strong>Total Unclassified</strong> :
                                        Only reads with no identified barcodes are used.
                        </ul>
                </span>
        </div>
{/block}
