{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=results_title} Mapping results {/block}
{block name=results}
	{assign var="label" value="Pairs"}
	{if $analyse_results|@count > 0}
		{assign var="firstelt" value=array_shift(array_slice($analyse_results, 0, 1))}
		{if 'totalReads'|array_key_exists:$firstelt["default"] }
			{$label="Reads"}
		{/if}
	{/if}
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th><center><input type="checkbox" id="chk_all_sample"></center></th>
				<th>Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
				<th># {$label} analysed</th>
				<th># {$label} with unique best hit</th>
				<th># {$label} with no hit</th>
				<th># {$label} with mutiple hits</th>
				<th>Mapping efficiency</th>
			</tr>
		</thead>
		<tbody>
		{assign var="i" value=0}
		{assign var="total" value=0}
		{assign var="totalUniqueHit" value=0}
		{assign var="totalNoHit" value=0}
		{assign var="totalMultipleHit" value=0}
		{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
		{foreach from=$analyse_results_sorted key=sample item=sample_results}
			{assign var="mappingEfficiency" value=0}
	   		{if $label == "Reads"}
	   			{$total=$total+$sample_results["default"].totalReads}
				{$totalUniqueHit=$totalUniqueHit+$sample_results["default"].nbrReadsBestHit}
				{$totalNoHit=$totalNoHit+$sample_results["default"].nbrReadsNoAlignment}
				{$totalMultipleHit=$totalMultipleHit+$sample_results["default"].nbrReadsNoUniqAlignment}
				{$mappingEfficiency=($sample_results["default"].nbrReadsBestHit/$sample_results["default"].totalReads)*100}
				
	   		{else}
		   		{$total=$total+$sample_results["default"].totalPairs}
				{$totalUniqueHit=$totalUniqueHit+$sample_results["default"].nbrPairsBestHit}
				{$totalNoHit=$totalNoHit+$sample_results["default"].nbrPairsNoAlignment}
				{$totalMultipleHit=$totalMultipleHit+$sample_results["default"].nbrPairsNoUniqAlignment}
				{$mappingEfficiency=($sample_results["default"].nbrPairsBestHit/$sample_results["default"].totalPairs)*100}
			{/if}
			
			{if $mappingEfficiency >= 0.05 }
				{$mappingEfficiency = round($mappingEfficiency,2)}
			{else}
				{$mappingEfficiency = round($mappingEfficiency,3)}
			{/if}
			
		   	<tr>
				<input type="hidden" id="descrip_{$i}" value="{$sample|get_description:$descriptions}"/>
				<td><center><input type="checkbox" id="chk_sample_{$i}" value="sample"/></center></td>
				<td>{$sample|get_description:$descriptions}</td>
			   	{if $label=="Reads"}
					<td>{$sample_results["default"].totalReads|number_format:0:' ':' '}</td>
					<td>{$sample_results["default"].nbrReadsBestHit|number_format:0:' ':' '}</td>
					<td>{$sample_results["default"].nbrReadsNoAlignment|number_format:0:' ':' '}</td>
					<td>{$sample_results["default"].nbrReadsNoUniqAlignment|number_format:0:' ':' '}</td>
					<input type="hidden" id="nb_unique_{$i}" value="{$sample_results["default"].nbrReadsBestHit}"/>
					<input type="hidden" id="nb_no_hit_{$i}" value="{$sample_results["default"].nbrReadsNoAlignment}"/>
					<input type="hidden" id="nb_multiple_{$i}" value="{$sample_results["default"].nbrReadsNoUniqAlignment}"/>
				{else}
					<td>{$sample_results["default"].totalPairs|number_format:0:' ':' '}</td>
					<td>{$sample_results["default"].nbrPairsBestHit|number_format:0:' ':' '}</td>
					<td>{$sample_results["default"].nbrPairsNoAlignment|number_format:0:' ':' '}</td>
					<td>{$sample_results["default"].nbrPairsNoUniqAlignment|number_format:0:' ':' '}</td>
					<input type="hidden" id="nb_unique_{$i}" value="{$sample_results["default"].nbrPairsBestHit}"/>
					<input type="hidden" id="nb_no_hit_{$i}" value="{$sample_results["default"].nbrPairsNoAlignment}"/>
					<input type="hidden" id="nb_multiple_{$i}" value="{$sample_results["default"].nbrPairsNoUniqAlignment}"/>
				{/if}
				<td>{$mappingEfficiency}%</td>
		   	</tr>
		   	{$i=$i+1}
	   	{/foreach}
	   	</tbody>
	   	<input type="hidden" id="label" value="{$label}"/>
		{if $analyse_results|@count > 1 }
	   	<tfoot>
			<tr>
				<th></th>
				<th>Total</th>
				<th>{$total|number_format:0:' ':' '}</th>
				<th>{$totalUniqueHit|number_format:0:' ':' '}</th>
				<th>{$totalNoHit|number_format:0:' ':' '}</th>
				<th>{$totalMultipleHit|number_format:0:' ':' '}</th>
				<th>NA</th>
			</tr>
			<tr>
				<th></th>
				<th>Mean</th>
				<th>{($total/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{($totalUniqueHit/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{($totalNoHit/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{($totalMultipleHit/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>NA</th>
			</tr>
			<tr>
				<th align="left" colspan="7">
					With selection :
					<button type="button" class="btn btn-default multiple-selection-btn aln-view-btn" id="aln-view-btn"><i class="glyphicon glyphicon-signal"></i> alignment histogram</button>
				</th>
			</tr>
		</tfoot>
		{else}
		<tfoot>
			<tr>
				<th align="left" colspan="7">
					With selection :
					<button type="button" class="btn btn-default multiple-selection-btn aln-view-btn" id="aln-view-btn"><i class="glyphicon glyphicon-signal"></i> alignment histogram</button>
				</th>
			</tr>
		</tfoot>
		{/if}
	</table>
{/block}