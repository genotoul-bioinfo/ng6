{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=results_title} Contamination Results {/block}

{block name=results}
{assign var="template_version" value="new" scope="global"}
{assign var="samples" value=($analyse_results|@array_keys)}
{if $analyse_results|@count && $analyse_results[$samples[0]]["total"] }
	{$template_version = "old"}
{/if}
{if $template_version == "new" }  {* New template (krona) *}
<table class="table table-striped table-bordered dataTable analysis-result-table">
	<thead>
		<tr>
			<th><input type="checkbox" id="chk_all_sample"></th>
			<th class="string-sort">Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
			<th class="numeric-sort">Number of sequences in the subset</th>
		</tr>
	</thead>
	<tbody>
		{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
		{assign var="i" value=0}
		{foreach from=$analyse_results_sorted key=sample item=sample_results}
			{$sample_name=$sample}
		   	<tr>
		   		<td>
					<input type="checkbox" id="chk_sample_{$i}" value="sample">
					<input type="hidden" id="krona_file_{$i}" value="{$sample_results['default'].html}"/>
				</td>
				<td id="sample_id_{$i}">{$sample|get_description:$descriptions}</td>
				<td>{$sample_results["default"].nb_seq_extracted}</td>
			</tr>
			{$i = $i +1}
		{/foreach}
	</tbody>
	<tfoot>
		<tr>
			<th align="left" colspan="3"> 
				With selection :
				<button type="button" class="btn btn-default single-selection-btn krona-view-btn"><i class="glyphicon glyphicon-search"></i> Krona view</button>			
			</th>
		</tr>
	</tfoot>
</table>
{else} {* Old template (alignment against n database) *}
{assign var="database" value=array()}
{foreach from=$analyse_results key=sample item=sample_results}
	{foreach from=$sample_results key=group item=value}
		{if !in_array($group, $database) }
			{$database[]=$group}
		{/if}
	{/foreach}
{/foreach}

{$null = sort($database)}
<table class="table table-striped table-bordered dataTable analysis-result-table">
	<thead>
		<tr>
			<th>Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
			{foreach $database as $group}
			{if $group != "total"}
			<th> {$group} </th>
			{/if}
			{/foreach}
			<th> Total </th>
		</tr>
	</thead>
	<tbody>
		{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
		{foreach from=$analyse_results_sorted key=sample item=sample_results}
		<tr>
			<td>{$sample|get_description:$descriptions}</td>
			{foreach $database as $group}
			{if $group != "total"}
			<td> {$sample_results[$group].nb_conta|number_format:0:' ':' '} </td>
			{/if}
			{/foreach}
			<td> {$sample_results["total"].nb_conta|number_format:0:' ':' '} </td>
		</tr>
		{/foreach}
	</tbody>
</table>
{/if}
{/block}

{block name=params}
	{block name=params_content}
	{assign var="template_version" value="new" scope="global"}
	{assign var="samples" value=($analyse_results|@array_keys)}
	{if $analyse_results|@count && $analyse_results[$samples[0]]["total"] }
		{$template_version = "old"}
	{/if}
	{if $template_version == "new" }  {* New template (krona) *}
		{assign var=options value=" "|explode:$analyse.params}
		<ul>
			<li class="parameter">{$options[0]*100}% of the original file was used for this analysis.</li>
			<li class="parameter">The minimum threshold of sequences used is {$options[1]}.</li>
			<li class="parameter">The maximum threshold of sequences used is {$options[2]}.</li>
			<li class="parameter">The database {$options[3]} was used by {$analyse.software} {$analyse.version}.</li>
		</ul>
	{/if}
	{/block}
{/block}
