{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=description_update}
	<br/>
	<div  style="float:right;">
		{if $is_project_admin }
			<button id="add_file" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-plus"></i> add files</button>
		{/if}
	</div>
{/block}

{* No result tab*}
{block name=nav_menu_results}{/block}
{block name=tab_content_results}{/block}