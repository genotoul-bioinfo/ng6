{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=params_title} Cleaning options {/block}
{block name=params_content}
	{assign var="params" value=" "|explode:$analyse.params}
	<ul>
		{if in_array("--clean-length-std", $params)}
		{assign var="index" value=$params|@array_keys:"--std"}
		<li class="parameter">Clean reads shorter than mean length of the reads minus {$params[$index[0]+1]} standard deviation and reads longer than mean length of the reads plus {$params[$index[0]+1]} standard deviation.</li>
		{/if}
		{if in_array("--clean-length-win", $params)}
		{assign var="min_index" value=$params|@array_keys:"--min"}
		{assign var="max_index" value=$params|@array_keys:"--max"}
		<li class="parameter">Clean reads with a length not in between [{$params[$min_index[0]+1]};{$params[$max_index[0]+1]}].</li>
		{/if}
		{if in_array("--clean-ns", $params)}
		{assign var="index" value=$params|@array_keys:"--ns_percent"}
		<li class="parameter">Clean reads with a percentage of Ns higher than {$params[$index[0]+1]}%.</li>
		{/if}
		{if in_array("--clean-complexity-win", $params)}
		{assign var="win_index" value=$params|@array_keys:"--window"}
		{assign var="step_index" value=$params|@array_keys:"--step"}
		{assign var="ratio_index" value=$params|@array_keys:"--complexity"}
		<li class="parameter">Clean complexity based on a sliding window with a size of {$params[$win_index[0]+1]} bp, a step of {$params[$step_index[0]+1]} and {$params[$ratio_index[0]+1]} as complexity/length ratio limit.</li>
		{/if}
		{if in_array("--clean-complexity-full", $params)}
		{assign var="index" value=$params|@array_keys:"--complexity"}
		<li class="parameter">Clean complexity based on the whole sequence with {$params[$index[0]+1]} as complexity/length ratio limit.</li>
		{/if}
		{if in_array("--clean-quality", $params)}
		{assign var="index" value=$params|@array_keys:"--quality-threshold"}
		<li class="parameter">Clean reads if no bases quality has a score higher than {$params[$index[0]+1]}.</li>
		{/if}
		{if in_array("--clean-duplicated-reads", $params) AND in_array("--aggressive", $params)}
		{assign var="index" value=$params|@array_keys:"--duplication_limit"}
		<li class="parameter">
		Clean duplicated reads using {$params[$index[0]+1]} as limit for the difference between reads ends to consider them as duplicate (Keep only 1 read per cluster).</li>
		{elseif in_array("--clean-duplicated-reads", $params)}
		{assign var="index" value=$params|@array_keys:"--duplication_limit"}
		<li class="parameter">Clean duplicated reads using {$params[$index[0]+1]} as limit for the difference between reads ends to consider them as duplicate.</li>
		{/if}
		{if in_array("--clean-pairends", $params)}
		{assign var="border_index" value=$params|@array_keys:"--border-limit"}
		{assign var="match_index" value=$params|@array_keys:"--missmatch"}
		<li class="parameter">Clean pairends reads if the sequence size between the spacer and the read beginning/end is higher than {$params[$border_index[0]+1]} nucleotides or if {$params[$match_index[0]+1]} nucleotides mismatch with the spacer.</li>
		{/if}
	</ul>
{/block}

{block name=results}
	<div id="user_information_dialog" title=""></div>
	{assign var="params" value=" "|explode:$analyse.params}
	{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
	{assign var="has_js_graph" value=false}
	{foreach from=$analyse_results_sorted key=sample item=sample_results}
		{if $sample_results["default"].profile_x AND $sample_results["default"].profile_y}
			{assign var="has_js_graph" value=true}
		{/if}
	{/foreach}
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th><center><input type="checkbox" id="chk_all_sample"></center></th>
				<th class="string-sort">Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
				<th class="numeric-sort">Before cleaning</th>
				<th>After cleaning</th>
				{if in_array("--clean-length-std", $params) OR in_array("--clean-length-win", $params)}
				<th class="numeric-sort">Too short / too long</th>
				{/if}
				{if in_array("--clean-ns", $params)}
				<th class="numeric-sort">Too many Ns</th>
				{/if}
				{if in_array("--clean-complexity-win", $params) OR in_array("--clean-complexity-full", $params)}
				<th class="numeric-sort">Low complexity</th>
				{/if}
				{if in_array("--clean-quality", $params)}
				<th class="numeric-sort">Poor quality</th>
				{/if}
				{if in_array("--clean-duplicated-reads", $params)}
				<th class="numeric-sort">Duplicated</th>
				{/if}
				{if in_array("--clean-pairends", $params)}
				<th class="numeric-sort">Filtered (paired-end)</th>
				{/if}
				<th class="numeric-sort">% deleted</th>
			</tr>
		</thead>
		{assign var="nb_reads_begining" value=0}
		{assign var="nb_reads_end" value=0}
		{assign var="del_length" value=0}
		{assign var="del_ns" value=0}
		{assign var="del_complexity" value=0}
		{assign var="del_quality" value=0}
		{assign var="del_duplicat" value=0}
		{assign var="nb_pe" value=0}
		{assign var="i" value=0}
		<tbody>
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
			{assign var="nb_column" value=5}
			{$sample_name=$sample}
			   	<tr>
					<td><center> <input type="checkbox" id="chk_sample_{$i}" value="sample"></center></td>
					<td id="sample_name_{$i}">{$sample|get_description:$descriptions}</td>	
					{$nb_reads_begining=$nb_reads_begining+$sample_results["default"].nb_reads_begining}
					{$nb_reads_end=$nb_reads_end+$sample_results["default"].nb_reads_end}
					<td>{$sample_results["default"].nb_reads_begining|number_format:0:' ':' '}</td>
					<td>{$sample_results["default"].nb_reads_end|number_format:0:' ':' '}</td>
					{if in_array("--clean-length-std", $params) OR in_array("--clean-length-win", $params)}
					{$del_length=$del_length+$sample_results["default"].del_length}
					<td>{$sample_results["default"].del_length|number_format:0:' ':' '}</td>
					{$nb_column=$nb_column + 1}	
					{/if}
					{if in_array("--clean-ns", $params)}
					{$del_ns=$del_ns+$sample_results["default"].del_ns}
					<td>{$sample_results["default"].del_ns|number_format:0:' ':' '}</td>
					{$nb_column=$nb_column + 1}	
					{/if}
					{if in_array("--clean-complexity-win", $params) OR in_array("--clean-complexity-full", $params)}
					{$del_complexity=$del_complexity+$sample_results["default"].del_complexity}
					<td>{$sample_results["default"].del_complexity|number_format:0:' ':' '}</td>
					{$nb_column=$nb_column + 1}	
					{/if}
					{if in_array("--clean-quality", $params)}
					{$del_quality=$del_quality+$sample_results["default"].del_quality}
					<td>{$sample_results["default"].del_quality|number_format:0:' ':' '}</td>
					{$nb_column=$nb_column + 1}	
					{/if}
					{if in_array("--clean-duplicated-reads", $params)}
					{$del_duplicat=$del_duplicat+$sample_results["default"].del_duplicat}
					<td>{$sample_results["default"].del_duplicat|number_format:0:' ':' '}</td>
					{$nb_column=$nb_column + 1}	
					{/if}
					{if in_array("--clean-pairends", $params)}
					{$nb_pe=$nb_pe+$sample_results["default"].nb_pe}
					<td>{$sample_results["default"].nb_pe|number_format:0:' ':' '}</td>
					{$nb_column=$nb_column + 1}	
					{/if}
					<td>{(100.00-((($sample_results["default"].nb_reads_end)*100)/$sample_results["default"].nb_reads_begining))|number_format:2:'.':' '}</td>
					{if $has_js_graph}
					<input type="hidden" id="profile_x_{$i}" value="{$sample_results["default"].profile_x}"/>
					<input type="hidden" id="profile_y_{$i}" value="{$sample_results["default"].profile_y}"/>
					{else}
					<input type="hidden" id="profile_{$i}" value="{$sample_results["default"].profile}" />
					{/if}
			   	</tr>
			   	{$i = $i +1}
	   		{/foreach}
	   	</tbody>
		<tfoot>
			{if $analyse_results|@count > 1 }
			<tr>
				<th></th>
				<th>Total</th>
				<th>{$nb_reads_begining|number_format:0:' ':' '}</th>
				<th>{$nb_reads_end|number_format:0:' ':' '}</th>
				{if in_array("--clean-length-std", $params) OR in_array("--clean-length-win", $params)}
				<th>{$del_length|number_format:0:' ':' '}</th>
				{/if}
				{if in_array("--clean-ns", $params)}
				<th>{$del_ns|number_format:0:' ':' '}</th>
				{/if}
				{if in_array("--clean-complexity-win", $params) OR in_array("--clean-complexity-full", $params)}
				<th>{$del_complexity|number_format:0:' ':' '}</th>
				{/if}
				{if in_array("--clean-quality", $params)}
				<th>{$del_quality|number_format:0:' ':' '}</th>
				{/if}
				{if in_array("--clean-duplicated-reads", $params)}
				<th>{$del_duplicat|number_format:0:' ':' '}</th>
				{/if}
				{if in_array("--clean-pairends", $params)}
				<th>{$nb_pe|number_format:0:' ':' '}</th>
				{/if}
				<th>/</th>
			</tr>
			<tr>
				<th></th>
				<th>Mean</th>
				<th>{($nb_reads_begining/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{($nb_reads_end/($analyse_results|@count))|number_format:0:' ':' '}</th>
				{if in_array("--clean-length-std", $params) OR in_array("--clean-length-win", $params)}
				<th>{($del_length/($analyse_results|@count))|number_format:0:' ':' '}</th>
				{/if}
				{if in_array("--clean-ns", $params)}
				<th>{($del_ns/($analyse_results|@count))|number_format:0:' ':' '}</th>
				{/if}
				{if in_array("--clean-complexity-win", $params) OR in_array("--clean-complexity-full", $params)}
				<th>{($del_complexity/($analyse_results|@count))|number_format:0:' ':' '}</th>
				{/if}
				{if in_array("--clean-quality", $params)}
				<th>{($del_quality/($analyse_results|@count))|number_format:0:' ':' '}</th>
				{/if}
				{if in_array("--clean-duplicated-reads", $params)}
				<th>{($del_duplicat/($analyse_results|@count))|number_format:0:' ':' '}</th>
				{/if}
				{if in_array("--clean-pairends", $params)}
				<th>{($nb_pe/($analyse_results|@count))|number_format:0:' ':' '}</th>
				{/if}
				<th>/</th>
			</tr>
			{/if}
			<tr>
				<th align="left" colspan="{$nb_column}">
					With selection : 
					{if $has_js_graph}
					<button type="button" class="btn btn-default btn-sm js-duplication-profile-btn multiple-selection-btn"><i class="glyphicon glyphicon-signal"></i> duplication profile</button>
					{else}
					<button type="button" class="btn btn-default btn-sm duplication-profile-btn single-selection-btn"><i class="glyphicon glyphicon-signal"></i> duplication profile</button>
					{/if}
				</th>
			</tr>
		</tfoot>
	</table>
{/block}
