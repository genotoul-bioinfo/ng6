{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{
{block name=params}
	{assign var="form_params" value=";"|explode:$analyse.params}
	<ul>
	{foreach from=$analyse_results key=sample item=sample_results}
	{assign var="sample" value=$sample}
	{/foreach}
	
	{foreach from=$form_params item=params_line}
	<li class="parameter">{$params_line}</li>
	{/foreach}	
	</ul>
{/block}

{block name=results_title} Cutadapt results {/block}
{block name=results}
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th ><center><input type="checkbox" id="chk_all_sample"></center></th>
				<th class="string-sort">Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
				<th class="numeric-sort">Processed reads</th>
				<th class="numeric-sort">Processed base</th>
				<th class="numeric-sort">Trimmed reads</th>
				<th class="numeric-sort">Trimmed bases</th>
				<th class="numeric-sort">Too short reads</th>
				<th class="numeric-sort">Final reads</th>
			</tr>

		</thead>
		<tbody>
			{assign var="i" value=0}
			{assign var="totale" value=0}
			{assign var="total1" value=0}
			{assign var="prct_extend_total" value=0}
			{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
			<tr>
				<td><center>
					<input type="checkbox" id="chk_sample_{$i}" value="sample">
					<input type="hidden" id="size{$i}" value="{$sample_results["default"].size}"/>
					<input type="hidden" id="nb_size{$i}" value="{$sample_results["default"].nb_size}"/>
				</center></td>			
				<td id="sample_id_{$i}">{$sample|get_description:$descriptions}</td>
				<td>{$sample_results["default"].processedread|number_format:0:' ':' '}</td>
				{$totale=$totale+$sample_results["default"].processedread}
				<td>{$sample_results["default"].processedbase|number_format:0:' ':' '}</td>
				{assign var="prct_trimmed" value=0}
				{$prct_trimmed=($sample_results["default"].trimmedread/($sample_results["default"].processedread))*100}
				<td>{$sample_results["default"].trimmedread|number_format:0:' ':' '} ({round($prct_trimmed,2)}%)</td>
				{$total1=$total1+$sample_results["default"].trimmedread}
				{assign var="prct_bases_trimmed" value=0}
				{$prct_bases_trimmed=($sample_results["default"].trimmedbase/($sample_results["default"].processedbase))*100}
				<td>{$sample_results["default"].trimmedbase|number_format:0:' ':' '} ({round($prct_bases_trimmed,2)}%)</td>
				<td>{$sample_results["default"].shortread|number_format:0:' ':' '}</td>
				<td>{$sample_results["default"].final_read|number_format:0:' ':' '}</td>
				{$i = $i +1}
			</tr>
			{/foreach}
		</tbody>
		<tfoot>
			<tr>
				<th align="left" colspan="8">
					With selection :
					<button type="button" class="btn btn-default multiple-selection-btn length-view-btn"><i class="glyphicon glyphicon-signal"></i> Length distribution</button>
				</th>
			</tr>
		</tfoot>
	</table>
{/block}
