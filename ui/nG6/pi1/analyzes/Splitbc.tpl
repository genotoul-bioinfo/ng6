{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=params_title} Parameters used {/block}
{block name=params_content}
	{assign var="params" value=" "|explode:$analyse.params}
	{foreach from=$analyse_results key=sample item=sample_results}
	{assign var="sample" value=$sample}
	{/foreach}
	<ul>
		{if in_array("--trim", $params)}
		<li class="parameter">Trim sequences when the barcode is found.</li>
		{/if}
		{if in_array("--mismatches", $params)}
		{assign var="mindex" value=$params|@array_keys:"--mismatches"}
		<li class="parameter">Allow {$params[$mindex[0]+1]} mismatch with the barcode.</li>
		{/if}
		{if in_array("--partial", $params)}
		{assign var="pindex" value=$params|@array_keys:"--partial"}
		<li class="parameter">Allow a trucated barcode of {$params[$pindex[0]+1]} nucleotides.</li>
		{/if}
		{assign var="index" value=$params|@array_keys:"--bcfile"}
		<li class="parameter">With barcode file containing:
			<br /> 
			<div class="file-display">{$analyse_results["barcode_file"]["default"]["barcode_file"]}</div>
		</li>
	</ul>
{/block}

{block name=results_title} Demultiplexing results {/block}
{block name=results}
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th class="string-sort" rowspan="1">Samples </th>
			    <th class="numeric-sort" rowspan="1"> Number of sequences </th>
			</tr>
		</thead>
		<tbody>
			{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
				{if $sample != "barcode_file"}
				    {foreach from=$sample_results["default"] key=type item=value}
        				{if $type == "single"}
	            			
	            			<tr>
                                {if {$sample} == 'total'}
                                    {assign var="total" value=$value}
                                    {$total = $total*2}
                                {else}
	            			        <td> {$sample} </td>
	            			        <td> {$value|number_format:0:' ':' '} </td>
                                {/if}
	            			</tr>
	        			{else}
	            			
	            			<tr>
	            			    {if {$sample} == 'total'}
                                    {assign var="total" value=$value}
                                    {$total = $total*2}
                                {else}
                                    <td> {$sample} (R1+R2)</td>
                			        {assign var="displayval" value=$value}
                			        {$displayval = $displayval*2}
	            			        <td> {$displayval|number_format:0:' ':' '} </td>
                                {/if}
	            			</tr>
	        			{/if}
	    			{/foreach}
				{/if}
			{/foreach}
		</tbody>
		<tfoot>
            {if $type == "single"}
			<tr>
				<th>Ambiguous</th>
				<th>{$sample_results["ambiguous"].single*2|number_format:0:' ':' '}</th>
                
			</tr>
			<tr>
				<th>Unmatched</th>
				<th>{$sample_results["unmatched"].single*2|number_format:0:' ':' '}</th>
                
			</tr>
            {else}
 				<th>Ambiguous</th>
				<th>{$sample_results["ambiguous"].paired*2|number_format:0:' ':' '}</th>
                
			</tr>
			<tr>
				<th>Unmatched</th>
				<th>{$sample_results["unmatched"].paired*2|number_format:0:' ':' '}</th>
                
			</tr>
            {/if}
			<tr>
				<th>Total</th>
				<th>{$total|number_format:0:' ':' '}</th>
            </tr>
			<tr>
				<th>Mean</th>
				<th>{($total/($analyse_results|@count))|number_format:0:' ':' '}</th>
            </tr>
		</tfoot>
	</table>
{/block}
