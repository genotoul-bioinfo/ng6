{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=params_title} Mothur commands and parameters used {/block}
{block name=params_content}
	{assign var="params" value=";"|explode:$analyse.params}
	<ul>
		{foreach from=$params item=param}
		<li class="parameter">{$param}</li>
		{/foreach}
	</ul>
{/block}

{block name=results_title} Classification results {/block}
{block name=results}

	<input type="hidden" id="data_folder" value="{$data_folder}"/>
	<input type="hidden" id="analysis_folder" value="{$data_folder|cat:$analyse.directory}"/>

	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th><center><input type="checkbox" id="chk_all_sample"></center></th>
				<th class="string-sort">Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
				<th class="numeric-sort">Number of sequences</th>
			</tr>
		</thead>
		<tbody>
			{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
			{assign var="i" value=0}
			{assign var="krona_done" value=0}
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
		   	<tr>
				<td><center>
					<input type="checkbox" id="chk_sample_{$i}" value="sample">
					<input type="hidden" class="phylogeny-file" id="phylogeny_file_{$i}" value="{$sample_results['default'].summary}"/>
					{if $sample_results['default'].krona}
						{$krona_done=$krona_done+1}
						<input type="hidden" id="krona_file_{$i}" value="{$sample_results['default'].krona}"/>
					{/if}
				</center></td>
				<td id="sample_id_{$i}">{$sample|get_description:$descriptions}</td>
				<td>{$sample_results["default"].nb_classified_seqs|number_format:0:' ':' '}</td>
		   	</tr>
		   	{$i = $i +1}
		   	{/foreach}
		</tbody>
		<tfoot>
			<tr>
				<th align="left" colspan="10">
					With selection :
					{if $analyse_results|@count > 1 }
						<button type="button" class="btn btn-default btn-sm phylogeny-bar-view-btn multiple-selection-btn"><i class="glyphicon glyphicon-signal"></i> Bar view</button>
						<button type="button" class="btn btn-default btn-sm phylogeny-stackedbar-view-btn multiple-selection-btn"><i class="glyphicon glyphicon-signal"></i> Stacked Bar view</button>
						{if $krona_done>0}
						<button type="button" class="btn btn-default btn-sm phylogeny-krona-view-btn multiple-selection-btn"><i class="glyphicon glyphicon-search"></i> Krona view</button>
						{/if}
						<button type="button" class="btn btn-default btn-sm venn-view-btn multiple5-selection-btn"><i class="glyphicon glyphicon-search"></i> Venn view</button>
					{else}
						<button type="button" class="btn btn-default btn-sm phylogeny-bar-view-btn multiple-selection-btn"><i class="glyphicon glyphicon-signal"></i> Bar view</button>
						{if $krona_done>0}
						<button type="button" class="btn btn-default btn-sm phylogeny-krona-view-btn"><i class="glyphicon glyphicon-search"></i> Krona view</button>
						{/if}
					{/if}
				</th>
			</tr>
		</tfoot>
	</table>
{/block}
