/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */

$(function () {
	
    /* 
    * Define functions in charge to visualize venn charts
    */
	$(".length-view-btn").click(function() {
		
		var nb_step = 30;
		
    	if ($(":checked[id^=chk_sample_]").size() > 0) {
			//Set dialog window
			$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
    		$("#modal-body-tmpl").html('<div id="highcharts_container"></div>');
    		$("#modal-body-tmpl").css('max-height', modal_height(200, 100)+'px');
    		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
    		$("#highcharts_container").css('width', '845px');
    		
    		//Set graph
            var min = 1000000,
            max = 0;
            $(":checked[id^=chk_sample_]").each(function(){
                    var index = $(this).attr("id").split("_")[2];
                    var val_array =	$("#count_clone_"+index).val().split(",");
                    var tmax = Math.max.apply( Math, val_array),
                        tmin = Math.min.apply( Math, val_array);
                    if (min > tmin)	{ min = tmin; }
                    if (max < tmax)	{ max = tmax; }

           	});
           	var step_size = Math.ceil((max-min)/nb_step);
           	
            var x_labels = new Array();
            for( var i=min ; i<=max ; i += step_size ) {
            	x_labels.push( String(i) + "-" + String(i + step_size -1) );
            }
             
            var y_table = new Array();         
            $(":checked[id^=chk_sample_]").each(function(){
            	var index = $(this).attr("id").split("_")[2];
                var index_array = $("#count_clone_"+index).val().split(",") ;
                var value_array = $("#num_clone_"+index).val().split(",") ;
                
                // Init values
                var y_values = new Array();
                for (var i=0; i<x_labels.length; i++) {
                	y_values.push(0);
                }              

                // For each step
                for( var i=0 ; i<x_labels.length ; i++ ) {
                	for( var j=0 ; j<index_array.length ; j++ ) {
                		if( parseInt(index_array[j]) >= (min + (i*step_size)) 
                			&& parseInt(index_array[j]) < (min + (i*step_size) + step_size) ) {
                			y_values[i] += parseInt(value_array[j]);
                		}
                	}
                }

            	y_table.push({
            		name: $("#sample_id_"+index).html(),
            		data: y_values
            	});
            });
	       	
	        var chart = new Highcharts.Chart({
	            chart: {
	               renderTo: 'highcharts_container',
	               defaultSeriesType: 'line'
	            },
	            title: {
	               text: "Distribution of sequence lengths over all sequences.",
	               x: -20 //center
	            },
	            xAxis: {
	               categories: x_labels,
			       title: {
			    	   text: "Sequence length (bp)"
				   },
				   labels: {
					   rotation: -45,
					   align: 'right'
				   }
	            },
	            yAxis: {
	               title: {
	                  text: "Number of sequences"
	               },
	               min: 0,
	               plotLines: [{
	                  value: 0,
	                  width: 1,
	                  color: '#808080'
	               }]
	            },
	            tooltip: {
	               formatter: function() {
	                         return '<b>'+ this.series.name +'</b><br/>'+
	                         		this.x +'bp : '+ this.y +'sequences';
	               }
	            },
	            credits: { enabled: false },
	            legend: {
	               layout: 'vertical',
	               align: 'right',
	               verticalAlign: 'top',
	               x: -10,
	               y: 100,
	               borderWidth: 0
	            },
	            series: y_table
	         });
			
	        resize_center_btmodal('#ng6modal', chart.chartWidth + 50);
			//Display
			$("#ng6modal").modal();
    	}
	});

});
