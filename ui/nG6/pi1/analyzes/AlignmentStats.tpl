{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=params}
	{assign var="softwares_params" value=";"|explode:$analyse.params}
	<ul>
	{foreach from=$softwares_params item=params_line}
		{assign var="params" value=" "|explode:$params_line}
		{assign var="analyse_type" value=array_shift($params)}
		{if $analyse_type == "cigarline"}
		<h5>CigarlineGraph parameters</h5>
		{elseif $analyse_type == "duplication"}
		<h5>Duplication parameters</h5>
		{/if}
		{foreach from=$params item=param}
			{if $analyse_type == "cigarline"}
				{assign var="split_param" value=' '|explode:$param}
				{if in_array("--readsplit", $split_param)}
					<li class="parameter">Build one cigarlineGraph by read (--readsplit).</li>
				{/if}
			{elseif $analyse_type == "duplication"}
				{assign var="split_param" value='='|explode:$param}
				{if $split_param[0] == "VALIDATION_STRINGENCY"}
					<li class="parameter">Validation stringency for all SAM files are {$split_param[1]} ({$split_param[0]}={$split_param[1]}).</li>
				{elseif $split_param[0] == "READ_NAME_REGEX"}
					<li class="parameter">Regular expression that can be used to extract three variables : tile/region, x coordinate and y coordinate ({$split_param[0]}={$split_param[1]}). These values are used to estimate the rate of optical duplication.</li>
				{elseif $split_param[0] == "OPTICAL_DUPLICATE_PIXEL_DISTANCE"}
					<li class="parameter">The maximum offset between two duplicate clusters in order to consider them optical duplicates is {$split_param[1]} ({$split_param[0]}={$split_param[1]}).</li>
				{/if}
			{/if}
		{/foreach}	
	{/foreach}
	</ul>
{/block}

{block name=results_title} Reads alignment statistics {/block}
{block name=results}
	<div id="user_information_dialog" title=""></div>
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th rowspan="2" style="vertical-align:Middle"><center><input type="checkbox" id="chk_all_sample"></center></th>
				<th rowspan="2" class="string-sort" style="vertical-align:Middle">Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
				<th colspan="3">Reads</th>
				<th colspan="6">Alignment</th>
				<th colspan="3">Duplication</th>
			</tr>
			<tr>
				<th class="numeric-sort" style="vertical-align:Middle"><center>Paired</center></th>
				<th class="numeric-sort" style="vertical-align:Middle"><center>Read1</center></th>
				<th class="numeric-sort" style="vertical-align:Middle"><center>Read2</center></th>
				<th class="numeric-sort" style="vertical-align:Middle"><center>Mapped</center></th>
				<th class="numeric-sort" style="vertical-align:Middle"><center>Properly paired</center></th>
	 			<th class="numeric-sort" style="vertical-align:Middle"><center>With itself and mate mapped</center></th>
				<th class="numeric-sort" style="vertical-align:Middle"><center>Singletons</center></th>
	  			<th class="numeric-sort" style="vertical-align:Middle"><center>Mate mapped on a different chr</center></th>
	  			<th class="numeric-sort" style="vertical-align:Middle"><center>Supplementary</center></th>
				<th class="numeric-sort" style="vertical-align:Middle"><center>Nb read duplicated</center></th>
				<th class="numeric-sort" style="vertical-align:Middle"><center>Read pair duplicates</center></th>
				<th class="numeric-sort" style="vertical-align:Middle"><center>Read pair optical duplicates</center></th>
				
			</tr>
		</thead>
		<tbody>
			{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
			{assign var="i" value=0}
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
				{$sample_name=$sample}
				   	<tr>
				   		<td>
							<center><input type="checkbox" id="chk_sample_{$i}"></center>
							{if !isset($sample_results["read1"]) && !isset($sample_results["all"])}
								<input type="hidden" id="cigarlinegraph_img_{$i}" value="{$sample_results["default"].cigarline}">
							{else}
								{assign var="reads" value="all"}
								{if isset($sample_results["read1"])}
									{assign var="reads" value="read1" }
								{/if}
								{if isset($sample_results["read2"])}
									{append var="reads" value="read2"}
								{/if}
								{foreach from=$reads item=read}
								<input type="hidden" id="match_{$read}_{$i}" value="{$sample_results[$read].match}">
								<input type="hidden" id="mismatch_{$read}_{$i}" value="{$sample_results[$read].mismatch}">
								<input type="hidden" id="clipping_{$read}_{$i}" value="{$sample_results[$read].clipping}">
								<input type="hidden" id="insertion_{$read}_{$i}" value="{$sample_results[$read].insertion}">
								{/foreach}
							{/if}
						</td>
						
						<td>{$sample|get_description:$descriptions}</td>
						<td>{$sample_results["default"].paired|number_format:0:' ':' '}</td>
						<td>{$sample_results["default"].read1|number_format:0:' ':' '}</td>
						<td>{$sample_results["default"].read2|number_format:0:' ':' '}</td>
						{assign var="mapped" value=" "|explode:$sample_results["default"].mapped}
						{assign var="properlypaired" value=" "|explode:$sample_results["default"].properlypaired}
						{assign var="singletons" value=" "|explode:$sample_results["default"].singletons}
						<td>{$mapped[0]|number_format:0:' ':' '} {$mapped[1]}{$mapped[2]}</td>
						<td>{$properlypaired[0]|number_format:0:' ':' '} {$properlypaired[1]}{$properlypaired[2]}</td>
						<td>{$sample_results["default"].matemapped|number_format:0:' ':' '}</td>
						<td>{$singletons[0]|number_format:0:' ':' '} {$singletons[1]}{$singletons[2]}</td>
						<td>{$sample_results["default"].mapch1|number_format:0:' ':' '}</td>
						<td>{$sample_results["default"].supplementary|number_format:0:' ':' '}</td>
						{if !isset($sample_results["default"]["pairOpticalDuplicates"]) }
						<td>-</td>
						<td>-</td>
						<td>-</td>
						{else}
						<td>{($sample_results["default"].pairDuplicates*2 + $sample_results["default"].unpairDuplicates)|number_format:0:' ':' '} ({$sample_results["default"].percentDuplication*100}%)</td>
						<td>{$sample_results["default"].pairDuplicates|number_format:0:' ':' '}</td>
						<td>{$sample_results["default"].pairOpticalDuplicates|number_format:0:' ':' '}</td>
						{/if}
					</tr>
				{$i = $i +1}
		   	{/foreach}
	   	</tbody>
	   	<tfoot>
	   		<tr>
				<th align="left" colspan="13">
						<div id="buttons_cigargraph">With selection :
							{if !isset($sample_results["read1"]) && !isset($sample_results["all"])}
							<button type="button" class="btn btn-default single-selection-btn" id="cigarlinegraph-view-btn"><i class="glyphicon glyphicon-picture"></i> CigarlineGraph</button>
							{elseif !isset($sample_results["read2"]) || isset($sample_results["all"])}
							<button type="button" class="btn btn-default single-selection-btn" id="merge-view-btn"><i class="glyphicon glyphicon-signal"></i> CigarlineGraph</button>
							{else}
							<button type="button" class="btn btn-default single-selection-btn" id="read1-view-btn"><i class="glyphicon glyphicon-signal"></i> Read1 CigarlineGraph</button>
							<button type="button" class="btn btn-default single-selection-btn" id="read2-view-btn"><i class="glyphicon glyphicon-signal"></i> Read2 CigarlineGraph</button>
							{/if}
						</div>
				</th>
			</tr>
	   	</tfoot>
	</table>

{/block}
