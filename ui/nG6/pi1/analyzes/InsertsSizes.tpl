{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=params_title} Parameters used {/block}
{block name=params_content}
	{foreach from=$analyse_results key=sample item=sample_results}
	{assign var="sample" value=$sample}
	{/foreach}
	<ul>
	{foreach from=" "|explode:$analyse.params item=param}
		{assign var="split_param" value='='|explode:$param}
		{if $split_param[0] == "HISTOGRAM_WIDTH"}
		<li class="parameter">The inserts with length <= {$split_param[1]}bp are used in mean and standard deviation calculation and in the graph ({$split_param[0]}={$split_param[1]}).</li>
		{elseif $split_param[0] == "MINIMUM_PCT"}
		<li class="parameter">Discard any data categories (out of FR, TANDEM, RF) that have fewer than {$split_param[1]*100}% of overall reads ({$split_param[0]}={$split_param[1]}).</li>
		{elseif $split_param[0] == "VALIDATION_STRINGENCY"}
		<li class="parameter">Validation stringency for all SAM files are {$split_param[1]} ({$split_param[0]}={$split_param[1]}).</li>
		{/if}
	{/foreach}
	</ul>
{/block}

{block name=results_title} Inserts sizes {/block}
{block name=results}
	{assign var="nb_processed_samples" value=($analyse_results|@count)}
	{assign var="nb_omitted_samples" value=0}
	
	{if isset($analyse_results['analysis'])}
		{$nb_processed_samples = $nb_processed_samples - 1}
		{if $analyse_results['analysis']['default'].nb_omitted_samples > 0}
			{$nb_omitted_samples = $analyse_results['analysis']['default'].nb_omitted_samples}
			<p class="information"><b>{$nb_omitted_samples} samples have been omitted</b> in this analysis because they have 0 reads properly paired.</p>
		{/if}
	{/if}
	
	{assign var="has_several_samples" value=($nb_processed_samples + $nb_omitted_samples > 1)}
	
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th><center><input type="checkbox" id="chk_all_sample"></center></th>
				{if $has_several_samples && ($nb_processed_samples >=1)}
				<th class="string-sort">Samples ({$nb_processed_samples})</th>
				{/if}
				<th class="numeric-sort">Pair orientation</th>
				<th class="numeric-sort">Median size</th>
				<th class="numeric-sort">Mean size</th>
				<th class="numeric-sort">Min size</th>
				<th class="numeric-sort">Max size</th>
				<th class="numeric-sort">Standard deviation</th>
				<th class="numeric-sort">Number of pair</th>
				<th class="numeric-sort">Percentage of all pairs</th>
			</tr>
		</thead>
		<tbody>
			{assign var="i" value=0}
			{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
				{foreach from=$sample_results key=rgroup item=rgroup}
				    {if $rgroup != default}
					<tr>
						<td><center>
							<input type="checkbox" id="chk_sample_{$i}" value="sample"/>
							<input type="hidden" id="inserts_sizes_{$i}" value="{$sample_results["default"].inserts_sizes}"/>
							<input type="hidden" id="nb_inserts_sizes_{$i}" value="{$sample_results[$rgroup].nb_inserts_sizes}"/>
						</center></td>			
						{if $has_several_samples && ($nb_processed_samples >=1)}
						<td id="sample_id_{$i}">{$sample|get_description:$descriptions}</td>
						{/if}
						<td id="orientation_id_{$i}"><center>{$rgroup}</center></td>
						<td>{$sample_results[$rgroup].median_size|number_format:2:'.':' '}</td>
						<td>{$sample_results[$rgroup].mean_size|number_format:2:'.':' '}</td>
						<td>{$sample_results[$rgroup].min_size|number_format:0:'.':' '}</td>
						<td>{$sample_results[$rgroup].max_size|number_format:0:'.':' '}</td>
						<td>{$sample_results[$rgroup].std_deviation|number_format:2:'.':' '}</td>
						<td>{$sample_results[$rgroup].nb_pair_used|number_format:0:'.':' '}</td>
						{assign var="prct_all_pairs" value=($sample_results[$rgroup].nb_pair_used/$sample_results["default"].nb_pair_in_file)*100}
						<td>{$prct_all_pairs|number_format:2:'.':' '}</td>
						{$i = $i +1}
					</tr>
					{/if}
				{/foreach}
			{/foreach}
		</tbody>
		<tfoot>
			<tr>
				<th align="left" colspan="{if $has_several_samples && ($nb_processed_samples >=1)}10{else}9{/if}">
					With selection :
					<button type="button" class="btn btn-default multiple-selection-btn length-view-btn"><i class="glyphicon glyphicon-signal"></i> Length distribution</button>
				</th>
			</tr>
		</tfoot>
	</table>
{/block}
