{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=params}
	{assign var="params" value=" "|explode:$analyse.params}
	<ul>
		{if in_array("-m", $params)}
		{assign var="mindex" value=$params|@array_keys:"-m"}
		<li class="parameter">{$params[$mindex[0]+1]} as minimum depth of coverage required to create a stack.</li>
		{/if}
		{if in_array("-M", $params)}
		{assign var="m2index" value=$params|@array_keys:"-M"}
		<li class="parameter">{$params[$m2index[0]+1]}bp as maximum distance allowed between stacks.</li>
		{/if}
		{if in_array("-N", $params)}
		{assign var="xindex" value=$params|@array_keys:"-N"}
		<li class="parameter">{$params[$xindex[0]+1]} as maximum distance allowed to align secondary reads to primary stack.</li>
		{/if}
	</ul>
{/block}

{block name=results_title} Clustering results {/block}
{block name=results}
	On {$analyse_results["all"]["default"].nb_tot} total reads only {$analyse_results["all"]["default"].nb_used} have been used for clustering
	<br/><br/>
	<h5>Statistics on clusters</h5>
	<table class="table table-striped table-bordered dataTable">
		<thead>
			<tr>
				<th><input type="checkbox" id="chk_all_sample"></th>
				<th class="numeric-sort">Nb. clusters</th>
				<th class="numeric-sort">Nb. whitelisted clusters</th>
				<th class="numeric-sort">Nb. min clusters</th>
				<th class="numeric-sort">Nb. max clusters</th>
				<th class="numeric-sort">Nb. mean clusters</th>
				<th class="numeric-sort">Nb. median clusters</th>
				<th class="numeric-sort">Standart deviation</th>
				<th class="numeric-sort">Depth mean (min-max)</th>
			</tr>
		</thead>
		<tbody>
			{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
			{assign var="i" value=0}
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
				<tr>
					<td>
						<input type="checkbox" id="chk_sample_{$i}">
						<input type="hidden" id="name_{$i}"    value="{$sample}">
						<input type="hidden" id="min_{$i}"     value="{$sample_results["default"].min_cons}">
						<input type="hidden" id="max_{$i}"     value="{$sample_results["default"].max_cons}">
						<input type="hidden" id="median_{$i}"  value="{$sample_results["default"].median_cons}">
						<input type="hidden" id="quart25_{$i}" value="{$sample_results["default"].quart25_cons}">
						<input type="hidden" id="quart75_{$i}" value="{$sample_results["default"].quart75_cons}">
					</td>
					<td>{$analyse_results["all"]["default"].nb_cons_tot|number_format:0:' ':' '}</td>
					<td>{$analyse_results["all"]["default"].nb_cons_white_tot|number_format:0:' ':' '}</td>
    				<td>{$analyse_results["all"]["default"].min_cons|number_format:0:' ':' '}</td>
    				<td>{$analyse_results["all"]["default"].max_cons|number_format:0:' ':' '}</td>
    				<td>{$analyse_results["all"]["default"].mean_cons|number_format:0:' ':' '}</td>
    				<td>{$analyse_results["all"]["default"].median_cons|number_format:0:' ':' '}</td>
    				<td>{$analyse_results["all"]["default"].std_cons|number_format:0:' ':' '}</td>
    				<td>
    					{$analyse_results["all"]["default"].mean_cov}
    					({$analyse_results["all"]["default"].min_cov} - {$analyse_results["all"]["default"].max_cov})
    				</td>
    			</tr>
    			{$i = $i +1}
		   	{/foreach}
		</tbody>
		<tfoot>
	   		<tr>
				<th align="left" colspan="9">
					With selection :
					<button type="button" class="btn btn-default multiple-selection-btn cluster-view-btn">
						<i class="glyphicon glyphicon-signal"></i> Number of clusters
					</button>
				</th>
			</tr>
	   	</tfoot>
	</table>

	</br>
	<h5>Statistics on SNPs for whitelisted clusters</h5>
	<table class="table table-striped table-bordered dataTable">
		<thead>
			<tr>
				<th>Nb. clusters with SNPs (with one - two - three SNPs)</th>
				<th>Nb. of SNPs</th>
				<th>Mean nb. of SNPs</th>
				<th>Depth mean</th>
				<th>Depth median</th>
			</tr>
		</thead>
		<tbody>
			{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
			{assign var="i" value=0}
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
				<tr>
					<td>
						{$analyse_results["all"]["default"].snp_cluster_all|number_format:0:' ':' '}
						({$analyse_results["all"]["default"].cluster1_all|number_format:0:' ':' '} -
						 {$analyse_results["all"]["default"].cluster2_all|number_format:0:' ':' '} -
						 {$analyse_results["all"]["default"].cluster3_all|number_format:0:' ':' '})
					</td>
					<td>{$analyse_results["all"]["default"].nb_snps_all|number_format:0:' ':' '}</td>
					<td>{($analyse_results["all"]["default"].nb_snps_all/$analyse_results["all"]["default"].snp_cluster_all)|number_format:2:'.':' '}</td>
					<td>{$analyse_results["all"]["default"].meansnp_cov|number_format:0:' ':' '}</td>
    				<td>{$analyse_results["all"]["default"].mediansnp_cov|number_format:0:' ':' '}</td>
    			</tr>
    			{$i = $i +1}
		   	{/foreach}
		</tbody>
	</table>
{/block}
