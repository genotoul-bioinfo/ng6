/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */

$(function () {
		
    $('a[data-toggle="tab"]').on('shown', function () {
        if ($(this).attr("href") == "#contigs") {
        	var oTable = $('#contigs-table').DataTable();
        	oTable.fnDraw();
        	oTable.fnAdjustColumnSizing();
        } else if ($(this).attr("href") == "#isogroups") {
        	var oTable = $('#isogroups-table').DataTable();
        	oTable.fnDraw();
        	oTable.fnAdjustColumnSizing();
        } else if ($(this).attr("href") == "#isotigs") {
        	var oTable = $('#isotigs-table').DataTable();
        	oTable.fnDraw();
        	oTable.fnAdjustColumnSizing();
        } else if ($(this).attr("href") == "#ace") {
        	var oTable = $('#ace-table').DataTable();
        	oTable.fnDraw();
        	oTable.fnAdjustColumnSizing();
        }
    });
	
	$(".js-depth-btn").click(function() {
		$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
		$("#modal-body-tmpl").html('<div id="highcharts_container"></div>');
		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
		resize_center_btmodal('#ng6modal', 750);
       	var series = new Array();
   		$(":checked[id^=chk_sample_]").each(function(){
			var index = $(this).attr("id").split("_")[2],
				sample = $("#sample_name_"+index).html(),
				indexes = $("#depth_x_"+index).val().split(","),
				values = $("#depth_y_"+index).val().split(","),
				values_to_display = new Array();
			for (var i=0; i<indexes.length; i++) {
				values_to_display.push(new Array(parseInt(indexes[i]), parseInt(values[i])));
			}
			series.push({   
            	name: sample,
                data: values_to_display
            });
		});
   		
		var depth = new Highcharts.Chart({
			chart: {
				renderTo: 'highcharts_container',
				type: 'scatter',
				zoomType: 'xy'
			},
			title: { text: "Contigs depth distribution" },
			xAxis: {
				min: 0,
				title: {
					enabled: true,
					text: 'Sum of Reads length'
				},
				startOnTick: true,
				endOnTick: true,
				showLastLabel: true
			},
			credits: { enabled: false },
			yAxis: {
				min: 0,
				title: { text: 'Contigs length'	}
			},
			tooltip: {
				formatter: function() {
					var depth = parseInt(this.x) / parseInt(this.y); 
					return 'Depth: <b>' + depth + ' x </b> <br />Reads sum: ' + this.x + " bp <br /> Length: " + this.y + " bp";
				}
			},
			plotOptions: {
				scatter: {
					states: {
						hover: {
							marker: {
								enabled: false
							}
						}
					}
				},
				series: {fillOpacity: 0.5}
			},
			series: series
		});
       	chart.setSize(720, 450);
       	$("#ng6modal").modal();
	});

	$(".js-length-btn").click(function() {
		$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
		$("#modal-body-tmpl").html('<div id="highcharts_container"></div>');
		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
		resize_center_btmodal('#ng6modal', 750);
       	var index = 0;
       	$(":checked[id^=chk_sample_]").each(function() {
       		index = $(this).attr("id").split("_")[2];
       	});
       	var sample = $("#sample_name_"+index).html(),
       		parseInt10 = function(val) { return parseInt(val,10) },
       		categories = $("#length_x_"+index).val().split(",").map(parseInt10),
			series = new Array({data: $("#length_y_"+index).val().split(",").map(parseInt10)});
       	
   		chart = new Highcharts.Chart({
            chart: {
                renderTo: 'highcharts_container',
                type: 'column'
            },
            title: {
                text: 'Contigs length distribution'
            },
            credits: { enabled: false },
            xAxis: {
                categories: categories,
                title: {
                    text: 'Contigs length'
                },
                labels: {
                    rotation: -45,
                    align: 'right'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Number of contigs'
                }
            },
            legend: { enabled: false },
            tooltip: {
                formatter: function() {
                	if (this.y > 1) {
                		return this.y +' contigs';
                	} else {
                		return this.y +' contig';
                	}
                }
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: series
        });
       	chart.setSize(720, 450);
       	$("#ng6modal").modal();
	});

	$(".depth-btn").click(function() {
		if ($(":checked[id^=chk_sample_]").size() == 1) {
            var path = $("#depth_" + $(":checked[id^=chk_sample_]").attr("id").split("_")[2]).val();            
    		$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
    		$("#modal-body-tmpl").html('<div id="img_container"><img src="' + path + '" alt="Cannot display"></div>');
    		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
    		resize_center_btmodal('#ng6modal', 750);
           	$("#ng6modal").modal();
    	}
	});
	
	$(".length-btn").click(function() {
		if ($(":checked[id^=chk_sample_]").size() == 1) {
            var path = $("#length_" + $(":checked[id^=chk_sample_]").attr("id").split("_")[2]).val();            
    		$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
    		$("#modal-body-tmpl").html('<div id="img_container"><img src="' + path + '" alt="Cannot display"></div>');
    		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
    		resize_center_btmodal('#ng6modal', 750);
           	$("#ng6modal").modal();
    	}
	});
	
	
});
