{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=params}{/block}

{block name=results_title} Search results {/block}
{block name=results}
	{* First find out all mid names tested *}
	{assign var="mids" value=array()}
	{foreach from=$analyse_results key=sample item=sample_results}
		{foreach from=$sample_results["default"] key=mid_name item=mid_value}
			{if !in_array($mid_name, $mids) }
				{$mids[]=$mid_name}
			{/if}
		{/foreach}
	{/foreach}
	{assign var="total_mids" value=0}
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th class="string-sort">MID name</th>
				{foreach from=$analyse_results key=sample item=sample_results}
				<th class="numeric-sort">{$sample}</th>
				{/foreach}
		    </tr>
		</thead>
		<tbody>
		    {foreach $mids as $mid_name}
		    <tr>
		    	<td> {$mid_name} </td>
		    	{foreach from=$analyse_results key=sample item=sample_results}
		    	{$total_mids = $total_mids + $sample_results["default"][$mid_name]}
				<td>{$sample_results["default"][$mid_name]|number_format:0:' ':' '}</td>
				{/foreach}
		    </tr>
		    {/foreach}
	    </tbody>
		<tfoot>
			<tr>
				<th>Total</th>
				<th>{$total_mids|number_format:0:' ':' '}</th>
			</tr>
		</tfoot>
	</table>
{/block}