/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */
	
/* 
 * Resize the HTML element.
 * @param HTMLElement - The element to resize.
 * @param float - The percentage of the initial size of the element.
 */
function resize(elt, ratio) {
	elt.width = ratio*elt.width ;
}

$(function () {
    
	/**
	 * Fonction pour l'affichage du nombre de C analysé
 	*/

	
    /* 
    * Define functions in charge to visualize charts
    */
	$("#aln-view-btn").click(function() {
	   	if ($(":checked[id^=chk_sample_]").size() > 0) {
			// Set dialog window
			$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
    		$("#modal-body-tmpl").html('<div id="highcharts_container"></div>');
    		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
    		$("#highcharts_container").css('width', '845px');

    		// Set graph
			var description = new Array();
			var valeur_unique = new Array();
			var valeur_multiple = new Array();
			var valeur_no_hit = new Array();
			var mapp_eff = new Array();
            $(":checked[id^=chk_sample_]").each(function(){
            	var index = $(this).attr("id").split("_")[2] ;
            	
                var index_description = $("#descrip_" + index).val() ;
                description.push(index_description);
                
                var value_unique     = $("#nb_unique_" + index).val() ;
                valeur_unique.push(parseInt(value_unique));
                
                var value_multiple     = $("#nb_multiple_" + index).val() ;
                valeur_multiple.push(parseInt(value_multiple));
                
                var value_no_hit     = $("#nb_no_hit_" + index).val() ;
                valeur_no_hit.push(parseInt(value_no_hit));
            });
	       	
            var options = {
				chart: {
					renderTo: 'highcharts_container', 
					type: 'column',
					margin: [50, 50, 100, 80]
				},
				title: {
					text: $("#label").val()+' hits :'
				},
				xAxis: {
					categories: description,
					labels: {
						rotation: -45,
						align: 'right',
						style: {
							fontSize: '13px',
							fontFamily: 'Verdana, sans-serif'
						}
					}
				},
				yAxis: {
					min: 0,
					title: {
						text: '# '+$("#label").val()
					}
				},
				stackLabels: {
	                enabled: true,
	                style: {
	                	ontSize: '13px',
						fontFamily: 'Verdana, sans-serif'
					}
	            },
		       legend: {
		            align: 'right',
		            x: -30,
		            verticalAlign: 'top',
		            y: 25,
		            floating: true,
		            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
		            borderColor: '#CCC',
		            borderWidth: 1,
		            shadow: false
		        },
		        
				tooltip: {
					formatter: function () {
		                return '<b>' + this.x + '</b><br/>' +
		                    this.series.name + ': ' + this.y + '<br/>' +
		                    '%: ' + this.point.percentage+ '<br/>' +
		                    'Total: ' + this.point.stackTotal  ;
		            }
				},
				plotOptions: {
		            column: {
		                stacking: 'normal',
		                dataLabels: {
		                    enabled: true,
		                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
		                    style: {
		                        textShadow: '0 0 3px black'
		                    }
		                }
		            }
		        },
				series: [{
		            name: 'No hit',
		            data: valeur_no_hit
		        },{
		            name: 'Multiple hit',
		            data: valeur_multiple
		        },{
					name: 'Uniquely aligned',
					data: valeur_unique,
				}, ]
			};
            
            // Draw graph
			var chart = new Highcharts.Chart(options);
			resize_center_btmodal('#ng6modal', chart.chartWidth + 50);
			
			//Display
			$("#ng6modal").modal();
    	}
	});

});



