{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=params}

{/block}


{block name=results_title} Reports {/block}
{block name=results}
	{* find if user is CTD *}
	<input type="hidden" id="user_login" value="{$user_login}" />
	{if $user_login == "CTD"}
		{assign var="isCTD" value=true}
	{else}
		{assign var="isCTD" value=false}
	{/if}
	{assign var="template_status" value=$analyse_results["metrics"].analyse_info.template_status}
	{assign var="metrics" value=$analyse_results["metrics"]}
	{* If it is the version 2.0 of the Runstats analysis *}
	{if $template_status == "2.0"}
		{assign var="sample_name" value=$analyse_results["metrics"].analyse_info.Sample_name}
		{assign var="rawdata" value=$analyse_results[$sample_name].rawdata}
		{assign var='rawdata_headers' value=','|explode:$metrics['rawdata'].headers}
		{assign var='rawdata_count' value=$rawdata_headers|@count}
		{assign var="Q7" value=$analyse_results[$sample_name].Q7}
		{assign var='Q7_headers' value=','|explode:$metrics['Q7'].headers}
		{assign var='Q7_count' value=$Q7_headers|@count}
		{assign var="Q9" value=$analyse_results[$sample_name].Q9}
		{assign var='Q9_headers' value=','|explode:$metrics['Q9'].headers}
		{assign var='Q9_count' value=$Q9_headers|@count}
	{else}
		{assign var="sample_name" value="ONT_sample"}
		{assign var="basic" value=$analyse_results[$sample_name].basic}
		{assign var="quality" value=$analyse_results[$sample_name].quality}
		{assign var='basic_headers' value=','|explode:$metrics['basic'].headers}
		{assign var='quality_headers' value=','|explode:$metrics['quality'].headers}
		{assign var='quality_count' value=$quality_headers|@count}
		{assign var='basic_count' value=$basic_headers|@count}
	{/if}
	
	{assign var="analyse_results_sorted" value=$analyse_results[$sample_name]|@ksort}
	{assign var="plots" value=$analyse_results[$sample_name].plots}
	{assign var="plots_barcode" value=$analyse_results[$sample_name].plots_barcode}
	
	{assign var='plots_headers' value=','|explode:$metrics['plots'].headers}
	{assign var='plots_count' value=$plots_headers|@count}
	
	{assign var='plots_barcode_headers' value=','|explode:$metrics['plots_barcode'].headers}
	{assign var='plots_barcode_count' value=$plots_barcode_headers|@count}
    
	{assign var='barcode_headers' value=','|explode:$metrics['barcode'].headers|@ksort}
	{assign var='barcode_headers_count' value=$barcode_headers|@count}
    {assign var='barcode_name_sample' value=','|explode:$metrics['barcode'].names|@ksort}
    {assign var='barcode_name_count' value=$barcode_name_sample|@count}
    
	<legend>Analyse results report - Sample name : {$sample_name}</legend>
	
	{if $template_status != "2.0"}
		{assign var='data_col' value=2}
		<div class="row">
			{if $metrics['basic']}
				<div class="col-md-4 col-lg-4">
					<table id="ont_table_basic" class="table table-striped table-bordered dataTable">
					<thead><tr><th colspan="2"><h4>Basic metrics report</h4></thead></tr></th>
						<tbody>
						{foreach from=$basic_headers key=k item=head}
							<tr>
								{if ($head == 'median_yield_per_sec')}
									<td><label>{$head|replace:'_':' '}</label></td>
									<td>{($basic.$head)|number_format:2:',':' '}</td>
								{elseif $head == 'nb_actif_channel'}
								{else}
									<td><label>{$head|replace:'_':' '}</label></td>
									<td>{($basic.$head)|number_format:0:',':' '}</td>
								{/if}
							</tr>
							{$data_col = $data_col + 1}
						{/foreach}
						</body>
					</table>
				</div>
			{/if}
			{if $metrics['quality']}
				<div class="col-md-4 col-lg-4">
					<table id="ont_table_quality" class="table table-striped table-bordered dataTable">
						<thead><tr><th colspan="2"><h4>Quality report</h4></thead></tr></th>
					<tbody>
						{foreach from=$quality_headers key=k item=head}
							<tr>
								{if ($head == 'median_read_quality')  or ($head == 'median read quality')}
									<td><label>{$head|replace:'_':' '}</label></td>
									<td>{($quality.$head)|number_format:2:',':' '}</td>
								{elseif ($head == 'N50_read_length_utils') or ($head == 'N50_read_length_useful_data')}
									<td><label>N50 read length useful data</label></td>
									<td>{($quality.$head)|number_format:0:',':' '}
								{elseif $head|strstr:"nb_read"}
									<td><label>{$head|replace:'_':' '|replace:'utils':'useful data'}</label></td>
									<td>{($quality.$head)|number_format:0:',':' '} ({($quality.$head / $basic.nb_reads*100)|number_format:2:',':' '}%)</td>
								{elseif $head|strstr:"total_bases"}
									<td><label>{$head|replace:'_':' '|replace:'utils':'useful data'}</label></td>
									<td>{($quality.$head)|number_format:0:',':' '} ({($quality.$head / $basic.total_bases*100)|number_format:2:',':' '}%)</td>
								{/if}
							</tr>
							{$data_col = $data_col + 1}
						{/foreach}
					</tbody>
					</table>
				</div>
			{/if}
			{if $metrics['plots']}
				<div class="col-md-3 col-lg-3">
					<table id="ont_table_plot" class="table table-striped table-bordered dataTable">
						<thead><tr><th colspan="2"><h4>Plots report</h4></h4></thead></tr></th>
					<tbody>
						{foreach from=$plots_headers key=k item=head}
							<tr>
									<td><label>{$head|replace:'_':' '|replace:'utils':'useful data'}</label></td>
									<td><a class="imglink" href="{$plots.$head}" ><i class="glyphicon glyphicon-picture"></i></a></td>
							</tr>
						{/foreach}
						{if $barcode_name_count > 1 }
						{foreach from=$plots_barcode_headers key=k item=head}
							<tr>
									<td><label>{$head|replace:'_':' '|replace:'utils':'useful data'}</label></td>
									<td><a class="imglink" href="{$plots_barcode.$head}" ><i class="glyphicon glyphicon-picture"></i></a></td>
							</tr>
						{/foreach}
						{/if}
					</tbody>
					</table>
				</div>
			{/if}
		</div>
	
	{* If it is the version 2.0 of the Runstats analysis *}
	{else}
		
		<div class="row">
		<div class="col-md-8 col-lg-8">
			{if $metrics['rawdata'] and $metrics['Q7'] and $metrics['Q7']}
				<table id="ont_table_rawdata" class="table table-striped table-bordered dataTable" style="white-space:nowrap;">
					<thead>
						<tr><th colspan="4"><h4><center>ONT Metrics report</center</h4></th></tr>
						<tr>
							<th colspan="1"><h4>Metrics</h4></th>
							<th colspan="1"><h4>Rawdata</h4></th>
							<th colspan="1"><h4>Q7</h4></th>
							<th colspan="1"><h4>Q9</h4></th>
						</tr>
					</thead>
					<tbody>
						{foreach from=$rawdata_headers key=k item=head}
							{assign var="headQ7" value=$head|cat:"_Q7"}
							{assign var="headQ9" value=$head|cat:"_Q9"}
							{assign var="count_longread" value=1}
							{assign var="corresp_num" value=['1' => 'First', '2' => 'Second', '3' => 'Third', '4' => 'Fourth', '5' => 'Fifth']}
							<tr>
								{if ($head == 'mean_read_quality')}
									<td><label>{$head|replace:'_':' '}</label></td>
									<td>{($rawdata.$head)|number_format:2:',':' '}</td>
									<td>{($Q7.$headQ7)|number_format:2:',':' '}</td>
									<td>{($Q9.$headQ9)|number_format:2:',':' '}</td>
								{elseif ($head == 'nb_reads')}
									<td><label>{$head|replace:'_':' '}</label></td>
									<td>{($rawdata.$head)|number_format:0:',':' '}</td>
									<td>{($Q7.$headQ7)|number_format:0:',':' '} ({($Q7.$headQ7 / $rawdata.nb_reads*100)|number_format:2:',':' '}%)</td>
									<td>{($Q9.$headQ9)|number_format:0:',':' '} ({($Q9.$headQ9 / $rawdata.nb_reads*100)|number_format:2:',':' '}%)</td>
								{elseif ($head == 'total_bases')}
									<td><label>{$head|replace:'_':' '}</label></td>
									<td>{($rawdata.$head)|number_format:0:',':' '}</td>
									<td>{($Q7.$headQ7)|number_format:0:',':' '} ({($Q7.$headQ7 / $rawdata.total_bases*100)|number_format:2:',':' '}%)</td>
									<td>{($Q9.$headQ9)|number_format:0:',':' '} ({($Q9.$headQ9 / $rawdata.total_bases*100)|number_format:2:',':' '}%)</td>
								{elseif ($head|strstr:"longread")}
									{assign var="corresp" value=$head|replace:'longread':''}
									<td><label>{$corresp_num[$corresp]} longest read</label></td>
									<td>{($rawdata.$head)|number_format:0:',':' '}</td>
									<td>{($Q7.$headQ7)|number_format:0:',':' '}</td>
									<td>{($Q9.$headQ9)|number_format:0:',':' '}</td>
								{else}
									<td><label>{$head|replace:'_':' '}</label></td>
									<td>{($rawdata.$head)|number_format:0:',':' '}</td>
									<td>{($Q7.$headQ7)|number_format:0:',':' '}</td>
									<td>{($Q9.$headQ9)|number_format:0:',':' '}</td>
								{/if}
							</tr>
						{/foreach}
					</tbody>
				</table>
			{/if}
		</div>
		<div class="col-md-3 col-lg-3">
			{if $metrics['plots']}
				<table id="ont_table_plot" class="table table-striped table-bordered dataTable">
					<thead><tr><th colspan="2"><h4><center>Plots report</center></h4></h4></thead></tr></th>
					<tbody>
						{foreach from=$plots_headers key=k item=head}
							<tr>
								{if ($head == 'cumulyield')}
									<td><label>Cumulate Yield per second</label></td>
									<td><a class="imglink" href="{$plots.$head}" ><i class="glyphicon glyphicon-picture"></i></a></td>
								{elseif ($head == 'distriblength')}
									<td><label>Length distribution by reads</label></td>
									<td><a class="imglink" href="{$plots.$head}" ><i class="glyphicon glyphicon-picture"></i></a></td>
								{elseif ($head == 'distriblength_bybases')}
									<td><label>Length distribution by bases</label></td>
									<td><a class="imglink" href="{$plots.$head}" ><i class="glyphicon glyphicon-picture"></i></a></td>
								{elseif ($head == 'distribquality')}
									<td><label>Quality distribution</label></td>
									<td><a class="imglink" href="{$plots.$head}" ><i class="glyphicon glyphicon-picture"></i></a></td>
								{/if}
								{if $isCTD == true}
									{if ($head == 'seqrate')}
										<td><label>Sequencing rate over time</label></td>
										<td><a class="imglink" href="{$plots.$head}" ><i class="glyphicon glyphicon-picture"></i></a></td>
									{elseif ($head == 'channelbases')}
										<td><label>FlowCell Channel overview for number of bases</label></td>
										<td><a class="imglink" href="{$plots.$head}" ><i class="glyphicon glyphicon-picture"></i></a></td>
									{elseif ($head == 'channelreads')}
										<td><label>FlowCell Channel overview for number of reads</label></td>
										<td><a class="imglink" href="{$plots.$head}" ><i class="glyphicon glyphicon-picture"></i></a></td>
									{/if}
								{/if}
							</tr>
						{/foreach}
						{if $barcode_name_count > 1 }
						{foreach from=$plots_barcode_headers key=k item=head}
							<tr>
									<td><label>{$head|replace:'_':' '}</label></td>
									<td><a class="imglink" href="{$plots_barcode.$head}" ><i class="glyphicon glyphicon-picture"></i></a></td>
							</tr>
						{/foreach}
						{/if}
					</tbody>
				</table>
			{/if}
		</div>
		</div>
	{/if}
	
	<br><br>
	{*If there are barcodes file in the run*}
	{if $barcode_name_count > 1 }	
	
	<table id="ont_stats_datatable" class="table table-striped table-bordered dataTable analysis-result-table" style="white-space:nowrap;">
		<thead>
			<tr>
				{assign var="nb_samples" value=$barcode_name_count}
				<th class="string-sort" rowspan="2" id="th_id_1"><center>Sample {if $nb_samples > 1 }({$nb_samples}){/if}</center></th>
        		<th colspan="{$barcode_headers_count}"><center>Barcode ONT results</center></th>
			</tr>
			<tr>
				{assign var="th_id" value=2}
				{foreach from=$barcode_headers key=k item=head}
					{if $head == 'nb_reads'}
						<th class = "numeric-sort"  id="th_id_{$th_id}">Nb reads</th>
					{elseif $head == 'total_bases'}
						<th class = "numeric-sort" id="th_id_{$th_id}">Total bases</th>
					{elseif $head == 'mean_read_length'}
						<th class = "numeric-sort"  id="th_id_{$th_id}">Mean read length</th>
					{elseif $head == 'mean_read_quality'}
						<th class = "numeric-sort"  id="th_id_{$th_id}">Mean read quality</th>
					{elseif $head == 'N50_read_length'}
						<th class = "numeric-sort"  id="th_id_{$th_id}">N50 read length</th>
					{elseif $head == 'barcode_score'}
						<th class = "numeric-sort"  id="th_id_{$th_id}">Barcode score</th>
					{elseif $head == 'mean_yield_per_sec'}
						<th class = "numeric-sort"  id="th_id_{$th_id}">Mean yield per sec</th>
						
					{elseif $head == 'nb_reads_Q7'}
						<th class = "numeric-sort"  id="th_id_{$th_id}">Nb reads Q7</th>
					{elseif $head == 'total_bases_Q7'}
						<th class = "numeric-sort" id="th_id_{$th_id}">Total bases Q7</th>
					{elseif $head == 'mean_read_length_Q7'}
						<th class = "numeric-sort"  id="th_id_{$th_id}">Mean read length Q7</th>
					{elseif $head == 'mean_read_quality_Q7'}
						<th class = "numeric-sort"  id="th_id_{$th_id}">Mean read quality Q7</th>
					{elseif $head == 'N50_read_length_Q7'}
						<th class = "numeric-sort"  id="th_id_{$th_id}">N50 read length Q7</th>
					{elseif $head == 'barcode_score_Q7'}
						<th class = "numeric-sort"  id="th_id_{$th_id}">Barcode score Q7</th>
					{elseif $head == 'mean_yield_per_sec_Q7'}
						<th class = "numeric-sort"  id="th_id_{$th_id}">Mean yield per sec Q7</th>
						
					{elseif $head == 'nb_reads_Q9'}
						<th class = "numeric-sort"  id="th_id_{$th_id}">Nb reads Q9</th>
					{elseif $head == 'total_bases_Q9'}
						<th class = "numeric-sort" id="th_id_{$th_id}">Total bases Q9</th>
					{elseif $head == 'mean_read_length_Q9'}
						<th class = "numeric-sort"  id="th_id_{$th_id}">Mean read length Q9</th>
					{elseif $head == 'mean_read_quality_Q9'}
						<th class = "numeric-sort"  id="th_id_{$th_id}">Mean read quality Q9</th>
					{elseif $head == 'N50_read_length_Q9'}
						<th class = "numeric-sort"  id="th_id_{$th_id}">N50 read length Q9</th>
					{elseif $head == 'barcode_score_Q9'}
						<th class = "numeric-sort"  id="th_id_{$th_id}">Barcode score Q9</th>
					{elseif $head == 'mean_yield_per_sec_Q9'}
						<th class = "numeric-sort"  id="th_id_{$th_id}">Mean yield per sec Q9</th>
					{/if}
					{$th_id = $th_id +1}
				{/foreach}
			</tr>
		</thead>
		<tbody>
			{$i = 1}
			{assign var="totalReads" value=0}
			{assign var="totalBases" value=0}
			
            {foreach from=$analyse_results_sorted key=sample item=sample_results}
            	{if $sample|in_array:$barcode_name_sample}
            		<tr>
						<td id='sample_{$i}_col_1' class="sample_name">{$sample}</td>
						{$col_id = 2}
						{foreach from=$barcode_headers key=k item=head}
							{if $head == 'nb_reads' || $head == 'nb_reads_Q7' || $head == 'nb_reads_Q9'}
								{if $template_status == "2.0"}
									<th id="sample_{$i}_col_{$col_id}">{($sample_results.$head)|number_format:0:',':' '} ({($sample_results.$head / $rawdata.nb_reads*100)|number_format:2:',':' '}%)</th>
								{else}
									<th id="sample_{$i}_col_{$col_id}">{($sample_results.$head)|number_format:0:',':' '} ({($sample_results.$head / $basic.nb_reads*100)|number_format:2:',':' '}%)</th>
								{/if}
							{elseif $head == 'total_bases' || $head == 'total_bases_Q7' || $head == 'total_bases_Q9'}
								{if $template_status == "2.0"}
									<th id="sample_{$i}_col_{$col_id}">{($sample_results.$head)|number_format:0:',':' '} ({($sample_results.$head / $rawdata.total_bases*100)|number_format:2:',':' '}%)</th>
								{else}
									<th id="sample_{$i}_col_{$col_id}">{($sample_results.$head)|number_format:0:',':' '} ({($sample_results.$head / $basic.total_bases*100)|number_format:2:',':' '}%)</th>
								{/if}
							{elseif $head == 'mean_read_length' || $head == 'mean_read_length_Q7' || $head == 'mean_read_length_Q9'}
								<th id="sample_{$i}_col_{$col_id}">{($sample_results.$head)|number_format:0:',':' '}</th>
							{elseif $head == 'mean_read_quality' || $head == 'mean_read_quality_Q7' || $head == 'mean_read_quality_Q9'}
								<th id="sample_{$i}_col_{$col_id}">{($sample_results.$head)|number_format:1:',':' '}</th>
							{elseif $head == 'N50_read_length' || $head == 'N50_read_length_Q7' || $head == 'N50_read_length_Q9'}
								<th id="sample_{$i}_col_{$col_id}">{($sample_results.$head)|number_format:0:',':' '}</th>
							{elseif $head == 'barcode_score' || $head == 'barcode_score_Q7' || $head == 'barcode_score_Q9'}
								<th id="sample_{$i}_col_{$col_id}">{($sample_results.$head)|number_format:1:',':' '}</th>
							{elseif $head == 'mean_yield_per_sec' || $head == 'mean_yield_per_sec_Q7' || $head == 'mean_yield_per_sec_Q9'}
								<th id="sample_{$i}_col_{$col_id}">{($sample_results.$head)|number_format:0:',':' '}</th>
							{/if}
							
							{$col_id = $col_id + 1}
						{/foreach}
					</tr>
					{$i = $i + 1}
          		{/if}
            {/foreach}

		</tbody>
	</table>
	<br><br>
	{/if}

{* Help block *}
	{if $template_status == "2.0"}
	<div class="tx-nG6-pi1-help">
		<img src="" alt="" class="img" />
		<p>Help for ONT metrics report :</p>
		<span class="meta">
			<ul>
				<li><strong>Nb reads</strong> : 
					The total number of reads for this Flow Cell (including the reads that are filtered during the basecalling).
				</li>
				<li><strong>Total bases</strong> : 
					The number of bases for this Flow Cell.
				</li>
				<li><strong>Mean read length</strong> : 
					The median or mean length of the total reads (bases).
				</li>
				<li><strong>N50 read length</strong> : 
					50% of all bases come from reads longer than this value.
				</li>
					<li><strong>N90 read length</strong> : 
					90% of all bases come from reads longer than this value.
				</li>
				</li>
					<li><strong>N10 read length</strong> : 
					10% of all bases come from reads longer than this value.
				</li>
				<li><strong>Mean read quality</strong> : 
					The mean quality (qscore ONT) for this Flow Cell.
				</li>
				<li><strong>Mean yield per sec</strong> : 
					The mean number of bases per second for the pores of this Flow Cell.
				</li>
				<li><strong>Q7</strong> : 
					Filtered data composed by reads with quality > 7.
				</li>
				<li><strong>Q9</strong> : 
					Filtered data composed by reads with quality > 9.
				</li>
			</ul>
		</span>
	</div>
	<div class="tx-nG6-pi1-help">
		<img src="" alt="" class="img" />
		<p>Help for Plots report :</p>
		<span class="meta">
			<ul>
				<li><strong>Cumulate yield per second</strong> : 
					The cumulate number of bases per hour for this Flow Cell.
				</li>
				<li><strong>Length distribution</strong> : 
					The length distribution of reads for this Flow Cell with reads count or bases count.
				</li>
				<li><strong>Quality distribution</strong> : 
					The quality (qscore ONT) distribution of reads for this Flow Cell.
				</li>
				{if $barcode_name_count > 1 }
				<li><strong> Qscore boxplot</strong> : 
					Boxplots for each barcode of the mean qscore of reads for this Flow Cell.
				</li>
				<li><strong> Qscore per time intervals boxplot</strong> : 
					Boxplots for each barcode of the mean qscore of reads per interval of time for this Flow Cell.
				</li>
				{/if}
			</ul>
		</span>
	</div>
	{if $barcode_name_count > 1 }
		<div class="tx-nG6-pi1-help">
			<img src="" alt="" class="img" />
			<p>Help for Barcode ONT results :</p>
			<span class="meta">
			<ul>
				<li><strong>Barcode score</strong> :
					The quality score of the barcode alignment.
				</li>
			</ul>
			</span>
		</div>
	{/if}
	
	{else}
	<div class="tx-nG6-pi1-help">
		<img src="" alt="" class="img" />
		<p>Help for Basic metrics report :</p>
		<span class="meta">
			<ul>
				<li><strong>Nb reads</strong> : 
					The total number of reads for this Flow Cell (including the reads that are filtered during the basecalling).
				</li>
				<li><strong>Total bases</strong> : 
					The number of bases for this Flow Cell.
				</li>
				<li><strong>Median/mean read length</strong> : 
					The median or mean length of the total reads (bases).
				</li>
				<li><strong>N50 read length</strong> : 
					50% of all bases come from reads longer than this value.
				</li>
				<li><strong>Median yield per sec</strong> : 
					The median number of bases per second for the pores of this Flow Cell.
				</li>
			</ul>
		</span>
	</div>
	<div class="tx-nG6-pi1-help">
		<img src="" alt="" class="img" />
		<p>Help for Quality report :</p>
		<span class="meta">
			<ul>
				<li><strong>Median read quality</strong> : 
					The median quality (qscore ONT) for this Flow Cell.
				</li>
				{if $template_status != "2.0"}
					<li><strong>Useful data</strong> : 
						Filtered data composed by reads with length > 3000 and quality > 7.
					</li>
				{/if}
				{if $template_status == "2.0"}

				{/if}
			</ul>
		</span>
	</div>
	
	<div class="tx-nG6-pi1-help">
		<img src="" alt="" class="img" />
		<p>Help for Plots report :</p>
		<span class="meta">
			<ul>
				<li><strong>Cumulate yield per hour</strong> : 
					The cumulate number of bases per hour for this Flow Cell.
				</li>
				<li><strong>Length distribution</strong> : 
					The length distribution of reads for this Flow Cell with reads count count.
				</li>
				<li><strong>Qscore distribution</strong> : 
					The quality (qscore ONT) distribution of reads for this Flow Cell.
				</li>
				{if $barcode_name_count > 1 }
				<li><strong> Qscore boxplot</strong> : 
					Boxplots for each barcode of the mean qscore of reads for this Flow Cell.
				</li>
				<li><strong> Qscore per time intervals boxplot</strong> : 
					Boxplots for each barcode of the mean qscore of reads per interval of time for this Flow Cell.
				</li>
				{/if}
			</ul>
		</span>
	</div>
	{if $barcode_name_count > 1 }
		<div class="tx-nG6-pi1-help">
			<img src="" alt="" class="img" />
			<p>Help for Barcode ONT results :</p>
			<span class="meta">
			<ul>
				<li><strong>Barcode score</strong> :
					The quality score of the barcode alignment.
				</li>
			</ul>
			</span>
		</div>
	{/if}
	{/if}
{/block}

