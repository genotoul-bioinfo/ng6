{extends file='AnalysisTemplate.tpl'}

<script src="http://bioinfo.genotoul.fr/jvenn/src/jvenn.min.js" type="text/javascript"></script>

{block name=description_update}
    <div  style="float:right;">
        {if $is_project_admin }
            <button id="add_file" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-plus"></i> add files</button>
        {/if}
    </div>
{/block}


{block name=results_title}Structural variations stats{/block}


{block name=results}
    {assign var=my_types value=['ALL', 'DEL', 'DUP', 'INV', 'INS']}
    {assign var="j" value=0}
    With selection :
    <button type="button" class="btn btn-default multiple-selection-btn jvenn-btn"><i class="glyphicon glyphicon-picture"></i> Venn Diagram </button>
    <button type="button" class="btn btn-default multiple-selection-btn length-view-btn"><i class="glyphicon glyphicon-picture"></i> Boxplots </button>
    <button type="button" class="btn btn-default multiple-selection-btn compo-view-btn"><i class="glyphicon glyphicon-picture"></i> Accumulation Curves </button>
    <br/><br/>
    NB: Venn works only with a max of 4 indiv and cannot mix ALL/DEL/DUP/...
    <style type="text/css">
        .my_tab th{
            width: auto !important;
        }
        .dataTables_scrollHeadInner {
            width: 100% !important;
        }
        .my_tab{
            width: 100% !important;
        }
    </style>
    {foreach from=$my_types  item=t}
        <div id="user_information_dialog" title=""><br/></div>
        <input type="hidden" id="venn_count_{$t}" value="{$analyse_results["software_merge"][$t].venn_count}"/>
        <a href="#around_tab_{$t}" data-toggle="collapse" class="expander">
            <br/> {$t}
        </a>
        {if $t == "ALL"}
        <div id="around_tab_{$t}" class="collapse in">
        {else}
        <div id="around_tab_{$t}" class="collapse">
        {/if}
            <table class="table table-striped table-bordered dataTable analysis-result-table my_tab" style="width: 100%;">
                <thead class="my_thead">
                    <tr>
                        <th></th>
                        <th class="string-sort">Software</th>
                        <th class="numeric-sort">Number of SV</th>
                        <th class="numeric-sort">Median size</th>
                        <th class="numeric-sort">Mean size</th>
                        <th class="numeric-sort">Min size</th>
                        <th class="numeric-sort">Max size</th>
                        <th class="numeric-sort">Standard deviation</th>
                        <th class="numeric-sort">Outliers(boxplot)</th>
                    </tr>
                </thead>
                {assign var="i" value=0}
                            <tbody>
                {foreach from=$analyse_results key=software item=software_results}
                    {foreach from=$software_results key=type item=infos_list}
                        {if $type == $t && $software != "software_merge"}
                                <tr>
                                    <td>
                                        <center>
                                            <input type="checkbox" id="chk_sample_{$j}_{$i}" name="chk_soft" value="{$infos_list.soft}"/>
                                            <input type="hidden" id="boxplot_{$j}_{$i}" value="{$infos_list.boxplot}"/>
                                            <input type="hidden" id="outliers_{$j}_{$i}" value="{$infos_list.outliers}"/>
                                            <input type="hidden" id="soft_{$j}_{$i}" value="{$infos_list.soft}"/>
                                            <input type="hidden" id="ntag_{$j}_{$i}" value="{$infos_list.n_tag}"/>
                                            <input type="hidden" id="ncount_{$j}_{$i}" value="{$infos_list.n_count}"/>
                                            <input type="hidden" id="type_{$j}_{$i}" value="{$t}"/>
                                        </center>
                                    </td>
                                    <td align="center">{$infos_list.soft}</td>
                                    <td align="center">{$infos_list.number}</td>
                                    <td align="center">{$infos_list.median}</td>
                                    <td align="center">{$infos_list.mean}</td>
                                    <td align="center">{$infos_list.min}</td>
                                    <td align="center">{$infos_list.max}</td>
                                    <td align="center">{$infos_list.sd}</td>
                                    <td align="center">{$infos_list.outliers}</td>
                                </tr>
                            {$i = $i +1}
                        {/if}
                    {/foreach}
                {/foreach}
                            </tbody>
            </table>
        </div>
        {$j = $j +1}
    {/foreach}
    <br/><br/><br/><br/>
    With selection :
    <button type="button" class="btn btn-default multiple-selection-btn jvenn-btn"><i class="glyphicon glyphicon-picture"></i> Venn Diagram </button>
    <button type="button" class="btn btn-default multiple-selection-btn length-view-btn"><i class="glyphicon glyphicon-picture"></i> Boxplots </button>
    <button type="button" class="btn btn-default multiple-selection-btn compo-view-btn"><i class="glyphicon glyphicon-picture"></i> Accumulation Curves </button>
    <br/>
{/block}