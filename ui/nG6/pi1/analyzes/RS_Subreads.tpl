{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=params}
	{assign var="params" value=" "|explode:$analyse.params}
	{foreach from=$analyse_results key=sample item=sample_results}
	{assign var="sample" value=$sample}
	{/foreach}
	<ul>
		{if in_array("minSubReadLength", $params)}
		{assign var="minSubReadLength" value=$params|@array_keys:"minSubReadLength"}
		<li class="parameter">Subreads shorter than {$params[$minSubReadLength[0]+1]} (in base pairs) are filtered out and excluded from analysis.</li>
		{/if}
		{if in_array("readScore", $params)}
		{assign var="polymerase_read_qual" value=$params|@array_keys:"readScore"}
		<li class="parameter">Polymerase reads with lower quality than {$params[$polymerase_read_qual[0]+1]} are filtered out and excluded from analysis. </li>
		{/if}
		{if in_array("minLength", $params)}
		{assign var="polymerase_read_length" value=$params|@array_keys:"minLength"}
		<li class="parameter">Polymerase reads shorter than {$params[$polymerase_read_length[0]+1]} (in base pairs) are filtered out and excluded from analysis.</li>
		{/if}
		{if in_array("barcode_file", $params)}
		{assign var="barcode_file" value=$params|@array_keys:"barcode_file"}
		<li class="parameter">Input barcode file : {$params[$barcode_file[0]+1]}.</li>
		{/if}
		{if in_array("barcode_score", $params)}
		{assign var="barcode_score" value=$params|@array_keys:"barcode_score"}
		<li class="parameter">Min identical base for barcode : {$params[$barcode_score[0]+1]}.</li>
		{/if}
	</ul>
{/block}


{block name=results_title} Reports {/block}
{block name=results}    
	{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
	{assign var="metrics" value=$analyse_results["metrics"]}
	{assign var="metrics2" value=$analyse_results["metrics2"]}
	
    
	{assign var='loading_headers' value=','|explode:$metrics['loading'].headers|@ksort}
	{assign var='loading_stats_count' value=$loading_headers|@count}
    
	{assign var='postfilter_headers' value=','|explode:$metrics['postfilter_stats'].headers|@ksort}
	{assign var='postfilter_stats_count' value=$postfilter_headers|@count}
    
	{assign var='subreads_headers' value=','|explode:$metrics['subreads_stats'].headers|@ksort}
	{assign var='subreads_stats_count' value=$subreads_headers|@count}
    
	{assign var='barcode_headers' value=','|explode:$metrics2['barcode_results'].headers|@ksort}
	{assign var='barcode_results_count' value=$barcode_headers|@count}
        {assign var='barcode_name_sample' value=','|explode:$metrics2['barcode_results'].barcode_sample|@ksort}
    
	<div>
		<legend>Analyse results report - Sample name : {$descriptions.sample_1}</legend>
		{assign var='data_col' value=2}
		<div class="row">            
			{if $metrics['loading']}
				<div class="col-md-3 col-lg-3">
					<h4>Loading report</h4>
					<ul class="list-unstyled">
						{foreach from=$analyse_results_sorted key=sample item=results_sample}
						{if $sample != "metrics" and $sample != "metrics2"}
						{foreach from=$loading_headers key=k item=head}
							<li>
								<div>
									{if ($head == 'Productivity 0')  or ($head == 'Productivity 1') or ($head == 'Productivity 2')}
										<label>{$head} :  </label> {($results_sample['loading'].$head*100)|number_format:2:',':' '}%
									{else}
										<label>{$head} :  </label> {($results_sample['loading'].$head)|number_format:0:',':' '}
									{/if}
								</div>
							</li>
							{$data_col = $data_col + 1}
						{/foreach}
						{/if}
						{/foreach}
					</ul>
				</div>
			{/if} 
			{if $metrics['postfilter_stats']}
				<div class="col-md-4 col-lg-4">
					<h4>Polymerase reads Stats</h4>
					<ul class="list-unstyled">
						{foreach from=$analyse_results_sorted key=sample item=results_sample}
                                                {if $sample != "metrics" and $sample != "metrics2"}
						{foreach from=$postfilter_headers key=k item=head}
							<li>
								<div>
			                    				{if ($head == 'readlen' || $head == 'readscore')}
										<label>{$head} :  </label> <a class="imglink" href="{$results_sample['postfilter_stats'].$head}" >pic</a>
			                    				{elseif $head == 'Mean Read Score'}
			                        				<label>{$head} :  </label> {($results_sample['postfilter_stats'].$head)|number_format:2:',':' '}
			                        			{else}
			                        				<label>{$head} :  </label> {($results_sample['postfilter_stats'].$head)|number_format:0:',':' '}
			                    				{/if}
                                				</div>
                            				</li>
                            				{$data_col = $data_col + 1}
	                    			{/foreach}
			    			{/if}
                            			{/foreach}

                    			</ul>
                		</div>
			{/if}
			{if $metrics['subreads_stats']}
				<div class="col-md-4 col-lg-4">
					<h4>Subreads Filter Stats</h4>
					<ul class="list-unstyled">
						{foreach from=$analyse_results_sorted key=sample item=results_sample}
                                                {if $sample != "metrics" and $sample != "metrics2"}
                        			{foreach from=$subreads_headers key=k item=head}
                            				<li>
                                				<div>
								{if $head == 'report' }
            			            				<label>{$head} :  </label> <a class="imglink" href="{$results_sample['subreads_stats'].$head}" >pic</a>
			                    			{else}
			                        			<label>{$head} :  </label> {($results_sample['subreads_stats'].$head)|number_format:0:',':' '}
			                    			{/if}
                                				</div>
                            				</li>
                            				{$data_col = $data_col + 1}
	                    			{/foreach}
						{/if}
                            			{/foreach}
                    			</ul>
				</div>
			{/if}
		</div>
	</div>
	{*If there are barcodes file in the run*}
	{if $barcode_results_count > 1 }	
	
	<table id="rs_subreads_datatable" class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th rowspan="2"><center><input type="checkbox" id="chk_all_sample"/></center></th>
				{assign var="nb_samples" value=$barcode_name_sample|@count}
				<th class="string-sort" rowspan="2" id="th_id_1"><center>Sample {if $nb_samples > 1 }({$nb_samples}){/if}</center></th>
        		<th colspan="{$barcode_results_count}"><center>Barcode SubReads results (Before filters)</center></th>
			</tr>
			<tr>
				{assign var="th_id" value=2}
				{foreach from=$barcode_headers key=k item=head}
					{if $head == 'Reads'}
						<th class = "numeric-sort" id="th_id_{$th_id}">NB SubReads</th>
					{elseif $head == 'Bases'}
						<th class = "numeric-sort"  id="th_id_{$th_id}">NB Bases</th>
					{/if}
					{$th_id = $th_id +1}
				{/foreach}
			</tr>
		</thead>
		<tbody>
			{$i = 1}
			{assign var="totalReads" value=0}
			{assign var="totalBases" value=0}

                        {foreach from=$analyse_results_sorted key=sample item=sample_results}
                                {if ($sample!="metrics2") and ($sample!="metrics")}
					{foreach from=$barcode_name_sample item=sample}
                                        <tr>
                                                <td><center><input type="checkbox" id="chk_sample_{$i}" value="sample"/></center></td>
                                                <td id='sample_{$i}_col_1' class="sample_name">{$sample}</td>

                                                {$col_id = 2}
                                                {foreach from=$barcode_headers key=k item=head}
                                                        {if $head == 'Reads'}
                                                                <td id="sample_{$i}_col_{$col_id}">{$sample_results[$sample].$head|number_format:0:',':' '}</td>
                                                                {$totalReads = $totalReads + $sample_results[$sample].$head}
                                                        {elseif $head == 'Bases'}
                                                                <td id="sample_{$i}_col_{$col_id}">{$sample_results[$sample].$head|number_format:0:',':' '}</td>
                                                                {$totalBases = $totalBases + $sample_results[$sample].$head}
                                                        {/if}
                                                        {$col_id = $col_id + 1}
                                                {/foreach}
                                        </tr>
                                        {$i = $i + 1}
					{/foreach}
                                {/if}
                        {/foreach}
		{*
		<div>
			{$data_col = 2}
			<div class="row">
				<div class="col-md-2 col-lg-2">
					<h4>Barcode SubReads results (Before filters)</h4>
					<ul class="list-unstyled">
						{foreach from=$barcode_headers key=k item=head}
							<span class="checkbox"><label><input type="checkbox" data-column="{$data_col}" class="toogle-vis" checked/> {$head}</label></span>
							{$data_col = $data_col + 1}
						{/foreach}
					</ul>
				</div>
			</div>
		</div>
		*}
		</tbody>
	   	
	   	<tfoot>
			<tr>
				<th>Total : </th>
				<th>&nbsp;</th>
				{foreach from=$barcode_headers key=k item=head}
					{if $head == 'Reads'}
						<th>{$totalReads|number_format:0:',':' '}</th>
					{elseif $head == 'Bases'}
						<th>{$totalBases|number_format:0:',':' '}</th>
					{/if}
				{/foreach}
			</tr>
			<tr>
				<th>Mean : </th>
				<th>&nbsp;</th>
				{foreach from=$barcode_headers key=k item=head}
					{if $head == 'Reads'}
						<th>{($totalReads/$nb_samples)|number_format:0:' ':' '}</th>
					{elseif $head == 'Bases'}
						<th>{($totalBases/$nb_samples)|number_format:0:' ':' '}</th>
					{/if}
				{/foreach}
			</tr>
			<tr>
				<th>All metrics :  <input type="checkbox" id="check_all_metrics"></th>
				<th>&nbsp;</th>
				{$th_id = 2}
				{foreach from=$barcode_headers key=k item=head}
					<th><center><input type="checkbox" id="chk_col_{$th_id}"></center></th>
					{$th_id = $th_id +1}
				{/foreach}
			</tr>
			<tr>
				<th align="left" colspan="{$th_id}">
					With selection :
					<button type="button" class="btn btn-default multiple-selection-btn" id="create_graph"><i class="glyphicon glyphicon-signal"></i> Compare</button>
				</th>
			</tr>
		</tfoot>
	</table>
	
	{/if}

	<div class="tx-nG6-pi1-help">
		<img src="" alt="" class="img" />
		<p>Help for loading report :</p>
		<span class="meta">
			<ul>
				<li><strong>Productive ZMWs</strong> : 
					The number of ZMWs for this SMRT Cell that produced results.
				</li>
				<li><strong>P0</strong> : 
					The percentage of ZMWs that are empty, with no polymerase.
				</li>
				<li><strong>P1</strong> : 
					The percentage of ZMWs that are productive and sequencing.
				</li>
				<li><strong>P2</strong> : 
					The percentage of ZMWs that are not P0 (empty) or P1 (productive). This may occur for a variety of reasons - the sequence data is not usable.
				</li>
			</ul>
		</span>
	</div>
	<div class="tx-nG6-pi1-help">
		<img src="" alt="" class="img" />
		<p>Help for Subreads report :</p>
		<span class="meta">
			<ul>
				<li><strong>Mean Subread length</strong> : 
					The mean length of the subreads that passed filtering.
				</li>
				<li><strong>Total Number of Bases</strong> : 
					The total number of bases in the subreads that passed filtering.
				</li>
				<li><strong>N50</strong> : 
					50% of all bases come from subreads longer than this value.
				</li>
				<li><strong>Number of Reads</strong> : 
					The total number of reads that passed filtering.
				</li>
			</ul>
		</span>
	</div>
	
	<div class="tx-nG6-pi1-help">
		<img src="" alt="" class="img" />
		<p>Help for Polymerase reads report :</p>
		<span class="meta">
			<ul>
				<li><strong>Polymerase Read Bases</strong> : 
					The number of bases in the polymerase reads after filtering, including adaptors.
				</li>
				<li><strong>Polymerase Reads</strong> : 
					The number of polymerases generating trimmed reads after filtering. Polymerase reads include bases from adaptors and multiple passes around a circular template.
				</li>
				<li><strong>Polymerase Read Length</strong> : 
					The mean trimmed read length of all polymerase reads after filtering. The value includes bases from adaptors as well as multiple passes around a circular template.
				</li>
				<li><strong>Polymerase Read Quality</strong> : 
					The mean single-pass read quality of all polymerase reads after filtering.
				</li>
			</ul>
		</span>
	</div>
{/block}

