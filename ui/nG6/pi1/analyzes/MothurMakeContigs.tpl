{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=params_title} Mothur commands and parameters used {/block}
{block name=params_content}
	{assign var="params" value=";"|explode:$analyse.params}
	<ul>
	<li class="parameter">stability.file is created from read1.fastq and read2.fastq for all samples</li>
		{foreach from=$params item=param}
			{assign var="args_cmd" value=","|explode:$param}
			{if in_array("#make.contigs(file=stability.file", $args_cmd)}
			{foreach from=$args_cmd item=arg_cmd}
				{assign var="params_cmd" value="="|explode:$arg_cmd}
				{if in_array("pdiffs", $params_cmd)}
				{assign var="pdiffs_index" value=$params_cmd|@array_keys:"pdiffs"}
				<li class="parameter">The maximum number of differences allowed with the primer is {$params_cmd[$pdiffs_index[0]+1]|replace:')':''}.</li>
				{/if}
			{/foreach}
			{/if}
			
			{if in_array("#screen.seqs(fasta=FILE.fasta", $args_cmd)}
			{foreach from=$args_cmd item=arg_cmd}
				{assign var="params_cmd" value="="|explode:$arg_cmd}
				{if in_array("maxambig", $params_cmd)}
				{assign var="maxambig_index" value=$params_cmd|@array_keys:"maxambig"}
				<li class="parameter">Cull sequences with have  {$params_cmd[$maxambig_index[0]+1]} ambiguous bases.</li>
				{/if}
				{if in_array("maxlength", $params_cmd)}
				{assign var="maxlength_index" value=$params_cmd|@array_keys:"maxlength"}
				<li class="parameter">Remove any sequences with anything longer than {$params_cmd[$maxlength_index[0]+1]|replace:')':''} bp.</li>
				{/if}
			{/foreach}
			{/if}
		{/foreach}
		<br />
		{foreach from=$params item=param}
		<li class="parameter">{$param}</li>
		{/foreach}
	</ul>
{/block}

{block name=results_title} Make Contigs results {/block}
{block name=results}

	<input type="hidden" id="data_folder" value="{$data_folder}"/>
	<input type="hidden" id="analysis_folder" value="{$data_folder|cat:$analyse.directory}"/>

	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th><center>
					<input type="checkbox" id="chk_all_sample">
				</center></th>
				<th class="string-sort">Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
				<th class="numeric-sort">Number of reads (R1 associated to R2)</th>
				<th class="numeric-sort">Number of reads (R1+R2) after cleaning</th>
				<th class="numeric-sort">Not combined reads (R1+R2)</th>
				<th class="numeric-sort">% deleted</th>
			</tr>
		</thead>
		<tbody>
			{assign var="nb_reads_begining" value=0}
			{assign var="nb_reads_end" value=0}
			{assign var="uncorrect" value=0}
			{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
			{assign var="i" value=0}
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
		   	<tr>
		   		<td><center>
					<input type="checkbox" id="chk_sample_{$i}" value="sample">
					<input type="hidden" id="contigs_length_{$i}" value="{$sample_results["default"].contigs_length}"/>
					<input type="hidden" id="contigs_length_count_{$i}" value="{$sample_results["default"].contigs_length_count}"/>
				</center></td>
				<td id="sample_id_{$i}">{$sample|get_description:$descriptions}</td>
				{$nb_reads_begining=$nb_reads_begining+$sample_results["default"].nb_seq_begining}
				{$nb_reads_end=$nb_reads_end+$sample_results["default"].nb_contigs_end}
				{$uncorrect=$uncorrect+($sample_results["default"].nb_seq_begining-$sample_results["default"].nb_contigs_end)}
				<td>{$sample_results["default"].nb_seq_begining|number_format:0:' ':' '}</td>
				<td>{$sample_results["default"].nb_contigs_end|number_format:0:' ':' '}</td>
				<td>{($sample_results["default"].nb_seq_begining-$sample_results["default"].nb_contigs_end)|number_format:0:' ':' '}</td>
				<td>{(100.00-((($sample_results["default"].nb_contigs_end)*100)/$sample_results["default"].nb_seq_begining))|number_format:2:'.':' '}</td>
		   	</tr>
		   	{$i = $i +1}
		   	{/foreach}
		</tbody>
		<tfoot>
			{if $analyse_results|@count > 1 }
			<tr>
				<th></th>
				<th>Total</th>
				<th>{$nb_reads_begining|number_format:0:' ':' '}</th>
				<th>{$nb_reads_end|number_format:0:' ':' '}</th>
				<th>{$uncorrect|number_format:0:' ':' '}</th>
				<th>{(100.00-((($nb_reads_end)*100)/$nb_reads_begining))|number_format:2:'.':' '}</th>
			</tr>
			<tr>
				<th></th>
				<th>Mean</td>
				<th>{($nb_reads_begining/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{($nb_reads_end/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{($uncorrect/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{(100.00-((($nb_reads_end)*100)/$nb_reads_begining))|number_format:2:'.':' '}</th>
			</tr>
			<tr>
				<th align="left" colspan="10">
					With selection :
						<button type="button" class="btn btn-default btn-sm contigs-bar-view-btn multiple-selection-btn"><i class="glyphicon glyphicon-signal"></i> Bar view</button>
				</th>
			</tr>
			{/if}
		</tfoot>
	</table>
{/block}
