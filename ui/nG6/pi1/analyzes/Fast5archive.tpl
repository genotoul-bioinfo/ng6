{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}
{block name=params}
	{assign var="params" value=" "|explode:$analyse.params}
	{foreach from=$analyse_results key=sample item=sample_results}
	{assign var="sample" value=$sample}
	{/foreach}>
{/block}


{block name=results_title} Reports {/block}
{block name=results}
	{assign var="analyse_results_sorted" value=$analyse_results["ont_sample"]|@ksort}
	{assign var="metrics" value=$analyse_results["metrics"]}

	{assign var='barcode_headers' value=','|explode:$metrics['barcode'].headers|@ksort}
	{assign var='barcode_headers_count' value=$barcode_headers|@count}
	{assign var='barcode_name_sample' value=','|explode:$metrics['barcode'].names|@ksort}
	{assign var='barcode_name_count' value=$barcode_name_sample|@count}
	{assign var='ont_sample_count' value=$analyse_results_sorted|@count}
	{assign var='ont_metrics_names' value=','|explode:$metrics['statsporechop'].headers|@ksort}
	{assign var='ont_metrics_count' value=$ont_metrics_names|@count}
	{*debug*}
	
	<legend>Analyse results report - Sample name : {$descriptions.sample_1}</legend>
		
	<br>
	<div class="tx-nG6-pi1-help">
		<img src="" alt="" class="img" />
		<p>Help for extract files results :</p>
		<span class="meta">
			<ul>
				<li><strong>Download archive</strong> : 
					Use the Downloads view tab to download the archive.
					The archive was compressed by tar + gzip.
				</li>
				<li><strong>Decompress archive</strong> : 
					Use tar -xzf fast5archive.tar.gz
				</li>
			</ul>
		</span>
	</div>
	
{/block}

