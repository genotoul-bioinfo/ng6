{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=results_title} Filtering results {/block}
{block name=results}
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th class="string-sort">Samples</th>
				<th class="numeric-sort">Before filtering</th>
				<th class="numeric-sort">After filtering</th>
				<th class="numeric-sort">Filtered</th>
			</tr>
		</thead>
		{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
		<tbody>
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
		   	<tr>
				<td>{$sample|get_description:$descriptions}</td>
				<td>{$sample_results["default"].input|number_format:0:' ':' '}</td>
				<td>{$sample_results["default"].output|number_format:0:' ':' '}</td>
				<td>{($sample_results["default"].input-$sample_results["default"].output)|number_format:0:' ':' '}</td>
		   	</tr>
		   	{/foreach}
	   	</tbody>
	</table>
{/block}

{block name=download}{/block}



