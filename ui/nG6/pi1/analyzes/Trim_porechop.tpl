{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}
{block name=params}
	{assign var="params" value=" "|explode:$analyse.params}
	{foreach from=$analyse_results key=sample item=sample_results}
	{assign var="sample" value=$sample}
	{/foreach}
	<ul>
		<li class="parameter"> {$analyse.params} : Reads that contain adapters in the middle will be removed.</li>
	</ul>
{/block}


{block name=results_title} Reports {/block}
{block name=results}
	{assign var="analyse_results_sorted" value=$analyse_results["ont_sample"]|@ksort}
	{assign var="metrics" value=$analyse_results["metrics"]}

	{assign var='barcode_headers' value=','|explode:$metrics['barcode'].headers|@ksort}
	{assign var='barcode_headers_count' value=$barcode_headers|@count}
	{assign var='barcode_name_sample' value=','|explode:$metrics['barcode'].names|@ksort}
	{assign var='barcode_name_count' value=$barcode_name_sample|@count}
	{assign var='ont_sample_count' value=$analyse_results_sorted|@count}
	{assign var='ont_metrics_names' value=','|explode:$metrics['statsporechop'].headers|@ksort}
	{assign var='ont_metrics_count' value=$ont_metrics_names|@count}
	{*debug*}
	
	<legend>Analyse results report - Sample name : {$descriptions.sample_1}</legend>
		
	<br>
	<table id="ont_stats_datatable" class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				{assign var="nb_samples" value=$ont_sample_count}
				<th class="string-sort" rowspan="2" id="th_id_1"><center>Sample {if $nb_samples > 1 }({$nb_samples}){/if}</center></th>
				<th colspan="{$ont_metrics_count}"><center>Trimmed ONT files results</center></th>
			</tr>
			<tr>
				{assign var="th_id" value=2}
                                {assign var="list_var" value=','|explode:"read_total_start,read_trim_start,bp_removed_start,read_trim_end,bp_removed_end"}
                                {foreach from=$list_var key=k item=head}
                                        {if $head == 'read_total_start'}
                                                <th class = "numeric-sort" id="th_id_{$th_id}">NB_read_total</th>
                                        {elseif $head == 'read_trim_start'}
                                                <th class = "numeric-sort"  id="th_id_{$th_id}">NB_read_trim_5'</th>
                                        {elseif $head == 'bp_removed_start'}
                                                <th class = "numeric-sort"  id="th_id_{$th_id}">NB_bases_trim_5'</th>
                                        {elseif $head == 'read_trim_end'}
                                                <th class = "numeric-sort" id="th_id_{$th_id}">NB_read_trim_3'</th>
                                        {elseif $head == 'bp_removed_end'}
                                                <th class = "numeric-sort"  id="th_id_{$th_id}">NB_bases_trim_3'</th>
                                        {/if}
                                        {$th_id = $th_id +1}
                                {/foreach}
                        </tr>
                </thead>
                <body>
                        {foreach from=$analyse_results_sorted key=sample item=sample_results}
                                <tr>
                                <td id='sample_{$i}_col_1' class="sample_name">{$sample}</td>
                                {$col_id = 2}
                                {foreach from=$list_var key=k item=head}
                                        {if $head == 'read_total_start'}
                                                <th id="sample_{$i}_col_{$col_id}">{($sample_results.$head)|number_format:0:',':' '}</th>
                                        {elseif $head == 'read_trim_start'}
                                                <th id="sample_{$i}_col_{$col_id}">{($sample_results.$head)|number_format:0:',':' '}</th>
                                        {elseif $head == 'bp_removed_start'}
                                                <th id="sample_{$i}_col_{$col_id}">{($sample_results.$head)|number_format:0:',':' '}</th>
                                        {elseif $head == 'read_trim_end'}
                                                <th id="sample_{$i}_col_{$col_id}">{($sample_results.$head)|number_format:0:',':' '}</th>
                                        {elseif $head == 'bp_removed_end'}
                                                <th id="sample_{$i}_col_{$col_id}">{($sample_results.$head)|number_format:0:',':' '}</th>
                                        {/if}
                                        {$col_id = $col_id + 1}
                                {/foreach}
                                </tr>
                                {$i = $i + 1}
                        {/foreach}
                </body>
        </table>
        <div class="tx-nG6-pi1-help">
                <img src="" alt="" class="img" />
                <p>Help for Trimmed ONT files results :</p>
                <span class="meta">
                        <ul>
                                <li><strong>NB_read_total</strong> :
                                        The total number of reads for this sample.
                                </li>
                                <li><strong>NB_read_trim_5'</strong> :
                                        The number of reads trimmed because they have adapters in 5'.
                                </li>
                                <li><strong>NB_bases_trim_5'</strong> :
                                        The number of bases removed because they are part of adapters in 5'.
                                </li>
                                <li><strong>NB_read_trim_3'</strong> :
                                        The number of reads trimmed because they have adapters in 3'.
                                </li>
                                <li><strong>NB_bases_trim_3'</strong> :
                                        The number of bases removed because they are part of adapters in 3'.
                                </li>
		</span>
	</div>
	
{/block}

