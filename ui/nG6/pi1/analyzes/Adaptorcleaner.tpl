{*
Copyright (C) 2009 INRA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=params_title} Cleaning options {/block}
{block name=params_content}
	{assign var="params" value=" "|explode:$analyse.params}
	<ul>
		{assign var="score_index" value=$params|@array_keys:"--score"}
		<li class="parameter">Min score used by crossmatch : {$params[$score_index[0]+1]}% of adaptor length.</li>
		{assign var="match_index" value=$params|@array_keys:"--match"}
		<li class="parameter">Min match used by crossmatch : {$params[$match_index[0]+1]}% of adaptor length.</li>
		{assign var="len_index" value=$params|@array_keys:"--minlen"}
		<li class="parameter">Clean reads with a smaller lenght than $params[$len_index[0]+1]} bp.</li>
		{if $analyse_results["all"]["default"]["adaptor_file"] == ""}
		<li class="parameter">Adaptor used: <br />
			{$isseq=false}
			{foreach $params as $param}
				{if $isseq}
				{$param} <br />
				{$isseq=false}
				{/if}
				{if $param|substr:0:1 == ">"}
				{$param} <br />
				{$isseq=true}
				{/if}
			{/foreach}
		</li>
		{else}
		{assign var="index" value=$params|@array_keys:"--adaptor"}
		<li class="parameter">Adaptor used:
			<br />
			<div class="file-display">{$analyse_results["all"]["default"]["adaptor_file"]}</div>
		</li>
		{/if}
	</ul>
{/block}

{block name=content}

	<ul id="myTab" class="nav nav-tabs">
		<li class="active"><a href="#results" data-toggle="tab">Cleaning results</a></li>
		<li><a href="#adaptor" data-toggle="tab">Adaptor(s)</a></li>
		<li><a href="#parameters" data-toggle="tab">Parameters</a></li>
		<li><a href="#downloads" data-toggle="tab">{block name=downloads_title} Downloads {/block}</a></li>
	</ul>
	<div id="myTabContent" class="tab-content">
		<div class="tab-pane fade in active" id="results">
			{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
			<table class="table table-striped table-bordered dataTable analysis-result-table">
				<thead>
					<tr>
						<th class = "string-sort">Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
						<th class = "numeric-sort">Before cleaning</th>
						<th class = "numeric-sort">After cleaning</th>
						<th class = "numeric-sort">Only adaptor found</th>
						<th class = "numeric-sort">Too short</th>
						<th class = "numeric-sort">% deleted</th>
					</tr>
				</thead>
				<tbody>
					{foreach from=$analyse_results_sorted key=sample item=sample_results}
					{if $sample != "all"}
					   	<tr>
							<td>{$sample|get_description:$descriptions}</td>
							<td>{$sample_results["default"].nb_reads_begining|number_format:0:' ':' '}</td>
							<td>{$sample_results["default"].nb_reads_end|number_format:0:' ':' '}</td>
							<td>{$sample_results["default"].del_only_adaptator|number_format:0:' ':' '}</td>
							<td>{$sample_results["default"].del_length|number_format:0:' ':' '}</td>
							<td>{(100.00-((($sample_results["default"].nb_reads_end)*100)/$sample_results["default"].nb_reads_begining))|number_format:2:'.':' '}</td>
					   	</tr>
					{/if}
				   	{/foreach}
				</tbody>
			</table>
		</div>

		<div class="tab-pane fade" id="adaptor">
			{* First find out all adaptors used *}
			{assign var="adaptors" value=array()}
			{foreach from=$analyse_results key=sample item=sample_results}
				{foreach from=$sample_results["adaptor"] key=group item=value}
					{if !in_array($group, $adaptors) }
						{$adaptors[]=$group}
					{/if}
				{/foreach}
			{/foreach}
			<table id="adaptor-table" class="table table-striped table-bordered dataTable analysis-result-table">
				<thead>
					<tr>
						<th>Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
					    {foreach $adaptors as $adaptor}
					    <th> {$adaptor} </th>
					    {/foreach}
					</tr>
				</thead>
				<tbody>
					{foreach from=$analyse_results_sorted key=sample item=sample_results}
					{if $sample != "all"}
					   	<tr>
							<td>{$sample|get_description:$descriptions}</td>
							{foreach $adaptors as $adaptor}
							<td>{$sample_results["adaptor"][$adaptor]|number_format:0:' ':' '}</td>
							{/foreach}
					   	</tr>
					{/if}
				   	{/foreach}
			   	</tbody>
			</table>
		</div>

		<div class="tab-pane fade" id="parameters">
			{block name=params}
				{block name=params_content}
				<ul>
					<li class="parameter">{$analyse.params}</li>
				</ul>
				{/block}
			{/block}
		</div>

		<div class="tab-pane fade" id="downloads">
			{block name=download}
				{if $nb_files != 0}
					<p><ul>
						{$dir=$data_folder|cat:$analyse.directory}
						{assign var="nb_files" value=0}
						{foreach $dir|scandir as $file}
						{if $file != "." and $file != "" and $file != ".." and ($file|substr:-strlen(".png")) != ".png"}
							{$link=(('fileadmin'|cat:$analyse.directory)|cat:'/')|cat:$file}
							<li class="filelist"><a href="{$link}">{$file}</a> </li>
							{$nb_files = $nb_files + 1}
						{/if}
						{/foreach}
					</ul></p>
				{else}
				    <div class="alert alert-info">
						Results folder not synchronized yet...
	    			</div>
				{/if}
			{/block}
		</div>

	</div>
{/block}
