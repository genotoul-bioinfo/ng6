
$(function () {
	
	$(".image-link").click(function() {
		var img_href = $(this).attr('href') ;
		var imgLoad = $("<img />")
			.attr("src", img_href)
			.attr('alt', 'Cannot display')
			.unbind("load")
			.bind("load", function () {
				var imgwidth = ( this.width > 900 ) ? 900 : this.width ;
				resize_center_btmodal('#ng6modal',imgwidth + 50 );
				$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
				$("#modal-body-tmpl").html('<div id="img_container" '+
						'style="overflow-x : auto"><img src="' + img_href + '" alt="Cannot display"></div>');
				$("#modal-foot-tmpl").html('<button class="btn btn-default" ' + 
						'data-dismiss="modal" aria-hidden="true">' + 
						'<i class="glyphicon glyphicon-remove"></i> Close</button>');
				$("#ng6modal").modal();
			});

       	return false;
	});
	
});


