{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=params_content}
	{assign var="params" value=" "|explode:$analyse.params}
	<ul>
	{if in_array("--casava", $params)}
		<li class="parameter">Use casava output (--casava).</li>
	{/if}
	{if in_array("--nogroup", $params)}
		<li class="parameter">Disable grouping of bases for reads >50bp (--nogroup).</li>
	{/if}
	</ul>
{/block}

{block name=results_title} Reads and quality statistics {/block}
{block name=results}
    {assign var="metrics" value=false}
    {assign var="pps_count" value=4}
    {assign var="pss_count" value=7}
    
    {if 'metrics'|array_key_exists:$analyse_results }
        {assign var="metrics" value=$analyse_results["metrics"]}
        
        {assign var='per_pos_stats' value=','|explode:"perbasequal,baseGC,nperbase,seqcontent"}
        {assign var='per_seq_stats' value=','|explode:"perseqqual,seqGC,seqlen,seqduplication,kmers,seqoverrepresentation,pertilequal,adaptercontent"}
        
        {assign var="pps_count" value=0}
        {assign var="pss_count" value=1}
        
        {foreach from=$per_pos_stats key=i item=j}
            {if $metrics[$j]}
                {$pps_count = $pps_count + 1}
            {/if}
        {/foreach}
        
        {foreach from=$per_seq_stats key=i item=j}
            {if $metrics[$j]}
                {$pss_count = $pss_count + 1}
            {/if}
        {/foreach}
    {/if}
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th rowspan="2"><center><input type="checkbox" id="chk_all_sample"/></center></th>
				{assign var="nb_samples" value=$analyse_results|@count }
				{if $metrics}
				    {$nb_samples = $nb_samples -1 }
				{/if}
				<th class="string-sort" rowspan="2" id="th_id_1">Samples {if $nb_samples > 1 }({$nb_samples}){/if}</th>
		        <th colspan="{$pps_count}" >Per position statistics</th>
		        <th colspan="{$pss_count}" >Per sequence statistics</th>
			</tr>
			<tr>
			    {assign var="th_id" value=2}
				{if $metrics}
				    {if $metrics['perbasequal']}
				        <th class="string-sort" id="th_id_{$th_id}">Quality</th>
    				    {$th_id = $th_id +1}
    				{/if}
    			{* no metrics means old template*}
				{else}
				    <th class="string-sort" id="th_id_{$th_id}" >Quality</th>
				    {$th_id = $th_id +1}
				{/if}
				
				{if $metrics}
    				{if $metrics['baseGC']}
	    			    <th class="string-sort" id="th_id_{$th_id}">GC %</th>
	    			    {$th_id = $th_id +1}
	    			{/if}
    			{else}
    			    <th class="string-sort" id="th_id_{$th_id}">GC %</th>
    			    {$th_id = $th_id +1}
				{/if}
				
				{if $metrics}
			        {if $metrics['nperbase']}
			            <th class="string-sort" id="th_id_{$th_id}">N content</th>
			            {$th_id = $th_id +1}
			        {/if}
		        {else}
		            <th class="string-sort" id="th_id_{$th_id}">N content</th>
		            {$th_id = $th_id +1}
		        {/if}
				
				{if $metrics}
				    {if $metrics['seqcontent']}
				        <th class="string-sort" id="th_id_{$th_id}">Per base content</th>
				        {$th_id = $th_id +1}
				    {/if}
				{else}
				    <th class="string-sort" id="th_id_{$th_id}">Per base content</th>
			        {$th_id = $th_id +1}
				{/if}
				
				<th class="numeric-sort" id="th_id_{$th_id}">Number of sequences</th>
				{$th_id = $th_id +1}
				
				{if $metrics}
				    {if $metrics['perseqqual']}
				        <th class="string-sort" id="th_id_{$th_id}">Quality</th>
				        {$th_id = $th_id +1}
				    {/if}
				{else}
				    <th class="string-sort" id="th_id_{$th_id}">Quality</th>
			        {$th_id = $th_id +1}
				{/if}
				
				{if $metrics}
				    {if $metrics['seqGC']}
				        <th class="string-sort" id="th_id_{$th_id}">GC%</th>
				        {$th_id = $th_id +1}
				    {/if}
				{else}
				    <th class="string-sort" id="th_id_{$th_id}">GC%</th>
			        {$th_id = $th_id +1}
				{/if}
				
				{if $metrics}
				    {if $metrics['seqlen']}
				        <th class="string-sort" id="th_id_{$th_id}">Length Distribution</th>
				        {$th_id = $th_id +1}
				    {/if}
				{else}
				    <th class="string-sort" id="th_id_{$th_id}">Length Distribution</th>
			        {$th_id = $th_id +1}
				{/if}
				
				{if $metrics}
				    {if $metrics['seqduplication']}
				        <th class="string-sort" id="th_id_{$th_id}">Duplication Levels</th>
				        {$th_id = $th_id +1}
				    {/if}
				{else}
				    <th class="string-sort" id="th_id_{$th_id}">Duplication Levels</th>
			        {$th_id = $th_id +1}
				{/if}
				
				{if $metrics}
				    {if $metrics['kmers']}
				        <th class="string-sort" id="th_id_{$th_id}">Kmer profiles</th>
				        {$th_id = $th_id +1}
				    {/if}
				{else}
				    <th class="string-sort" id="th_id_{$th_id}">Kmer Content</th>
			        {$th_id = $th_id +1}
				{/if}
				
				{if $metrics}
				    {if $metrics['seqoverrepresentation']}
				        <th class="string-sort" id="th_id_{$th_id}">Overrepresented sequences</th>
				        {$th_id = $th_id +1}
				    {/if}
				{else}
				    <th class="string-sort" id="th_id_{$th_id}">Overrepresented sequences</th>
			        {$th_id = $th_id +1}
				{/if}

				{if $metrics}
				    {if $metrics['adaptercontent']}
				        <th class="string-sort" id="th_id_{$th_id}">Adapter Content</th>
				        {$th_id = $th_id +1}
				    {/if}
			    {/if}
			</tr>
		</thead>
		
		{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
		<tbody>
			{assign var=i value=1}
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
			    {if $sample!="metrics"}
			        <tr>
				        <td><center><input type="checkbox" id="chk_sample_{$i}" value="sample"/></center></td>			
				        <td id='sample_{$i}_col_1' class="sample_name">{$sample|get_description:$descriptions}</td>
				        
				        {assign var="col_id" value=2}
				        {if $metrics}
				            {if $metrics['perbasequal']}
				                <td id="sample_{$i}_col_{$col_id}"><a class="imglink" href="{$sample_results["pbqpng"].img}"> {$sample_results["pbqpng"].result} </a></td>
				                {$col_id = $col_id + 1}
				            {/if}
				        {else}
				            <td id="sample_{$i}_col_{$col_id}"><a class="imglink" href="{$sample_results["pbqpng"].img}"> {$sample_results["pbqpng"].result} </a></td>
			                {$col_id = $col_id + 1}
				        {/if}
				
				        {if $metrics}
				            {if $metrics['baseGC']}
				                <td id="sample_{$i}_col_{$col_id}"><a class="imglink" href="{$sample_results["pbgcpng"].img}"> {$sample_results["pbgcpng"].result} </a></td>
				                {$col_id = $col_id + 1}
				            {/if}
				        {else}
				            <td id="sample_{$i}_col_{$col_id}"><a class="imglink" href="{$sample_results["pbgcpng"].img}"> {$sample_results["pbgcpng"].result} </a></td>
			                {$col_id = $col_id + 1}
				        {/if}
				        
				        {if $metrics}
				            {if $metrics['nperbase']}
				                <td id="sample_{$i}_col_{$col_id}"><a class="imglink" href="{$sample_results["pbnspng"].img}"> {$sample_results["pbnspng"].result} </a></td>
				                {$col_id = $col_id + 1}
				            {/if}
			            {else}
			                <td id="sample_{$i}_col_{$col_id}"><a class="imglink" href="{$sample_results["pbnspng"].img}"> {$sample_results["pbnspng"].result} </a></td>
			                {$col_id = $col_id + 1}
			            {/if}
				
			            {if $metrics}
				            {if $metrics['seqcontent']}
				                <td id="sample_{$i}_col_{$col_id}"><a class="imglink" href="{$sample_results["pbspng"].img}"> {$sample_results["pbspng"].result} </a></td>
				                {$col_id = $col_id + 1}
				            {/if}
				        {else}
				            <td id="sample_{$i}_col_{$col_id}"><a class="imglink" href="{$sample_results["pbspng"].img}"> {$sample_results["pbspng"].result} </a></td>
			                {$col_id = $col_id + 1}
				        {/if}
				
				        <td id="sample_{$i}_col_{$col_id}">{$sample_results["default"].nbseq|number_format:0:' ':' '}</td>
				        {$col_id = $col_id + 1}
				        
				        {if $metrics}
				            {if $metrics['perseqqual']}
				                <td id="sample_{$i}_col_{$col_id}"><a class="imglink" href="{$sample_results["psqpng"].img}"> {$sample_results["psqpng"].result} </a></td>
				                {$col_id = $col_id + 1}
				            {/if}
				        {else}
				            <td id="sample_{$i}_col_{$col_id}"><a class="imglink" href="{$sample_results["psqpng"].img}"> {$sample_results["psqpng"].result} </a></td>
			                {$col_id = $col_id + 1}
				        {/if}
				        
				        {if $metrics}
				            {if $metrics['seqGC']}
				                <td id="sample_{$i}_col_{$col_id}">{$sample_results["psgcpng"].value}(<a class="imglink" href="{$sample_results["psgcpng"].img}">{$sample_results["psgcpng"].result}</a>)</td>
				                {$col_id = $col_id + 1}
				            {/if}
				        {else}
				            <td id="sample_{$i}_col_{$col_id}">{$sample_results["psgcpng"].value}(<a class="imglink" href="{$sample_results["psgcpng"].img}">{$sample_results["psgcpng"].result}</a>)</td>
			                {$col_id = $col_id + 1}
				        {/if}
				        
				        {if $metrics}
				            {if $metrics['seqlen']}
				                <td id="sample_{$i}_col_{$col_id}">{$sample_results["sldpng"].value}(<a class="imglink" href="{$sample_results["sldpng"].img}">{$sample_results["sldpng"].result}</a>)</td>
				                {$col_id = $col_id + 1}
				            {/if}
			            {else}
			                <td id="sample_{$i}_col_{$col_id}">{$sample_results["sldpng"].value}(<a class="imglink" href="{$sample_results["sldpng"].img}">{$sample_results["sldpng"].result}</a>)</td>
			                {$col_id = $col_id + 1}
			            {/if}
				
				        {if $metrics}
				            {if $metrics['seqduplication']}
				                <td id="sample_{$i}_col_{$col_id}"><a class="imglink" href="{$sample_results["dlpng"].img}"> {$sample_results["dlpng"].result} </a></td>
				                {$col_id = $col_id + 1}
				            {/if}
				        {else}
				            <td id="sample_{$i}_col_{$col_id}"><a class="imglink" href="{$sample_results["dlpng"].img}"> {$sample_results["dlpng"].result} </a></td>
			                {$col_id = $col_id + 1}
				        {/if}
				        
				        {if $metrics}
				            {if $metrics['kmers']}
				                {if $sample_results["kppng"].img}
				                    <td id="sample_{$i}_col_{$col_id}"><a class="imglink" href="{$sample_results["kppng"].img}"> {$sample_results["kppng"].result} </a></td>
				                {else}
				                    <td id="sample_{$i}_col_{$col_id}">{$sample_results["kppng"].result}</td>
				                {/if}
				                {$col_id = $col_id + 1}
				            {/if}
			            {else}
			                {if $sample_results["kppng"].img}
			                    <td id="sample_{$i}_col_{$col_id}"><a class="imglink" href="{$sample_results["kppng"].img}"> {$sample_results["kppng"].result} </a></td>
			                {else}
			                    <td id="sample_{$i}_col_{$col_id}">{$sample_results["kppng"].result}</td>
			                {/if}
			                {$col_id = $col_id + 1}
			            {/if}
				
				        {if $metrics}
				            {if $metrics['seqoverrepresentation']}
				                <td id="sample_{$i}_col_{$col_id}">{$sample_results["default"].seqoverrepresentation}</td>
				                {$col_id = $col_id + 1}
				            {/if}
				        {else}
				            <td id="sample_{$i}_col_{$col_id}">{$sample_results["default"].seqoverrepresentation}</td>
			                {$col_id = $col_id + 1}
				        {/if}

				        {if $metrics}
				            {if $metrics['adaptercontent']}
				                <td id="sample_{$i}_col_{$col_id}"><a class="imglink" href="{$sample_results["acqpng"].img}"> {$sample_results["acqpng"].result} </a></td>
				                {$col_id = $col_id + 1}
				            {/if}
				        {/if}
			        </tr>
			        
			        {$i = $i + 1}
			    {/if}
			{/foreach}
		</tbody>
		<tfoot>
			<tr>
			    <th colspan="2">All metrics : <input type="checkbox" id="check_all_metrics"></th>
				{assign var="th_id" value=2}
				
				{if $metrics}
				    {if $metrics['perbasequal']}
        				<th><center><input type="checkbox" id="chk_col_{$th_id}"></center></th>
				        {$th_id = $th_id +1}
				    {/if}
				{else}
				    <th><center><input type="checkbox" id="chk_col_{$th_id}"></center></th>
			        {$th_id = $th_id +1}
				{/if}
				
				{if $metrics}
				    {if $metrics['baseGC']}
				        <th><center><input type="checkbox" id="chk_col_{$th_id}"></center></th>
				        {$th_id = $th_id +1}
				    {/if}
				{else}
				    <th><center><input type="checkbox" id="chk_col_{$th_id}"></center></th>
			        {$th_id = $th_id +1}
				{/if}
				
				{if $metrics}
				    {if $metrics['nperbase']}
				        <th><center><input type="checkbox" id="chk_col_{$th_id}"></center></th>
				        {$th_id = $th_id +1}
				    {/if}
				{else}
				    <th><center><input type="checkbox" id="chk_col_{$th_id}"></center></th>
			        {$th_id = $th_id +1}
				{/if}
				
				{if $metrics}
				    {if $metrics['seqcontent']}
				        <th><center><input type="checkbox" id="chk_col_{$th_id}"></center></th>
				        {$th_id = $th_id +1}
				    {/if}
				{else}
				    <th><center><input type="checkbox" id="chk_col_{$th_id}"></center></th>
			        {$th_id = $th_id +1}
				{/if}
				
				<th></th>
			    {$th_id = $th_id +1}
				
				{if $metrics}
				    {if $metrics['perseqqual']}
				        <th><center><input type="checkbox" id="chk_col_{$th_id}"></center></th>
				        {$th_id = $th_id +1}
				    {/if}
				{else}
    				<th><center><input type="checkbox" id="chk_col_{$th_id}"></center></th>
			        {$th_id = $th_id +1}
				{/if}
				
				{if $metrics}
				    {if $metrics['seqGC']}
				        <th><center><input type="checkbox" id="chk_col_{$th_id}"></center></th>
				        {$th_id = $th_id +1}
				    {/if}
				{else}
				    <th><center><input type="checkbox" id="chk_col_{$th_id}"></center></th>
			        {$th_id = $th_id +1}
				{/if}
				
				{if $metrics}
				    {if $metrics['seqlen']}
				        <th><center><input type="checkbox" id="chk_col_{$th_id}"></center></th>
				        {$th_id = $th_id +1}
				    {/if}
				{else}
				    <th><center><input type="checkbox" id="chk_col_{$th_id}"></center></th>
			        {$th_id = $th_id +1}
				{/if}
				
				{if $metrics}
				    {if $metrics['seqduplication']}
				        <th><center><input type="checkbox" id="chk_col_{$th_id}"></center></th>
				        {$th_id = $th_id +1}
				    {/if}
				{else}
				    <th><center><input type="checkbox" id="chk_col_{$th_id}"></center></th>
			        {$th_id = $th_id +1}
				{/if}
				
				{if $metrics}
				    {if $metrics['kmers']}
				        <th><center><input type="checkbox" id="chk_col_{$th_id}"></center></th>
				        {$th_id = $th_id +1}
				    {/if}
				{else}
				    <th><center><input type="checkbox" id="chk_col_{$th_id}"></center></th>
				    {$th_id = $th_id +1}
				{/if}
				
				{if $metrics}
				    {if $metrics['seqoverrepresentation']}
				        <th></th>
				        {$th_id = $th_id +1}
				    {/if}
				{else}
				    <th></th>
			        {$th_id = $th_id +1}
				{/if}
				
				{if $metrics}
				    {if $metrics['adaptercontent']}
				        <th><center><input type="checkbox" id="chk_col_{$th_id}"></center></th>
				        {$th_id = $th_id +1}
				    {/if}
				{/if}
			</tr>
			<tr>
				<th align="left" colspan="{$th_id + 2}">
					With selection :
					<button type="button" class="btn btn-default multiple-selection-btn" id="columns_extract"><i class="glyphicon glyphicon-picture"></i> Compare</button>
				</th>
			</tr>
		</tfoot>
	</table>
	
	<div class="tx-nG6-pi1-help">
		<img src="" alt="" class="img" />
		<p>Help for per position statistics :</p>
		<span class="meta">
		<ul>
			<li><strong>Quality</strong> : 
				WARN = A warning will be issued if the lower quartile for any base is less than 10, or if the median for any base is less than 25. 
				FAIL = This module will raise a failure if the lower quartile for any base is less than 5 or if the median for any base is less than 20.
			</li>
			<li><strong>GC%</strong> : 
				WARN = This module issues a warning it the GC content of any base strays more than 5% from the mean GC content. 
				FAIL = This module will fail if the GC content of any base strays more than 10% from the mean GC content.
			</li>
			<li><strong>N content</strong> : 
				This module raises a warning if any position shows an N content of >5%. 
				FAIL = This module will raise an error if any position shows an N content of >20%.
			</li>
			<li><strong>Per base content</strong> : 
				WARN = This module issues a warning if the difference between A and T, or G and C is greater than 10% in any position. 
				FAIL = This module will fail if the difference between A and T, or G and C is greater than 20% in any position.
			</li>
		</ul>
		</span>
	</div>
	<div class="tx-nG6-pi1-help">
		<img src="" alt="" class="img" />
		<p>Help for per sequence statistics :</p>
		<span class="meta">
		<ul>
			<li><strong>Quality</strong> : 
				WARN = A warning is raised if the most frequently observed mean quality is below 27 - this equates to a 0.2% error rate. 
				FAIL = An error is raised if the most frequently observed mean quality is below 20 - this equates to a 1% error rate.
			</li>
			<li><strong>GC%</strong> : 
				WARN = A warning is raised if the sum of the deviations from the normal distribution represents more than 15% of the reads. 
				FAIL = This module will indicate a failure if the sum of the deviations from the normal distribution represents more than 30% of the reads.
			</li>
			<li><strong>Length distribution</strong> : 
				WARN = This module will raise a warning if all sequences are not the same length. 
				FAIL = This module will raise an error if any of the sequences have zero length.
			</li>
			<li><strong>Duplication level</strong> : 
				WARN = This module will issue a warning if non-unique sequences make up more than 20% of the total. 
				FAIL = This module will issue a error if non-unique sequences make up more than 50% of the total.
			</li>
			<li><strong>Kmer profiles</strong> : 
				WARN = This module will issue a warning if any k-mer is enriched more than 3 fold overall, or more than 5 fold at any individual position. 
				FAIL = This module will issue a error if any k-mer is enriched more than 10 fold at any individual base position.
			</li>
			<li><strong>Overrepresented sequences</strong> : 
				WARN = This module will issue a warning if any sequence is found to represent more than 0.1% of the total. 
				FAIL = This module will issue an error if any sequence is found to represent more than 1% of the total.
			</li>
		</ul>
		</span>
	</div>
{/block}
