/***************************************************************
 *  Copyright notice
 *
 *  (c) 2009 PF bioinformatique de Toulouse <>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */


$(function () {
	
	function _enable(elmt, enable){
		if (enable){
			elmt.removeAttr("disabled");
	    }
	    else{
	    	elmt.attr("disabled", 'true');
	    }
	}
	
	function isNumber (o) {
		return ! isNaN (o-0) && o !== null && o.replace(/^\s\s*/, '') !== "" && o !== false;
	}

	/**
	 * generic histogram highchart
	 */
	function _histogram(containerID, labels, values, x_title, y_title, tooltip) {
		var parseInt10 = function(val) { return parseInt(val,10) },
			values = values.split(",").map(parseInt10),
			labels = labels.split(","),
			modified_labels = new Array();
		
		for (var i = 0; i < labels.length; i++) {
			if (labels[i].length > 22) { modified_labels.push(labels[i].substring(0,19) + "...") }
			else                { modified_labels.push(labels[i]) }
		}
		var xAxis = {
			categories: modified_labels,
			labels: {
				rotation: -45,
				align: 'right',
				step : parseInt( modified_labels.length / 20 ),
				formatter: function() {
					var xlabel = this.value;
					if (isNumber(labels[0])) {
						xlabel = Highcharts.numberFormat(this.value, 0);
					}
					return xlabel
				}
			},
			title: { text: x_title }
		}, yAxis = {
			min: 0,
			title: {
				text: y_title
			}
		};
		
		var chartOptions = {
				chart: {
					renderTo: containerID,
					type: 'column',
		            width: 670,
		            spacingLeft: 0,
		            spacingRight: 20
		        },
				title: { text: null },
				credits: { enabled: false },
				xAxis: xAxis,
				yAxis: yAxis,
				legend: {
					enabled: false
				},
		        plotOptions: {
		            bar: {
		                dataLabels: {
		                    enabled: true
		                }
		            }
		        },
				series: [{
					data: values
				}]
		};
		var tt = {};
		if (tooltip){
			tt = {
				formatter: function() {
					if (isNumber(labels[0])) {
						var x = Highcharts.numberFormat(this.x, 0);
					} else {
						var x = this.x;
					}
					return tooltip.replace("###X###", x).replace("###Y###", Highcharts.numberFormat(this.y, 0));
				}	
			};
		}
		else{
			tt = {enabled : false};
		}
		chartOptions.tooltip = tt;
		return new Highcharts.Chart(chartOptions);
	}

	/**
	 * Generic piechart highchart
	 */
	function _piechart (containerID, labels, values, tooltip) {
		var parseInt10 = function(val) { return parseInt(val,10) },
			values = values.split(",").map(parseInt10),
			labels = labels.split(","),
			max = 0,
			max_index = 0,
			sum = 0,
			count = {},
			series = new Array();
		for (var index = 0; index < values.length; index++) {
			if (values[index] > max) {
				max = values[index];
				max_index = index;
			}
			sum += values[index];
		}
		for (var index = 0; index < values.length; index++) {
			if (max_index == index) {
				series.push({
	                name: labels[index],
	                y: Math.round((values[index]/sum)*10000)/100,
	                sliced: true,
	                selected: true
	            });
			} else {
				series.push(new Array(labels[index], Math.round((values[index]/sum)*10000)/100));
			}
			count[labels[index]] = values[index];
		}
		
		var chartOptions = {
		        chart: {
		            renderTo: containerID,
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false,
		            width: 670,
		            spacingLeft: 0,
		            spacingRight: 20
		        },
		        title: { text: null },
				credits: { enabled: false },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    color: '#000000',
		                    formatter: function() {
		                        return '<b>'+ this.point.name +'</b>: <br />'+ this.y +' %';
		                    }
		                }
		            }
		        },
		        series: [{
		            type: 'pie',
		            data: series
		        }]
		};
		
		var tt = {};
		if (tooltip){
			tt = {
				pointFormat: '{series.name}: <b>{point.percentage}%</b>',
	    	    percentageDecimals: 1,
				formatter: function() {
					return tooltip.replace("###X###", this.point.name).replace("###Y###", Highcharts.numberFormat(count[this.point.name], 0));
				}	
			};
		}
		else{
			tt = {enabled : false};
		}
		chartOptions.tooltip = tt;
		return new Highcharts.Chart(chartOptions);
	}
	
	
	/**
	 * Generic linechart drawing 
	 */
	function _linechart_multival(containerID, labels, name_values, x_title, y_title, tooltip){
		var parseInt10 = function(val) { return parseInt(val,10) },
			labels = labels.split(","),
			modified_labels = new Array();
		
		for (var i = 0; i < labels.length; i++) {
			if (labels[i].length > 22) { modified_labels.push(labels[i].substring(0,19) + "...") }
			else                { modified_labels.push(labels[i]) }
		}
		
		var xAxis = {
			categories: modified_labels,
			labels: {
				//rotation: -45,
				align: 'right',
				step : parseInt( modified_labels.length / 20 ),
				formatter: function() {
					var xlabel = this.value;
					if (isNumber(labels[0])) {
						xlabel = Highcharts.numberFormat(this.value, 0);
					}
					return xlabel
				}
			},
			title: { text: x_title }
		}, yAxis = {
			min: 0,
			title: {
				text: y_title
			}
		};
		
		var series = [];
		for(var i = 0 ; i < name_values.length; i+=2){
			series.push({
				name: name_values[i],
                data: name_values[i+1].split(',').map(parseInt10)
			});
		}
		
		var chartOptions = {
				chart: {
					renderTo: containerID,
					defaultSeriesType: 'line',
		            width: 670,
		            spacingLeft: 0,
		            spacingRight: 20
		        },
				title: { text: null },
				credits: { enabled: false },
				xAxis: xAxis,
				yAxis: yAxis,
		        plotOptions: {
		            bar: {
		                dataLabels: {
		                    enabled: true
		                }
		            }
		        },
				series: series
		};
		return new Highcharts.Chart(chartOptions);
	};
	
	
	/**
	 * Enable or disable button if data are available
	 */
	function check_data(){
		
		if ($(":checked[id^=chk_sample_]").size() > 0) {
			
			//Inner distance (insert size)
			var available = ($(":input[id*='innerdist_frequences_']").val() != "") && ($(":input[id*='innerdist_insert_sizes_']").val() != "") ;
			_enable($('#inn_dist-view-btn'), available) ;
			
			// Gene body coverage
	        var available = false;
	        $(":checked[id^=chk_sample_]").each(function () {
	            var index = $(this).attr("id").split("_")[2];
	            var values = $("#gbc_count_" + index ).val();
	            available = values ? available || true : available || false;
	        });
	        _enable($('#gbc-view-btn'), available) ;
			
			if ($(":checked[id^=chk_sample_]").size() == 1) {
				var index = $(":checked[id^=chk_sample_]").attr("id").split("_")[2];
				
				//Junction annotation
				var splincing_data_types = ["juncannot_splicing_junctions", "juncannot_splicing_events"],
					available = false ;
				
	            for (var type in splincing_data_types) {
	                var values = $("#" + splincing_data_types[type] + "_" + index ).val();
	                available = values ? available || true : available || false;
	            }
	            _enable($('#junc_ann-view-btn'), available) ;
	            
	            // Junction saturation 
	            var juncsat_status = ["juncsat_all_junctions", "juncsat_known_junctions", "juncsat_novel_junctions"],
	            	available = false;
	            
	            for (var statut in juncsat_status) {
	                var values = $("#" + juncsat_status[statut] + "_" + index ).val();
	                available = values ? available || true : available || false;
	            }
	            _enable($('#junc_sat-view-btn'), available) ;
			}
		}
	}
	
	check_data();
	$('.rseqc-data-check').change(function(){
		check_data();
	});
	
    /* 
     * Define functions in charge to visualize infer experiment charts
     */
    $("#infer-view-btn").click(function () {
        if ($(":checked[id^=chk_sample_]").size() == 1) {
            $("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
            $('#modal-body-tmpl').html('<div id="infer-experiment-piechart-wrapper" style="width:auto;"> </div> \
            		<div id="infer-experiment-text-wrapper" style="width:auto"> <dl> </dl></div>');
            $("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
            
            var combinations = {
            		"1++,1--,2+-,2-+" : {
            			title : 'PairEnd C1',
            			tooltip : [
            			           "<dd>read1 mapped to ‘+’ strand indicates parental gene on ‘+’ strand</dd>",
            			           "<dd>read1 mapped to ‘-‘ strand indicates parental gene on ‘-‘ strand</dd>",
            			           "<dd>read2 mapped to ‘+’ strand indicates parental gene on ‘-‘ strand</dd>",
            			           "<dd>read2 mapped to ‘-‘ strand indicates parental gene on ‘+’ strand</dd>"
            			           ].join("")
            		},
            		"1+-,1-+,2++,2--" : {
            			title : 'PairEnd C2',
            			tooltip : [ 
            			            "<dd>read1 mapped to ‘+’ strand indicates parental gene on ‘-’ strand</dd>",
            			            "<dd>read1 mapped to ‘-‘ strand indicates parental gene on ‘+‘ strand</dd>",
            			            "<dd>read2 mapped to ‘+’ strand indicates parental gene on ‘+‘ strand</dd>",
            			            "<dd>read2 mapped to ‘-‘ strand indicates parental gene on ‘-’ strand</dd>"
            			            ].join("")
            		},
            		"++,--" : {
            			title : 'SingleEnd C1',
            			tooltip : [ 
            			            "<dd>read mapped to ‘+’ strand indicates parental gene on ‘+’ strand</dd>",
            			            "<dd>read mapped to ‘-‘ strand indicates parental gene on ‘-‘ strand</dd>"
            			           ].join("")
            		},
            		"+-,-+" : {
            			title : 'SingleEnd C2',
            			tooltip : [ 
            			            "<dd>read mapped to ‘+’ strand indicates parental gene on ‘-‘ strand</dd>",
            			            "<dd>read mapped to ‘-‘ strand indicates parental gene on ‘+’ strand</dd>"
            			          ].join("")
            		},
            		'other' : {
            			title : 'other',
            			tooltip : '<dt>Other</dt><dd>Other combinations</dd>'
            		}
            };
            
            var titles = { 'PairEnd C1' : "1++,1--,2+-,2-+" , 'PairEnd C2' : "1+-,1-+,2++,2--", 'SingleEnd C1' : "++,--" , 'SingleEnd C2' : "+-,-+",
            		'other' : 'other'
            };
            
            var index  = $(":checked[id^=chk_sample_]").attr("id").split("_")[2],
            	labels = [],
            	values = [];
            
            var select = $("#reads_orientation_" + index ).val().split(';');
            for (var i in select) {
            	var tmp = select[i].split(':');
            	if(parseFloat(tmp[1]) > 0){
            		labels.push(combinations[tmp[0]].title);
                	values.push(parseFloat(tmp[1]));
                	$(  "<dt>" + combinations[tmp[0]].title + "</dt>" +  combinations[tmp[0]].tooltip ).appendTo($("#infer-experiment-text-wrapper dl"));
            	}
            }
            labels = labels.join(',');
            values = values.join(',');
            var chart = _piechart ('infer-experiment-piechart-wrapper' , labels, values) ;
            resize_center_btmodal('#ng6modal', chart.chartWidth + 50);
            
            $("#ng6modal").modal();
        }
    });

    /* 
     * Define functions insert sizes charts
     */
    $("#inn_dist-view-btn").click(function () {

        if ($(":checked[id^=chk_sample_]").size() == 1) {
            $("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
            $("#modal-body-tmpl").html('<div id="highcharts_container"></div>');
            $("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
            $("#highcharts_container").css('width', '730px');
            resize_center_btmodal('#ng6modal', 750);
            $("#ng6modal").modal();
            
            $(":checked[id^=chk_sample_]").each(function () {
                var index = $(this).attr("id").split("_")[2];
                _histogram('highcharts_container', $("#innerdist_insert_sizes_" + index ).val(), $("#innerdist_frequences_" + index ).val(), 
                		'mRNA insert sizes (bp)', 'Density', 'Insert size <strong>###X###</strong> has a density of <strong>###Y###</strong>')
            });
        }
    });

    /* 
     * Define functions for junction annotation charts 
     */
    $("#junc_ann-view-btn").click(function () {
        if ($(":checked[id^=chk_sample_]").size() == 1) {
            $("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
            $('#modal-body-tmpl').html('\
	            		<div id="juncannot-nav" > \
	        				<ul class="nav nav-tabs " > \
	        					<li id="li-juncannot-junctions" class = "active"><a href="#juncannot-chart-splicing-junctions" data-toggle="tab">Splicing junction</a></li> \
	        					<li id="li-juncannot-events" ><a href="#juncannot-chart-splicing-events" data-toggle="tab">Splicing events</a></li> \
	        				</ul> \
	            		</div> \
	            		<div class="tab-content">\
            				<div id="juncannot-chart-splicing-junctions" class="tab-pane fade in active"></div>\
            				<div id="juncannot-chart-splicing-events" class="tab-pane fade in"></div>\
            			</div>\
            		');
            $("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
            
            var index = $(":checked[id^=chk_sample_]").attr("id").split("_")[2],
            	data_types = ["junctions", "events"];

            for (var i in data_types) {
            	var type   = data_types[i],
            		labels = [],
            		values = [];
            	
                var jvalues = $("#juncannot_splicing_" + type + "_" + index ).val();
                jvalues = jvalues ? jvalues.split(';') : jvalues;
                for (var j  in jvalues ) {
                	var tmp = jvalues[j].split(',');
                	labels.push(tmp[0]);
                	values.push( parseFloat(tmp[1]) );
                }
                _piechart ( 'juncannot-chart-splicing-' + type, labels.join(','),values.join(','), '###X###') ;
            }
            resize_center_btmodal('#ng6modal', 750);
            $("#ng6modal").modal();
        }
    });

    
    /* 
     * Define functions in charge to visualize junction saturation charts
     */
    $("#junc_sat-view-btn").click(function () {
        if ($(":checked[id^=chk_sample_]").size() == 1) {
            $("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
            $("#modal-body-tmpl").html('<div id="juncsat-highcharts-container"></div>');
            $("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
            $("#highcharts_container").css('width', '730px');
            resize_center_btmodal('#ng6modal', 750);

            var index = $(":checked[id^=chk_sample_]").attr("id").split("_")[2];
            var statuts = ["all_junctions", "known_junctions", "novel_junctions"];
            var name_values = [],
            	labels		= [];
            for (var statut in statuts) {
                var junction_values = $("#juncsat_" + statuts[statut] + "_" + index ).val();
                name_values.push(statuts[statut]);
                name_values.push(junction_values);
            }
            
            for (var i in  $("#juncsat_" + statuts[0] + "_" + index ).val().split(',')){
            	labels.push(i);
            }
            labels = labels.join(',');
            
            _linechart_multival('juncsat-highcharts-container', labels, name_values, "Percent of total reads", 'Number of splicing junctions')
            $("#ng6modal").modal();
        }
    });

    /* 
     * Define functions in charge to visualize GeneBodyCoverage charts
     */
    $("#gbc-view-btn").click(function () {

        if ($(":checked[id^=chk_sample_]").size() > 0) {
            $("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
            $("#modal-body-tmpl").html('<div id="gbc-highcharts-container"></div>');
            $("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
            $("#highcharts_container").css('width', '730px');
            resize_center_btmodal('#ng6modal', 750);

            var name_values = [],
        		labels		= [];
            $(":checked[id^=chk_sample_]").each(function () {
                var index = $(this).attr("id").split("_")[2];
                var count_values = $("#gbc_count_" + index ).val();
                var sample_name = $("#sample_" + index ).val();
                name_values.push(sample_name);
                name_values.push(count_values);
            });
	        
	        for (var i in  name_values[1].split(',')){
	        	labels.push(i);
	        }
	        labels = labels.join(',');
	        _linechart_multival('gbc-highcharts-container', labels, name_values, "Percentile of gene body (5'-3')", 'Read count')
            $("#ng6modal").modal();
        }
    });

    
    /* 
     * Define function in charge to visualize RPKM Saturation chart
     */
    $("#rpkm_sat-view-btn").click(function () {
        if ($(":checked[id^=chk_sample_]").size() == 1 ) {
        	$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
            $('#modal-body-tmpl').html('\
				<ul class="nav nav-tabs" > \
					<li class="active"><a href="#div-rpkm-Q1" data-toggle="tab">Q1</a></li> \
            		<li ><a href="#div-rpkm-Q2" data-toggle="tab">Q2</a></li> \
            		<li ><a href="#div-rpkm-Q3" data-toggle="tab">Q3</a></li> \
            		<li ><a href="#div-rpkm-Q4" data-toggle="tab">Q4</a></li> \
				</ul> \
        		<div class="tab-content">\
    				<div id="div-rpkm-Q1" class="tab-pane fade in active"><div id="rpkm-chart-Q1"></div ><p class="tx-nG6-pi1-help">Transcripts with expression level ranked below 25 percentile</p></div>\
            		<div id="div-rpkm-Q2" class="tab-pane fade "><div id="rpkm-chart-Q2"></div><p class="tx-nG6-pi1-help">Transcripts with expression level ranked between 25 percentile and 50 percentile</p></div>\
            		<div id="div-rpkm-Q3" class="tab-pane fade "><div id="rpkm-chart-Q3"></div><p class="tx-nG6-pi1-help">Transcripts with expression level ranked between 50 percentile and 75 percentile</p></div>\
            		<div id="div-rpkm-Q4" class="tab-pane fade "><div id="rpkm-chart-Q4"></div><p class="tx-nG6-pi1-help">Transcripts with expression level ranked above 75 percentile</p></div>\
    			</div>\
            		');
            $("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
            resize_center_btmodal('#ng6modal', 750);
            
            var index = $(":checked[id^=chk_sample_]").attr("id").split("_")[2];
            var quartiles = { "Q1" : 'Q1 (0-25%)', "Q2" : "Q2 (25-50%)", "Q3" : "Q3 (50-75%)", "Q4" : "Q4 (75-100%)" };
            
            for (var qname in quartiles){
            	if (quartiles.hasOwnProperty(qname)){
            		var alldata = $('#quartile_' + qname + '_' + index).val();
            		if (alldata){
            			var options = {
                				chart: {
                                    renderTo: 'rpkm-chart-' + qname,
                                    type: 'boxplot'
                                },
                                yAxis: { title: { text: 'Percent Relative Error' }, min: 0 },
                                xAxis: { categories: [], title: { text: 'Resampling Percentage'}},
                                title: { text: quartiles[qname] },
                                series: [],
                                credits: { enabled: false }	
                    	};
            			var obj 		= JSON.parse(alldata),        		
	            			keys 		= Object.keys(obj).sort(),
	            			categories 	= [],
	            			serie  		= {
	                			name : qname,
	                			data : [],
	                			showInLegend : false
	                		};
	            		
	            		for (var i = 0; i < keys.length; i++){
	            		    var k = keys[i];
	            		    categories.push(k.replace(/S/gi,''));
	            		    serie.data.push(obj[k])
	            		}
	            		options.xAxis.categories = categories ;
	            		options.series.push(serie);
	            		var chart = new Highcharts.Chart(options);
            		}
            	}
            }
            $("#ng6modal").modal();
        }
	});
});

