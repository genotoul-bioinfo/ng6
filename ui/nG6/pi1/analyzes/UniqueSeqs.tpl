{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=results_title} Cleaning results {/block}
{block name=results}

	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th><center><input type="checkbox" id="chk_all_sample"></center></th>
				<th class="string-sort">Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
				<th class="numeric-sort">Before cleaning</th>
				<th class="numeric-sort">After cleaning</th>
				<th class="numeric-sort">Single</th>
				<th class="numeric-sort">% deleted</th>
			</tr>
		</thead>
		<tbody>
			{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
			{assign var="i" value=0}
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
			{$sample_name=$sample}
		   	<tr>
		   		<td><center>
					<input type="checkbox" id="chk_sample_{$i}" value="sample">
				</center></td>	
				<td>{$sample|get_description:$descriptions}</td>
				<td>{$sample_results["default"].nb_reads_begining|number_format:0:' ':' '}</td>
				<td>{$sample_results["default"].nb_reads_end|number_format:0:' ':' '}</td>
				<td>{$sample_results["default"].nb_single|number_format:0:' ':' '}</td>
				<td>{(100.00-((($sample_results["default"].nb_reads_end)*100)/$sample_results["default"].nb_reads_begining))|number_format:2:'.':' '}</td>
				<input type="hidden" id="profile_{$i}" value="{$sample_results["default"].profile}" />
		   	</tr>
		   	{$i = $i +1}
		   	{/foreach}
		</tbody>
		<tfoot>
			<tr>
				{if $analyse_results|@count > 1 }
				<th align="left" colspan="5">
				{else}
				<th align="left" colspan="4">
				{/if}
					With selection :
					<button type="button" class="btn btn-default profile-view-btn single-selection-btn"><i class="glyphicon glyphicon-signal"></i> Profile</button>
				</th>
			</tr>
		</tfoot>
	</table>
	
{/block}
