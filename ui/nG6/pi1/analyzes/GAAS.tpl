{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=results_title} Classifying results {/block}

{block name=results}

	<input type="hidden" id="analysis_directory" value="{(('fileadmin'|cat:$analyse.directory)|cat:'/')|cat:$file}"/>
	{* First find out all database used *}
	{assign var="database" value=array()}
	{foreach from=$analyse_results key=sample item=sample_results}
		{foreach from=$sample_results key=group item=value}
			{if !in_array($group, $database) && $group != "default"}
				{$database[]=$group}
			{/if}
		{/foreach}
	{/foreach}

	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th><center><input type="checkbox" id="chk_all_sample"></center></th>
				<th class="string-sort">Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
				<th class="numeric-sort">Number of sequences</th>
				{foreach $database as $group}
	    			<th class="numeric-sort"> % affiliated with {$group} :</th>
	    		{/foreach}
			</tr>
		</thead>
		<tbody>
			{assign var="total" value=0}
			{assign var="totalaff" value=0}
			{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
			<tr>
				<td><center>
					<input type="checkbox" id="chk_sample_{$sample}" value="sample">
				</center></td>			
				<td>{$sample|get_description:$descriptions}</td>
				{$total=$total+$sample_results["default"].nb_sequences}
				{$totalaff=$totalaff+$sample_results["default"].nb_affiliated}
				<td>{$sample_results["default"].nb_sequences|number_format:0:' ':' '}</td>
				{foreach $database as $group}
					<td>{(($sample_results[$group].nb_sequences*100)/$sample_results["default"].nb_sequences)|number_format:2:'.':' '}%</td>
	    		{/foreach}				
			</tr>
			{/foreach}
		</tbody>
		<tfoot>
			{assign var="colspan" value=3}
			{if $analyse_results|@count > 1 }
			<tr>
				<th></th>
				<th>Total</th>
				<th>{$total|number_format:0:' ':' '}</th>
				{foreach $database as $group}
					<th>/</th>
	    		{/foreach}	
			</tr>
			<tr>
				<th></th>
				<th>Mean</th>
				<th>{($total/($analyse_results|@count))|number_format:0:' ':' '}</th>
				{foreach $database as $group}
					<th>/</th>
	    		{/foreach}	
			</tr>
			{/if}
			{$colspan = $colspan + $analyse_results|@count}
			<tr>
				<th align="left" colspan="{$colspan}">
					With selection :
					<button type="button" class="btn btn-default krona-view-btn single-selection-btn"><span><span> <i class="glyphicon glyphicon-search"></i> Krona view</span></span></button>
				</th>
			</tr>
		</tfoot>
	</table>
{/block}

{block name=download}
	<ul>
		{$dir=$data_folder|cat:$analyse.directory}
		{assign var="nb_files" value=0}
		{foreach $dir|scandir as $file}
		{if $file != "." and $file != "" and $file != ".." and ($file|substr:-strlen(".png")) != ".png" and ($file|substr:-strlen(".html")) != ".html"}
			{$link=(('fileadmin'|cat:$analyse.directory)|cat:'/')|cat:$file}
			<li class="filelist"><a href="{$link}">{$file}</a> </li>
			{$nb_files = $nb_files + 1}
		{/if}
		{/foreach}
	</ul></p>
	{if $nb_files == 0} 
	    <div class="alert alert-info">
			Results folder not synchronized yet...
		</div>
	{/if}
{/block}
