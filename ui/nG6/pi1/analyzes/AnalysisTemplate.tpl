{*
Copyright (C) 2009 INRA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

<input type="hidden" id="analyse_name" value="{$analyse.name}"/>
<input type="hidden" id="view" value="analyze" />
<input type="hidden" id="ids" value="{$analyze_id}" />
<input type="hidden" id="server_url" value="{$server_url}" />
<input type="hidden" id="user_login" value="{$user_login}" />
<input type="hidden" id="user_id" value="{$user_id}" />

<div class="sub-content sc-top">
	{block name=description}
		<div class="ng6-content-header-left analysis">
			<h2>Analysis <small>{$analyse.name}</small></h2>
		</div>
		<div class="ng6-content-header-right">
			{$analyse.description}
			<br />
			All data related to this analysis use <strong> {$analyse_size} </strong> on the hard drive.<br/>
			{block name=description_update}{/block}
			{if $analysis_data_state == 'stored'}
				The analysis data is <strong>stored</strong>. It will be kept until <strong>{$analysis_retention_date}</strong>.<br/>
			{elseif $analysis_data_state == 'purged'}
				The analysis data has been <strong>purged</strong> on <strong>{$analysis_purge_date}</strong>.<br/>
			{elseif $analysis_data_state == 'extended'}
				The analysis data retention has been <strong>extended</strong> to <strong>{$analysis_retention_date}</strong>.<br/>
			{/if}
		</div>
		<div style="clear:both"></div>
	{/block}
</div>


<div class="sub-content sc-bottom">
	{block name=content}

		<ul id="myTab" class="nav nav-tabs">

			{block name=nav_menu_results}
				<li class="active"><a href="#results" data-toggle="tab">{block name=results_title} Results {/block}</a></li>
			{/block}

			{block name=nav_menu_params}
				{if $analyse.params != ""}
				<li><a href="#parameters" data-toggle="tab">{block name=params_title} Parameters {/block}</a></li>
				{/if}
			{/block}

			{block name=nav_menu_downloads}
				<li><a href="#downloads" data-toggle="tab">{block name=downloads_title} Downloads {/block}</a></li>
			{/block}

			{assign var="comment_tab" value="analysis_comments"}
			<li><a href="#{$comment_tab}" data-toggle="tab">Comments</a></li>

			{block name=nav_menu_update}{/block}
		</ul>


		<div id="myTabContent" class="tab-content">

			{block name=tab_content_results}
				<div class="tab-pane fade in active" id="results">
					{block name=results}{/block}
				</div>
			{/block}

			{block name=tab_content_params}
				{if $analyse.params != ""}
					<div class="tab-pane fade" id="parameters">
						{block name=params}
							{block name=params_content}
							{assign var="params" value=";"|explode:$analyse.params}
							<ul>
							    {foreach from=$params item=current_param}
								    <li class="parameter">{$current_param}</li>
								{/foreach}
							</ul>
							{/block}

							{block name=command_line_content}
								{if $is_project_admin && $analyse.is_editable }
								<br/>
								<br/>
								<div class="editable-block">
									<span class="editable-block-head label label-info">Command</span>
									<i>{$analyse.software}</i>
									<span class="editable editable-block-content"
										id="editable-command-line" data-pk="{$analyze_id}"
										data-url="index.php?eID=tx_nG6&type=update_db_field&table=tx_nG6_analyze&field=params" data-type="textarea" >
										{$analyse.params}
									</span>
								</div>
								{/if}
							{/block}
						{/block}
					</div>
				{/if}
			{/block}

			{block name=tab_content_downloads}
				<div class="tab-pane fade" id="downloads">
					{block name=download}
						{assign var="nb_files" value=0}
						{$dir=$data_folder|cat:$analyse.directory}
						{foreach $dir|scandir as $file}
							{assign var="link" value=(('fileadmin'|cat:$analyse.directory)|cat:'/')|cat:$file}
							{if $file != "." and $file != "" and $file != ".." and !is_dir($link)}
								{$nb_files = $nb_files + 1}
							{/if}
						{/foreach}
						{if $nb_files == 0}
							<div class="alert alert-info">
								{if $analyse.data_state=="purged"}
									The data have been purged.  (Retention limit : {$analyse.retention_date|date_format})
								{else}
									Results folder not synchronized yet...
								{/if}
							</div>
						{else}
							<ul>
								<div class="alert alert-info" name="retention-info">
									Retention date is {$analyse.retention_date|date_format}. After this deadline, these data will no longer be available. Only metadata and quality control results will remain in NG6.
								</div>
								<br />
								<div class="alert alert-info" name="DownloadData_analysis">
									<p style="text-align: center;">You can download your files easily with <a href="https://ng6.toulouse.inra.fr/download" target="_blank"><strong>Menu > Download</strong></a>. Please visit the <a href="https://ng6.toulouse.inra.fr/faq" target="_blank"><strong>FAQ</strong></a> > "How to get my data?" section to know more.</p>
								</div>
								{foreach $dir|scandir as $file}
								{assign var="link" value=(('fileadmin'|cat:'/'|cat:$analyse.directory)|cat:'/')|cat:$file}
								{if $file != "." and $file != "" and $file != ".." and !is_dir($link)}
									<li class="filelist"><a href="{$link}">{$file}</a> </li>
								{/if}
								{/foreach}
							</ul>
						{/if}
					{/block}
				</div>
			{/block}

			{include file='../comments.tpl' comments=$comments  tab_id=$comment_tab  add_new_comment=$is_project_admin  user_id=$user_id}

			{block name=tab_content_update}{/block}
		</div>
	{/block}
</div>

{include file='../../template/modals.tpl'}
