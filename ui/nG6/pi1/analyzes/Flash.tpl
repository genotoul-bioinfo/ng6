{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=params}
	{assign var="params" value=" "|explode:$analyse.params}
	{foreach from=$analyse_results key=sample item=sample_results}
	{assign var="sample" value=$sample}
	{/foreach}
	<ul>
		{if in_array("-m", $params)}
		{assign var="mindex" value=$params|@array_keys:"-m"}
		<li class="parameter">An overlap of {$params[$mindex[0]+1]}bp is required between two reads to provide a confident overlap.</li>
		{/if}
		{if in_array("-M", $params)}
		{assign var="m2index" value=$params|@array_keys:"-M"}
		<li class="parameter">An overlap of {$params[$m2index[0]+1]}bp is the maximum overlap length expected in approximately 90% of read pairs. </li>
		{/if}
		{if in_array("-x", $params)}
		{assign var="xindex" value=$params|@array_keys:"-x"}
		<li class="parameter">{$params[$xindex[0]+1]} as allowed ratio of the number of mismatches and the overlap length.</li>
		{/if}
		{if in_array("-r", $params)}
		{assign var="rindex" value=$params|@array_keys:"-r"}
		<li class="parameter">{$params[$rindex[0]+1]}bp is the average reads length.</li>
		{/if}
		{if in_array("-f", $params)}
		{assign var="findex" value=$params|@array_keys:"-f"}
		<li class="parameter">{$params[$findex[0]+1]}bp is the average fragment length.</li>
		{/if}
		{if in_array("-s", $params)}
		{assign var="sindex" value=$params|@array_keys:"-s"}
		<li class="parameter">The standard deviation of fragment lengths : {$params[$sindex[0]+1]}%.</li>
		{/if}
		{if in_array("-p", $params)}
		{assign var="pindex" value=$params|@array_keys:"-p"}
		<li class="parameter">{$params[$pindex[0]+1]} used as phred offest.</li>
		{/if}
	</ul>
{/block}

{block name=results_title} Assembly results {/block}
{block name=results}
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th rowspan="2"><center><input type="checkbox" id="chk_all_sample"></center></th>
				<th class="string-sort" rowspan="2">Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
				<th class="numeric-sort" colspan="2">Extended fragments (R1 associated to R2)</th>
				<th class="numeric-sort" rowspan="2">Not combined fragments (R1+R2)</th>
			</tr>
			<tr>
				<th><center>Count</center></th>
				<th><center>Percentage</center></th>
			</tr>
		</thead>
		<tbody>
			{assign var="i" value=0}
			{assign var="totale" value=0}
			{assign var="total1" value=0}
			{assign var="prct_extend_total" value=0}
			{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
			<tr>
				<td><center>
					<input type="checkbox" id="chk_sample_{$i}" value="sample">
					<input type="hidden" id="size_extended_{$i}" value="{$sample_results["default"].size_extended}"/>
					<input type="hidden" id="nb_size_extended_{$i}" value="{$sample_results["default"].nb_size_extended}"/>
				</center></td>			
				<td id="sample_id_{$i}">{$sample|get_description:$descriptions}</td>
				{$totale=$totale+$sample_results["default"].nb_extended}
				<td>{$sample_results["default"].nb_extended|number_format:0:' ':' '}</td>
				{assign var="prct_extended" value=0}
				{if ($sample_results["default"].nb_extended != 0) || ($sample_results["default"].nb_notcombined1 != 0) }
					{$prct_extended=($sample_results["default"].nb_extended/($sample_results["default"].nb_extended+$sample_results["default"].nb_notcombined1))*100}
				    {$prct_extend_total=$prct_extend_total+$prct_extended}
				{/if}
				<td>{round($prct_extended,2)}</td>
				{assign var="r1r2" value=0}
				{$r1r2=$sample_results["default"].nb_notcombined1 + $sample_results["default"].nb_notcombined2}
				{$total1=$total1+$r1r2}
				<td>{$r1r2|number_format:0:' ':' '}</td>
				{$i = $i +1}
			</tr>
			{/foreach}
		</tbody>
		<tfoot>
			{if $analyse_results|@count > 1 }
			<tr>
				<th colspan="2">Total</th>
				<th>{$totale|number_format:0:' ':' '}</th>
				<th>-</th>
				<th>{$total1|number_format:0:' ':' '}</th>
			</tr>
			<tr>
				<th colspan="2">Mean</th>
				<th>{($totale/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{round(($prct_extend_total/$analyse_results|@count),2)}</th>
				<th>{($total1/($analyse_results|@count))|number_format:0:' ':' '}</th>
			</tr>
			{/if}
			<tr>
				<th align="left" colspan="5">
					With selection :
					<button type="button" class="btn btn-default multiple-selection-btn length-view-btn"><i class="glyphicon glyphicon-signal"></i> Length distribution</button>
				</th>
			</tr>
		</tfoot>
	</table>
{/block}
