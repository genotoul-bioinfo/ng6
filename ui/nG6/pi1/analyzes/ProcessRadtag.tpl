{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=params}
	{assign var="params" value=" "|explode:$analyse.params}
	{foreach from=$analyse_results key=sample item=sample_results}
	{assign var="sample" value=$sample}
	{/foreach}
	<ul>
		{if in_array("-t", $params)}
		{assign var="readlength" value=$params|@array_keys:"-t"}
		<li class="parameter">Truncate final read length to {$params[$readlength[0]+1]}bp.</li>
		{/if}
		{if in_array("-e ", $params)}
		{assign var="enzyme" value=$params|@array_keys:"-e"}
		<li class="parameter">The restriction enzyme used are {$params[$enzyme[0]+1]}. </li>
		{/if}
		{if in_array("-s", $params)}
		{assign var="score" value=$params|@array_keys:"-s"}
		<li class="parameter">The score limit {$params[$score[0]+1]}. If the average score within the sliding window drops below this value, the read is discarded (default 10)</li>
		{/if}
		{if in_array("-r", $params)}
		{assign var="rescue" value=$params|@array_keys:"-r"}
		<li class="parameter">Rescue barcodes and RAD-Tags.</li>
		{/if}
		{if in_array("-q", $params)}
		{assign var="discard" value=$params|@array_keys:"-q"}
		<li class="parameter">Discard reads with low quality scores.</li>
		{/if}
		{if in_array("-D", $params)}
		{assign var="capture" value=$params|@array_keys:"-D"}
		<li class="parameter">Capture discarded reads to a file</li>
		{/if}
		{if in_array("-w", $params)}
		{assign var="size" value=$params|@array_keys:"-w"}
		<li class="parameter">The size of the sliding window {$params[$size[0]+1]} as a fraction of the read length, between 0 and 1 (default 0.15).</li>
		{/if}
		{if in_array("-c", $params)}
		{assign var="clean" value=$params|@array_keys:"-c"}
		<li class="parameter">Clean data, remove any read with an uncalled base.</li>
		{/if}
		{if in_array("--dereplicate", $params)}
		<li class="parameter">Processing clone filter to identify PCR clones</li>
		{/if}
	</ul>
{/block}

{block name=results_title} Preprocessing statistic {/block}
{block name=results}
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th rowspan="2"><center><input type="checkbox" id="chk_all_sample"></center></th>
				<th class="string-sort" rowspan="2">Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
				<th class="numeric-sort" colspan="5"><center>ProcessRadtag</center></th>
				<th class="numeric-sort" colspan="3"><center>Clone Filter</center></th>
			</tr>
			<tr>
				<th class="numeric-sort" style="vertical-align:Middle"><center>InputRead</center></th>
				<th class="numeric-sort" style="vertical-align:Middle"><center>Ambiguous barcode</center></th>
				<th class="numeric-sort" style="vertical-align:Middle"><center>Low quality</center></th>
				<th class="numeric-sort" style="vertical-align:Middle"><center>Ambigous Rad</center></th>
				<th class="numeric-sort" style="vertical-align:Middle"><center>Retained Read</center></th>		
				<th class="numeric-sort" style="vertical-align:Middle"><center>Discard Read</center></th>
				<th class="numeric-sort" style="vertical-align:Middle"><center>Retained Read</center></th>
			</tr>
			
		</thead>
		<tbody>
			{assign var="i" value=0}
			{assign var="totale" value=0}
			{assign var="total1" value=0}
			{assign var="prct_extend_total" value=0}
			{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
			<tr>
				   	<td>
						<center><input type="checkbox" id="chk_sample_{$i}"></center>
							
					</td>
					<td>{$sample|get_description:$descriptions}</td>
					<td>{$sample_results["result"].processinput|number_format:0:' ':' '}</td>
					<td>{$sample_results["result"].processambibarc|number_format:0:' ':' '}</td>
					<td>{$sample_results["result"].processlowqual|number_format:0:' ':' '}</td>
					<td>{$sample_results["result"].processambiRAD|number_format:0:' ':' '}</td>
					<td>{$sample_results["result"].processoutput|number_format:0:' ':' '} - {($sample_results["result"].processoutput/$sample_results["result"].processinput*100)|number_format:0:' ':' '}%</td>
					<td>{$sample_results["result"].clonediscard|number_format:0:' ':' '}</td>
					<td>{$sample_results["result"].cloneoutput|number_format:0:' ':' '} - {($sample_results["result"].cloneoutput/$sample_results["result"].processinput*100)|number_format:0:' ':' '}%</td>
				{$i = $i +1}
				
			</tr>
			{/foreach}
		</tbody>
		<tfoot>
			<tr>
				<th align="left" colspan="5">
					With selection :
					<button type="button" class="btn btn-default multiple-selection-btn length-view-btn" id="length_clonefilter"><i class="glyphicon glyphicon-signal"></i> Length distribution </button>
				</th>
			</tr>
		</tfoot>
	</table>
{/block}
