{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=params}
	{$analyse.params}
	{assign var="params" value=" "|explode:$analyse.params}

	<ul>
		<li class="parameter">For more information on the above parameters, please have a look to the trim_galore documentation : http://www.bioinformatics.babraham.ac.uk/projects/trim_galore/</li>
		<li class="parameter">Some default parameters used by trim_galore! can be absent from the above list, please read the content of the files in the download area to have the full list</li>
	</ul>
{/block}

{block name=results_title}Cleaning results {/block}
{block name=results}
	{assign var="is_paired" value=false}
	{assign var="is_non_directionnal" value=false}
	{assign var="is_trim1" value=false}
	{assign var="is_rrbs" value=false}
	{assign var="is_old_version" value=false}
	{if substr_count($analyse.params, 'paired')}
		{$is_paired=true}
	{/if} 
	{if substr_count($analyse.params, 'non_directional')}
		{$is_non_directionnal=true}
	{/if} 
	
	{if substr_count($analyse.params, 'trim1')}
		{$is_trim1=true}
	{/if} 
	{if substr_count($analyse.params, 'rrbs')}
		{$is_rrbs=true}
	{/if} 
	{if $analyse_results[key($analyse_results)]["default"]["totalPairs"] }
		{$is_old_version=true}
	{/if}
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
					<th>Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
					{if $is_old_version==1}
						<th class="numeric-sort" >Number of pairs</th>
						<th class="numeric-sort" >Number of remaining pairs</th>
						<th class="numeric-sort" >% of removed pairs</th>
					{else}	
						<th class="numeric-sort" >Nb reads</th>
						<th class="numeric-sort" >Nb trimmed reads</th>
						<th class="numeric-sort" >Nb bases</th>
						<th class="numeric-sort" >Nb trimmed bases by qual</th>
						<th class="numeric-sort" >Nb trimmed bases</th>
						{if 'too_short_reads'|array_key_exists:$analyse_results[key($analyse_results)]["default"] }
							<th class="numeric-sort" >Nb reads too short</th>
						{/if}
						
						
						
						{if $is_paired }
							<th >Nb pairs removed</th>
						{/if}
						{if $is_rrbs }
							<th >RRBS reads trimmed by additional 2 bp </th>
						{/if}
						{if $is_non_directionnal }
							<th >reads trimmed start with CAA </th>
							<th >reads trimmed start with CGA </th>
						{/if}
					{/if}
			</tr>
		</thead>
		<tbody>
		{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
		{assign var="total_reads" value=0}
		{assign var="trimmed_reads" value=0}
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
				{if $is_old_version}
					{$total_reads=$total_reads+$sample_results["default"].totalPairs}
					{$trimmed_reads=$trimmed_reads+$sample_results["default"].pairsRemoved}
				{else}
					{$total_reads=$total_reads+$sample_results["default"].total_reads}
					{$trimmed_reads=$trimmed_reads+$sample_results["default"].trimmed_reads}
				{/if}
				<tr>
					<td>{$sample|get_description:$descriptions}</td>
					{if $is_old_version}
						<td>{$sample_results["default"].totalPairs|number_format:0:' ':' '}</td>
						<td>{($sample_results["default"].totalPairs - $sample_results["default"].pairsRemoved)|number_format:0:' ':' '}</td>
						<td>{round(($sample_results["default"].pairsRemoved/$sample_results["default"].totalPairs)*100,2)}</td>
					{else}	
						<td>{$sample_results["default"].total_reads|number_format:0:' ':' '}</td>
						<td>{$sample_results["default"].trimmed_reads|number_format:0:' ':' '}</td>
						<td>{$sample_results["default"].total_bases|number_format:0:' ':' '}</td>
						{if $sample_results["default"].qual_trimmed_bases }
							<td  >{$sample_results["default"].qual_trimmed_bases|number_format:0:' ':' '}</td>
						{else}
							<td >-</td>
						{/if}
						<td>{$sample_results["default"].trimmed_bases|number_format:0:' ':' '}</td>
						
						{if 'too_short_reads'|array_key_exists:$sample_results["default"] }
							<td  >{$sample_results["default"].too_short_reads|number_format:0:' ':' '}</td>
						{/if}
						{if $is_paired }
							{if 'pairs_removed'|array_key_exists:$sample_results["default"] }
								<td  >{$sample_results["default"].pairs_removed|number_format:0:' ':' '}</td>
							{else}
								<td >-</td>
							{/if}
						{/if}
						{if $is_rrbs }
							<td >{$sample_results["default"].rrbs_2bp_apdater|number_format:0:' ':' '}</td>
						{/if}
						{if $is_non_directionnal }
							<td >{$sample_results["default"].rrbs_2bp_read_CAA|number_format:0:' ':' '}</td>
							<td >{$sample_results["default"].rrbs_2bp_read_CGA|number_format:0:' ':' '}</td>
						{/if}
					{/if}
			   	</tr>
		   	{/foreach}
	   	</tbody>
		{if $analyse_results|@count > 1 }
	   	<tfoot>
			<tr>
				<th>Total</th>
				<th>{$total_reads|number_format:0:' ':' '}</th>
				<th>{($total_reads - $trimmed_reads)|number_format:0:' ':' '}</th>
				{if ! $is_old_version}
					<th></th>
					<th></th>
					<th></th>
				{/if}
				{if 'too_short_reads'|array_key_exists:$analyse_results[key($analyse_results)]["default"] }
				    <th></th>
				{/if}
				
				{if $is_paired }
					<th></th>
				{/if}
				{if $is_rrbs }
					<th ></th>
				{/if}
				{if $is_non_directionnal }
					<th ></th>
					<th ></th>
				{/if}
			</tr>
			<tr>
				<th>Mean</th>
				<th>{($total_reads/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{(($total_reads - $trimmed_reads)/($analyse_results|@count))|number_format:0:' ':' '}</th>
				{if ! $is_old_version}
		            <th></th>
		            <th></th>
		            <th></th>
                {/if}
				{if 'too_short_reads'|array_key_exists:$analyse_results[key($analyse_results)]["default"] }
				    <th></th>
				{/if}
				{if $is_paired }
					<th></th>
				{/if}
				{if $is_rrbs }
					<th ></th>
				{/if}
				{if $is_non_directionnal }
					<th ></th>
					<th ></th>
				{/if}
			</tr>
		</tfoot>
		{/if}
	</table>
{/block}
