{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

<div class="sub-content sc-top clearfix">
	<div class="ng6-content-header-left run">
		<h2>Analysis <small>--</small></h2>
	</div>
	<div class="ng6-content-header-right">
	    <div class="alert alert-warning" > <strong>Access denied !</strong>Sorry you don't have access to this Analysis. Please select another analysis</div>
	</div>
</div>

<div class="sub-content sc-bottom">&nbsp;</div>
