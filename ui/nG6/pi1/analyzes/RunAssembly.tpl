{*
Copyright (C) 2009 INRA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=content}

	<ul id="myTab" class="nav nav-tabs">
		<li class="active"><a href="#general" data-toggle="tab">{block name=results_title} General information {/block}</a></li>
		{* If not a cdna assembly, display contigs info *}
		{if !in_array("-cdna", $params)}
		<li><a href="#contigs" data-toggle="tab">{block name=params_title} Contigs {/block}</a></li>
		{* If it's a cdna assembly display isotigs and isogroups information *}
		{else}
		<li><a href="#isogroups" data-toggle="tab">{block name=params_title} Isogroups {/block}</a></li>
		<li><a href="#isotigs" data-toggle="tab">{block name=params_title} Isotigs {/block}</a></li>
		{/if}
		<li><a href="#ace" data-toggle="tab">{block name=params_title} Ace information {/block}</a></li>
		<li><a href="#parameters" data-toggle="tab">{block name=params_title} Parameters {/block}</a></li>
		<li><a href="#downloads" data-toggle="tab">{block name=downloads_title} Downloads {/block}</a></li>
	</ul>

	{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
	<div id="myTabContent" class="tab-content">

		<div class="tab-pane fade in active" id="general">
			{assign var="reads" value=0}
			{assign var="bases" value=0}
			{assign var="rerror" value=0}
			{assign var="assembled" value=0}
			{assign var="partial" value=0}
			{assign var="single" value=0}
			{assign var="repeat" value=0}
			{assign var="outlier" value=0}
			{assign var="short" value=0}
			<table class="table table-striped table-bordered dataTable analysis-result-table">
				<thead>
				<tr>
					<th class="string-sort">Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
					<th class="numeric-sort">Aligned reads</th>
					<th class="numeric-sort">Aligned bases</th>
					<th class="numeric-sort">Inferred read error</th>
					<th class="numeric-sort">Assembled reads</th>
					<th class="numeric-sort">Partially assembled reads</th>
					<th class="numeric-sort">Singletons</th>
					<th class="numeric-sort">Repeated reads</th>
					<th class="numeric-sort">Outlier reads</th>
					<th class="numeric-sort">Too short reads</th>
				</tr>
				</thead>
				<tbody>
				{foreach from=$analyse_results_sorted key=sample item=sample_results}
				<tr>
					<td>{$sample|get_description:$descriptions}</td>
					{assign var="aligned" value=" "|explode:$sample_results["readStatus"].numAlignedReads}
					<td>{$aligned[0]|number_format:0:' ':' '} {$aligned[1]}</td>
					{assign var="alignedbases" value=" "|explode:$sample_results["readStatus"].numAlignedBases}
					<td>{$alignedbases[0]|number_format:0:' ':' '} {$alignedbases[1]}</td>
					{assign var="error" value=" "|explode:$sample_results["readStatus"].inferredReadError}
					<td>{$error[0]|number_format:0:' ':' '} {$error[1]}</td>
					<td>{$sample_results["readStatus"].numberAssembled|number_format:0:' ':' '}</td>
					<td>{$sample_results["readStatus"].numberPartial|number_format:0:' ':' '}</td>
					<td>{$sample_results["readStatus"].numberSingleton|number_format:0:' ':' '}</td>
					<td>{$sample_results["readStatus"].numberRepeat|number_format:0:' ':' '}</td>
					<td>{$sample_results["readStatus"].numberOutlier|number_format:0:' ':' '}</td>
					<td>{$sample_results["readStatus"].numberTooShort|number_format:0:' ':' '}</td>
				</tr>
				{$reads=$reads+$aligned[0]}
				{$bases=$bases+$alignedbases[0]}
				{$rerror=$rerror+$error[0]}
				{$assembled=$assembled+$sample_results["readStatus"].numberAssembled}
				{$partial=$partial+$sample_results["readStatus"].numberPartial}
				{$single=$single+$sample_results["readStatus"].numberSingleton}
				{$repeat=$repeat+$sample_results["readStatus"].numberRepeat}
				{$outlier=$outlier+$sample_results["readStatus"].numberOutlier}
				{$short=$short+$sample_results["readStatus"].numberTooShort}
				{/foreach}
				</tbody>
				{if $analyse_results|@count > 1 }
				<tfoot>
				<tr class="tx-nG6-pi1-listrowlast">
					<th>Total</td>
					<th>{$reads|number_format:0:' ':' '}</th>
					<th>{$bases|number_format:0:' ':' '}</th>
					<th>{$rerror|number_format:0:' ':' '}</th>
					<th>{$assembled|number_format:0:' ':' '}</th>
					<th>{$partial|number_format:0:' ':' '}</th>
					<th>{$single|number_format:0:' ':' '}</th>
					<th>{$repeat|number_format:0:' ':' '}</th>
					<th>{$outlier|number_format:0:' ':' '}</th>
					<th>{$short|number_format:0:' ':' '}</th>
				</tr>
				<tr class="tx-nG6-pi1-listrowlast">
					<th>Mean</td>
					<th>{($reads/($analyse_results|@count))|number_format:0:' ':' '}</th>
					<th>{($bases/($analyse_results|@count))|number_format:0:' ':' '}</th>
					<th>{($rerror/($analyse_results|@count))|number_format:0:' ':' '}</th>
					<th>{($assembled/($analyse_results|@count))|number_format:0:' ':' '}</th>
					<th>{($partial/($analyse_results|@count))|number_format:0:' ':' '}</th>
					<th>{($single/($analyse_results|@count))|number_format:0:' ':' '}</th>
					<th>{($repeat/($analyse_results|@count))|number_format:0:' ':' '}</th>
					<th>{($outlier/($analyse_results|@count))|number_format:0:' ':' '}</th>
					<th>{($short/($analyse_results|@count))|number_format:0:' ':' '}</th>
				</tr>
				</tfoot>
				{/if}
			</table>
		</div>


		{* If not a cdna assembly, display contigs info *}
		{if !in_array("-cdna", $params)}
		<div class="tab-pane fade" id="contigs">
			{assign var="all_contigs" value=0}
			{assign var="all_bases" value=0}
			{assign var="large_contigs" value=0}
			{assign var="large_bases" value=0}
			{assign var="nb_q40" value=0}
			{assign var="nb_q39" value=0}
			<table id="contigs-table" class="table table-striped table-bordered dataTable analysis-result-table">
				<thead>
				<tr>
					<th rowspan="2">Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
					<th colspan="2">All contigs</th>
					<th colspan="7">Large contigs</th>
				</tr>
				<tr>
					<th>Number</th>
					<th>Bases</th>
					<th>Number</th>
					<th>Bases</th>
					<th>Mean length</th>
					<th>N50</th>
					<th>Max length</th>
					<th>Bases with quality ≥ 40</th>
					<th>Bases with quality &lt; 40</th>
				</tr>
				</thead>
				<tbody>
				{foreach from=$analyse_results_sorted key=sample item=sample_results}
				{$all_contigs=$all_contigs+$sample_results["allContigMetrics"].numberOfContigs}
				{$all_bases=$all_bases+$sample_results["allContigMetrics"].numberOfBases}
				{$large_contigs=$large_contigs+$sample_results["largeContigMetrics"].numberOfContigs}
				{$large_bases=$large_bases+$sample_results["largeContigMetrics"].numberOfBases}
				<tr>
					<td>{$sample|get_description:$descriptions}</td>
					<td>{$sample_results["allContigMetrics"].numberOfContigs|number_format:0:' ':' '}</td>
					<td>{$sample_results["allContigMetrics"].numberOfBases|number_format:0:' ':' '}</td>
					<td>{$sample_results["largeContigMetrics"].numberOfContigs|number_format:0:' ':' '}</td>
					<td>{$sample_results["largeContigMetrics"].numberOfBases|number_format:0:' ':' '}</td>
					<td>{$sample_results["largeContigMetrics"].avgContigSize|number_format:0:' ':' '}</td>
					<td>{$sample_results["largeContigMetrics"].N50ContigSize|number_format:0:' ':' '}</td>
					<td>{$sample_results["largeContigMetrics"].largestContigSize|number_format:0:' ':' '}</td>
					{assign var="q40" value=" "|explode:$sample_results["largeContigMetrics"].Q40PlusBases}
					<td>{$q40[0]|number_format:0:' ':' '} {$q40[1]}</td>
					{assign var="q39" value=" "|explode:$sample_results["largeContigMetrics"].Q39MinusBases}
					<td>{$q39[0]|number_format:0:' ':' '} {$q39[1]}</td>
					{$nb_q40=$nb_q40+$q40[0]}
					{$nb_q39=$nb_q39+$q39[0]}
				</tr>
				{/foreach}
				</tbody>
				{if $analyse_results|@count > 1 }
				<tfoot>
				<tr class="tx-nG6-pi1-listrowlast">
					<th>Total</th>
					<th>{$all_contigs|number_format:0:' ':' '}</th>
					<th>{$all_bases|number_format:0:' ':' '}</th>
					<th>{$large_contigs|number_format:0:' ':' '}</th>
					<th>{$large_bases|number_format:0:' ':' '}</th>
					<th>/</th>
					<th>/</th>
					<th>/</th>
					<th>{$nb_q40|number_format:0:' ':' '}</th>
					<th>{$nb_q39|number_format:0:' ':' '}</th>
				</tr>
				<tr class="tx-nG6-pi1-listrowlast">
					<th>Mean</th>
					<th>{($all_contigs/($analyse_results|@count))|number_format:0:' ':' '}</th>
					<th>{($all_bases/($analyse_results|@count))|number_format:0:' ':' '}</th>
					<th>{($large_contigs/($analyse_results|@count))|number_format:0:' ':' '}</th>
					<th>{($large_bases/($analyse_results|@count))|number_format:0:' ':' '}</th>
					<th>/</th>
					<th>/</th>
					<th>/</th>
					<th>{($nb_q40/($analyse_results|@count))|number_format:0:' ':' '}</th>
					<th>{($nb_q39/($analyse_results|@count))|number_format:0:' ':' '}</th>
				</tr>
				</tfoot>
				{/if}
			</table>
		</div>
		{* If it's a cdna assembly display isotigs and isogroups information *}
		{else}
		<div class="tab-pane fade" id="isogroups">
			<div class="alert alert-info">
				An isogroup represents a gene (cf Roche manual : GSFLXSystemSoftwareManual_PartC_Assembler-Mapper-SFFTools.pdf partie 5.2).
	    	</div>
	    	<table id="isogroups-table" class="table table-striped table-bordered dataTable analysis-result-table">
			<thead>
				<tr>
					<th>Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
					<th>Number</th>
					<th>Mean contigs in isogroup</th>
					<th>Max contigs in isogroup</th>
					<th>Isogroups with one contig</th>
					<th>Mean isotigs in isogroup</th>
					<th>Max Isotigs in isogroup</th>
					<th>Isogroup with one isotig</th>
				</tr>
			</thead>
			<tbody>
				{foreach from=$analyse_results_sorted key=sample item=sample_results}
				<tr>
					<td>{$sample|get_description:$descriptions}</td>
					<td>{$sample_results["isogroupMetrics"].numberOfIsogroups|number_format:0:' ':' '}</td>
					<td>{$sample_results["isogroupMetrics"].avgContigCnt|number_format:0:' ':' '}</td>
					<td>{$sample_results["isogroupMetrics"].largestContigCnt|number_format:0:' ':' '}</td>
					<td>{$sample_results["isogroupMetrics"].numberWithOneContig|number_format:0:' ':' '}</td>
					<td>{$sample_results["isogroupMetrics"].avgIsotigCnt|number_format:0:' ':' '}</td>
					<td>{$sample_results["isogroupMetrics"].largestIsotigCnt|number_format:0:' ':' '}</td>
					<td>{$sample_results["isogroupMetrics"].numberWithOneIsotig|number_format:0:' ':' '}</td>
				</tr>
				{/foreach}
			</tbody>
			</table>
		</div>
		<div class="tab-pane fade" id="isotigs">
			<div class="alert alert-info">
				An isotig represent a splice variant (cf Roche manual : GSFLXSystemSoftwareManual_PartC_Assembler-Mapper-SFFTools.pdf partie 5.2). <br />
			</div>
			<table id="isotigs-table" class="table table-striped table-bordered dataTable analysis-result-table">
			<thead>
				<tr>
					<th>Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
					<th>Number</th>
					<th>Max contigs in isotig</th>
					<th>Isotigs with one contig</th>
					<th>Number of bases</th>
					<th>Mean length</th>
					<th>Max length</th>
					<th>N50</th>
				</tr>
			</thead>
			<tbody>
				{foreach from=$analyse_results_sorted key=sample item=sample_results}
				<tr>
					<td>{$sample|get_description:$descriptions}</td>
					<td>{$sample_results["isotigMetrics"].numberOfIsotigs|number_format:0:' ':' '}</td>
					<td>{$sample_results["isotigMetrics"].largestContigCnt|number_format:0:' ':' '}</td>
					<td>{$sample_results["isotigMetrics"].numberWithOneContig|number_format:0:' ':' '}</td>
					<td>{$sample_results["isotigMetrics"].numberOfBases|number_format:0:' ':' '}</td>
					<td>{$sample_results["isotigMetrics"].avgIsotigSize|number_format:0:' ':' '}</td>
					<td>{$sample_results["isotigMetrics"].largestIsotigSize|number_format:0:' ':' '}</td>
					<td>{$sample_results["isotigMetrics"].N50IsotigSize|number_format:0:' ':' '}</td>
				</tr>
				{/foreach}
			</tbody>
			</table>
		</div>
		{/if}

		{assign var="has_js_graph" value=false}
		{foreach from=$analyse_results_sorted key=sample item=sample_results}
			{if $sample_results["aceDepth"].depthpng_x AND $sample_results["aceDepth"].depthpng_y}
				{assign var="has_js_graph" value=true}
			{/if}
		{/foreach}
		<div class="tab-pane fade" id="ace">
			{assign var="nb_contigs" value=0}
			{assign var="nb_reads" value=0}
			<table id="ace-table" class="table table-striped table-bordered dataTable analysis-result-table">
				<thead>
				<tr>
					<th rowspan="2"><center><input type="checkbox" id="chk_all_sample"></center></th>
					<th rowspan="2">Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
					<th rowspan="2">Number of contigs</th>
					<th rowspan="2">Reads</th>
					<th colspan="4">Length</th>
					<th colspan="4">Depth</th>
				</tr>
				<tr>
					<th>Min</th>
					<th>Max</th>
					<th>Median</th>
					<th>Mean</th>
					<th>Min</th>
					<th>Max</th>
					<th>Median</th>
					<th>Mean</th>
				</tr>
				</thead>
				<tbody>
				{assign var="i" value=0}
				{foreach from=$analyse_results_sorted key=sample item=sample_results}
				{$sample_name=$sample}
				{$nb_contigs=$nb_contigs+$sample_results["aceContigs"].nbcontigs}
				{$nb_reads=$nb_reads+$sample_results["aceContigs"].nbreads}
				<tr>
					<td><center> <input type="checkbox" id="chk_sample_{$i}" value="sample"></center></td>
					<td id="sample_name_{$i}">{$sample|get_description:$descriptions}</td>
					<td>{$sample_results["aceContigs"].nbcontigs|number_format:0:' ':' '}</td>
					<td>{$sample_results["aceContigs"].nbreads|number_format:0:' ':' '}</td>
					<td>{$sample_results["aceContigs"].minlength|number_format:0:' ':' '}</td>
					<td>{$sample_results["aceContigs"].maxlength|number_format:0:' ':' '}</td>
					<td>{$sample_results["aceContigs"].medlength|number_format:0:' ':' '}</td>
					<td>{$sample_results["aceContigs"].meanlength|number_format:0:' ':' '}</td>
					<input type="hidden" id="length_{$i}" value="{$sample_results["aceContigs"].lengthpng}" />
					<input type="hidden" id="length_x_{$i}" value="{$sample_results["aceContigs"].lengthpng_x}"/>
					<input type="hidden" id="length_y_{$i}" value="{$sample_results["aceContigs"].lengthpng_y}"/>
					<td>{$sample_results["aceDepth"].mindepth|number_format:0:' ':' '}</td>
					<td>{$sample_results["aceDepth"].maxdepth|number_format:0:' ':' '}</td>
					<td>{$sample_results["aceDepth"].meddepth|number_format:0:' ':' '}</td>
					<td>{$sample_results["aceDepth"].meandepth|number_format:0:' ':' '}</td>
					<input type="hidden" id="depth_{$i}" value="{$sample_results["aceDepth"].depthpng}" />
					<input type="hidden" id="depth_x_{$i}" value="{$sample_results["aceDepth"].depthpng_x}"/>
					<input type="hidden" id="depth_y_{$i}" value="{$sample_results["aceDepth"].depthpng_y}"/>
				</tr>
				{$i = $i +1}
				{/foreach}
				</tbody>

				<tfoot>
				{if $analyse_results|@count > 1 }
				<tr>
					<th></th>
					<th>Total</th>
					<th>{$nb_contigs|number_format:0:' ':' '}</th>
					<th>{$nb_reads|number_format:0:' ':' '}</th>
					<th>/</th>
					<th>/</th>
					<th>/</th>
					<th>/</th>
					<th>/</th>
					<th>/</th>
					<th>/</th>
					<th>/</th>
				</tr>
				<tr class="tx-nG6-pi1-listrowlast">
					<th></th>
					<th>Mean</th>
					<th>{($nb_contigs/($analyse_results|@count))|number_format:0:' ':' '}</th>
					<th>{($nb_reads/($analyse_results|@count))|number_format:0:' ':' '}</th>
					<th>/</th>
					<th>/</th>
					<th>/</th>
					<th>/</th>
					<th>/</th>
					<th>/</th>
					<th>/</th>
					<th>/</th>
				</tr>
				{/if}
				<tr>
					<th align="left" colspan="12">
						With selection :
						{if $has_js_graph}
						<button type="button" class="btn js-length-btn single-selection-btn"><i class="icon-signal"></i> Length</button>
						<button type="button" class="btn js-depth-btn multiple-selection-btn"><i class="icon-signal"></i> Depth</button>
						{else}
						<button type="button" class="btn length-btn single-selection-btn"><i class="icon-signal"></i> Length</button>
						<button type="button" class="btn depth-btn single-selection-btn"><i class="icon-signal"></i> Depth</button>
						{/if}
					</th>
				</tr>
				</tfoot>
			</table>
		</div>

		<div class="tab-pane fade" id="parameters">
			{if $analyse.params != ""}
			<ul>
				<li class="parameter">{$analyse.params}</li>
			</ul>
			{/if}
		</div>

		<div class="tab-pane fade" id="downloads">
			<ul>
				{$dir=$data_folder|cat:$analyse.directory}
				{assign var="nb_files" value=0}
				{foreach $dir|scandir as $file}
				{if $file != "." and $file != "" and $file != ".." and ($file|substr:-strlen(".png")) != ".png"}
					{$link=(('fileadmin'|cat:$analyse.directory)|cat:'/')|cat:$file}
					<li class="filelist"><a href="{$link}">{$file}</a> </li>
					{$nb_files = $nb_files + 1}
				{/if}
				{/foreach}
			</ul>
			{if $nb_files == 0}
				<div class="alert alert-info">
					Results folder not synchronized yet...
				</div>
			{/if}
		</div>

	</div>
{/block}
