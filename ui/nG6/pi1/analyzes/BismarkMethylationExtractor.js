/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */
	
/* 
 * Resize the HTML element.
 * @param HTMLElement - The element to resize.
 * @param float - The percentage of the initial size of the element.
 */
function resize(elt, ratio) {
	elt.width = ratio*elt.width ;
}

$(function () {
    /* Opens a window that all the element in the selected column */
    $("#columns_extract").click(function() {
    	var samples = new Array() ;
		$("input[id^='chk_sample_']:checked").each( function() {
    		samples.push( parseInt($(this).attr("id").split('_')[2]) ) ;
    	}) ;
    	var columns = new Array() ;
    	$("input[id^='chk_col_']:checked").each( function() {
    		columns.push( parseInt($(this).attr("id").split('_')[2]) ) ;
    	}) ;
		
		if( samples.length != 0 ) {
			if( $('#sample_1_col_1') !== 'undefined' ) {
				columns.push( 1 ) ;
			}
			columns.sort(function(a,b){return a-b}) ;
			samples.sort(function(a,b){return a-b}) ;
			
			//Build thead
			var thead = '<tr>' ;
			if( jQuery.inArray( 1, columns) != -1 ) {
				thead += '<th>Samples</th>' ;
			}
			if( jQuery.inArray( 12, columns) != -1 ) {
				thead += '<th><center>M-Bias R1</center></th>' ;
			}
			if( jQuery.inArray( 13, columns) != -1 ) {
				thead += '<th><center>M-Bias R2</center></th>' ;
			}
			thead += '</tr>' ;	
			thead = '<thead>' + thead + '</thead>' ;
			
			//Build tbody
			var tbody = "" ; 
			var pattern_regexp = new RegExp( '<a[^>]+class="[^"]*imglink[^"]*"[^>]+href="(fileadmin\/data\/analyze\/[^"]+)' ) ;
			for( var i = 0 ; i < samples.length ; i++ ) {
				tbody += '<tr>' ; 
				for( var j = 0 ; j < columns.length ; j++ ) {
					var new_html = $('#sample_' + samples[i] + '_col_' + columns[j]).html() ;
					var inner_exp = pattern_regexp.exec( new_html ) ;
					// If the cell match the pattern
					if( inner_exp != null )
					{
						if( inner_exp[1] === undefined ) {
							inner_exp[1] = "" ;
						}
						new_html = '<img src="' + inner_exp[1] + '" onload="resize(this, 0.5)">' ;
					}	
					tbody += '<td>' + new_html + '</td>' ;
				}	
				tbody += '</tr>' ; 
			}
			tbody = '<tbody>' + tbody + '</tbody>' ;
	
			// Display images table
			$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
			$("#modal-body-tmpl").html('<div id="table_container" style="overflow-x : auto"></div>');
			$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
			$("#table_container ").css({ 
				'max-height' : modal_height(200, 50)+'px', 
				'overflow-x' : 'auto',
				'overflow-y' : 'auto'
			});
			$("#ng6modal .modal-content").css('width' , '870px');
			$("#ng6modal").css('margin-left', '-135px');
			$("#table_container").append( '<table class="table table-striped table-bordered ">' + thead + tbody + '</table>' );
			
			$("#ng6modal").modal();
		}
    }) ;


	/**
	 * Fonction pour l'affichage du nombre de C analysé
 	*/

	
    /* 
    * Define functions in charge to visualize charts
    */
	$(".length-view-btn").click(function() {
	   	if ($(":checked[id^=chk_sample_]").size() > 0) {
			// Set dialog window
			$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
    		$("#modal-body-tmpl").html('<div id="highcharts_container"></div>');
    		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
    		$("#highcharts_container").css('width', '845px');

    		// Set graph
			var description = new Array();
			var valeur = new Array();
            $(":checked[id^=chk_sample_]").each(function(){
            	var index = $(this).attr("id").split("_")[2] ;
            	
                var index_description = $("#descrip_" + index).val() ;
                description.push(index_description);
                
                var value_nombreC     = $("#nb_C_" + index).val() ;
                valeur.push(parseInt(value_nombreC));
            });
	       	
            var options = {
				chart: {
					renderTo: 'highcharts_container', 
					type: 'column',
					margin: [50, 50, 100, 80]
				},
				title: {
					text: 'Number of C analysed :'
				},
				xAxis: {
					categories: description,
					labels: {
						rotation: -45,
						align: 'right',
						style: {
							fontSize: '13px',
							fontFamily: 'Verdana, sans-serif'
						}
					}
				},
				yAxis: {
					min: 0,
					title: {
						text: '# C analysed'
					}
				},
				legend: {
					enabled: false
				},
				tooltip: {
					pointFormat: '# C analysed',
				},
				series: [{
					name: 'NumberC',
					data: valeur,
					dataLabels: {
						enabled: true,
						rotation: -90,
						color: '#FFFFFF',
						align: 'right',
						x: 4,
						y: 10,
						style: {
							fontSize: '13px',
							fontFamily: 'Verdana, sans-serif',
							textShadow: '0 0 3px black'
						}
					}
				}]
			};
            
            // Draw graph
			var chart = new Highcharts.Chart(options);
			resize_center_btmodal('#ng6modal', chart.chartWidth + 50);
			
			//Display
			$("#ng6modal").modal();
    	}
	});



	/**
	 * Fonction pour l'affichage graphique du % de methylation tout contexte
	 */

	
    /* 
    * Define functions in charge to visualize charts
    */
	
	$("#percent_graph").click(function() {
	   	if ($(":checked[id^=chk_sample_]").size() > 0) {
			// Set dialog window
	   		
			$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
    		$("#modal-body-tmpl").html('<div id="highcharts_container"></div>');
    		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
    		$("#highcharts_container").css('width', '845px');
    		
    		// Set graph
			var description = new Array();
			var valeurCPG = new Array();
			var valeurCHG = new Array();
			var valeurCHH = new Array();
			
            $(":checked[id^=chk_sample_]").each(function(){
            	var index = $(this).attr("id").split("_")[2] ;
            	
                var index_description = $("#descrip_" + index).val() ;
                description.push(index_description);
                
                // tableau pour les valeurs de c en contexte CpG
                var V_PercentCinCPG = $("#PercentCinCPG_" + index).val() ;
                valeurCPG.push(parseFloat(V_PercentCinCPG));
                // tableau pour les valeurs de c en contexte CHG
                var V_PercentCinCHG = $("#PercentCinCHG_" + index).val() ;
                valeurCHG.push(parseFloat(V_PercentCinCHG));
                // tableau pour les valeurs de c en contexte CHH
                var V_PercentCinCHH = $("#PercentCinCHH_" + index).val() ;
                valeurCHH.push(parseFloat(V_PercentCinCHH));
                
            });
	       	
	       	// Draw graph
			var options = {
				chart: {
						renderTo: 'highcharts_container', 
						type: 'column',
						margin: [50, 50, 100, 80]
					},
				title: {
					text: '% methylation in context CPG, CHG and CHH :'
				},
				xAxis: {
					categories: description,
					labels: {
						rotation: -45,
						align: 'right',
						style: {
							fontSize: '13px',
							fontFamily: 'Verdana, sans-serif'
						}
					}
				},
				yAxis: {
					min: 0,
					title: {
						text: 'Percent'
					}
				},
				tooltip: {
					headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
					pointFormat:  '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
									 '<td style="padding:0"><b>{point.y:.2f}%</b></td></tr>',
					footerFormat: '</table>',
					shared: true,
					useHTML: true
				},
				plotOptions: {
					column: {
						pointPadding: 0.2,
						borderWidth: 0
					}
				},
				series: [
				{ name: '% C in CPG', data: valeurCPG }, 
				{ name: '% C in CHG', data: valeurCHG },
				{ name: '% C in CHH', data: valeurCHH }]
			};
			// Draw graph
			var chart = new Highcharts.Chart(options);
			resize_center_btmodal('#ng6modal', chart.chartWidth + 50);
			//Display
			$("#ng6modal").modal();
    	}
	});

});



