{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=results_title} Classification results {/block}
{block name=results}
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th><input type="checkbox" id="chk_all_sample"></th>
				<th>Observation File</th>
				<th class="string-sort">Samples</th>
			</tr>
		</thead>
		<tbody>
			{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
			{assign var="i" value=0}
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
				{foreach from=$sample_results key=biom item=sample_biom_results}
				<tr>
			   		<td>
						<input type="checkbox" id="chk_sample_{$i}" value="sample">
						<input type="hidden" id="krona_file_{$i}" value="{$sample_biom_results.html}"/>
					</td>
					<td>{$biom}</td>
					<td id="sample_id_{$i}">{$sample|get_description:$descriptions}</td>
				</tr>
				{$i = $i +1}
				{/foreach}
			{/foreach}
		</tbody>
		<tfoot>
			<tr>
				<th align="left" colspan="3"> 
					With selection :
					<button type="button" class="btn btn-default single-selection-btn krona-view-btn"><i class="glyphicon glyphicon-search"></i> Krona view</button>			
				</th>
			</tr>
		</tfoot>
	</table>
{/block}
