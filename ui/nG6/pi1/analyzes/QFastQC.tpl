
{extends file='AnalysisTemplate.tpl'}

{block name=results_title} Reads and quality statistics {/block}

{block name=results}
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th class="string-sort" >Sample</th>
				<th class="string-sort" >Per base quality</th>
				<th class="string-sort" >Per base sequence content</th>
			</tr>
		</thead>
		
		{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
		<tbody>
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
		        <tr>
			        <td class="sample_name">{$sample|get_description:$descriptions}</td>
			        <td><a class="image-link" href="{$sample_results["pbqpng"].img}"> {$sample_results["pbqpng"].result} </a></td>
			        <td><a class="image-link" href="{$sample_results["pbspng"].img}"> {$sample_results["pbspng"].result} </a></td>
		        </tr>
			{/foreach}
		</tbody>
	</table>
{/block}
