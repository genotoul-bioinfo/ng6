/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */

$(function () {
	
	var DUPLICATION_LIMIT = 20;
	
	$(".js-duplication-profile-btn").click(function() {
    	if ($(":checked[id^=chk_sample_]").size() > 0) {
        	// First confirm with the user if he wants to hide
    		$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
    		$("#modal-body-tmpl").html('<div id="highcharts_container"></div>');
    		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
    		resize_center_btmodal('#ng6modal', 750);
	       	var categories = new Array(),
	       		series = new Array();
	       	for (var i=1; i<=DUPLICATION_LIMIT; i++) {
	       		categories.push(i.toString());
	       	}
       		$(":checked[id^=chk_sample_]").each(function(){
				var index = $(this).attr("id").split("_")[2],
					sample = $("#sample_name_"+index).html(),
					indexes = $("#profile_x_"+index).val().split(","),
					values = $("#profile_y_"+index).val().split(","),
					values_to_display = new Array(),
					links = {};
				for (var i=0; i<indexes.length; i++) { links[indexes[i]] = values[i]; }
				for (var i=0; i<categories.length; i++) {
		       		if (categories[i].toString() in links) {
		       			values_to_display.push(parseInt(links[categories[i].toString()]));
		       		} else {
		       			values_to_display.push(1);
		       		}
		       	}
				series.push({   
	            	name: sample,
	                data: values_to_display
	            });
			});
	       	chart = new Highcharts.Chart({
	            chart: {
	                renderTo: 'highcharts_container',
	                type: 'column'
	            },
	            title: {
	                text: 'Sequence duplication profile'
	            },
	            xAxis: {
	                categories: categories,
                    title: { text: 'Cluster size' }
	            },
	            yAxis: {
	                type: 'logarithmic',
	                minorTickInterval: 0.1,
	                min: 1,
	                title: { text: 'Number of sequences in log scale' }
	            },
				credits: { enabled: false },
	            tooltip: {
	                headerFormat: '<b>{series.name}</b><br />',
	                pointFormat: 'x = {point.x}, y = {point.y}'
	            },
	            series: series
	        });
	       	chart.setSize(720, 450);
	       	$("#ng6modal").modal();
    	}
	});
	
	$(".duplication-profile-btn").click(function() {
		if ($(":checked[id^=chk_sample_]").size() == 1) {
            var profile_path = $("#profile_" + $(":checked[id^=chk_sample_]").attr("id").split("_")[2]).val();            
    		$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
    		$("#modal-body-tmpl").html('<div id="img_container"><img src="' + profile_path + '" alt="Cannot display"></div>');
    		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
    		resize_center_btmodal('#ng6modal', 750);
           	$("#ng6modal").modal();
    	}
	});

});
