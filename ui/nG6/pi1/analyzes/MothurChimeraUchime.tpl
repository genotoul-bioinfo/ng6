{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}
{extends file='AnalysisTemplate.tpl'}

{block name=params_title} Chimera Uchime options {/block}
{block name=params_content}
	{assign var="params" value=";"|explode:$analyse.params}
	<ul>
		{foreach from=$params item=param}
		<li class="parameter">{$param}</li>
		{/foreach}
	</ul>
{/block}

{block name=results_title} Chimera Uchime results {/block}
{block name=results}
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th class="string-sort">{if $analyse_results|@count > 1 }Samples ({$analyse_results|@count}){/if}</th>
				<th class="numeric-sort">Aligned reads</th>
				<th class="numeric-sort">After removed chimera</th>
				<th class="numeric-sort">Chimera</th>
				<th class="numeric-sort">% deleted</th>
			</tr>
		</thead>
		<tbody>
			{assign var="nb_reads_begining" value=0}
			{assign var="nb_reads_end" value=0}
			{assign var="uncorrect" value=0}
			{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
		   	<tr>
				<td>{$sample|get_description:$descriptions}</td>
				{$nb_reads_begining=$nb_reads_begining+$sample_results["default"].nb_seqs_before_chimera}
				{$nb_reads_end=$nb_reads_end+$sample_results["default"].nb_seqs_after_chimera}
				{$uncorrect=$uncorrect+($sample_results["default"].nb_seqs_before_chimera-$sample_results["default"].nb_seqs_after_chimera)}
				<td>{$sample_results["default"].nb_seqs_before_chimera|number_format:0:' ':' '}</td>
				<td>{$sample_results["default"].nb_seqs_after_chimera|number_format:0:' ':' '}</td>
				<td>{($sample_results["default"].nb_seqs_before_chimera-$sample_results["default"].nb_seqs_after_chimera)|number_format:0:' ':' '}</td>
				<td>{(100.00-((($sample_results["default"].nb_seqs_after_chimera)*100)/$sample_results["default"].nb_seqs_before_chimera))|number_format:2:'.':' '}</td>
		   	</tr>
		   	{/foreach}
		</tbody>
		<tfoot>
			{if $analyse_results|@count > 1 }
			<tr>
				<th>Total</th>
				<th>{$nb_reads_begining|number_format:0:' ':' '}</th>
				<th>{$nb_reads_end|number_format:0:' ':' '}</th>
				<th>{$uncorrect|number_format:0:' ':' '}</th>
				<th>{(100.00-((($nb_reads_end)*100)/$nb_reads_begining))|number_format:2:'.':' '}</th>
			</tr>
			<tr>
				<th>Mean</td>
				<th>{($nb_reads_begining/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{($nb_reads_end/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{($uncorrect/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{(100.00-((($nb_reads_end)*100)/$nb_reads_begining))|number_format:2:'.':' '}</th>
			</tr>
			{/if}
		</tfoot>
	</table>
{/block}