{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}
{block name=params_title} Processing options {/block}

{block name=params_content}
	{assign var="params" value="|"|explode:$analyse.params}
	<p>Following step were performed :</p>
	<ul>
		{foreach $params as $step}
			{if $step == "samtools rmdup"}
				<li class="parameter">{$step}: Clean paired duplicate.</li>
				{$analyse.type="paired"}
			{else if $step == "samtools rmdup -s"}
				<li class="parameter">{$step}: Clean single duplicate.</li>
			{else if $step == "samtools flagstat"}
				<li class="parameter">{$step}: Count mapped reads.</li>
			{else if $step == "samtools sort"}
				<li class="parameter">{$step}: Sort bam file for rmdup.</li>
			{else}
				<li class="parameter">{$step}</li>
			{/if}
		{/foreach}
	</ul>
{/block}

{block name=results}
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th>Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
				<th class="numeric-sort" ># Reads in alignment </th>
				<th class="numeric-sort" ># Reads after rmdup</th>
				{assign var="firstelt" value=array_shift(array_slice($analyse_results, 0, 1))}
				{if 'rmdup'|array_key_exists:$firstelt["default"]}
					<th class="numeric-sort" ># Reads after removing singleton</th>
				{/if}
			</tr>
		</thead>
		<tbody>
			{foreach from=$analyse_results key=sample item=sample_results}
			<tr>
				<td>{$sample|get_description:$descriptions}</td>
				<td>{$sample_results["default"].input|number_format:0:' ':' '}</td>
				{if 'rmdup'|array_key_exists:$sample_results["default"]}
					<td>{$sample_results["default"].rmdup|number_format:0:' ':' '}</td>
				{/if}
				<td>{$sample_results["default"].cleaned|number_format:0:' ':' '}</td>
		   	</tr>
			{/foreach}
		</tbody>
	</table>
{/block}
