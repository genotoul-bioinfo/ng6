{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. # 
*}

{extends file='AnalysisTemplate.tpl'}

{block name=params_title} Parameters {/block}
{block name=params_content}
	{assign var="params" value=" "|explode:$analyse.params}
	<ul>
		<li class="parameter">Unknown indices with a number of fragments < {$params[0]*100}% of the number of fragments in the sample with the littlest population are merged in "All others". In other words, each unknown indice with a number of fragments > {$params[0]*100}% (included) of the number of fragments in the sample with the smallest population is displayed </li>
	</ul>
{/block}

{block name=results_title} Illumina metrics {/block}
{block name=results}
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th class="string-sort">Sample {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
				<th class="string-sort">Type</th>
				<th class="string-sort">Index</th>
				<th class="numeric-sort">% of total fragments</th>
				<th class="numeric-sort">Number of fragments</th>
				<th class="numeric-sort">% of passing filter</th>
			</tr>
		</thead>
		{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
		{assign var="total" value=0}
		{assign var="undetermined" value=0}
		{assign var="determined" value=0}
		{foreach from=$analyse_results_sorted key=sample item=sample_results}
			{if array_key_exists("undetermined", $sample_results)}
				{$total = $total+$sample_results["undetermined"].number}
				{$undetermined = $undetermined+$sample_results["undetermined"].number}
			{else}
				{$total = $total+$sample_results["determined"].number}
				{$determined = $determined+$sample_results["determined"].number}	
			{/if}
		{/foreach}
		<tbody>
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
		   	<tr>
				{if array_key_exists("undetermined", $sample_results)}
				<td>-</td>
				<td>Undetermined</td>
				<td>{$sample}</td>
				<td>{((($sample_results["undetermined"].number)/$total)*100)|number_format:2:'.':' '}</td>
				<td>{$sample_results["undetermined"].number|number_format:0:' ':' '}</td>
				<td>{($sample_results["undetermined"].passing_filter*100/$sample_results["undetermined"].number)|number_format:2:'.':' '}</td>
		   		{else}
		   		<td>{$sample|get_description:$descriptions}</td>
		   		<td>Sample</td>
				<td>{$sample}</td>
				<td>{((($sample_results["determined"].number)/$total)*100)|number_format:2:'.':' '}</td>
				<td>{$sample_results["determined"].number|number_format:0:' ':' '}</td>
				<td>{($sample_results["determined"].passing_filter*100/$sample_results["determined"].number)|number_format:2:'.':' '}</td>
				{/if}
		   	</tr>
		   	{/foreach}
	   	</tbody>
	   	{if $analyse_results|@count > 1 }
	   	<tfoot>
			<tr>
				<th>Total Samples</th>
				<th></th>
				<th></th>
				<th>{(($determined/$total)*100)|number_format:2:'.':' '}</th>
				<th>{$determined|number_format:0:' ':' '}</th>
				<th></th>
			</tr>
			<tr>
				<th>Total Undetermined</th>
				<th></th>
				<th></th>
				<th>{(($undetermined/$total)*100)|number_format:2:'.':' '}</th>
				<th>{$undetermined|number_format:0:' ':' '}</th>
				<th></th>
			</tr>
		</tfoot>
		{/if}
	</table>
{/block}

{block name=download}{/block}
