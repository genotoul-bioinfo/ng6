/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */

//Global data
	//Depths histogram
var sum_data = new Array();
	//Linkage view
var linkage_data = null ;
var tree = null ;
var diagonal = null ;
var svg_layout = null ;
var i = 0 ;

$(function () {
	$(".depth-stat-view-btn").click(function() {
		// Set dialog window
		$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
		$("#modal-body-tmpl").html('<div id="highcharts_container"></div>');
		$("#modal-body-tmpl").css('max-height', modal_height(200, 100)+'px');
		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
		$("#highcharts_container").css('max-width', '700px');
		$("#ng6modal").css('width', 'auto');
		
		// Build graph
		var chart = build_depth_boxplot( 'highcharts_container' );
		resize_center_btmodal('#ng6modal', chart.chartWidth + 50);
	    
		// Display
		$("#ng6modal").modal();
	})
	
	$(".depth-hist-view-btn").click(function() {
		// Set dialog window
		$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
		$("#modal-body-tmpl").html('<div id="highcharts_container"></div>');
		$("#modal-body-tmpl").css('max-height', modal_height(200, 100)+'px');
		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
		$("#highcharts_container").css('width', '845px');
		
		// Build graph
		var chart = build_depth_histogram( 'highcharts_container', 'logarithmic' );
		resize_center_btmodal('#ng6modal', chart.chartWidth + 50);
		
		// Display
		$("#ng6modal").modal();
    });
	
	$(".reads-curve-view-btn").click(function() {
		// Set dialog window
		$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
		$("#modal-body-tmpl").html('<div id="highcharts_container"></div>');
		$("#modal-body-tmpl").css('max-height', modal_height(200, 100)+'px');
		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
		$("#highcharts_container").css('width', '845px');
		
		// Build graph
		var chart = build_reads_curve( 'highcharts_container' );
		resize_center_btmodal('#ng6modal', chart.chartWidth + 50);
		
		// Display
		$("#ng6modal").modal();
    });
	
	$(".linkage-tree-view-btn").click(function() {
		//Set dialog window
		$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
		$("#modal-body-tmpl").html('<div id="d3_container"></div>');
		$("#modal-body-tmpl").css('max-height', modal_height(200, 100)+'px');
		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
		$("#d3_container").css('width', '845px');
		$("#d3_container").css('height', '500px');
		
		//Retrieve values
		var nb_terminal_nodes = null ;
		var sample_name_max_length = 0 ;
		linkage_data = null ;
		$(":checked[id^=chk_sample_]").each(function(){
	        var index = $(this).attr("id").split("_")[2];
	        nb_terminal_nodes = $("#linkage_"+index).val().match(/\[\s*\]/g).length ;
	        linkage_data = $.parseJSON( $("#linkage_"+index).val() );
	        // Sample name max length
	        samples_names = $("#linkage_"+index).val().match(/\"name\"\s*:\s*\"[^\"]+"/g) ;
	        for( var i = 0 ; i < samples_names.length ; i++ ) {
	        	sample_name_max_length = Math.max( sample_name_max_length, (samples_names[i].length - 10) ) ;
	        }
       	});
		
		//Build chart
		var width = getDepth(linkage_data) * 95 + (sample_name_max_length * 6) ; // nb_link * width_link + text_length 
		var height = 50 + nb_terminal_nodes * 30 ; //margin_top + nb_terminal_nodes + height_nodes
		tree = null ;
		diagonal = null ;
		svg_layout = null ;
		i = 0 ;
		
		tree = d3.layout.tree()
	    	.size([height, width]);

		diagonal = d3.svg.diagonal()
		    .projection(function(d) { return [d.y, d.x]; });
	
		svg_layout = d3.select("#d3_container").append("svg:svg")
		    .attr("width", width)
		    .attr("height", height)
		    .append("svg:g")
		    .attr("transform", "translate(20,20)");

		linkage_data.x0 = height / 2;
		linkage_data.y0 = 0;
		
		update_linkage_nodes(linkage_data);
		
		resize_center_btmodal('#ng6modal', 845 + 50);
		
		//Display
		$("#ng6modal").modal();
    });
	
	/**
	 * Return the object max depth.
	 */
	getDepth = function( obj ) {
	    var depth = 0;
	    if (obj.children) {
	        obj.children.forEach(function (d) {
	            var tmpDepth = getDepth(d)
	            if (tmpDepth > depth) {
	                depth = tmpDepth
	            }
	        })
	    }
	    return 1 + depth
	}

	/**
	 * Draws the boxplot of the clusters depths.
	 */
	function build_depth_boxplot( container ) {
		//Retrieve values
		var clustering_names = new Array() ;
		var clustering_data = new Array() ;
        $(":checked[id^=chk_sample_]").each(function(){
	        var index = $(this).attr("id").split("_")[2];
	        var min = parseFloat( $("#min_"+index).text().replace(/\s+/g, '') );
	        var lq = parseFloat( $("#lq_"+index).text().replace(/\s+/g, '') );
	        var median = parseFloat( $("#median_"+index).text().replace(/\s+/g, '') );
	        var uq = parseFloat( $("#uq_"+index).text().replace(/\s+/g, '') );
	        var max = parseFloat( $("#max_"+index).text().replace(/\s+/g, '') );
	        clustering_data.push( new Array(min, lq, median, uq, max) );
	        clustering_names.push( $("#sample_id_"+index).text() );
       	});
		
		//Build chart
        return new Highcharts.Chart({
		    chart: {
		        type: 'boxplot',
		        renderTo: container,
		    },
		    title: {
		        text: 'Clustering depths'
		    },
		    legend: {
		        enabled: false
		    },
		    xAxis: {
		        categories: clustering_names
		    },
		    yAxis: {
		        title: {
		            text: 'Depths'
		        }
		    },
		    series: [{
		    	name: 'Depths',
		        data: clustering_data
		    }],
		    credits: {
		        enabled: false
		    },
		});
	}
	
	/**
	 * Draws the curve that represents the cumulative percentage of reads by the cluster depth.
	 */
	function build_reads_curve( container ) {
		//Retrieve values
		var counts_data = new Array() ;
		var max_depth = 0 ;
        $(":checked[id^=chk_sample_]").each(function(){
        	var curve_idx = counts_data.length ;
	        var index = $(this).attr("id").split("_")[2] ;
        	var nb_reads = parseInt($("#seq_"+index).text().replace(/\s+/g, '')) ;
	        var counts = $("#counts_"+index).val().split(",") ;
	        var depths = $("#depths_"+index).val().split(",") ;
	        counts_data.push( 
		        	{ name: $("#sample_id_"+index).text(),
		        	  data: new Array(),
		        	  pointStart: 1 }
		        );
	        max_depth = Math.max( max_depth, parseInt(depths[depths.length-1]) );
	        var previous_count = 0 ;
	        for (var i=0 ; i<counts.length ; i++) {
	        	previous_count += parseInt(counts[i]) * parseInt(depths[i]) ;
	        	cumulative_prct = (previous_count/nb_reads)*100
	        	counts_data[curve_idx]["data"].push( new Array(parseInt(depths[i]), Math.round(cumulative_prct*100)/100) );
	        }
       	});
      
       //Build chart
       return new Highcharts.Chart({
    	   chart: {
		        type: 'area',
		        renderTo: container,
	            zoomType:"x"
            },
            title: {
                text: 'Cumulative reads proportion by cluster depth'
            },
            xAxis: {
    	        title: {
    	            text: 'Clusters depths'
    	        },
                tickInterval: Math.max(1, parseInt(max_depth/10))
            },
            yAxis: {
    	        title: {
    	            text: '% reads'
    	        },
            },
            tooltip: {
                formatter:function() {
                	tooltip_head = '<b>Depths <= ' + this.x + '</b>' ;
                    tooltip_body = '' ;
                    for( var i=0 ; i<this.points.length ; i++) {
                    	var pcnt = (this.points[i].point.y / sum_data[this.points[i].series.name]) * 100;
                    	tooltip_body += '<tr>' +
                    		'<td style="color:' + this.points[i].series.color +'">' + this.points[i].series.name + ' : </td>' +
                    		'<td> ' + this.points[i].point.y + '% </td>' +
                    		'<td> reads</td>' +
                    	   '</tr>' ;
                    }
                    return tooltip_head + '<table>' + tooltip_body + '</table>' ;
                },
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.1,
                    borderWidth: 0
                }
            },
            series: counts_data,
            credits: {
		        enabled: false
		    },
        });
	}
	
	/**
	 * Draws the histogram that represents the number of clusters by the cluster depth.
	 */
	function build_depth_histogram( container, scale_type ) {
		//Retrieve values
		sum_data = new Array();
		var max_depth = 0 ;
		var counts_data = new Array() ;
        $(":checked[id^=chk_sample_]").each(function(){
        	var hist_idx = counts_data.length ;
	        var index = $(this).attr("id").split("_")[2] ;
	        var counts = $("#counts_"+index).val().split(",") ;
	        var depths = $("#depths_"+index).val().split(",") ;
	        max_depth = Math.max( max_depth, parseInt(depths[depths.length-1]) );
	        counts_data.push( 
		        	{ name: $("#sample_id_"+index).text(),
		        	  data: new Array(),
		        	  pointStart: 1 }
		        );
	        for (var i=0 ; i<counts.length ; i++) {
	        	counts_data[hist_idx]["data"].push( new Array(parseInt(depths[i]), parseInt( counts[i] )) );
	        }
	        sum_data[$("#sample_id_"+index).text()] = parseFloat( $("#count_"+index).text().replace(/\s+/g, '') );
       	});

		//Build chart
		return new Highcharts.Chart({
		    chart: {
		        type: 'column',
		        renderTo: container,
	            zoomType:"x"
            },
            title: {
                text: 'Depths by cluster count'
            },
            xAxis: {
    	        title: {
    	            text: 'Depths'
    	        },
                tickInterval: Math.max(1, parseInt(max_depth/10))
            },
            yAxis: {
    	        title: {
    	            text: 'Clusters count'
    	        },
                type: scale_type,
                minorTickInterval: 0.1
            },
            tooltip: {
                formatter:function() {
                	tooltip_head = '<b>Depths ' + this.x + '</b>' ;
                    tooltip_body = '' ;
                    for( var i=0 ; i<this.points.length ; i++) {
                    	var pcnt = (this.points[i].point.y / sum_data[this.points[i].series.name]) * 100;
                    	tooltip_body += '<tr>' +
                    		'<td style="color:' + this.points[i].series.color +'">' + this.points[i].series.name + ' : </td>' +
                    		'<td> ' + this.points[i].point.y + ' </td>' +
                    		'<td> (' + Highcharts.numberFormat(pcnt) + '%)</td>' +
                    		'<td> clusters</td>' +
                    	   '</tr>' ;
                    }
                    return tooltip_head + '<table>' + tooltip_body + '</table>' ;
                },
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.1,
                    borderWidth: 0
                }
            },
            series: counts_data,
            credits: {
		        enabled: false
		    },
        });
	}
	
	/**
	 * Draw/update linkage tree.
	 */
	function update_linkage_nodes(source) {
		var duration = d3.event && d3.event.altKey ? 5000 : 500;

		// Compute the new tree layout.
		var nodes = tree.nodes(linkage_data).reverse();

		// Normalize for fixed-depth.
		nodes.forEach(function(d) { d.y = d.depth * 100; });

		// Update the nodes.
		var node = svg_layout.selectAll("g.d3-node")
			.data(nodes, function(d) { return d.id || (d.id = ++i); });

		// Enter any new nodes at the parent's previous position.
		var nodeEnter = node.enter().append("svg:g")
			.attr("class", "d3-node")
			.attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
			.on("click", function(d) { toggle(d); });

		nodeEnter.append("svg:circle")
			.attr("r", 1e-6)
			.attr("class", function(d) { return d._children ? "d3-collapsed-node" : "d3-node-dot"; });

		nodeEnter.append("svg:text")
			.attr("x", function(d) { return d.children || d._children ? -10 : 10; })
			.attr("dy", ".35em")
			.attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
			.text(function(d) { return d.name; })
			.style("fill-opacity", 1e-6);

		// Transition nodes to their new position.
		var nodeUpdate = node.transition()
			.duration(duration)
			.attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

		nodeUpdate.select("circle")
			.attr("r", 4.5)
			.attr("class", function(d) { return d._children ? "d3-collapsed-node" : "d3-node-dot"; });

		nodeUpdate.select("text")
			.style("fill-opacity", 1);

		// Transition exiting nodes to the parent's new position.
		var nodeExit = node.exit().transition()
			.duration(duration)
			.attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
			.remove();

		nodeExit.select("circle")
			.attr("r", 1e-6);

		nodeExit.select("text")
			.style("fill-opacity", 1e-6);

		// Update the links
		var link = svg_layout.selectAll("path.d3-link")
			.data( tree.links(nodes), function(d) { return d.target.id; } );

		// Enter any new links at the parent's previous position.
		link.enter().insert("svg:path", "g")
			.attr("class", "d3-link")
			.attr("d", function(d) {
				var o = {x: source.x0, y: source.y0};
				return diagonal({source: o, target: o});
			})
			.transition()
			.duration(duration)
			.attr("d", diagonal);

		// Transition links to their new position.
		link.transition()
			.duration(duration)
			.attr("d", diagonal);

		// Transition exiting nodes to the parent's new position.
		link.exit().transition()
			.duration(duration)
			.attr("d", function(d) {
				var o = {x: source.x, y: source.y};
				return diagonal({source: o, target: o});
			})
			.remove();

		// Stash the old positions for transition.
		nodes.forEach(function(d) {
			d.x0 = d.x;
			d.y0 = d.y;
		});
	}

	/**
	 * Hide/Unhide children's node when toggle on node in linkage view.
	 */
	function toggle( d ) {
		//Hide/Unhide attributes
		if (d.children) {
			d._children = d.children;
			d.children = null;
		} else {
			d.children = d._children;
			d._children = null;
		}
		//Update node and her children in SVG
		update_linkage_nodes(d);
	}
});