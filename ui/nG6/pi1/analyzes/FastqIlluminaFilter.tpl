{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=params_title} Filtering options {/block}
{block name=params_content}
	{assign var="params" value=" "|explode:$analyse.params}
	{assign var="filter_index" value=$params|@array_keys:"--keep"}
	<ul>
		{if $params[$filter_index[0]+1] == "N"}
		<li class="parameter">Keep reads which pass the Illumina filters (Reads that have 'N' in the read-ID line).</li>
		{else}
		<li class="parameter">Keep reads which not pass the Illumina filters (Reads that have 'Y' in the read-ID line).</li>
		{/if}
	</ul>
{/block}

{block name=results_title} Filtering results {/block}
{block name=results}
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
				<th class="string-sort">Samples {if $analyse_results|@count > 1 }({$analyse_results|@count}){/if}</th>
				<th class="numeric-sort">Before filtering</th>
				<th class="numeric-sort">After filtering</th>
				<th class="numeric-sort">Filtered</th>
				<th class="numeric-sort">% deleted</th>
			</tr>
		</thead>
		{assign var="nb_reads_begining" value=0}
		{assign var="nb_reads_end" value=0}
		{assign var="filtered" value=0}
		{assign var="analyse_results_sorted" value=$analyse_results|@ksort}
		<tbody>
			{foreach from=$analyse_results_sorted key=sample item=sample_results}
		   	<tr>
				<td>{$sample|get_description:$descriptions}</td>
				{$nb_reads_begining=$nb_reads_begining+$sample_results["default"].input}
				{$nb_reads_end=$nb_reads_end+$sample_results["default"].output}
				{$filtered=$filtered+($sample_results["default"].input-$sample_results["default"].output)}
				<td>{$sample_results["default"].input|number_format:0:' ':' '}</td>
				<td>{$sample_results["default"].output|number_format:0:' ':' '}</td>
				<td>{($sample_results["default"].input-$sample_results["default"].output)|number_format:0:' ':' '}</td>
				<td>{(100.00-((($sample_results["default"].output)*100)/$sample_results["default"].input))|number_format:2:'.':' '}</td>
		   	</tr>
		   	{/foreach}
	   	</tbody>
		{if $analyse_results|@count > 1 }
	   	<tfoot>
			<tr>
				<th>Total</th>
				<th>{$nb_reads_begining|number_format:0:' ':' '}</th>
				<th>{$nb_reads_end|number_format:0:' ':' '}</th>
				<th>{$filtered|number_format:0:' ':' '}</th>
				<th>{(100.00-((($nb_reads_end)*100)/$nb_reads_begining))|number_format:2:'.':' '}</th>
			</tr>
			<tr>
				<th>Mean</th>
				<th>{($nb_reads_begining/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{($nb_reads_end/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{($filtered/($analyse_results|@count))|number_format:0:' ':' '}</th>
				<th>{(100.00-((($nb_reads_end)*100)/$nb_reads_begining))|number_format:2:'.':' '}</th>
			</tr>
		</tfoot>
		{/if}
	</table>
{/block}

{block name=download}{/block}
