{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

{extends file='AnalysisTemplate.tpl'}

{block name=params_content}
	{assign var="params" value=" "|explode:$analyse.params}
	{assign var="index" value=$params|@array_keys:"-mcf"}
	{foreach from=$analyse_results key=sample item=sample_results}
	{assign var="sample" value=$sample}
	{/foreach}
	<ul>
		<li class="parameter">{$analyse.params}</li>
		<li class="parameter">With <strong> {$params[$index[0]+1]} </strong> containing: 
			<br /> 
			<div class="file-display">{$analyse_results[$sample]["default"]["config_file"]}</div> 
			<i> with: mid = "Samples", "fwd_seq", nb_mismatch, "rvrs_seq";</i>
		</li>
	</ul>
{/block}

{block name=results_title} Demultiplexing results {/block}
{block name=results}
	{foreach from=$analyse_results key=sample item=sample_results}
	{assign var="sample" value=$sample}
	{/foreach}
	{assign var="analyse_results_sorted" value=$analyse_results[$sample]["default"]|@ksort}
	<table class="table table-striped table-bordered dataTable analysis-result-table">
		<thead>
			<tr>
			    <th class="string-sort"> Samples </th>
			    <th class="numeric-sort"> Number of sequences </th>
			</tr>
		</thead>
		<tbody>
			{assign var="total" value=0}
			{foreach from=$analyse_results_sorted key=mid item=value}
			{if $mid != "config_file" }
			<tr>
			    <td> {$mid} </td>
			    <td> {$value|number_format:0:' ':' '} </td>
			    {$total=$total+$value}
			</tr>
			{/if}
			{/foreach}
		</tbody>
		<tfoot>
			<tr>
				<th>Total</th>
				<th>{$total|number_format:0:' ':' '}</th>
			</tr>
			<tr>
				<th>Mean</th>
				<th>{($total/($analyse_results_sorted|@count-1))|number_format:0:' ':' '}</th>
			</tr>
		</tfoot>
	</table>
{/block}
