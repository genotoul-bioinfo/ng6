function powerset(ary) { //set of all subsets of "ary"
    var ps = [[]];
    for (var i=0; i < ary.length; i++) {
        for (var j = 0, len = ps.length; j < len; j++) {
            ps.push(ps[j].concat(ary[i]));
        }
    }
    return ps;
}

$(document).ready(function(){
	$(document).on("click", ".length-view-btn", function() {
		if ($(":checked[id^=chk_sample_]").size() > 0) {

			var all_boxplot = [];
			var all_outliers = [];
			var all_sample_id = [];
			var plot_count = 0;
			var colors = ['#7cb5ec', '#434348', '#90ed7d', '#f7a35c', '#8085e9', 
   									'#f15c80', '#e4d354', '#2b908f', '#f45b5b', '#91e8e1']

   			// for each checked sample get
			$(":checked[id^=chk_sample_]").each(function(){
				//index
	            var index = $(this).attr("id").split("_")[2];
	            var index2 = $(this).attr("id").split("_")[3];
	            //id
	            var val_soft = $("#soft_"+index+"_"+index2).val();
	            var sv_type = $("#type_"+index+"_"+index2).val();
	            all_sample_id.push(val_soft+"_"+sv_type);
	            //boxplot
	            var raw_boxplot = $("#boxplot_"+index+"_"+index2).val().split(";");
	            raw_boxplot = raw_boxplot.map(function (x) { 
				    return parseInt(x, 10); //cast to int
				});
				obj_boxplot = {
					x: plot_count, 
					color: colors[plot_count],
					low: raw_boxplot[0],
					q1: raw_boxplot[1], 
					median: raw_boxplot[2], 
					q3: raw_boxplot[3], 
					high: raw_boxplot[4]
				};
	            all_boxplot.push(obj_boxplot);
	            //outliers
	            var val_outliers = $("#outliers_"+index+"_"+index2).val().split(";");
	            if(val_outliers[0] != '' ){
		            $.each(val_outliers, function(position, value){
	            		all_outliers.push([parseInt(index),parseInt(value)]);
	            	});
	            }
				plot_count++
	        });
			
			//set the modal
			$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
			$("#modal-body-tmpl").html('<div id="highcharts_container"></div>');
			$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
			$("#highcharts_container").css('width', '845px');

	        //set the graph
			var chart = new Highcharts.Chart({ 
		        chart: {
		           renderTo: 'highcharts_container',
		           type: 'boxplot',
		           zoomType: 'xy'
		        },
		        title: {
		            text: 'SV length Box-Plot'
		        },
		        legend: {
		            enabled: false
		        },
		        xAxis: { 
		        	categories: all_sample_id,
		            title: {
		                text: 'Software'
		            }
		        },
		        yAxis: {
		            title: {
		                text: 'Sizes'
		            }
		        },
		        series: [{
		            name: 'Sizes',
		            data: all_boxplot,
		            tooltip: {
		                headerFormat: '<em>Software: {point.key}</em><br/>'
		            }
		        }]
		    });

		    //display the modal
		    resize_center_btmodal('#ng6modal', chart.chartWidth + 50);
			$("#ng6modal").modal();
		}
	});

	////////////////////////////////////////////////////////////////////////////////////////////////////
	$(document).on("click", ".compo-view-btn", function() {
		if ($(":checked[id^=chk_sample_]").size() > 0) {

			var tags = $("#ntag_0_1").val().split(";") //Quick&Dirty
			var all_count = [];

			//for each sample get
			$(":checked[id^=chk_sample_]").each(function(){
				//index
	            var index = $(this).attr("id").split("_")[2];
	            var index2 = $(this).attr("id").split("_")[3];
	            //id
	            var soft = $("#soft_"+index+"_"+index2).val();
	            var sv_type = $("#type_"+index+"_"+index2).val();
	            var sample = soft + "_" + sv_type;
	            //counts
	            var val_count = $("#ncount_"+index+"_"+index2).val().split(";");
	            val_count = val_count.map(function (x) { 
				    return parseInt(x, 10); //cast to int
				});
	            all_count.push({name:sample, data:val_count});
	        });

			//set the modal
			$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
			$("#modal-body-tmpl").html('<div id="highcharts_container"></div>');
			$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
			$("#highcharts_container").css('width', '845px');

			//set the chart
			var chart = new Highcharts.Chart({
		        chart: {
		           renderTo: 'highcharts_container',
		           zoomType: 'xy',
		           type: 'line'
		        },
		        title: {
		            text: 'SVR found in at least N individuals'
		        },
		        xAxis: {
		            categories: tags,
		            title: {
		                text: 'At least N individuals',
		            },
		            labels: {
		                overflow: 'justify'
		            }
		        },
		        yAxis: {
		            min: 0,
		            title: {
		                text: 'Count',
		            },
		            labels: {
		                overflow: 'justify'
		            }
		        },
		        plotOptions: {
		            bar: {
		                dataLabels: {
		                    enabled: true
		                }
		            }
		        },
		        legend: {
		            layout: 'vertical',
		            align: 'right',
		            verticalAlign: 'top',
		            floating: true,
		            borderWidth: 1,
		            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
		            shadow: true,
		            title: {
	                	text: 'Software<br/><span style="font-size: 9px; color: #666; font-weight: normal">(Click to hide)</span>',
	                	style: {
	                    	fontStyle: 'italic'
	                    }
	                },
	                x: -100
		        },
		        credits: {
		            enabled: false
		        },
		        series: all_count
		    });

			//display the modal
	        resize_center_btmodal('#ng6modal', chart.chartWidth + 50);
			$("#ng6modal").modal();
		}
	});
	////////////////////////////////////////////////////////////////////////////////////////////////////

	$.getScript("http://bioinfo.genotoul.fr/jvenn/src/jvenn.min.js", function(){
		$(document).on("click", ".jvenn-btn", function() {
			var all_venn = [];
			var type4title = null;
			$(".collapse").each(function(){
				var sv_type = $(this).attr("id").split("_")[2]
				if ($(":checked[id^=chk_sample_]", this).size() > 1) {
					//venn_count
			        var venn_count = [];
			        var tmp = $("#venn_count_"+sv_type).val().split(",");
			        $.each(tmp, function(index, item){
			        	var pair = item.split(":");
			        	venn_count[pair[0].split(";").sort().join(";")] = pair[1];
			        });
			        //combinations
					var selection = [];
					$(":checked[id^=chk_sample_]").each(function(){ // ALL only
			            var soft = $(this).val();
			            selection.push(soft);
			        });
			        var combinations = powerset(selection);
			        var keys = powerset(['A','B','C','D']);
			        var my_values = {};
			        var my_names = {};
			        $.each(combinations, function(index, item){
			        	if (keys[index] != '')
			        		my_values[keys[index].join("")] = venn_count[item.sort().join(";")];
			        	if (keys[index].length == 1){
			        		my_names[keys[index].join("")] = item.sort().join(";");
			        	}
			        });
			        all_venn.push({
			        	name: my_names,
			    		values: my_values
			    	});
					type4title = sv_type;
				}
		    });
			// set the modal
			console.log($(this));
			$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + ": Venn Diagram of " + type4title + " type</small>");
			$("#modal-body-tmpl").html('<div id="jvenn_container"></div>');
			$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
			$("#jvenn_container");
	        //jvenn
			$('#jvenn_container').jvenn({
		    	// series: [{
			    // 	name: my_names,
			    // 	values: my_values
			    // }],
			    series: all_venn,
			    disableClick: false
			});
			//launch modal
			// resize_center_btmodal('#ng6modal');
			$("#ng6modal").modal();
		});
	});
});