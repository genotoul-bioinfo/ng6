/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */

$(function () {
	/* 
    * Define functions in charge to visualize box plots
    */
	$(".cluster-view-btn").click(function() {
		
		var max_xvalues = 30;
		
    	if ($(":checked[id^=chk_sample_]").size() > 0) {
			//Set dialog window
			$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
    		$("#modal-body-tmpl").html('<div id="highcharts_container"></div>');
    		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
    		$("#highcharts_container").css('width', '845px');
			resize_center_btmodal('#ng6modal', 845);
			
    		//Set graph
			var x_table = new Array();
			var y_table = new Array();
			$(":checked[id^=chk_sample_]").each(function(){
	          	var index = $(this).attr("id").split("_")[2];
	           	var x1_table = new Array();
	           	x1_table.push(parseInt($("#min_"+index).val()));
	           	x1_table.push(parseInt($("#quart25_"+index).val()));
	           	x1_table.push(parseInt($("#median_"+index).val()));
	           	x1_table.push(parseInt($("#quart75_"+index).val()));
	           	x1_table.push(parseInt($("#max_"+index).val()))
	           	x_table.push(x1_table);
	           	y_table.push($("#name_"+index).val());
	        });
			
	        chart = new Highcharts.Chart({
	        	chart: {
	               renderTo: 'highcharts_container',
	    	        type: 'boxplot'
	            },
	    	    
	    	    title: {
	    	        text: 'Number of clusters'
	    	    },
	    	    
	    	    legend: {
	    	        enabled: false
	    	    },

	    	    credits: { enabled: false },
	    	
	    	    xAxis: {
	    	        categories: y_table,
	    	        title: {
	    	            text: ''
	    	        }
	    	    },
	    	    
	    	    yAxis: {
	    	        title: {
	    	            text: 'Observations'
	    	        }
	    	    },
	    	
	    	    series:
	    	    	[{
		    	        name: 'Observations',
		    	        data: x_table,
		    	        tooltip: {
		    	            headerFormat: '<em>For {point.key}</em><br/>'
		    	        }
	    	    	}]
	         });
		
			//Display
			$("#ng6modal").modal();
    	}
	});

});
