{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}
<table class="table table-striped table-bordered dataTable" id="project_data_table">
	<thead>
		<tr>
		    {assign var="pcolspan" value=3}
			{if $is_at_least_admin_of_1_project && $login_user}
				<th><center><input type="checkbox" id="chk_all_project"></center></th>
				{assign var="pcolspan" value= $pcolspan + 1}
			{/if}
			<th>Project Name</th>
			<th>Date</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$projects key=project_id item=project_values}
			{assign var="classes" value=""}
			{if $login_user && $project_values.public == 0 && $project_values.is_admin}
				{$classes = $classes|cat:"<span class='label label-warning'><i class='glyphicon glyphicon-thumbs-up'></i></span> "}
			{/if}
			{if $project_values.hidden == 1}
				{$classes = $classes|cat:" <span class='label label-info'><i class='glyphicon glyphicon-eye-close'></i></span> "}
			{/if}
			{if ($project_values.hidden == 1 && $project_values.is_admin) || $project_values.hidden == 0}
				<tr id="tr_project_{$project_values.id}">
					{if $project_values.is_admin}
						<td><center><input type="checkbox" id="chk_project_{$project_values.id}" value="project_{$project_values.id}"></center></td>
					{elseif $is_at_least_admin_of_1_project}
						<td></td>
					{/if}
					{if $project_values.is_admin}
						<td class="editable"> 
							{$classes} <span class="editable" data-type="text" data-pk="{$project_values.id}" data-url="index.php?eID=tx_nG6&type=update_db_field&table=tx_nG6_project&field=name" data-original-title="Enter project name">{$project_values.href}</span>
						</td>
					{else}
						<td> 
							{$project_values.href}
						</td>
					{/if}
					{if $project_values.is_admin}
						<td class="editable"> 
							<span class="editable-date" data-type="date" data-pk="{$project_values.id}" data-url="index.php?eID=tx_nG6&type=update_db_field&table=tx_nG6_project&field=crdate" data-original-title="Select date">{$project_values.date|date_format:"%d/%m/%Y"}</span>
						</td>
					{else}
						<td>
							{$project_values.date|date_format:"%d/%m/%Y"}
						</td>
					{/if}
					{if $project_values.is_admin}
						<td class="editable"> 
							<span class="editable" data-type="textarea" data-pk="{$project_values.id}" data-url="index.php?eID=tx_nG6&type=update_db_field&table=tx_nG6_project&field=description" data-original-title="Enter description"> {$project_values.description}</span>
						</td>
					{else}
						<td> 
							{$project_values.description}
						</td>
					{/if}
				</tr>
			{/if}
		{/foreach}
	</tbody>
	{if ($is_at_least_admin_of_1_project && $login_user) || $is_ng6_admin }
	<tfoot>
		<tr>
			<th align="left" colspan="{$pcolspan}">
				With selection :
				<div  class="btn-group">
					<button id="hide_project" type="button" class="btn multiplep-selection-btn btn-sm btn-default"><i class="glyphicon glyphicon-eye-close"></i> hide</button>
					<button id="unhide_project" type="button" class="btn multiplep-selection-btn btn-sm btn-default"><i class="glyphicon glyphicon-eye-open"></i> unhide</button>
				</div>
				<div class="btn-group">
					<button id="publish_project" type="button" class="btn multiplep-selection-btn btn-sm btn-default"><i class="glyphicon glyphicon-thumbs-up"></i> publish</button>
					<button id="unpublish_project" type="button" class="btn multiplep-selection-btn btn-sm btn-default"><i class="glyphicon glyphicon-thumbs-down"></i> unpublish</button>
				</div>
				<div class="btn-group">
					{if $is_ng6_admin}
						<button id="add_project" type="button" class="btn nop-selection-btn btn-sm btn-default"><i class="glyphicon glyphicon-plus"></i> add</button>
					{else}
						<button id="add_project" type="button" class="btn btn-sm btn-default" disabled><i class="glyphicon glyphicon-plus"></i> add</button>
					{/if}	
					<button id="delete_project" type="button" class="btn multiplep-selection-btn btn-sm btn-default"><i class="glyphicon glyphicon-minus"></i> delete</button>
				</div>
			</th>
		</tr>
	</tfoot>
	{/if}
</table>
