<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Class that adds the wizard icon.
 *
 * @author	PF bioinformatique de Toulouse <>
 */


use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
class tx_nG6_pi1_wizicon {

	/**
	 * Processing the wizard items array
	 *
	 * @param	array		$wizardItems: The wizard items
	 * @return	Modified array with wizard items
	 */
    
	function proc($wizardItems)	{
		global $LANG;

		$LL = $this->includeLocalLang();

		$wizardItems['plugins_tx_nG6_pi1'] = array(
			'icon'=>\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('nG6')).'pi1/ce_wiz.gif',
			'title'=>TypoScriptFrontendController->sL('pi1_title'),
			'description'=>TypoScriptFrontendController->sL('pi1_plus_wiz_description'),
			'params'=>'&defVals[tt_content][CType]=list&defVals[tt_content][list_type]=nG6_pi1'
		);

		return $wizardItems;
	}

	/**
	 * Reads the [extDir]/locallang.xml and returns the \$LOCAL_LANG array found in that file.
	 *
	 * @return	The array with language labels
	 */
	function includeLocalLang()	{
		global $LANG;

		$LOCAL_LANG = $LANG->includeLLFile('EXT:nG6/locallang.xml');
		return $LOCAL_LANG;
	}
}



if (defined('TYPO3') && $TYPO3_CONF_VARS[TYPO3]['XCLASS']['ext/nG6/pi1/class.tx_nG6_pi1_wizicon.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3]['XCLASS']['ext/nG6/pi1/class.tx_nG6_pi1_wizicon.php']);
}

?>
