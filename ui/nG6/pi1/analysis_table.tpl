{*
Copyright (C) 2009 INRA 
 
This program is a free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

<table class="table table-striped table-bordered dataTable" id="analysis_data_table">
	<thead>
		<tr>
		    {assign var="acolspan" value=5}
			{if $is_admin && $login_user && $display_analysis_result}
    			<th><center><input type="checkbox" id="chk_all_analysis"></center></th>
    			{assign var="acolspan" value= $acolspan +1 }
			{/if}
			<th nowrap>Name</th>
			<th>Description</th>
			<th>Date</th>
			<th>Software</th>
			<th>Version</th>
			<th>Data state</th>
		</tr>
	</thead>
	<tbody>
		{include file="recursive_list_analysis_display.tpl" element=$h_analysis prof=0}
	</tbody>
	{if $is_admin}
		<tfoot>
			<tr>
				<th align="left" colspan="{$acolspan}">
					With selection :
					<div  class="btn-group">
						<button id="hide_analysis" type="button" class="btn multiplea-selection-btn btn-sm btn-default"><i class="glyphicon glyphicon-eye-close"></i> hide</button>
						<button id="unhide_analysis" type="button" class="btn multiplea-selection-btn btn-sm btn-default"><i class="glyphicon glyphicon-eye-open"></i> unhide</button>
					</div>
					<div  class="btn-group">
						<button id="add_analysis" type="button" class="btn noa-selection-btn btn-sm btn-default"><i class="glyphicon glyphicon-plus"></i> add</button>
						<button id="delete_analysis" type="button" class="btn multiplea-selection-btn btn-sm btn-default"><i class="glyphicon glyphicon-minus"></i> delete</button>
					</div>
				</th>
			</tr>
		</tfoot>
	{/if}
</table>
