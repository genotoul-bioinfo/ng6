{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

<div class="tab-pane fade comment-tab" id="{$tab_id}">
    {if $comments}
        {foreach from=$comments key=comm_id item=cdata}
            <div class="comment-block ng6-block-box" id="comment_id_{$cdata.id}">
                <div class='comment-block-header ng6-block-box-header'> 
                    <i class="icon-user"></i>{$cdata.cruser_info.username}
                    {$cdata.date} 
                    {if $cdata.cruser_info.uid == $user_id}
                       <small ><a class="comment-edit" href="" >edit</a> <a class="comment-delete" href="">delete</a></small>
                    {/if}
                    
                 </div>
                <div class="comment-block-body ng6-block-box-body">{$cdata.comment}</div>
            </div>
        {/foreach}
    {else}
        No comments have been added yet
    {/if}
    
    {if $add_new_comment}
        <div class="new-comment-block">
            New comment
            <textarea class="wysihtml5-textarea" ></textarea>
            <div style="text-align : right; margin-top : 5px"><button id="post-comment" type="button" class="btn btn-sm btn-primary">Post comment</button></div>
        </div>
    {/if}
</div>
