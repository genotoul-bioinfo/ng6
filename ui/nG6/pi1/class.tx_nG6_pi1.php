<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */

require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/Classes/Controller/class.tx_nG6_utils.php');
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/Classes/Controller/tx_nG6_db.php');
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/lib/class.tx_nG6_upgrade.php');
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/libs/Smarty.class.php'); 
use TYPO3\CMS\Core\Context\Context;
use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use nG6\Controller\tx_nG6_db;
use nG6\Controller\tx_nG6_utils;

class tx_nG6_pi1 extends \TYPO3\CMS\Frontend\Plugin\AbstractPlugin {
	
	var $prefixId = 'tx_nG6_pi1';		// Same as class name
	var $scriptRelPath = 'pi1/class.tx_nG6_pi1.php';	// Path to this script relative to the extension dir.
	var $extKey = 'nG6';	// The extension key.
	
	/**
	 * Main method of your PlugIn
	 *
	 * @param	string		$content: The content of the PlugIn
	 * @param	array		$conf: The PlugIn Configuration
	 * @return	The content that should be displayed on the website
	 */
	function main($content,$conf)	{
	    $context = \TYPO3\CMS\Core\Utility\GeneralUtility ::makeInstance(Context::class);
	    if (strstr($this->cObj->currentRecord,'tt_content'))	{
			$conf['pidList'] = $this->cObj->data['pages'];
		}
		// Setting the TypoScript passed to this function in $this->conf
		$this->conf=$conf;
		
		$this->pi_setPiVarDefaults();
		// Configuring so caching is not expected. This value means that no cHash params are ever set. We do this, because it's a USER_INT object!
		//$this->pi_USER_INT_obj=1;
		// Loading the LOCAL_LANG values
		$this->pi_loadLL();		
		
		// Add the ng6 plugins css
		$GLOBALS['TSFE']->additionalHeaderData[$this->prefixId] = '
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/jquery.min.js"></script>
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/jquery.dataTables.min.js"></script>
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/wysihtml5-0.3.0.min.js"></script>
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/bootstrap.min.js"></script>
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/bootstrap-editable.min.js"></script>			
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/bootstrap3-wysihtml5.all.min.js"></script>
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/dataTables.bootstrap.js"></script>
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/typeahead.bundle.js"></script>
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/jflow.min.js"></script>
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/tx_nG6_pi1.js"></script>
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/tx_nG6_utils.js"></script>
			
			<link type="text/css" rel="stylesheet" media="screen" href="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/css/bootstrap3-wysihtml5.min.css"/>
			<link type="text/css" rel="stylesheet" media="screen" href="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/css/dataTables.bootstrap.css"/>
			<link type="text/css" rel="stylesheet" media="screen" href="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/css/bootstrap-editable.css"/>	
			<link type="text/css" rel="stylesheet" media="screen" href="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/css/typeaheadjs.css"/>	
			<link type="text/css" rel="stylesheet" media="screen" href="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/css/font-awesome.css"/>
			<link type="text/css" rel="stylesheet" media="screen" href="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/css/jflow.min.css"/>
			<link type="text/css" rel="stylesheet" media="screen" href="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/css/tx_nG6.css"/>';			
		
		// if asked to upgrade to version 1.2
		if($this->piVars['upgrade']){
			$content = '<div class="sub-content sc-top">'.tx_nG6_upgrade::upgrade($this->piVars['upgrade'], $this->conf["data"]).'</div>';
		} 
		else if(! $this->pi_is_install_finalized()) {
			$content .= $this->pi_install_view();
		}
		else {
			switch((string)$this->conf['view'])	{
				// If the plugin is configured to display results by run
				case 'run':
					if ($this->piVars['analyze_id']) {
						$content .= $this->pi_analyze_view();
					} else {
						$content .= $this->pi_run_view();
					}
					break;
				// If the plugin is not configured, display by project is default
				default:
					if ($this->piVars['analyze_id']) {
						$content .= $this->pi_analyze_view();
					} elseif ($this->piVars['project_id'] && $this->piVars['run_id']) {
						$content .= $this->pi_run_view();
					} else {
						$content .= $this->pi_project_view();
					}
					break;
			}
		}
		return $this->pi_wrapInBaseClass($content);
		
	}

	/**
	 * return a redirection view
	 * @return string
	 */
	function redirect_view(){
		$protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https')	=== FALSE ? 'http' : 'https';
		$host     = $_SERVER['HTTP_HOST'];
		$script   = $_SERVER['SCRIPT_NAME'];
		$currentUrl = $protocol . '://' . $host . $script ;
		return '<div class="sub-content"><br/><div class="alert alert-warning"><b>Access denied</b> - You are not authorized to access this page. You will be redirected in 3 seconds ' .
				'Click <a href="' . $currentUrl . '">here</a> if the redirection did not work'.
				'<meta http-equiv="refresh" content="3; URL=' . $currentUrl . '"></div></div>';
	}
	
	
	/**
	 * Return the project view
	 */
	function pi_project_view()	{		
	    $projects = array();
		$context = \TYPO3\CMS\Core\Utility\GeneralUtility ::makeInstance(Context::class);
		if (!$context->getPropertyFromAspect('frontend.user', 'isLoggedIn')) { $user_id = null; }
		else { $user_id = $context->getPropertyFromAspect('frontend.user', 'id'); }
		$is_current_user_superadmin = tx_nG6_db::is_user_ng6_superadmin($user_id);
		
		
		$single_project_display = false;
		// If a single element
		if ($this->piVars['project_id']) {
			if (tx_nG6_db::user_is_authorized($user_id, $this->piVars['project_id'], $this->piVars['run_id'])) {
				$projects = array('project_'.$this->piVars['project_id'] => tx_nG6_db::select_project($this->piVars['project_id']));
				$single_project_display = true ;
			}elseif( $is_current_user_superadmin ){
				$projects = array('project_'.$this->piVars['project_id'] => tx_nG6_db::select_project($this->piVars['project_id']));
				$single_project_display = true ;
			}
		} else {
			if( $is_current_user_superadmin ){
				$projects = tx_nG6_db::select_superadmin_projects();
				$visible_projects = NULL;
			}else{
				$projects = tx_nG6_db::select_all_user_projects($user_id, 'tx_nG6_project_run.uid DESC');
				//$projects = tx_nG6_db::select_all_user_projects($user_id, 'tx_nG6_project.name');
				$visible_projects = tx_nG6_db::get_user_projects($user_id);
			}
			
		}
		
		// If there is no project the user can access
		if (count($projects) > 0) {
			$smarty = new Smarty();
			$smarty->setTemplateDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/pi1');
			$smarty->setCompileDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/templates_c');
			$smarty->setCacheDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/cache');
			$smarty->setConfigDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/configs');
			$smarty->security = true;
			$smarty->security_settings['MODIFIER_FUNCS'] = array('count');		
			// Add some information to the table
			$project_ids = "";
			$user_id = $context->getPropertyFromAspect('frontend.user', 'id');
			$user_name = $context->getPropertyFromAspect('frontend.user', 'username');
			foreach($projects as $project_id => $project_values) {
				// project admin ?
			    $projects[$project_id]['is_admin'] = tx_nG6_db::is_project_administrator($user_id, $project_values['id']);
				// project manager ?
			    $projects[$project_id]['is_manager'] = tx_nG6_db::is_project_manager($user_id, $project_values['id']);
				// project member ?
			    $projects[$project_id]['is_member'] = tx_nG6_db::is_project_member($user_id, $project_values['id']);
				
				//If the user is a superadmin, we set its rights to admin on each project
				if( $is_current_user_superadmin ){
					$projects[$project_id]['is_admin'] = true;
				}
				
				$project_ids .= $project_values['id'].",";
				$projects[$project_id]['href'] = $this->pi_list_linkSingle($project_values['name'],$project_values['id'],1, array('project_id'=>$project_values['id']));
			}
			$smarty->assign('projects', $projects);
			$smarty->assign('login_user', $user_name);
			$smarty->assign('user_id', $user_id);
			$smarty->assign('user_login', $user_name);
			$smarty->assign('data_folder', $this->conf["data"]);
			$smarty->assign('from_email', $this->conf["FromEmail"]);
			$smarty->assign('envelope_sender_address', $this->conf["envelope_sender_address"]);
			$smarty->assign('server_name', $this->conf["server_name"]);
			$smarty->assign('project_ids', substr($project_ids,0,-1));
			$smarty->assign('pid', $this->conf['userpidList']);
			$smarty->assign('server_url', $this->conf['server_url']);
			$smarty->assign('single_project_display', $single_project_display);
			$smarty->assign('is_ng6_admin', tx_nG6_db::is_ng6_administrator($user_id) || tx_nG6_db::is_user_ng6_superadmin($user_id) ? true : false);
			$smarty->assign('is_ng6_superadmin', tx_nG6_db::is_user_ng6_superadmin($user_id) ? true : false);
			$smarty->assign('visible_projects', $visible_projects);
			
			
			// If it's a single project, add runs and analysis information
			if ($single_project_display) {
				
				//comments on the project
				$comments = tx_nG6_db::get_all_project_comments($projects[key($projects)]['id']);
				$smarty->assign('comments', $comments);
				
				// Get all users on project
				$project_users = tx_nG6_db::get_all_users_on_project($projects[key($projects)]['id']);
				$smarty->assign('project_users', $project_users);
	
				// Add some information to the table
				$project_runs = tx_nG6_db::get_project_runs($projects[key($projects)]['id']);
				foreach($project_runs as $run_id => $run_values) {
				    $project_runs[$run_id]['is_admin'] = tx_nG6_db::is_administrator($user_id, 'run', $run_values['id']);
					
					if( $is_current_user_superadmin ){
						$project_runs[$run_id]['is_admin'] = 1;
					}
					
					$project_runs[$run_id]['href'] = $this->pi_list_linkSingle($run_values['name'],$run_values['id'],1, array('run_id'=>$run_values['id'], 'project_id'=>$run_values['project_id']));
				}
				$smarty->assign('project_runs', $project_runs);
				$project_analysis = tx_nG6_db::get_project_analysis($projects[key($projects)]['id']);
				
				// Add some information to the table
				foreach($project_analysis as $analysis_id => $analysis_values) {
				    $project_analysis[$analysis_id]['is_admin'] = tx_nG6_db::is_administrator($user_id, 'analyze', $analysis_values['id']);
					
					if( $is_current_user_superadmin ){
						$project_analysis[$analysis_id]['is_admin'] = 1;
					}
					
					$project_analysis[$analysis_id]['href'] = $this->pi_list_linkSingle($analysis_values['name'],$analysis_values['id'],1, array('analyze_id'=>$analysis_values['id'], 'project_id'=>$this->piVars['project_id']));
				}
				$smarty->assign('h_analysis', tx_nG6_utils::trace_hierarchy($project_analysis));
				$smarty->assign('project_analysis', $project_analysis);
				
				if( $projects[$project_id]['is_admin'] == 1 ){
					$smarty->assign('managment_purged_data', tx_nG6_db::select_a_project_retention_data_info($projects[key($projects)]['id'], TRUE, TRUE));
				}else{
					$smarty->assign('managment_purged_data', tx_nG6_db::select_a_project_retention_data_info($projects[key($projects)]['id'], TRUE, FALSE));
				}
				
				$txNG6Utils = new tx_nG6_utils;
				//$smarty->register_object('tx_nG6_utils',$txNG6Utils);
				$smarty->assignByRef('tx_nG6_utils', $txNG6Utils);

				$smarty->assignByRef('retention_policy', tx_nG6_utils::get_project_retention_policy($projects[key($projects)]['id']));
				$smarty->assign('space_purge_msg', tx_nG6_utils::get_project_purge_msg($projects[key($projects)]['id']));
				
			}	
			return $smarty->fetch('project_view.tpl');
		} else {
			return $this->redirect_view();
		}
	}
	

	/**
	 * Return the run view
	 */
    function pi_run_view() {
        $context = \TYPO3\CMS\Core\Utility\GeneralUtility ::makeInstance(Context::class);
		$runs = array();
		if (!$context->getPropertyFromAspect('frontend.user', 'isLoggedIn')) { $user_id = null; }
		else { 
		    $user_id = $context->getPropertyFromAspect('frontend.user', 'id');
		    $user_name = $context->getPropertyFromAspect('frontend.user', 'username');
		}
		$is_current_user_superadmin = tx_nG6_db::is_user_ng6_superadmin($user_id);
		
		$single_run_display = false;
		// If a single element
		if ($this->piVars['run_id']) {
			if (tx_nG6_db::user_is_authorized($user_id, $this->piVars['project_id'], $this->piVars['run_id']) || $is_current_user_superadmin ) {
				$runs = array('run_'.$this->piVars['run_id'] => tx_nG6_db::select_run($this->piVars['run_id']));
				$single_run_display = true;
			}
		} else {
			$runs = tx_nG6_db::select_all_user_runs($user_id,"tx_nG6_run.uid DESC");
		}
		
		if (count($runs) > 0) {
			$smarty = new Smarty();
			$smarty->setTemplateDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/pi1');
			$smarty->setCompileDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/templates_c');
			$smarty->setCacheDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/cache');
			$smarty->setConfigDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/configs');
			$smarty->security = true;
			$smarty->security_settings['MODIFIER_FUNCS'] = array('count');
	
			// Add some information to the table
			$run_ids = "";
			foreach($runs as $run_id => $run_values) {
				if (tx_nG6_db::is_administrator($user_id, 'run', $run_values['id']) || $is_current_user_superadmin ) {
					$runs[$run_id]['is_admin'] = true;
				} else {
					$runs[$run_id]['is_admin'] = false;
				}
				$run_ids .= $run_values['id'].",";
				if ($this->piVars['project_id']) {
					$runs[$run_id]['href'] = $this->pi_list_linkSingle($run_values['name'],$run_values['id'],1, array('run_id'=>$run_values['id'], 'project_id'=>$run_values['project_id']));	
				} else {
					$runs[$run_id]['href'] = $this->pi_list_linkSingle($run_values['name'],$run_values['id'],1, array('run_id'=>$run_values['id']));
				}
			}
			$smarty->assign('runs', $runs);
			$smarty->assign('login_user', $user_name);
			$smarty->assign('user_login', $user_name);
			$smarty->assign('user_id', $user_id);
			$smarty->assign('data_folder', $this->conf["data"]);
			$smarty->assign('server_name', $this->conf["server_name"]);
			$smarty->assign('server_url', $this->conf['server_url']);
			$smarty->assign('run_ids', substr($run_ids,0,-1));
			$smarty->assign('single_run_display', $single_run_display);
			
			// If it's a single run, add analysis information
			if ($single_run_display) {
				
				//comments on the run
				$comments = tx_nG6_db::get_all_run_comments($runs[key($runs)]['id']);
				$smarty->assign('comments', $comments);
				
				$retention_policy = tx_nG6_db::select_run_retention_status($runs[key($runs)]['id']);
				$smarty->assign('run_data_state', $retention_policy['run_data_state']);
				$smarty->assign('run_retention_date', $retention_policy['run_retention_date']);
				$smarty->assign('run_purge_date', $retention_policy['run_purge_date']);
				
				$run_analysis = tx_nG6_db::get_run_analysis($runs[key($runs)]['id']);
				// Add some information to the table
				foreach($run_analysis as $analysis_id => $analysis_values) {
					$run_analysis[$analysis_id]['is_admin'] = tx_nG6_db::is_administrator($user_id, 'analyze', $analysis_values['id']);
					if($is_current_user_superadmin){
						$run_analysis[$analysis_id]['is_admin'] = 1;
					}
					if ($this->piVars['project_id']) {
						$run_analysis[$analysis_id]['href'] = $this->pi_list_linkSingle($analysis_values['name'],$analysis_values['id'],1, array('analyze_id'=>$analysis_values['id'], 'project_id'=>$this->piVars['project_id'], 'run_id'=>$this->piVars['run_id']));
					} else {
						$run_analysis[$analysis_id]['href'] = $this->pi_list_linkSingle($analysis_values['name'],$analysis_values['id'],1, array('analyze_id'=>$analysis_values['id'], 'run_id'=>$this->piVars['run_id']));
					}
				}
				$smarty->assign('h_analysis', tx_nG6_utils::trace_hierarchy($run_analysis));
				$smarty->assign('run_analysis', $run_analysis);
			}		
			return $smarty->fetch('run_view.tpl');
		} else {
			return $this->redirect_view();
		}
	}
	

	/**
	 * Return the analysis view
	 */
	function pi_analyze_view() {
	    $context = \TYPO3\CMS\Core\Utility\GeneralUtility ::makeInstance(Context::class);
	    if (!$context->getPropertyFromAspect('frontend.user', 'isLoggedIn')) { $user_id = null; }
		else { 
		    $user_id = $context->getPropertyFromAspect('frontend.user', 'id');
		    $user_name = $context->getPropertyFromAspect('frontend.user', 'username');
		}
		$is_current_user_superadmin = tx_nG6_db::is_user_ng6_superadmin($user_id);
		
		$smarty = new Smarty();
		$smarty->setTemplateDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/pi1/analyzes');
		$smarty->setCompileDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/templates_c');
		$smarty->setCacheDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/cache');
		$smarty->setConfigDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/configs');
		$smarty->security = true;
		$smarty->security_settings['MODIFIER_FUNCS'] = array('count');
		
		// First select the analyse
		$analyse = tx_nG6_db::select_analyse($this->piVars['analyze_id']);
		
		// is project admin
		$is_project_admin = tx_nG6_db::is_project_administrator($user_id, $analyse['project_id']);
		
		if( $is_current_user_superadmin ){ $is_project_admin = 1; }
		
		if (tx_nG6_db::user_is_authorized($user_id, $analyse['project_id'], $this->piVars['run_id']) || $is_current_user_superadmin ) {
			
			if ($is_project_admin || $analyse['hidden'] == "0") {
				// Handle old fashion
				if (file_exists($this->conf['data'].$analyse['directory'].'/index.html' )) {
					// Add the analyse description
					$content = '<div id="img_dialog" title=""></div>';
					$content .= '<input type="hidden" id="analyse_name" value="'.$analyse['name'].'" />';
					$content .= '<div'.$this->pi_classParam('singleView').'>';
					$content .= '<h2>Analyse '.$analyse['name'].' : <span>'.$analyse['description'].'</span></h2></div>';
					$fp = fopen((string)$this->conf['data'].$analyse['directory'].'/index.html',"r");
					while (!feof($fp)) {
						$content .= fgets($fp, 4096);
					}
					return $content;
					// Else meaning using smarty
				} else {
				
					$smarty->assign('is_project_admin', $is_project_admin);
					$smarty->assign('analyse', $analyse);
					$analysis_size = tx_nG6_db::get_analysis_size($this->piVars['analyze_id']);
					$smarty->assign('analyse_size', tx_nG6_utils::get_octet_string_representation($analysis_size));
					$smarty->assign('data_folder', $this->conf["data"]);
					$smarty->assign('analyze_id', $this->piVars['analyze_id']);
					$smarty->assign('server_url', $this->conf['server_url']);
					$smarty->assign('user_login', $user_name);
					$smarty->assign('user_id', $user_id);
					
					$retention_policy = tx_nG6_db::select_analysis_retention_status($this->piVars['analyze_id']);
					$smarty->assign('analysis_data_state', $retention_policy['analysis_data_state']);
					$smarty->assign('analysis_retention_date', $retention_policy['analysis_retention_date']);
					$smarty->assign('analysis_purge_date', $retention_policy['analysis_purge_date']);
				
					// Then select analyse results
					$results = tx_nG6_db::select_analyse_results($this->piVars['analyze_id']);
					$smarty->assign('analyse_results', $results);
				
					//comments on the analysis
					$comments = tx_nG6_db::get_all_analyze_comments($this->piVars['analyze_id']);
					$smarty->assign('comments', $comments);
				
					// Select the run file description
					if ($analyse['run_id'] != 'None') {
						$descriptions = tx_nG6_db::select_mid_descriptions($analyse['run_id']);
					} else {
						$descriptions = array();
					}
					$smarty->assign('descriptions', $descriptions);
					$smarty->assign('data_folder', $this->conf['data']);
				
					// Try to process the analyse template
					try {
						$GLOBALS['TSFE']->additionalHeaderData[$this->prefixId] .= '
						<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/tx_nG6_pi1_analysis.js"></script>
						<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/jquery.venny.min.js"></script>
						<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/jquery.highcharts.js"></script>
						<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/jquery.highcharts.exporting.js"></script>
						<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/jquery.highcharts.more.js"></script>
						<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/jquery.highcharts.exportcsv.js"></script>
						<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/jsphylosvg-min.js"></script>
						<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/raphael-min.js"></script>
						<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/unitip.js"></script>
						<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/jquery.jstree.min.js"></script>
						<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/d3-3.4.4.min.js"></script>
						<link type="text/css" rel="stylesheet" media="screen" href="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/css/unitip.css"/>';
						
						if (file_exists(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($this->extKey)).'pi1/analyzes/'.$analyse['class'].'.js') {
							$GLOBALS['TSFE']->additionalHeaderData[$this->prefixId] .= '
							<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'pi1/analyzes/'.$analyse['class'].'.js"></script>';
						}
						
						return $smarty->fetch($analyse['class'].'.tpl');
					} catch (Exception $e) {
						return 'No template found for class ' . $analyse['class'];
					}
				}
			}
		}
		return $smarty->fetch('AnalaysisAccessDenied.tpl');
	}
	
	
	/**
	 * Return the install view
	 */
	function pi_install_view() {
		$smarty = new Smarty();
		$smarty->setTemplateDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/pi1');
		$smarty->setCompileDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/templates_c');
		$smarty->setCacheDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/cache');
		$smarty->setConfigDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/configs');
		$smarty->security = true;
		$smarty->security_settings['MODIFIER_FUNCS'] = array('count');
		$smarty->assign('server_name', $this->conf["server_name"]);
		$smarty->assign('server_url', $this->conf['server_url']);
		$smarty->assign('pid', $this->conf['userpidList']);
		return $smarty->fetch('install_view.tpl');
	}
	/**
	 * Does installation need is finalized ( admin_install user exist in db )
	 *
	 * @param	integer		Alternative page ID for the link. (By default this function links to the SAME page!)
	 * @return	boolean		If true, intallation allready finalysed
	 */
	function pi_is_install_finalized()	{
		$res = tx_nG6_db::select_user_by_username("admin_install");
		if ($res == null) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Overloading of the tslib_pibase->pi_list_linkSingle function so piVars[showUid] is not set.
	 * Uses pi_linkTP for the linking
	 *
	 * @param	string		The content string to wrap in <a> tags
	 * @param	integer		UID of the record for which to display details (basically this will become the value of [showUid]
	 * @param	boolean		See pi_linkTP_keepPIvars
	 * @param	array		Array of values to override in the current piVars. Same as $overrulePIvars in pi_linkTP_keepPIvars
	 * @param	boolean		If true, only the URL is returned, not a full link
	 * @param	integer		Alternative page ID for the link. (By default this function links to the SAME page!)
	 * @return	string		The input string wrapped in <a> tags
	 * @see pi_linkTP(), pi_linkTP_keepPIvars()
	 */
	function pi_list_linkSingle($str,$uid,$cache=FALSE,$mergeArr=array(),$urlOnly=FALSE,$altPageId=0)	{
		if ($this->prefixId)	{
			if ($cache)	{
				$overrulePIvars=(array)$mergeArr;
				$str = $this->pi_linkTP($str,Array($this->prefixId=>$overrulePIvars),$cache,$altPageId);
			} else {
				$overrulePIvars=(array)$mergeArr;
				$str = $this->pi_linkTP_keepPIvars($str,$overrulePIvars,$cache,0,$altPageId);
			}

				// If urlOnly flag, return only URL as it has recently be generated.
			if ($urlOnly)	{
				$str = $this->cObj->lastTypoLinkUrl;
			}
		}
		return $str;
	}
	
}

if (defined('TYPO3') && $TYPO3_CONF_VARS[TYPO3]['XCLASS']['ext/nG6/pi1/class.tx_nG6_pi1.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3]['XCLASS']['ext/nG6/pi1/class.tx_nG6_pi1.php']);
}

?>
