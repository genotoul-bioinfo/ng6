{*
Copyright (C) 2009 INRA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of

ERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

<div id="user_information_dialog" title=""></div>
<div id="error_dialog" title=""></div>

<input type="hidden" id="server_name" value="{$server_name}" />
<input type="hidden" id="user_id" value="{$user_id}" />
<input type="hidden" id="data_folder" value="{$data_folder}" />
<input type="hidden" id="view" value="run" />
<input type="hidden" id="ids" value="{$run_ids}" />
<input type="hidden" id="login_user" value="{$login_user}" />
<input type="hidden" id="user_login" value="{$user_login}" />
<input type="hidden" id="server_url" value="{$server_url}" />

{* Find out if the current user is super user of a run *}
{assign var="is_at_least_admin_of_1_run" value=false}
{foreach from=$runs key=run_id item=run_values}
	{if $run_values.is_admin && !$is_at_least_admin_of_1_run}
		{assign var="is_at_least_admin_of_1_run" value=true}
	{elseif !$is_at_least_admin_of_1_run}
		{assign var="is_at_least_admin_of_1_run" value=false}
	{/if}
{/foreach}

{* If no runs can be displayed *}
{if $runs|@count == 0 || ($runs|@count == 1 && $runs[key($runs)].hidden == 1 && !$runs[key($runs)].is_admin) }
    <div class="sub-content sc-top clearfix">
		<div class="ng6-content-header-left run">
			<h2>Run <small>--</small></h2>
		</div>
		<div class="ng6-content-header-right">
		    <div class="alert alert-warning" > <strong>Access denied !</strong>Sorry you don't have access to this run. Please select another run</div>
		</div>
    </div>

    <div class="sub-content sc-bottom">&nbsp;</div>

{* If a single run has to be displayed *}
{elseif $single_run_display}
	<div class="sub-content sc-top">
		<div class="ng6-content-header-left run">
			<h2>Run <small>{$runs[key($runs)].name}</small></h2>
		</div>
		<div class="ng6-content-header-right">
			{$runs[key($runs)].description} <br/><br/>

			<div style="float:left;margin-right:25px">
				<p class="bullet"><strong>Project name: </strong> {$runs[key($runs)].project_name}</p>
				<p class="bullet"><strong>Date: </strong> {$runs[key($runs)].date|date_format:"%d/%m/%y"}</p>
				<p class="bullet"><strong>Species: </strong> {$runs[key($runs)].species}</p>
				<p class="bullet"><strong>Type: </strong> {$runs[key($runs)].type}</p>
				{if $is_at_least_admin_of_1_run && $login_user}
					<p style="margin: 6px 0 0;padding-left: 20px;"><span id="wfstatus_run_id_{$run_ids}"></span></p>
				{/if}
			</div>
			<div style="float:left">
				<p class="bullet"><strong>Data nature: </strong> {$runs[key($runs)].data_nature}</p>
				<p class="bullet"><strong>Number of sequences: </strong> {$runs[key($runs)].nb_sequences|number_format:0:' ':' '}</p>
				<p class="bullet"><strong>Full sequences length: </strong> {$runs[key($runs)].full_seq_size|number_format:0:' ':' '}</p>
				<p class="bullet"><strong>Sequencer: </strong> {$runs[key($runs)].sequencer}</p>
			</div>
			<div style="clear:both"></div>
			{*
			{assign var="run_size" value="<span id='size' class='tx-nG6-mini-wait'></span>"}
			<br />
			Raw data and analysis results use <strong>{$run_size}</strong> on the hard drive.<br/>
			*}
			{if $run_analysis|@count > 0}
				<strong>{$run_analysis|@count}</strong> analysis have been done on this run.<br/>
			{else}
				<strong>{$run_analysis|@count}</strong> analysis has been done on this run.<br/>
			{/if}
			{if $run_data_state == 'stored'}
				The run data is <strong>stored</strong>. It will be kept until <strong>{$run_retention_date}</strong>.<br/>
			{elseif $run_data_state == 'purged'}
				The run data has been <strong>purged</strong> on <strong>{$run_purge_date}</strong>.<br/>
			{elseif $run_data_state == 'extended'}
				The run data retention has been <strong>extended</strong> to <strong>{$run_retention_date}</strong>.<br/>
			{/if}
			
			

			<br/>
			<div  style="float:right;">
				{if $runs[key($runs)].is_admin }
					<button id="add_file" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-plus"></i> add files</button>
				{/if}
			</div>

		</div>
		<div style="clear:both"></div>
	</div>

	<div class="sub-content sc-bottom">
		<ul id="myTab" class="nav nav-tabs">
			<li class="active"><a href="#analyses" data-toggle="tab">Analyses</a></li>
	 		<li><a href="#downloads" data-toggle="tab">Raw data</a></li>
	 		{assign var="comment_tab" value="run_comments"}
	 		<li><a href="#{$comment_tab}" data-toggle="tab">Comments</a></li>
		</ul>

		<div id="myTabContent" class="tab-content">
	 		<div class="tab-pane fade in active" id="analyses">
	 			{* Find out if an analysis table should be displayed *}
				{assign var="display_analysis_result" value=false}
				{foreach from=$h_analysis key=analysis_id item=analysis_values}
					{if $analysis_values.is_admin && $analysis_values.hidden == 1}
						{assign var="display_analysis_result" value=true}
					{elseif $analysis_values.hidden == 0}
						{assign var="display_analysis_result" value=true}
					{/if}
				{/foreach}
				{assign var="is_admin" value=$runs[key($runs)].is_admin}
				{include file='analysis_table.tpl'}
	 		</div>

	 		<div class="tab-pane fade" id="downloads">
	 			{$dir=$data_folder|cat:$runs[key($runs)].directory}
				{assign var="nb_files" value=0}
				{foreach $dir|scandir as $file}
					{if $file != "." and $file !="" and $file != ".." and ($file|substr:-strlen(".png")) != ".png"}
						{$nb_files = $nb_files + 1}
					{/if}
				{/foreach}
				
				<ul>
				{if $nb_files >= 1}
					<div class="alert alert-info" name="retention-info">
						Retention date is {$runs[key($runs)].retention_date|date_format}. After this deadline, these data will no longer be available. Only metadata and quality control results will remain in NG6.
					</div>
					<br /> 
					<div class="alert alert-info" name="DownloadData_run">
						<p style="text-align: center;">You can download your files easily with <a href="https://ng6.toulouse.inra.fr/download" target="_blank"><strong>Menu > Download</strong></a>. Please visit the <a href="https://ng6.toulouse.inra.fr/faq" target="_blank"><strong>FAQ</strong></a> > "<strong>How to get my data?</strong>" section to know more.</p>
				</div>
				{/if}
				{assign var="nb_files" value=0}
				{foreach $dir|scandir as $file}
					{if $file != "." and $file !="" and $file != ".." and ($file|substr:-strlen(".png")) != ".png"}
						{$link=(('fileadmin'|cat:$runs[key($runs)].directory)|cat:'/')|cat:$file}
						<li class="filelist"><a href="{$link}">{$file}</a> <br /></li>
						{$nb_files = $nb_files + 1}
					{/if}
				{/foreach}
				</ul>
				{if $nb_files == 0}
					<div class="alert alert-info">
						{if $runs[key($runs)].data_state=="purged"}
							The data have been purged.  (Retention limit : {$runs[key($runs)].retention_date|date_format})
						{else}
							Results folder not synchronized yet... 
						{/if}

	    			</div>
				{/if}
	 		</div>

	 		{include file='comments.tpl' comments=$comments  tab_id=$comment_tab  add_new_comment=$runs[key($runs)].is_admin  user_id=$user_id }

 		</div>
 	</div>

{* If multiple runs have to be displayed *}
{else}
	{* Then display the table *}
	<div class="sub-content sc-top">
		<div class="ng6-content-header-left run">
			<h2>Runs list <small> you can access </small></h2>
		</div>
		<div class="ng6-content-header-right">
			You have access to <strong>{$runs|@count}</strong> runs.
			<br />
			{*
			{assign var="run_size" value="<span id='size' class='tx-nG6-mini-wait'></span>"}
			Raw data and analysis results use <strong>{$run_size}</strong> on the hard drive.
			*}
		</div>
		<div style="clear:both"></div>
	</div>

	<div class="sub-content sc-bottom">
		{include file='run_table.tpl'}
	</div>
{/if}

{include file='../template/modals.tpl'}
