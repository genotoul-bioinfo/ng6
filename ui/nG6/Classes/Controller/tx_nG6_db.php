<?php
namespace ng6\Controller;
/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/Classes/Controller/class.tx_nG6_utils.php');
use nG6\Controller\tx_nG6_utils;
/**
 * Class_nG6_db' for the 'nG6' extension.
 * This class is in charge of all communication with the database
 *
 * @author	PF bioinformatique de Toulouse <>
 */
class tx_nG6_db {
	
	/*
	 * Global functions
	*------------------------------------------------------------*/
	
	/**
	* Update the specified field of the table
	 *
	* @param	string  $table the entity to change
	* @param	string  $id     the element id to update
	* @param	string  $field  the field to change
	* @param	string  $value  the new value
	*/
        static function update_field($table, $id, $field, $value,$no_quote_fields=FALSE) {
            if( $field == 'uid' ) {
                throw new Exception ("The function 'update_field' cannot change an id.") ;
            }
            $array=array();
            if( is_array($field) ){
                    for($i =0; $i<count($field);$i++){
                            $array[$field[$i]]=$value[$i];
                    }
            }else{
                     $array=array($field => $value);
            }
            if (is_array($id)){
                $GLOBALS['TYPO3_DB']-> exec_UPDATEquery ($table, 'uid IN ('.implode(',',$id).')', $array,$no_quote_fields);
            }else{ 
                $GLOBALS['TYPO3_DB']-> exec_UPDATEquery ($table, 'uid='.$id, $array,$no_quote_fields);
            }
	}
	
	static function select_all_in_view($view, $where) {
		$queryParts = array(
					'SELECT' 	=> '*',
					'FROM' 		=> $view,
					'WHERE' 	=> $where ,
					'GROUPBY' 	=> '',
					'ORDERBY' 	=> '',
					'LIMIT' 	=> ''
			);	
			$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
			return ($res);
	}
	
	static function select_count_in_view($view,$where) {
		#select count All runs per project
		$queryParts = array(
					'SELECT' 	=> 'project_id , count(*) AS count',
					'FROM' 		=> $view ,
					'WHERE' 	=> $where,
					'GROUPBY' 	=> 'project_id',
					'ORDERBY' 	=> '',
					'LIMIT' 	=> ''
			);
		$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
                return ($res);

	}

	static function get_project_list_by_group_filter($create_user_id, $without_group_id, $with_group_id){
		$where=Array();
		$list_project_id_filter_by_group="";
		$list_project_id_laboratories="";
		### Search for project without group_id implied
                if($without_group_id !=""){
			#select all project_id with group OR/AND without group
			$queryParts=array(
				'SELECT' => 'project_id',
				'FROM' => 'tx_nG6_view_project_user',
				'WHERE' => 'user_group IN ( '.$without_group_id .')',
				'GROUPBY' 	=> '',
				'ORDERBY' 	=> '',
				'LIMIT' 	=> ''
			);
		
			$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
			while($res_row= $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
				if( $list_project_id_filter_by_group != ""){
					$list_project_id_filter_by_group .= ",".$res_row['project_id'];
				}else{
					$list_project_id_filter_by_group = $res_row['project_id'];
				}
			}
			$where[] = "uid NOT IN (".$list_project_id_filter_by_group.")";
		}
                ### Search for project with group_id implied
                if($with_group_id !=""){
			#select all project_id with group OR/AND without group
			$queryParts=array(
				'SELECT' => 'project_id',
				'FROM' => 'tx_nG6_view_project_user',
				'WHERE' => 'user_group IN ( '.$with_group_id .')',
				'GROUPBY' 	=> '',
				'ORDERBY' 	=> '',
				'LIMIT' 	=> ''
			);
		
			$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
			while($res_row= $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
				if( $list_project_id_filter_by_group != ""){
					$list_project_id_filter_by_group .= ",".$res_row['project_id'];
				}else{
					$list_project_id_filter_by_group = $res_row['project_id'];
				}
			}
			$where[] = "uid IN (".$list_project_id_filter_by_group.")";
		}
		### add create user filter
		if($create_user_id != ""){
				$where[] = "cruser_id IN (".$create_user_id.")";
		}
		$queryParts=array(
			'SELECT' => 'uid AS project_id',
			'FROM' => 'tx_nG6_project',
			'WHERE' => join (' AND ', $where),
			'GROUPBY' 	=> '',
			'ORDERBY' 	=> '',
			'LIMIT' 	=> ''
		);
		
		$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
		while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			if ($list_project_id_laboratories != "") {
				$list_project_id_laboratories .=",".$res_row['project_id'];
			}else{
				$list_project_id_laboratories=$res_row['project_id'];
				
			}
		}
    	return ($list_project_id_laboratories);
		
	}
	
        ################################
        # OBSOLETE / PURGE management
        ################################
	//Get list of obsolete run/analyses per project
	static function filter_list_retention_data_info($filter_space, $max_retention_date, $create_user_id, $without_user_group_id, $with_user_group_id) {
		
		$conditions=Array();
                //filter by date
                if ( $max_retention_date != "" ) {
                        //from user filter value
                        $conditions[] =  ' DATEDIFF( FROM_UNIXTIME( retention_date ) , STR_TO_DATE( "'.$max_retention_date.'", "%d/%m/%Y" ) ) <0';
                } else {
                        //from now
                        $conditions[] =  ' DATEDIFF( FROM_UNIXTIME( retention_date ) , now( ) ) <0';
                }
                $conditions[] = 'purge_demand_id IS NULL';
                $conditions[] = 'state IN ( \'stored\', \'extended\')';                
		
                if ($filter_space != "") {
					$conditions[] = "space_id = '$filter_space'";
				}

		$project_list_ids = "";
		if ($create_user_id != "" or $without_user_group_id != "" or $with_user_group_id != "") {
                        #retrieve list of project id created by a user and with or without a list of group_id
			$project_list_ids = tx_nG6_db::get_project_list_by_group_filter($create_user_id,$without_user_group_id, $with_user_group_id);
			$conditions[] = "project_id in (" . $project_list_ids.")" ;
		}
        $where = join(' AND ', $conditions);

        $log_file_path = tx_nG6_utils::get_log_path();
        
        return (tx_nG6_db::select_list_retention_data_info($where));
	}
	
	static function select_a_project_retention_data_info($project_id=null, $all=FALSE, $count_hidden=FALSE) {
		$condition=Array();
		if ($all === FALSE) {
			$condition[]='DATEDIFF( FROM_UNIXTIME( retention_date ) , now( ) ) <0';
		}
		if (null != $project_id) {
			$condition[]="project_id = " . $project_id ;
		}
		if(!$count_hidden){
			$condition[]="hidden = 0";
		}
		$where = join( ' AND ', $condition);
		return(tx_nG6_db::select_list_retention_data_info($where));
	}
	
	static function select_list_retention_data_info($where) {
		
		$by_project=array();
		$empty_res_structure = array(
						'project_name'			=> "",
						'state'					=> array("purged"  => array ('nb_run' => 0, 'size_run' => 0, 'run_ids'=> array(),
                                                "nb_analyze"=> 0, "size_analyze"=> 0, 'analysis_ids'=> array()),
                                                "extended"  => array ('nb_run' => 0, 'size_run' => 0, 'run_ids'=> array(),
                                                "nb_analyze"=> 0, "size_analyze"=> 0, 'analysis_ids'=> array()),
                                                "stored"  => array (	'nb_run' => 0, 'size_run' => 0, 'run_ids'=> array(),
							                                                "nb_analyze"=> 0, "size_analyze"=> 0, 'analysis_ids'=> array()
						                                                 )
                                                ),
						'total_purgeable_size'	=> 0,
						'nb_runs'				=>0,
						'nb_analyses'			=>0,
						'space_id' 		=> "");
		//select obsolete runs
		$res = tx_nG6_db::select_all_in_view( "tx_nG6_view_project_run" , $where);
		while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			$current_project_id = $res_row['project_id'];			
			if (!isset ($by_project[$res_row['project_id']])) {
				$by_project[$current_project_id]= $empty_res_structure;
				$by_project[$current_project_id]['project_name']= $res_row['project_name'];
				$by_project[$res_row['project_id']]['space_id'] = $res_row['space_id'];
			}
			
			
			
			if ($res_row['run_id'] != "") {
             $by_project[$res_row['project_id']]['state'][$res_row['state']]['nb_run']+=1;
             $key = 'storage_size';
				 if($res_row['state'] == "purged"){
				 	$key = "purged_size";
             }
             $by_project[$current_project_id]['state'][$res_row['state']]['size_run']+=$res_row[$key];
             $by_project[$current_project_id]['state'][$res_row['state']]['run_ids'][]=$res_row['run_id'];
				 $by_project[$current_project_id]['total_purgeable_size'] += $res_row['storage_size'];
			}
		}
		
		//select obsolete analyses of a run
		$res = tx_nG6_db::select_all_in_view("tx_nG6_view_project_run_analyze",$where,'10');
		while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			$current_project_id = $res_row['project_id'];
			if (!isset ($by_project[$res_row['project_id']])) {
				$by_project[$current_project_id]= $empty_res_structure;
				$by_project[$current_project_id]['project_name']= $res_row['project_name'];
				$by_project[$current_project_id]['space_id'] = $res_row['space_id'];
			}
			if ($res_row['analyze_id'] != ""){
        		$by_project[$current_project_id]['state'][$res_row['state']]['nb_analyze'] += 1;
           		$key = 'storage_size';
           		if($res_row['state'] == "purged"){
           			$key = "purged_size";
           		}
           		$by_project[$current_project_id]['state'][$res_row['state']]['size_analyze'] += $res_row[$key];
           		$by_project[$current_project_id]['state'][$res_row['state']]['analysis_ids'][]=$res_row['analyze_id'];
           		$by_project[$current_project_id]['total_purgeable_size'] += $res_row['storage_size'];
			}
		}
		
		//select obsolete analyses of a project
		$res = tx_nG6_db::select_all_in_view("tx_nG6_view_project_analyze", $where);
		while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			$current_project_id = $res_row['project_id'];
			if (!isset ($by_project[$res_row['project_id']])) {
				$by_project[$current_project_id]= $empty_res_structure;
				$by_project[$current_project_id]['project_name']= $res_row['project_name'];
				$by_project[$current_project_id]['space_id'] = $res_row['space_id'];
			}
			
			if ($res_row['analyze_id'] != ""){
				$by_project[$current_project_id]['state'][$res_row['state']]['nb_analyze'] += 1;
                $key = 'storage_size';
                if($res_row['state'] == "purged"){
                	$key = "purged_size";
                }
                $by_project[$current_project_id]['state'][$res_row['state']]['size_analyze'] += $res_row[$key];
				$by_project[$current_project_id]['state'][$res_row['state']]['analysis_ids'][]=$res_row['analyze_id'];
				$by_project[$current_project_id]['total_purgeable_size'] += $res_row['storage_size'];
                               
			}
		}
		
		$res = tx_nG6_db::select_count_in_view("tx_nG6_view_project_run",$where);
		while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			if (isset ($by_project[$res_row['project_id']])) {
				$by_project[$res_row['project_id']]["nb_runs"]=$res_row['count'];
			}
		}
		
		$res = tx_nG6_db::select_count_in_view("tx_nG6_view_project_run_analyze",$where);
		while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			if (isset ($by_project[$res_row['project_id']])) {
				$by_project[$res_row['project_id']]["nb_analyses"]=$res_row['count'];
			}
		}

		$res = tx_nG6_db::select_count_in_view("tx_nG6_view_project_analyze",$where);
		while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			if (isset ($by_project[$res_row['project_id']])) {
				if (!isset ($by_project[$res_row['project_id']]["nb_analyses"])) {
					$by_project[$res_row['project_id']]["nb_analyses"]=$res_row['count'];
				}else{
					$by_project[$res_row['project_id']]["nb_analyses"]+=$res_row['count'];
				}
			}
		}
		
		#retrieve user per project
		$res = tx_nG6_db::select_all_in_view("tx_nG6_view_project_user", "");
		while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			if(isset($by_project[$res_row['project_id']])){
				if (!isset ($by_project[$res_row['project_id']]["users"])) {
					$by_project[$res_row['project_id']]["users"]=array();
				}
				array_push($by_project[$res_row['project_id']]["users"],array ('user_id' => $res_row['user_id'], 'user_name' => $res_row['user_name'], 'email' =>$res_row['email'], 'groups' =>$res_row['user_group'], 'right_level_label' =>$res_row['right_level_label'], 'user_group_title' => $res_row['user_group_title']));
			}
		}
		
		$log_file_path = tx_nG6_utils::get_log_path();
		
		return($by_project);
	}

    /**
     * Add purge initial purge demand in db
     *
     * @param	int	$project_id	project id link to the purge demand
     * @param	int	$total_purgeable_size	size of files to purge
     * @return	New purge id
     */
    static function add_purge_demand($cruser_id, $project_id, $total_purgeable_size, $all_runs_ids, $all_analyses_ids,$user_ids){
      $date= time();
    	$purge_data = array(
			'cruser_id'			=>	$cruser_id,
         'project_id'      => $project_id,
         'analyze_ids'     => implode(',',$all_analyses_ids),
         'run_ids'         => implode(',',$all_runs_ids),
			'mail_sent_date'	=>	$date,
         'purge_size'      => $total_purgeable_size,
			'demand_state'	=>	"sent",
			'tstamp'			=> 	$date,
			'crdate'			=> 	$date
        );
    
    	$GLOBALS['TYPO3_DB']->exec_INSERTquery('tx_nG6_purge_demand', $purge_data);
        $purge_demand_id = $GLOBALS['TYPO3_DB']->sql_insert_id();
        //If the trigger "check_demand_insert" is triggered, the previous request will return 0, so we have to prevents the updates to tx_nG6_run and tx_nG6_analyzes in that case
		if( $purge_demand_id > 0){
        	tx_nG6_db::update_field('tx_nG6_run', $all_runs_ids, array('mail_sent_date','purge_demand_id'),array($date,$purge_demand_id));
        	tx_nG6_db::update_field('tx_nG6_analyze',$all_analyses_ids,array('mail_sent_date','purge_demand_id'),array($date,$purge_demand_id));
        
        	foreach($user_ids as $a_id){
                $purge_data_user = array(
                        'tstamp'			=> 	$date,
						'crdate'			=> 	$date,
                        'cruser_id'          =>	$cruser_id,
                        'purge_demand_id'    =>  $purge_demand_id,
                        'fe_users_id'        =>  $a_id
                );
                $GLOBALS['TYPO3_DB']->exec_INSERTquery('tx_nG6_purge_demand_fe_users', $purge_data_user);
        	}
        }
    	return $purge_demand_id;
    }
    
    static function get_purge_demand_from_id($values_str){
        #select all demand id not processed
        $queryParts=array(
                'SELECT' => ' uid as demand_id,project_id,purge_size,mail_sent_date,processed_date,demand_state,analyze_ids,run_ids',
                'FROM' => 'tx_nG6_purge_demand',
                'WHERE' => 'uid IN ('.$values_str.')',
                'GROUPBY' 	=> '',
                'ORDERBY' 	=> '',
                'LIMIT' 	=> ''
        );
        
        $res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
        $result=array();
        while($res_row= $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
                 $queryPartsEmails=array(
                        'SELECT' => ' email ',
                        'FROM' => 'fe_users JOIN  tx_nG6_purge_demand_fe_users ON fe_users.uid = tx_nG6_purge_demand_fe_users.fe_users_id',
                        'WHERE' => 'purge_demand_id = '.$res_row["demand_id"],
                        'GROUPBY' 	=> '',
                        'ORDERBY' 	=> '',
                        'LIMIT' 	=> ''
                );
                $emails=Array();
                $res_email = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryPartsEmails);
                while($email = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res_email)) {
                        $emails[]=$email["email"];
                }
                
                $result[]=array(
                "demand_id"=>$res_row["demand_id"],
                "project_id"=>$res_row["project_id"],
                "purge_size"=>$res_row["purge_size"],
                "mail_sent_date"=>$res_row["mail_sent_date"],
                "processed_date"=>$res_row["processed_date"],
                "demand_state"=>$res_row["demand_state"],
                "analyze_ids"=>$res_row["analyze_ids"],
                "run_ids"=>$res_row["run_ids"],
                "emails"=>$emails
                );
        }
        return($result);
    }  
    
    
    static function get_purge_demand_list($delay_in_days,$space_filter,$delay_exceeded_filter){        
        #We first build the WHERE conditions from the given parameters
        if ($space_filter != "") {
			$conditions[] = "space_id = '$space_filter'";
		}
		
		//For an unknown reason, we can't use the alias 'delay_excedeed' in the WHERE clause...
		if ($delay_exceeded_filter == 'yes') {
			$conditions[] = "DATEDIFF( now( ), FROM_UNIXTIME( tx_nG6_purge_demand.mail_sent_date ) ) >".$delay_in_days;
		}elseif($delay_exceeded_filter == 'no'){
			$conditions[] = "DATEDIFF( now( ), FROM_UNIXTIME( tx_nG6_purge_demand.mail_sent_date ) ) <".$delay_in_days;
		}
		$conditions[] = '`tx_nG6_purge_demand`.processed_date IS NULL';
        $where = join(' AND ', $conditions);
        
        #select all demand id not processed
    	$queryParts=array(
                'SELECT' => 'tx_nG6_purge_demand.uid AS demand_id, `tx_nG6_purge_demand`.project_id AS project_id, tx_nG6_project.name as project_name, 
                `tx_nG6_purge_demand`.purge_size AS purge_size, `tx_nG6_purge_demand`.mail_sent_date, `tx_nG6_purge_demand`.demand_state, 
                `fe_users`.username, `fe_groups`.title AS user_group_title, `tx_nG6_project`.space_id AS space_id,
                DATEDIFF( now( ), FROM_UNIXTIME( `tx_nG6_purge_demand`.mail_sent_date ) ) >'.$delay_in_days.' AS delay_excedeed',
                'FROM' => '`tx_nG6_purge_demand`
INNER JOIN tx_nG6_project ON tx_nG6_project.uid = tx_nG6_purge_demand.project_id
INNER JOIN `tx_nG6_purge_demand_fe_users` ON `tx_nG6_purge_demand_fe_users`.`purge_demand_id` = tx_nG6_purge_demand.uid
INNER JOIN fe_users ON fe_users.uid = `tx_nG6_purge_demand_fe_users`.`fe_users_id`
INNER JOIN fe_groups ON fe_groups.uid = fe_users.usergroup',
                'WHERE' 	=> $where,
                'GROUPBY' 	=> '',
                'ORDERBY' 	=> '',
                'LIMIT' 	=> ''
        );
        
        $results=array();
        $res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
        while($res_row= $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
                
                if ( array_key_exists($res_row["demand_id"] , $results)){
                        $results[$res_row["demand_id"]]["users"][]=array("username"=>$res_row["username"], "user_group_title"=>$res_row["user_group_title"]);
                } else {
                        $results[$res_row["demand_id"]]=array("project_id"=>$res_row["project_id"],
                                "purge_size"=>$res_row["purge_size"],
                                "mail_sent_date"=>date("d/m/Y",$res_row["mail_sent_date"]),
                                "project_name"=>$res_row["project_name"],
                                "demand_state"=>$res_row["demand_state"],
                                "delay_excedeed"=>$res_row["delay_excedeed"],
                                "space_id"=>$res_row["space_id"],
                                "users"=>array(array("username"=>$res_row["username"], "user_group_title"=>$res_row["user_group_title"]))
                                );
                }
        }
        return ($results);
    }
	
        static function extend_retention_date_from_demand($demand_ids, $date, $type_extend){
                 $result=array();               
                //extend
                $all_demands=tx_nG6_db::get_purge_demand_from_id( join(', ', $demand_ids));
                $all_runs=array();
                $all_analyses=array();
                foreach($all_demands as $res){
                        if ($res["demand_state"] != "deleted")
                        {
                                $all_runs=array();
                                $all_analyses=array();
                                if ($type_extend == 'extend_all_project') {
                                        tx_nG6_db::extend_retention_date_from_project($res["project_id"],$date, FALSE);
                                } else {
                                        $all_analyses=explode(',',$res["analyze_ids"]);
                                        $all_runs=explode(',',$res["run_ids"]);
                                        tx_nG6_db::extend_runs_analyses  ($all_runs, $all_analyses, $date);
                                }
                        }
                }
                tx_nG6_db::update_field('tx_nG6_purge_demand',$demand_ids,
                        array("processed_date","demand_state"),
                        array($date,"extended"));
                $result[] = "Done extend";
                return $result;
        }	
        
        static function extend_retention_date_from_project($projects_ids, $date, $check_purge){
                
                        $all_project_filter = " project_id in (".$projects_ids. ") AND DATEDIFF( FROM_UNIXTIME( retention_date ), FROM_UNIXTIME(" .$date. ") ) <0
                        AND state in ('extended','stored') ";
                        
                        //$all_project=tx_nG6_db::get_purge_demand_from_id( join(', ', $demand_ids));
                        $res_project_analyze = tx_nG6_db::select_all_in_view("tx_nG6_view_project_analyze", $all_project_filter);
                        $res_project_run = tx_nG6_db::select_all_in_view("tx_nG6_view_project_run", $all_project_filter);
                        $res_project_run_analyze = tx_nG6_db::select_all_in_view("tx_nG6_view_project_run_analyze", $all_project_filter);
                        $purge_ids=Array();
                        while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res_project_analyze)) {
                                $all_analyses[] = $res_row["analyze_id"];
                                if ($res_row["purge_demand_id"] != NULL) {
                                        $purge_ids[] = $res_row["purge_demand_id"];
                                }
                        }
                        while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res_project_run_analyze)) {
                                $all_analyses[] = $res_row["analyze_id"];
                                if ($res_row["purge_demand_id"] != NULL) {
                                        $purge_ids[] = $res_row["purge_demand_id"];
                                }
                        }
                        while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res_project_run)) {
                                $all_runs[] = $res_row["run_id"];
                                if ($res_row["purge_demand_id"] != NULL) {
                                        $purge_ids[] = $res_row["purge_demand_id"];
                                }
                        }
                       tx_nG6_db::extend_runs_analyses  ($all_runs, $all_analyses, $date);
                       if ($check_purge == True){
                               $purge_uniq = array_unique ($purge_ids);
                               
                               if (count($purge_uniq) > 0) {
                                        tx_nG6_db::update_field('tx_nG6_purge_demand',$purge_uniq,
                                        array("processed_date","demand_state"),
                                        array($date,"extended"));
                               }
                       }
                       return array(1);
        }
        static function extend_runs_analyses($all_runs, $all_analyses, $date){
                tx_nG6_db::update_field('tx_nG6_run', $all_runs, 
                        array("retention_date","data_state", "mail_sent_date","purge_demand_id"),
                        array($date,"extended","0","NULL"),
                        array("purge_demand_id"));
                tx_nG6_db::update_field('tx_nG6_analyze',$all_analyses,
                        array("retention_date","data_state", "mail_sent_date","purge_demand_id"),
                        array($date,"extended","0","NULL"),
                        array("purge_demand_id"));
        }
        
        static function delete_purge_demand($demand_ids,$user_login, $user_pwd, $data_folder){
                $date= time();
                $all_demands=tx_nG6_db::get_purge_demand_from_id($demand_ids);
                $directories=array();
                $res = 0 ;
                foreach($all_demands as $demand){
                        if ($demand["state"] != "deleted"){      
                                if ($demand["analyze_ids"] != ""){
                                        foreach(explode(',',$demand["analyze_ids"]) as $a_id){
                                               $res = tx_nG6_db::purge_analysis($a_id, $user_login, $user_pwd, $data_folder);
                                               if ($res != 0) {
                                                      break;  
                                                }
                                        }
                                }
                                if ($res == 0) {
                                        if ($demand["run_ids"] != ""){
                                                foreach(explode(',',$demand["run_ids"]) as $r_id){
                                                        
                                                        $res = tx_nG6_db::purge_run($r_id, $user_login, $user_pwd, $data_folder);
                                                        if ($res != 0) {
                                                              break;  
                                                        }
                                                }
                                        }
                                }
                                else {
                                        break;
                                }
                        }
                }
                # finally set purge demand as purged
                if ($res == 0){
                        tx_nG6_db::update_field('tx_nG6_purge_demand',explode(',',$demand_ids),
                        array("processed_date","demand_state"),
                        array($date,"purged"));
                }
                return $res;
        }	
    /**
     * Select all project for the current user
     *
     * @param	string	$by	by can group, organism, location
     * @param	string	$role	role can be create_user, manager
     * @return	table with all projects
     */
    static function select_projects_repartition($role, $by='title') {
    	if ($role == "create_user") {
    		$queryParts = array(
    				'SELECT' 	=> 'tx_nG6_project.uid as pid, fe_groups.title as title, fe_groups.uid as group_id , fe_groups.tx_nG6_organism as organism, fe_groups.tx_nG6_location as location',
    				'FROM' 		=> 'tx_nG6_project '
    				.' INNER JOIN fe_users ON tx_nG6_project.cruser_id=fe_users.uid '
    				.' INNER JOIN fe_groups ON fe_groups.uid=fe_users.usergroup ',
    				'WHERE' 	=> "",
    				'GROUPBY' 	=> '',
    				'ORDERBY' 	=> '',
    				'LIMIT' 	=> ''
    		);
    		//$where = "tx_nG6_project.cruser_id="
    	} else if ($role == "manager") {
	    	$queryParts = array(
    				'SELECT' 	=> 'tx_nG6_project.uid as pid, fe_groups.title as title, fe_groups.uid as group_id , fe_groups.tx_nG6_organism as organism, fe_groups.tx_nG6_location as location',
	    			'FROM' 		=> 'tx_nG6_project '
	    			.' INNER JOIN fe_rights ON tx_nG6_project.uid=fe_rights.project_id '
	    			.' INNER JOIN fe_users ON fe_rights.fe_user_id=fe_users.uid '
	    			.' INNER JOIN fe_groups ON fe_groups.uid=fe_users.usergroup ',
	    			'WHERE' 	=> "fe_rights.right_id=1",
	    			'GROUPBY' 	=> '',
	    			'ORDERBY' 	=> '',
	    			'LIMIT' 	=> ''
	    	);
    	}
    	$by_title = array() ;
    	$by_organism = array();
    	$by_location = array();
    	$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
    	while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
    		if (!isset ($by_title[$res_row['title']])) {
    			$by_title[$res_row['title']] = array(
    					'count'			=> 1,
    					'group_id'	=> $res_row['group_id'],
    					'organism'		=> $res_row['organism'],
    					'location'		=> $res_row['location']);
    		} else {
    			$by_title[$res_row['title']]['count'] += 1;
    		}
    		
    		if (!isset ($by_organism[$res_row['organism']])) {
    			$by_organism[$res_row['organism']] = array(
    					'count'			=> 1);
    		} else {
    			$by_organism[$res_row['organism']]['count'] += 1;
    		}
    		
    		if (!isset ($by_location[$res_row['location']])) {
    			$by_location[$res_row['location']] = array(
    					'count'			=> 1);
    		} else {
    			$by_location[$res_row['location']]['count'] += 1;
    		}
    	}
    	if ($by=='title') {
    		return ($by_title);
    	} else if ($by=='organism') {
    		return ($by_organism);
    	} else if ($by=='location') {
    		return ($by_location);
    	}
    }
    static function select_all_projects(){
            $result=Array();
            $queryParts = array(
    				'SELECT' 	=> 'tx_nG6_project.uid as pid,  tx_nG6_project.name as project_name ',
    				'FROM' 		=> 'tx_nG6_project ',
    				'WHERE' 	=> "",
    				'GROUPBY' 	=> '',
    				'ORDERBY' 	=> '',
    				'LIMIT' 	=> ''
    		);
                $res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
                while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
                        $results[] =  $res_row;
                }
                return($results);
    }
    static function select_projects_evolution($values, $by, $role, $cumulate) {
        if ($by == "organism" || $by == "location") {
                $by = "tx_nG6_".$by;
        }

        $from = "" ;
        $where = "fe_groups.".$by." IN ('".implode("', '", $values)."')" ;
    	if ($role == "create_user") {
    		$from = 'tx_nG6_project '
	    			.' INNER JOIN fe_users ON tx_nG6_project.cruser_id=fe_users.uid '
	    			.' INNER JOIN fe_groups ON fe_groups.uid=fe_users.usergroup ' ;
    	} else if ($role == "manager") {
    		$from = 'tx_nG6_project '
    				.' INNER JOIN fe_rights ON tx_nG6_project.uid=fe_rights.project_id '
    				.' INNER JOIN fe_users ON fe_rights.fe_user_id=fe_users.uid '
    				.' INNER JOIN fe_groups ON fe_groups.uid=fe_users.usergroup ';
    		$where .= " AND fe_rights.right_id=1" ;
    	}
    	$queryParts = array(
    			'SELECT' 	=> 'tx_nG6_project.crdate, count(tx_nG6_project.crdate) nb',
    			'FROM' 		=> $from,
    			'WHERE' 	=> $where,
    			'GROUPBY' 	=> 'tx_nG6_project.crdate',
    			'ORDERBY' 	=> 'tx_nG6_project.crdate',
    			'LIMIT' 	=> ''
    	);
    	$results = array();
    	$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
    	while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
    		if ($cumulate && count($results) > 0) {
    			$results[] = array($res_row["crdate"], $res_row["nb"] + $results[count($results)-1][1]);
    		} else {
    			$results[] = array($res_row["crdate"], $res_row["nb"]);
    		}
    	}
    	return $results;
    }

    static function select_projects_distribution($values, $by, $role) {
        $from = "" ;
        $where = "" ;
        if ($by == "organism" || $by == "location") {
                $by = "tx_nG6_".$by;
        }
    	if ($role == "create_user") {
    		$from = 'tx_nG6_project '
	    			.' INNER JOIN fe_users ON tx_nG6_project.cruser_id=fe_users.uid '
	    			.' INNER JOIN fe_groups ON fe_groups.uid=fe_users.usergroup ' ;
    	} else if ($role == "manager") {
    		$from = 'tx_nG6_project '
    				.' INNER JOIN fe_rights ON tx_nG6_project.uid=fe_rights.project_id '
    				.' INNER JOIN fe_users ON fe_rights.fe_user_id=fe_users.uid '
    				.' INNER JOIN fe_groups ON fe_groups.uid=fe_users.usergroup ';
    		$where = "fe_rights.right_id=1" ;
    	}
    	$queryParts = array(
    			'SELECT' 	=> 'fe_groups.'.$by.', count(fe_groups.'.$by.') nb',
    			'FROM' 		=> $from,
    			'WHERE' 	=> $where,
    			'GROUPBY' 	=> 'fe_groups.'.$by,
    			'ORDERBY' 	=> '',
    			'LIMIT' 	=> ''
    	);
    	$results = array();
    	$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
    	$others = 0 ;
    	while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
    		if( in_array($res_row[$by], $values) ) {
    			$results[] = array($res_row[$by], $res_row["nb"]);
    		} else {
    			$others += $res_row["nb"] ;
    		}
    	}
    	if ($others != 0) { 
    		$results[] = array("OTHERS", $others);
    	}
    	return $results;
    }
    
    static function select_storage_distribution($values, $by, $role, $get_analyzes, $octet) {
    	if ($by == "organism" || $by == "location") {
    		$by = "tx_nG6_".$by;
    	}
    	$storage_unit = "full_seq_size";
    	if ($octet) {
    		$storage_unit = "storage_size";
    	}
    	$from = "" ;
    	$where = "" ;
    	if ($role == "create_user") {
    		$from = 'tx_nG6_project '
    		.' INNER JOIN tx_nG6_project_run ON tx_nG6_project_run.project_id= tx_nG6_project.uid'
    		.' INNER JOIN tx_nG6_run ON tx_nG6_project_run.run_id= tx_nG6_run.uid'
    		.' INNER JOIN fe_users ON tx_nG6_project.cruser_id=fe_users.uid '
    		.' INNER JOIN fe_groups ON fe_groups.uid=fe_users.usergroup ' ;
    	} else if ($role == "manager") {
    		$from = 'tx_nG6_project '
    		.' INNER JOIN tx_nG6_project_run ON tx_nG6_project_run.project_id= tx_nG6_project.uid'
    		.' INNER JOIN tx_nG6_run ON tx_nG6_project_run.run_id= tx_nG6_run.uid'
    		.' INNER JOIN fe_rights ON tx_nG6_project.uid=fe_rights.project_id '
    		.' INNER JOIN fe_users ON fe_rights.fe_user_id=fe_users.uid '
    		.' INNER JOIN fe_groups ON fe_groups.uid=fe_users.usergroup ';
    		$where = "fe_rights.right_id=1" ;
    	}
    	$queryParts = array(
    			'SELECT' 	=> 'tx_nG6_run.uid, tx_nG6_run.'.$storage_unit.', fe_groups.'.$by,
    			'FROM' 		=> $from,
    			'WHERE' 	=> $where,
    			'GROUPBY' 	=> '',
    			'ORDERBY' 	=> '',
    			'LIMIT' 	=> ''
    	);
    	
    	$results = array();
    	$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
    	$others = 0 ;
    	while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			
    		$analyses_size = 0;
    		if ($get_analyzes && $octet) {
    			$run_analysis = tx_nG6_db::get_run_analysis($res_row["uid"]);
    			foreach($run_analysis as $analyse_id => $analyze_values) {
    				$analyses_size += intval($analyze_values["storage_size"]);
    			}
    		}
    		if( in_array($res_row[$by], $values) ) {
    			
    			if( array_key_exists( $res_row[$by], $results) ) {
    				$results[$res_row[$by]] += $res_row[$storage_unit] + $analyses_size;
    			} else {
    				$results[$res_row[$by]] = $res_row[$storage_unit] + $analyses_size;
    			}
    		} else {
    			$others += $res_row[$storage_unit] + $analyses_size;
    		}
    	}
    	if ($others != 0) {
    		$results["OTHERS"] = $others;
    	}
    	$final_results = array();
    	foreach($results as $group_key => $group_values) {
    		$final_results[] = array( $group_key, $group_values );
    	}
    	return $final_results;
    }
    
	static function select_storage_evolution($values, $by, $role, $get_analyzes, $octet, $cumulate) {
        if ($by == "organism" || $by == "location") {
                $by = "tx_nG6_".$by;
        }
        $storage_unit = "full_seq_size";
        if ($octet) {
        	$storage_unit = "storage_size";
        }
        $from = "" ;
        $where = "fe_groups.".$by." IN ('".implode("', '", $values)."')" ;
       
        if ($role == "create_user") {
                $from = 'tx_nG6_project '
                .' INNER JOIN tx_nG6_project_run ON tx_nG6_project_run.project_id= tx_nG6_project.uid'
                .' INNER JOIN tx_nG6_run ON tx_nG6_project_run.run_id= tx_nG6_run.uid'
                .' INNER JOIN fe_users ON tx_nG6_project.cruser_id=fe_users.uid '
                .' INNER JOIN fe_groups ON fe_groups.uid=fe_users.usergroup ' ;
        } else if ($role == "manager") {
                $from = 'tx_nG6_project '
                .' INNER JOIN tx_nG6_project_run ON tx_nG6_project_run.project_id= tx_nG6_project.uid'
                .' INNER JOIN tx_nG6_run ON tx_nG6_project_run.run_id= tx_nG6_run.uid'
                .' INNER JOIN fe_rights ON tx_nG6_project.uid=fe_rights.project_id '
                .' INNER JOIN fe_users ON fe_rights.fe_user_id=fe_users.uid '
                .' INNER JOIN fe_groups ON fe_groups.uid=fe_users.usergroup ';
                $where .= " AND fe_rights.right_id=1" ;
        }
        $queryParts = array(
                        'SELECT'        => 'tx_nG6_run.uid, tx_nG6_run.crdate, tx_nG6_run.'.$storage_unit.', tx_nG6_run.purged_size, fe_groups.'.$by,
                        'FROM'          => $from,
                        'WHERE'         => $where,
                        'GROUPBY'       => '',
                        'ORDERBY'       => 'tx_nG6_run.crdate',
                        'LIMIT'         => ''
        );

        $results = array();
        $res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
        while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
                $analyses_size = 0;
                $analyses_size_purged = 0;
                if ($get_analyzes && $octet) {
                        $run_analysis = tx_nG6_db::get_run_analysis($res_row["uid"]);
                        foreach($run_analysis as $analyse_id => $analyze_values) {
                                $analyses_size += intval($analyze_values["storage_size"]);
                                $analyses_size_purged += intval($analyze_values["purged_size"]) + intval($analyze_values["storage_size"]);
                        }
                }

                if( array_key_exists( $res_row["crdate"], $results) ) {
                        $results[$res_row["crdate"]][0] += $res_row[$storage_unit] + $analyses_size;
                        $results[$res_row["crdate"]][1] += $res_row["purged_size"] + $analyses_size_purged;
                } else {
                        $results[$res_row["crdate"]] = array($res_row[$storage_unit] + $analyses_size, $res_row["purged_size"] + $analyses_size_purged);
                }
        }
        
        $final_results = array();
        foreach($results as $group_key => $group_values) {
                if ($cumulate && count($final_results) > 0) {
                        $final_results[] = array( $group_key, $group_values[0] + $final_results[count($final_results)-1][1],$group_values[0] + $group_values[1] + $final_results[count($final_results)-1][2]);
                } else {
                        $final_results[] = array( $group_key, $group_values[0], $group_values[0] + $group_values[1] );
                }

        }
        return $final_results;
    }

	/**
	 * Add a comment to the comment table
	 * @param unknown $table_name
	 * @param unknown $id_label
	 * @param unknown $element_id
	 * @param unknown $cruser_id
	 * @param unknown $new_comment
	 */
    static function add_comment($table_name, $id_label, $id, $cruser_id, $new_comment){
    	$new_comment = htmlentities($new_comment, ENT_QUOTES | ENT_COMPAT | ENT_HTML5);
    	$comment_data = array(
    	 		'cruser_id'			=>	$cruser_id,
    	 		'comment'			=>	$new_comment,
    	 		'tstamp'			=> 	time(),
    	 		'crdate'			=> 	time()
    	);
    	$GLOBALS['TYPO3_DB']->exec_INSERTquery('tx_nG6_comment', $comment_data);
    	$comment_id =  $GLOBALS['TYPO3_DB']->sql_insert_id();
    	
    	$comment_link_data = array(
    	 		'cruser_id'		=>	$cruser_id,
    	 		$id_label		=>	$id,
    			'comment_id'	=> 	$comment_id,
    	 		'tstamp'		=> 	time(),
    	 		'crdate'		=> 	time()
    	);
    	
    	$GLOBALS['TYPO3_DB']->exec_INSERTquery($table_name, $comment_link_data);
    	return $GLOBALS['TYPO3_DB']->sql_insert_id();
    }

    /**
     * Retrieve all comments from a given project, run or analysis
     * @param unknown $table_name
     * @param unknown $id_label
     * @param unknown $uid
     * @return multitype:multitype:unknown
     */
    static function get_all_comments($table_name, $id_label, $id){
    	$comments = array();
    	$temp = array();
    	$queryParts = array(
    			'SELECT' 	=> 'tx_nG6_comment.uid AS comment_id, '.
    			'tx_nG6_comment.comment AS comment, '.
    			'tx_nG6_comment.cruser_id AS cruser_id, '.
    			'tx_nG6_comment.crdate AS comment_crdate ',
    			'FROM' 		=> $table_name .' INNER JOIN tx_nG6_comment ON '. $table_name . '.comment_id=tx_nG6_comment.uid',
    			'WHERE' 	=> $table_name . '.' . $id_label . '=' . $id,
    			'ORDERBY' 	=> 'comment_id',
    	);
    	 
    	$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
    	while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
    		$comment_id = $row['comment_id'];
    		if (!isset ($temp['comment_'.$comment_id])) {
    			$temp['comment_'.$comment_id] = '';
    			$user_info = tx_nG6_db::get_user_informations($row['cruser_id']);
    			$comments[] = array(
    					'id'			=> $comment_id,
    					'comment'		=> html_entity_decode($row['comment'], ENT_QUOTES | ENT_COMPAT | ENT_HTML5),
    					'date'			=> date( 'd.M.Y-H:i' ,$row['comment_crdate']),
    					'cruser_info'	=> $user_info,
    			);
    		}
    	}
    	return $comments;
    }
    
    /**
     * Delete a given commentary from the database
     * @param unknown $table_name
     * @param unknown $id_label
     * @param unknown $id
     * @param unknown $comment_id
     */
    static function delete_comment($table_name, $id_label, $id, $comment_id){
    	
    	$res = 0 ;
    	//from association table
    	$where = $table_name.'.'.$id_label.'='.$id.' AND '.$table_name.'.comment_id='.$comment_id ;
    	$res = $GLOBALS['TYPO3_DB']-> exec_DELETEquery ($table_name, $where);
    	
    	//from comment table
    	$where = 'tx_nG6_comment.uid='.$comment_id ;
    	$res = $GLOBALS['TYPO3_DB']-> exec_DELETEquery ('tx_nG6_comment', $where);
    	if ($res != 1) {
    		$res = 1 ;
    	}
    	return $res ;
    }
    
    /**
     * Update an existing comment
     * @param unknown $comment_id
     * @param unknown $new_comment
     */
    static function update_comment($comment_id, $new_comment){
    	return $GLOBALS['TYPO3_DB']-> exec_UPDATEquery ('tx_nG6_comment', 'uid='.$comment_id, array('comment' => $new_comment));
    }
    
    
	/*
	 * Project functions
	 *------------------------------------------------------------*/

    /**
     * Add a comment to the project described by project_id
     * @param unknown $project_id
     * @param unknown $cruser_id
     * @param unknown $new_comment
     */
    static function add_project_comment($project_id, $cruser_id, $new_comment){
    	return tx_nG6_db::add_comment('tx_nG6_project_comment', 'project_id', $project_id, $cruser_id, $new_comment);
    }
    
    /**
     * Retrieve all comments from a given project
     * @param unknown $project_id
     */
    static function get_all_project_comments($project_id){
    	return tx_nG6_db::get_all_comments('tx_nG6_project_comment', 'project_id', $project_id);
    }
    
    /**
     * Delete a given project comment
     * @param unknown $project_id
     * @param unknown $comment_id
     */
    static function delete_project_comment($project_id, $comment_id){
    	return tx_nG6_db::delete_comment('tx_nG6_project_comment', 'project_id', $project_id, $comment_id);
    }
    
	/**
	 * Select all project for the current user
	 * 
	 * @param	string	$user_id	the user id
	 * @return	table with all projects
	 */
	static function select_all_user_projects($user_id, $orderby='', $limit='') {		
		$projects = array();
		
		// If the user is not logged on display demonstration project
		if ($user_id == null) { $where = 'tx_nG6_project.public=0'; } 
		else { $where = 'fe_rights.fe_user_id='.$user_id; }
		
		//First, get the analyzis project-scale
		$queryParts = array(
			'SELECT' => 'distinct(tx_nG6_project.uid) AS project_id, '.
						'tx_nG6_project.name AS project_name, '.
						'tx_nG6_project.description AS project_description, '.
						'tx_nG6_project.public AS project_public, '.
						'tx_nG6_project.crdate AS project_crdate, '.
						'tx_nG6_project.hidden AS project_hidden ',
			'FROM' => 'tx_nG6_project INNER JOIN fe_rights ON fe_rights.project_id=tx_nG6_project.uid LEFT OUTER JOIN tx_nG6_project_run ON tx_nG6_project_run.project_id = tx_nG6_project.uid',
			'WHERE' => $where,
			'GROUPBY' => '',
			'ORDERBY' => $orderby,
			'LIMIT' => $limit
		);
		$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);		
		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			$project_id = $row['project_id'];
			if (!isset ($projects['project_'.$project_id])) {		
				$projects['project_'.$project_id] = array(
					'id'			=> $project_id,				
					'name'			=> $row['project_name'],
					'hidden'		=> $row['project_hidden'],
					'date'			=> $row['project_crdate'],
					'public'		=> $row['project_public'],
					'description'	=> $row['project_description']);
			}
						
		}		
		return $projects;
	}

	/**
	* Select all project for the current user
	*
	* @param	string	$user_id	the user id
	* @return	table with all projects
	*/
	static function get_user_projects($user_id, $orderby='', $limit='') {
		$projects = array();
	
		// If the user is not logged on display demonstration project
		if ($user_id == null) {
			$where = 'tx_nG6_project.public=0';
		}
		else { 
				$where = 'fe_rights.fe_user_id='.$user_id.' AND ((fe_rights.right_id<>2 AND tx_nG6_project.hidden=0) '
				.'OR fe_rights.right_id=2)' ;
		}
	
		//First, get the analyzis project-scale
		$queryParts = array(
				'SELECT' => 'tx_nG6_project.uid AS project_id, '.
							'tx_nG6_project.name AS project_name, '.
							'tx_nG6_project.description AS project_description, '.
							'tx_nG6_project.public AS project_public, '.
							'tx_nG6_project.crdate AS project_crdate, '.
							'tx_nG6_project.hidden AS project_hidden ',
				'FROM' => 'tx_nG6_project INNER JOIN fe_rights ON fe_rights.project_id=tx_nG6_project.uid',
				'WHERE' => $where,
				'GROUPBY' => '',
				'ORDERBY' => $orderby,
				'LIMIT' => $limit
		);
		$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			$project_id = $row['project_id'];
			if (!isset ($projects['project_'.$project_id])) {
				$projects['project_'.$project_id] = array(
						'id'			=> $project_id,				
						'name'			=> $row['project_name'],
						'hidden'		=> $row['project_hidden'],
						'date'			=> $row['project_crdate'],
						'public'		=> $row['project_public'],
						'description'	=> $row['project_description']);
			}
	
		}
		return $projects;
	}
	
	/**
	 * Select all project for the superadmin
	 * 
	 * @param	string	$user_id	the user id
	 * @return	table with all projects
	 */
	static function select_superadmin_projects() {		
		$projects = array();
		
		$queryParts = array(
			'SELECT' => 'tx_nG6_project.uid AS project_id, '.
						'tx_nG6_project.name AS project_name, '.
						'tx_nG6_project.description AS project_description, '.
						'tx_nG6_project.public AS project_public, '.
						'tx_nG6_project.crdate AS project_crdate, '.
						'tx_nG6_project.hidden AS project_hidden ',
			'FROM' => 'tx_nG6_project',
			'WHERE' => $where,
			'GROUPBY' => '',
			'ORDERBY' => $orderby,
			'LIMIT' => $limit
		);
		$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);		
		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			$project_id = $row['project_id'];
			if (!isset ($projects['project_'.$project_id])) {		
				$projects['project_'.$project_id] = array(
					'id'			=> $project_id,				
					'name'			=> $row['project_name'],
					'hidden'		=> $row['project_hidden'],
					'date'			=> $row['project_crdate'],
					'public'		=> $row['project_public'],
					'description'	=> $row['project_description']);
			}

		}		
		return $projects;
	}

	/**
	 * Select a run from the database
	 *
	 * @param	string	$project_id	the project id to return
	 * @return	hash table with all project information
	 */
	static function select_project($project_id) {
		$queryParts = array(
			'SELECT' => '*',
			'FROM' => 'tx_nG6_project',
			'WHERE' => 'tx_nG6_project.uid = '.$project_id,
			'GROUPBY' => '',
			'ORDERBY' => '',
			'LIMIT' => ''
		);
		$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
		$vals = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
		return array('id'			=> $vals['uid'],				
					 'name'			=> $vals['name'],
					 'hidden'		=> $vals['hidden'],
					 'date'			=> $vals['crdate'],
					 'public'		=> $vals['public'],
					 'description'	=> $vals['description'],
					 'space_id'	    => $vals['space_id']);
	}

	/**
	 * Select all runs linked to the specified project
	 *
	 * @param	string	$project_id	the project id
	 * @return	hash table with all runs information
	 */
static function get_project_runs($project_id, $get_hidden=TRUE, $orderby='', $limit='') {
    	
		// First select all runs from the database
		$queryParts = Array( 
			'SELECT' => 'tx_nG6_run.uid AS run_id,'.
					 	'tx_nG6_run.directory AS run_directory,'.
					 	'tx_nG6_run.species AS run_species,'.
						'tx_nG6_run.description AS run_description, '.
						'tx_nG6_run.data_nature AS run_data_nature, '.
						'tx_nG6_run.sequencer AS run_sequencer, '.
						'tx_nG6_run.type AS run_type, '.
						'tx_nG6_run.name AS run_name, '.
						'tx_nG6_run.nb_sequences AS run_nb_sequences, '.
						'tx_nG6_run.full_seq_size AS run_full_seq_size, '.
						'tx_nG6_run.date AS run_date, '.
						'tx_nG6_run.hidden AS run_hidden, '.
						'tx_nG6_run.data_state AS run_data_state, '.
						'tx_nG6_run.retention_date AS run_retention_date, '.
						'tx_nG6_run.purged_date AS run_purged_date, '.
						'tx_nG6_run.purged_size AS run_purged_size, '.
						'tx_nG6_run.mail_sent_date AS run_mail_sent_date, '.
						'tx_nG6_project.uid AS project_id, '.
						'tx_nG6_project.name AS project_name ',
			'FROM' => 'tx_nG6_project '.
  						'INNER JOIN tx_nG6_project_run ON  tx_nG6_project.uid=tx_nG6_project_run.project_id '.
  						'INNER JOIN tx_nG6_run ON tx_nG6_project_run.run_id=tx_nG6_run.uid ',
			'WHERE' => 'tx_nG6_project.uid='.$project_id,
			'GROUPBY' => '',
			'ORDERBY' => $orderby,
			'LIMIT' 	=> $limit
		);
		
		if(!$get_hidden){
			$queryParts['WHERE'] .= ' AND tx_nG6_run.hidden = 0';
		}

	
		// Then create the result hash table
		$results = array();
		$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			$run_id = $row['run_id'];				
			if (!isset($results['run_'.$run_id])) {				
				$results['run_'.$run_id] = array( 
					'id'			=> $run_id,
					'directory'		=> $row['run_directory'], 
					'name' 			=> $row['run_name'],
					'project_name' 	=> $row['project_name'],
					'hidden'		=> $row['run_hidden'],
					'species' 		=> $row['run_species'],
					'nb_sequences' 	=> $row['run_nb_sequences'],
					'full_seq_size' => $row['run_full_seq_size'],
					'project_id' 	=> $row['project_id'],
					'date'			=> $row['run_date'],
					'data_nature'	=> $row['run_data_nature'],
					'sequencer'		=> $row['run_sequencer'],
					'type'			=> $row['run_type'],
					'description' 	=> $row['run_description'],
					'data_state' 	=> $row['run_data_state'],
					'retention_date' 	=> $row['run_retention_date'],
					'purged_date' 	=> $row['run_purged_date'],
					'purged_size' 	=> $row['run_purged_size'],
					'mail_sent_date' 	=> $row['run_mail_sent_date'],
				);
			}						
		}
		return $results;
    }
    /**
     * Select all runs linked to the specified project for the user
     *
     * @param	string	$user_id	the user id
     * @param	string	$project_id	the project id
     * @return	hash table with all runs information
     */
     static function get_user_project_runs($user_id, $project_id, $orderby='', $limit='') {
    	// If the user is not logged on display demonstration project
	    if ($user_id == null) {
	    	$where = 'tx_nG6_project.uid='.$project_id.' AND tx_nG6_project.public=0 AND tx_nG6_run.hidden=0';
	    }
	    else {
	    	$where = 'fe_users.uid='.$user_id.' AND '.'tx_nG6_project.uid='.$project_id.
	    		' AND ((fe_rights.right_id<>2 AND tx_nG6_run.hidden=0) OR fe_rights.right_id=2)' ;
	    }
    	
	    // First select all runs from the database
	    $queryParts = Array(
		    'SELECT' => 'tx_nG6_run.uid AS run_id,'.
			    'tx_nG6_run.directory AS run_directory,'.
			    'tx_nG6_run.species AS run_species,'.
			    'tx_nG6_run.description AS run_description, '.
			    'tx_nG6_run.data_nature AS run_data_nature, '.
			    'tx_nG6_run.sequencer AS run_sequencer, '.
			    'tx_nG6_run.type AS run_type, '.
			    'tx_nG6_run.name AS run_name, '.
			    'tx_nG6_run.nb_sequences AS run_nb_sequences, '.
			    'tx_nG6_run.full_seq_size AS run_full_seq_size, '.
			    'tx_nG6_run.date AS run_date, '.
			    'tx_nG6_run.hidden AS run_hidden, '.
			    'tx_nG6_run.data_state AS run_data_state, '.
				'tx_nG6_run.retention_date AS run_retention_date, '.
				'tx_nG6_run.purged_date AS run_purged_date, '.
				'tx_nG6_run.purged_size AS run_purged_size, '.
				'tx_nG6_run.mail_sent_date AS run_mail_sent_date, '.
			    'tx_nG6_project.uid AS project_id, '.
			    'tx_nG6_project.name AS project_name ',
		    'FROM' => 'tx_nG6_run '.
			    'INNER JOIN tx_nG6_project_run ON tx_nG6_run.uid=tx_nG6_project_run.run_id '.
			    'INNER JOIN tx_nG6_project ON tx_nG6_project_run.project_id=tx_nG6_project.uid '.
			    'INNER JOIN fe_rights ON tx_nG6_project.uid=fe_rights.project_id '.
			    'INNER JOIN fe_users ON fe_rights.fe_user_id=fe_users.uid ',
		    'WHERE' => $where,
		    'GROUPBY' => '',
		    'ORDERBY' => $orderby,
		    'LIMIT' 	=> $limit
	    );
    
	    // Then create the result hash table
	    $results = array();
	    $res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
	    while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
	    	$run_id = $row['run_id'];
	    	if (!isset($results['run_'.$run_id])) {
			    $results['run_'.$run_id] = array(
			    'id'			=> $run_id,
			    'directory'		=> $row['run_directory'],
			    'name' 			=> $row['run_name'],
			    'project_name' 	=> $row['project_name'],
			    'hidden'		=> $row['run_hidden'],
			    'data_state' 	=> $row['run_data_state'],
				'retention_date' 	=> $row['run_retention_date'],
				'purged_date' 	=> $row['run_purged_date'],
				'purged_size' 	=> $row['run_purged_size'],
				'mail_sent_date' 	=> $row['run_mail_sent_date'],
			    'species' 		=> $row['run_species'],
			    'nb_sequences' 	=> $row['run_nb_sequences'],
			    'full_seq_size' => $row['run_full_seq_size'],
			    'project_id' 	=> $row['project_id'],
			    'date'			=> $row['run_date'],
			    'data_nature'	=> $row['run_data_nature'],
			    'sequencer'		=> $row['run_sequencer'],
			    'type'			=> $row['run_type'],
			    'description' 	=> $row['run_description']
			    				);
		    }
	    }
	    return $results;
    }

	/**
	 * Select all analysis linked to the specified project
	 *
	 * @param	string	$project_id	the project id
	 * @return	hash table with all analysis information
	 */
	static function get_project_analysis($project_id, $get_hidden=TRUE, $orderby='', $limit='') {
    	
    	// First select all analysis from the database    	
    	$queryParts = array( 
			'SELECT' => 'tx_nG6_analyze.uid AS analyze_id,'. 
						'tx_nG6_analyze.directory AS analyze_directory,'. 
						'tx_nG6_analyze.name AS analyze_name, '.
						'tx_nG6_analyze.class AS analyze_class, '.
						'tx_nG6_analyze.date AS analyze_date, '.
						'tx_nG6_analyze.software AS analyze_software, '.
						'tx_nG6_analyze.version AS analyze_version, '.
						'tx_nG6_analyze.data_state AS analyze_data_state, '.
						'tx_nG6_analyze.retention_date AS analyze_retention_date, '.
						'tx_nG6_analyze.purged_date AS analyze_purged_date, '.
						'tx_nG6_analyze.purged_size AS analyze_purged_size, '.
						'tx_nG6_analyze.mail_sent_date AS analyze_mail_sent_date, '.
						'tx_nG6_analyze.params AS analyze_params, '.
						'tx_nG6_analyze.description AS analyze_description, '.
    					'tx_nG6_analyze.is_editable AS analyze_is_editable, '.
						'tx_nG6_analyze.parent_uid AS analyze_parent_uid, '.
						'tx_nG6_analyze.hidden AS analyze_hidden ',
			'FROM' => 'tx_nG6_project '.
						'INNER JOIN tx_nG6_project_analyze ON tx_nG6_project.uid = tx_nG6_project_analyze.project_id '.
						'INNER JOIN tx_nG6_analyze ON tx_nG6_analyze.uid=tx_nG6_project_analyze.analyze_id ',
			'WHERE' => 'tx_nG6_project.uid='.$project_id,
			'GROUPBY' => '',
			'ORDERBY' => $orderby,
			'LIMIT' => $limit
		);
		
		if(!$get_hidden){
			$queryParts['WHERE'] .= ' AND tx_nG6_analyze.hidden = 0';
		}
		
		// Then create the result hash table
		$results = array();
		$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			$analyze_id = $row['analyze_id'];
			if (!isset ($results['analyse_'.$analyze_id])) {				
				$results['analyse_'.$analyze_id] = array( 
					'directory'		=> $row['analyze_directory'], 
					'name' 			=> $row['analyze_name'],
					'class'			=> $row['analyze_class'],
					'id' 			=> $row['analyze_id'],
					'hidden' 		=> $row['analyze_hidden'],
					'params' 		=> $row['analyze_params'],
					'software' 		=> $row['analyze_software'],
					'version' 		=> $row['analyze_version'],
					'data_state' 		=> $row['analyze_data_state'],
					'retention_date' 		=> $row['analyze_retention_date'],
					'purged_date' 		=> $row['analyze_purged_date'],
					'purged_size' 		=> $row['analyze_purged_size'],
					'mail_sent_date' 		=> $row['analyze_mail_sent_date'],
					'date'			=> $row['analyze_date'],
					'description' 	=> $row['analyze_description'],
					'is_editable' 	=> $row['analyze_is_editable'],
					'parent_id'		=> $row['analyze_parent_uid']	
				);
			}						
		}
		return $results;
    }

    /**
    * Select all analysis linked to the specified project for the user
    *
    * @param	string	$user_id	the user id
    * @param	string	$project_id	the project id
    * @return	hash table with all analysis information
    */
    static function get_user_project_analysis($user_id, $project_id, $orderby='', $limit='') {
    	// If the user is not logged on display demonstration project
    	if ($user_id == null) {
    		$where = 'tx_nG6_project.uid='.$project_id.' AND tx_nG6_project.public=0 AND tx_nG6_run.hidden=0';
    	}
    	else {
    		$where = 'fe_users.uid='.$user_id.' AND tx_nG6_project.uid='.$project_id.
    					' AND ((fe_rights.right_id<>2 AND tx_nG6_analyze.hidden=0) OR fe_rights.right_id=2)' ;
    	}
    	
    	// First select all analysis from the database
    	$queryParts = array(
    			'SELECT' => 'tx_nG6_analyze.uid AS analyze_id,'. 
    						'tx_nG6_analyze.directory AS analyze_directory,'. 
    						'tx_nG6_analyze.name AS analyze_name, '.
    						'tx_nG6_analyze.class AS analyze_class, '.
    						'tx_nG6_analyze.date AS analyze_date, '.
    						'tx_nG6_analyze.software AS analyze_software, '.
    						'tx_nG6_analyze.version AS analyze_version, '.
    						'tx_nG6_analyze.data_state AS analyze_data_state, '.
							'tx_nG6_analyze.retention_date AS analyze_retention_date, '.
							'tx_nG6_analyze.purged_date AS analyze_purged_date, '.
							'tx_nG6_analyze.purged_size AS analyze_purged_size, '.
							'tx_nG6_analyze.mail_sent_date AS analyze_mail_sent_date, '.
    						'tx_nG6_analyze.params AS analyze_params, '.
    						'tx_nG6_analyze.description AS analyze_description, '.
    						'tx_nG6_analyze.is_editable AS analyze_is_editable, '.
    						'tx_nG6_analyze.parent_uid AS analyze_parent_uid, '.
    						'tx_nG6_analyze.hidden AS analyze_hidden ',
    			'FROM' => 'tx_nG6_analyze '.
    						'INNER JOIN tx_nG6_project_analyze ON tx_nG6_analyze.uid = tx_nG6_project_analyze.analyze_id '.
    						'INNER JOIN tx_nG6_project ON tx_nG6_project_analyze.project_id=tx_nG6_project.uid '.
							'INNER JOIN fe_rights ON tx_nG6_project.uid=fe_rights.project_id '.
					  		'INNER JOIN fe_users ON fe_rights.fe_user_id=fe_users.uid ',
    			'WHERE' => $where,
    			'GROUPBY' => '',
    			'ORDERBY' => $orderby,
    			'LIMIT' => $limit
    	);
    	// Then create the result hash table
    	$results = array();
    	$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
    	while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
    		$analyze_id = $row['analyze_id'];
    		if (!isset ($results['analyse_'.$analyze_id])) {
    			$results['analyse_'.$analyze_id] = array(
    					'directory'		=> $row['analyze_directory'], 
    					'name' 			=> $row['analyze_name'],
    					'class'			=> $row['analyze_class'],
    					'id' 			=> $row['analyze_id'],
    					'hidden' 		=> $row['analyze_hidden'],
    					'params' 		=> $row['analyze_params'],
    					'software' 		=> $row['analyze_software'],
    					'version' 		=> $row['analyze_version'],
    					'date'			=> $row['analyze_date'],
    					'data_state' 		=> $row['analyze_data_state'],
						'retention_date' 		=> $row['analyze_retention_date'],
						'purged_date' 		=> $row['analyze_purged_date'],
						'purged_size' 		=> $row['analyze_purged_size'],
						'mail_sent_date' 		=> $row['analyze_mail_sent_date'],
    					'description' 	=> $row['analyze_description'],
    					'is_editable' 	=> $row['analyze_is_editable'],
    					'parent_id'		=> $row['analyze_parent_uid']	
    			);
    		}
    	}
    	return $results;
    }

    /**
     * Hide a project and sublevels (runs & analyzes) datas from database project
     * 
     * @param	string	$p_id	the project id to hide
     */
	static function hide_project($p_id) {
		
		// First change all project runs
		$project_runs = tx_nG6_db::get_project_runs($p_id);
		foreach($project_runs as $run_id => $run_values) {
			tx_nG6_db::hide_run($run_values['id']);
		}

		// Then all project analysis
    	$project_analysis = tx_nG6_db::get_project_analysis($p_id);
		foreach($project_analysis as $analysis_id => $analysis_values) {  
			tx_nG6_db::hide_analysis($analysis_values['id']);
		}
		tx_nG6_db::unpublish_project($p_id);
		// Finaly change the project itself
		$GLOBALS['TYPO3_DB']-> exec_UPDATEquery ('tx_nG6_project', 'uid='.$p_id, array('hidden' => '1'));
	}

    /**
     * Unhide a project and sublevels (runs & analyzes) datas
     * 
     * @param	string	$p_id	the project id to hide
     */
	static function unhide_project($p_id) {
		
		// First change all project runs
		$project_runs = tx_nG6_db::get_project_runs($p_id);
		foreach($project_runs as $run_id => $run_values) {
			tx_nG6_db::unhide_run($run_values['id']);
		}

		// Then all project analysis
    	$project_analysis = tx_nG6_db::get_project_analysis($p_id);
		foreach($project_analysis as $analysis_id => $analysis_values) {  
			tx_nG6_db::unhide_analysis($analysis_values['id']);
		}
		
		// Finaly change the project itself
		$GLOBALS['TYPO3_DB']-> exec_UPDATEquery ('tx_nG6_project', 'uid='.$p_id, array('hidden' => '0'));
	}

    /**
     * Publish the project
     * 
     * @param	string	$p_id	the project id to publish
     */
	static function publish_project($p_id) {
		// First unhide the project
		tx_nG6_db::unhide_project($p_id);
		// The set the project as visible
		$GLOBALS['TYPO3_DB']-> exec_UPDATEquery ('tx_nG6_project', 'uid='.$p_id, array('public' => '0'));
	}

    /**
     * Unpublish the project
     * 
     * @param	string	$p_id	the project id to publish
     */
	static function unpublish_project($p_id) {
		$GLOBALS['TYPO3_DB']-> exec_UPDATEquery ('tx_nG6_project', 'uid='.$p_id, array('public' => '1'));
	}

    /**
     * Delete a project and sublevels (runs & analyzes) datas from database project
     * 
     * @param string $p_id			the project id
     * @param string $user_login	the user login
     * @param string $user_pwd		the user password
     * @param string $data_folder	the data folder
     */
	static function delete_project($p_id, $user_login, $user_pwd, $data_folder) {
		
		$res = 0;
		// All runs
		$project_runs = tx_nG6_db::get_project_runs($p_id);
		foreach($project_runs as $run_id => $run_values) {
			if ($run_values['id'] != 'undefined' && $run_values['id'] != '') {
				//delete the run sublevels
				$res = tx_nG6_db::delete_run($run_values['id'], $user_login, $user_pwd, $data_folder);
				if ($res != 0) { break; }				
			}
		}
		
		// All project_analysis
		if ($res == 0) {
			$project_analysis = tx_nG6_db::get_project_analysis($p_id);
			foreach($project_analysis as $analyse_id => $analyze_values) {  
				if ($analyze_values['id'] != 'undefined' && $analyze_values['id'] != '') {
					// delete table 'analyze' entries
					$res = tx_nG6_db::delete_analysis($analyze_values['id'], $user_login, $user_pwd, $data_folder);
					if ($res != 0) { break; }
				}
			}
		}
		
		//all project comments
		$project_comments = tx_nG6_db::get_all_project_comments($p_id);
		foreach($project_comments as $id => $values ){
			tx_nG6_db::delete_project_comment($p_id, $values['id']);
		}
		
		// Delete DB project entry
		if ($res == 0) {
			$table='tx_nG6_project';
			$where='tx_nG6_project.uid='.$p_id;
			$res1 = $GLOBALS['TYPO3_DB']-> exec_DELETEquery ($table, $where);
			if ($res1 != 1) {$res = 1;}
		}
		
		// Delete right associated to the project
		if ($res == 0) {
			$table='fe_rights';
			$where='fe_rights.project_id='.$p_id;
			$res1 = $GLOBALS['TYPO3_DB']-> exec_DELETEquery ($table, $where);
			if ($res1 != 1) {$res = 1;}
		}
		
		return $res;
	}

    /**
     * Return the size of the project
     * 
     * @param	string	$p_id	the project id to return the size
	 * @param	string	$data_folder	the data folder
	 * @param	boolean $get_analyzes	get analyzes size ?
	 * @param	boolean $get_hidden	    get the hidden runs and analyzes ? 
     */
	static function get_project_size($p_id, $get_analyzes=false, $get_hidden=true) {
		
		$full_size = 0;
		// All runs
		$project_runs = tx_nG6_db::get_project_runs($p_id, $get_hidden);
		foreach($project_runs as $run_id => $run_values) {
			$full_size += tx_nG6_db::get_run_size($run_values['id'], $get_analyzes);
		}
		if ($get_analyzes) {                                        
			// All analysis
			$project_analysis = tx_nG6_db::get_project_analysis($p_id, $get_hidden);
			foreach($project_analysis as $analyse_id => $analyze_values) {  
				$full_size += tx_nG6_db::get_analysis_size($analyze_values['id']);
			}
		}
		return $full_size;
	}
	
	static function get_project_space_id($project_id){
		
		$queryParts = Array( 
			'SELECT' => ' space_id ',
			'FROM' => 'tx_nG6_project ',
			'WHERE' => 'uid = '.$project_id.' ',
			'GROUPBY' => '',
			'ORDERBY' => '',
			'LIMIT' 	=> ''
		);
		$space_id = "";
		$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			$space_id = $row['space_id'];	
		}
		return $space_id;

	}
	
	


	/*
	 * Run functions
	 *------------------------------------------------------------*/
	
	/**
     * Add a comment to the run described by run_id
     * @param unknown $run_id
     * @param unknown $cruser_id
     * @param unknown $new_comment
     */
    static function add_run_comment($run_id, $cruser_id, $new_comment){
    	return tx_nG6_db::add_comment('tx_nG6_run_comment', 'run_id', $run_id, $cruser_id, $new_comment);
    }
    
    /**
     * Retrieve all comments from a given run
     * @param unknown $run_id
     */
    static function get_all_run_comments($run_id){
    	return tx_nG6_db::get_all_comments('tx_nG6_run_comment', 'run_id', $run_id);
    }
    
    /**
     * Delete a given run comment
     * @param unknown $run_id
     * @param unknown $comment_id
     */
    static function delete_run_comment($run_id, $comment_id){
    	return tx_nG6_db::delete_comment('tx_nG6_run_comment', 'run_id', $run_id, $comment_id);
    }
	
	/**
	 * Select all run for the specified user
	 * 
	 * @param	string	$user_id	the user id
	 * @return	table with all projects
	 */
	static function select_all_user_runs($user_id, $orderby='', $limit='') {

		// If the user is not logged on display demonstration project
		if ($user_id == null) { $where = 'tx_nG6_project.public=0'; } 
		else { $where = 'fe_rights.fe_user_id='.$user_id; }
		
     	// Execute the request
		$queryParts = Array( 
			'SELECT' => 'tx_nG6_run.uid AS run_id,'.
					 	'tx_nG6_run.directory AS run_directory,'.
					 	'tx_nG6_run.species AS run_species,'.
						'tx_nG6_run.description AS run_description, '.
						'tx_nG6_run.data_nature AS run_data_nature, '. 
						'tx_nG6_run.sequencer AS run_sequencer, '.
						'tx_nG6_run.type AS run_type, '.
						'tx_nG6_run.name AS run_name, '.
						'tx_nG6_run.nb_sequences AS run_nb_sequences, '.
						'tx_nG6_run.full_seq_size AS run_full_seq_size, '.
						'tx_nG6_run.date AS run_date, '.
						'tx_nG6_run.hidden AS run_hidden, '.
						'tx_nG6_run.data_state AS run_data_state, '.
						'tx_nG6_run.retention_date AS run_retention_date, '.
						'tx_nG6_run.purged_date AS run_purged_date, '.
						'tx_nG6_run.purged_size AS run_purged_size, '.
						'tx_nG6_run.mail_sent_date AS run_mail_sent_date, '.
						'tx_nG6_project.uid AS project_id, '.
						'tx_nG6_project.name AS project_name ',
			'FROM' => 'fe_rights '.
						'INNER JOIN tx_nG6_project ON tx_nG6_project.uid=fe_rights.project_id '.
						'INNER JOIN tx_nG6_project_run ON tx_nG6_project.uid=tx_nG6_project_run.project_id '.
						'INNER JOIN tx_nG6_run ON tx_nG6_project_run.run_id=tx_nG6_run.uid ',
			'WHERE' => $where,
			'GROUPBY' => '',
			'ORDERBY' => $orderby,
			'LIMIT' 	=> $limit
		); 
		// Then create the result hash table
		$results = array();
		$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			$run_id = $row['run_id'];	
			if (!isset($results['run_'.$run_id])) {				
				$results['run_'.$run_id] = array(
					'id'			=> $run_id,  
					'directory'		=> $row['run_directory'], 
					'name' 			=> $row['run_name'],
					'project_name' 	=> $row['project_name'],
					'project_id' 	=> $row['project_id'],
					'hidden'		=> $row['run_hidden'],
					'data_state' 	=> $row['run_data_state'],
					'retention_date' 	=> $row['run_retention_date'],
					'purged_date' 	=> $row['run_purged_date'],
					'purged_size' 	=> $row['run_purged_size'],
					'mail_sent_date' 	=> $row['run_mail_sent_date'],
					'species' 		=> $row['run_species'],
					'nb_sequences' 	=> $row['run_nb_sequences'],
					'full_seq_size' => $row['run_full_seq_size'],
					'date'			=> $row['run_date'],
					'data_nature'	=> $row['run_data_nature'],
					'sequencer'		=> $row['run_sequencer'],
					'type'			=> $row['run_type'],
					'description' 	=> $row['run_description']				
				);
			}						
		}
		return $results;

	}

	/**
	 * Select all information for the specified run
	 *
	 * @param	string	$run_id	the run id
	 * @return	hash table with all the run information
	 */
	static function select_run($run_id) {
		
		// First select the run from the database
		$queryParts = Array( 
			'SELECT' => 'tx_nG6_project.uid AS project_id,'.
						'tx_nG6_project.name AS project_name,'.
						'tx_nG6_run.uid AS run_id,'.
					 	'tx_nG6_run.directory AS run_directory,'.
						'tx_nG6_run.description AS run_description, '.
						'tx_nG6_run.species AS run_species, '.
						'tx_nG6_run.name AS run_name, '.
						'tx_nG6_run.data_nature AS run_data_nature, '.
						'tx_nG6_run.sequencer AS run_sequencer, '.
						'tx_nG6_run.type AS run_type, '.
						'tx_nG6_run.full_seq_size, '.
						'tx_nG6_run.hidden AS run_hidden, '.
						'tx_nG6_run.data_state AS run_data_state, '.
						'tx_nG6_run.retention_date AS run_retention_date, '.
						'tx_nG6_run.purged_date AS run_purged_date, '.
						'tx_nG6_run.purged_size AS run_purged_size, '.
						'tx_nG6_run.mail_sent_date AS run_mail_sent_date, '.
						'tx_nG6_run.nb_sequences, '.
						'tx_nG6_run.storage_size, '.
						'tx_nG6_run.date AS run_date',
			'FROM' => 'fe_rights '.
						'INNER JOIN tx_nG6_project ON tx_nG6_project.uid=fe_rights.project_id '.
						'INNER JOIN tx_nG6_project_run ON tx_nG6_project.uid=tx_nG6_project_run.project_id '.
						'INNER JOIN tx_nG6_run ON tx_nG6_project_run.run_id=tx_nG6_run.uid ',
			'WHERE' => 'tx_nG6_run.uid='.$run_id,
			'GROUPBY' => '',
			'ORDERBY' => 'tx_nG6_run.date DESC',
			'LIMIT' 	=> ''
		); 
		// Then create the result hash table
		$result = array();
		$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {			
			$result = array( 
				'id'			=> $row['run_id'],  
				'project_id'	=> $row['project_id'], 
				'project_name'	=> $row['project_name'], 
				'directory'		=> $row['run_directory'], 
				'name' 			=> $row['run_name'],
				'hidden'		=> $row['run_hidden'],
				'data_state' 	=> $row['run_data_state'],
				'retention_date' 	=> $row['run_retention_date'],
				'purged_date' 	=> $row['run_purged_date'],
				'purged_size' 	=> $row['run_purged_size'],
				'mail_sent_date' 	=> $row['run_mail_sent_date'],
				'species' 		=> $row['run_species'],
				'nb_sequences' 	=> $row['nb_sequences'],
				'storage_size' 	=> intval($row['storage_size']),
				'full_seq_size' => $row['full_seq_size'],
				'date'			=> $row['run_date'],
				'data_nature' 	=> $row['run_data_nature'],
				'sequencer' 	=> $row['run_sequencer'],
				'type' 			=> $row['run_type'],
				'description' 	=> $row['run_description']				
			);					
		}
		return $result;
    }

	/**
	 * Select all mids linked to the specified run
	 *
	 * @param	string	$run_id	the run id
	 * @return	hash table with all mids description
	 */
	static function select_mid_descriptions($run_id) {
		
		// First select all analysis from the database
		$queryParts = array( 
			'SELECT' => 'sample_id, name ',
			'FROM' => 'tx_nG6_sample ',
			'WHERE' => 'run_id='.$run_id,
			'GROUPBY' => '',
			'ORDERBY' => '',
			'LIMIT' => ''
		);
		
		$result = array();
		$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			$result[$row["sample_id"]] = $row["name"];
		}
		return $result;
		
	}	

	/**
	 * Select all analysis linked to the specified run
	 *
	 * @param	string	$run_id	the run id
	 * @return	hash table with all analysis information
	 */
    static function get_run_analysis($run_id, $orderby='', $limit='') {
		// First select all analysis from the database
		$queryParts = array( 
			'SELECT' => 'tx_nG6_analyze.uid AS analyze_id,'. 
						'tx_nG6_analyze.directory AS analyze_directory,'. 
						'tx_nG6_analyze.name AS analyze_name, '.
						'tx_nG6_analyze.params AS analyze_params, '.
						'tx_nG6_analyze.class AS analyze_class, '.
						'tx_nG6_analyze.date AS analyze_date, '.
						'tx_nG6_analyze.software AS analyze_software, '.
						'tx_nG6_analyze.version AS analyze_version, '.
						'tx_nG6_analyze.data_state AS analyze_data_state, '.
						'tx_nG6_analyze.retention_date AS analyze_retention_date, '.
						'tx_nG6_analyze.purged_date AS analyze_purged_date, '.
						'tx_nG6_analyze.purged_size AS analyze_purged_size, '.
						'tx_nG6_analyze.mail_sent_date AS analyze_mail_sent_date, '.
						'tx_nG6_analyze.hidden AS analyze_hidden, '.
						'tx_nG6_analyze.description AS analyze_description, '.
						'tx_nG6_analyze.storage_size AS analyze_storage_size, '.
						'tx_nG6_analyze.is_editable AS analyze_is_editable, '.
						'tx_nG6_analyze.parent_uid AS analyze_parent_uid ',
			'FROM' => 'tx_nG6_run_analyze '.
						'INNER JOIN tx_nG6_analyze ON tx_nG6_analyze.uid = tx_nG6_run_analyze.analyze_id ',
			'WHERE' => 'tx_nG6_run_analyze.run_id='.$run_id,
			'GROUPBY' => '',
			'ORDERBY' => $orderby,
			'LIMIT' => $limit
		);
		// Then create the result hash table
		$results = array();
		$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			$analyze_id = $row['analyze_id'];
			if (!isset ($results['analyse_'.$analyze_id])) {				
				$results['analyse_'.$analyze_id] = array( 
					'directory'		=> $row['analyze_directory'], 
					'name' 			=> $row['analyze_name'],
					'params' 		=> $row['analyze_params'],
					'class' 		=> $row['analyze_class'],
					'id' 			=> $row['analyze_id'],
					'hidden' 		=> $row['analyze_hidden'],
					'software' 		=> $row['analyze_software'],
					'version' 		=> $row['analyze_version'],
					'data_state' 		=> $row['analyze_data_state'],
					'retention_date' 		=> $row['analyze_retention_date'],
					'purged_date' 		=> $row['analyze_purged_date'],
					'purged_size' 		=> $row['analyze_purged_size'],
					'mail_sent_date' 		=> $row['analyze_mail_sent_date'],
					'date'			=> $row['analyze_date'],
					'description' 	=> $row['analyze_description'],
					'storage_size' 	=> $row['analyze_storage_size'],
					'is_editable' 	=> $row['analyze_is_editable'],
					'parent_id'		=> $row['analyze_parent_uid']
				);
			}						
		}
		return $results;
    }

    /**
    * Select all analysis linked to the specified run for the user
    *
    * @param	string	$user_id	the user id
    * @param	string	$run_id	the run id
    * @return	hash table with all analysis information
    */
    static function get_user_run_analysis($user_id, $run_id, $orderby='', $limit='') {
    	// If the user is not logged on display demonstration project
    	if ($user_id == null) {
    		$where = 'tx_nG6_run_analyze.run_id='.$run_id.' AND tx_nG6_project.public=0 AND tx_nG6_analyze.hidden=0' ;
    	}
    	else {
    		$where = 'fe_users.uid='.$user_id.' AND '.'tx_nG6_run_analyze.run_id='.$run_id.
    						' AND ((fe_rights.right_id<>2 AND tx_nG6_analyze.hidden=0) OR fe_rights.right_id=2)' ;
    	}
    	
    	// First select all analysis from the database
    	$queryParts = array(
    			'SELECT' => 'tx_nG6_analyze.uid AS analyze_id,'. 
    						'tx_nG6_analyze.directory AS analyze_directory,'. 
    						'tx_nG6_analyze.name AS analyze_name, '.
    						'tx_nG6_analyze.params AS analyze_params, '.
    						'tx_nG6_analyze.class AS analyze_class, '.
    						'tx_nG6_analyze.date AS analyze_date, '.
    						'tx_nG6_analyze.software AS analyze_software, '.
    						'tx_nG6_analyze.version AS analyze_version, '.
    						'tx_nG6_analyze.hidden AS analyze_hidden, '.
    						'tx_nG6_analyze.data_state AS analyze_data_state, '.
							'tx_nG6_analyze.retention_date AS analyze_retention_date, '.
							'tx_nG6_analyze.purged_date AS analyze_purged_date, '.
							'tx_nG6_analyze.purged_size AS analyze_purged_size, '.
							'tx_nG6_analyze.mail_sent_date AS analyze_mail_sent_date, '.
    						'tx_nG6_analyze.description AS analyze_description, '.
    						'tx_nG6_analyze.is_editable AS analyze_is_editable, '.
    						'tx_nG6_analyze.parent_uid AS analyze_parent_uid ',
    			'FROM' => 'tx_nG6_analyze '.
							'INNER JOIN tx_nG6_run_analyze ON tx_nG6_analyze.uid = tx_nG6_run_analyze.analyze_id '.
							'INNER JOIN tx_nG6_run ON tx_nG6_run_analyze.run_id = tx_nG6_run.uid '.
							'INNER JOIN tx_nG6_project_run ON tx_nG6_run.uid = tx_nG6_project_run.run_id '.
							'INNER JOIN tx_nG6_project ON tx_nG6_project_run.project_id = tx_nG6_project.uid '. 
							'INNER JOIN fe_rights ON tx_nG6_project.uid=fe_rights.project_id '.
							'INNER JOIN fe_users ON fe_rights.fe_user_id=fe_users.uid ',
    			'WHERE' => $where,
    			'GROUPBY' => '',
    			'ORDERBY' => $orderby,
    			'LIMIT' => $limit
    	);
    	// Then create the result hash table
    	$results = array();
    	$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
    	while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
    		$analyze_id = $row['analyze_id'];
    		if (!isset ($results['analyse_'.$analyze_id])) {
    			$results['analyse_'.$analyze_id] = array(
    					'directory'		=> $row['analyze_directory'], 
    					'name' 			=> $row['analyze_name'],
    					'params' 		=> $row['analyze_params'],
    					'class' 		=> $row['analyze_class'],
    					'id' 			=> $row['analyze_id'],
    					'hidden' 		=> $row['analyze_hidden'],
    					'software' 		=> $row['analyze_software'],
    					'version' 		=> $row['analyze_version'],
    					'data_state' 		=> $row['analyze_data_state'],
						'retention_date' 		=> $row['analyze_retention_date'],
						'purged_date' 		=> $row['analyze_purged_date'],
						'purged_size' 		=> $row['analyze_purged_size'],
						'mail_sent_date' 		=> $row['analyze_mail_sent_date'],
    					'date'			=> $row['analyze_date'],
    					'description' 	=> $row['analyze_description'],
    					'is_editable' 	=> $row['analyze_is_editable'],
    					'parent_id'		=> $row['analyze_parent_uid']
    			);
    		}
    	}
    	return $results;
    }

	/**
	 * Delete a run, sample and sublevel (analyzes) datas from database project
	 * 
	 * @param string $r_id			the run id
	 * @param string $user_login	the user login
	 * @param string $user_pwd		the user password
	 * @param string $data_folder	the data folder
	 */
	static function delete_run($r_id, $user_login, $user_pwd, $data_folder) {		

		$res = 0;		
		$run_analysis = tx_nG6_db::get_run_analysis($r_id);
		foreach($run_analysis as $analyse_id => $analyze_values) {
			if ($analyze_values['id'] != 'undefined' && $analyze_values['id'] != '') {				
				// delete table 'analyze' entries
				$res = tx_nG6_db::delete_analysis($analyze_values['id'], $user_login, $user_pwd, $data_folder);
				if ($res != 0) { break; }
			}
		}
		
		// First select the run
		$my_run = tx_nG6_db::select_run($r_id);
		
		// delete run
		if ($res == 0) {
			$table='tx_nG6_project_run';
			$where='run_id='.$r_id;
			$GLOBALS['TYPO3_DB']-> exec_DELETEquery ($table, $where);
			
			$table='tx_nG6_run';
			$where='tx_nG6_run.uid='.$r_id;
			$res1 = $GLOBALS['TYPO3_DB']-> exec_DELETEquery ($table, $where);
			if ($res1 != 1) {$res = 1;}
		}
			
		// delete run samples
		if ($res == 0) {
			$table='tx_nG6_sample';
			$where='tx_nG6_sample.run_id='.$r_id;
			$GLOBALS['TYPO3_DB']-> exec_DELETEquery ($table, $where);
		}
		
		if ($res == 0) {
			// Delete the run directory
			$res = tx_nG6_utils::delete_directory($user_login, $user_pwd, $data_folder.$my_run['directory']);
		}
		
		// delete all run comments
		$run_comments = tx_nG6_db::get_all_run_comments($r_id);
		foreach ($run_comments as $id => $values){
			tx_nG6_db::delete_run_comment($r_id, $values['id']);
		}
		
		return $res;
	}

    /**
     * Return the size of the run
     * 
     * @param	string	$r_id	the run id to return the size
	 * @param	string	$data_folder	the data folder
	 * @param	boolean $get_analyzes	get analyzes size ? 
     */
	static function get_run_size($r_id, $get_analyzes=false) {
	    $full_size = 0;
		if($get_analyzes){
			$run_analysis = tx_nG6_db::get_run_analysis($r_id);
			foreach($run_analysis as $analyse_id => $analyze_values) {
				$full_size += tx_nG6_db::get_analysis_size($analyze_values['id']);
			}
		}
		// Then select the run
		$my_run = tx_nG6_db::select_run($r_id);
		$full_size += $my_run['storage_size'];
			
		return $full_size;
	}

    /**
     * Hide a run and sublevels (analysis)
     * 
     * @param	string	$r_id	the run id to hide
     */
	static function hide_run($r_id) {

		// First select all run analysis
//     	$run_analysis = tx_nG6_db::get_run_analysis($r_id);
// 		foreach($run_analysis as $analysis_id => $analysis_values) {  
// 			tx_nG6_db::hide_analysis($analysis_values['id']);
// 		}
		
		// Finaly change the project itself
		$GLOBALS['TYPO3_DB']-> exec_UPDATEquery ('tx_nG6_run', 'uid='.$r_id, array('hidden' => '1'));
	}

    /**
     * Unhide a run and sublevels (analysis)
     * 
     * @param	string	$r_id	the run id to hide
     */
	static function unhide_run($r_id, $unhide_up=false) {
	
		if ($unhide_up) {
			// Unhide the project
			$GLOBALS['TYPO3_DB']-> exec_UPDATEquery (
				'tx_nG6_project, tx_nG6_project_run',
				'tx_nG6_project.uid = tx_nG6_project_run.project_id AND tx_nG6_project_run.run_id = '.$r_id ,
				array('tx_nG6_project.hidden' => '0')
			);
		}

		// Then all run analysis
//     	$run_analysis = tx_nG6_db::get_run_analysis($r_id);
// 		foreach($run_analysis as $analysis_id => $analysis_values) {  
// 			tx_nG6_db::unhide_analysis($analysis_values['id']);
// 		}
		
		// Finaly change the run itself
		$GLOBALS['TYPO3_DB']-> exec_UPDATEquery ('tx_nG6_run', 'uid='.$r_id, array('hidden' => '0'));
	}

    /**
     * Get the run min date
     * 
     * @return int timestamp
     */
    static function get_run_min_date(){
    	$queryParts = Array(
    		'SELECT' 	=> 'MIN(tx_nG6_run.date) as min_date ',
    		'FROM' 		=> 'tx_nG6_run',
    		'WHERE' 	=> 'tx_nG6_run.date!=0',
    		'GROUPBY' 	=> '',
    		'ORDERBY' 	=> '',
    		'LIMIT' 	=> ''
    	);
    	$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
   		$min_date = 0;
    	if($GLOBALS['TYPO3_DB']->sql_num_rows($res) == 1) {
    		$tab = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
    		$min_date = intval($tab['min_date']);
    	}else{
    		$min_date = -1;
    	}
    	return $min_date;
    }
    
    /**
    * Get the run max date
    *
    * @return int timestamp
    */
    static function get_run_max_date(){
    	$queryParts = Array(
        		'SELECT' 	=> 'MAX(tx_nG6_run.date) as max_date ',
        		'FROM' 		=> 'tx_nG6_run',
        		'WHERE' 	=> 'tx_nG6_run.date!=0',
        		'GROUPBY' 	=> '',
        		'ORDERBY' 	=> '',
        		'LIMIT' 	=> ''
    	);
    	$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
    	$max_date = 0;
    	if($GLOBALS['TYPO3_DB']->sql_num_rows($res) == 1) {
    		$tab = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
    		$max_date = intval($tab['max_date']);
    	}else{
    		$max_date = -1;
    	}
    	return $max_date;
    }
    
    
    /**
	 * Purge an run from database project
	 * 
	 * @param array $a_id			the analyze ids
	 * @param string $user_login	the user login
	 * @param string $user_pwd		the user password
	 * @param string $data_folder	the data folder
	 * @return	0=>everything ok, 1=>user right problem, 2=>wrong authentification, 3=>connection error
	 */
	static function purge_run($r_id, $user_login, $user_pwd, $data_folder) {

		$res = 0;
		// First select the analyse
		$my_run = tx_nG6_db::select_run($r_id);
		
		if (! array_key_exists('storage_size',$my_run) ){
		    //run does not exist in the database
		    return 0;
		}
                $storage_size = $my_run['storage_size'];
                $datas = array(
    			'purged_date'	=>	time(),
    			'data_state'	=>	"purged",
    			'purged_size'	=>	$storage_size,
    			'storage_size'	=>	0
   			
                );
                
                $res = tx_nG6_utils::purge_directory($user_login, $user_pwd, $data_folder.$my_run['directory']);
                if ($res == 0 ){ //delete file ok
                        if ( $my_run['data_state'] != "purged"){
                                $res_query = $GLOBALS['TYPO3_DB']->exec_UPDATEquery('tx_nG6_run', 'uid='.$r_id, $datas );
                                if ($res_query == False){
                                        $res = 5;
                                }
                        }
                }else { //error delete file 
                        $res = 4;
                }
                return $res;
	}
	
	/**
	 * Returns a run's retention status and date
	 * 
	 * @param string  $r_id			    the analyze ids
	 * @return['run_data_state']	    purged=> the date is purged, stored=> the data is stored, extended=>retention extended and data still stored
	 * @return['run_retention_date']	the unix epoch timestamp
	 */
	static function select_run_retention_status($r_id) {

		// First select the run from the database
		$queryParts = Array( 
			'SELECT' => 'tx_nG6_run.data_state AS run_data_state, '. 
    					'tx_nG6_run.retention_date AS run_retention_date, '.
    					'tx_nG6_run.purged_date AS run_purged_date ',
			'FROM' => 'tx_nG6_run ',
			'WHERE' => 'tx_nG6_run.uid='.$r_id,
			'GROUPBY' => '',
			'ORDERBY' => '',
			'LIMIT' 	=> ''
		); 
		// Then create the result hash table
		$result = array();
		$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {			
			$result['run_data_state'] = $row['run_data_state'];
			$result['run_retention_date'] = tx_nG6_utils::convert_epoch_timestamp_to_nice_date($row['run_retention_date']);
			$result['run_purge_date'] = tx_nG6_utils::convert_epoch_timestamp_to_nice_date($row['run_purged_date']);
			//$result['run_purge_date'] = $row['run_purged_date'];
		}
		return $result;
	}

    
    
	/*
	 * Analysis functions
	 *------------------------------------------------------------*/
	 
    /**
     * Add a comment to the analysis described by $analyze_id
     * @param unknown $analyze_id
     * @param unknown $cruser_id
     * @param unknown $new_comment
     */
    static function add_analyze_comment($analyze_id, $cruser_id, $new_comment){
    	return tx_nG6_db::add_comment('tx_nG6_analyze_comment', 'analyze_id', $analyze_id, $cruser_id, $new_comment);
    }
    
    /**
     * Retrieve all comments from a given analysis
     * @param unknown $run_id
     */
    static function get_all_analyze_comments($analyze_id){
    	return tx_nG6_db::get_all_comments('tx_nG6_analyze_comment', 'analyze_id', $analyze_id);
    }
    
    /**
     * Delete a given analyze comment
     * @param unknown $run_id
     * @param unknown $comment_id
     */
    static function delete_analyze_comment($analyze_id, $comment_id){
    	return tx_nG6_db::delete_comment('tx_nG6_analyze_comment', 'analyze_id', $analyze_id, $comment_id);
    }
    
    
    
	/**
	 * Select all information on an analyse
	 *
	 * @param	string	$analyse_id	the analyse id
	 * @return	hash table with all analyse information
	 */
    static function select_analyse($analyse_id) {

		// First select all analyse information
		$queryParts = array( 
			'SELECT' => 'tx_nG6_project.uid AS project_id,'. 
						'tx_nG6_project.name AS project_name,'. 
						'tx_nG6_analyze.uid AS analyze_id,'.
						'tx_nG6_analyze.hidden AS analyze_hidden,'.
						'tx_nG6_analyze.directory AS analyze_directory,'. 
						'tx_nG6_analyze.name AS analyze_name, '.
						'tx_nG6_analyze.params AS analyze_params, '.
						'tx_nG6_analyze.software AS analyze_software, '.
						'tx_nG6_analyze.class AS analyze_class, '.
						'tx_nG6_analyze.date AS analyze_date, '.
						'tx_nG6_analyze.data_state AS analyze_data_state, '.
						'tx_nG6_analyze.retention_date AS analyze_retention_date, '.
						'tx_nG6_analyze.purged_date AS analyze_purged_date, '.
						'tx_nG6_analyze.purged_size AS analyze_purged_size, '.
						'tx_nG6_analyze.mail_sent_date AS analyze_mail_sent_date, '.
						'tx_nG6_analyze.description AS analyze_description, '.
						'tx_nG6_analyze.version AS analyze_version, '.
						'tx_nG6_analyze.is_editable AS analyze_is_editable, '.
						'tx_nG6_analyze.storage_size AS analyze_storage_size, '.
						'tx_nG6_analyze.parent_uid AS analyze_parent_uid ',
			'FROM' => 'tx_nG6_project '.
						'INNER JOIN tx_nG6_project_analyze ON tx_nG6_project.uid = tx_nG6_project_analyze.project_id '.
						'INNER JOIN tx_nG6_analyze ON tx_nG6_analyze.uid=tx_nG6_project_analyze.analyze_id ',
			'WHERE' => 'tx_nG6_analyze.uid='.$analyse_id,
			'GROUPBY' => '',
			'ORDERBY' => '',
			'LIMIT' => ''
		);
		
		$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
		
		// If it's a project analyse, add project information
		if ($GLOBALS['TYPO3_DB']->sql_num_rows($res) > 0) {
			$result = array();
			while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {	
				$result = array( 
					'project_name'	=> $row['project_name'], 
					'project_id'	=> $row['project_id'],
					'run_name'		=> 'None', 
					'run_id'		=> 'None',					 
					'directory'		=> $row['analyze_directory'],
					'hidden'		=> $row['analyze_hidden'],
					'name' 			=> $row['analyze_name'],
					'class'			=> $row['analyze_class'],
					'params'		=> $row['analyze_params'],
					'date'			=> $row['analyze_date'],
					'data_state' 		=> $row['analyze_data_state'],
					'retention_date' 		=> $row['analyze_retention_date'],
					'purged_date' 		=> $row['analyze_purged_date'],
					'purged_size' 		=> $row['analyze_purged_size'],
					'mail_sent_date' 		=> $row['analyze_mail_sent_date'],
					'description' 	=> $row['analyze_description'],
					'parent_id'		=> $row['analyze_parent_uid'],
					'software'		=> $row['analyze_software'],
					'storage_size'	=> intval($row['analyze_storage_size']),
					'version'		=> $row['analyze_version'],
					'is_editable'   => $row['analyze_is_editable']
				);				
			}
			return $result;
		} else { // If it's a run analyse, add run information
			$queryParts = array( 
				'SELECT' => 'tx_nG6_project.name AS project_name,'.
							'tx_nG6_project.uid AS project_id,'.
							'tx_nG6_run.uid AS run_id,'. 
							'tx_nG6_run.name AS run_name,'.
							'tx_nG6_analyze.hidden AS analyze_hidden,'.
							'tx_nG6_analyze.uid AS analyze_id,'. 
							'tx_nG6_analyze.directory AS analyze_directory,'. 
							'tx_nG6_analyze.name AS analyze_name, '.
							'tx_nG6_analyze.params AS analyze_params, '.
							'tx_nG6_analyze.class AS analyze_class, '.
							'tx_nG6_analyze.date AS analyze_date, '.
							'tx_nG6_analyze.software AS analyze_software, '.
							'tx_nG6_analyze.version AS analyze_version, '.
							'tx_nG6_analyze.data_state AS analyze_data_state, '.
							'tx_nG6_analyze.retention_date AS analyze_retention_date, '.
							'tx_nG6_analyze.purged_date AS analyze_purged_date, '.
							'tx_nG6_analyze.purged_size AS analyze_purged_size, '.
							'tx_nG6_analyze.mail_sent_date AS analyze_mail_sent_date, '.
							'tx_nG6_analyze.description AS analyze_description,'.
							'tx_nG6_analyze.is_editable AS analyze_is_editable,'.
							'tx_nG6_analyze.storage_size AS analyze_storage_size, '.
							'tx_nG6_analyze.parent_uid AS analyze_parent_uid ',
				'FROM' => 'tx_nG6_project '.
							'INNER JOIN tx_nG6_project_run ON tx_nG6_project_run.project_id = tx_nG6_project.uid '.
							'INNER JOIN tx_nG6_run ON tx_nG6_run.uid = tx_nG6_project_run.run_id '.
							'INNER JOIN tx_nG6_run_analyze ON tx_nG6_run_analyze.run_id = tx_nG6_run.uid '.
							'INNER JOIN tx_nG6_analyze ON tx_nG6_analyze.uid = tx_nG6_run_analyze.analyze_id ',
				'WHERE' => 'tx_nG6_analyze.uid='.$analyse_id,
				'GROUPBY' => '',
				'ORDERBY' => 'tx_nG6_analyze.name',
				'LIMIT' => ''
			);
			$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
			$result = array();
			while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {	
				$result = array( 
					'project_name'	=> $row['project_name'], 
					'project_id'	=> $row['project_id'],
					'run_name'		=> $row['run_name'], 
					'run_id'		=> $row['run_id'], 			 
					'directory'		=> $row['analyze_directory'], 
					'hidden'		=> $row['analyze_hidden'],
					'name' 			=> $row['analyze_name'],
					'params'		=> $row['analyze_params'],
					'class' 		=> $row['analyze_class'],
					'date'			=> $row['analyze_date'],
					'data_state' 		=> $row['analyze_data_state'],
					'retention_date' 		=> $row['analyze_retention_date'],
					'purged_date' 		=> $row['analyze_purged_date'],
					'purged_size' 		=> $row['analyze_purged_size'],
					'mail_sent_date' 		=> $row['analyze_mail_sent_date'],
					'description' 	=> $row['analyze_description'],
					'software'		=> $row['analyze_software'],
					'parent_id'		=> $row['analyze_parent_uid'],
					'version'		=> $row['analyze_version'], 
					'storage_size'	=> intval($row['analyze_storage_size']),
					'is_editable'	=> $row['analyze_is_editable']
				);				
			}
			return $result;
		}
    }

	/**
	 * Select all results of an analyse
	 *
	 * @param	string	$analyse_id	the analyse id
	 * @return	hash table with all analyse results
	 */
    static function select_analyse_results($analyse_id) {

		// First select all analyse information
		$queryParts = array( 
			'SELECT' => 'uid,file,rkey,rvalue,rgroup ',
			'FROM' => 'tx_nG6_result ',
			'WHERE' => 'analyze_id='.$analyse_id,
			'GROUPBY' => '',
			'ORDERBY' => '',
			'LIMIT' => ''
		);
		
		$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
		$result = array();
		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			if (array_key_exists($row['file'], $result)) {
				if (array_key_exists($row['rgroup'], $result[$row['file']])) {
					$result[$row['file']][$row['rgroup']][$row['rkey']] = $row['rvalue'];
				} else {
					$result[$row['file']][$row['rgroup']] = array($row['rkey'] => $row['rvalue']);
				}
			} else {
				$result[$row['file']] = array($row['rgroup'] => array($row['rkey'] => $row['rvalue']));
			}			
		}
		return $result;
    }

    /**
     * Return the size of the analysis
     * 
     * @param	string	$a_id	the analysis id to return the size
	 * @param	string	$data_folder	the data folder
     */
	static function get_analysis_size($a_id) {
		// First select the analyse
		$my_analysis = tx_nG6_db::select_analyse($a_id);
		return $my_analysis['storage_size'];
	}

    /**
     * Hide an analysis
     * 
     * @param	string	$a_id	the analysis id to hide
     */
	static function hide_analysis($a_id) {
		// Change the analysis itself
		$GLOBALS['TYPO3_DB']-> exec_UPDATEquery ('tx_nG6_analyze', 'uid='.$a_id, array('hidden' => '1'));
	}

    /**
     * Unhide an analysis
     * 
     * @param	string	$a_id	the analysis id to hide
     */
	static function unhide_analysis($a_id, $unhide_up = false) {
		if ($unhide_up) {		    
			$queryParts = array(
				'SELECT'  => 'tx_nG6_project_analyze.uid',
				'FROM'    => 'tx_nG6_project_analyze',
				'WHERE'   => 'tx_nG6_project_analyze.analyze_id = '.$a_id,
				'GROUPBY' => '',
				'ORDERBY' => '',
				'LIMIT'   => ''
			);
			$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts) ;
			$is_project_analyze = $GLOBALS['TYPO3_DB']->sql_num_rows( $res ) ;
			
			// If the analyse isn't link to run
			if( $is_project_analyze ) 
			{
				// unhide the project
				$GLOBALS['TYPO3_DB']-> exec_UPDATEquery (
					'tx_nG6_project, tx_nG6_project_analyze',
					'tx_nG6_project.uid = tx_nG6_project_analyze.project_id AND tx_nG6_project_analyze.analyze_id = '.$a_id ,
					array('tx_nG6_project.hidden' => '0')
				) ;
			}
			// If the analyse is link to run
			else
			{
				// unhide the project and the run
				$GLOBALS['TYPO3_DB']->exec_UPDATEquery (
					'tx_nG6_project, tx_nG6_project_run, tx_nG6_run, tx_nG6_run_analyze', 
					'tx_nG6_project.uid = tx_nG6_project_run.project_id '.
						'AND tx_nG6_project_run.run_id = tx_nG6_run.uid '.
						'AND tx_nG6_run.uid = tx_nG6_run_analyze.run_id '.
						'AND tx_nG6_run_analyze.analyze_id = '.$a_id ,
					array('tx_nG6_project.hidden' => '0', 'tx_nG6_run.hidden' => '0')
				) ;
			}
		}

		// Change the analysis itself
		$GLOBALS['TYPO3_DB']-> exec_UPDATEquery ('tx_nG6_analyze', 'uid='.$a_id, array('hidden' => '0'));
	}

	/**
	 * Delete an analyze and results from database project
	 * 
	 * @param string $a_id			the analyze id
	 * @param string $user_login	the user login
	 * @param string $user_pwd		the user password
	 * @param string $data_folder	the data folder
	 * @return	0=>everything ok, 1=>user right problem, 2=>wrong authentification, 3=>connection error
	 */
	static function delete_analysis($a_id, $user_login, $user_pwd, $data_folder) {

		$res = 0;

		// First select the analyse
		$my_analysis = tx_nG6_db::select_analyse($a_id);
		
		// Delete the analysis results from the database
		$table='tx_nG6_result';
		$where='tx_nG6_result.analyze_id='.$a_id;
		$GLOBALS['TYPO3_DB']-> exec_DELETEquery ($table, $where);

		// Intermediate tables
		$table='tx_nG6_project_analyze';
		$where='analyze_id='.$a_id;
		$GLOBALS['TYPO3_DB']-> exec_DELETEquery ($table, $where);
		$table='tx_nG6_run_analyze';
		$where='analyze_id='.$a_id;
		$GLOBALS['TYPO3_DB']-> exec_DELETEquery ($table, $where);
		
		// Finaly the analysis itself
		$table='tx_nG6_analyze';
		$where='tx_nG6_analyze.uid='.$a_id;
		$res1 = $GLOBALS['TYPO3_DB']-> exec_DELETEquery ($table, $where);
		if ($res1 != 1) { $res = 1; } 
		
		if ($res == 0) {
			// Delete the analyse directory
			$res = tx_nG6_utils::delete_directory($user_login, $user_pwd, $data_folder.$my_analysis['directory']);
		}
		
		// delete all analyze comments
		$analyzes_comments = tx_nG6_db::get_all_analyze_comments($a_id) ;
		foreach ($analyzes_comments as $id => $values){
			tx_nG6_db::delete_analyze_comment($a_id, $values['id']);
		}
		return $res;
	}
        
        
	/**
	 * Purge an analyze and results from database project
	 * 
	 * @param array $a_id			the analyze ids
	 * @param string $user_login	the user login
	 * @param string $user_pwd		the user password
	 * @param string $data_folder	the data folder
	 * @return	0=>everything ok, 1=>user right problem, 2=>wrong authentification, 3=>connection error
	 */
    
	static function purge_analysis($a_id, $user_login, $user_pwd, $data_folder) {

		$res = 0;
                
		// First select the analyse
		$my_analysis = tx_nG6_db::select_analyse($a_id);
		if (! array_key_exists('storage_size',$my_analysis) ){
		    //analysis does not exist in the database
		    return 0;
		}
                $storage_size = $my_analysis['storage_size'];
                $datas = array(
    			'purged_date'	=>	time(),
    			'data_state'	=>	"purged",
    			'purged_size'	=>	$storage_size,
    			'storage_size'	=>	0,
                );
                $res = tx_nG6_utils::purge_directory($user_login, $user_pwd, $data_folder.$my_analysis['directory']);
                if ($res == 0 ){ //delete file ok
                        if ( $my_analysis['data_state'] != "purged"){
                                $res_query = $GLOBALS['TYPO3_DB']->exec_UPDATEquery('tx_nG6_analyze', 'uid='.$a_id, $datas );
                                if ($res_query == False){
                                        $res = 5;
                                }
                        }
                }else { //error delete file 
                        $res = 4;
                }
                return $res;
	}

	/**
	 * Returns an analysis's retention status and date
	 * 
	 * @param string  $a_id				  the analysis id
	 * @return['analysis_data_state']	  purged=> the date is purged, stored=> the data is stored
	 * @return['analysis_retention_date'] the retention unix epoch timestamp converted to a nice date
	 * @return['analysis_purged_date']    the purge unix epoch timestamp converted to a nice date
	 */
	static function select_analysis_retention_status($a_id) {

		// First select the analysis from the database
		$queryParts = Array( 
			'SELECT' => 'tx_nG6_analyze.data_state AS analysis_data_state, '. 
    					'tx_nG6_analyze.retention_date AS analysis_retention_date, '.
    					'tx_nG6_analyze.purged_date AS analysis_purged_date ',
			'FROM' => 'tx_nG6_analyze ',
			'WHERE' => 'tx_nG6_analyze.uid='.$a_id,
			'GROUPBY' => '',
			'ORDERBY' => '',
			'LIMIT' 	=> ''
		); 
		// Then create the result hash table
		$result = array();
		$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {			
			$result['analysis_data_state'] = $row['analysis_data_state'];
			$result['analysis_retention_date'] = tx_nG6_utils::convert_epoch_timestamp_to_nice_date($row['analysis_retention_date']);
			$result['analysis_purge_date'] = tx_nG6_utils::convert_epoch_timestamp_to_nice_date($row['analysis_purged_date']);
		}
		return $result;
	}


	/*
	 * User functions
	 *------------------------------------------------------------*/
	
	/**
	 * Return if the user is authorized or not to access the specified project and run
	 *
	 * @param	int	$user_id	the user id
	 * @param	string	$project_id	the project id
	 * @param	string	$project_id	the run id
	 * @return	boolean
	 */
	static function user_is_authorized ($user_id, $project_id, $run_id) {
		$authorized = false;
		
		// public project ?
		if ($project_id) {
			$queryParts = array(
				'SELECT' => 'public',
				'FROM' => 'tx_nG6_project',
				'WHERE' => 'uid='.$project_id,
				'GROUPBY' => '',
				'ORDERBY' => '',
				'LIMIT' => ''
			);
			$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
			$val = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
			$authorized = ($val['public'] == 0);
		} elseif($run_id) {
			$queryParts = array(
				'SELECT' => 'public',
				'FROM' => 'tx_nG6_project INNER JOIN tx_nG6_project_run ON tx_nG6_project.uid=tx_nG6_project_run.project_id',
				'WHERE' => 'tx_nG6_project_run.run_id='.$run_id,
				'GROUPBY' => '',
				'ORDERBY' => '',
				'LIMIT' => ''
			);
			$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
			$val = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
			$authorized = ($val['public'] == 0);
		}
		
		// not a public project
		if(!$authorized){
		
			// authorized in project ?
			if ($project_id) {			
				$queryParts = array(
					'SELECT' => 'fe_rights.right_id ',
					'FROM' => 'fe_rights INNER JOIN tx_nG6_project ON tx_nG6_project.uid=fe_rights.project_id ',
					'WHERE' => 'fe_rights.fe_user_id='.$user_id.' AND tx_nG6_project.uid='.$project_id,
					'GROUPBY' => '',
					'ORDERBY' => '',
					'LIMIT' => ''
				);
				$res1 = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
				$val = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res1);
				
				if(isset($val['right_id'])){
					$authorized = true;
				}
				
			// authorized in run ?
			} elseif ($run_id) {
				$queryParts = array(
					'SELECT' => 'fe_rights.right_id ',
					'FROM' => 'fe_rights '.
							'INNER JOIN tx_nG6_project ON tx_nG6_project.uid=fe_rights.project_id '.
							'INNER JOIN tx_nG6_project_run ON tx_nG6_project.uid=tx_nG6_project_run.project_id ',
					'WHERE' => 'fe_rights.fe_user_id='.$user_id.' AND tx_nG6_project_run.run_id='.$run_id,
					'GROUPBY' => '',
					'ORDERBY' => '',
					'LIMIT' => ''
				);
				$res1 = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
				$val = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res1);
				
				if(isset($val['right_id'])){
					$authorized = true;
				}
				
			} 
		}
		return $authorized;
	}

	/**
	 * Return true if the user is a ng6 administrator (belongs to ng6_admin group)
	 * 
	 * @param string $user_id	The user id
	 * @param unknown $user_id
	 */
	static function is_ng6_administrator($user_id){
		$is_ng6_admin = false;
		if( $user_id != null){
			$usergroup = null;
			$queryParts = Array(
					'SELECT' => 'fe_users.usergroup',
					'FROM' => 'fe_users',
					'WHERE' => 'fe_users.uid='.$user_id,
					'GROUPBY' => '',
					'ORDERBY' => '',
					'LIMIT' 	=> '',
			);
			$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
			while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
				$usergroup = $res_row['usergroup'];
			}
			$groups = explode ( ',', $usergroup) ;
			if (in_array('1', $groups)){
				$is_ng6_admin = true;
			}
		}
		return $is_ng6_admin;
	}
	
	/**
	 * Return the list of ng6_admin users
	 */
	static function get_ng6_admin_users(){
		$ng6_admin_users = null;
		$queryParts = Array(
				'SELECT' => 'fe_users.uid, fe_users.username, fe_users.first_name, fe_users.last_name, fe_users.email',
				'FROM' => 'fe_users',
				'WHERE' => '',
				'GROUPBY' => '',
				'ORDERBY' => '',
				'LIMIT' 	=> '',
		);
		$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
		
		while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			if (tx_nG6_db::is_ng6_administrator($res_row['uid'])){
				$ng6_admin_users[ ] =  array(
						'id' 			=>	$res_row['uid'],
	    				'username' 		=>	$res_row['username'],
	    				'first_name' 	=>	$res_row['first_name'],
	    				'last_name' 	=>	$res_row['last_name'],
	    				'email' 		=>	$res_row['email']
				);
			}
		}
		return $ng6_admin_users;
	}
	
	/**
	 * Add an existing user to the ng6_admin group
	 * @param unknown $userid
	 */
	static function add_to_ng6_admin( $userid ){
		$user_info = tx_nG6_db::get_user_informations($userid);
		$pieces = explode(",", $user_info['usergroup']);
		
		if (!in_array("1", $pieces)) {
			$pieces [] = "1";
			$group = implode(",", $pieces);
			$GLOBALS['TYPO3_DB']->exec_UPDATEquery ('fe_users', 'fe_users.uid='.$userid, array('fe_users.usergroup' => $group));
		}
	}
	
	
	
	/**
	 * Remove an user from the ng6_admin group
	 * @param unknown $userids
	 */
	static function remove_from_ng6_admin( $userids ){
		
		foreach ($userids as $userid){
			$user_info = tx_nG6_db::get_user_informations($userid);
			$pieces = explode(",", $user_info['usergroup']);
			
			if (in_array('1', $pieces)){
				unset($pieces[array_search('1',$pieces)]);
				$group = implode(",", $pieces);
				$GLOBALS['TYPO3_DB']->exec_UPDATEquery ('fe_users', 'fe_users.uid='.$userid, array('fe_users.usergroup' => $group));
			}
		}
	}
	
	
	/**
	 * Return if the user is a superuser (administrator) for the specified project/run/analyse
	 *
	 * @param	string	$user_id		the user id
	 * @param	string	$type			can be project/run/analyse
	 * @param	string	$uid			the [project/run/analyse]'s uid
	 * @return	boolean {1 = ok, 0 = nok}
	 */
    static function is_administrator($user_id, $type, $uid) {
    	
    	$is_admin = false;
    	
    	if(!$user_id == null){
    	
	    	// Project
	   		if ($type == 'project') {
				$is_admin = tx_nG6_db::is_project_administrator($user_id, $uid);
			
			// Run
			} elseif ($type == 'run') {
				$queryParts = array(
					'SELECT' => 'project_id',
					'FROM' => 'tx_nG6_project_run',
					'WHERE' => 'run_id='.$uid,
					'GROUPBY' => '',
					'ORDERBY' => '',
					'LIMIT' => ''
				);
				$res1 = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
				$val = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res1);
				$is_admin = tx_nG6_db::is_project_administrator($user_id, $val['project_id']);
			
			// Analyze
			} elseif ($type == 'analyze') {
				$queryParts = array(
					'SELECT' => 'run_id',
					'FROM' => 'tx_nG6_run_analyze',
					'WHERE' => 'analyze_id='.$uid,
					'GROUPBY' => '',
					'ORDERBY' => '',
					'LIMIT' => ''
				);
				$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
				
				// Run_analyze
				if ($GLOBALS['TYPO3_DB']->sql_num_rows($res) > 0) {
					$val = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
					$queryParts = array(
						'SELECT' => 'project_id',
						'FROM' => 'tx_nG6_project_run',
						'WHERE' => 'run_id='.$val['run_id'],
						'GROUPBY' => '',
						'ORDERBY' => '',
						'LIMIT' => ''
					);
					$res2 = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
					$val = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res2);
					$is_admin = tx_nG6_db::is_project_administrator($user_id, $val['project_id']);
				
				// project_analyze
				} else {
					$queryParts = array(
						'SELECT' => 'project_id',
						'FROM' => 'tx_nG6_project_analyze',
						'WHERE' => 'analyze_id='.$uid,
						'GROUPBY' => '',
						'ORDERBY' => '',
						'LIMIT' => ''
					);
					$res2 = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
					$val = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res2);
					$is_admin = tx_nG6_db::is_project_administrator($user_id, $val['project_id']);
				}
				
			} 
    	}
		
		return $is_admin; 
    }

    /**
    * Get user's rights on project.
    *
    * @param int $project_id the project id
    * @param int $user_id the user id
    */
    static function get_user_right_on_project($user_id, $project_id) {
    	$right_id = -1;
    	$queryParts = array(
    		'SELECT' => 'fe_rights.right_id AS right_id ',
    		'FROM' => 'fe_rights',
    		'WHERE' => 'fe_rights.fe_user_id='. $user_id .' AND fe_rights.project_id='.$project_id
    	);
    	$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
    	while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
    		$right_id = $row['right_id'];
    	}    	
    	return $right_id;
    }
    
    /**
     * Return true if the user is a member on the project
     *
     * @param int $user_id the user id
     * @param int $project_id ths project id
     */
    static function is_project_member($user_id, $project_id){
    	$is_member = false;
    	$right = tx_nG6_db::get_user_right_on_project($user_id, $project_id);
    	if ($right == 0) {
    		$is_member = true;
    	}
    	return $is_member;
    }
    
    /**
     * Return true if the user is a manager on the project
     *
     * @param int $user_id the user id
     * @param int $project_id ths project id
     */
    static function is_project_manager($user_id, $project_id){
    	$is_manager = false;
    	$right = tx_nG6_db::get_user_right_on_project($user_id, $project_id);
    	if ($right == 1) {
    		$is_manager = true;
    	}
    	return $is_manager;
    }
    
    /**
     * Return true if the user is an administrator on the project
     *
     * @param int $user_id the user id
     * @param int $project_id ths project id
     */
    static function is_project_administrator($user_id, $project_id){
    	$is_admin = false;
    	$right = tx_nG6_db::get_user_right_on_project($user_id, $project_id);
    	if ($right == 2) {
    		$is_admin = true;
    	}
    	return $is_admin;
    }

    
    
    /**
     * Get all users  with rights on the project.
     *
     * @param int $project_id the project id
     */
    static function get_all_users_on_project($project_id){
    	$users = array();
    	$queryParts = Array(
    			'SELECT' => 'fe_users.uid, fe_users.username, fe_users.lastlogin, fe_users.first_name, '
    			.' fe_users.last_name, fe_users.email, fe_rights.right_id, fe_users.cruser_id , fe_users.updatable_password',
    			'FROM' => 'fe_users INNER JOIN fe_rights ON fe_users.uid=fe_rights.fe_user_id ',
    			'WHERE' => 'fe_rights.project_id='.$project_id,
    			'GROUPBY' => '',
    			'ORDERBY' => '',
    			'LIMIT' 	=> '',
    			);
    	$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
    	$res_tab = array();
    	while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
    
    		$user_id = $res_row['uid'];
    		$lastlogin = date("d-m-Y", $res_row['lastlogin']);
    
    		$cruser_infos = tx_nG6_db::get_user_informations($res_row['cruser_id']);
    
    		if (!isset ($users['user_'.$user_id])){
    			$users['user_'.$user_id] = array(
    					'id' 			=>	$user_id,
    					'username' 		=>	$res_row['username'],
    					'lastlogin' 	=>	$lastlogin,
    					'first_name' 	=>	$res_row['first_name'],
    					'last_name' 	=>	$res_row['last_name'],
    					'email' 		=>	$res_row['email'],
    					'right_id' 		=>	$res_row['right_id'],
    					'cruser_id'		=>  $res_row['cruser_id'],
    					'cruser_name'	=>  $cruser_infos['username'],
    					'updatable_password'		=>  $res_row['updatable_password']
    			);
    		}
    
    	}
    	return $users;
    }
    

    /**
     * Delete acces from a project to a specific member
     * 
     * @param int $user_id	the user id
     * @param int $project_id	the project id
     */
    static function delete_access_to_project($user_id, $project_id){
    	$GLOBALS['TYPO3_DB']->exec_DELETEquery('fe_rights', 'fe_rights.fe_user_id='.$user_id.' AND fe_rights.project_id='.$project_id);
    }
    
    
    /**
     * Change right on project for a member.
     * 
     * @param int $user_id		the user id
     * @param int $project_id	the project id
     * @param int $right		the right id (0=member, 1=manager, 2=admin)
     */
    static function change_access_to_project($user_id, $project_id, $right){
    	$GLOBALS['TYPO3_DB']->exec_UPDATEquery ('fe_rights', 'fe_rights.fe_user_id='.$user_id.' AND fe_rights.project_id='.$project_id, array('fe_rights.right_id' => $right));
    }
    
    /**
     * Add a new member to the project.
     *
     * @param int $user_id		the user id
     * @param int $project_id	the project id
     * @param int $right			the right id (0=member, 1=manager, 2=admin)
     */
    static function add_access_to_project($user_id, $project_id, $right_id){
    	
    	$member_tab = array(
    		'fe_user_id'	=>	$user_id,
    		'project_id'	=>	$project_id,
    		'right_id'	=>	$right_id
    	);
    	
    	$GLOBALS['TYPO3_DB']->exec_INSERTquery ('fe_rights', $member_tab);
    }
    
    
    
    /**
     * Get all usernames, firstnames and lastnames starting with a string.
     * 
     * @param string $name_start	the string part
     * @param string $user_col		the fe_users column to use
     * @return array
     */
    static function get_all_names_starting_with ($name_start, $user_col) {
    	$queryParts = Array(
    		// 'SELECT' => 'fe_users.uid, fe_users.username, fe_users.last_name, fe_users.first_name, fe_users.password, fe_users.email, fe_users.cruser_id, fe_groups.title, fe_groups.tx_nG6_organism, fe_groups.tx_nG6_location',
			'SELECT' => 'fe_users.uid, fe_users.username, fe_users.last_name, fe_users.first_name, fe_users.email, fe_users.cruser_id, fe_groups.title, fe_groups.tx_nG6_organism, fe_groups.tx_nG6_location',
			'FROM' => 'fe_users INNER JOIN fe_groups ON fe_users.usergroup=fe_groups.uid',
			'WHERE' => 'fe_users.'.$user_col.' LIKE "'.$name_start.'%"',
			'GROUPBY' => '',
			'ORDERBY' => '',
			'LIMIT' 	=> '',
    	);
    	$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
    	$res_tab = array();
    	while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
    		$res_tab[] = array(
    			'uid'			=>	$res_row['uid'],
    			'username' 		=>	$res_row['username'],
    			'first_name' 	=>	$res_row['first_name'],
    			'last_name' 	=>	$res_row['last_name'],
    			// 'password' 		=>	$res_row['password'],
    			'email' 		=>	$res_row['email'],
    			'title' 		=>	$res_row['title'],
    			'cruser_id' 	=>	$res_row['cruser_id'],
    			'organism' 		=>	$res_row['tx_nG6_organism'],
    			'location' 		=>	$res_row['tx_nG6_location']
    				
    		);
    	}
    	return $res_tab;
    }
    
    
   /**
    * Get all groupnames starting with a string.
    * 
    * @param string $starts_with 	the string part
    * @param string $col 	        the colone name to seek in
    * @return array
    */
    static function get_all_starts_with_in_group($starts_with, $col) {
    	$queryParts = Array(
        		'SELECT' => '*',
    			'FROM' => 'fe_groups',
    			'WHERE' => 'fe_groups.'.$col.' LIKE "'.$starts_with.'%"',
    			'GROUPBY' => '',
    			'ORDERBY' => '',
    			'LIMIT' 	=> '',
    	);
    	$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
    	$res_tab = array();
    	$distinct = array();
    	while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
    		if( $col == 'title') {
    			if ($res_row["title"] != 'ng6_admin'){
    				$res_tab[] = array(
    						'uid'			=>	$res_row['uid'],
    						'title'			=>	$res_row['title'],
    						'organism'		=>	$res_row['tx_nG6_organism'],
    						'location'		=>	$res_row['tx_nG6_location']
    				);
    			}
    		} else {
    			if( !isset($distinct[$res_row[$col]]) ) {
	    			$res_tab[] = array(
	    				'uid'			=>	$res_row['uid'],
	    				substr($col, 7)		=>	$res_row[$col]
	    			);
	    			$distinct[$res_row[$col]] = True ;
    			}
    		}
    	}
    	return $res_tab;
    }
    
    
    /**
     * Get user informations.
     * 
     * @param int  $user_id the user id
     */
    static function get_user_informations($user_id){
    	$queryParts = Array(
    		'SELECT' => 'fe_users.uid, fe_users.username, fe_users.last_name, fe_users.first_name, fe_users.password, fe_users.email, fe_users.cruser_id, fe_users.usergroup',
    		'FROM' => 'fe_users',
    		'WHERE' => 'fe_users.uid='.$user_id,
    		'GROUPBY' => '',
    		'ORDERBY' => '',
    		'LIMIT' 	=> '',
    	);
    	$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
    	$res_tab = null;
    	while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
    		$res_tab = array(
    			'uid'			=>	$res_row['uid'],
    			'username' 		=>	$res_row['username'],
    			'first_name' 	=>	$res_row['first_name'],
    			'last_name' 	=>	$res_row['last_name'],
    			'password' 		=>	$res_row['password'],
    			'email' 		=>	$res_row['email'],
    			'cruser_id' 	=>	$res_row['cruser_id'],
    			'usergroup'		=> 	$res_row['usergroup']
    		);
    	}
    	return $res_tab;
    }
    
    static function select_user_by_username($username){
    	$res_tab = null;
    	$queryParts = Array(
    		'SELECT' => 'fe_users.uid, fe_users.username, fe_users.first_name, fe_users.last_name, fe_users.email',
    		'FROM' => 'fe_users',
    		'WHERE' => 'fe_users.username LIKE "'.$username.'" ',
			'GROUPBY' => '',
			'ORDERBY' => '',
			'LIMIT' 	=> '',
    	);
    	$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
    	while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
	    	$res_tab = array(
    			'uid'			=>	$res_row['uid'],
    			'username' 		=>	$res_row['username'],
    			'first_name' 	=>	$res_row['first_name'],
    			'last_name' 	=>	$res_row['last_name'],
    			'email' 		=>	$res_row['email'],
    		);
    	}
    	return $res_tab;
    }
    
    static function user_field_exist ($field, $value){
    	$queryParts = Array(
    		'SELECT' => 'fe_users.uid',
    		'FROM' => 'fe_users',
    		'WHERE' => 'fe_users.' . $field . ' LIKE "'.$value.'" ',
			'GROUPBY' => '',
			'ORDERBY' => '',
			'LIMIT' 	=> '',
    	);
    	$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
    	$uid = null;
    	while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
	    	$uid = $res_row['uid'];
    	}
    	return isset($uid);
    }

    /**
     * Create a new user in the DB.
     * 
     * @param array $user_datas the user array
     * @return 0: ok, 1: username exists, 2: email exists
     */
    static function add_user($user_name, $first_name, $last_name, $email, $password, $cruser_id, $group_id, $pid){
    	
    	// first check if username or email are unique
    	if (tx_nG6_db::user_field_exist("username", $user_name)) {
    		return 1;
    	} else if (tx_nG6_db::user_field_exist("email", $email)) {
    		return 2;
    	} else {
			// Create new user
			$user_datas = array(
				'username'		=>	$user_name,
				'pid'			=>	$pid,
				'first_name'	=>	$first_name,
				'last_name'		=>	$last_name,
				'email'			=>	$email,
				'password'		=>	$password,
				'cruser_id'		=>	$cruser_id,
				'usergroup'		=> 	$group_id,
				'tstamp'		=> 	time(),
				'crdate'		=> 	time()
			);
	    	$GLOBALS['TYPO3_DB']->exec_INSERTquery('fe_users', $user_datas);
	    	return 0;
	    }
    	
    }
    
    /**
     * Update user informations
     *
     * @param $id the id of the user to update
     * @param array $user_data the user array
     * @return 
     */
    static function update_user( $user_id , $first_name, $last_name, $email, $group_name,$organism, $location ){
    
    	$user_datas = array(
    			'first_name'	=>	$first_name,
    			'last_name'		=>	$last_name,
    			'email'			=>	$email,
    			'password'      =>  $password,
    			'usergroup'		=>  null
    	);
    	
    	// if group does not exists, create group
    	if (isset($group_name) and !empty($group_name)) {
    		$group_id = tx_nG6_db::get_group_id($group_name);
    		if( !isset($group_id) ){
    			$cruser_id = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('creator'));
    			tx_nG6_db::create_new_group($cruser_id, $group_name, $organism, $location);
    			$group_id = tx_nG6_db::get_group_id($group_name);
    			$user_datas['usergroup'] = $group_id;
    		}
    	}
    	
    	$updated = array();
    	foreach ($user_datas as $key => $value) {
    		if (isset($value) and !empty($value)) {
    			tx_nG6_db::update_field('fe_users', $user_id, $key, $value);
    			$updated[$key] = $value;
    		}
    	}
    	
    	if (count($updated) > 0){
			tx_nG6_db::update_field('fe_users', $user_id, 'tstamp', time());
    	}
    	return $updated;
    }
    
    /**
     * Tell if the group name exists in the DB.
     * 
     * @param string $group_name the group name
     */
    static function get_group_id($group_name) {
    	$exists = null;
    	$queryParts = Array(
    		'SELECT' => 'fe_groups.uid',
    		'FROM' => 'fe_groups',
    		'WHERE' => 'fe_groups.title="'.$group_name.'" ',
    		'GROUPBY' => '',
    		'ORDERBY' => '',
    		'LIMIT' 	=> '',
    	);
    	$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
    	while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
    		if(isset($res_row['uid'])){
    			$exists = $res_row['uid'];
    		}
    	}
    	return $exists;
    }
    
    
    /**
     * Found the usergroup of a user.
     * 
     * @param int $user_id user id
     * @return the user group, null error
     */
	static function get_user_usergroup($user_id){
    	$usergroup = null;
    	$queryParts = Array(
    	    		'SELECT' => 'fe_users.usergroup',
    	    		'FROM' => 'fe_users',
    	    		'WHERE' => 'fe_users.uid='.$user_id,
    				'GROUPBY' => '',
    				'ORDERBY' => '',
    				'LIMIT' 	=> '',
    	);
    	$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
    	while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
    		$usergroup = $res_row['usergroup'];
    	}
    	// remove ng6 admin group if present
    	$ex = explode(',' , $usergroup);
    	if(($key = array_search('1', $ex)) !== false) {
    		unset($ex[$key]);
    	}
    	$usergroup = $ex[0];
    	return $usergroup;
    }
    
    /**
     * Number of project the user can access to
     * 
     * @param int $user_id the user id
     * @return string the count
     */
    static function count_number_of_project_access($user_id) {
    	$queryParts = array(
    			'SELECT' => 'COUNT(fe_rights.project_id) AS p_number ',
    			'FROM' => 'fe_rights',
    			'WHERE' => 'fe_rights.fe_user_id='.$user_id,
    			'GROUPBY' => '',
    			'ORDERBY' => '',
    			'LIMIT' => ''
    	);
    	$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
    	while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
    		$number = $row['p_number'];
    	}
    	return $number;
    }
    
    /**
     * Delete a user from database
     * 
     * @param int $user_id the user id
     */
    static function delete_user($user_id){
    	$GLOBALS['TYPO3_DB']->exec_DELETEquery('fe_users', 'fe_users.uid='.$user_id);
    }
    /**
     * finalize_installation : Update fe and be user from database
     *
     * @param string user_name the user id
     * @return bool true if ok false else
     */
    static function finalize_installation($user_id, $user_name, $first_name, $last_name, $email, $password, $pid, $project, $descr,
    		$group_name, $organism, $location){
    	
    	$group_id = tx_nG6_db::get_group_id($group_name);
    	if( !isset($group_id) ){
    		$group_id = tx_nG6_db::create_new_group($user_id, $group_name, $organism, $location);
    	}
    	
    	$fe_user_datas = array(
    			'username'		=>	$user_name,
    			'pid'			=>	$pid,
    			'first_name'	=>	$first_name,
    			'last_name'		=>	$last_name,
    			'email'			=>	$email,
    			'password'		=>	$password,
    			'tstamp'		=> 	time(),
    			'usergroup'		=>	$group_id
    	);
    	$be_user_datas = array(
    			'username'		=>	$user_name,
    			'realName'	=>	$first_name. " ".$last_name,
    			'email'			=>	$email,
    			'password'		=>	$password,
    			'tstamp'		=> 	time()
    	);

    	$project_datas = array(
    			'name'			=>	$project,
    			'description'	=>	$descr
    	);
    	 
    	$res = $GLOBALS['TYPO3_DB']->exec_UPDATEquery('fe_users', 'uid='.$user_id, $fe_user_datas );
    	if ($res == True){
    		tx_nG6_db::add_to_ng6_admin($user_id);
    		$res2 = $GLOBALS['TYPO3_DB']->exec_UPDATEquery('be_users', 'uid='.$user_id, $be_user_datas );
    	}
    	if ($res == True){
    		$queryParts = array(
    				'SELECT' => 'uid',
    				'FROM' => 'tx_nG6_project',
    				'WHERE' => 'name = "project_install"',
    				'GROUPBY' => '',
    				'ORDERBY' => '',
    				'LIMIT' => ''
    		);
    		$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
    		$project_id="";
    		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
    	    		$project_id = $row['uid'];
    		}
    		if ($project_id != "") {
    			$res = $GLOBALS['TYPO3_DB']->exec_UPDATEquery('tx_nG6_project', 'uid='.$project_id,$project_datas );
    		} else {
    			$res=False;
    		}
    	}
    	if ($res == True){
			$res = $GLOBALS['TYPO3_DB']->exec_UPDATEquery('pages', 'uid=2', array('hidden' => '0'));
			$res = $GLOBALS['TYPO3_DB']->exec_UPDATEquery('pages', 'uid=3', array('hidden' => '0', 'title' => 'Projects'));
			$res = $GLOBALS['TYPO3_DB']->exec_UPDATEquery('pages', 'uid=4', array('hidden' => '0'));
			$res = $GLOBALS['TYPO3_DB']->exec_UPDATEquery('pages', 'uid=6', array('hidden' => '0'));
    	}
    	return $res;
    }
    
    /**
     * Count user in a group
     * 
     * @param int $group_id the group id
     * @return string the count
     */
    static function count_users_in_group($group_id){
    	$queryParts = array(
    		'SELECT' => 'COUNT(fe_users.usergroup) AS g_count ',
    		'FROM' => 'fe_users',
    		'WHERE' => 'fe_users.usergroup='.$group_id,
    		'GROUPBY' => '',
    		'ORDERBY' => '',
    		'LIMIT' => ''
    	);
    	$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
    	while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
    		$number = $row['g_count'];
    	}
    	return $number;
    }
    
    /**
     * Delete a group from database
     * 
     * @param int $group_id the group id
     */
    static function delete_group($group_id){
    	$GLOBALS['TYPO3_DB']->exec_DELETEquery('fe_groups', 'fe_groups.uid='.$group_id);
    }
    
    /**
    * Create a new group in the DB.
    *
    * @param array $group_datas the group array
    */
    static function create_new_group($cruser_id, $group_name, $organism, $location) {
    
    	// Create new user
		$group_datas = array(
			'cruser_id'			=>	$cruser_id,
			'title'				=>	$group_name,
			'tx_nG6_organism'	=>	$organism,
			'tx_nG6_location'	=>	$location,
			'hidden'			=>	0,
			'deleted'			=>	0,
			'description'		=>	'',
			'subgroup'			=>	'',
			'tstamp'			=> 	time(),
			'crdate'			=> 	time()
		);
    
    	$GLOBALS['TYPO3_DB']->exec_INSERTquery('fe_groups', $group_datas);
    	return $GLOBALS['TYPO3_DB']->sql_insert_id();
    }
    
    /**
     * List all groups <uid,title>
     * 
     * @return array
     */
    static function get_group_list(){
    	$queryParts = Array(
    		'SELECT' => 'fe_groups.uid, fe_groups.title',
    		'FROM' => 'fe_groups',
    		'WHERE' => '',
    		'GROUPBY' => '',
    		'ORDERBY' => 'fe_groups.uid',
    		'LIMIT' 	=> '',
    	);
    	$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
    	$res_tab = null;
    	while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
    		$res_tab[] = array(
    			'uid'	=>	$res_row['uid'],
    			'title' =>	$res_row['title']
    		);
    	}
    	return $res_tab;
    }
    
    /**
     * List all users in group
     * 
     * @param int $group_id	the grou pid
     * @return array
     */
    static function get_all_users_in_group($group_id){
    	$users_in_group = null;
    	$queryParts = Array(
        		'SELECT' => 'fe_users.uid',
        		'FROM' => 'fe_users',
        		'WHERE' => 'fe_users.usergroup='.$group_id,
        		'GROUPBY' => '',
        		'ORDERBY' => '',
        		'LIMIT' 	=> '',
    	);
    	$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
    	while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
    		$users_in_group[] = $res_row['uid'];
    	}
    	return $users_in_group;
    }
    
    /**
     * Get group informations.
     * 
     * @param int $group_id
     * @return array group information or null
     */
    static function get_group_informations($group_id){
    	$queryParts = Array(
    		'SELECT' => 'fe_groups.uid, fe_groups.title, fe_groups.description, fe_groups.crdate, fe_groups.cruser_id, fe_groups.tx_nG6_organism, fe_groups.tx_nG6_location ',
    		'FROM' => 'fe_groups',
    		'WHERE' => 'fe_groups.uid='.$group_id,
    		'GROUPBY' => '',
    		'ORDERBY' => '',
    		'LIMIT' 	=> '',
    	);
    	$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
    	$res_tab = null;
    	while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
    		$res_tab = array(
    			'uid'			=>	$res_row['uid'],
    			'title' 		=>	$res_row['title'],
    			'description' 	=>	$res_row['description'],
    			'crdate' 		=>	$res_row['crdate'],
    			'cruser_id' 	=>	$res_row['cruser_id'],
    			'organism'		=>	$res_row['tx_nG6_organism'],
    			'location'		=>	$res_row['tx_nG6_location']
    		);
    	}
    	return $res_tab;
    }
    
    /**
	 * Returns true if the user belongs to the ng6 superadmin group
	 * 
	 * @param string $user_id	The user id
	 * @param unknown $user_id
	 */
	static function is_user_ng6_superadmin($user_id){
		$is_ng6_superadmin = false;
		if( $user_id != null){
			$usergroup = null;
			$queryParts = Array(
					'SELECT' => 'fe_users.usergroup',
					'FROM' => 'fe_users',
					'WHERE' => 'fe_users.uid='.$user_id,
					'GROUPBY' => '',
					'ORDERBY' => '',
					'LIMIT' 	=> '',
			);
			$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
			
			while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
				$usergroup = $res_row['usergroup'];
			}
			$groups = explode ( ',', $usergroup) ;
			
    		$superadmin_group_id = tx_nG6_db::get_superadmin_group_id();
    			
			if (in_array($superadmin_group_id, $groups)){
				$is_ng6_superadmin = true;
			}
		}
		return $is_ng6_superadmin;
	}
	
	/**
	 * Returns the id of the ng6 superadmin group
	 *
	 * 
	 */
	static function get_superadmin_group_id(){
		$queryParts = array(
    		'SELECT' => 'uid',
    		'FROM' => 'fe_groups',
    		'WHERE' => "fe_groups.title='ng6_superadmin'",
    		'GROUPBY' => '',
    		'ORDERBY' => '',
    		'LIMIT' => ''
    	);
    	$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
    	$superadmin_group_id = NULL;
    	while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
    		$superadmin_group_id = $row['uid'];
    	}
		return $superadmin_group_id;
	}
	
    
    /**
	 * Returns the list of ng6_superadmin users
	 */
	static function get_ng6_superadmin_users(){
		$ng6_superadmin_users = null;
		$queryParts = Array(
			'SELECT' => 'fe_users.uid, fe_users.username, fe_users.first_name, fe_users.last_name, fe_users.email',
			'FROM' => 'fe_users',
			'WHERE' => '',
			'GROUPBY' => '',
			'ORDERBY' => '',
			'LIMIT' 	=> '',
		);
		$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
		
		while($res_row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			if (tx_nG6_db::is_user_ng6_superadmin($res_row['uid'])){
				$ng6_superadmin_users[ ] =  array(
					'id' 			=>	$res_row['uid'],
	    			'username' 		=>	$res_row['username'],
	    			'first_name' 	=>	$res_row['first_name'],
	    			'last_name' 	=>	$res_row['last_name'],
	    			'email' 		=>	$res_row['email']
				);
			}
		}
		return $ng6_superadmin_users;
	}
	
	/**
	 * Add an existing user to the ng6_superadmin group
	 * @param unknown $userid
	 */
	static function add_to_ng6_superadmin( $userid ){

		$user_info = tx_nG6_db::get_user_informations($userid);
		$pieces = explode(",", $user_info['usergroup']);
		$superadmin_group_id = tx_nG6_db::get_superadmin_group_id();
		
		if (!in_array($superadmin_group_id, $pieces)) {
			$pieces [] = $superadmin_group_id;
			$group = implode(",", $pieces);
			$GLOBALS['TYPO3_DB']->exec_UPDATEquery ('fe_users', 'fe_users.uid='.$userid, array('fe_users.usergroup' => $group));
		}
	}
	
	
	
	/**
	 * Remove an user from the ng6_superadmin group
	 * @param unknown $userids
	 */
	static function remove_from_ng6_superadmin( $userids ){
		$superadmin_group_id = tx_nG6_db::get_superadmin_group_id();
		foreach ($userids as $userid){
			$user_info = tx_nG6_db::get_user_informations($userid);
			$pieces = explode(",", $user_info['usergroup']);
			
			if (in_array($superadmin_group_id, $pieces)){
				unset($pieces[array_search($superadmin_group_id,$pieces)]);
				$group = implode(",", $pieces);
				$GLOBALS['TYPO3_DB']->exec_UPDATEquery ('fe_users', 'fe_users.uid='.$userid, array('fe_users.usergroup' => $group));
			}
		}
	}   
}

if (defined('TYPO3') && $TYPO3_CONF_VARS[TYPO3]['XCLASS']['ext/nG6/class.tx_nG6_db.php']) {
    include_once($TYPO3_CONF_VARS[TYPO3]['XCLASS']['ext/nG6/class.tx_nG6_db.php']);
}

?>
