<?php
namespace ng6\Controller;
/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/Classes/Controller/class.tx_nG6_utils.php');
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/Classes/Controller/tx_nG6_db.php');
use TYPO3\CMS\Core\Context\Context;
use nG6\Controller\tx_nG6_db;
use nG6\Controller\tx_nG6_utils;
class tx_nG6_pi6_purge {

 static function send_purge_demand_mail($project_ids, $extension_allowed = 1){
        $context = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(Context::class);
        $user_id = $context->getPropertyFromAspect('frontend.user', 'id');
        $project_name="";
        $configurationManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Configuration\\BackendConfigurationManager');
        //$configurationManager->currentPageId = 1;
        $extbaseFrameworkConfiguration = $configurationManager->getTypoScriptSetup();
        $email_to_discard = $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi6.']["email_to_discard"];
        $run_link_url = $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi6.']["run_link_url"];
        $min_extension_duration = $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi6.']["min_extension_duration"];
        $delay_purge = $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi6.']["delay_purge"];
        $min_extension_size = $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi6.']["min_extension_size"];
        $extension_url_price = $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi6.']["extension_url_price"];
        $email_from = $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi6.']["email_from"];
        $email_copy = $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi6.']["email_copy"];
        $envelope_sender_address = $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi6.']["envelope_sender_address"];
        $email_warning= $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi6.']["email_warning"];
        
        foreach(explode(",", $project_ids) as $project_id){
			#retrieve project data
			$p=tx_nG6_db::select_a_project_retention_data_info($project_id, FALSE, TRUE);

			#retrieve discarded emails
			$string_emails_to_discard = $email_to_discard ;
			$array_emails_to_discard = explode(',',$string_emails_to_discard);
            
			#build email list of managers
			$users_id=array();
			$users_emails = array();
			foreach ( $p[$project_id]["users"] as  $u ){
				if (!isset ($users_id[$u["right_level_label"]]) ){
					$users_id[$u["right_level_label"]]=Array();
					$users_emails[$u["right_level_label"]]=Array();
				}
				$users_id[$u["right_level_label"]][] = $u["user_id"];
				$users_emails[$u["right_level_label"]][] = $u["email"];
			}

			//Warn if mail is sent to several manager
			$email_warn="" ;
			$purge_email_to="";
			if ( isset($users_emails['manager']) && count( $users_emails['manager']) > 1 ) {
				$email_warn.="Be aware that every user associated with this project received this email, please send only one answer per purge alert number.\n";
			}

			$purge_user_id_to = array();
			$users_emails_to = array();
			if(isset($users_emails['administrator'])){
				$users_emails_to = array_merge($users_emails_to, $users_emails['administrator']);
				$purge_user_id_to = array_merge($purge_user_id_to, $users_id['administrator']);
			}
			if(isset($users_emails['manager'])){
				$users_emails_to = array_merge($users_emails_to,$users_emails['manager']);
				$purge_user_id_to = array_merge($purge_user_id_to, $users_id['manager']);
			}
			if(isset($users_emails['member'])){
				$users_emails_to = array_merge($users_emails_to, $users_emails['member']);
				$purge_user_id_to = array_merge($purge_user_id_to, $users_id['member']);
			}

			$purge_email_to = join(', ',$users_emails_to );

			$email_warn.=  "Every user associated with this project received this alert.\n";
			if( isset($users_emails['administrator']) && count($users_emails['administrator']) > 0 ){
				$email_warn.=  " - Administrator(s): ".join(', ',$users_emails['administrator']). "\n";
			}
			if( isset($users_emails['manager']) && count($users_emails['manager']) > 0 ){
				$email_warn.=  " - Manager(s): ".join(', ',$users_emails['manager']). "\n";
			}
			if( isset($users_emails['member']) && count($users_emails['member']) > 0 ){
				$email_warn.=  " - Member(s): ".join(', ',$users_emails['member']). "\n";
			}
            
            //Retrieve purgeable information for email
            $run_info=Array();
            $analyses_info=Array();
            $all_purgeable_runs=array_merge($p[$project_id]["state"]["stored"]["run_ids"],$p[$project_id]["state"]["extended"]["run_ids"]);
            $all_purgeable_analysis=array_merge($p[$project_id]["state"]["stored"]["analysis_ids"],$p[$project_id]["state"]["extended"]["analysis_ids"]);
            $nb_run_purgeable = $p[$project_id]["state"]["stored"]["nb_run"]+ $p[$project_id]["state"]["extended"]["nb_run"];
            $nb_analyse_purgeable = $p[$project_id]["state"]["stored"]["nb_analyze"]+ $p[$project_id]["state"]["extended"]["nb_analyze"];
            //Retrieve run name
            $search=array("###TYPE_OBJECT###","###RUN_ID###","###PROJECT_ID###");
            
            foreach($all_purgeable_runs as $run_id ){
            	$run = tx_nG6_db::select_run($run_id);
            	$run_name = $run["name"];    
            	$replace=array("run_id",$run_id,$project_id);
            	$run_info[] = '<a href="'.str_replace($search, $replace, $run_link_url).'">'.$run["name"].' ('.$run_id.')</a>';
            }
            foreach($all_purgeable_analysis as $analysis_id ){
            	$analysis = tx_nG6_db::select_analyse($analysis_id); 
            	$analysis_name = $analysis["name"];
            	$replace=array("analyze_id",$analysis_id,$project_id);
            	$analyses_info[] = '<a href="'.str_replace($search, $replace, $run_link_url).'">'.$analysis_name.' ('.$analysis_id.')</a>';
            }
            if (!$user_id){
            	http_response_code(401);
            	return "Nobody seems to be authenticated.";	
            }
            #Add purge demand to get id
            $purge_demand_id = tx_nG6_db::add_purge_demand($user_id,$project_id,$p[$project_id]["total_purgeable_size"],$all_purgeable_runs,$all_purgeable_analysis,$purge_user_id_to);
            
            //If the trigger check_demand_insert is triggered, add_purge_demand() returns 0 and the process has to send a mail to warn the admins instead.
            if($purge_demand_id > 0){
            	// We remove the hidden runs and analyzes from $p so they will not appear in the purge alert mail
            	$p=tx_nG6_db::select_a_project_retention_data_info($project_id, FALSE, FALSE);
            	
            	//We then need to compute the displayed values once more, to make the hidden elements part of the purge without displaying them in the mail
                $all_purgeable_runs=array_merge($p[$project_id]["state"]["stored"]["run_ids"],$p[$project_id]["state"]["extended"]["run_ids"]);
                $all_purgeable_analysis=array_merge($p[$project_id]["state"]["stored"]["analysis_ids"],$p[$project_id]["state"]["extended"]["analysis_ids"]);
                $nb_run_purgeable = $p[$project_id]["state"]["stored"]["nb_run"]+ $p[$project_id]["state"]["extended"]["nb_run"];
                $nb_analyse_purgeable = $p[$project_id]["state"]["stored"]["nb_analyze"]+ $p[$project_id]["state"]["extended"]["nb_analyze"]; 
                
                //We need to reset those values to only display unhidden elements
                $run_info=Array();
                $analyses_info=Array();
                
                foreach($all_purgeable_runs as $run_id ){
                	$run = tx_nG6_db::select_run($run_id);
                	$run_name = $run["name"];    
                	$replace=array("run_id",$run_id,$project_id);
                	$run_info[] = '<a href="'.str_replace($search, $replace, $run_link_url).'">'.$run["name"].' ('.$run_id.')</a>';
                }
                foreach($all_purgeable_analysis as $analysis_id ){
                    $analysis = tx_nG6_db::select_analyse($analysis_id); 
                    $analysis_name = $analysis["name"];
                    $replace=array("analyze_id",$analysis_id,$project_id);
                    $analyses_info[] = '<a href="'.str_replace($search, $replace, $run_link_url).'">'.$analysis_name.' ('.$analysis_id.')</a>';
                }
                
                #Build corresponding string array           
                //We get the project size without any hidden run or analyze
                $total_project_size = tx_nG6_db::get_project_size($project_id, true, false);
            			
            	$mail = tx_nG6_utils::get_purge_mail($p[$project_id]["project_name"],$project_id, $nb_run_purgeable,$nb_analyse_purgeable, $purge_demand_id,
					tx_nG6_utils::get_octet_string_representation($p[$project_id]["total_purgeable_size"]),    tx_nG6_utils::get_octet_string_representation($total_project_size),
            	    $delay_purge,
            	    $extension_url_price,$min_extension_duration,
            	    $min_extension_size, $email_warn,  join(', ', $run_info), join(', ', $analyses_info), $extension_allowed);
            			
                $to      =   $purge_email_to;
                $subject = '[nG6 purge] No '.$purge_demand_id.' - Project '.$p[$project_id]["project_name"];
                $headers = array();
                $headers[] = 'From: '.$email_from;
                $headers[] = 'Errors-To: '.$email_from;
                $headers[] = 'X-Mailer: PHP/' . phpversion();
                $headers[] = 'MIME-Version: 1.0';
                $headers[] = 'Content-type: text/html; charset=utf-8';
                $headers[] = 'Cc: '.$email_copy;
                
                //RT : The values checked below do not include hidden elements (2nd query, it doesn't return hidden elements). If both=0, the demand only contains hidden elements, so no mail should be sent.
                if($nb_run_purgeable > 0 || $nb_analyse_purgeable > 0){
                    mail($to, $subject, $mail, implode("\r\n", $headers), '-f '. $envelope_sender_address);
                    //TODO check return function mail ok
                }
            }else{
        		//If the demand could not be inserted because another one already exists for the given project, we send a mail to the nG6 admins
        		$mail = tx_nG6_utils::get_multiple_purge_demand_mail($p[$project_id]["project_name"]);
                
                $to        =   $email_warning;
                $subject   = '[nG6 purge] Project '.$p[$project_id]["project_name"].' already has a purge demand';
                $headers[] = 'From: '.$email_from;
                $headers[] = 'Errors-To: '.$email_from;
                $headers[] = 'X-Mailer: PHP/' . phpversion();
                $headers[] = 'MIME-Version: 1.0';
                $headers[] = 'Content-type: text/html; charset=utf-8';
                $headers[] = 'Cc: '.$email_copy;
                
                mail($to, $subject, $mail, implode("\r\n", $headers), '-f '. $envelope_sender_address);
        	}
        }
    	return "Mail sent";
	}
 
        static function resend_purge_demand_mail ($demands_id) {
            $configurationManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Configuration\\BackendConfigurationManager');
            //$configurationManager->currentPageId = 1;
            $extbaseFrameworkConfiguration = $configurationManager->getTypoScriptSetup();
            $run_link_url = $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi6.']["run_link_url"];
            $email_to_discard = $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi6.']["email_to_discard"];
            $run_link_url = $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi6.']["run_link_url"];
            $min_extension_duration = $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi6.']["min_extension_duration"];
            $delay_purge = $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi6.']["delay_purge"];
            $min_extension_size = $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi6.']["min_extension_size"];
            $extension_url_price = $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi6.']["extension_url_price"];
            $email_from = $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi6.']["email_from"];
            $email_copy = $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi6.']["email_copy"];
            $envelope_sender_address = $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi6.']["envelope_sender_address"];
            $email_warning= $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi6.']["email_warning"];
            $res_demands = tx_nG6_db::get_purge_demand_from_id($demands_id);
                foreach($res_demands as $res_demand){
                        $res_project =  tx_nG6_db::select_project($res_demand["project_id"]);
                        //We compute the project size without hidden elements
                        $total_project_size = tx_nG6_db::get_project_size($res_demand["project_id"], true, false) ;
                        $run_info=Array();
                        $analyses_info=Array();
                        $search=array("###TYPE_OBJECT###","###RUN_ID###","###PROJECT_ID###");
                        $project_id = $res_demand["project_id"];
                        $all_purgeable_runs = explode(',',$res_demand["run_ids"]);
                        $all_purgeable_analysis = explode(',',$res_demand["analyze_ids"]);
                        $nb_run_purgeable = 0;
                        $nb_analyse_purgeable = 0;
                        $displayed_purge_size = 0;
                        foreach($all_purgeable_runs as $run_id ){
    						$run = tx_nG6_db::select_run($run_id);
    						// We exclude hidden runs from the mail
    						if( $run["hidden"] != 1 && isset($run["name"]) ){
    							$run_name = $run["name"];
    							$replace=array("run_id",$run_id,$project_id);
    							$run_info[] = '<a href="'.str_replace($search, $replace, $run_link_url).'">'.$run["name"].' ('.$run_id.')</a>';
								$nb_run_purgeable++;
								$displayed_purge_size = $displayed_purge_size + $run["storage_size"];
							}
						}
						foreach($all_purgeable_analysis as $analysis_id ){
							$analysis = tx_nG6_db::select_analyse($analysis_id); 
    						// We exclude hidden analyzes from the mail
    						if( $analysis["hidden"] != 1 && isset($analysis["name"]) ){
    							$analysis_name = $analysis["name"];
    							$replace=array("analyze_id",$analysis_id,$project_id);
    							$analyses_info[] = '<a href="'.str_replace($search, $replace, $run_link_url).'">'.$analysis_name.' ('.$analysis_id.')</a>';
								$nb_analyse_purgeable++;
								$displayed_purge_size = $displayed_purge_size + $analysis["storage_size"];
							}
						}
						//We now use join(', ', $run_info) and join(', ', $analyses_info) in get_purge_mail().
                        
                        $extension_allowed = 1;
                        $mail = tx_nG6_utils::get_purge_mail($res_project["name"],$res_demand["project_id"],
                            $nb_run_purgeable, $nb_analyse_purgeable, $res_demand["demand_id"],
                            tx_nG6_utils::get_octet_string_representation($displayed_purge_size), tx_nG6_utils::get_octet_string_representation($total_project_size),
                            $delay_purge,
                            $extension_url_price, $min_extension_duration,
                            $min_extension_size, "This email was send to ". join(', ',$res_demand["emails"]).". ", join(', ', $run_info), join(', ', $analyses_info), $extension_allowed);
                        
                        $headers = array();
                        $subject = '[nG6 purge / reminder] No '.$res_demand["demand_id"].' - Project '.$res_project["name"];
                        $headers[] = 'From: '.$email_from;
                        $headers[] = 'Errors-To: '.$email_from;
                        $headers[] = 'X-Mailer: PHP/' . phpversion();
                        $headers[] = 'MIME-Version: 1.0';
                        $headers[] = 'Content-type: text/html; charset=utf-8';
                        $headers[] = 'Cc: '.$email_copy;
                        
                        $to= join(",",$res_demand["emails"]);
                        mail($to, $subject, $mail, implode("\r\n", $headers), '-f '. $envelope_sender_address);
                }
        }
}
        ?>