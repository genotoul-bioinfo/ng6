<?php
declare(strict_types=1);
namespace ng6\Controller;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use TYPO3\CMS\Core\Http\JsonResponse;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Http\HtmlResponse;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;
use Psr\Log\LoggerInterface;
use Psr\Log\LoggerAwareTrait;

require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'Classes/Controller/tx_nG6_db.php');
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'Classes/Controller/class.tx_nG6_utils.php');
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'lib/class.tx_nG6_process.php');
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'Classes/Controller/class.tx_nG6_pi6_purge.php');
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'res/smarty/libs/Smarty.class.php');
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'Classes/vendor/autoload.php');
use phpseclib3\Net\SSH2;
/**
 * Class NG6
 */
class NG6 implements \Psr\Log\LoggerAwareInterface
{
    use LoggerAwareTrait; // use the trait to instantiate the logger
    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function processRequest(ServerRequestInterface $request)
    {
        
        $context = GeneralUtility::makeInstance(Context::class);
        $user_id = $context->getPropertyFromAspect('frontend.user', 'id');

        $type = (string)$request->getQueryParams()['type'] ;
        $this->logger->debug('in processRequest, type = $type'); // log right away
        
        if ($type == 'project') {
            $this->logger->debug('in processRequest, type=project  $project_id'); // log right away
            $project_id = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('object_id'));
            // Select all runs linked to the project
            $project_runs = tx_nG6_db::get_user_project_runs($user_id, $project_id, 'tx_nG6_run.date DESC');
            // for each run
            $html_tree = '';
            foreach($project_runs as $run_id => $run_values) {
                $date = $run_values['date'];
                settype($date,'int');
                $html_tree .= '<li class="jstree-closed" name=\''.str_replace(array('###SPAN_NAME###', '###SPAN_DATE###'), array($run_values['name'], strftime('%d-%m-%y',$date)), trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('run_short_description'))).'\' id=\''.$run_id.'\'> <a href=\'#\'>'.str_replace(array('###SPAN_NAME###', '###SPAN_DATE###', '###SPAN_NB_SEQ###', '###SPAN_SPECIES###'), array($run_values['name'], strftime('%d-%m-%y',$date), $run_values['nb_sequences'], $run_values['species']), trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('run_description'))).'</a></li>';
            }
            // Then all project analysis
            $project_analysis = tx_nG6_db::get_user_project_analysis($user_id, $project_id, 'tx_nG6_analyze.name');
            foreach($project_analysis as $analyse_id => $analyse_values) {
                $html_tree .= '<li name=\''.str_replace(array('###SPAN_NAME###'), array($analyse_values["name"]), trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('analyse_description'))).'\' id=\''.$analyse_id.'\'> <a href=\'#\'>'.str_replace(array('###SPAN_NAME###'), array($analyse_values["name"]), trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('analyse_description'))).'</a></li>';
            }
            
            // Return the html_tree
            return GeneralUtility::makeInstance(HtmlResponse::class, $html_tree);
            
            // If the type of data requested is run
        }
        elseif ($type == 'run'){
            $run_id = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('object_id'));
            // Select all analysis linked to the run and the run information
            $run_analysis = tx_nG6_db::get_user_run_analysis($user_id, $run_id, 'tx_nG6_analyze.name');
            $run_info = tx_nG6_db::select_run($run_id);
            // for each analysis
            $html_tree = '';
            $html_tree .= '<li name=\'run_'.$run_id.'_'.trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP("raw_data_description")).'\'  id=\'data_'.$run_id.'\'> <a href=\'#\'>'.trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP("raw_data_description")).'</a></li>';
            foreach($run_analysis as $analyse_id => $analyse_values) {
                $html_tree .= '<li name=\'run_'.$run_id.'_'.str_replace(array('###SPAN_NAME###'), array($analyse_values["name"]), trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('analyse_description'))).'\' id=\''.$analyse_id.'\'> <a href=\'#\'>'.str_replace(array('###SPAN_NAME###'), array($analyse_values["name"]), trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('analyse_description'))).'</a></li>';
            }
            // Return the html_tree
            return GeneralUtility::makeInstance(HtmlResponse::class, $html_tree);
            
            // If asked to check a job status
        }
        elseif($type == 'check_status') {
            $pid = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('pid'));
            $archive_path = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('archive_path'));
            $process = new Process();
            $process->setPid($pid);
            // If the job is done
            if ($process->status()) {
                return GeneralUtility::makeInstance(HtmlResponse::class,"pid=0&archive_path=".$archive_path);
                // If the job is still runing
            } else {
                return GeneralUtility::makeInstance(HtmlResponse::class,"pid=".$process->getPid()."&archive_path=".$archive_path);
            }
            
            // If asked to hide a {project/run}
        } elseif ($type == 'hide') {
            
            $ids = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('ids'));
            // {project/run}_id to array (1;2;3;...) -> [1;2;3;...]
            $tab_ids = explode(";",$ids);
            $hide_level = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('hide_level'));
            
            if ($hide_level == 'project') {
                foreach($tab_ids as $id => $value) {
                    tx_nG6_db::hide_project($value);
                }
            } elseif ($hide_level == 'run') {
                foreach($tab_ids as $id => $value) {
                    tx_nG6_db::hide_run($value, $user_id);
                }
            } elseif ($hide_level == 'analysis') {
                foreach($tab_ids as $id => $value) {
                    tx_nG6_db::hide_analysis($value, $user_id);
                }
            }
            return GeneralUtility::makeInstance(HtmlResponse::class,1);
            
            // If asked to unhide a {project/run}
        } elseif ($type == 'unhide') {
            
            $ids = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('ids'));
            $this->logger->debug("in processRequest, ids $ids"); // log right away
            // {project/run}_id to array (1;2;3;...) -> [1;2;3;...]
            $tab_ids = explode(";",$ids);
            $unhide_level = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('unhide_level'));
            if ($unhide_level == 'project') {
                foreach($tab_ids as $id => $value) {
                    tx_nG6_db::unhide_project($value);
                }
            } elseif ($unhide_level == 'run') {
                foreach($tab_ids as $id => $value) {
                    tx_nG6_db::unhide_run($value, FALSE);
                }
            } elseif ($unhide_level == 'analysis') {
                foreach($tab_ids as $id => $value) {
                    tx_nG6_db::unhide_analysis($value, FALSE);
                }
            }
            return GeneralUtility::makeInstance(HtmlResponse::class,1);
            // If asked to delete a {project/run/analyze}
        }
        elseif ($type == 'delete') {
            
            $ids = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('ids'));
            // {project/run}_id to array (1;2;3;...) -> [1;2;3;...]
            $tab_ids = explode(";",$ids);
            $delete_level = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('del_level'));
            $data_folder = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('data_folder'));
            $user_login = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('user_login'));
            $user_pwd = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('user_pwd'));
            
            $res = 0;
            // check login and password
            if (isset($user_login) && isset($user_pwd) && $user_login != 'undefined' && $user_pwd != 'undefined') {
                $res = 0;
                $ssh = new SSH2('localhost');
                if (!$ssh->login($user_login, $user_pwd)) {
                    $res = 2;
                }
                
//                 $connection = ssh2_connect('127.0.0.1', 22);
//                 if (!$connection) { $res = 3; }
//                 if (!ssh2_auth_password($connection, $user_login, $user_pwd)) { $res=2;}
            } else {
                $res = 3;
            }
            
            if ($res == 0) {
                if ($delete_level == 'project') {
                    foreach($tab_ids as $id => $value) {
                        $res = tx_nG6_db::delete_project($value, $user_login, $user_pwd, $data_folder);
                        if ($res != 0) { break; }
                    }
                } elseif ($delete_level == 'run') {
                    foreach($tab_ids as $id => $value) {
                        $res = tx_nG6_db::delete_run($value, $user_login, $user_pwd, $data_folder);
                        if ($res != 0) { break; }
                    }
                } elseif ($delete_level == 'analysis') {
                    foreach($tab_ids as $id => $value) {
                        $res = tx_nG6_db::delete_analysis($value, $user_login, $user_pwd, $data_folder);
                        if ($res != 0) { break; }
                    }
                }
            }
            return GeneralUtility::makeInstance(HtmlResponse::class,$res);
            
            // If asked to publish a project
        } elseif ($type == 'publish') {
            
            $ids = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('ids'));
            // project_id to array (1;2;3;...) -> [1;2;3;...]
            $tab_ids = explode(";",$ids);
            foreach($tab_ids as $id => $value) {
                tx_nG6_db::publish_project($value);
            }
            return GeneralUtility::makeInstance(HtmlResponse::class,1);
            // If asked to unpublish a project
        } elseif ($type == 'unpublish') {
            
            $ids = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('ids'));
            // project_id to array (1;2;3;...) -> [1;2;3;...]
            $tab_ids = explode(";",$ids);
            foreach($tab_ids as $id => $value) {
                tx_nG6_db::unpublish_project($value);
            }
            
            // If asked the size
        } elseif ($type == 'get_size') {
            $ids = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('ids'));
            $view = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('view'));
            $tab_ids = explode(",",$ids);
            $full_size = 0;
            foreach($tab_ids as $id => $value) {
                if ($view == "run") {
                    $full_size += tx_nG6_db::get_run_size($value, true);
                } elseif ($view == "project") {
                    $full_size += tx_nG6_db::get_project_size($value, true);
                }
            }
            return GeneralUtility::makeInstance(HtmlResponse::class,tx_nG6_utils::get_octet_string_representation($full_size));
            
            // data size
        }elseif ($type == 'get_data_size'){
            $run_ids = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('run_ids'));
            $data_ids = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('data_ids'));
            $analysis_ids = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('analysis_ids'));
            $full_size = 0;
            
            if ($run_ids){
                $data = explode(',', $run_ids);
                foreach($data as $id => $value) {
                    $full_size += tx_nG6_db::get_run_size($value, true);
                }
            }
            if ($data_ids){
                $data = explode(',', $data_ids);
                foreach($data as $id => $value) {
                    $full_size += tx_nG6_db::get_run_size($value, false);
                }
            }
            if ($analysis_ids){
                $data = explode(',', $analysis_ids);
                foreach($data as $id => $value) {
                    $full_size += tx_nG6_db::get_analysis_size($value);
                }
            }
            return GeneralUtility::makeInstance(HtmlResponse::class,$full_size . ":::" . tx_nG6_utils::get_octet_string_representation($full_size));
            
            // If update a field
        } elseif ($type == 'update_db_field') {
            
            //Retrieve infomation
            $table = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('table'));
            $id     = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('pk'));
            $field  = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('field'));
            $value  = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('value'));
            
            //If change concernes a date
            if( $field == "date" || $field == "crdate" ) {
                //Change date format
                $date = explode('-', $value);
                $timestamp = mktime (0, 0, 1, intval($date[1]), intval($date[2]), intval($date[0]));
                //Update the entity
                tx_nG6_db::update_field($table, $id, $field, $timestamp);
            }
            else {
                //Update the entity
                tx_nG6_db::update_field($table, $id, $field, $value);
            }
            return GeneralUtility::makeInstance(HtmlResponse::class,$value);
            
        }
        // add comment
        elseif ($type == 'add_comment') {
            $view 			= trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('view'));
            $view_id    	= trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('view_id')); // project_id or run_id or analyze_id
            $new_comment  	= trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('new_comment'));
            $cruser_id		= trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('cruser_id'));
            $res = 0 ;
            
            if ($new_comment){
                if($view == 'project'){
                    $res = tx_nG6_db::add_project_comment($view_id, $cruser_id, $new_comment) ;
                }
                elseif ($view == 'run') {
                    $res = tx_nG6_db::add_run_comment($view_id, $cruser_id, $new_comment) ;
                }
                elseif ($view == 'analyze') {
                    $res = tx_nG6_db::add_analyze_comment($view_id, $cruser_id, $new_comment) ;
                }
            }
            
            return GeneralUtility::makeInstance(HtmlResponse::class,$res);
        }
        //delete comment
        elseif ($type == 'delete_comment') {
            $view 			= trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('view'));
            $view_id    	= trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('view_id')); // project_id or run_id or analyze_id
            $comment_id		= trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('comment_id'));
            $res = 0 ;
            if($view == 'project'){
                $res = tx_nG6_db::delete_project_comment($view_id, $comment_id);
            }
            elseif ($view == 'run') {
                $res = tx_nG6_db::delete_run_comment($view_id, $comment_id);
            }
            elseif ($view == 'analyze') {
                $res = tx_nG6_db::delete_analyze_comment($view_id, $comment_id);
            }
            return GeneralUtility::makeInstance(HtmlResponse::class,$res);
        }
        //update comment
        elseif ($type == 'update_comment') {
            $comment_id		= trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('comment_id'));
            $new_comment  	= trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('new_comment'));
            $res = 0;
            $new_comment = urldecode($new_comment);
            $res = tx_nG6_db::update_comment($comment_id, $new_comment);
            return GeneralUtility::makeInstance(HtmlResponse::class,$res);
            
            // If asked the krona page
        } elseif ($type == 'get_krona') {
            
            $data_folder = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('data_folder'));
            $file_name = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('file_name'));
            $sequence_name = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('sequence_name'));
            
            $doc = new DomDocument();
            $data_root = $doc->createElement('data');
            $doc->appendChild($data_root);
            $attr_data_root = $doc->createAttribute("xml:id");
            $data_root->appendChild($attr_data_root);
            
            $t_data_root = $doc->createTextNode('data_root');
            $attr_data_root->appendChild($t_data_root);
            $data_root->setIdAttribute("xml:id", true);
            
            $magnitude = $doc->createElement('magnitude');
            $magnitude->setAttribute("attribute", "g");
            $magnitude = $data_root->appendChild($magnitude);
            $magnitudet = $doc->createTextNode('');
            $magnitudet = $magnitude->appendChild($magnitudet);
            
            $color = $doc->createElement('color');
            $color = $data_root->appendChild($color);
            $colort = $doc->createTextNode('');
            $colort = $color->appendChild($colort);
            
            $attributes = $doc->createElement('attributes');
            $attributes->setAttribute("g", $sequence_name);
            $attributes = $data_root->appendChild($attributes);
            $attributest = $doc->createTextNode('');
            $attributest = $attributes->appendChild($attributest);
            
            $datasets = $doc->createElement('datasets');
            $datasets->setAttribute("names", $file_name);
            $datasets = $data_root->appendChild($datasets);
            $datasetst = $doc->createTextNode('');
            $datasetst = $datasets->appendChild($datasetst);
            
            $file_path = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('file_path'));
            $file_path = $data_folder."/".$file_path;
            
            // If the file exists
            $total = 0;
            if (file_exists($file_path)) {
                $lines = file($file_path);
                // Loop through our array
                foreach ($lines as $line_num => $line) {
                    if ($line != "") {
                        $mparts = explode("\t", trim($line));
                        if ($mparts[0] != "taxlevel" and $mparts[0] != "0") {
                            // If this guy doesnt exist yet
                            $id_searched = str_replace(".", "_", "node_".$mparts[1]);
                            if (is_null($doc->getElementById($id_searched))) {
                                $parentl = implode("_", array_slice(explode(".", $mparts[1]), 0, -1));
                                // If no parent yet, this is the root
                                if (is_null($doc->getElementById("node_".$parentl))) {
                                    $current_node = $doc->createElement('node');
                                    $current_node->setAttribute("name", $mparts[2]);
                                    $current_node->setAttribute("g", $mparts[4]);
                                    $id = $doc->getElementById("data_root");
                                    $id->appendChild($current_node);
                                    $attr_current_node = $doc->createAttribute("xml:id");
                                    $current_node->appendChild($attr_current_node);
                                    $t_current_node = $doc->createTextNode("node_".str_replace(".", "_", $mparts[1]));
                                    $attr_current_node->appendChild($t_current_node);
                                    $current_node->setIdAttribute("xml:id", true);
                                    $current_nodet = $doc->createTextNode('');
                                    $current_nodet = $current_node->appendChild($current_nodet);
                                } else {
                                    $current_node = $doc->createElement('node');
                                    $current_node->setAttribute("name", $mparts[2]);
                                    $current_node->setAttribute("g", $mparts[4]);
                                    $id = $doc->getElementById("node_".$parentl);
                                    $id->appendChild($current_node);
                                    $attr_current_node = $doc->createAttribute("xml:id");
                                    $current_node->appendChild($attr_current_node);
                                    $t_current_node = $doc->createTextNode("node_".str_replace(".", "_", $mparts[1]));
                                    $attr_current_node->appendChild($t_current_node);
                                    $current_node->setIdAttribute("xml:id", true);
                                    $current_nodet = $doc->createTextNode('');
                                    $current_nodet = $current_node->appendChild($current_nodet);
                                }
                            }
                        }
                        if ($mparts[0] == "1") {
                            $total += (int)$mparts[4];
                        }
                    }
                }
            }
            
            $color->setAttribute("attribute", "g");
            $color->setAttribute("valueStart", "0");
            $color->setAttribute("valueEnd", (string)$total);
            $color->setAttribute("hueStart", "120");
            $color->setAttribute("hueEnd", "360");
            
            $xml_string = $doc->saveXML();
            $xml_parts = explode("\n", $xml_string);
            $val = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
            $val .= '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">';
            $val .= '<head>';
            $val .= '</head>';
            $val .= '<body>';
            $val .= '<div id="options" style="position:absolute;left:0;top:0"></div>';
            $val .= '<div id="details" style="position:absolute;top:1%;right:2%;text-align:right;"></div>';
            $val .= '<canvas id="canvas" width="100%" height="100%">This browser does not support HTML5</canvas><div>';
            $val .= '<img id="hiddenImage" src="http://krona.sourceforge.net/img/hidden.png" visibility="hide"/>';
            $val .= '<script name="tree" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('nG6')).'res/js/krona-1.1.js"></script>';
            $val .= $xml_parts[1];
            $val .= '</div>';
            $val .= '</body>';
            $val .= '</html>';
            return GeneralUtility::makeInstance(HtmlResponse::class,$val);
            
        } elseif ($type == 'delete_users') {
            
            $ids = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('ids'));
            // users_id to array (1;2;3;...) -> [1;2;3;...]
            $tab_ids = explode(";",$ids);
            $project_id = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('project_id');
            
            // try to delete users in project ...
            if($project_id != "") {
                $none_access = "";
                foreach($tab_ids as $id => $del_user_id) {
                    tx_nG6_db::delete_access_to_project($del_user_id, $project_id);
                    
                    // find if user can't access to any project
                    $users_access = tx_nG6_db::count_number_of_project_access($del_user_id);
                    if($users_access == '0'){
                        $none_access .= $del_user_id.";";
                    }
                }
                
                if($none_access == ""){		// everybody has at least one access to a project
                    $others_access = '0';
                }else{						// list separate by coma of users who can't access to any project
                    $others_access = substr($none_access, 0, -1);
                }
                
                return GeneralUtility::makeInstance(HtmlResponse::class,$others_access);
                
                // delete users of DB...
            }else{
                
                foreach($tab_ids as $k_id => $v_id){
                    // delete group
                    $gid = tx_nG6_db::get_user_usergroup($v_id);
                    // delete user
                    tx_nG6_db::delete_user($v_id);
                    if(tx_nG6_db::count_users_in_group($gid) == 0){
                        tx_nG6_db::delete_group($gid);
                    }
                }
                
            }
            return GeneralUtility::makeInstance(HtmlResponse::class,1);
            // to change users' right on project
        } elseif ($type == 'change_right') {
            
            $project_id = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('project_id'));
            $c_user_id = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('c_user_id'));
            $right_id = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('right_id'));
            
            tx_nG6_db::change_access_to_project($c_user_id, $project_id, $right_id);
            
            // jquery autocomplete
        } elseif ($type == 'autocomplete') {
            $this->logger->debug('in processRequest, type = $type'); // log right away
            $col = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('gender'));
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('project_id') != null){
                $project_id = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('project_id'));
            }
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('name_start') != null){
                $name_start = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('name_start'));
            }
           
            
            // user search
            if($col != 'title' && $col != 'location' && $col != 'organism'){
                $this->logger->debug('in processRequest, in if, before get_all_names_starting_with'); 
                $res = tx_nG6_db::get_all_names_starting_with($name_start, $col);
                foreach($res as $res_id => $res_val){
                    // find right on project
                    $res[$res_id]['right_id'] = tx_nG6_db::get_user_right_on_project($res_val['uid'], $project_id);
                    
                    // find creator username
                    $cruser_infos = tx_nG6_db::get_user_informations($res[$res_id]['cruser_id']);
                    $res[$res_id]['cruser_username'] = $cruser_infos['username'];
                }
                
                // group search
            }else{
                $this->logger->debug('in processRequest, in else, before get_all_names_starting_with'); 
                if ($col == "location" || $col == "organism") { $col = "tx_nG6_".$col; }
                $res = tx_nG6_db::get_all_starts_with_in_group($name_start, $col);
            }
            return GeneralUtility::makeInstance(JsonResponse::class,$res);
            
            // add a new member on project
        } elseif ($type == 'add_user') {
            $user_name	= trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('username'));
            $project_id = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('project_id'));
            $right_id	= trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('right'));
            $group_name	= trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('title'));
            $location	= trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('location'));
            $organism	= trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('organism'));
            $email = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('email'));
            $first_name = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('first_name'));
            $last_name = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('last_name'));
            $password = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('password'));
            $cruser_id = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('creator'));
            $from_email = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('from_email'));
            $envelope_sender_address = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('envelope_sender_address'));
            $pid = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('pid'));
            $project_url = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('project_url'));
            $send_an_email = filter_var(trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('send_an_email')), FILTER_VALIDATE_BOOLEAN);
            
            
            // if group not exists, create group
            $group_id = tx_nG6_db::get_group_id($group_name);
            if( !isset($group_id) ){
                
                $cruser_id = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('creator'));
                tx_nG6_db::create_new_group($cruser_id, $group_name, $organism, $location);
                $group_id = tx_nG6_db::get_group_id($group_name);
                
            }
            
            $user = tx_nG6_db::select_user_by_username($user_name);
            
            // if user does not exists in DB
            if(!isset($user)){
                
                $create_user_email = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('create_user_email'));
                $create_user_title = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('create_user_title'));
                
                $res_code = tx_nG6_db::add_user($user_name, $first_name, $last_name, $email, tx_nG6_utils::hash_password($password), $cruser_id, $group_id, $pid);
                
                if ($res_code == 0) {
                    // find the new user id
                    $user = tx_nG6_db::select_user_by_username($user_name);
                    
                    // authorize new access
                    tx_nG6_db::add_access_to_project($user["uid"], $project_id, $right_id);
                    
                    if ($send_an_email) {
                        $configurationManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Configuration\\BackendConfigurationManager');
                        //$configurationManager->currentPageId = 1;
                        $extbaseFrameworkConfiguration = $configurationManager->getTypoScriptSetup();
                        $reinitPasswordURL = $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi6.']["reinit_password_url"];
                        $reinit_password_message = "\nYou can visit the following address to (re)init your password, linked to $email : ".$reinitPasswordURL.".";
                        // send an email to the new user
                        $message = str_replace("###USER_FIRST_NAME###", $first_name, $create_user_email);
                        $message = str_replace("###USER_LAST_NAME###", $last_name, $message);
                        $message = str_replace("###USER_LOGIN###", $user_name, $message);
                        $message = str_replace("###PROJECT_LINK###", $project_url, $message);
                        $message = str_replace("###REINIT_PASSWORD###", $reinit_password_message, $message);
                        $msg_table = explode('\n', $message);
                        $message = implode("\n", $msg_table);
                        
                        mail($email, $create_user_title, $message, "From: <".$from_email.">",'-f '.$envelope_sender_address);
                    }
                    return GeneralUtility::makeInstance(HtmlResponse::class,2); // ok
                } elseif ($res_code == 1) {
                    return GeneralUtility::makeInstance(HtmlResponse::class,4); // username exists
                } elseif ($res_code == 2) {
                    return GeneralUtility::makeInstance(HtmlResponse::class,5); // email exists
                }
                
                // user exists in DB
            } elseif ($user["username"] != $user_name || $user["email"] != $email || $user["first_name"] != $first_name || $user["last_name"] != $last_name) {
                // there is difference between information provided and the user account
                return GeneralUtility::makeInstance(HtmlResponse::class,3);
            } else {
                // can user access to the project ?
                if(tx_nG6_db::get_user_right_on_project($user["uid"], $project_id) == -1){
                    
                    // authorize new access
                    tx_nG6_db::add_access_to_project($user["uid"], $project_id, $right_id);
                    
                    // send an email to the  user
                    if ($email != "" && $send_an_email) {
                        $configurationManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Configuration\\BackendConfigurationManager');
                        //$configurationManager->currentPageId = 1;
                        $extbaseFrameworkConfiguration = $configurationManager->getTypoScriptSetup();
                        $reinitPasswordURL = $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi6.']["reinit_password_url"];
                        $reinit_password_message = "\nYou can visit the following address to (re)init your password, linked to $email : ".$reinitPasswordURL.".";
                        $add_user_email = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('add_user_email'));
                        $add_user_title = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('add_user_title'));
                        $project = tx_nG6_db::select_project($project_id);
                        $message = str_replace("###USER_FIRST_NAME###", $first_name, $add_user_email);
                        $message = str_replace("###USER_LAST_NAME###", $last_name, $message);
                        $message = str_replace("###PROJECT_NAME###", $project["name"], $message);
                        $message = str_replace("###PROJECT_DESCRIPTION###", $project["description"], $message);
                        $message = str_replace("###PROJECT_LINK###", $project_url, $message);
                        $message = str_replace("###REINIT_PASSWORD###", $reinit_password_message, $message);
                        $msg_table = explode('\n', $message);
                        $message = implode("\n", $msg_table);
                        mail($email, $add_user_title, $message, "From: <".$from_email.">",'-f '.$envelope_sender_address);
                    }
                    return GeneralUtility::makeInstance(HtmlResponse::class,0);
                    
                } else {
                    return GeneralUtility::makeInstance(HtmlResponse::class,1);	// already access to the project
                }
            }
            
        }
        elseif ($type == 'check_ssh_user'){
            $user_login = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('user_login'));
            $user_pwd = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('user_pwd'));
            $project_id = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('project_id'));
            
            $path_to_check = tx_nG6_utils::get_project_data_save_dir($project_id);
            
            $res = 3;
            // check login and password
            if (isset($user_login) && isset($user_pwd) && $user_login != 'undefined' && $user_pwd != 'undefined') {
                $ssh_command = 'touch '.$path_to_check.'/run/test ; rm -f '.$path_to_check.'/run/test' ;
//                 $connection = ssh2_connect('127.0.0.1', 22);
                $ssh = new SSH2('localhost');
                if (!$ssh->login($user_login, $user_pwd)) {
                    $res = 2;
//                 }
//                 if (!$connection) {
//                     //1st test, basic SSH connection
//                     $res = 3;
//                 } elseif( !ssh2_auth_password($connection, $user_login,$user_pwd ) ){
//                     //2nd test, SSH connection with user + pwd
//                     $res = 2;
                }else{
                    $ssh->enableQuietMode();
                    $outputStream = $ssh->exec($ssh_command);
                    $errorStream = $ssh->getStdError();
                    if($errorStream != "") {
                        $res = 4;
                    }else{
                        $res = 0;
                    }
                    //3rd test, excuting the SSH command
//                     $stream = ssh2_exec($connection, $ssh_command );
//                     $errorStream = ssh2_fetch_stream($stream, SSH2_STREAM_STDERR);
//                     // Enable blocking for both streams
//                     stream_set_blocking($errorStream, true);
//                     stream_set_blocking($stream, true);
                    
//                     if(stream_get_contents($errorStream)) {
//                         // Close the streams
//                         fclose($errorStream);
//                         fclose($stream);
//                         $res = 4;
//                     }else{
//                         fclose($errorStream);
//                         fclose($stream);
//                         $res = 0;
//                     }
                }
                
            } else {
                //The user and/or password is missing
                $res = 3;
            }
            return GeneralUtility::makeInstance(HtmlResponse::class,$res);
        }
        elseif ( $type == 'add_to_ng6_admin'){
            $userid = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('userid'));
            tx_nG6_db::add_to_ng6_admin($userid);
            return GeneralUtility::makeInstance(HtmlResponse::class,1);
        }
        elseif ( $type == 'delete_from_ng6_admin'){
            $userids = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('userids'));
            $userids = explode(',', $userids);
            tx_nG6_db::remove_from_ng6_admin($userids);
            return GeneralUtility::makeInstance(HtmlResponse::class,1);
        }
        elseif ( $type == 'add_to_ng6_superadmin'){
            $userid = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('userid'));
            tx_nG6_db::add_to_ng6_superadmin($userid);
            return GeneralUtility::makeInstance(HtmlResponse::class,1);
        }
        elseif ( $type == 'delete_from_ng6_superadmin'){
            $userids = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('userids'));
            $userids = explode(',', $userids);
            tx_nG6_db::remove_from_ng6_superadmin($userids);
            return GeneralUtility::makeInstance(HtmlResponse::class,1);
        }
        elseif($type == 'update_user'){
            $email = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('email'));
            $first_name = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('first_name'));
            $last_name = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('last_name'));
            $group_name	= trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('title'));
            $location	= trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('location'));
            $organism	= trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('organism'));
            $user_id = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('user_id'));
            $password = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('password');
            if($password){
                $password = trim($password);
            }
           
            $cruser_id = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('creator'));
            $send_an_email = filter_var(trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('send_an_email')), FILTER_VALIDATE_BOOLEAN);
            $from_email = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('from_email'));
            $envelope_sender_address = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('envelope_sender_address'));
            
            $updated = tx_nG6_db::update_user($user_id, $first_name, $last_name, $email, $group_name, $organism, $location) ;
            
            $nb_updated = count($updated);
            
            if ( $nb_updated > 0 && $send_an_email){
                $user_infos = tx_nG6_db::get_user_informations($user_id);
                $email = $user_infos['email'];
                $first_name =  $user_infos['first_name'];
                $last_name =  $user_infos['last_name'];
                $username = $user_infos['username'];
                
                $mail_title = "[NG6]Your user informations have been updated.";
                $mail_content = "Dear " . $first_name . " " . $last_name . " (user name : ".$username."),\n\nSome of your personnal informations have been updated : \n\n";
                if ($email != ''){
                    foreach($updated as $key => $value){
                        
                        if($key == 'password'){
                            $mail_content .= 'password' . "\t: " . $password . "\n" ;
                        }
                        elseif ($key == 'usergroup'){
                            $mail_content .= 'title' . "\t: " . $group_name . "\n" ;
                            $mail_content .= 'location' . "\t: " . $location . "\n" ;
                            $mail_content .= 'organism' . "\t: " . $organism . "\n" ;
                        }
                        else {
                            $mail_content .= ucwords(str_replace("_", " ", $key)) . "\t: " . $value . "\n" ;
                        }
                    }
                   
                    
                    $configurationManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Configuration\\BackendConfigurationManager');
                    //$configurationManager->currentPageId = 1;
                    $extbaseFrameworkConfiguration = $configurationManager->getTypoScriptSetup();
                    $reinitPasswordURL = $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi6.']["reinit_password_url"];
                    $mail_content .= "\nYou can visit the following address to update your password : ".$reinitPasswordURL.".";
                    mail($email, $mail_title, $mail_content, "From: <".$from_email.">",'-f '.$envelope_sender_address);
                }
            }
            
            return GeneralUtility::makeInstance(HtmlResponse::class,$nb_updated);
        }
        elseif ($type == 'get_user_group'){
            $id = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('user_id'));
            return GeneralUtility::makeInstance(JsonResponse::class,tx_nG6_db::get_group_informations(tx_nG6_db::get_user_usergroup($id)));
        }
        elseif($type == 'runs_table') {
            $smarty = new Smarty();
            $smarty->setTemplateDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/pi1');
            $smarty->setCompileDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/templates_c');
            $smarty->setCacheDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/cache');
            $smarty->setConfigDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/configs');
            $smarty->security = true;
            $smarty->security_settings['MODIFIER_FUNCS'] = array('count');
            $project_id = intVal(trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('project_id')));
            $user_id = intVal(trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('user_id')));
            $login_user = intVal(trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('login_user')));
            $page_id = intVal(trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('page_id')));
            $project_runs = tx_nG6_db::get_project_runs($project_id);
            $is_project_admin = tx_nG6_db::is_administrator($user_id, 'project', $project_id);
            $is_at_least_admin_of_1_run = false;
            foreach($project_runs as $run_id => $run_values) {
                $is_admin = tx_nG6_db::is_administrator($user_id, 'run', $run_values['id']);
                $project_runs[$run_id]['is_admin'] = $is_admin;
                if ($is_admin) { $is_at_least_admin_of_1_run = true; }
                $href = '<a href="index.php?id='.$page_id.'&tx_nG6_pi1[run_id]='.$run_values['id'].'&tx_nG6_pi1[project_id]='.$run_values['project_id'].'">'.$run_values['name'].'</a>';
                $project_runs[$run_id]['href'] = $href;
            }
            $smarty->assign('runs', $project_runs);
            $smarty->assign('is_project_admin', $is_project_admin);
            $smarty->assign('is_at_least_admin_of_1_run', $is_at_least_admin_of_1_run);
            $smarty->assign('login_user', $login_user);
            return GeneralUtility::makeInstance(HtmlResponse::class,$smarty->fetch('run_table.tpl'));
            
        }
        elseif($type == 'projects_table') {
            $smarty = new Smarty();
            $smarty->setTemplateDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/pi1');
            $smarty->setCompileDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/templates_c');
            $smarty->setCacheDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/cache');
            $smarty->setConfigDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/configs');
            $smarty->security = true;
            $smarty->security_settings['MODIFIER_FUNCS'] = array('count');
            $user_id = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('user_id'));
            $login_user = intVal(trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('login_user')));
            $page_id = intVal(trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('page_id')));
            $projects = tx_nG6_db::select_all_user_projects($user_id, 'tx_nG6_project_run.uid DESC');
            //$projects = tx_nG6_db::select_all_user_projects($user_id, 'tx_nG6_project.name');
            $is_at_least_admin_of_1_project = false;
            foreach($projects as $project_id => $project_values) {
                $is_admin = tx_nG6_db::is_project_administrator($user_id, $project_values['id']);
                if ($is_admin) { $is_at_least_admin_of_1_project = true; }
                $projects[$project_id]['is_admin'] = $is_admin;
                $projects[$project_id]['is_manager'] = tx_nG6_db::is_project_manager($user_id, $project_values['id']);
                $projects[$project_id]['is_member'] = tx_nG6_db::is_project_member($user_id, $project_values['id']);
                $href = '<a href="index.php?id='.$page_id.'&tx_nG6_pi1[project_id]='.$project_values['id'].'">'.$project_values['name'].'</a>';
                $projects[$project_id]['href'] = $href;
            }
            $smarty->assign('projects', $projects);
            $smarty->assign('is_at_least_admin_of_1_project', $is_at_least_admin_of_1_project);
            $smarty->assign('login_user', $login_user);
            $smarty->assign('is_ng6_admin', tx_nG6_db::is_ng6_administrator($user_id) ? true : false);
            return GeneralUtility::makeInstance(HtmlResponse::class,$smarty->fetch('project_table.tpl'));
            
        } elseif($type == 'analyses_table') {
            $smarty = new Smarty();
            $smarty->setTemplateDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/pi1');
            $smarty->setCompileDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/templates_c');
            $smarty->setCacheDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/cache');
            $smarty->setConfigDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/configs');
            $smarty->security = true;
            $smarty->security_settings['MODIFIER_FUNCS'] = array('count');
            $user_id = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('user_id'));
            $login_user = intVal(trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('login_user')));
            $page_id = intVal(trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('page_id')));
            $project_id = intVal(trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('project_id')));
            $run_id = intVal(trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('run_id')));
            if ($run_id != "") {
                $analysis = tx_nG6_db::get_run_analysis($run_id);
            } else {
                $analysis = tx_nG6_db::get_project_analysis($project_id);
            }
            // Add some information to the table
            foreach($analysis as $analysis_id => $analysis_values) {
                if (tx_nG6_db::is_administrator($user_id, 'analyze', $analysis_values['id'])) { $is_admin = true; }
                $analysis[$analysis_id]['is_admin'] = tx_nG6_db::is_administrator($user_id, 'analyze', $analysis_values['id']);
                if ($run_id != "") {
                    $href = '<a href="index.php?id='.$page_id.'&tx_nG6_pi1[project_id]='.$project_id.'&tx_nG6_pi1[run_id]='.$run_id.'&tx_nG6_pi1[analyze_id]='.$analysis_values['id'].'">'.$analysis_values['name'].'</a>';
                } else {
                    $href = '<a href="index.php?id='.$page_id.'&tx_nG6_pi1[project_id]='.$project_id.'&tx_nG6_pi1[analyze_id]='.$analysis_values['id'].'">'.$analysis_values['name'].'</a>';
                }
                $analysis[$analysis_id]['href'] = $href;
            }
            $smarty->assign('display_analysis_result', $is_admin);
            $smarty->assign('is_admin', $is_admin);
            $smarty->assign('h_analysis', tx_nG6_utils::trace_hierarchy($analysis));
            $smarty->assign('login_user', $login_user);
            return GeneralUtility::makeInstance(HtmlResponse::class,$smarty->fetch('analysis_table.tpl'));
        } elseif($type == 'project_data_repartition') {
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('by') != null){
                $by = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('by'));
            }
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('role') != null){
                $role = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('role'));
            }
            return GeneralUtility::makeInstance(JsonResponse::class,tx_nG6_db::select_projects_repartition($role, $by));
        } elseif($type == 'project_distribution') {
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('values') != null){
                $values = explode(",", trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('values')));
            }
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('by') != null){
                $by = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('by'));
            }
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('role') != null){
                $role = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('role'));
            }
            return GeneralUtility::makeInstance(JsonResponse::class,tx_nG6_db::select_projects_distribution($values, $by, $role));
        } elseif($type == 'project_evolution') {
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('values') != null){
                $values = explode(",", trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('values')));
            }
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('by') != null){
                $by = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('by'));
            }
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('role') != null){
                $role = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('role'));
            }
            $cumulate = false;
            if(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('cumulate') != null && trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('cumulate')) == '1'){
                $cumulate = true;
            }
            return GeneralUtility::makeInstance(JsonResponse::class,tx_nG6_db::select_projects_evolution($values, $by, $role, $cumulate));
            
        } elseif($type == 'data_distribution'){
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('values') != null){
                $values = explode(",", trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('values')));
            }
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('by') != null){
                $by = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('by'));
            }
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('role') != null){
                $role = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('role'));
            }
            $get_analyzes = false;
            if(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('get_analyzes') != null && trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('get_analyzes')) == '1'){
                $get_analyzes = true;
            }
            $octet = true;
            if(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('bases') != null && trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('bases')) == '1'){
                $octet = false;
            }
            return GeneralUtility::makeInstance(JsonResponse::class,tx_nG6_db::select_storage_distribution($values, $by, $role, $get_analyzes, $octet));
            
        } elseif($type == 'data_evolution'){
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('values') != null){
                $values = explode(",", trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('values')));
            }
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('by') != null){
                $by = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('by'));
            }
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('role') != null){
                $role = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('role'));
            }
            $get_analyzes = false;
            if(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('get_analyzes') != null && trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('get_analyzes')) == '1'){
                $get_analyzes = true;
            }
            $octet = true;
            if(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('bases') != null && trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('bases')) == '1'){
                $octet = false;
            }
            $cumulate = false;
            if(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('cumulate') != null && trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('cumulate')) == '1'){
                $cumulate = true;
            }
            return GeneralUtility::makeInstance(JsonResponse::class,tx_nG6_db::select_storage_evolution($values, $by, $role, $get_analyzes, $octet, $cumulate));
            
        } elseif ($type == 'purge_managment_mail') {
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('project') != null){
                $all_projects = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('project'));
            }
            $with_extension = 1;
            tx_nG6_pi6_purge::send_purge_demand_mail($all_projects,$with_extension);
            return GeneralUtility::makeInstance(HtmlResponse::class,1);
            
        } elseif ($type == 'purge_managment_mail_without_extension') {
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('project') != null){
                $all_projects = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('project'));
            }
            $with_extension = 0;
            tx_nG6_pi6_purge::send_purge_demand_mail($all_projects,$with_extension);
            return GeneralUtility::makeInstance(HtmlResponse::class,1);
            
        } elseif ($type == 'purge_managment_resend_mail') {
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('purge_demand') != null){
                $purge_ids = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('purge_demand'));
            }
            tx_nG6_pi6_purge::resend_purge_demand_mail($purge_ids);
            return GeneralUtility::makeInstance(HtmlResponse::class,1);
        } elseif ($type == 'refresh_purge_demand') {
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('purge_delay') != null){
                $purge_delay = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('purge_delay'));
            }
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('purge_demand_filter_space') != null){
                $purge_demand_filter_space = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('purge_demand_filter_space'));
            }
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('purge_demand_filter_delay_exceeded') != null){
                $purge_demand_filter_delay_exceeded = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('purge_demand_filter_delay_exceeded'));
            }
            
            return GeneralUtility::makeInstance(JsonResponse::class,tx_nG6_db::get_purge_demand_list($purge_delay,$purge_demand_filter_space,$purge_demand_filter_delay_exceeded));
        }  elseif ($type == 'refresh_purge_list') {
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('max_retention_date') != null){
                $max_retention_date = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('max_retention_date'));
            }
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('filter_space') != null){
                $filter_space = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('filter_space'));
            }
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('without_lab') != null){
                $without_laboratories_id = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('without_lab'));
            }
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('with_lab') != null){
                $with_laboratories_id = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('with_lab'));
            }
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('create_users') != null){
                $create_users = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('create_users'));
            }
            
            return GeneralUtility::makeInstance(JsonResponse::class,tx_nG6_db::filter_list_retention_data_info($filter_space,$max_retention_date,$create_users, $without_laboratories_id,$with_laboratories_id));
        } elseif ($type == 'delete_purge_data') {
            //Delete data link to the list of purge demand
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('purge_demand') != null){
                $purge_ids = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('purge_demand'));
            }
            if(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('data_folder') != null){
                $data_folder = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('data_folder'));
            }
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('user_login') != null){
                $user_login = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('user_login'));
            }
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('user_pwd') != null){
                $user_pwd = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('user_pwd'));
                $this->logger->debug("in processRequest, type = $type, password = $user_pwd " . \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('user_pwd')); // log right away
                
            }
            
            $res = tx_nG6_db::delete_purge_demand($purge_ids,$user_login, $user_pwd, $data_folder);
            return GeneralUtility::makeInstance(HtmlResponse::class,$res);
        } elseif ($type == 'extend_retention_date_from_demand') {
            //Extend the retention date for the
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('purge_demand') != null){
                $purge_ids = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('purge_demand'));
            }
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('date_value') != null){
                $value = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('date_value'));
            }
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('type_extend') != null){
                $type_extend = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('type_extend'));
            }
            //Change date format
            $date = explode('/', $value);
            $timestamp = mktime (0, 0, 1, (int)$date[1], (int)$date[0], (int)$date[2]);
            $this->logger->debug("in processRequest, type = $type, " . tx_nG6_db::extend_retention_date_from_demand(explode(',',$purge_ids),$timestamp,$type_extend)); // log right away
            
            return GeneralUtility::makeInstance(JsonResponse::class,tx_nG6_db::extend_retention_date_from_demand(explode(',',$purge_ids),$timestamp,$type_extend));
        } elseif ($type == 'extend_retention_date_from_project') {
            //Extend the retention date for the
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('projects_id') != null){
                $projects_id_str = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('projects_id'));
            }
            if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('date_value') != null){
                $value = trim(\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('date_value'));
            }
            //Change date format
            $date = explode('/', $value);
            $timestamp = mktime (0, 0, 1, (int)$date[1], (int)$date[0], (int)$date[2]);
            //TODO project
            return GeneralUtility::makeInstance(JsonResponse::class,tx_nG6_db::extend_retention_date_from_project($projects_id_str,$timestamp, TRUE));
        } elseif ($type == 'get_all_project') {
            return GeneralUtility::makeInstance(JsonResponse::class,tx_nG6_db::select_all_projects());
        }	
        

        return GeneralUtility::makeInstance(HtmlResponse::class,1);
    }
    
}