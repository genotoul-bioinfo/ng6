{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

<div id="user_information_dialog" title=""></div>

<input type="hidden" id="user_is_login" value="{$login_user}" />
<input type="hidden" id="user_id" value="{$user_id}" />
<input type="hidden" id="user_name" value="{$user_name}" />
<input type="hidden" id="data_folder" value="{$data_folder}" />
<input type="hidden" id="usergroup" value="{$group_info.uid}" />
<input type="hidden" id="server_url" value="{$server_url}" />
<input type="hidden" id="server_name" value="{$server_name}" />

<input type="hidden" id="purge_delay" value="{$ng6_purge_delay}" />
<div class="sub-content sc-top">	
	<div class="ng6-content-header-left administration">
		<h2>Administration <small> center </small> </h2>
	</div>
	<div class="ng6-content-header-right">
		Monitor running workflows and disk space usage, manage users with administation rights.
	</div>
	<div style="clear:both"></div>
</div>

<div class="sub-content sc-bottom">

	<ul id="myTab" class="nav nav-tabs">
		<li class="active"><a href="#statistics" data-toggle="tab">Statistics</a></li>
		<li><a href="#admin_management" data-toggle="tab">Admin management</a></li>
		<li><a href="#wf_monitoring" data-toggle="tab">Workflows monitoring</a></li>
		<li><a href="#project_data_management" data-toggle="tab">Mail obsolete project</a></li>
                <li><a href="#purge_demand_management" data-toggle="tab">Process mail demands</a></li>
                <li><a href="#all_project_extention" data-toggle="tab">Extend project</a></li>
	</ul>

	<div id="myTabContent" class="tab-content">
	
		<div class="tab-pane fade in active" id="statistics">
		    <div id="stat_menu" class="clearfix">
		        <div>
		            <p>
		                Please use the following options to configure the statistics to display, select an element in the 
		                following table  and use the button to generate the chart.
		            </p>
		        </div>
		        <div class="col-sm-6">
		            <fieldset class="col-sm-12">
                                <h4>1. Which users should be considered ?</h4>
                                        <div class="radio">
                                            <label class="radio">
                                                    <input type="radio" name="users_to_consider" id="select_cruser" value="create_user" checked>
                                                            users who have added data 
                                            </label>
                                        </div>
			            <div class="radio">
				            <label class="radio">
					            <input type="radio" name="users_to_consider" id="select_owner" value="manager">
						            users for whom data were made ​​available
				            </label>
			            </div>
                    </fieldset>
                    
                    <fieldset class="col-sm-12">
                        <h4>2. How should they be grouped ?</h4>
				        <div class="radio">
				            <label class="radio">
					            <input type="radio" name="group_by" id="by_laboratories" value="title" checked>
						            by laboratories
				            </label>
			            </div>
			            <div class="radio">
				            <label class="radio">
					            <input type="radio" name="group_by" id="by_organizations" value="organism">
						            by organization
				            </label>
			            </div>
			            <div class="radio">
				            <label class="radio">
					            <input type="radio" name="group_by" id="by_locations" value="location">
						            by location
				            </label>
			            </div>
                    </fieldset>
		        </div>
		        
		        <div class="col-sm-6">
		            <fieldset class="col-sm-12">
                        <h4>3. How should the data be displayed ?</h4>
				        <div class="radio">
				            <label class="radio">
					            <input type="radio" name="display_by" id="project_distribution" value="project_distribution" checked>
						            display the projects as a pie chart
				            </label>
			            </div>
			            <div class="radio">
				            <label class="radio">
					            <input type="radio" name="display_by" id="project_evolution" value="project_evolution">
						            display the projects creation evolution 
				            </label>
			            </div>
			            <div class="radio">
				            <label class="radio">
					            <input type="radio" name="display_by" id="data_distribution" value="data_distribution">
						            display the stored data as a pie chart 
				            </label>
			            </div>
			            <div class="radio">
				            <label class="radio">
					            <input type="radio" name="display_by" id="data_evolution" value="data_evolution">
						            display the stored data evolution
				            </label>
			            </div>
				        
				        <h5>3.1. Misc</h5>
				
				        <div class="checkbox">
				            <label class="checkbox" style="font-size:12px">
					            <input id="chk_includes_analyses" type="checkbox" disabled>
					            Includes analyses
				            </label>
			            </div>
			            <div class="checkbox">
				            <label class="checkbox" style="font-size:12px">
					            <input id="chk_display_bases" type="checkbox" disabled>
             					Should the size be in number of bases (default is in bytes)
				            </label>
			            </div>
			            <div class="checkbox">
				            <label class="checkbox" style="font-size:12px">
					            <input id="chk_cumulate" type="checkbox" disabled>
             					Cumulate values
				            </label>
			            </div>
                    </fieldset>
		        </div>
                
                
                
		    </div>

			<div id="selection_tables" class="clearfix">
				<h4><strong>Which <span id="by_span">laboratories</span> should be displayed ? <small> non selected ones will be gathered in the "others" group </small></strong></h4>
				
				<div id="wrapper_datatable_laboratories">
			        <table class="table table-striped table-bordered dataTable" id="data_table_laboratories">
			            <thead>
			                <th><center><input type="checkbox" id="chk_all_laboratories"></center></th>
			                <th>Laboratories</th>
			                <th>Organizations</th>
			                <th>Locations</th>
			                <th>Number of project</th>
			            </thead>
			            <tbody>
			                {foreach $distribution key=group_name item=group_values}
			                    <tr>
			                        <td><center><input type="checkbox" class="chk_stat_element"  value="{$group_name}"></center></td>
			                        <td>{$group_name}</td>
								    <td>{$group_values.organism}</td>
								    <td>{$group_values.location}</td>
								    <td>{$group_values.count}</td>
			                    </tr>
			                {/foreach}
			            </tbody>
			        </table>
			    </div>
			    
			    <div id="wrapper_datatable_organizations" style="display:none">
			        <table class="table table-striped table-bordered dataTable" id="data_table_organizations">
			            <thead>
			                <th><center><input type="checkbox" id="chk_all_organizations"></center></th>
			                <th>Organizations</th>
			                <th>Number of project</th>
			            </thead>
			            <tbody></tbody>
			        </table>
			    </div>
			    
			    <div id="wrapper_datatable_locations" style="display:none">
			        <table class="table table-striped table-bordered dataTable" id="data_table_locations">
			            <thead>
			                <th><center><input type="checkbox" id="chk_all_locations"></center></th>
			                <th>Locations</th>
			                <th>Number of project</th>
			            </thead>
			            <tbody></tbody>
			        </table>
			    </div>
			    
				<br />
				
				<div class="row">
					<div class="col-sm-offset-10 col-sm-2">
						<button type="button" id="refresh_graph_btn" class="multiple-selection-btn btn btn-primary" disabled> <i class="glyphicon glyphicon-refresh"></i> refresh graph</button>
					</div>
			    </div>
				
			</div>
			
			<br />
			
			<div id="highcharts_graph"> </div>
		</div>
		
		<div class="tab-pane fade" id="admin_management">
		
		    <div id="admin_users">
	            <p>This table allows you to add a user to the list of ng6 administrator</p>
	            <table class="table table-striped table-bordered dataTable" id="users_data_table">
		            <thead>
			            <tr>
				            <th><center></th>
				            <th>Login</th>
				            <th>Last name</th>
				            <th>First name</th>
				            <th>Email</th>
			            </tr>
		            </thead>
		            <tbody> 
			            {foreach from=$ng6_admin_users key=c_user_id item=user_values}
				            {* highlight user personnal line *}
				            {assign var="emphasis" value=""}
				            {if $user_values.id==$user_id}
					            {assign var="emphasis" value="tx-nG6-pi1-line-emphasis"}
				            {/if}
			
				            <tr id="tr_user_{$user_values.id}" class="{$emphasis}">
					
					            {if $user_values.id == $user_id }
						            <td></td>
					            {else}
						            <td><center><input type="checkbox" id="chk_user_{$user_values.id}" value="user_{$user_values.id}" /></center></td>
					            {/if}
					
					            <td>{$user_values.username}</td>
					            <td>{$user_values.last_name}</td>
					            <td>{$user_values.first_name}</td>
					            <td>{$user_values.email}</td>
					
				            </tr>
			            {/foreach}
		            </tbody>
		            <tfoot>
			            <tr>
				            <th align="left" colspan="8">
					            With selection :
					            <div  class="btn-group">
						            <button id="add_admin" type="button" class="btn btn-sm btn-default" ><i class="glyphicon glyphicon-plus"></i> add user</button>
						            <button id="del_admin" type="button" class="btn multipleu-selection-btn btn-sm btn-default"><i class="glyphicon glyphicon-minus"></i> delete user</button>
					            </div>
				            </th>
			            </tr>
		            </tfoot>
	            </table>
            </div>
		
			{if $is_current_user_superadmin}
				<div id="superadmin_users">
					<p></br>This table allows you to manage every ng6 superadmin</p>
	            	<table class="table table-striped table-bordered dataTable" id="superadmin_users_data_table">
						<thead>
			           		<tr>
				           		<th><center></th>
				          		<th>Login</th>
				           		<th>Last name</th>
				           		<th>First name</th>
				           		<th>Email</th>
			           		</tr>
		           		</thead>
						<tbody> 
			           		{foreach from=$ng6_superadmin_users key=c_user_id item=user_values}
				           		{* highlight user personnal line *}
				           		{assign var="emphasis" value=""}
				           		{if $user_values.id==$user_id}
					           		{assign var="emphasis" value="tx-nG6-pi1-line-emphasis"}
				           		{/if}
			
				           		<tr id="tr_user_{$user_values.id}" class="{$emphasis}">
				
				            		{if $user_values.id == $user_id }
					           			<td></td>
				            		{else}
					           			<td><center><input type="checkbox" id="chk_user_{$user_values.id}" value="user_{$user_values.id}" /></center></td>
				            		{/if}
				
				            		<td>{$user_values.username}</td>
				            		<td>{$user_values.last_name}</td>
				            		<td>{$user_values.first_name}</td>
				            		<td>{$user_values.email}</td>
				           		</tr>
			           		{/foreach}
		           		</tbody>
						<tfoot>
			           		<tr>
				           		<th align="left" colspan="8">
				            		With selection :
				            		<div  class="btn-group">
					            		<button id="add_superadmin" type="button" class="btn btn-sm btn-default" ><i class="glyphicon glyphicon-plus"></i> add user</button>
					            		<button id="del_superadmin" type="button" class="btn multipleu-selection-btn btn-sm btn-default"><i class="glyphicon glyphicon-minus"></i> delete user</button>
				            		</div>
				           		</th>
			           		</tr>
		           		</tfoot>
					</table>
				</div>
			{/if}
		</div>
				
		<div class="tab-pane fade" id="wf_monitoring"></div>
		<div class="tab-pane fade" id="project_data_management">
		    <div id="purge_menu" class="clearfix">
		        <div>
		            <p>
		                Please use the following options to retrieve project, run, analyses acces to purge options.
		            </p>
		        </div>
	        
		       <div  class="col-sm-6">
                    <fieldset class="col-sm-12">
                        <h4>1. Filter on space</h4>
				        <div class="text">
				            
				            <select name="space_select_options" id="filter_space" >
				            	<option value="">No filter</option>
				            	{foreach $available_space_ids key=space_id item=space_name}
									<option value="{$space_name}">{$space_name}</option>
                                {/foreach}
				            </select>
					            
			            </div>
                    </fieldset>
		        </div>
		        
		       <div  class="col-sm-6">
                        <fieldset class="col-sm-12">
                        <h4>2. Retention date lower than : </h4>
				        <div class="text">
				            <label class="text">
					            <input id="max_retention_date" class="form-control" type="text" value="" data-date-format="dd/mm/yyyy" data-provide="datepicker" name="max_retention_date"/>
				            </label>
			            </div>
                        </fieldset>
		        </div>
		         
		        <div  class="col-sm-6">
                                <fieldset class="col-sm-12">
                                <h4>3. Which create user ?</h4>
                                               <div id="wrapper_datatable_create">
                                        <table class="table table-striped table-bordered dataTable" id="data_table_users">
                                            <thead>
                                                <th></th>
                                                <th>Create user</th>
                                            </thead>
                                            <tbody>
                                                {foreach $ng6_admin_users key=user_key item=user_values}
                                                    <tr>
                                                        <td><center><input type="checkbox" class="chk_create_user"  value="{$user_values.id}"></center></td>
                                                        <td>{$user_values.username}</td>
                                                    </tr>
                                                {/foreach}
                                            </tbody>
                                        </table>
			         </fieldset>
			    </div>
				
                        <div  class="col-sm-6">
		            <fieldset class="col-sm-12">
                                <h4>4. Laboratories implie: </h4>
				       <div id="wrapper_datatable_laboratories">
			        <table class="table table-striped table-bordered dataTable" id="data_table_select_lab">
			            <thead>
			                <th>With</th>
                                        <th>Without</th>
			                <th>Laboratories</th>
			            </thead>
			            <tbody>
			                {foreach $distribution key=group_name item=group_values}
			                    <tr>
			                        <td><center><input type="checkbox" class="chk_with_lab_element"  name="group_{$group_values.group_id}" value="{$group_values.group_id}"></center></td>
                                                <td><center><input type="checkbox" class="chk_without_lab_element" name="group_{$group_values.group_id}"  value="{$group_values.group_id}"></center></td>
			                        <td>{$group_name}</td>
			                    </tr>
			                {/foreach}
			            </tbody>
			        </table>
			    </div>
                            </fieldset>
                        </div>
                     
                            
                        <div class="row">
                                <div class="col-sm-offset-10 col-sm-2">
                                        <button type="button" id="refresh_obsolete_list_btn" class="multiple-selection-btn btn btn-primary"> <i class="glyphicon glyphicon-refresh"></i> Refresh list</button>
                                </div>
                        </div>
                
                        </div>	
			
			
			<br />
			<div class="tx-nG6-wait" id="data_table_obsolete_wait">Please wait while processing ...</span></div>
			<div class="alert alert-danger" id="data_table_obsolete_error" style="display:none"></div>
			<div id="obsolete_list"> 
			<div>
		            <p>
		                Following table contains runs and analysis which can be purged, please alert project managers with button "send mail" after selecting projects.</br>
                                Then the project will be available for extention or purge in tab "Purge demand management"<br/>
				This table contains <b><span id="nb_purgeable_project"></span> </b> project corresponding to <b><span id="global_purgeable_size"</span></b>.
		            </p>
                        
		        </div>
			<table class="table table-striped table-bordered dataTable" id="data_table_obsolete">
		            <thead>
			            <tr>
				            <th rowspan="2"><center><input type="checkbox" id="all_chk_obsolete"></center></th>
				            <th rowspan="2">Project</th>
				            <th rowspan="2">All Run</th>
				            <th colspan="2"><center>Obsoletes Runs </center></th>
				            <th rowspan="2">All Analyze</th>
				            <th colspan="2"><center>Obsoletes Analyzes</center></th>
				            <th rowspan="2">Total Size Purgeable</th>
				            <th rowspan="2">Project category</th>
				            <th rowspan="2">Project space</th>
				            <th rowspan="2">Users</th>
				        </tr>
				       <tr>
				            <th>Nb Run stored</th>
				            <th>Nb Run extended</th>
				            <th>Nb Analyze stored</th>
				            <th>Nb Analyze extended</th>
			            </tr>
		            </thead>
		            <tbody> 
		            </tbody>
		            <tfoot>
			            <tr>
				            <th align="left" colspan="12">
					            With selection :
                                                        <div  class="btn-group">
                                                            <button id="btn_obsolete_mail" type="button" class="btn btn-sm btn-default" ><i class="glyphicon glyphicon-envelope"></i> Send mail </button>
                                                            <button id="btn_obsolete_mail_without_extension" type="button" class="btn btn-sm btn-default" ><i class="glyphicon glyphicon-envelope"></i> Send mail without extension</button>
					            </div>
				            </th>
			            </tr>
		            </tfoot>
	            </table>
			</div>
		</div>
                
                <div class="tab-pane fade" id="purge_demand_management">
                        <div>
		            <p>
		                Following table contains list of purge demand sent, here you can delete data or extend retention date.</br>
                                Delay exceeded is yes if mail sent more than {$ng6_purge_delay} days, so if you don't have answer from manager you can purge data.
		            </p>
                        
		        </div>
		        
		        <div  class="col-sm-6">
    				<fieldset class="col-sm-12">
        				<h4>1. Filter on space</h4>
						<div class="text">
							<select name="space_select_options" id="purge_demand_filter_space" >
								<option value="">No filter</option>
								{foreach $available_space_ids key=space_id item=space_name}
									<option value="{$space_name}">{$space_name}</option>
								{/foreach}
							</select>		            
        				</div>
    				</fieldset>
				</div>
				
				<div  class="col-sm-6">
                    <fieldset class="col-sm-12">
                        <h4>2. Delay exceeded : </h4>
				        <div class="text">
				            <select name="delay_exceeded_options" id="purge_demand_filter_delay_exceeded" >
								<option value="">No filter</option>
								<option value="yes">Yes</option>
								<option value="no">No</option>
							</select>
			            </div>    
					</fieldset>
		        </div>
		        
		        <div class="row">
                    <div class="col-sm-offset-10 col-sm-2">
                    	<button type="button" id="refresh_purge_demands_list_btn" class="multiple-selection-btn btn btn-primary"> <i class="glyphicon glyphicon-refresh"></i> Refresh list</button>
                	</div>
                </div>
                </br>
		        
                        <div class="tx-nG6-wait" id="data_table_purge_demand_wait">Please wait while processing ...</div>
                        <div class="alert alert-danger" id="purge_demand_error"></div>
                        <div id="purge_demand_list"> 
                            <table class="table table-striped table-bordered dataTable" id="data_table_demand">
                                    <thead>
                                        <tr>
                                            <th><center><input type="checkbox" id="all_chk_demand"></center></th>
                                            <th>Demand number</th>
                                            <th>Project name</th>
                                            <th>Project space</th>
                                            <th>Purgeable size</th>
                                            <th>Demand date</th>
                                            <th>Users</th>
                                            <th class="data-filter">Delay exceeded</th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody> 
                                  
                                    </tbody>
                                    <tfoot>
                                          <tr>
                                                    <th align="left" colspan="8">
                                                            With selection :
                                                            <div  class="btn-group">
                                                                   <button id="btn_demand_resend_mail" type="button" class="btn btn-sm btn-default" ><i class="glyphicon glyphicon-remove"></i> Resend mail</button>
                                                                   <button id="btn_demand_delete_data" type="button" class="btn btn-sm btn-default" ><i class="glyphicon glyphicon-remove"></i> Delete data</button>
                                                                   <button id="btn_demand_retention" type="button" class="btn btn-sm btn-default"><i class="glyphicon glyphicon-pencil"></i> Extend retention date </button> 
                                                            </div>
                                                    </th>
                                            </tr>
                                    </tfoot>
                                    
                            </table>
                        </div> <!--end purge_demand_list-->
                         
                </div> <!--Extend projects-->
                <div class="tab-pane fade" id="all_project_extention">
                        <div>
		            <p>
		                Following table contains all projects.</br>
		            </p>
                        
		        </div>
		        		<input type="hidden" id="tx_nG6_pi6_redirection_page" value="{$tx_nG6_pi6_redirection_page}"/>
                        <div class="tx-nG6-wait" id="all_project_wait">Please wait while processing ...</div>
                        <div class="alert alert-danger" id="all_project_error"></div>
                        <div id="all_project_list"> 
                            <table class="table table-striped table-bordered dataTable" id="data_table_allproject">
                                    <thead>
                                        <tr>
                                            <th><center><input type="checkbox" id="all_chk_project"></center></th>
                                            <th>Project name</th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody> 
                                  
                                    </tbody>
                                    <tfoot>
                                          <tr>
                                                    <th align="left" colspan="2">
                                                        <div  style="float: left;">
                                                            Select extention date :
                                                                   <input id="input_extend_all_project" class="form-control" type="text" value="" data-date-format="dd/mm/yyyy" data-provide="datepicker" name="change_retention_date"/>                                                                      
                                                                   </div>
                                                            <div  class="btn-group" style="float: right;">
                                                                   <button id="btn_extend_all_project" type="button" class="btn btn-sm btn-default"><i class="glyphicon glyphicon-pencil"></i> Extend retention date </button> 
                                                            </div>
                                                    </th>
                                            </tr>
                                    </tfoot>
                                    
                            </table>
                        </div> <!--all_project_list-->
                         
                </div> <!--Extend projects-->
        	</div>
	
        
        
        </div>
        
        
        
        
        
        
        
</div>

{include file='../template/modals.tpl'}
