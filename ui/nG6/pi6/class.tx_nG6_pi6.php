<?php
//namespace ng6\Controller;
/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/Classes/Controller/class.tx_nG6_utils.php');
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/Classes/Controller/tx_nG6_db.php');
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/libs/Smarty.class.php'); 
use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Context\Context;
use nG6\Controller\tx_nG6_db;
use nG6\Controller\tx_nG6_utils;
class tx_nG6_pi6 extends \TYPO3\CMS\Frontend\Plugin\AbstractPlugin {
	var $prefixId = 'tx_nG6_pi6';		// Same as class name
	var $scriptRelPath = 'pi6/class.tx_nG6_pi6.php';	// Path to this script relative to the extension dir.
	var $extKey = 'nG6';	// The extension key.
	
	//var $pi_checkCHash = TRUE;
	
	/**
	 * Main method of your PlugIn
	 *
	 * @param	string		$content: The content of the PlugIn
	 * @param	array		$conf: The PlugIn Configuration
	 * @return	The content that should be displayed on the website
	 */
	function main($content,$conf)	{
	    $context = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(Context::class);
	    $user_id = $context->getPropertyFromAspect('frontend.user', 'id');
	    $user_name = $context->getPropertyFromAspect('frontend.user', 'username');
		if (strstr($this->cObj->currentRecord,'tt_content'))	{
			$conf['pidList'] = $this->cObj->data['pages'];
			$conf['recursive'] = $this->cObj->data['recursive'];
		}
		$this->pi_loadLL();
		$this->conf=$conf;
		$this->pi_setPiVarDefaults();
		//$this->pi_USER_INT_obj=1;

		// Add the jquery libs + the tree plugins and its css
		$GLOBALS['TSFE']->additionalHeaderData[$this->prefixId] = '
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/jquery.min.js"></script>
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/highstock.js"></script>
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/jquery.highcharts.exporting.js"></script>
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/jquery.dataTables.min.js"></script>
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/bootstrap.min.js"></script>
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/dataTables.bootstrap.js"></script>
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/typeahead.bundle.js"></script>
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/jflow.min.js"></script>
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/tx_nG6_utils.js"></script>
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/tx_nG6_pi6.js"></script>
					
			<link type="text/css" rel="stylesheet" media="screen" href="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/css/bootstrap-theme.min.css"/>
			<link type="text/css" rel="stylesheet" media="screen" href="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/css/dataTables.bootstrap.css"/>
			<link type="text/css" rel="stylesheet" media="screen" href="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/css/typeaheadjs.css"/>	
			<link type="text/css" rel="stylesheet" media="screen" href="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/css/font-awesome.css"/>
			<link type="text/css" rel="stylesheet" media="screen" href="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/css/jflow.min.css"/>
			<link type="text/css" rel="stylesheet" media="screen" href="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/css/tx_nG6.css"/>';

		$smarty = new Smarty();
		$smarty->setTemplateDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/pi6');
		$smarty->setCompileDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/templates_c');
		$smarty->setCacheDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/cache');
		$smarty->setConfigDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/configs');
		$smarty->security = true;
		$smarty->security_settings['MODIFIER_FUNCS'] = array('count');

		$group_list = tx_nG6_db::get_group_list();		
		$smarty->assign('login_user', $user_namer);
		$smarty->assign('user_id', $user_id);
		$smarty->assign('user_name', $user_namer);
		$smarty->assign('group_list', $group_list);
		$smarty->assign('data_folder', $this->conf["data"]);
		$smarty->assign('server_url', $this->conf['server_url']);
        $smarty->assign('server_name', $this->conf["server_name"]);
                
		$distribution = tx_nG6_db::select_projects_repartition('create_user', 'title');
		$smarty->assign('distribution', $distribution);
		$smarty->assign('ng6_admin_users', tx_nG6_db::get_ng6_admin_users() );
		
		$smarty->assign('ng6_superadmin_users', tx_nG6_db::get_ng6_superadmin_users() );
		$smarty->assign('is_current_user_superadmin', tx_nG6_db::is_user_ng6_superadmin($user_id) );
		$smarty->assign('available_space_ids', tx_nG6_utils::get_available_space_ids());
        $smarty->assign('tx_nG6_pi6_redirection_page',$this->conf['redirection_page']);
        
		$smarty->assign('ng6_purge_delay',$this->conf['delay_purge']);
		return $smarty->fetch('administration_view.tpl');
	}
        
   
}

if (defined('TYPO3') && $TYPO3_CONF_VARS[TYPO3]['XCLASS']['ext/nG6/pi6/class.tx_nG6_pi6.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3]['XCLASS']['ext/nG6/pi6/class.tx_nG6_pi6.php']);
}

?>
