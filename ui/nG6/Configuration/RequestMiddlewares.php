<?php

return [
    'frontend' => [
        'typo3/cms-frontend/eid' => [
            'disabled' => true
        ],
        'typo3/cms-frontend/eid-new' => [
            'target' => \TYPO3\CMS\Frontend\Middleware\EidHandler::class,
            'after' => [
                'typo3/cms-frontend/tsfe',
            ],
            'before' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ]
        ]
    ]
];