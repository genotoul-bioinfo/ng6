<?php
/**
 * Smarty plugin
 * 
 * @package Smarty
 * @subpackage PluginsModifier
 */

/**
 * NG6 get_description modifier plugin
 * 
 * Type:     modifier<br>
 * Name:     get_description<br>
 * Purpose:  return a mid description from a string 
 * 
 * @link 
 * @author Jerome Mariette <jerome dot mariete at toulouse dot inra dot fr> 
 * @param string $
 * @param hash table $  
 * @return string 
 */
 
/**
 * Return a description for the sample based on a 
 * newbler formated file name
 * 
 * @param	string	the file name to process
 * @param	desc	the sample description table 
 * @return	the best description, if none found the $string itself
 */
function get_newbler_desc($string, $desc) {
	
	$best_desc = array();
	$best_description = $string;
    foreach($desc as $mid => $description) {
		// Mids name can be in between 2 dots
    	if (preg_match("/\.".$mid."\./i", $string)) {
    		$best_desc[$mid] = $description;
    		$best_description = $description;
    	// Or can be rigth at the end
    	} elseif (preg_match("/\.".$mid."$/i", $string)) {
    		$best_desc[$mid] = $description;
    		$best_description = $description;
    	} elseif (preg_match("/\.".$description."\./i", $string)) {
    		$best_desc[$mid] = $description;
    		$best_description = $description;
    	// Or can be rigth at the end
    	} elseif (preg_match("/\.".$description."$/i", $string)) {
    		$best_desc[$mid] = $description;
    		$best_description = $description;
    	}
    }
    // If no mids found in between .[desc].
    if (count($best_desc) == 0) {
    	$best_description = $string;
    // If multple mids matches take the longuest from the $final_descriptions
    } elseif (count($best_desc) > 1) {
    	$max_len = 0;
    	$best_description = $string;
    	foreach($best_desc as $mid => $description) {
    		if (strlen($mid) > $max_len) {
    			$max_len = strlen($mid);
    			$best_description = $description;
    		}
    	}
    }
	return $best_description;
} 

/**
 * Return a description for the sample based on a 
 * casava_1_7 formated file name
 * 
 * @param	string	the file name to process
 * @param	desc	the sample description table 
 * @return	the best description, if none found the $string itself
 */
function get_casava_1_7_desc($string, $desc) {
	$best_description = $string;
    foreach($desc as $mid => $description) {
		// Index must be at begining
    	if (preg_match("/^".$description."_/i", $string)) {
    		// Find the pairend info if there is one
    		if (preg_match("/_1_qseq/i", $string)) {
    			$best_description = $description." (R1)";
    		} elseif (preg_match("/_2_qseq/i", $string)) {
    			$best_description = $description." (R2)";
    		}
    	}
    }
    return $best_description;
}

/**
 * Return a description for the sample based on a 
 * casava_1_8 formated file name
 * 
 * @param	string	the file name to process
 * @param	desc	the sample description table 
 * @return	the best description, if none found the $string itself
 */
function get_casava_1_8_desc($string, $desc) {
	$best_description = $string;
    foreach($desc as $mid => $description) {
		// Index must be at begining
    	if (preg_match("/^".$description."_/i", $string)) {
    		// Find the pairend info if there is one
    		if (preg_match("/_R1/i", $string)) {
    			$best_description = $description." (R1)";
    		} elseif (preg_match("/_R2/i", $string)) {
    			$best_description = $description." (R2)";
    		} elseif (preg_match("/_I1/i", $string)) {
    			$best_description = $description." (I1)";
    		}
    	}
    }
    return $best_description;
}

/**
 * Return a description for the sample based on a 
 * generic illumina formated file name
 * 
 * @param	string	the file name to process
 * @param	desc	the sample description table 
 * @return	the best description, if none found the $string itself
 */
function get_generic_illumina_desc($string, $desc) {
	$best_description = $string;
    foreach($desc as $mid => $description) {
		// Index must be at begining
    	if (preg_match("/".$mid."_/i", $string)) {
    		// Find the pairend info if there is one
    		if (preg_match("/_1_sequence/i", $string)) {
    			$best_description = $description." (R1)";
    		} elseif (preg_match("/_2_sequence/i", $string)) {
    			$best_description = $description." (R2)";
    		}
    	}
    }
    return $best_description;
}

/**
 * Return a description for the sample based on a 
 * file name
 * 
 * @param	string	the file name to process
 * @param	desc	the sample description table 
 * @return	the best description, if none found the $string itself
 */
function get_desc($string, $desc) {
	$best_desc = array();
	$best_description = $string;
    foreach($desc as $mid => $description) {
		// Mids name can be in between 2 dots
    	if (preg_match("/".$mid."/i", $string)) {
    		$best_desc[$mid] = $description;
    		$best_description = $description;
    	}
    }
    // If no mids found
    if (count($best_desc) == 0) {
    	$best_description = $string;
    // If multple mids matches take the longuest from the $final_descriptions
    } elseif (count($best_desc) > 1) {
    	$max_len = 0;
    	$best_description = $string;
    	foreach($best_desc as $mid => $description) {
    		if (strlen($mid) > $max_len) {
    			$max_len = strlen($mid);
    			$best_description = $description;
    		}
    	}
    }
	return $best_description;
}
 
function smarty_modifier_get_description($string, $desc) { 
    
    $best_description = $string;
	// First try to get the newbler description
	$best_description = get_newbler_desc($string, $desc);
	
	// if no description found as newbler
	if ($best_description == $string) {
		$best_description = get_casava_1_7_desc($string, $desc);
	}
	
	// if no description found as casava_1_7
	if ($best_description == $string) {
		$best_description = get_casava_1_8_desc($string, $desc);
	}

	// if no description found as casava_1_8
	if ($best_description == $string) {
		$best_description = get_generic_illumina_desc($string, $desc);
	}
	
	// if no description found based on specific formatting, check in the whole file name
	if ($best_description == $string) {
		$best_description = get_desc($string, $desc);
	}
	
	return $best_description;
} 

?>