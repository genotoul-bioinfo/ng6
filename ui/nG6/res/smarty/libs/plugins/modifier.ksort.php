<?php
/**
 * Smarty plugin
 *
 * @package Smarty
 * @subpackage PluginsModifier
 */

/**
 * Smarty sort a table on key values
 *
 * Type:     modifier<br>
 * Name:     ksort<br>
 * Purpose:  sort a table on key values
 * @author Jerome Mariette
 * @param array $array array to sort
 * @return sorted array
 */
function smarty_modifier_ksort($array) {
	ksort($array);
    return $array;
}

?>