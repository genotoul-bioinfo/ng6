
/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */

$.validator.setDefaults({
    highlight: function (element, errorClass, validClass) {
    	var $element;
		if ( element.type === "radio" ) {
			$element = this.findByName(element.name);
		} else {
			$element = $(element);
		}
        // add the bootstrap error class
        $element.parents("div.form-group").addClass("has-error");
		if ($element.parent("div.input-append").find(".btn").length > 0) {
			$element.parent("div.input-append").find(".btn").addClass("btn-danger");
			$element.parent("div.input-append").find(".glyphicon-wrench").addClass("icon-white");
		}
    },
    unhighlight: function (element, errorClass, validClass) {
        var $element;
        if (element.type === 'radio') {
            $element = this.findByName(element.name);
        } else {
            $element = $(element);
        }
        $element.removeClass(errorClass).addClass(validClass);
        // remove the bootstrap error class
        if ($element.parents("div.form-group").find("." + errorClass).length == 0) {
            // Only remove the class if there are no other errors
            $element.parents("div.form-group").removeClass(errorClass).addClass(validClass);
        }
		if ($element.parent("div.input-append").find(".btn").length > 0) {
			$element.parent("div.input-append").find(".btn").removeClass("btn-danger");
			$element.parent("div.input-append").find(".glyphicon-wrench").removeClass("icon-white");
		}
    },
    errorPlacement: function(error, element) {}
});

jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "date-eu-pre": function ( date ) {
    	var date = date.replace("-", "/");
    	if (date.indexOf("<") != -1) {
    		date = $(date).html().replace(" ", "");
    	} else {
    		date = date.replace(" ", "");
    	}

        if (date.indexOf('.') > 0) {
            /*date a, format dd.mn.(yyyy) ; (year is optional)*/
            var eu_date = date.split('.');
        } else {
            /*date a, format dd/mn/(yyyy) ; (year is optional)*/
            var eu_date = date.split('/');
        }

        /*year (optional)*/
        if (eu_date[2]) {
            var year = eu_date[2];
        } else {
            var year = 0;
        }

        /*month*/
        var month = eu_date[1];
        if (month.length == 1) {
            month = 0+month;
        }

        /*day*/
        var day = eu_date[0];
        if (day.length == 1) {
            day = 0+day;
        }

        return (year + month + day) * 1;
    },

    "date-eu-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },

    "date-eu-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
} );

jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "formatted-num-pre": function ( a ) {
        a = (a === "-" || a === "") ? 0 : a.replace( /[^\d\-\.]/g, "" );
        return parseFloat( a );
    },

    "formatted-num-asc": function ( a, b ) {
        return a - b;
    },

    "formatted-num-desc": function ( a, b ) {
        return b - a;
    }
} );

jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "anti-label-pre": function ( a ) {
    	var $html = $("<div>");
    	$html.append(a);
//    	alert(a);
		if ($html.children("span.editable").length > 0) {
			a = $html.children("span.editable").children("a").html();
		} else if (($html.children("span.label").length > 0) || ($html.children("a").length > 0)) {
			a = $html.children("a").html();
		}
//		alert(a)
        return a;
    },

    "anti-label-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },

    "anti-label-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
} );

$(function () {

	initEditableFields();
	initCheckboxes();
	initChangeSpaceForm();
	updateProjectButtonStatus();
	$("[id^=chk_project_]").change(function() {
		updateProjectButtonStatus();
	});
	$("#chk_all_project").change(function() {
		updateProjectButtonStatus();
	});

	updateRunButtonStatus();
	$("[id^=chk_run_]").change(function() {
		updateRunButtonStatus();
	});
	$("#chk_all_run").change(function() {
		updateRunButtonStatus();
	});

	updateAnalysisButtonStatus();
	$("[id^=chk_analysis_]").change(function() {
		updateAnalysisButtonStatus();
	});
	$("#chk_all_analysis").change(function() {
		updateAnalysisButtonStatus();
	});

	updateUsersButtonStatus();
	$("[id^=chk_user_]").change(function() {
		updateUsersButtonStatus();
	});
	$("#chk_all_user").change(function() {
		updateUsersButtonStatus();
	});

	addWorkflowsStatusOnRun();
	addWorkflowsStatusOnSingleRun();

	// Init tables
	var projectTable = initProjectTable(),
		runTable = initRunTable(),
		analysisTable = initAnalysisTable(),
		usersTable = initUsersTable();

    // Grab relevant info from URL fragment
    var params = simpleQueryParams(location.href),
    	active_tab =  params["active_tab"];

	if (active_tab ) {
		$('#myTab a[href="#' + active_tab + '"]').tab('show');
	} else {
		$('#myTab').tab();
	}

    $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
        if ($(this).attr("href") == "#analyses") {
        	analysisTable.fnDraw();
        	analysisTable.fnAdjustColumnSizing();
        } else if ($(this).attr("href") == "#users") {
        	usersTable.fnDraw();
        	usersTable.fnAdjustColumnSizing();
        }
    });

    $('a[data-toggle="tab"]').each(function() {
    	if ($(this).parent("li").hasClass("active")) {
            if ($(this).attr("href") == "#analyses") {
            	analysisTable.fnDraw();
            	analysisTable.fnAdjustColumnSizing();
            } else if ($(this).attr("href") == "#users") {
            	usersTable.fnDraw();
            	usersTable.fnAdjustColumnSizing();
            }
    	}
    });
    
    /*
	// We don't need to get the project/run's size on this page anymore.
	var size_url = "index.php?eID=tx_nG6&type=get_size";
	size_url += "&user_id="+$("#user_id").val();
	size_url += "&view="+$("#view").val();
	size_url += "&ids="+$("#ids").val();
	size_url += "&data_folder="+$("#data_folder").val();
	$.ajax({
        url: size_url,
        success: function(val, status, xhr) {
			$("#size").removeClass("tx-nG6-mini-wait");
			$("#size").html(val);
        }
    });
	*/
    
    /* Delete toolbar option */
    $('[id^="delete_"]').on('click', deletePRAHandler);
    /* Hide toolbar option */
    $("[id^=hide_]").on('click', hideHandler);
    /* Unhide toolbar option */
    $("[id^=unhide_]").on('click', unhideHandler);
    /* Unpublish toolbar option */
    $("[id=unpublish_project]").on('click', unpublishHandler);
    /* Publish toolbar option */
    $("[id=publish_project]").on('click', publishHandler);

	/* change user right on project option */
	function change_right(select){
		// launch the delete request on server
	    var val_url = "index.php?eID=tx_nG6&type=change_right";
	    val_url += "&project_id=" + $("#current_project_id").val();
	    val_url += "&c_user_id="+$(select).attr("id").split("_")[3];
	    val_url += "&right_id="+$(select).val();

		$.ajax({
		   url: val_url,
		   success: function(val, status, xhr) {
				if (!endsWith(location.href, "active_tab=users")) {
					location.assign(location.href+"&active_tab=users");
				} else {
					location.assign(location.href);
				}
		   }
	   });
	}

	$("div#users_data_table_wrapper").find("select[name=users_data_table_length]").change(function(){
		$("table#users_data_table > tbody > tr > td > select").change(function(){
			change_right(this);
		});
	});

	$("table#users_data_table > tbody > tr > td > select").change(function(){
		change_right(this);
	});

    /* add a brand new project option */
	$(":button[id=add_project]").on('click', addProjectHandler);

    /* add a brand new run option */
	$(":button[id=add_run]").on('click', addRunHandler);

	/* add a result file to an existing run or analysis*/
	$(":button[id=add_file]").on('click', addFileHandler);

    /* add a brand new analysis option */
	$(":button[id=add_analysis]").on('click', addAnalysisHandler);

	/* delete user from project option */
	$(":button[id=del_user]").click(function(){

		if ($(':checked[id^=chk_user]').size() != 0) {

			$("#modal-label-tmpl").html("Delete user");
    		$("#modal-body-tmpl").html("Are you sure you want to delete the selected elements ?");
    		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">No</button>' +
    			  					   '<button id="modal-btn-yes" class="btn btn-primary">Yes</button>');
    		$("#ng6modal").modal();

    		// Define dialog buttons actions
    		$("#modal-btn-yes").click( function() {
    			// launch the delete request on server
    		    var val_url = "index.php?eID=tx_nG6&type=delete_users";
    		    val_url += "&project_id=" + $("#current_project_id").val();
    		    val_url += "&user_id="+$("#user_id").val() + "&ids=";
	        	$(':checked[id^=chk_user]').each(function(i){
	        		val_url += $(this).val().split("_",2)[1]+";";
	     		});

	        	$("#modal-body-tmpl").html('<div class="tx-nG6-wait">Deleting, please wait...</div>');
    			$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">No</button>' +
    									   '<button id="modal-btn-yes" class="btn btn-primary disabled">Yes</button>');
	    		$("#ng6modal").modal();

    			val_url = val_url.substr(0, val_url.length-1);

    			$.ajax({
					url: val_url,
					success: function(val, status, xhr) {

						// Uncheck all checkboxes
						$(':checked').each(function(i){
							$(this).attr('checked',false);
						});

						if(val == '0'){
							$("#ng6modal").modal('hide');
							if (!endsWith(location.href, "active_tab=users")) {
								location.assign(location.href+"&active_tab=users");
							} else {
								location.assign(location.href);
							}

						// user can't access anymore to any project. Delete him from DB ?
						}else{
							if(val.search(";") != -1 && val.split(";").length > 1){
								$("#modal-body-tmpl").html("Some users can't access anymore to any project. Delete them from database ?");
							}else{
								$("#modal-body-tmpl").html("This user has no longer access to any project. Should this user be deleted from the database ?");
							}
							$("#modal-foot-tmpl").html('<button id="modal-btn-no" class="btn btn-default" aria-hidden="true">No</button>' +
							   						   '<button id="modal-btn-yes" class="btn btn-primary">Yes</button>');

							$("#modal-btn-yes").click( function() {  // delete users (and groups if users are alone in their group)

								var val_url = "index.php?eID=tx_nG6&type=delete_users";
								val_url += "&user_id="+$("#user_id").val();
								val_url += "&ids="+val;

				    		    $.ajax({
									url: val_url,
									success: function(val_p, status_p, xhr_p) {
										$("#ng6modal").modal('hide');
										if (!endsWith(location.href, "active_tab=users")) {
											location.assign(location.href+"&active_tab=users");
										} else {
											location.assign(location.href);
										}
									}
				    		    });
							});

							$("#modal-btn-no").click( function() {  // delete users (and groups if users are alone in their group)
								location.assign(location.href+"&active_tab=users");
							});
						}
					}
				});
	    	});
		}
	});

  	//update user properties
  	$(":button[id=update_user]").click(function(){
  		//only one user can be updated at a time
  		if ($(':checked[id^=chk_user]').size() == 1){

  			var user_id = $.trim($(':checked[id^=chk_user]').val().split("_",2)[1]),
  				user_login = $.trim( $('#tr_user_' + user_id + ' td:eq(1)').text()),
  				first_name = $.trim( $('#tr_user_' + user_id + ' td:eq(5)').text()),
  				last_name = $.trim( $('#tr_user_' + user_id + ' td:eq(4)').text()),
  				email = $.trim( $('#tr_user_' + user_id + ' td:eq(6)').text()),
  				update_pwd=$("#chk_updatablepwd_" + user_id).val();
  			console.log("user_login " + user_login);
  			console.log("first_name " + first_name);
  			console.log("last_name " + last_name);
  			console.log("user_id " + user_id);
  			console.log("update_pwd " + update_pwd);
  			// get title laboratory and organism from ajax query
  			$.ajax({
				url: "index.php?eID=tx_nG6&type=get_user_group&user_id=" + user_id ,
				dataType: 'json',
				success: function (data) {
					update_user_call_back(data, first_name);
				},
				error : function (){
					update_user_call_back({'title' : 'Laboratory', 'organism' : 'Organism', 'location' : "Location",'first_name':first_name});
				}
			});

  			function update_user_call_back (user_group_json) {
  				var	title = user_group_json.title,
	  	  			organism = user_group_json.organism,
	  	  			location = user_group_json.location;
  				console.log("update_user_call_back first_name " + first_name);
	  			$("#modal-label-tmpl").html("Update user <small>" + user_login + "</small>");
	  			var update_user_html = [
                '<div id="error_message" class="alert alert-danger">',
                '    <button class="close" data-dismiss="alert" type="button">x</button>',
                '</div>',
	  	  	    '<div class="tx-nG6-pi1-update-user" >Update user information. For the laboratory you can use autocomplete to seek for an already existing one or create a new one using the formular<br /> <br />' ,
	  	        '    <form class="form-horizontal" id="update_user_form">',
	  	        '        <div class="form-group">',
	  	        '            <label class="col-sm-offset-2 col-sm-2 control-label">First name</label>',
	  	        '            <div class="col-sm-8">',
	  	        '                <input type="text" id="first_name_val" class="form-control" name="first_name_val"  value="' + first_name + '"/>',
	  	        '            </div>',
	  	        '        </div>',
	  	        '        <div class="form-group">',
	  	        '            <label class="col-sm-offset-2 col-sm-2 control-label">Last name</label>',
	  	        '            <div class="col-sm-8">',
	  	        '                <input type="text" id="last_name_val" class="form-control" name="last_name_val" value="' + last_name + '"/> ',
	  	        '            </div>',
	  	        '        </div>',
	  	        '        <div class="form-group">',
	  	        '            <label class="col-sm-offset-2 col-sm-2 control-label">Email</label>',
	  	        '            <div class="col-sm-8">',
	  	        '                <input type="text" id="email_val" class="form-control" name="email_val" value="' + email + '"/>',
	  	        '            </div>',
	  	        '        </div>',
	  		    '       <div class="form-group" id="password-form">',
	  		    '           <label class="col-sm-offset-2 col-sm-2 control-label">Password</label>',
	  		    '           <div class="col-sm-8">',
	  		    '               <div class="input-group">',
	  		    '                   <input class="form-control" type="password" id="user_password_pwd_val" name="user_password_pwd_val" placeholder="***********"/> ',
	  		    '                   <span class="input-group-btn"> ',
	  		    '                       <button class="btn btn-default" type="button" id="key_gen_btn"><i class="glyphicon glyphicon-random"></i>&nbsp;</button>',
	  		    '                   </span> ',
	  		    '               </div>',
	  		    '           </div>',
	  		    '       </div>',
	  	        '        <div class="form-group">',
	  	        '            <label class="col-sm-offset-2 col-sm-2 control-label">Laboratory</label>',
	  	        '            <div class="col-sm-8">',
	  	        '                <input type="text" id="title_val" class="form-control group_typeahead" name="title_val" value="' + title + '"/>',
	  	        '            </div>',
	  	        '        </div>',
	  	        '        <div class="form-group">',
	  	        '            <label class="col-sm-offset-2 col-sm-2 control-label">Organism</label>',
	  	        '            <div class="col-sm-8">',
	  	        '                <input type="text" id="organism_val" class="form-control group_typeahead" name="organism_val" value="' + organism + '"/>',
	  	        '            </div>',
	  	        '        </div>',
	  	        '        <div class="form-group">',
	  	        '            <label class="col-sm-offset-2 col-sm-2 control-label">Location</label>',
	  	        '            <div class="col-sm-8">',
	  	        '                <input type="text" id="location_val" class="form-control group_typeahead" name="location_val" value="' + location + '"/>',
	  	        '            </div>',
	  	        '        </div>',
	  	        '        <div class="checkbox col-sm-offset-4">',
	  	        '            <label>',
	  	        '                <input type="checkbox" checked="checked" id="send_email_chk" name="send_email_chk"/> Send an email to the user',
	  	        '            </label>',
	  	        '        </div>',
	  	        '    </form>',
	  	        '</div>'
	  	        ].join('');

	  	  	    $("#modal-body-tmpl").html(update_user_html);
	  		    $("#modal-foot-tmpl").html('<button class="btn btn-default" id="raz_form"><i class="icon-repeat"></i> Clear form</a>' +
	  		    							'<button id="modal-btn-updateuser" class="btn btn-primary"><i class="icon-ok icon-white"></i> Save</button>');

	  	  	    // show modal box
	  	  	    $("#ng6modal").modal().ready(function() {
	  	  	    	$("#error_message").hide();
	  	  	    	if ( update_pwd == 0 ) {
						$("#password-form").hide();
	  	  	    	}
		  	  	    $('.group_typeahead').each(function(){
						var $this = $(this);
						$this.on('typeahead:selected', function(typeahead, item){
	  						if( item.hasOwnProperty('title') ) {
	  							$("#title_val").val(item.title);
	  							$("#organism_val").val(item.organism);
	  							$("#location_val").val(item.location);
	  							lock_group_fields(1);
	  						} else if( item.hasOwnProperty('organism') ) {
	  							$("#organism_val").val(item.organism);
	  						} else if( item.hasOwnProperty('location') ) {
	  							$("#location_val").val(item.location);
	  						}
						});

						$this.typeahead({
							minLength: 2,
							autoselect: true
						},{
							displayKey : 'value',
							source: function (query, process) {
								var gender = $this.attr("id").split("_val")[0];
								$.ajax({
		  							url: "index.php?eID=tx_nG6&type=autocomplete&gender="+gender+"&project_id=" + $("#current_project_id").val() + "&name_start=" + query,
		  							dataType: 'json',
		  							success: function (users) {
		  								var data = new Array();
		  								for (index in users) {
		  									users[index]['value'] = users[index][gender];
		  									data.push(users[index]);
		  								}
		  								process(data);
		  							}
		  						});
							}
						})

					});

	  	  	      	// lock/unlock fields when autocomplete
	  				function lock_group_fields(order){
	  					// lock fields
	  					if(order == 1){
	  						$("#title_val").attr('disabled', 'disabled');
	  						$("#organism_val").attr('disabled', 'disabled');
	  						$("#location_val").attr('disabled', 'disabled');
	  					// unlock fields
	  					} else if(order == 0){
	  						$("#title_val").removeAttr('disabled');
	  						$("#organism_val").removeAttr('disabled');
	  						$("#location_val").removeAttr('disabled');
	  					}
	  				}

	  				// 	generate key button
	  				$("#key_gen_btn").click(function(){
	  					$("#user_password_pwd_val").val(key_gen(10));
	  					$(this).removeClass("btn-danger");
	  					$(".icon-wrench").removeClass("icon-white");
	  					$(this).parent().parent().parent().removeClass("error");
	  				});


	  				$("#raz_form").click(function(){
	  					$("#last_name_val").val("");
	  					$("#first_name_val").val("");
	  					$("#user_password_pwd_val").val("");
	  					$("#email_val").val("");
	  					$("#title_val").val("");
	  					$("#organism_val").val("");
	  					$("#location_val").val("");
	  					lock_group_fields(0);		// unlock fields
	  					$("#error_message").hide();
	  				});

	  	  	        $("#update_user_form").validate({
	  	  	            rules: {
	  						email_val: {  email: true }
	  					},
	  					messages: {
	  						email_val: null
	  				    },
	  		            submitHandler: function(form) {
	  					    var val_url = "index.php?eID=tx_nG6&type=update_user";
	  					    val_url += "&last_name=" + $("#last_name_val").val();
	  					    val_url += "&first_name=" + $("#first_name_val").val();
	  					    val_url += "&email=" + $("#email_val").val();
	  					    val_url += "&title=" + $("#title_val").val();
	  					    val_url += "&organism=" + $("#organism_val").val();
	  					    val_url += "&location=" + $("#location_val").val();
	  					    val_url += "&creator=" + $("#user_id").val();
	  					    if($("#user_password_pwd_val").val()) {
	  					    	val_url += "&password=" + $("#user_password_pwd_val").val();
	  					    }
	  					    val_url += "&user_id=" + user_id ;
	  					    val_url += "&send_an_email=" + $("#send_email_chk")[0].checked;
	  					    val_url += "&from_email=" + $("#from_email").val();
	  					    val_url += "&envelope_sender_address=" + $("#envelope_sender_address").val();

	  						$.ajax({
	  							url: val_url,
	  							success: function(val, status, xhr) {
	  							    if(parseInt(val) >= 0 ) {
	  							        $("#ng6modal").modal('hide');
	  									if (!endsWith(window.location.href, "active_tab=users")) {
	  										window.location.assign(window.location.href+"&active_tab=users");
	  									} else {
	  										window.location.assign(window.location.href);
	  									}
	  							    }
	  							}
	  						});
	  		            }
	  				});
	  	  	    });

	  	  	    //Submit the form
	  	  	    $("#modal-btn-updateuser").click( function() {
	  			    $("#update_user_form").submit();
	  		    });
  			}
  		}
  	});


	/* add user to project option */
	$(":button[id=add_user]").click(function(){

		$("#modal-label-tmpl").html("Add a user to the project");

	    var add_user_html = [
	    '<div id="error_message" class="alert alert-danger">',
	    '   <button class="close" data-dismiss="alert" type="button">x</button>',
	    '</div>',
	    '<div class="tx-nG6-pi1-add-new">To add a user to the project, seek the user using the autocompletion fields or fill the form to create a brand new user. <br /> <br /> ',
	    '   <form class="form-horizontal" id="new_member">',
	    '       <div class="form-group">',
	    '           <label class="col-sm-offset-2 col-sm-2 control-label">Last name</label>',
	    '           <div class="col-sm-8">',
	    '               <input type="text" id="last_name_val" class="form-control typeahead" name="last_name_val" placeholder="Last name"/> ',
	    '           </div>',
	    '       </div>',
	    '       <div class="form-group">',
	    '           <label class="col-sm-offset-2 col-sm-2 control-label">First name</label>',
	    '           <div class="col-sm-8">',
	    '               <input type="text" id="first_name_val" class="form-control typeahead" name="first_name_val" placeholder="First name"/>',
	    '           </div>',
	    '       </div>',
	    '       <div class="form-group">',
	    '           <label class="col-sm-offset-2 col-sm-2 control-label">Login</label>',
	    '           <div class="col-sm-8">',
	    '               <input type="text" id="username_val" class="form-control typeahead" name="username_val" placeholder="Login"/>',
	    '           </div>',
	    '       </div>',
	    '       <div class="form-group">',
	    '           <label class="col-sm-offset-2 col-sm-2 control-label">Password</label>',
	    '           <div class="col-sm-8">',
	    '               <div class="input-group">',
	    '                   <input class="form-control" type="password" id="user_password_pwd_val" name="user_password_pwd_val" placeholder="Password"/> ',
	    '                   <span class="input-group-btn"> ',
	    '                       <button class="btn btn-default" type="button" id="key_gen_btn"><i class="glyphicon glyphicon-random"></i>&nbsp;</button>',
	    '                   </span> ',
	    '               </div>',
	    '           </div>',
	    '       </div>',
	    '       <div class="form-group">',
	    '           <label class="col-sm-offset-2 col-sm-2 control-label">Email</label>',
	    '           <div class="col-sm-8">',
	    '               <input type="text" id="email_val" class="form-control typeahead" name="email_val" placeholder="Email"/>',
	    '           </div>',
	    '       </div>',
	    '       <div class="form-group">',
	    '           <label class="col-sm-offset-2 col-sm-2 control-label">Laboratory</label>',
	    '           <div class="col-sm-8">',
	    '               <input type="text" id="title_val" class="form-control  group_typeahead" name="title_val" placeholder="Laboratory"/>',
	    '           </div>',
	    '       </div>',
	    '       <div class="form-group">',
	    '           <label class="col-sm-offset-2 col-sm-2 control-label">Organism</label>',
	    '           <div class="col-sm-8">',
	    '               <input type="text" id="organism_val" class="form-control group_typeahead" name="organism_val" placeholder="Organism"/>',
	    '           </div>',
	    '       </div>',
	    '       <div class="form-group">',
	    '           <label class="col-sm-offset-2 col-sm-2 control-label">Location</label>',
	    '           <div class="col-sm-8">',
	    '               <input type="text" id="location_val" class="form-control group_typeahead" name="location_val" placeholder="Location"/>',
	    '           </div>',
	    '       </div>',
	    '       <div class="form-group">',
	    '           <label class="col-sm-offset-2 col-sm-2 control-label">Right</label>',
	    '           <div class="col-sm-8">',
	    '               <select class="form-control" id="cbb_user_right" name="user_right_val">',
	    $("#is_project_admin").val() ? ' <option value="2">Administrator</option>' : '' ,
	    '                   <option value="1">Manager</option>',
	    '                   <option value="0" selected="selected">Member</option>',
	    '               </select>',
	    '           </div>',
	    '       </div>',
	    '       <div class="checkbox col-sm-offset-4">',
	    '           <label>',
	    '               <input type="checkbox" checked="checked" id="send_email_chk" name="send_email_chk"/> Send an email to the user',
	    '           </label>',
	    '       </div>',
	    '   </form>',
	    '</div>'
	    ].join('');

		$("#modal-body-tmpl").html(add_user_html);
		$("#modal-foot-tmpl").html('<button class="btn btn-default" id="raz_form"><i class="glyphicon glyphicon-repeat"></i> Clear form</a>' +
								   '<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>' +
								   '<button id="modal-btn-adduser" class="btn btn-primary"><i class="glyphicon glyphicon-user"></i> Add</button>');

		$("#ng6modal").modal().ready(function() {
			$("#error_message").hide();

			$('.typeahead').each(function(){
				var $this = $(this);
				$this.on('typeahead:selected', function(typeahead, item){
					$("#new_member div.control-group").removeClass("error");
					$(".btn-danger").removeClass("btn-danger");
					$(".icon-wrench").removeClass("icon-white");
					$("#last_name_val").val(item.last_name);
					$("#first_name_val").val(item.first_name);
					$("#username_val").val(item.username);
					$("#user_password_pwd_val").val(item.password);
					$("#email_val").val(item.email);
					$("#title_val").val(item.title);
					$("#organism_val").val(item.organism);
					$("#location_val").val(item.location);
					$('select#cbb_user_right option[value='+item.right_id+']').attr("selected","selected");
					lock_fields(1);
				});
				$this.typeahead({
					minLength: 2,
					autoselect: true
				},{
					displayKey : 'value',
					source : function(query, process){
						var gender = $this.attr("id").split("_val")[0];
						$.ajax({
							url: "index.php?eID=tx_nG6&type=autocomplete&gender="+gender+"&project_id=" + $("#current_project_id").val() + "&name_start=" + query,
							dataType: 'json',
							success: function (users) {
								var data = new Array();
								for (index in users) {
									users[index]['value'] = users[index][gender];
									data.push(users[index]);
								}
								process(data);
							}
						});
					}
				});
			});

			$('.group_typeahead').each(function(){
				var $this = $(this);
				$this.on('typeahead:selected', function(typeahead, item){
					if( item.hasOwnProperty('title') ) {
						$("#title_val").val(item.title);
						$("#organism_val").val(item.organism);
						$("#location_val").val(item.location);
						lock_group_fields(1);
					} else if( item.hasOwnProperty('organism') ) {
						$("#organism_val").val(item.organism);
					} else if( item.hasOwnProperty('location') ) {
						$("#location_val").val(item.location);
					}
				});

				$this.typeahead({
					minLength: 2,
					autoselect: true
				},{
					displayKey : 'value',
					source: function (query, process) {
						var gender = $this.attr("id").split("_val")[0];
						$.ajax({
							url: "index.php?eID=tx_nG6&type=autocomplete&gender="+gender+"&project_id=" + $("#current_project_id").val() + "&name_start=" + query,
							dataType: 'json',
							success: function (users) {
								var data = new Array();
								for (index in users) {
									users[index]['value'] = users[index][gender];
									data.push(users[index]);
								}
								process(data);
							}
						});
					}
				})

			});

			// generate key button
			$("#key_gen_btn").click(function(){
				$("#user_password_pwd_val").val(key_gen(10));
				$(this).removeClass("btn-danger");
				$(".icon-wrench").removeClass("icon-white");
				$(this).parent().parent().parent().removeClass("error");
			});

			// empty the form
			$("#raz_form").click(function(){
				$("#last_name_val").val("");
				$("#first_name_val").val("");
				$("#username_val").val("");
				$("#user_password_pwd_val").val("");
				$("#email_val").val("");
				$("#title_val").val("");
				$("#organism_val").val("");
				$("#location_val").val("");
				$('select#cbb_user_right option[value=0]').attr("selected","selected");
				lock_fields(0);		// unlock fields
				lock_group_fields(0);		// unlock fields
				$("#new_member div.control-group").removeClass("error");
				$(".btn-danger").removeClass("btn-danger");
				$(".icon-wrench").removeClass("icon-white");
				$("#error_message").hide();
			});

			// lock/unlock fields when autocomplete
			function lock_fields(order){
				// lock fields
				if(order == 1){
					$("#username_val").attr('disabled', 'disabled');
					$("#last_name_val").attr('disabled', 'disabled');
					$("#first_name_val").attr('disabled', 'disabled');
					$("#email_val").attr('disabled', 'disabled');
					$("#key_gen_btn").addClass('disabled');
					$("#user_password_pwd_val").attr('disabled', 'disabled');
					$('.typeahead').each(function(){
						$(this).css('background-color','');
					});
					lock_group_fields(order);

				// unlock fields
				} else if(order == 0){
					$("#username_val").removeAttr('disabled');
					$("#last_name_val").removeAttr('disabled');
					$("#first_name_val").removeAttr('disabled');
					$("#email_val").removeAttr('disabled');
					$("#key_gen_btn").removeClass('disabled');
					$("#title_val").removeAttr('disabled');
					$("#organism_val").removeAttr('disabled');
					$("#location_val").removeAttr('disabled');
					$("#user_password_pwd_val").removeAttr('disabled');
				}
			}

			// lock/unlock fields when autocomplete
			function lock_group_fields(order){
				// lock fields
				if(order == 1){
					$("#title_val").attr('disabled', 'disabled');
					$("#organism_val").attr('disabled', 'disabled');
					$("#location_val").attr('disabled', 'disabled');
					$('.group_typeahead').each(function(){
						$(this).css('background-color','');
					});
				// unlock fields
				} else if(order == 0){
					$("#title_val").removeAttr('disabled');
					$("#organism_val").removeAttr('disabled');
					$("#location_val").removeAttr('disabled');
				}
			}

			if($("#username_val").attr('disabled') != 'disabled'){
				$("#new_member").validate({
					rules: {
						first_name_val: { required: true },
						email_val: { required: true, email: true },
						last_name_val: { required: true },
						username_val: { required: true },
						title_val: { required: true },
						organism_val: { required: true },
						location_val: { required: true },
						user_password_pwd_val: { required: true }
					},
					messages: {
						first_name_val: null,
						email_val: null,
						last_name_val: null,
						username_val: null,
						title_val: null,
						organism_val: null,
						location_val: null,
						user_password_pwd_val: null
				    },
		            submitHandler: function(form) {

					    var val_url = "index.php?eID=tx_nG6&type=add_user";
					    val_url += "&username=" + $("#username_val").val();
					    val_url += "&first_name=" + $("#first_name_val").val();
					    val_url += "&last_name=" + $("#last_name_val").val();
					    val_url += "&password=" + escape($("#user_password_pwd_val").val());
					    val_url += "&email=" + $("#email_val").val();
					    val_url += "&creator=" + $("#user_id").val();
					    val_url += "&title=" + $("#title_val").val();
					    val_url += "&organism=" + $("#organism_val").val();
					    val_url += "&location=" + $("#location_val").val();
					    val_url += "&project_id=" + $("#current_project_id").val();
					    val_url += "&right=" + $("#cbb_user_right").val();
					    val_url += "&pid=" + $("#pid").val();
					    val_url += "&add_user_email=" + escape("Dear ###USER_FIRST_NAME### ###USER_LAST_NAME###,\nYou have now acccess to the ###PROJECT_NAME### project (###PROJECT_DESCRIPTION###) accessible once logged in by the following url: ###PROJECT_LINK###\n###REINIT_PASSWORD###\nThe NG6 team\n");
					    val_url += "&add_user_title=[NG6] You have been added to a project";
					    val_url += "&create_user_email=" + escape("Dear ###USER_FIRST_NAME### ###USER_LAST_NAME###,\nPlease find below your login required to log into the NG6 system (###PROJECT_LINK###): \n   - login: ###USER_LOGIN###\n###REINIT_PASSWORD###\nThe NG6 team\n");
					    val_url += "&create_user_title=[NG6] Account creation";
					    val_url += "&from_email=" + $("#from_email").val();
					    val_url += "&envelope_sender_address=" + $("#envelope_sender_address").val();
					    val_url += "&project_url=" + escape(location.href);
					    val_url += "&send_an_email=" + $("#send_email_chk")[0].checked;

						$.ajax({
							url: val_url,
							success: function(val, status, xhr) {
								val = parseInt(val);
								if (val == 1) {
									// user has already access to this project
									$("#error_message").html($("#username_val").val() + ' has already access to this project!').show();
								} else if (val == 4 || val == 3) {
									// username exists in database cannot create
									$("#error_message").html('Cannot create this user account, ' + $("#username_val").val() + ' already exists!').show();
								} else if (val == 5) {
									// email exists in database cannot create
									$("#error_message").html('Cannot create this user account, ' + $("#email_val").val() + ' already exists!').show();
								} else {
				    	    		$("#ng6modal").modal('hide');
									if (!endsWith(location.href, "active_tab=users")) {
										location.assign(location.href+"&active_tab=users");
									} else {
										location.assign(location.href);
									}
								}
							}
						});
		            }
				});
			}

		});

		$("#modal-btn-adduser").click( function() {
			$("#new_member").submit();
		});

	});

	$("#install_form").validate({
		rules: {
			first_name_val: { required: true },
			email_val: { required: true, email: true },
			last_name_val: { required: true },
			username_val: { required: true },
			user_password_pwd_val: { required: true },
			user_password_pwd2_val: { required: true, equalTo: "#user_password_pwd_val"},
			title_val : { required : true },
			organism_val : { required : true },
			location_val : { required : true}
		},
		messages: {
			first_name_val: null,
			email_val: null,
			last_name_val: null,
			username_val: null,
			user_password_pwd_val: null,
			user_password_pwd2_val: "Same password is required",
			title_val: null,
			organism_val: null,
			location_val: null
	    },
	    submitHandler: function(form) {
	    	var val_url = "index.php?eID=tx_nG6&type=install";
		    val_url += "&username=" + $("#username_val").val();
		    val_url += "&first_name=" + $("#first_name_val").val();
		    val_url += "&last_name=" + $("#last_name_val").val();
		    val_url += "&password=" + $("#user_password_pwd_val").val();
		    val_url += "&email=" + $("#email_val").val();
		    val_url += "&pid=" + $("#pid").val();
		    val_url += "&title=" + $("#title_val").val();
		    val_url += "&organism=" + $("#organism_val").val();
		    val_url += "&location=" + $("#location_val").val();
			$.ajax({
				url: val_url,
				success: function(val, status, xhr) {
					location.assign(location.href);
				},
				error: function (xhr, ajaxOptions, thrownError) {
			    	var html = '<div id="message" class="alert alert-danger"><strong>Error!</strong> An error occured when attempting to finalize installation.<button class="close" data-dismiss="alert" type="button" >x</button></div>';
			    	$("#message").html(html).removeClass("hidden");
			    }
			});
        }
	});
	$("#btn-install").click( function() {
		if ($("#username_val").val() == "admin_install") {
			var html = '<div id="message" class="alert alert-danger"><strong>Login error!</strong> This login is not allowed.<button class="close" data-dismiss="alert" type="button" >x</button></div>';
	    	$("#message").html(html).removeClass("hidden");
		} else {
			$("#install_form").submit();
		}
	});
	$("#raz_form_install").click( function() {
		$("#username_val").val("");
		$("#first_name_val").val("");
		$("#last_name_val").val("");
		$("#user_password_pwd_val").val("");
		$("#user_password_pwd2_val").val("");
		$("#email_val").val("");
		$("#project_name_val").val("");
		$("#description_val").val("");
		$("#title_val").val("");
		$("#organism_val").val("");
		$("#location_val").val("");
	});

	//comments handling
	initComments();

	//toogle run view
	$('.full-run-view').each(function(idx, element){
		// Get the column API object
        var column = runTable.column( $(this).attr('data-column') );
        column.visible( false );
	});

	$('#toggle_runtable_display').click(function(){
		$(this).toggleClass('full_table_view');
		var visible = $(this).hasClass('full_table_view');
		var indexes = runTable.columns()[0];

		if ($(this).hasClass('full_table_view')){
			$(this).html('Summary view');
		}
		else {
			$(this).html('Full view');
		}


		$.each(indexes, function(i, idx){
			var column = runTable.column( idx );
			if($(column.header()).hasClass('full-run-view')) {
				column.visible( visible );
			}
		});
	});

});


function initComments(){

	var reset_comments = function(){
		$('div.comment-block.editing').each(function(){
			$(this).removeClass('editing');
			var old_html = $(this).data('old_html');
			$(this).children('.comment-block-body').first().html(old_html);
		});
	}

	var refresh_comment = function(active_id){
		document.location.assign(location.href.replace(/&*active_tab=[^&]*(&*)/g, '$1')  + "&active_tab="+active_id);
	};

	$('.wysihtml5-textarea').wysihtml5({
		"font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
		"emphasis": true, //Italics, bold, etc. Default true
		"lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
		"html": false, //Button which allows you to edit the generated HTML. Default false
		"link": true, //Button to insert a link. Default true
		"image": false, //Button to insert an image. Default true,
		"color": false //Button to change color of font
	});

	$('#post-comment').click(function(){
		var new_comment	= $('.wysihtml5-textarea').data("wysihtml5").editor.getValue(),
			view		= $('#view').val(),
			view_id		= $('#ids').val(),
			cruser_id	= $('#user_id').val(),
			comment_tab_id = $(this).closest('div.comment-tab').attr('id');

		$.ajax({
	        url: "index.php",
	        data : {
	        	'eID'			: 'tx_nG6',
	        	'type'			: 'add_comment',
	        	'view'			: view,
	        	'view_id'		: view_id,
	        	'cruser_id'		: cruser_id,
	        	'new_comment'	: new_comment
	        },
	        type	: 'POST',
	        success: function(val, status, xhr) {
		    	if (val != 0){
		    		refresh_comment(comment_tab_id);
	        	}
	        }
	    });
	});

	$('.comment-edit').click(function(e){
		e.preventDefault();
		var edit_link = $(this);
		//hide new comment block

		$('.new-comment-block').hide();

		reset_comments();

		// mark as editing
		edit_link.closest('div.comment-block').addClass('editing');

		var old_html = edit_link.closest('div.comment-block').children('.comment-block-body').first().html();
		edit_link.closest('div.comment-block').data('old_html', old_html );
		edit_link.closest('div.comment-block').children('.comment-block-body').first().html('<textarea class="edit-wysihtml5-textarea" ></textarea>'+
				'<div style="text-align : right;"><button id="cancel-comment" type="button" class="btn btn-default btn-sm ">cancel</button> '+
				'<button id="save-comment" type="button" class="btn btn-sm btn-primary">save</button></div>');

		$('.edit-wysihtml5-textarea').wysihtml5({
			"font-styles": false,
			"emphasis": true,
			"lists": true,
			"html": false,
			"link": true,
			"image": false,
			"color": false
		});
		$('.edit-wysihtml5-textarea').data("wysihtml5").editor.setValue(old_html);

		$('#cancel-comment').click(function(){
			reset_comments();
			$('.new-comment-block').show();
		});

		$('#save-comment').click(function(){
			var new_comment	= $('.edit-wysihtml5-textarea').data("wysihtml5").editor.getValue(),
				comment_id = edit_link.closest('div.comment-block').attr('id').split('_')[2],
				comment_tab_id = edit_link.closest('div.comment-tab').attr('id');

			$.ajax({
				url: "index.php",
		        data : {
		        	'eID'			: 'tx_nG6',
		        	'type'			: 'update_comment',
		        	'comment_id'	: comment_id,
		        	'new_comment'	: new_comment
		        },
		        type	: 'POST',
			    success: function(val, status, xhr) {
			    	if (val != 0){
			    		refresh_comment(comment_tab_id);
			    	}
			    }
			});
		});
	});

	$('.comment-delete').click(function(e){
		e.preventDefault();
		var delete_link = $(this);
		// First confirm with the user
		$("#modal-label-tmpl").html("Delete");
		$("#modal-body-tmpl").html("Are you sure you want to delete ?");
		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">No</button>' +
								   '<button id="modal-btn-yes" class="btn btn-primary">Yes</button>');
		$("#ng6modal").modal();

		$("#modal-btn-yes").click( function() {
			var comment_id = delete_link.closest('div.comment-block').attr('id').split('_')[2],
			view		= $('#view').val(),
			view_id		= $('#ids').val(),
			cruser_id	= $('#user_id').val(),
			comment_tab_id = delete_link.closest('div.comment-tab').attr('id');

			var val_url = "index.php?eID=tx_nG6&type=delete_comment";
			val_url += "&view="+view;
			val_url += "&view_id="+view_id;
			val_url += "&comment_id="+comment_id;

			$.ajax({
			    url: val_url,
			    success: function(val, status, xhr) {
			    	if (val != 0){
			    		refresh_comment(comment_tab_id);
			    	}
			    }
			});
		});
	});
}



function addWorkflowsStatusOnRun() {
	if ($("[id^=wf_run_]").length > 0) {
		// Init active workflows on runs
		var run_ids = "";
		var counter = 0;
		var counter = 0;
        
        runIds = $( "[id^=tr_run_]" ).get().sort().reverse();
        for (var i = 0; i < runIds.length; i++) {
        	counter = counter+1;
            if (counter < 20){
            	run_ids += "run_id=" + runIds[i].id.split("_")[2] + ",";
            }
        }
		if (run_ids != "") {
			run_ids = run_ids.slice(0,-1);
			// use the jflow REST api to retrieve workflows with these metadata
			var serverURL = "http://localhost:8080";
			if ($("#server_url").val()){ serverURL = $("#server_url").val(); }
			$.ajax({
			    url: serverURL +'/get_workflows_status?metadata_filter='+run_ids+'&callback=?',
			    dataType: "json",
			    success: function(data) {
			    	var class_label = "";
			    	for (var i in data) {
			    		if (data[i]["status"] == "completed"){
			    			class_label = 'btn-success';
			    		} else if (data[i]["status"] == "failed"){
			    			class_label = 'btn-danger';
			    		} else if (data[i]["status"] == "aborted"){
			    			class_label = 'btn-danger';
			    		} else if (data[i]["status"] == "started"){
			    			class_label = 'btn-info';
			    		}
			    		var $html = $('<button class="workflow-btn btn btn-default btn-sm '+class_label+'" style="padding: 2px;"><i class="glyphicon glyphicon-cog"></i></button>'),
			    			run_id = "",
			    			workflow = {};
			    		for (var j in data[i]["metadata"]) {
			    			if (data[i]["metadata"][j].indexOf("run_id=") == 0) {
			    				run_id = data[i]["metadata"][j].split("=")[1];
			    				$html.data('workflow', data[i]);
			    				if ($("#wf_run_"+run_id).children('.btn-group').length > 0) {
			    					$("#wf_run_"+run_id).children('.btn-group').append($html);
			    				} else {
			    					$("#wf_run_"+run_id).append('<div class="btn-group"></div>');
			    					$("#wf_run_"+run_id).children('.btn-group').append($html);
			    				}
			    			}
			    		}
			    	}
			    	$(".workflow-btn").on("click", function(event){
		    		    $('#setAndRunModalLabel').html($(this).data('workflow')["name"] + " <small>" + $(this).data('workflow')["id"] + "</small>");
		    		    $('#setAndRunModalBody').wfstatus({
		    		    	workflowID: $(this).data('workflow').id,
		    		    	forceUsingWorkflow: $(this).data('workflow'),
		    		    	serverURL: $("#server_url").val()
		    		    });
		    			$('#setAndRunModalFooter').html([
                     	'    <div class="btn-group">',
                     	'        <button id="rerun_workflow" class="btn btn-default"><i class="glyphicon glyphicon-cog"></i> Rerun</button>',
                     	'        <button id="refresh_workflow" class="btn btn-primary"><i class="glyphicon glyphicon-refresh"></i> Refresh</button>',
                     	'    </div>',
                     	].join(''));
		    	    	$("#reset_workflow, #run_workflow").hide();
		    	    	unbindAll();
		    	    	$("#refresh_workflow").click(function(){
			    	    	$('#setAndRunModalBody').wfstatus('reload');
		    	    	});
		    	    	$("#rerun_workflow").bind('click', {'selector' : '#setAndRunModalBody' }, rerunWFHandler);
		    	    	$('#setAndRunModal').modal();
		    		});
			    }
	    	});
		}
	}
}

function addWorkflowsStatusOnSingleRun(){

	if ($("[id^=wfstatus_run_id]").length > 0) {
		var run_id = $("[id^=wfstatus_run_id]").first().attr("id").split("_")[3];

		if (run_id){
			var serverURL = "http://localhost:8080";
			if ($("#server_url").val()){ serverURL = $("#server_url").val(); }
			$.ajax({
			    url: serverURL +'/get_workflows_status?metadata_filter=run_id='+run_id+'&callback=?',
			    dataType: "json",
			    success: function(data) {
			    	var class_label = "";
			    	for (var i in data) {
			    		var myrun = data[i];
			    		if (myrun["status"] == "completed"){
			    			class_label = 'btn-success';
			    		} else if (myrun["status"] == "failed"){
			    			class_label = 'btn-danger';
			    		} else if (myrun["status"] == "aborted"){
			    			class_label = 'btn-danger';
			    		} else if (myrun["status"] == "started"){
			    			class_label = 'btn-info';
			    		}
			    		var $html = $('<button class="workflow-btn btn btn-default btn-sm '+class_label+'" style="padding: 2px;"><i class="glyphicon glyphicon-cog"></i></button>'),
			    			myrun_id = "";

			    		for (var j in myrun["metadata"]) {
			    			if (myrun["metadata"][j].indexOf("run_id=") == 0) {
			    				myrun_id = myrun["metadata"][j].split("=")[1];
			    				$html.data('workflow', myrun);

			    				if(myrun_id == run_id) {
			    					$("#wfstatus_run_id_"+run_id).html($html);
			    				}
			    			}
			    		}
			    	}
			    	$(".workflow-btn").on("click", function(event){
		    		    $('#setAndRunModalLabel').html($(this).data('workflow')["name"] + " <small>" + $(this).data('workflow')["id"] + "</small>");
		    		    $('#setAndRunModalBody').wfstatus({
		    		    	workflowID: $(this).data('workflow').id,
		    		    	forceUsingWorkflow: $(this).data('workflow'),
		    		    	serverURL: $("#server_url").val()
		    		    });
		    			$('#setAndRunModalFooter').html([
	                 	'    <div class="btn-group">',
	                 	'        <button id="refresh_workflow" class="btn btn-default"><i class="glyphicon glyphicon-refresh"></i> Refresh</button>',
	                 	'        <button id="rerun_workflow" class="btn btn-primary"><i class="glyphicon glyphicon-cog"></i> Rerun</button>',
	                 	'    </div>',
	                 	].join(''));
		    	    	$("#reset_workflow, #run_workflow").hide();
		    	    	unbindAll();
		    	    	$("#refresh_workflow").click(function(){
			    	    	$('#setAndRunModalBody').wfstatus('reload');
		    	    	});
		    	    	$("#rerun_workflow").bind('click', {'selector' : '#setAndRunModalBody' }, rerunWFHandler);
		    	    	$('#setAndRunModal').modal();
		    		});
			    }
	    	});
		}
	}
}


function updateProjectButtonStatus() {
	$('.multiplep-selection-btn').each(function(){
       	if ($(":checked[id^=chk_project_]").size() == 0) {
            $(this).attr('disabled', 'disabled');
        } else if ($(":checked[id^=chk_project_]").size() > 0) {
        	$(this).removeAttr('disabled');
        }
    });
	$(".nop-selection-btn").each(function(){ $(this).removeAttr('disabled'); });
}

function updateRunButtonStatus() {
	$('.multipler-selection-btn').each(function(){
       	if ($(":checked[id^=chk_run_]").size() == 0) {
            $(this).attr('disabled', 'disabled');
        } else if ($(":checked[id^=chk_run_]").size() > 0) {
        	$(this).removeAttr('disabled');
        }
    });
	$(".nor-selection-btn").each(function(){ $(this).removeAttr('disabled'); });
}

function updateAnalysisButtonStatus() {
	$('.multiplea-selection-btn').each(function(){
       	if ($(":checked[id^=chk_analysis_]").size() == 0) {
            $(this).attr('disabled', 'disabled');
        } else if ($(":checked[id^=chk_analysis_]").size() > 0) {
        	$(this).removeAttr('disabled');
        }
    });
	$(".noa-selection-btn").each(function(){ $(this).removeAttr('disabled'); });
}

function updateUsersButtonStatus() {
	$('.multipleu-selection-btn').each(function(){
		if ($(":checked[id^=chk_user_]").size() == 0) {
            $(this).attr('disabled', 'disabled');
        } else if ($(":checked[id^=chk_user_]").size() > 0) {
        	disabled = false;
        	user_id = $('#user_id').val();
    		$(":checked[id^=chk_user_]").each(function(){
    			checked_user_id = $(this).val().split("_",2)[1];
    			disabled = disabled || (checked_user_id == user_id);
    		});
    		if(disabled == true){
    			$('#del_user').attr('disabled', 'disabled');
    		}
    		else{
    			$('#del_user').removeAttr('disabled');
    		}
        }
    });
	$('.singleu-selection-btn').each(function(){
       	if ($(":checked[id^=chk_user_]").size() !=1 ) {
            $(this).attr('disabled', 'disabled');
        } else {
        	$(this).removeAttr('disabled');
        }
    });

	$(".nou-selection-btn").each(function(){ $(this).removeAttr('disabled'); });

	// Disable update user if this is not the user or the creator
	$('#update_user').each(function(){
		if ($(":checked[id^=chk_user_]").size() == 1 ) {
			var user_id = $('#user_id').val();
			var checked_user_id = $(":checked[id^=chk_user_]").val().split("_",2)[1];
			var cruser_id = $("#chk_cruser_" + checked_user_id).val();
			if( user_id == checked_user_id || user_id == cruser_id){
				$(this).removeAttr('disabled');
			}
			else{
				$(this).attr('disabled', 'disabled');
			}
        }
	});
}

function updateButtonStatus(){
	updateProjectButtonStatus();
	updateRunButtonStatus();
	updateAnalysisButtonStatus();
	updateAnalysisButtonStatus();
	updateUsersButtonStatus();
}


/* Run table init */
function initRunTable() {
	var options = {
		"pageLength"	: 25 ,
		"scrollX"		: "90%",
		"sScrollXInner"	: "100%",
		"order"			: [[2, 'desc']],
		"columnDefs" 	: [{ "targets" : 0, "type" : "anti-label" },
		             	   { "targets" : 2, "type" : "date" },
		             	   { "targets" : [6,7], "type": "formatted-num" }
		]
	};
  	if ($(":checkbox[id^=chk_run_]").size() > 0) {
  		options.order = [[3, 'desc']];
  		options.columnDefs = [
  			   { "targets" : 0, "orderable": false, "width": "1%" },
  			   { "targets" : 1, "type": "anti-label"},
  			   { "targets" : 3, "type": "date-eu"},
  			   { "targets" : [7,8], "type": "formatted-num" },
  			   { "type": "formatted-num" },
		       { "targets" : -1 , "orderable": false  }
  			];
  	} else if ($("[id^=wf_run_]").length > 0) {
		options.columnDefs.push({ "targets" : -1 , "orderable": false  })
	}
  	return $('#run_data_table').DataTable(options);
}

/* Project table init */
function initProjectTable () {
	var options = {
		"pageLength" : 25,
		"scrollX"	: "90%",
		"sScrollXInner": "100%",
		"order"		: [[0, 'desc']],
		"columns"	: [
	           { "type" : "anti-label"},
	           { "type" : "date-eu"},
	           null
       ]
	};

	if ($(":checkbox[id^=chk_project_]").size() > 0) {
		//options.order = [[1, 'asc']] ;
		options.columns = [
               { "orderable": false, "width" : "1%"},
               { "type" : "anti-label"},
               { "type" : "date-eu"},
               null
       ];
	}
  	return $('#project_data_table').dataTable(options);
}

function initAnalysisTable() {
	return $('#analysis_data_table').dataTable({
		"scrollX"	: "90%",
		"sScrollXInner": "100%",
  		"order": []
  	});
}

function initUsersTable() {
	var options = {
			"scrollX"		: "90%",
			"sScrollXInner"	: "100%",
//	    	"bDestroy":true,
	    	"order"			: [[0, 'asc']],
	    	"columnDefs" 	: [{"targets" : 1 ,"type": "date-eu"}]
	};

	if ($(":checkbox[id^=chk_user_]").size() > 0) {
		options.order = [[1, 'asc']] ;
		options.columnDefs = [
    		         { "targets" : 0, "orderable": false , "width" : "1%" },
      		         { "targets" : 2, "type": "date-eu" }
         ];
	}
	return $('#users_data_table').dataTable(options);
}

function initCheckboxes() {
    /* Checked or unchecked all checkboxes (master checkbox) */
    $(":checkbox[id^=chk_all]").click( function() {
        if ($(this)[0].checked) {
	    	$("input[type='checkbox'][id^=chk_"+$(this).attr("id").split("_")[2]+"]").each( function() {
	        		$(this)[0].checked = true;
	        	}
	        );
        } else {
        	 $("input[type='checkbox'][id^=chk_"+$(this).attr("id").split("_")[2]+"]").each( function() {
					$(this)[0].checked = false;
        		}
        	);
        }
    });

    /* Check main checkbox status */
    $(':checkbox[id^=chk]').click(function () {
     	if( $(this).attr("id").split("_")[1] != "all" && $(this).attr("id").split("_")[1] != "col") {
			if ($(':checkbox[id^=chk_'+ $(this).attr("id").split("_")[1] +']').size() == $(':checked[id^=chk_'+ $(this).attr("id").split("_")[1] +']').size()) {
				$(":checkbox[id=chk_all_"+ $(this).attr("id").split("_")[1] +"]")[0].checked = true;
			} else {
				$(":checkbox[id=chk_all_"+ $(this).attr("id").split("_")[1] +"]")[0].checked = false;
			}
		}
	});
}

function initChangeSpaceForm() {
        var workflow_status_handler = function(workflow_id){
		 $.ajax({
                           url : $("#server_url").val() + '/get_workflow_status?display=list&workflow_id=' + workflow_id ,
                           dataType : 'jsonp',
                           timeout: 20000 ,
                           success : function(data){
                                        $("#wfStatusMigration").html("Workflow status : " + data.status);
                                        if (data.status=="completed" ){
                                                $("#current_space_id").html($("#space_id").val());
                                        }
                                        if (data.status=="completed" || data.status=="failed" ){
                                                setTimeout(function(){
                                                      $("#myCarouselMigration").carousel('next');
                                                      $("#myCarouselMigration").carousel('pause');
                                                      $('#wfStatusMigration').html("Workflow status : <span class='tx-nG6-mini-wait'></span>");
                                                },3000);
                                        }
                           },
                           error : function(jqXHR, textStatus, errorThrown){
				$("#myCarouselMigration").html("Error on get status.");
                                 setTimeout(function(){
                                               $("#myCarouselMigration").carousel('next');
                                               $("#myCarouselMigration").carousel('pause');
                                               $('#wfStatusMigration').html("Workflow status : <span class='tx-nG6-mini-wait'></span>");
                                        },3000);
                           }
                       });
	};
        $('#migration_form').html([
	'    <div id="myCarouselMigration" class="carousel slide">',
	'        <div class="carousel-inner">',
	'            <div class="active item">',
        '               <p id="warnMigration"><i>Warning, migrating a project from one space to another will update both the files location and the retention limits.</i></p>',
        '               <div id="wfFormMigration" ></div>',
        '               <button id="run_change_space" style="float: right;" class="btn btn-info btn-sm"><i class="glyphicon glyphicon-share-alt"></i> Migrate</button>',
        '            </div>',
	'            <div class="item">',
        '               <input id="wfIdMigration" type="hidden"></input>',
        '               <p id="wfStatusMigration" >Workflow status : <span class="tx-nG6-mini-wait"></span></p>',
        '               <button id="refresh_workflow_migrate"  style="float: right;"  class="btn btn-info btn-sm"><i class="glyphicon glyphicon-refresh"></i> Refresh</button>',
        '            </div>',
	'        </div>',
	'    </div>',
        '    <div id="formMigrationFooter" align="right">',


        '    </div>'
	].join('')
        );

      	$('#myCarouselMigration').on('keydown', 'input', function(e) {
	  e.stopPropagation();
	});
        var wfform_options = {
                workflowClass: "SwitchSpaceId",
                serverURL: $("#server_url").val(),
                displayRunButton: false,
                displayResetButton: false,
                parameters : {
                        "admin_login"	: $("#user_login").val(),
                        "project_id"		:  $("#ids").val()
                }
	};
        $("#wfFormMigration").wfform(wfform_options);

        //Launch workflow migration
        $("#run_change_space").click(function(){
    		//We show a modal to check if the given SSH user + password is ok
                var switch_space_auth_html = [
                '<div class="tx-nG6-pi1-login">',
                '	This functionality is only available if you have an account on the ' + $("#server_name").val() + ' server. <br /> <br /> ',
                '	<form class="form-horizontal" id="tx-nG6-pi1-login-form" >',
                '		<div class="input-group" style="margin-left:50px">',
    		 	'			<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>',
    		 	'			<input class="form-control" style="width:200px" type="text" id="user_login_val" name="user_login_val" placeholder="Username"/>',
    		 	'		</div><br/>',
		 		'		<div class="input-group" style="margin-left:50px">',
			 	'			<span class="input-group-addon"><i class="glyphicon glyphicon-wrench"></i></span>',
			 	'			<input class="form-control" style="width:200px" type="password" id="user_pwd_val" name="user_pwd_val" placeholder="Password"/>',
			 	'		</div>',
			 	'	</form>',
			 	'</div>'
			].join('');
		$("#modal-label-tmpl").html("Migrate");
    		$("#modal-body-tmpl").html(switch_space_auth_html);
    		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>' +
    								   '<button id="modal-btn-migrate" class="btn btn-primary"><i class="glyphicon glyphicon-share-alt"></i> Migrate</button>');
    		$("#ng6modal").modal();

    		$("#modal-btn-migrate").click(function(){
                        //We call the ajax check "check_ssh_user"
                        var val_url = "index.php?eID=tx_nG6&type=check_ssh_user";
                        val_url += "&user_login=" + $("#user_login_val").val() + "&user_pwd=" + encodeURIComponent($("#user_pwd_val").val()) + "&project_id=" + $("#current_project_id").val();
                        $.ajax({
                                url: val_url,
                                success: function(val, status, xhr) {
                                        // return codes : 0 = ok, 2 = authentication error, 3 = connexion error, 4 = the user doesn't have the rights
                                        // For no reason I understand, the string returned is "\n\n<ret_code>", so we cast it to int.
                                        val = parseInt(val);
                                        if (val == 0) {
                                                $("#ng6modal").modal('hide');
                                                //Run switch_space_id workflow

                                                $('#wfFormMigration').wfform('run');
                                                $("#myCarouselMigration").carousel('next');
                                                $("#myCarouselMigration").carousel('pause');
                                        } else {
                                                $("#modal-label-tmpl").html("Error");
                                                if (val == 2) {
                                                        $("#modal-body-tmpl").html("<div class='tx-nG6-pi1-error'>An error occurred during login. Most likely you didn't enter the username or password correctly. Be certain that you enter them precisely as they are, including upper/lower case.</div>");
                                                } else if (val == 3) {
                                                        $("#modal-body-tmpl").html("<div class='tx-nG6-pi1-error'>An error occurred during server connection. Please contact the site administrator.</div>");
                                                } else if(val == 4){
                                                        $("#modal-body-tmpl").html("<div class='tx-nG6-pi1-error'>The user you entered doesn't have the rights to perform this action.</div>");
                                                }
                                                $("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
                                                $("#ng6modal").modal();
                                        }
                                }
                        });
                });
        });
        //add status part.
        $('#wfFormMigration').on('run.wfform', function(event, running_wf) {
                $('#wfIdMigration').val(running_wf.id);
                workflow_status_handler(running_wf.id);
        });
        $("#refresh_workflow_migrate").click(function(){
                $('#wfStatusMigration').html("Workflow status : <span class='tx-nG6-mini-wait'></span>");
                workflow_status_handler($('#wfIdMigration').val());
        })
}

function showEditableFromTD(e) {
	e.stopPropagation();
	$(this).children('span.editable').editable('show');
}

function initEditableFields() {
    $('span.editable').editable({
    	toggle: 'manual',
    	display: function(value) {
    		if ($(this).html().indexOf('<a') == 0) {
    			var newlink = $($(this).html()).html(value);
        		$(this).html(newlink);
        		$(this).find('a').click(function() {
        	    	$('td.editable').unbind('click', showEditableFromTD);
        	    });
    		} else {
    			$(this).html(value);
    		}
    	}
    });
    $('.editable-date').editable({
    	toggle: 'manual',
    	format: 'yyyy-mm-dd',
    	viewformat: 'dd/mm/yyyy',
    	datepicker: {
    		weekStart: 1
    	}
	});
    $('td.editable').bind('click', showEditableFromTD);
}

function deletePRAHandler() {
	if ($(':checked[id^=chk_'+ $(this).attr("id").split("_")[1] +']').size() != 0) {
		var del_level = $(this).attr("id").split("_")[1];
		$("#modal-label-tmpl").html("Delete");
		$("#modal-body-tmpl").html("Are you sure you want to delete the selected elements ?");
		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">No</button>' +
			  					   '<button id="modal-btn-yes" class="btn btn-primary">Yes</button>');
		$("#ng6modal").modal();

		// Define yes button actions
		$("#modal-btn-yes").click( function() {
			var del_auth_html = [
                '<div class="tx-nG6-pi1-login">',
                '	This functionality is only available if you have an account on the ' + $("#server_name").val() + ' server. <br /> <br /> ',
                '	<form class="form-horizontal" id="tx-nG6-pi1-login-form" >',
                '		<div class="input-group" style="margin-left:50px">',
    		 	'			<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>',
    		 	'			<input class="form-control" style="width:200px" type="text" id="user_login_val" name="user_login_val" placeholder="Username"/>',
    		 	'		</div><br/>',
		 		'		<div class="input-group" style="margin-left:50px">',
			 	'			<span class="input-group-addon"><i class="glyphicon glyphicon-wrench"></i></span>',
			 	'			<input class="form-control" style="width:200px" type="password" id="user_pwd_val" name="user_pwd_val" placeholder="Password"/>',
			 	'		</div>',
			 	'	</form>',
			 	'</div>'
			].join('');
    		$("#modal-body-tmpl").html(del_auth_html);
    		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>' +
    								   '<button id="modal-btn-delete" class="btn btn-primary"><i class="glyphicon glyphicon-trash"></i> Delete</button>');
    		$("#ng6modal").modal();

    		$('.tx-nG6-pi1-login input').keypress(function(e){
    			if(e.which == 13) {
	    			// launch the delete request on server
	    		    var val_url = "index.php?eID=tx_nG6&type=delete&del_level="+del_level+"&data_folder=" + $("#data_folder").val();
	    		    val_url += "&user_login=" + $("#user_login_val").val() + "&user_pwd=" + encodeURIComponent($("#user_pwd_val").val());
	    		    val_url += "&user_id="+$("#user_id").val() + "&ids=";
		        	$(':checked[id^=chk_'+ del_level +']').each(function(i){
		        		val_url += $(this).val().split("_",2)[1]+";";
		     		});

	    			$("#modal-body-tmpl").html('<div class="tx-nG6-wait">Deleting, please wait...</div>');
	    			$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>' +
	    									   '<button id="modal-btn-delete" class="btn btn-primary disabled"><i class="glyphicon glyphicon-trash"></i> Delete</button>');
		    		$("#ng6modal").modal();

	    			val_url = val_url.substr(0, val_url.length-1);

	    		    $.ajax({
	    		    	url: val_url,
	    		    	success: function(val, status, xhr) {
	    		    		// return code : 0 = ok, 1 = right error, 2 = authentication error, 3 = connexion error
	    		    		val = parseInt(val);
	    		    		if (val == 0) {
	    	        			// Uncheck all checkboxes
	    	        			$(':checked').each(function(i){
	    	    		    		$(this).attr('checked',false);
	    	    		 		});
	    	    	    		$("#ng6modal").modal('hide');
	    		    			location.reload();
	    		    		} else {
	    		    			$("#modal-label-tmpl").html("Error");
	    		    			if (val == 1) {
	    		    				$("#modal-body-tmpl").html("<div class='tx-nG6-pi1-error'>You don't have write permission on the specified directory.</div>");
	    		    			} else if (val == 2) {
	    		    				$("#modal-body-tmpl").html("<div class='tx-nG6-pi1-error'>An error occurred during login. Most likely you didn't enter the username or password correctly. Be certain that you enter them precisely as they are, including upper/lower case.</div>");
	    		    			} else if (val == 3) {
	    		    				$("#modal-body-tmpl").html("<div class='tx-nG6-pi1-error'>An error occurred during server connection. Please contact the site administrator.</div>");
	    		    			}
	    		    			$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
	    		        		$("#ng6modal").modal();
	    		    		}
	    			   }
	    		   });
    			}
    		});

    		$("#modal-btn-delete").click(function(){
    			$(".tx-nG6-pi1-login input").focus();
    		    var e = jQuery.Event("keypress");
    		    e.which = 13;
    		    $(".tx-nG6-pi1-login input").trigger(e);
    		})
		});
    }
}

function hideHandler() {
	if ($(':checked[id^=chk_'+ $(this).attr("id").split("_")[1] +']').size() != 0) {
		var hide_level = $(this).attr("id").split("_")[1];
    	// First confirm with the user if he wants to hide
		$("#modal-label-tmpl").html("Hide");
		$("#modal-body-tmpl").html("Are you sure you want to hide the selected elements ?");
		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">No</button>' +
								   '<button id="modal-btn-yes" class="btn btn-primary">Yes</button>');
		$("#ng6modal").modal();

		// Define yes button actions
		$("#modal-btn-yes").click( function() {
    		// Built the url
    		var val_url = "index.php?eID=tx_nG6&type=hide";
        	val_url += "&hide_level="+hide_level;
        	val_url += "&user_id="+$("#user_id").val();
        	val_url += "&ids=";
        	$(':checked[id^=chk_'+ hide_level +']').each(function(i){
        		val_url += $(this).val().split("_",2)[1]+";";
     		});

        	$.ajax({
    	        url: val_url,
    	        success: function(val, status, xhr) {
        			$(':checked[id^=chk_'+ hide_level +']').each(function(i){
        				if ($(this).parent().parent().parent().children("td:nth-child(2)").children("span.label-warning").length > 0) {
        					$(this).parent().parent().parent().children("td:nth-child(2)").children("span.label-warning").remove();
        				}
        				if ($(this).parent().parent().parent().children("td:nth-child(2)").children("span.label-info").length == 0) {
        					if ($(this).parent().parent().parent().children("td:nth-child(2)").children("span").length == 0) {
        						$(this).parent().parent().parent().children("td:nth-child(2)").prepend(" <span class='label label-info'><i class='glyphicon glyphicon-eye-close'></i></span> ");
        					} else {
        						$(this).parent().parent().parent().children("td:nth-child(2)").children("span").before(" <span class='label label-info'><i class='glyphicon glyphicon-eye-close'></i></span> ");
        					}

        				}
        			});
        			// Uncheck all checkboxes
        			$(':checked').each(function(i){
    		    		$(this).attr('checked',false);
    		    		updateButtonStatus();
    		 		});
    	    		$("#ng6modal").modal('hide');
    	        }
    	    });
    	});
	}
}

function unhideHandler() {
	if ($(':checked[id^=chk_'+ $(this).attr("id").split("_")[1] +']').size() != 0) {
		var unhide_level = $(this).attr("id").split("_")[1];
    	// First confirm with the user
		$("#modal-label-tmpl").html("Unhide");
		$("#modal-body-tmpl").html("Are you sure you want to unhide the selected elements ?");
		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">No</button>' +
								   '<button id="modal-btn-yes" class="btn btn-primary">Yes</button>');
		$("#ng6modal").modal();

		// Define yes button actions
		$("#modal-btn-yes").click( function() {
    		// Built the url
    		var val_url = "index.php?eID=tx_nG6&type=unhide";
        	val_url += "&unhide_level="+unhide_level;
        	val_url += "&user_id="+$("#user_id").val();
        	val_url += "&ids=";
        	$(':checked[id^=chk_'+ unhide_level +']').each(function(i){
        		val_url += $(this).val().split("_",2)[1]+";";
     		});

        	$.ajax({
    	        url: val_url,
    	        success: function(val, status, xhr) {
        			$(':checked[id^=chk_'+ unhide_level +']').each(function(i){
        				$(this).parent().parent().parent().children("td:nth-child(2)").children("span.label-info").remove();
        			});
        			// Uncheck all checkboxes
        			$(':checked').each(function(i){
    		    		$(this).attr('checked',false);
    		    		updateButtonStatus();
    		 		});
    	    		$("#ng6modal").modal('hide');
    	        }
    	    });
    	});
	}
}

function unpublishHandler() {
	if ($(':checked[id^=chk_project_]').size() != 0) {
    	// First confirm with the user
		$("#modal-label-tmpl").html("Unpublish");
		$("#modal-body-tmpl").html("Are you sure you want to unpublish the selected project(s) ?<br/><br/>When done analysis and data related to the project(s) will be no longer visible by all users.");
		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">No</button>' +
							       '<button id="modal-btn-yes" class="btn btn-primary">Yes</button>');
		$("#ng6modal").modal();

		// Define yes button actions
		$("#modal-btn-yes").click( function() {
			// Built the url
    		var val_url = "index.php?eID=tx_nG6&type=unpublish";
        	val_url += "&user_id="+$("#user_id").val();
        	val_url += "&ids=";
        	$(':checked[id^=chk_project_]').each(function(i){
        		val_url += $(this).val().split("_",2)[1]+";";
     		});

        	$.ajax({
    	        url: val_url,
    	        success: function(val, status, xhr) {
        			$(':checked[id^=chk_project_]').each(function(i){
        				$(this).parent().parent().parent().children("td:nth-child(2)").children("span.label-warning").remove();
        			});
        			// Uncheck all checkboxes
        			$(':checked').each(function(i){
    		    		$(this).attr('checked',false);
    		    		updateButtonStatus();
    		 		});
    	    		$("#ng6modal").modal('hide');
    	        }
    	    });
    	});
	}
}

function publishHandler () {
	if ($(':checked[id^=chk_project_]').size() != 0) {
		// First confirm with the user
		$("#modal-label-tmpl").html("Publish");
		$("#modal-body-tmpl").html("Are you sure you want to publish the selected project(s) ?<br/><br/> When done analysis and data related to the project(s) will be visible by all users.");
		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">No</button>' +
								   '<button id="modal-btn-yes" class="btn btn-primary">Yes</button>');
		$("#ng6modal").modal();

		// Define yes button actions
		$("#modal-btn-yes").click( function() {
    		// Built the url
    		var val_url = "index.php?eID=tx_nG6&type=publish";
        	val_url += "&user_id="+$("#user_id").val();
        	val_url += "&ids=";
        	$(':checked[id^=chk_project_]').each(function(i){
        		val_url += $(this).val().split("_",2)[1]+";";
     		});

        	$.ajax({
    	        url: val_url,
    	        success: function(val, status, xhr) {
        			$(':checked[id^=chk_project_]').each(function(i){
        				if ($(this).parent().parent().parent().children("td:nth-child(2)").children("span.label-warning").length == 0) {
        					$(this).parent().parent().parent().children("td:nth-child(2)").prepend(" <span class='label label-warning'><i class='glyphicon glyphicon-thumbs-up'></i></span> ");
        				}
        				if ($(this).parent().parent().parent().children("td:nth-child(2)").children("span.label-info").length > 0) {
        					$(this).parent().parent().parent().children("td:nth-child(2)").children("span.label-info").remove();
        				}
        			});
        			// Uncheck all checkboxes
        			$(':checked').each(function(i){
    		    		$(this).attr('checked',false);
    		    		updateButtonStatus();
    		 		});
    	    		$("#ng6modal").modal('hide');
    	        }
    	    });
    	});
	}
}


function runWFHandler(event){
	var selector = event.data.selector;
	$(selector).wfform('run');

	if(event.data.hasOwnProperty('callback')){
		event.data.callback();
	}
}

function resetWFHandler(event){
	var selector = event.data.selector;
	$(selector).wfform('reset');

	if(event.data.hasOwnProperty('callback')){
		event.data.callback();
	}
}

function refreshWFStatusHandler(event){
	var selector = event.data.selector;
	$(selector).wfstatus('reload');

	if(event.data.hasOwnProperty('callback')){
		event.data.callback();
	}
}

function rerunWFHandler(event){
	var selector = event.data.selector;
	$(selector).wfstatus('rerun');
	$(selector).wfstatus('reload');

	if(event.data.hasOwnProperty('callback')){
		event.data.callback();
	}
}

function outputWFHandler(event){
	var selector = event.data.selector,
		workflowID =  event.data.workflow_id;
	$(selector).wfoutputs({	workflowID: workflowID });

	if(event.data.hasOwnProperty('callback')){
		event.data.callback();
	}
}

function refreshAnalysisWorkflowHandler(event) {
	refreshWFStatusHandler(event);
	refreshAnalysisTable();
}
function refreshRunWorkflowHandler(event) {
	refreshWFStatusHandler(event);
	refreshRunTable();
}

function unbindAll() {
	$("#reset_workflow").unbind('click', resetWFHandler);
	$("#run_workflow").unbind('click', runWFHandler);
	$("#rerun_workflow").unbind('click', rerunWFHandler);
	$("#refresh_workflow").unbind('click', refreshAnalysisWorkflowHandler);
	$("#refresh_workflow").unbind('click', refreshRunWorkflowHandler);
	$("#refresh_workflow").unbind('click', refreshWFStatusHandler);
}

function refreshRunTable() {
    var params = simpleQueryParams(location.href),
		page_id = params["id"];
    $.ajax({
	    url: 'index.php?eID=tx_nG6&type=runs_table&project_id='+$("#current_project_id").val()+'&user_id='+$("#user_id").val()+'&login_user='+$("#login_user").val()+'&page_id='+page_id,
	    dataType: "html",
	    error: function (xhr, ajaxOptions, thrownError) {
	    	var html = '<div class="alert alert-danger"><strong>Error!</strong> An error occured when attempting to reload the runs table.</div>';
	    	$("#runs").html(html);
	    },
	    success: function(data) {
	    	if (data) {
	    		$("#runs").html(data);
	    		initEditableFields();
	    		initCheckboxes();
	    		updateRunButtonStatus();
	    		$("[id^=chk_run_]").change(function() {
	    			updateRunButtonStatus();
	    		});
	    		$("#chk_all_run").change(function() {
	    			updateRunButtonStatus();
	    		});
		    	var runTable = initRunTable();
		    	$('#delete_run').on('click', deletePRAHandler);
		    	$("#hide_run").on('click', hideHandler);
		    	$("#unhide_run").on('click', unhideHandler);
		    	$("#add_run").on('click', addRunHandler);
		    	addWorkflowsStatusOnRun();
	    	} else {
	    		var html = '<div class="alert alert-danger"><strong>Error!</strong> An error occured when attempting to reload the runs table.</div>';
		    	$("#runs").html(html);
	    	}
	    }
    });
}

function addRunHandler() {
	$('#setAndRunModalLabel').html("Add a run <small> from a workflow execution</small>");
	$('#setAndRunModalBody').html([
	'    <div id="myCarousel" class="carousel slide">',
	'        <div class="carousel-inner">',
	'            <div id="availableWorkflows" class="active item"></div>',
	'            <div id="wfForm" class="item"></div>',
	'            <div id="monitoringWorkflow" class="item"></div>',
	'        </div>',
	'    </div>'
	].join(''));
	$('#setAndRunModalFooter').html([
	'    <div class="btn-group">',
	'        <button id="close" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Close</button>',
	'    </div>',
	'    <div class="btn-group">',
	'        <button id="refresh_workflow" class="btn btn-default"><i class="glyphicon glyphicon-refresh"></i> Refresh</button>',
	'        <button id="rerun_workflow" class="btn btn-primary"><i class="glyphicon glyphicon-cog"></i> Rerun</button>',
	'    </div>',
	'    <div class="btn-group">',
	'        <button id="back_to_workflows_list" class="btn btn-default"><i class="glyphicon glyphicon-arrow-left"></i> Back</button>',
	'        <button id="reset_workflow" class="btn btn-default"><i class="glyphicon glyphicon-refresh"></i> Reset</button>',
	'        <button id="run_workflow" class="btn btn-primary"><i class="glyphicon glyphicon-cog"></i> Run</button>',
	'    </div>'
	].join(''));

	$('#myCarousel').on('keydown', 'input', function(e) {
	  e.stopPropagation();
	});

	$("#refresh_workflow, #rerun_workflow, #reset_workflow, #run_workflow, #back_to_workflows_list").hide();
	$("#close").show();
	$('#availableWorkflows').html("wait");
	$('#setAndRunModal').modal();

	$('#availableWorkflows').availablewf({
		filter_groups : ['addrun'],
		select : true,
		serverURL: $("#server_url").val()
	});

	$('#availableWorkflows').on('select.availablewf', function(event, workflow) {
		$("#myCarousel").carousel('next');
		$("#myCarousel").carousel('pause');
		$("#reset_workflow, #run_workflow, #back_to_workflows_list").show();
		$("#close").hide();
	    $("#back_to_workflows_list").click(function(){
			$("#reset_workflow, #run_workflow, #back_to_workflows_list").hide();
	    	$("#close").show();
	    	$("#myCarousel").carousel('prev');
	    });
    	$('#setAndRunModalLabel').html(workflow["name"] + " <small>" + workflow["help"] + "</small>");
    	var parameters = {}
    	if ($("#view").val() == "project") {
    		parameters = {"project_id": $("#ids").val()};
    		parameters = {"project_name": $("#current_project_name").val()};
    	}
    	parameters["admin_login"] = $("#user_login").val();

	    $('#wfForm').wfform({
			workflowClass: workflow["class"],
			displayRunButton: false,
			displayResetButton: false,
			parameters: parameters,
			serverURL: $("#server_url").val()
	    });
	});
    $('#wfForm').on('run.wfform', function(event, running_wf) {
    	$("#reset_workflow, #run_workflow, #back_to_workflows_list").hide();
    	$("#refresh_workflow, #rerun_workflow").show();
		$("#myCarousel").carousel('next');
		$("#myCarousel").carousel('pause');
	    $('#setAndRunModalLabel').html(running_wf["name"] + " <small>" + running_wf["id"] + "</small>");
	    $('#monitoringWorkflow').wfstatus({
	    	workflowID: running_wf.id,
	    	forceUsingWorkflow: running_wf,
	    	serverURL: $("#server_url").val()
	    });
	    refreshRunTable();
    });
    unbindAll();
    $("#reset_workflow").bind('click', {'selector' : '#wfForm' }, resetWFHandler);
    $("#run_workflow").bind('click', {'selector' : '#wfForm' }, runWFHandler);
    $("#rerun_workflow").bind('click', {'selector' : '#monitoringWorkflow' }, rerunWFHandler);
    $("#refresh_workflow").bind('click', {'selector' : '#monitoringWorkflow' }, refreshRunWorkflowHandler);
}

function addProjectHandler(){
	var params = simpleQueryParams(location.href),
	page_id = params["id"];

	$('#setAndRunModalLabel').html("Loading");

	$('#setAndRunModalFooter').html([
	'    <div class="btn-group">',
	'        <button id="close" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Close</button>',
	'    </div>',
	'    <div class="btn-group">',
	'        <button id="reset_workflow" class="btn btn-default"><i class="glyphicon glyphicon-refresh"></i> Reset</button>',
	'        <button id="run_workflow" class="btn btn-primary"><i class="glyphicon glyphicon-cog"></i> Run</button>',
	'    </div>'
    ].join(''));

	$("#reset_workflow, #run_workflow").hide();
	$("#close").show();

	$('#setAndRunModalBody').on("loaded.wfform", function(event, workflow) {
		$('#setAndRunModalLabel').html(workflow["name"] + " <small>" + workflow["help"] + "</small>");
		$("#reset_workflow, #run_workflow").show();
		$("#close").hide();
	});
    $('#setAndRunModalBody').wfform({
		workflowClass: "AddProject",
		displayRunButton: false,
		displayResetButton: false,
		parameters: {"admin_login": $("#user_login").val()},
		serverURL: $("#server_url").val()
    });
    $('#setAndRunModalBody').on('run.wfform', function(event, running_wf) {
    	$.ajax({
		    url: 'index.php?eID=tx_nG6&type=projects_table&user_id='+$("#user_id").val()+'&login_user='+$("#login_user").val()+'&page_id='+page_id,
		    dataType: "html",
		    error: function (xhr, ajaxOptions, thrownError) {
		    	var html = '<div class="alert alert-danger"><strong>Error!</strong> An error occured when attempting to reload the projects table.</div>';
		    	$("#projects").html(html);
		    },
		    success: function(data) {
		    	if (data) {
		    		$("#projects").html(data);
		    		initEditableFields();
		    		initCheckboxes();
		    		updateProjectButtonStatus();
		    		$("[id^=chk_project_]").change(function() {
		    			updateProjectButtonStatus();
		    		});
		    		$("#chk_all_project").change(function() {
		    			updateProjectButtonStatus();
		    		});
		    		var projectTable = initProjectTable();
			    	$('#delete_project').on('click', deletePRAHandler);
			    	$("#hide_project").on('click', hideHandler);
			    	$("#unhide_project").on('click', unhideHandler);
			        $("#unpublish_project").on('click', unpublishHandler);
			        $("#publish_project").on('click', publishHandler);
			    	$("#add_project").on('click', addProjectHandler);
			        $('#setAndRunModal').modal('hide');
		    	} else {
		    		var html = '<div class="alert alert-danger"><strong>Error!</strong> An error occured when attempting to reload the projects table.</div>';
			    	$("#projects").html(html);
		    	}
		    }
	    });
    });
    $('#setAndRunModal').modal();
    unbindAll();
    $("#reset_workflow").bind('click',  {'selector' : '#setAndRunModalBody' }, resetWFHandler);
  	$("#run_workflow").bind('click',  {'selector' : '#setAndRunModalBody' }, runWFHandler);
}

function refreshAnalysisTable() {
    var params = simpleQueryParams(location.href),
		page_id = params["id"],
		run_id = params["tx_nG6_pi1[run_id]"],
		project_id = params["tx_nG6_pi1[project_id]"],
		parameters = {};
    // define default parameters so the user do not have to fill them
    if (run_id) {
    	parameters["run_id"] = run_id;
    	parameters["project_id"] = '';
    } else if (project_id) {
    	parameters["project_id"] = project_id;
    	parameters["run_id"] = '';
    }
	$.ajax({
	    url: 'index.php?eID=tx_nG6&type=analyses_table&user_id='+$("#user_id").val()+'&login_user='+$("#login_user").val()+'&page_id='+page_id+"&project_id="+parameters["project_id"]+"&run_id="+parameters["run_id"],
	    dataType: "html",
	    error: function (xhr, ajaxOptions, thrownError) {
	    	var html = '<div class="alert alert-danger"><strong>Error!</strong> An error occured when attempting to reload the analysis table.</div>';
	    	$("#analyses").html(html);
	    },
	    success: function(data) {
	    	if (data) {
	    		$("#analyses").html(data);
	    		initEditableFields();
	    		initCheckboxes();
	    		updateAnalysisButtonStatus();
	    		$("[id^=chk_analysis_]").change(function() {
	    			updateAnalysisButtonStatus();
	    		});
	    		$("#chk_all_analysis").change(function() {
	    			updateAnalysisButtonStatus();
	    		});
	    		var analysisTable = initAnalysisTable();
		    	$('#delete_analysis').on('click', deletePRAHandler);
		    	$("#hide_analysis").on('click', hideHandler);
		    	$("#unhide_analysis").on('click', unhideHandler);
		    	$("#add_analysis").on('click', addAnalysisHandler);
	    	} else {
	    		var html = '<div class="alert alert-danger"><strong>Error!</strong> An error occured when attempting to reload the analysis table.</div>';
		    	$("#analyses").html(html);
	    	}
	    }
    });
}

function addFileHandler( event ){
	$('#setAndRunModalLabel').html("Loading");
	$('#setAndRunModalBody').html([
	'    <div id="myCarousel" class="carousel slide">',
	'        <div class="carousel-inner">',
	'            <div id="wfForm" class="active item"></div>',
	'            <div id="monitoringWorkflow" class="item"></div>',
	'        </div>',
	'    </div>'
	].join(''));

	$('#setAndRunModalFooter').html([
	'    <div class="btn-group">',
	'        <button id="close" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Close</button>',
	'    </div>',
	'    <div class="btn-group">',
	'        <button id="refresh_workflow" class="btn btn-default"><i class="glyphicon glyphicon-refresh"></i> Refresh</button>',
	'        <button id="rerun_workflow" class="btn btn-primary"><i class="glyphicon glyphicon-cog"></i> Rerun</button>',
	'    </div>',
	'    <div class="btn-group">',
	'        <button id="reset_workflow" class="btn btn-default"><i class="glyphicon glyphicon-refresh"></i> Reset</button>',
	'        <button id="run_workflow" class="btn btn-primary"><i class="glyphicon glyphicon-cog"></i> Run</button>',
	'    </div>'
	].join(''));

	$("#reset_workflow, #run_workflow, #refresh_workflow, #rerun_workflow").hide();

    var params = simpleQueryParams(location.href),
		page_id = params["id"],
		run_id = params["tx_nG6_pi1[run_id]"],
		analysis_id = params["tx_nG6_pi1[analyze_id]"],
		parameters = {};

    // define default parameters so the user do not have to fill them
    if (analysis_id) {
    	parameters["analysis_id"] = analysis_id;
    	parameters["run_id"] = null;
    }
    else if (run_id) {
    	parameters["run_id"] = run_id;
    	parameters["analysis_id"] = null;
    }
	parameters["admin_login"] = $("#user_login").val();

	$('#myCarousel').on('keydown', 'input', function(e) {
	  e.stopPropagation();
	});

	$("#close").show();
	$('#wfForm').on("loaded.wfform", function(event, workflow) {
		$('#setAndRunModalLabel').html(workflow["name"] + " <small>" + workflow["help"] + "</small>");
		$("#reset_workflow, #run_workflow").show();
		$("#close").hide();
	});

    $('#wfForm').wfform({
		workflowClass: "AddFiles",
		displayRunButton: false,
		displayResetButton: false,
		parameters: parameters,
		serverURL: $("#server_url").val()
    });

    $('#wfForm').on('run.wfform', function(event, running_wf) {
		$("#reset_workflow, #run_workflow").hide();
		$("#refresh_workflow, #rerun_workflow").show();
		$("#myCarousel").carousel('next');
		$("#myCarousel").carousel('pause');
	    $('#setAndRunModalLabel').html(running_wf["name"] + " <small>" + running_wf["id"] + "</small>");
	    $('#monitoringWorkflow').wfstatus({
	    	workflowID: running_wf.id,
	    	forceUsingWorkflow: running_wf,
	    	serverURL: $("#server_url").val()
	    });
	    refreshAnalysisTable();
    });

    $('#setAndRunModal').modal();
    unbindAll();

    $("#reset_workflow").bind('click', {'selector' : '#wfForm' }, resetWFHandler);
    $("#run_workflow").bind('click', {'selector' : '#wfForm' }, runWFHandler);
    $("#rerun_workflow").bind('click', {'selector' : '#monitoringWorkflow' }, rerunWFHandler);
    $("#refresh_workflow").bind('click', {'selector' : '#monitoringWorkflow' }, refreshWFStatusHandler);
}

function addAnalysisHandler(){
	$('#setAndRunModalLabel').html("Loading");
	$('#setAndRunModalBody').html([
   	'    <div id="myCarousel" class="carousel slide">',
	'        <div class="carousel-inner">',
	'            <div id="wfForm" class="active item"></div>',
	'            <div id="monitoringWorkflow" class="item"></div>',
	'        </div>',
	'    </div>'
	].join(''));

	$('#setAndRunModalFooter').html([
 	'    <div class="btn-group">',
	'        <button id="close" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Close</button>',
	'    </div>',
	'    <div class="btn-group">',
	'        <button id="refresh_workflow" class="btn btn-default"><i class="glyphicon glyphicon-refresh"></i> Refresh</button>',
	'        <button id="rerun_workflow" class="btn btn-primary"><i class="glyphicon glyphicon-cog"></i> Rerun</button>',
	'    </div>',
	'    <div class="btn-group">',
	'        <button id="reset_workflow" class="btn btn-default"><i class="glyphicon glyphicon-refresh"></i> Reset</button>',
	'        <button id="run_workflow" class="btn btn-primary"><i class="glyphicon glyphicon-cog"></i> Run</button>',
	'    </div>'
	].join(''));

	$('#myCarousel').on('keydown', 'input', function(e) {
	  e.stopPropagation();
	});

	$("#reset_workflow, #run_workflow, #refresh_workflow, #rerun_workflow").hide();
	$("#close").show();
//
    var params = simpleQueryParams(location.href),
		page_id = params["id"],
		run_id = params["tx_nG6_pi1[run_id]"],
		project_id = params["tx_nG6_pi1[project_id]"],
		parameters = {};

    // define default parameters so the user do not have to fill them
    if (run_id) {
    	parameters["run_id"] = run_id;
    	parameters["project_id"] = null;
    } else if (project_id) {
    	parameters["project_id"] = project_id;
    	parameters["run_id"] = null;
    }
	parameters["admin_login"] = $("#user_login").val();

	$('#wfForm').on("loaded.wfform", function(event, workflow) {
		$('#setAndRunModalLabel').html(workflow["name"] + " <small>" + workflow["help"] + "</small>");
		$("#reset_workflow, #run_workflow").show();
		$("#close").hide();
	});

    $('#wfForm').wfform({
		workflowClass: "AddAnalysis",
		displayRunButton: false,
		displayResetButton: false,
		parameters: parameters,
		serverURL: $("#server_url").val()
    });

    $('#wfForm').on('run.wfform', function(event, running_wf) {
		$("#reset_workflow, #run_workflow").hide();
		$("#refresh_workflow, #rerun_workflow").show();
		$("#myCarousel").carousel('next');
		$("#myCarousel").carousel('pause');
	    $('#setAndRunModalLabel').html(running_wf["name"] + " <small>" + running_wf["id"] + "</small>");
	    $('#monitoringWorkflow').wfstatus({
	    	workflowID: running_wf.id,
	    	forceUsingWorkflow: running_wf,
	    	serverURL: $("#server_url").val()
	    });
	    refreshAnalysisTable();
    });

    $('#setAndRunModal').modal();
    unbindAll();

    $("#reset_workflow").bind('click', {'selector' : '#wfForm' }, resetWFHandler);
    $("#run_workflow").bind('click', {'selector' : '#wfForm' }, runWFHandler);
    $("#rerun_workflow").bind('click', {'selector' : '#monitoringWorkflow' }, rerunWFHandler);
    $("#refresh_workflow").bind('click', {'selector' : '#monitoringWorkflow' }, refreshAnalysisWorkflowHandler);
}
