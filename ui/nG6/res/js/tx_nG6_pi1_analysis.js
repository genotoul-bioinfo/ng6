/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */

/* 
 * Resize center modal element.
 * @param String - Id of the element.
 */
function vertical_centering( id ) {
       	var windows_height = $(window).height() ;
       	var modal_height   = $('#'+id).height() ;
	    var margin_top_height = -(modal_height)/2 ;
		$('#'+id).css('top', '50%');
		$('#'+id).css('margin-top', margin_top_height+'px');
}

/*
 * Return the maximum pixel size for a modal element.
 * @param int - Size of gap between element and windows borders.
 * @param int - Min size.
 */
function modal_height(substract, min) {
	var windows_height = $(window).height() ;
    var body_height = windows_height - substract ;
    if( body_height < min ) {
    	body_height = min ;
    }
	
	return body_height ;
}

/*
 * resize bootstrap modal width and center it
 * @param selector : modal body selector
 * @param width : the width the modal must have
 */
function resize_center_btmodal(selector, width){
	var margin_left = (600 - width) / 2 ;
	$(selector).on('show.bs.modal', function () {
	    $(this).find('.modal-content').css({
			width 			: width + 'px', 
			'margin-left' 	: margin_left + 'px' 
	    });
	});
}


$(function () {
	
	// Grab relevant info from URL fragment
    var params = simpleQueryParams(location.href),
    	active_tab =  params["active_tab"];

	if (active_tab) {
		$('#myTab a[href="#'+active_tab+'"]').tab('show');
	}
	
    $(".file-display").each(function(i){
    	$(this).attr("id", "file_display_"+i);
    	var id = "file_display_"+i;
    	$.ajax({
            url: $(this).html(),
            dataType: "text",
            success: function(val, status, xhr) {
    			lines = val.split("\n");
    			var html_val = "";
    			for (j=0; j<lines.length; j++) {
    				html_val += lines[j] + "<br />";
    			}
    			$("#file_display_"+i).html(html_val);
            }
        });
	});
    
	$(".imglink").click(function() {
		var img_href = $(this).attr('href') ;
		var imgLoad = $("<img />")
			.attr("src", img_href)
			.attr('alt', 'Cannot display')
			.unbind("load")
			.bind("load", function () {
				var imgwidth = ( this.width > 900 ) ? 900 : this.width ;
				resize_center_btmodal('#ng6modal',imgwidth + 50 );
				$("#modal-label-tmpl").html("NG6 <small> " + $("#analyse_name").val() + "</small>");
				$("#modal-body-tmpl").html('<div id="img_container" style="overflow-x : auto"><img src="' + img_href + '" alt="Cannot display"></div>');
				$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
				$("#ng6modal").modal();
			});

       	return false;
	});
    
    function updateButtonStatus() {
    	$('.single-selection-btn').each(function(){
           	if ($(":checked[id^=chk_sample_]").size() != 1) {
           		// Add tooltip
                $(this).attr('rel', 'tooltip');
                $(this).attr('data-title', 'Select only one element');
                $(this).tooltip({container: 'body', placement: 'bottom'});
                // Disable button
	        	$(this).addClass("disabled").attr("rel", "tooltip");
	        } else {
	        	$(this).removeClass("disabled").attr("rel", null);
	        	// Remove tooltip
	        	$(this).removeAttr('rel');
                $(this).removeAttr('data-title');
                $(this).tooltip('destroy')
	        }
        });
    	$('.multiple-selection-btn').each(function(){
           	if ($(":checked[id^=chk_sample_]").size() == 0) {
           		// Add tooltip
                $(this).attr('rel', 'tooltip');
                // Disable button
                $(this).attr('data-title', 'Select one or more element(s)');
                $(this).tooltip({container: 'body', placement: 'bottom'});
	        	$(this).addClass("disabled").attr("rel", "tooltip");
	        } else {
	        	// Enable
	        	$(this).removeClass("disabled").attr("rel", null);
	        	// Remove tooltip
	        	$(this).removeAttr('rel');
                $(this).removeAttr('data-title');
                $(this).tooltip('destroy')
	        }
        });
    	$('.multiple5-selection-btn').each(function(){
           	if ( ($(":checked[id^=chk_sample_]").size() <= 5)  && ($(":checked[id^=chk_sample_]").size() > 0) ) {
	        	// Enable
	        	$(this).removeClass("disabled").attr("rel", null);
	        	// Remove tooltip
	        	$(this).removeAttr('rel');
                $(this).removeAttr('data-title');
                $(this).tooltip('destroy')
	        } else {
	        	// Add tooltip
                $(this).attr('rel', 'tooltip');
                // Disable button
                $(this).attr('data-title', 'Select between 1 and 5 element(s)');
                $(this).tooltip({container: 'body', placement: 'bottom'});
	        	$(this).addClass("disabled").attr("rel", "tooltip");
	        }
        });
    }
    
    updateButtonStatus();
    $("[id^=chk_sample_]").change(function() {
    	updateButtonStatus();
    });
    $("#chk_all_sample").change(function() {
    	updateButtonStatus();
    });
    
    $('.analysis-result-table').each(function(){
    	// Retrieve columns types
    	var th_types = new Array();
    	if ($(this).children("thead").children("tr").length >= 1) {
			$(this).children("thead").children("tr").each(function(){
				$(this).children("th").each(function(){
					
					if ($(this).attr("colspan") == undefined) {
						if (($(this).hasClass("numeric-sort") == true) || ($(this).hasClass("string-sort") == true)) {
							th_types.push({"type": $(this).attr("class").split("-")[0]});
						} else {
							th_types.push(null);
						}
					}
				});
			});
    	} else {
    		console.log("Parsing ERROR : no tr was found in the thead");
    	}
	
    	// Build dataTable
    	if ($(this).children("tbody").children("tr").children("td:has(input:checkbox)").length > 0) {
			$(this).DataTable({
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"scrollX"		: "90%",
				"sScrollXInner"	: "100%",
	  			"order"		: [[1, 'desc']],
			    "columns"	: th_types ,
				"columnDefs": [ { "orderable": false, "targets":  0  } ]
		  	});
	  	} else {
		    $(this).DataTable({
		    	"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		    	"scrollX"		: "90%",
				"sScrollXInner"	: "100%",
	  			"order"		: [[1, 'desc']],
			    "columns"	: th_types ,
		  	});
	  	}
  	});
  	
    // init editable 
    $('.editable-block .editable-block-content').editable({
    	toggle: 'manual'
    });
    
    $('.editable-block').click(function(e){
    	e.stopPropagation();
    	$(this).children('.editable-block-content').editable('option', 'container', 'body');
    	$(this).children('.editable-block-content').editable('option', 'success', function() {
			location.assign(location.href+"&active_tab=parameters");
 	   	});
    	$(this).children('.editable-block-content').editable('show');
    });
    $('.editable-block-content').click(function(e){
    	e.stopPropagation();
    	$(this).editable('option', 'success', function() {
			location.assign(location.href+"&active_tab=parameters");
 	   	});
    	$(this).editable('option', 'container', 'body');
    	$(this).editable('show');
    })
    
    //auto active tab
    if (!active_tab){
    	var href = $('#myTab > li').removeClass( "active" ).first().addClass( "active" ).children("a:first").attr('href');
        $('#myTabContent > div.tab-pane.fade').removeClass( "in active" );
        $(href).addClass( "in active" );
    }
    
    // Add numeric with white space in sort
	jQuery.extend( jQuery.fn.dataTableExt.oSort, {
		"numeric-pre": function ( a ) {
			a = (a === " " || a === "") ? 0 : a.replace( /[\s]/g, "" );
			return parseFloat( a );
		},

		"numeric-asc": function ( a, b ) {
			return a - b;
		},

		"numeric-desc": function ( a, b ) {
			return b - a;
		}
	});
});
