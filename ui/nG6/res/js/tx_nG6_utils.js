/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

function simpleQueryParams(s) {
    try {
    	if (!s) return null;
    	var url = s.split('?'), arr = url[1].split('&'), n = arr.length, dict = {}; 
    	while(n--) { 
    		var me = arr[n]; 
    		me = me.split('='); 
    		dict[me[0].replace(/\%5B/g, '[').replace(/\%5D/g, ']')] = decodeURIComponent(me[1]).replace(/\+/g, ' ');
    	}
    	return dict;
    } catch (err) {
    	return {};
    }
}
/* 
 * Does the string ends with the specified pattern
 * @param str the string to test
 * @param pattern the pattern to test
 * @return boolean True if str ends with the specified pattern, False otherwise
 */
function endsWith(str, pattern) {
	return (str.match(pattern+"$")==pattern);
}

// Password generator
function key_gen(size) {
	var chars='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	var pass='';
	while(pass.length < size) {
		pass+=chars.charAt(Math.round(Math.random() * (chars.length)))
	}
	return pass;
}

/* 
 * Resize the HTML element.
 * @param HTMLElement - The element to resize.
 * @param float - The percentage of the initial size of the element.
 */
function resize(elt, ratio) {
	elt.width = ratio*elt.width ;
}

function get_octet_string_representation (size, round) {
	var octets_link = new Array("bytes", "Kb", "Mb", "Gb", "Tb", "Pb", "Eb", "Zb"),
		p = parseInt(Math.ceil(parseFloat(size.toString().length)/parseFloat(3) - parseFloat(1))),
		pow_needed = p * 10;
	pow_needed = Math.pow(2, pow_needed);
	value = parseFloat(size)/parseFloat(pow_needed);
	var tmp = value.toString().split(".");
	if (tmp.length > 1 && round > 0) {
		value = tmp[0] + "."  + tmp[1].slice(0, round);
	} else if (tmp.length > 1 && round == 0) {
		value = tmp[0];
	}
        value = value + " " + octets_link[p];  
	return value;
}
