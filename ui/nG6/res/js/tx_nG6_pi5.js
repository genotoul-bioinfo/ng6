/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */

﻿$.validator.setDefaults({
    highlight: function (element, errorClass, validClass) {
    	var $element;
		if ( element.type === "radio" ) {
			$element = this.findByName(element.name);
		} else {
			$element = $(element);
		}
        // add the bootstrap error class
        $element.parents("div.form-group").addClass("has-error");
		if ($element.parent("div.input-append").find(".btn").length > 0) {
			$element.parent("div.input-append").find(".btn").addClass("btn-danger");
			$element.parent("div.input-append").find(".glyphicon-wrench").addClass("icon-white");
		}
    },
    unhighlight: function (element, errorClass, validClass) {
        var $element;
        if (element.type === 'radio') {
            $element = this.findByName(element.name);
        } else {
            $element = $(element);
        }
        $element.removeClass(errorClass).addClass(validClass);
        // remove the bootstrap error class
        if ($element.parents("div.form-group").find("." + errorClass).length == 0) {  
            // Only remove the class if there are no other errors
            $element.parents("div.form-group").removeClass(errorClass).addClass(validClass);
        }
		if ($element.parent("div.input-append").find(".btn").length > 0) {
			$element.parent("div.input-append").find(".btn").removeClass("btn-danger");
			$element.parent("div.input-append").find(".glyphicon-wrench").removeClass("icon-white");
		}
    },
    errorPlacement: function(error, element) {}
});

$(function () {
	
	var _get_workflow_status = function(workflow_id, callback, error_callback){
		 $.ajax({
            url : $("#server_url").val() + '/get_workflow_status?display=list&workflow_id=' + workflow_id ,
            dataType : 'jsonp',
            timeout: 20000 , 
            success : function(data){
	        	if (callback){
                    callback(data);
                }
	        },
	        error : function(jqXHR, textStatus, errorThrown){
            	if (error_callback){
            		error_callback(jqXHR, textStatus, errorThrown);
            	}
	        }
        });
	};	
	
	var _retrieve_data = function(workflow_id, callback, error_callback){
		console.log("IN _retrieve_data, workflow_id=" + workflow_id);
    	$.ajax({
            url : $("#server_url").val() + '/get_workflow_outputs?workflow_id=' + workflow_id,
            dataType : 'jsonp',
            success : function(data){
            	if (callback) {
            		callback(data);
            	}
            },
            error : function(jqXHR, textStatus, errorThrown){
            	if (error_callback){
            		error_callback(jqXHR, textStatus, errorThrown);
            	}
            }
        });
	};
	
	var _display_file_content = function(filepath, $elmt, process_data){
		$.get(filepath, function(data){
			if (process_data) {
				data = process_data(data);
			}
			$elmt.html([
                '<div class="alert">',
                '    <p>',
                '        <strong>Success !</strong> You will find bellow the list of paths for all your datas.',
                '        Use the <i class="glyphicon glyphicon-floppy-save"></i> button to copy all paths to the clipboard.<br/>',
                '    </p>',
                '</div>',
                '<div>',
                '    <button id="copy_urls" class="btn btn-default btn-sm pull-right" data-clipboard-target="url_filelist">',
                '        <i class="glyphicon glyphicon-floppy-save"></i>&nbsp;',
                '    </button><br/>',
                '    <textarea readonly id="url_filelist" rows="25" style="resize:none;white-space: pre; word-wrap: normal; overflow-x: scroll; width: 100%">',
                data.trim(),
                '    </textarea>',
                '</div>'
            ].join(''));
			var client = new ZeroClipboard($("#copy_urls"));
		});
	};
	
	// urls and links 
	var _retrieve_url_list = function(modal, running_wf, ufilename , process_data){
		modal.$body.html([
			  '<div>',
			  '    <div class="alert alert-info">',
			  '    <p>',
			  '        <strong>Your data</strong> are being created. Use the refresh button to get the status and retrieve them.',
			  '    </p>',
			  '    </div>',
			  '    <div id="wfstatus">',
			  '    </div>',
			  '</div>'
         ].join(''));
			
			modal.$footer.html([
				'    <div class="btn-group">',
				'        <button id="refresh_workflow" class="btn btn-default"><i class="glyphicon glyphicon-refresh"></i> Get data</button>',
				'    </div>',
				'    <div class="btn-group">',
				'        <button id="close" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Close</button>',
				'    </div>',
          ].join(''));
			
			
			$('#wfstatus').wfstatus({
			workflowID : running_wf.id,
			display    : 'list',
			serverURL: $("#server_url").val(),
		})
			
			$("#refresh_workflow").click(function(){
				modal.$body.html("<div class='tx-nG6-wait'> <strong>Please</strong> wait...</div>");
			button = this;
			button.disabled = true;
			
			_get_workflow_status(running_wf.id , 
				// successcb
				function(statusdata){
					if (statusdata.status == "completed"){
						_retrieve_data(running_wf.id, function(data){
							$(button).hide();
			            	$.each(data, function(i, component) {
	    			    		$.each(component, function(filename, file) {
	    			    			if (filename == ufilename){
	    			    				_display_file_content(file.url, modal.$body, process_data);
	    			    			}
	    			    		});
	    			    	});
						},
						//errorcb
						function(jqXHR, textStatus, errorThrown){
				        	modal.$body.html([
    	                        '<div class="alert alert-danger">',
    	                        '    <p><strong>Failed !</strong> to get outputs from ' + $("#server_url").val() + ' </p>',
    	                        '</div>',
    	                    ].join(''));
				        }
						);
					}
					else if (statusdata.status == "failed") {
						modal.$body.html([
    	                    '<div class="alert alert-danger">',
    	                    '    <p>The workflow has failed, either the login or password is incorrect. </p> <p>You must provide the credentials you use to connect to the GenoToul bioinformatics infrastructure.</p>',
    	                    '</div>',
    	                ].join(''));
					
					}
					else {
						$('#wfstatus').wfstatus('reload');
						button.disabled = false;
					}
				},
				// errorcb
				function(){
					$('#wfstatus').wfstatus('reload');
					button.disabled = false;
				}						
			);
	    });
	};
	
	// List of available downloads
    var download_options = {
    	
        
		// symlink
		"download_symlink" : {
    		label		: "symbolic link",
    		id    		: "download_symlink",
    		wfName 		: "DownloadSymlink",
    		run         : function(form_options, modal){
    			// display modal box 
    			modal.$modal.modal();
				modal.$modal.find('#wfform').wfform(form_options);
				modal.$modal.find('#wfform').on('run.wfform', function(event, running_wf) {
					_retrieve_url_list(modal, running_wf, 'output_list.txt');
				});
    		},
    	},
    	// urls 
    	"download_url" : {
    		label		: "url",
    		id    		: "download_url",
    		wfName 		: "DownloadURL",
    		run         : function(form_options, modal){
    			form_options.parameters['url_base'] = location.href.replace(/download/, "" ) + 'fileadmin';
    			// display modal box 
    			modal.$modal.modal();
				modal.$modal.find('#wfform').wfform(form_options);
				modal.$modal.find('#wfform').on('run.wfform', function(event, running_wf) {
					_retrieve_url_list(modal, running_wf, 'output_urls.txt', function(filedata){
						var link_prefix = location.href.replace(/download/, "" ) + 'fileadmin' ;
						var file_list = [];
    					$.each(filedata.trim().split('\n'), function(i, path){
    						file_list.push(link_prefix + path);
    					});
    					return file_list.join('\n');
					});
				});
    			
    		},
    	}
    };
    
    // Add download options to the dropdown list
    $.each(download_options, function(id, val){
    	$("#available-download-types").append($('<option  id="' + val.id + '" >' + val.label + '</option>'));
    });
	
    var get_selected_ids = function (){
    	var run_ids = [],
			data_ids = [],
			analysis_ids = [];
	
		$(".jstree-checked > ul > .jstree-checked, .jstree-undetermined > ul > .jstree-checked").each(function(){
			var parts =  $(this).attr("id").split('_');
			if (parts[0] == 'analyse') {
				analysis_ids.push(parts[1]);
			}
			else if (parts [0] == 'data'){
				data_ids.push(parts[1]);
			} 
			else if (parts[0] == 'run'){
				run_ids.push(parts[1]);
			} 
		});
		
		return [run_ids, data_ids, analysis_ids];
    };
    
    var check_data_size = function(){
    	var selected_ids = get_selected_ids(),
			run_ids = selected_ids[0],
			data_ids = selected_ids[1],
			analysis_ids = selected_ids[2];
    	
		$.ajax({
			url: "index.php?eID=tx_nG6&type=get_data_size&run_ids=" + run_ids.join(',') + '&data_ids=' + data_ids.join(',') +'&analysis_ids=' + analysis_ids.join(',') ,
			success: function (data) {
				var sp = data.split(':::'),
					octets = sp [0],
					string_representation = sp[1];
				
				if ( parseInt(octets) > 0 ){
					$('#total_data_size').html('<small>(' + string_representation + ')</small>');
					
					if ( $("#available-download-types option:selected" ).attr('id') == 'download_archive' ){
						if (parseInt(octets) > 104857600){
							$('#warning_message').html("The total size of selected data cannot exceed 100 Mo for archive downloading").show();
							$("#download_btn").addClass("disabled");
						}
						else{
							$('#warning_message').hide();
							$("#download_btn").removeClass("disabled");
						}
					}
					else {
						$('#warning_message').fadeOut(500);
						$("#download_btn").removeClass("disabled");
					}
				}
				else {
					$("#download_btn").addClass("disabled");
					$('#total_data_size').html('');
					$('#warning_message').fadeOut(500);
				}
			}
		});
    };
    
    
    $("#download_tree").jstree({
		"themes" : {
			"theme" : "default",
			"icons" : false
		},
        "html_data" : {
            "data" : $("#html_tree").attr("value"),
            "ajax" : {
            	"url" : "index.php?eID=tx_nG6",
            	"data" : function (n) {
					return {
						raw_data_description : "Raw data",
						run_short_description : "Run ###SPAN_NAME### (###SPAN_DATE###)",
						run_description : "Run ###SPAN_NAME### (###SPAN_SPECIES###) - (###SPAN_DATE###) produced ###SPAN_NB_SEQ### reads",
						analyse_description : "Analyse ###SPAN_NAME###",
						user_id : $("#user_id").val(),
						object_id : n.attr("id").split("_")[1],
						type : n.attr("id").split("_")[0]
					};
            	}
            }
        },
        "plugins" : [ "themes", "html_data", "ui", "checkbox" ]
    }).bind("change_state.jstree", function(event, data) {
		
    	var nb_selected = 0;
		var runs_list = new Array();
		var download_list = new Array();
		$("#download_list").html("");
			
		$(".jstree-checked > ul > .jstree-checked, .jstree-undetermined > ul > .jstree-checked").each(function(){
			nb_selected += 1;
			var tmp = $(this).attr("id").split("_");
			
			if (tmp[0] == "run") {
				runs_list.push($(this).attr("id"));
				download_list[$(this).attr("id")] = $(this).attr("name") + " : <br /> Raw data + all analyses";
			} else if (tmp[0] == "analyse") {
				var tmp2 = $(this).attr("name").split("_");
				if (tmp2.length > 1) {
					var is_in = false;
					for (var i = 0; i < runs_list.length; i++){ 
						if (tmp2[0]+"_"+tmp2[1] == runs_list[i]) {
							is_in = true;
						}
					}
					if (!is_in) {
						if (tmp2[0]+"_"+tmp2[1] in download_list) {
							download_list[tmp2[0]+"_"+tmp2[1]] += ", " + tmp2[2];
						} else {
							download_list[tmp2[0]+"_"+tmp2[1]] = $("#"+tmp2[0]+"_"+tmp2[1]).attr("name") + " : <br /> " + tmp2[2];
						}
					}
				} else {
					download_list[$(this).attr("id")] = $(this).attr("name");
				}
			} else if (tmp[0] == "data") {
				var tmp2 = $(this).attr("name").split("_");
				var is_in = false;
				for (var i = 0; i < runs_list.length; i++){ 
					if (tmp2[0]+"_"+tmp2[1] == runs_list[i]) {
						is_in = true;
					}
				}
				if (!is_in) {
					if (tmp2[0]+"_"+tmp2[1] in download_list) {
						download_list[tmp2[0]+"_"+tmp2[1]] += ", " + tmp2[2];
					} else {
						download_list[tmp2[0]+"_"+tmp2[1]] = $("#"+tmp2[0]+"_"+tmp2[1]).attr("name") + " : <br /> " + tmp2[2];
					}
				}
			}
    	});
    	for (var key in download_list) {
    		$("#download_list").append("<p class='bullet'>" + download_list[key] + "</p>");
    	}
    	
 		if (nb_selected == 0) {
			$("#download_list").html("<p>No run neither any analyze selected. <br/>Please select below the data you want to download...</p>");
			check_data_size();
		} else {
			check_data_size();
		}
    });
    
    $('#available-download-types').change(function(){
    	check_data_size();
    });
    
	$("#download_btn").click(function(){
	    var selected_download_option = download_options[$("#available-download-types option:selected" ).attr('id')];
		
		$('#setAndRunModalLabel').html("Loading");
		$('#setAndRunModalFooter').html([
		'    <div class="btn-group">',
		'        <button id="close" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Close</button>',
		'    </div>',
		'    <div class="btn-group">',
		'        <button id="reset_workflow" class="btn btn-default"><i class="glyphicon glyphicon-refresh"></i> Reset</button>',
		'        <button id="run_workflow" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-save"></i> Start</button>',
		'    </div>'
	    ].join(''));
		
		$("#reset_workflow, #run_workflow").hide();
		$("#close").show();
		
		$('#setAndRunModalBody').html([
			'<div >',
			'    <p id="wf-help"  class="text-justify"></p><br/><br/>',
			'    <div id="wfform"><div class="tx-nG6-wait">Please wait</div></div>',
			'</div>',
	    ].join(''));
		
		var modal = {
	    		'$label'    : $('#setAndRunModalLabel'),
	    		'$body'     : $('#setAndRunModalBody'),
	    		'$footer'   : $('#setAndRunModalFooter'),
	    		'$modal'    : $('#setAndRunModal')
    	};
		
		var sel = get_selected_ids(),
			run_ids = sel[0],
			data_ids = sel[1],
			analysis_ids = sel[2];
		
		var wfform_options = {
				workflowClass: selected_download_option.wfName,
				serverURL: $("#server_url").val(),
				displayRunButton: false,
				displayResetButton: false,
				parameters : {
					"admin_login"	: $("#user_login").val(),
					"run_id"		: sel[0],
					"data_id" 		:  sel[1],
					"analysis_id"	: sel[2],
				}
		};
		
		$('#wfform').on("loaded.wfform", function(event, workflow) {
			$("#reset_workflow, #run_workflow").show();
			$("#close").hide();
			if(workflow){
				modal.$label.html(workflow["name"]) ;
				$('#wf-help').html(workflow["help"]);
			}
			else{
				modal.$body.html('<div class="alert alert-warning"> <strong>Warning !</strong> The selected donwload type "' + selected_download_option.label + '" is not available</div>');
			}
		});
		
		$("#reset_workflow").click(function(){
	    	$('#wfform').wfform('reset');
	    });
	    $("#run_workflow").click(function(){
	    	$('#wfform').wfform('run');
	    });	
		// remove plugin data
	    $('#wfform').removeData();
	    selected_download_option.run(wfform_options, modal);
	});
    
});