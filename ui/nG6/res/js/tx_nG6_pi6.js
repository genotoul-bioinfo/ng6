/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */

$(function () {
	
	/*****************************************
	 * 
	 *  Global initialisations
	 *
	 *****************************************/ 
	
	// Grab relevant info from URL fragment
        var params = simpleQueryParams(location.href),
        active_tab =  params["active_tab"];

        if (active_tab ) {
                $('#myTab a[href="#' + active_tab + '"]').tab('show');
        } else {
                $('#myTab').tab();
        }
	
	
	/****************************************
	 * 
	 * Statistics configuration
	 *
	 ***************************************/
	// Init tables
	var stat_tables = initStatMenuDataTables();
	updateRefreshButton(stat_tables);
	
	// button for drawing
	$("#refresh_graph_btn").click(function(){
		$('#highcharts_graph').html('<div class="tx-nG6-wait">Chart generation, please wait ...</div>');
		
		var selected_display = $("input[type=radio][name=display_by]:checked").val() ;
		if (selected_display == "project_distribution") {
			drawProjectDistribution(stat_tables);
	    } else if (selected_display == "project_evolution") {
	    	drawProjectEvolution(stat_tables);
	    } else if (selected_display == "data_distribution") {
	    	drawDataDistribution(stat_tables);
	    } else if (selected_display == "data_evolution") {
	    	drawDataEvolution(stat_tables);
	    }
	});
	
	// refresh datatables
	$("input[type=radio][name=group_by]").change(function(){
		var group_by = $(this).attr("id").split("_")[1];
		$("#by_span").html(group_by);
		refreshDataTable(stat_tables);
		updateRefreshButton(stat_tables);
	});
	$("input[type=radio][name=users_to_consider]").change(function(){
		refreshDataTable(stat_tables);
		updateRefreshButton(stat_tables);
	});
	
	$("[id^=chk_all_]").change(function(){
		var type = $(this).attr("id").split("_")[2];
		var chk_status = $(this)[0].checked ;
		var $table = $('div[id=wrapper_datatable_' + type + ']:visible'),
			oDatatable = stat_tables[ $table.attr('id').split('_')[2] ];
		
		$.each(oDatatable.rows({search:'applied'}).nodes(), function(idx, node){
			$(node)
				.find('input[type=checkbox][class=chk_stat_element]')
				.each(function(idx, element){
					element.checked = chk_status;
				});
		});
		updateRefreshButton(stat_tables);
	});
	
	$("input[type=checkbox][class=chk_stat_element]").change(function(){
		updateRefreshButton(stat_tables);
	});
	
	$("input[type=radio][name=display_by]").change(function(){
		$('#chk_includes_analyses, #chk_display_bases, #chk_cumulate').each(function(idx, element){
			element.disabled = false;
			element.checked = false;
		});
		var selected = $(this).val();
		if (selected == "project_distribution") {
			$('#chk_includes_analyses, #chk_display_bases, #chk_cumulate').each(function(idx, element){
				element.disabled = true;
			});
		} else if (selected == "project_evolution") {
			$('#chk_includes_analyses, #chk_display_bases').each(function(idx, element){
				element.disabled = true;
			});
		} else if (selected == "data_distribution") {
			$('#chk_cumulate')[0].disabled = true;
		}
	});
	
	Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function(color) {
	    return {
	        radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
	        stops: [
	            [0, color],
	            [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
	        ]
	    };
	});
	
	/****************************************
	 * 
	 * WORKFLOW MONITORING
	 *
	 ***************************************/
	
	$("#wf_monitoring").activewf({
		serverURL: $("#server_url").val()
	});
	
	$('#setAndRunModalFooter').html([
         '<button id="refresh_workflow" class="btn btn-default"><i class="glyphicon glyphicon-refresh"></i> Refresh</button>',
         '<button id="rerun_workflow" class="btn btn-primary"><i class="glyphicon glyphicon-cog"></i> ReRun</button>'
        ].join(''));
	
	$("#refresh_workflow").click(function(){ $('#setAndRunModalBody').wfstatus('reload'); });
	$("#wf_monitoring").on("select.activewf", function(event, workflow) {
                $('#setAndRunModalLabel').html(workflow["name"] + " <small>" + workflow["id"] + "</small>");
                $('#setAndRunModalBody').wfstatus({
                        serverURL: $("#server_url").val(),
                        workflowID: workflow["id"]
                });
		$('#setAndRunModal').modal();
	});
	
	
	/****************************************
	 * 
	 * ADMINISTRATION MANAGEMENT
	 *
	 ***************************************/
	
	updateUsersButtonStatus();
	$("[id^=chk_user_]").change(function() {
		updateUsersButtonStatus();
	});
	
	$('#add_admin').click(function(){
		$("#modal-label-tmpl").html("Add a new ng6 admin user");
		var add_ng6_admin = [ 
                    '<div id="error_message" class="alert alert-danger"><button class="close" data-dismiss="alert" type="button">x</button></div>',
                    '<div class="tx-nG6-pi1-add-new">Seek the user using the autocompletion fields<br /> <br /> ',			
                    '    <form class="form-horizontal" id="new_ng6_admin_user">',
                    '        <input type="hidden" id="userid_val" name="userid_val" />',
                    '        <input type="hidden" id="added_userid_val" name="added_userid_val" />',
                    '        <div class="form-group">',
                    '            <label class="col-sm-offset-3 col-sm-2 control-label">Login</label>',
                    '            <div class="col-sm-7">',
                    '                <input type="text" id="username_val" class="form-control typeahead" name="username_val" placeholder="Login"/>',
                    '            </div>',
                    '        </div>',
                    '        <div class="form-group">',
                    '            <label class="col-sm-offset-3 col-sm-2 control-label">Last name</label>',
                    '            <div class="col-sm-7">',
                    '                <input type="text" id="last_name_val" class="form-control typeahead" name="last_name_val" placeholder="Last name"/> ', 
                    '            </div>',
                    '        </div>',
                    '        <div class="form-group">',
                    '            <label class="col-sm-offset-3 col-sm-2 control-label">First name</label>',
                    '            <div class="col-sm-7">',
                    '                <input type="text" id="first_name_val" class="form-control typeahead" name="first_name_val" placeholder="First name"/>',
                    '            </div>',
                    '        </div>',
                    '    </form>',
                    '</div>'].join('');

		$("#modal-body-tmpl").html(add_ng6_admin);
		$("#modal-foot-tmpl").html('<button class="btn btn-default" id="raz_form"><i class="glyphicon glyphicon-repeat"></i> Clear form</a>' +
								   '<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>' +
								   '<button id="modal-btn-addng6admin" class="btn btn-primary"><i class="glyphicon glyphicon-user"></i> Add</button>');
			
		$("#ng6modal").modal().ready(function() {
			$("#error_message").hide();
			
			$('.typeahead').each(function(){
				var $this = $(this);
				$this.on('typeahead:selected', function(typeahead, item){
					$("#new_member div.control-group").removeClass("error");
					$(".btn-danger").removeClass("btn-danger");
					$(".glyphicon-wrench").removeClass("icon-white");
					$("#last_name_val").val(item.last_name);
					$("#first_name_val").val(item.first_name);
					$("#username_val").val(item.username);
					$("#userid_val").val(item.uid);	
					lock_fields(1);
				});
				$this.typeahead({
					minLength: 2,
					autoselect: true
				},{
					displayKey : 'value',
					source : function(query, process){
						var gender = $this.attr("id").split("_val")[0];
						$.ajax({
							url: "index.php?eID=tx_nG6&type=autocomplete&gender="+gender+ "&name_start=" + query,
							dataType: 'json',
							success: function (users) {
								var data = new Array();
								for (index in users) {
									users[index]['value'] = users[index][gender];
									data.push(users[index]);
								}
								process(data);
							}
						});
					}
				});
			});
			
			// empty the form
			$("#raz_form").click(function(){
				$("#last_name_val").val("");
				$("#first_name_val").val("");
				$("#username_val").val("");
				$("#userid_val").val("");	
				lock_fields(0);		// unlock fields
				$(".btn-danger").removeClass("btn-danger");
				$(".glyphicon-wrench").removeClass("icon-white");
				$("#error_message").hide();
			});
			
			// lock/unlock fields when autocomplete
			function lock_fields(order){
				// lock fields
				if(order == 1){
					$("#username_val").attr('disabled', 'disabled');
					$("#last_name_val").attr('disabled', 'disabled');
					$("#first_name_val").attr('disabled', 'disabled');
					$('.group_typeahead').each(function(){
						$(this).css('background-color','');
					});
				} else if(order == 0){
					$("#username_val").removeAttr('disabled');
					$("#last_name_val").removeAttr('disabled');
					$("#first_name_val").removeAttr('disabled');
				}
			}

			if($("#username_val").attr('disabled') != 'disabled'){
				$("#new_ng6_admin_user").validate({ 
					rules: {
						first_name_val: { required: true },
						last_name_val: { required: true },
						username_val: { required: true },
					},
					messages: {
						first_name_val: null,
						last_name_val: null,
						username_val: null,
                                        },
                                        submitHandler: function(form) {
					    var val_url = "index.php?eID=tx_nG6&type=add_to_ng6_admin";
					    val_url += "&userid=" + $("#userid_val").val();
						$.ajax({
							url: val_url,
							success: function(val) {
						        $("#ng6modal").modal('hide');
						        set_active_tab('admin_management');
							}
						});
                                        }
				});
			}
		});
		
		$("#modal-btn-addng6admin").click( function() {
			$("#new_ng6_admin_user").submit();	
		});
	});
	
	
	$('#del_admin').click(function(){
		$("#modal-label-tmpl").html("Delete");
		$("#modal-body-tmpl").html("Are you sure you want to delete this user from admin group?");
		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">No</button>' +
			  					   '<button id="modal-btn-yes" class="btn btn-primary">Yes</button>');
		$("#ng6modal").modal();
		
		$("#modal-btn-yes").click( function() {
			var val_url = "index.php?eID=tx_nG6&type=delete_from_ng6_admin";
                        val_url += "&userids=" ;
                        var arr = [];
                        $(':checked[id^=chk_user]').each(function(i){
                                arr.push( $(this).val().split("_" , 2)[1] );
                        });
                        val_url += arr.join(',');
			$.ajax({
				url: val_url,
				success: function(val) {
			        $("#ng6modal").modal('hide');
			        set_active_tab('admin_management');
				}
			});
		});
	});
	
	/****************************************
	 * 
	 * SUPERADMINS MANAGEMENT
	 *
	 ***************************************/
	
	$('#add_superadmin').click(function(){
		$("#modal-label-tmpl").html("Add a new ng6 superadmin user");
		var add_ng6_superadmin = [ 
                    '<div id="error_message" class="alert alert-danger"><button class="close" data-dismiss="alert" type="button">x</button></div>',
                    '<div class="tx-nG6-pi1-add-new">Seek the user using the autocompletion fields<br /> <br /> ',			
                    '    <form class="form-horizontal" id="new_ng6_superadmin_user">',
                    '        <input type="hidden" id="userid_val" name="userid_val" />',
                    '        <input type="hidden" id="added_userid_val" name="added_userid_val" />',
                    '        <div class="form-group">',
                    '            <label class="col-sm-offset-3 col-sm-2 control-label">Login</label>',
                    '            <div class="col-sm-7">',
                    '                <input type="text" id="username_val" class="form-control typeahead" name="username_val" placeholder="Login"/>',
                    '            </div>',
                    '        </div>',
                    '        <div class="form-group">',
                    '            <label class="col-sm-offset-3 col-sm-2 control-label">Last name</label>',
                    '            <div class="col-sm-7">',
                    '                <input type="text" id="last_name_val" class="form-control typeahead" name="last_name_val" placeholder="Last name"/> ', 
                    '            </div>',
                    '        </div>',
                    '        <div class="form-group">',
                    '            <label class="col-sm-offset-3 col-sm-2 control-label">First name</label>',
                    '            <div class="col-sm-7">',
                    '                <input type="text" id="first_name_val" class="form-control typeahead" name="first_name_val" placeholder="First name"/>',
                    '            </div>',
                    '        </div>',
                    '    </form>',
                    '</div>'].join('');

		$("#modal-body-tmpl").html(add_ng6_superadmin);
		$("#modal-foot-tmpl").html('<button class="btn btn-default" id="raz_form"><i class="glyphicon glyphicon-repeat"></i> Clear form</a>' +
								   '<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>' +
								   '<button id="modal-btn-addng6superadmin" class="btn btn-primary"><i class="glyphicon glyphicon-user"></i> Add</button>');
			
		$("#ng6modal").modal().ready(function() {
			$("#error_message").hide();
			
			$('.typeahead').each(function(){
				var $this = $(this);
				$this.on('typeahead:selected', function(typeahead, item){
					$("#new_member div.control-group").removeClass("error");
					$(".btn-danger").removeClass("btn-danger");
					$(".glyphicon-wrench").removeClass("icon-white");
					$("#last_name_val").val(item.last_name);
					$("#first_name_val").val(item.first_name);
					$("#username_val").val(item.username);
					$("#userid_val").val(item.uid);	
					lock_fields(1);
				});
				$this.typeahead({
					minLength: 2,
					autoselect: true
				},{
					displayKey : 'value',
					source : function(query, process){
						var gender = $this.attr("id").split("_val")[0];
						$.ajax({
							url: "index.php?eID=tx_nG6&type=autocomplete&gender="+gender+ "&name_start=" + query,
							dataType: 'json',
							success: function (users) {
								var data = new Array();
								for (index in users) {
									users[index]['value'] = users[index][gender];
									data.push(users[index]);
								}
								process(data);
							}
						});
					}
				});
			});
			
			// empty the form
			$("#raz_form").click(function(){
				$("#last_name_val").val("");
				$("#first_name_val").val("");
				$("#username_val").val("");
				$("#userid_val").val("");	
				lock_fields(0);		// unlock fields
				$(".btn-danger").removeClass("btn-danger");
				$(".glyphicon-wrench").removeClass("icon-white");
				$("#error_message").hide();
			});
			
			// lock/unlock fields when autocomplete
			function lock_fields(order){
				// lock fields
				if(order == 1){
					$("#username_val").attr('disabled', 'disabled');
					$("#last_name_val").attr('disabled', 'disabled');
					$("#first_name_val").attr('disabled', 'disabled');
					$('.group_typeahead').each(function(){
						$(this).css('background-color','');
					});
				} else if(order == 0){
					$("#username_val").removeAttr('disabled');
					$("#last_name_val").removeAttr('disabled');
					$("#first_name_val").removeAttr('disabled');
				}
			}

			if($("#username_val").attr('disabled') != 'disabled'){
				$("#new_ng6_superadmin_user").validate({ 
					rules: {
						first_name_val: { required: true },
						last_name_val: { required: true },
						username_val: { required: true },
					},
					messages: {
						first_name_val: null,
						last_name_val: null,
						username_val: null,
                                        },
                                        submitHandler: function(form) {
					    var val_url = "index.php?eID=tx_nG6&type=add_to_ng6_superadmin";
					    val_url += "&userid=" + $("#userid_val").val();
						$.ajax({
							url: val_url,
							success: function(val) {
						        $("#ng6modal").modal('hide');
						        set_active_tab('admin_management');
							}
						});
                     }
				});
			}
		});
		
		$("#modal-btn-addng6superadmin").click( function() {
			$("#new_ng6_superadmin_user").submit();	
		});
	});
	
	
	$('#del_superadmin').click(function(){
		$("#modal-label-tmpl").html("Delete");
		$("#modal-body-tmpl").html("Are you sure you want to delete this user from superadmin group?");
		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">No</button>' +
			  					   '<button id="modal-btn-yes" class="btn btn-primary">Yes</button>');
		$("#ng6modal").modal();
		
		$("#modal-btn-yes").click( function() {
			var val_url = "index.php?eID=tx_nG6&type=delete_from_ng6_superadmin";
                        val_url += "&userids=" ;
                        var arr = [];
                        $(':checked[id^=chk_user]').each(function(i){
                                arr.push( $(this).val().split("_" , 2)[1] );
                        });
                        val_url += arr.join(',');
			$.ajax({
				url: val_url,
				success: function(val) {
			        $("#ng6modal").modal('hide');
			        set_active_tab('admin_management');
				}
			});
		});
	});
	
	
	/****************************************
	 * 
	 * OBSOLETE PROJECT MANAGEMENT /  PURGE DEMAND MANAGEMENT
	 *
	 ***************************************/
        /* init datatable */        
        var purge_table = {};
        purge_table["obsolete"] = $("#data_table_obsolete").DataTable({
		"language": {
			"info": 'Showing _START_ to _END_ of _MAX_ entries <strong><small id="nb_selected_span"></small></strong>'
		 },
  		"order": [[ 1, "asc" ]],
  		"columns": [
  		         { "orderable": false  },
  		         null,null,null,null,null,null,null,null,null,null,null]
  	});
        purge_table["demand"] = $("#data_table_demand").DataTable({
                "language": {
                        "info": 'Showing _START_ to _END_ of _MAX_ entries <strong><small id="nb_selected_span"></small></strong>'
                 },
                "order": [[ 1, "asc" ]],
                "columns": [
                         { "orderable": false },
                         null,null,null,null,null,null,null]});

        purge_table["allproject"] = $("#data_table_allproject").DataTable({});

        
        //Load data
        refresh_project_obsolete(purge_table["obsolete"]);
        refresh_purge_demand(purge_table["demand"]);

	/****************************************
	 * tab mail obsolete project
	 ***************************************/

        //filtre table obsolete
        $("#refresh_obsolete_list_btn").click(function(){
                refresh_project_obsolete(purge_table["obsolete"]);
	});
      
        //Obsolete project / purge demand 
        //mark mail sent
	$("#btn_obsolete_mail").click(function(){

		$('#data_table_obsolete_wait').show();
                $('#obsolete_list').hide();
		var all_project=Array();
                $("#ng6modal").modal('hide');
                all_project = getPurgeDatatableCheckedValue(purge_table["obsolete"], "obsolete")
                
                $.ajax({
                        url: "index.php?eID=tx_nG6&type=purge_managment_mail&project="+all_project.join(',')+"&user_id="+$('input[id=user_id]').val(),
                        dataType: 'json',
                        success: function(val, status, xhr) {
                        	refresh_project_obsolete(purge_table["obsolete"]);
                                refresh_purge_demand(purge_table["demand"]);
                                $("#purge_demand_error").hide()
                        },
                        error: function(returnval){
                        	alert(returnval.status)
                        	//TODO Afficher alerte en cas d'erreur de mail
                            var message = 'nG6 was unable to perform your action.';
                        	if (returnval.status ==  401){
                        		message = 'it seems your session expired. Please re-authenticate!';
                        	}
                        	$('#data_table_obsolete_wait').hide();
                        	$("#data_table_obsolete_error").html("<div class='tx-nG6-pi6-error'>Error : "+message+"</div>");
                            $("#data_table_obsolete_error").show();
                            $('#obsolete_list').show();
                                
                        }
                });
	});
	
	$("#btn_obsolete_mail_without_extension").click(function(){

		$('#data_table_obsolete_wait').show();
                $('#obsolete_list').hide();
		var all_project=Array();
                $("#ng6modal").modal('hide');
                all_project = getPurgeDatatableCheckedValue(purge_table["obsolete"], "obsolete")
                
                $.ajax({
                        url: "index.php?eID=tx_nG6&type=purge_managment_mail_without_extension&project="+all_project.join(',')+"&user_id="+$('input[id=user_id]').val(),
                        dataType: 'json',
                        success: function(val, status, xhr) {
                        	refresh_project_obsolete(purge_table["obsolete"]);
                                refresh_purge_demand(purge_table["demand"]);
                                $("#purge_demand_error").hide()
                        },
                        error: function(returnval){
                        	alert(returnval.status)
                        	//TODO Afficher alerte en cas d'erreur de mail
                            var message = 'nG6 was unable to perform your action.';
                        	if (returnval.status ==  401){
                        		message = 'it seems your session expired. Please re-authenticate!';
                        	}
                        	$('#data_table_obsolete_wait').hide();
                        	$("#data_table_obsolete_error").html("<div class='tx-nG6-pi6-error'>Error : "+message+"</div>");
                            $("#data_table_obsolete_error").show();
                            $('#obsolete_list').show();
                                
                        }
                });
	});
        
        /****************************************
	 * tab process mail demand
	 ***************************************/
		//refresh purge demands list button
    	$("#refresh_purge_demands_list_btn").click(function(){
            refresh_purge_demand(purge_table["demand"]);
    	});
	
	
        //resend email
        $("#btn_demand_resend_mail").click(function(){
                var demands = getPurgeDatatableCheckedValue(purge_table["demand"],"demand");
                 $.ajax({
                        url: "index.php?eID=tx_nG6&type=purge_managment_resend_mail&purge_demand="+demands.join(',')+"&user_id="+$('input[id=user_id]').val(),
                        dataType: 'json',
                        success: function(val, status, xhr) {
                                refresh_project_obsolete(purge_table["obsolete"]);
                                refresh_purge_demand(purge_table["demand"]);
                        },
                        error: function(returnval){
                                //TODO Afficher alerte en cas d'erreur de mail
                                $("#modal-body-tmpl").html("<div class='tx-nG6-pi6-error'>Error mail sending.</div>");
                        }
                });
        })
        
        // Delete data after selecting list of ids demand
        $("#btn_demand_delete_data").click(function(){
                var del_auth_html = [
                '<div class="tx-nG6-pi1-login">',
                '	This functionality is only available for ng6 unix user on the server. <br /> <br /> ',
                '	<form class="form-horizontal" id="tx-nG6-pi1-login-form" >',
                '		<div class="input-group" style="margin-left:50px">',
    		 	'			<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>',
    		 	'			<input class="form-control" style="width:200px" type="text" id="user_login_val" name="user_login_val" placeholder="Username"/>',
    		 	'		</div><br/>',
		 		'		<div class="input-group" style="margin-left:50px">',
			 	'			<span class="input-group-addon"><i class="glyphicon glyphicon-wrench"></i></span>',
			 	'			<input class="form-control" style="width:200px" type="password" id="user_pwd_val" name="user_pwd_val" placeholder="Password"/>',
			 	'		</div>',
			 	'	</form>',
			 	'</div>'
			].join('');
                $("#modal-label-tmpl").html("Purge");
    		$("#modal-body-tmpl").html(del_auth_html);
    		$("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>' +
    								   '<button id="modal-btn-purge" class="btn btn-primary"><i class="glyphicon glyphicon-trash"></i> Delete</button>');
    		$("#ng6modal").modal();
                var demands = [];
		$("#modal-btn-purge").click( function() {
                        demands = getPurgeDatatableCheckedValue(purge_table["demand"], "demand");
                        var val_url = "index.php?eID=tx_nG6&type=delete_purge_data&purge_demand=" + demands.join(',');
	    		    val_url += "&user_login=" + $("#user_login_val").val() + "&user_pwd=" + encodeURIComponent($("#user_pwd_val").val());
	    		    val_url += "&user_id="+$("#user_id").val() +"&data_folder=" + $("#data_folder").val() ;
                        $("#modal-label-tmpl").html("Purge");
                        $("#modal-body-tmpl").html('<div class="tx-nG6-wait">Purging, please wait...</div>');
                        $("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>' +
                                                                           '<button id="modal-btn-delete" class="btn btn-primary disabled"><i class="glyphicon glyphicon-trash"></i> Delete</button>');
                        $("#ng6modal").modal();
                        $.ajax({
                                url: val_url,
                                success: function(val, status, xhr) {
                                        // return code : 0 = ok, 1 = right error, 2 = authentication error, 3 = connexion error, 4 = error delete file , 5 error sql update 
                                		val = parseInt(val);
                                        if (val == 0) {
                                                // Uncheck all checkboxes
                                                $(".tx-nG6-wait").hide();
                                                $("#modal-body-tmpl").html('Done !');
                                                $("#ng6modal").modal();
                                                refresh_purge_demand(purge_table["demand"]);
                                        } else {
                                                $("#modal-label-tmpl").html("Error");
                                                if (val == 1) {
                                                        $("#modal-body-tmpl").html("<div class='tx-nG6-pi6-error'>You don't have write permission on the specified directory.</div>");
                                                } else if (val == 2) {
                                                        $("#modal-body-tmpl").html("<div class='tx-nG6-pi6-error'>An error occurred during login. Most likely you didn't enter the username or password correctly. Be certain that you enter them precisely as they are, including upper/lower case.</div>");
                                                } else if (val == 3) {
                                                        $("#modal-body-tmpl").html("<div class='tx-nG6-pi6-error'>An error occurred during server connection. Please contact the site administrator.</div>");
                                                } else if (val == 4) {
                                                        $("#modal-body-tmpl").html("<div class='tx-nG6-pi6-error'>An error occurred during file deletion check if user have unix rigth to remove files. Please contact the site administrator.</div>");
                                                } else if (val == 5) {
                                                        $("#modal-body-tmpl").html("<div class='tx-nG6-pi6-error'>An error occurred while updating in db. Please contact the site administrator.</div>");
                                                }
                                                $("#modal-foot-tmpl").html('<button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i> Close</button>');
                                                $("#ng6modal").modal();
                                        }
                                   },
                                   error: function(val, status, xhr)  { 
                                           $("#modal-body-tmpl").html("<div class='tx-nG6-pi6-error'>An error occurred during file deletion. Please contact the site administrator.</div>");
                                   }
                           });
                        
		});
 
        });
        
        // Extend retention date after selecting list of ids demand
        $("#btn_demand_retention").click(function(){
                var demands=Array();
                demands = getPurgeDatatableCheckedValue(purge_table["demand"],"demand");
                extend_date_from_demand(demands.join(','),purge_table["demand"],purge_table["obsolete"]);
        });
         $("#input_extend_all_project").change(function(){
                 $("#btn_extend_all_project").prop('disabled', false);
        });
        
        // Extend retention date with list of project_id
        $("#btn_extend_all_project").click(function(){
                var projects = getPurgeDatatableCheckedValue(purge_table["allproject"],"allproject"),
                date = $("#input_extend_all_project").val()
                extend_date_from_project(projects.join(','),purge_table);
                
        });
         /****************************************
	 * tab all project extention
	 ***************************************/
         $("#btn_extend_all_project").prop('disabled', true);
         refreshAllProjectDatatable(purge_table["allproject"]);
         
        /***** DataTable Obsolete & Demand chek all ****/
        $("[id^=all_chk_]").change(function(){
		var type = $(this).attr("id").split("_")[2];
		var chk_status = $(this)[0].checked ;
		$('input[id^=chk_'+type+']').prop('checked', chk_status);
                //updateDatatableButton(purge_table[type],type);
	});
    });
        /****************************************
	 * END init 3 tabs of purge
	 ***************************************/
   function refreshAllProjectDatatable(allproject_datatble){
          $("#all_project_error").hide();
          $("#all_project_wait").show();
          tx_nG6_pi6_redirection_page = $("#tx_nG6_pi6_redirection_page").val();
           $.ajax({
                url: "index.php?eID=tx_nG6&type=get_all_project",
                dataType: 'json',
                success: function(val, status, xhr) {
                        oTable = allproject_datatble;
                        oTable.clear();
                        $.each(val, function(key,values){
                                    var checkbox = '<center><input id="chk_allproject_"'+values["pid"]+'" type="checkbox" value="' + values["pid"] + '" class="chk_allproject"></center>' ; 
                                    var row=[checkbox, "<a href='index.php?id="+tx_nG6_pi6_redirection_page+"&tx_nG6_pi1[project_id]="+values["pid"]+"'>"+values["project_name"]+"</a>"];
                                    oTable.row.add(row);
                            });
                            oTable.draw();
                            $('#purge_demand_list').show();
                            $('#all_project_error').hide();
                            $("#all_project_wait").hide();
                            $('#input_extend_all_project').val("");
                        }
                });
        }
           
           
    function extend_date_from_demand(list_ids, purge_demand_datatable, obsolete_project_datatable){
        if(list_ids == ""){
                window.alert("You must select or enter purge demand. ");
                return 0;
        }
        $("#modal-label-tmpl").html("Extend retention date");
        $("#modal-body-tmpl").html('<div class="form-group param-field">'
                +'<fieldset>'
                +'<label id="label_default" class="col-sm-4 control-label" for="change_retention_date">Select extention date: </label>'
                +'<div class="col-sm-8">'
                        +'<input id="change_retention_date" class="form-control" type="text" value="" data-date-format="dd/mm/yyyy" data-provide="datepicker" name="change_retention_date"/>'
                +'</fieldset>'
	             +'<fieldset>'
                	+'<legend>Extend for</legend>'
                	+'<div class="col-sm-8">'
                	+'<input type="radio" id="extend_purgeable_element" name="is_all_project" checked />'
        				+'<label for="extend_purgeable_element">Only obsolete run and analyse</label><br/>'
                	+'<input type="radio" id="extend_all_project" name="is_all_project" />'
        				+'<label for="extend_all_project">The projects entirelly</label>'
                +'</fieldset>'
                +'</div>');
        
        $("#modal-foot-tmpl").html('<button id="close" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Close</button>    </div> '
                +'<button id="modal-btn-send-extend" class="btn btn-primary"  disabled="true">Change</button>');
                $("#ng6modal").modal();
        $("#change_retention_date").change(function(){
                 $("#modal-btn-send-extend").prop('disabled', false);
                });
        $("#modal-btn-send-extend").click(function(){
                date_send = $("#change_retention_date").val();
                type_extend = $('input[type=radio][name=is_all_project]:checked').attr('id');
                
                $('#data_table_purge_demand_wait').show();
                $('#purge_demand_list').hide();
                $('#purge_demand_error').hide();
                $.ajax({
                        url: "index.php?eID=tx_nG6&type=extend_retention_date_from_demand&purge_demand="+list_ids+"&date_value="+date_send+"&user_id="+$('input[id=user_id]').val()+'&type_extend='+type_extend,
                        dataType: 'json',
                        success: function(val, status, xhr) {
                                $("#ng6modal").modal('hide');
                                refresh_purge_demand(purge_demand_datatable);
                                refresh_project_obsolete(obsolete_project_datatable);
                        },
                        error: function(val) {
                                //TO PRINT ERROR
                                
                                $('#purge_demand_error').html ('Error while extending date ' + val );
                                $('#purge_demand_error').show();
                                $("#ng6modal").modal('hide');
                                
                        }
                        
                });
        });
    }
 
    function extend_date_from_project(list_ids, purge_tables){
        if(list_ids == ""){
                window.alert("You must select a project. ");
                return 0;
        }
        $('#data_table_all_project_wait').show();
        $('#all_project_wait').show();
        $('#all_project_error').hide();
         date_send = $("#input_extend_all_project").val();
         $.ajax({
                        url: "index.php?eID=tx_nG6&type=extend_retention_date_from_project&projects_id="+list_ids+"&date_value="+date_send+"&user_id="+$('input[id=user_id]').val(),
                        dataType: 'json',
                        success: function(val, status, xhr) {
                                $('#data_table_all_project_wait').hide();
                                refresh_purge_demand(purge_tables["demand"]);
                                refresh_project_obsolete(purge_tables["obsolete"]);
                                
                                        $('#all_project_wait').hide();
                                        $("#modal-label-tmpl").html("Extention");
                                        $("#modal-body-tmpl").html("Project(s) extended");
                                        $("#modal-foot-tmpl").html('<button id="modal-btn-ok" class="btn btn-primary">OK</button>');
                                        $("#ng6modal").modal();
                                        
                                        $("#modal-btn-ok").click( function() {
                                                $(':checked[id^=chk_allproject_]').each(function(i){
                                                        $(this).prop('checked', false);
                                                });
                                                $('#input_extend_all_project').val("");
                                                $("#ng6modal").modal('hide');
                                        });
                                
                        },
                        error: function(val) {
                                $('#data_table_all_project_wait').hide();
                                $('#all_project_error').html ('Error while extend date ' + val );
                                $('#all_project_error').show();
                        }
                });
    }
    function refresh_purge_demand(purge_demand_datatable){
        var purge_delay=$("#purge_delay").val();
        var purge_demand_filter_delay_exceeded = $("#purge_demand_filter_delay_exceeded").val();
        var purge_demand_filter_space = $("#purge_demand_filter_space").val();
        
        $("#purge_demand_error").hide();
        $('#data_table_purge_demand_wait').show();
        $('#purge_demand_list').hide();
        
        $.ajax({
                url: "index.php?eID=tx_nG6&type=refresh_purge_demand&purge_delay="+purge_delay+"&purge_demand_filter_space="+purge_demand_filter_space+"&purge_demand_filter_delay_exceeded="+purge_demand_filter_delay_exceeded,
                dataType: 'json',
                success: function(val, status, xhr) {
                oTable = purge_demand_datatable;
				oTable.clear();
                    $.each(val, function(key,values){
        
                            var checkbox = '<center><input id="chk_demand_"'+key+'" type="checkbox" value="' + key + '" class="chk_demand"></center>' ; 
                            tx_nG6_pi6_redirection_page = $("#tx_nG6_pi6_redirection_page").val();
                            var row=[checkbox, key, "<a href='index.php?id="+tx_nG6_pi6_redirection_page+"&tx_nG6_pi1[project_id]="+values["project_id"]+"'>"+values["project_name"]+"</a>", 
                            		values["space_id"],
                                    get_octet_string_representation(values["purge_size"],2),
                                    values["mail_sent_date"]];
                                    
                            var string_user= "";
                            $.each(values["users"], function(key_user,values_user){
                                    string_user+=values_user["username"]+" ["+values_user["user_group_title"]+"], ";
                            });
                            string_user = string_user.slice(0, -2);
                            row.push(string_user);
                            
                            if (values["delay_excedeed"]==1){
                                    row.push("yes");
                            } else {
                                    row.push("no");
                            }
                            oTable.row.add(row);
                    });
                    
                    oTable.draw();
                    $('#purge_demand_list').show();
                    $('#data_table_purge_demand_wait').hide();
                    /*
                    updateDatatableButton(purge_demand_datatable, "demand");
                    //BUG doesn't work after datatable filtering
                    $("input[type=checkbox][class=chk_demand]").change(function(){
                                updateDatatableButton(purge_demand_datatable, "demand");
                    });
                    * */
                }
                        
            });
        }

    function refresh_project_obsolete(obsolete_project_datatable){
	
		$('#data_table_obsolete_wait').show();
                $('#obsolete_list').hide();
        filter_space = $("#filter_space").val(),
		max_retention_date = $("#max_retention_date").val(),
		without_lab=[],
                with_lab=[],
		create_users=[];
		$(':checked[class^=chk_create_user]').each(function(){
			create_users.push($(this).val());
                });
		$(':checked[class^=chk_without_lab_]').each(function(){
			without_lab.push($(this).val());
                });
                $(':checked[class^=chk_with_lab_]').each(function(){
                    with_lab.push($(this).val());
                });
		without_laboratories = without_lab.join(',');
                with_laboratories = with_lab.join(',');
		create_users_string = create_users.join(',');
		$.ajax({
                        url: "index.php?eID=tx_nG6&type=refresh_purge_list&max_retention_date="+max_retention_date+"&filter_space="+filter_space+"&create_users="+create_users_string+"&without_lab="+without_laboratories+"&with_lab="+with_laboratories,
						dataType: 'json',
                        success: function(val, status, xhr) {
                                var nb_purgeable_project=0,
                                global_purgeable_size=0;
				oTable = obsolete_project_datatable;
				oTable.clear();
				$.each(val, function(key,values){
					
                                        global_purgeable_size+=values["total_purgeable_size"];
                                        nb_purgeable_project+=1;
                                        var checkbox = '<center><input id="chk_obsolete_'+key+'" class="chk_obsolete" type="checkbox" value="' + key + '"></center>' ; 
                                        tx_nG6_pi6_redirection_page = $("#tx_nG6_pi6_redirection_page").val();
                                        var row=[checkbox, "<a href='index.php?id="+tx_nG6_pi6_redirection_page+"&tx_nG6_pi1[project_id]="+key+"'>"+values["project_name"]+"</a>", values["nb_runs"]];
                                        var nb_purgeable_element=0,
                                        nb_all_element=parseInt(values["nb_runs"], 10);
                                        if (values["state"].hasOwnProperty('stored') && values["state"]["stored"] != null  && values["state"]["stored"]['nb_run'] != null) {
                                                row.push(values["state"]["stored"]['nb_run']+ " (" + get_octet_string_representation(values["state"]["stored"]['size_run'],2) + ")")
                                                nb_purgeable_element+=parseInt(values["state"]["stored"]['nb_run']);
                                        }else{
                                                row.push("-")
                                        }
                                        if (values["state"].hasOwnProperty('extended') && values["state"]["extended"] != null && values["state"]["extended"]['nb_run'] != null) {
                                                row.push(values["state"]["extended"]['nb_run']+ " (" + get_octet_string_representation(values["state"]["extended"]['size_run'],2) + ")")
                                                nb_purgeable_element+=parseInt(values["state"]["extended"]['nb_run']);
                                        }else{
                                                row.push("-")
                                        }
                                        row.push(values["nb_analyses"])
                                        nb_all_element+=parseInt(values["nb_analyses"]);
                                        if (values["state"].hasOwnProperty('stored') && values["state"]["stored"] != null && values["state"]["stored"]['nb_analyze'] != null) {
                                                row.push(values["state"]["stored"]['nb_analyze']+ " (" + get_octet_string_representation(values["state"]["stored"]['size_analyze'],2) + ")")
                                                nb_purgeable_element+=parseInt(values["state"]["stored"]['nb_analyze']);
                                        }else{
                                                row.push("-")
                                        }
                                        if (values["state"].hasOwnProperty('extended') && values["state"]["extended"] != null && values["state"]["extended"]['nb_analyze'] != null) {
                                                row.push(values["state"]["extended"]['nb_analyze']+ " (" + get_octet_string_representation(values["state"]["extended"]['size_analyze'],2) + ")")
                                                nb_purgeable_element+=parseInt(values["state"]["extended"]['nb_analyze']);
                                        }else{
                                                row.push("-")
                                        }
                                        row.push(get_octet_string_representation(values["total_purgeable_size"],2))
                                         if (nb_purgeable_element != nb_all_element) {
                                                row.push("open")
                                        }else{
                                                row.push("finished")
                                        }
                                        row.push(values["space_id"])
                                        
                                        var string_user= "";
                                        $.each(values["users"], function(key_user,values_user){
                                                if (values_user["right_level_label"]=="manager") {
                                                    string_user+=values_user["user_name"]+" [" + values_user["email"] +", "+ values_user["user_group_title"]+"], ";
                                                }else{
                                                    string_user+=values_user["user_name"]+", ";
                                                }
                                        });
                                        string_user = string_user.slice(0, -2);
                                        row.push(string_user);
                                        oTable.row.add(row );
                                });
                                oTable.draw();
                                $('#nb_purgeable_project').text(nb_purgeable_project);
                                $('#global_purgeable_size').text(get_octet_string_representation(global_purgeable_size,2));
                                
                                $('#obsolete_list').show();
                                $('#data_table_obsolete_wait').hide();
                                /*
                                updateDatatableButton(obsolete_project_datatable, "obsolete");
                                //BUG doesn't work after datatable filtering
                                $("input[type=checkbox][class=chk_obsolete]").change(function(){
                                        updateDatatableButton(obsolete_project_datatable, "obsolete");
                                });
                                * */
                        }
                        
                });
        }
        /***
         * Retrieve checked values for data managment
         ***/
        function getPurgeDatatableCheckedValue(oTable, type){
                var values = new Array();
                $.each(oTable.rows({search: 'applied'}).nodes(), function(idx, node){
                        $(node)
                                .find('input[type=checkbox][class=chk_'+type+']:checked')
                                .each(function(idx,element){
                                        values.push($(this).val());
                                });
                });
                return values
        }

        /***
         * Deprecated Handle buttons of datatables for data managment
         ***/
        function updateDatatableButton(oTable, type){
            var values = new Array();
            var nb_selected = getPurgeDatatableCheckedValue(oTable, type).length;
                if ( nb_selected <= 0) {
                        $("[id^=btn_"+type+"_]").each(function(){
                             this.disabled = true;
                        });
                        
                        $("[id^=all_chk_"+type+"]")[0].checked = false;
            } else {
                        $("[id^=btn_"+type+"_]").each(function(){
                             this.disabled = false;
                        });
                }
        }


function set_active_tab(active_tab){
	if (!endsWith(window.location.href, "active_tab=" + active_tab)) {
		window.location.assign(window.location.href+"&active_tab="+active_tab);
	} else {
		window.location.assign(window.location.href);
	}
}


function getDatatableCheckedValue(stat_tables){
	var values = new Array(),
		$table = $('div[id^=wrapper_datatable]:visible'),
		oDatatable = stat_tables[ $table.attr('id').split('_')[2] ];
	
	$.each(oDatatable.rows().nodes(), function(idx, node){
		$(node)
			.find('input[type=checkbox][class=chk_stat_element]:checked')
			.each(function(idx,element){
				values.push($(this).val());
			});
	});
	
	return values
}


function drawProjectDistribution(stat_tables) {
	var $group_by 	= $("input[type=radio][name=group_by]:checked"),
		group_by 	= $group_by.val(),
		real_group 	= $group_by.attr('id').split('_')[1],
		role 		= $("input[type=radio][name=users_to_consider]:checked").val(),
		values		= getDatatableCheckedValue(stat_tables);
	
	$.ajax({
        url: "index.php?eID=tx_nG6&type=project_distribution&by="+group_by+"&role="+role+"&values="+values.join(","),
        dataType: 'json',
        success: function(val, status, xhr) {
        	// reformat the data with values to integer
        	for (var i=0; i <val.length; i++ ) {
        		val[i][1] = parseInt(val[i][1]);
        	}
        	$('#highcharts_graph').highcharts({
                chart: {
                    width: 930
                },
                title: {
	   		         text: 'Number of projects gathered by ' + real_group
   		      	},
                tooltip: {
            	    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                credits: { enabled: false },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            color: '#000000',
                            connectorColor: '#000000',
                            formatter: function() {
                                return '<b>'+ this.point.name + '</b>';
                            }
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    data: val
                }]
            });
        }
	});
}

function drawProjectEvolution(stat_tables){
	var $group_by 	= $("input[type=radio][name=group_by]:checked"),
		group_by 	= $group_by.val(),
		real_group 	= $group_by.attr('id').split('_')[1],
		role 		        = $("input[type=radio][name=users_to_consider]:checked").val(),
		values		= getDatatableCheckedValue(stat_tables),
		cumulate 	= $("#chk_cumulate")[0].checked  ? "1" : "0" ;
	
	$.ajax({
        url: "index.php?eID=tx_nG6&type=project_evolution&by="+group_by+"&role="+role+"&cumulate="+cumulate+"&values="+values.join(","),
        dataType: 'json',
        success: function(val, status, xhr) {
        	// reformat the data with values to integer
        	for (var i=0; i <val.length; i++ ) {
        		val[i][0] = parseInt(val[i][0])*1000;
        		val[i][1] = parseInt(val[i][1]);
        	}
        	chart = new Highcharts.StockChart({
    		    chart: {
    		        renderTo: 'highcharts_graph'
    		    },
    		    rangeSelector: {
    		        selected: 4
    		    },
    		    yAxis: {
    		    	min: 0,
    		    	plotLines: [{
    		    		value: 0,
    		    		width: 2,
    		    		color: 'silver'
    		    	}]
    		    },
    		    credits: { enabled: false },
    		    series: [{name: "number of project", data: val}]
    		});
            }
	});
}

function drawDataDistribution(stat_tables){
	var $group_by 	= $("input[type=radio][name=group_by]:checked"),
		group_by 	= $group_by.val(),
		real_group 	= $group_by.attr('id').split('_')[1],
		role 		= $("input[type=radio][name=users_to_consider]:checked").val(),
		values		= getDatatableCheckedValue(stat_tables),
		bases 		= $("#chk_display_bases").attr("checked") ==  "checked" ? "1" : "0",
		analysis 	= $("#chk_includes_analyses").attr("checked") ==  "checked" ? "1" : "0";
	
	$.ajax({
                url: "index.php?eID=tx_nG6&type=data_distribution&by="+group_by+"&role="+role+"&get_analyzes="+analysis+"&bases="+bases+"&values="+values.join(","),
                dataType: 'json',
                success: function(val, status, xhr) {
                        // reformat the data with values to integer
                        for (var i=0; i <val.length; i++ ) {
                                val[i][1] = parseInt(val[i][1]);
                        }
                        $('#highcharts_graph').highcharts({
                        chart: {
                            width: 930
                        },
                        title: {
                                         text: 'Data storage size gathered by ' + real_group
                                },
                        tooltip: {
                                  formatter: function() {
                                        return '<b>'+ get_octet_string_representation(this.point.y, 2)+'</b> ('+Highcharts.numberFormat(this.percentage, 2)+'%)' ;
                                }
                                 ,shared: true
                        },
                        credits: { enabled: false },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    color: '#000000',
                                    connectorColor: '#000000',
                                    formatter: function() {
                                        return '<b>'+ this.point.name + '</b>';
                                    }
                                }
                            }
                        },
                        series: [{
                            type: 'pie',
                            data: val
                        }]
                    });
                }
	});
}


function drawDataEvolution(stat_tables){
	var $group_by 	= $("input[type=radio][name=group_by]:checked"),
		group_by 	= $group_by.val(),
		real_group 	= $group_by.attr('id').split('_')[1],
		role 		= $("input[type=radio][name=users_to_consider]:checked").val(),
		values		= getDatatableCheckedValue(stat_tables),
		bases 		= $("#chk_display_bases")[0].checked ? "1" : "0",
		analysis 	= $("#chk_includes_analyses")[0].checked ? "1" : "0",
		cumulate 	= $("#chk_cumulate")[0].checked ? "1" : "0" ;
	$.ajax({
        url: "index.php?eID=tx_nG6&type=data_evolution&by="+group_by+"&role="+role+"&cumulate="+cumulate+"&get_analyzes="+analysis+"&bases="+bases+"&values="+values.join(","),
        dataType: 'json',
        success: function(val, status, xhr) {
        	// reformat the data with values to integer
        	var val_stored = [];
        	var val_purged = [];
        	for (var i=0; i <val.length; i++ ) {
        		val_stored[i] = [parseInt(val[i][0])*1000,parseInt(val[i][1])];
        		val_purged[i] = [parseInt(val[i][0])*1000,parseInt(val[i][2])];

        	}
        	var seriesData = [];
        	seriesData = [
        	              {	   
        	            	  type:"line",
        	            	  name: "Size of data still stored", 
        	            	  data: val_stored,
                               color: "green"
                              
        	            },{
        	            	type:"line",
        	            	name: "Size of data (stored + purged)", 
        	            	data: val_purged, 
                            color: "blue",
                            dashStyle: 'dash'
                          
        	            }
        	          ],

        	chart = new Highcharts.StockChart({
    		    chart: {
    		        renderTo: 'highcharts_graph',
    		    },
    		    rangeSelector: {
    		        selected: 4
    		    },
    		    yAxis: [
    		    	{
    		    		gridLineWidth: 1,
    		    		title: {
                        text: 'Size of data still stored',
                        style: {
                            color: "green"
                        }
                    },
                    min: 0
    		    	},
    		    	{
    		    		gridLineWidth: 1,
    		    		title: {
                        text: 'Size of data (stored + purged)',
                        style: {
                            color: "blue"
                        }
                    },
                    min: 0
                    , opposite: true},
    		    	
    		    ],
    		    tooltip: {
    		    	formatter: function() {
    		    	    var toolTipTxt = '<b>'+ Highcharts.dateFormat('%B %Y', this.x) + ":" +'</b>';  
    		    	      $.each(this.points, function(i, point) {
    		    	        toolTipTxt += '<br/><span style="color:'+ point.series.color +'">  ' + point.series.name + ': ' + get_octet_string_representation(point.y, 2)+'</span>';
    		    	    });
    		    	    return toolTipTxt;
    		    	} ,shared: true,
    		    },
    		    credits: { enabled: false },
    		    series: seriesData,
    		    legend:{ enabled: true }
    		});
        }
	});
}

function initStatMenuDataTables () {
	var stat_tables = {};

	stat_tables["laboratories"] = $("#data_table_laboratories").DataTable({
		"language": {
			"info": 'Showing _START_ to _END_ of _MAX_ entries <strong><small id="nb_selected_span"></small></strong>'
		 },
  		"order": [[ 1, "asc" ]],
  		"columns": [
  		         { "orderable": false  },
  		         null,
  		         null,
  		         null,
  		         null]
  	});
	stat_tables["organizations"] = $("#data_table_organizations").DataTable({
		"language": {
			"info": 'Showing _START_ to _END_ of _MAX_ entries <strong><small id="nb_selected_span"></small></strong>'
		 },
  		"order": [[ 1, "asc" ]],
  		"columns": [
  		         { "orderable": false  },
  		         null,
  		         null]
  	});
	stat_tables["locations"] = $("#data_table_locations").DataTable({
		"language": {
			"info": 'Showing _START_ to _END_ of _MAX_ entries <strong><small id="nb_selected_span"></small></strong>'
		 },
  		"order": [[ 1, "asc" ]],
  		"columns": [
  		         { "orderable": false  },
  		         null,
  		         null]
  	});
	
	$("#data_table_laboratories, #data_table_organizations, #data_table_locations").on( 'draw.dt', function () {
		var $table = $('div[id^=wrapper_datatable]:visible'),
			oDatatable = stat_tables[ $table.attr('id').split('_')[2] ];
		
		$.each(oDatatable.rows().nodes(), function(idx, node){
			$(node)
				.find('input[type=checkbox][class=chk_stat_element]')
				.change(function(){
					updateRefreshButton (stat_tables);
				});
		});
		updateRefreshButton (stat_tables);
	});
	
	$.each(stat_tables,function(idx, table){
		table.draw();
	});
	
	return stat_tables;
}

function refreshDataTable(tables) {
	var role = $("input[type=radio][name=users_to_consider]:checked").val(),
		by = $("input[type=radio][name=group_by]:checked").val(),
		group_by = $("input[type=radio][name=group_by]:checked").attr("id").split("_")[1];
	
	$("input[type=checkbox][class=chk_stat_element], [id^=chk_all_] ").each(function(id, element){
		element.checked = false;
	});
	$.ajax({
        url: "index.php?eID=tx_nG6&type=project_data_repartition&role=" + role + "&by=" + by,
        dataType: 'json',
        success: function(val, status, xhr) { 
        	oTable = tables[group_by];
        	oTable.clear();
        	if( group_by == 'laboratories' ) {
        		$.each(val, function(key,values){
        			var checkbox = '<center><input class="chk_stat_element" type="checkbox" value="' + key + '"></center>' ; 
        			oTable.row.add( [checkbox, key, values["organism"], values["location"], values["count"] ]);
        		});
        		
        	} else {
        		$.each(val, function(key,values){
        			var checkbox = '<center><input class="chk_stat_element" type="checkbox" value="' + key + '"></center>' ;
        			oTable.row.add( [checkbox, key, values["count"] ]);
        		});
        	}
        	//draw then show/hide
        	oTable.draw();
        	$('div[id^=wrapper_datatable_]').each(function(id, element){
        		$(element).hide();
        	});
        	$('#wrapper_datatable_' + group_by).show();
        	
        	updateRefreshButton(tables);
        	$("input[type=checkbox][class=chk_stat_element]").change(function(){
        		updateRefreshButton(tables);
        	});
        }
    });
}


function updateRefreshButton (stat_tables) {
	var nb_selected = getDatatableCheckedValue(stat_tables).length;
   	if ( nb_selected <= 0) {
   		$("#refresh_graph_btn")[0].disabled = true;
   		$("#refresh_graph_btn").tooltip({
   			title : 'Select one or more element(s)'
   		});
   		$('#nb_selected_span').html('');
   		$("[id^=chk_all_]")[0].checked = false;
    } 
   	else{
   		$('#nb_selected_span').html('( ' + nb_selected + ' selected )');
   		$("#refresh_graph_btn")[0].disabled = false;
   		$("#refresh_graph_btn").tooltip('destroy');
   	}
}


function updateUsersButtonStatus() {
	$('.multipleu-selection-btn').each(function(){
		if ($(":checked[id^=chk_user_]").size() == 0) {
            $(this).attr('disabled', 'disabled');
        } else if ($(":checked[id^=chk_user_]").size() > 0) {
        	$(this).removeAttr('disabled');
        }
    });
	$('.singleu-selection-btn').each(function(){
       	if ($(":checked[id^=chk_user_]").size() !=1 ) {
            $(this).attr('disabled', 'disabled');
        } else {
        	$(this).removeAttr('disabled');
        }
    });

	$(".nou-selection-btn").each(function(){ $(this).removeAttr('disabled'); });
}
