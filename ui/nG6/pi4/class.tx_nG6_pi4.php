<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/Classes/Controller/class.tx_nG6_utils.php');
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/Classes/Controller/tx_nG6_db.php');
use TYPO3\CMS\Core\Context\Context;
use nG6\Controller\tx_nG6_db;
use nG6\Controller\tx_nG6_utils;
class tx_nG6_pi4 extends \TYPO3\CMS\Frontend\Plugin\AbstractPlugin {
	var $prefixId = 'tx_nG6_pi1';		// Same as class name
	var $scriptRelPath = 'pi4/class.tx_nG6_pi4.php';	// Path to this script relative to the extension dir.
	var $extKey = 'nG6';	// The extension key.
	//var $pi_checkCHash = TRUE;
	
	/**
	 * Main method of your PlugIn
	 *
	 * @param	string		$content: The content of the PlugIn
	 * @param	array		$conf: The PlugIn Configuration
	 * @return	The content that should be displayed on the website
	 */
	function main($content,$conf)	{
	    $context = \TYPO3\CMS\Core\Utility\GeneralUtility ::makeInstance(Context::class);
	    $user_id = $context->getPropertyFromAspect('frontend.user', 'id');
	    if (strstr($this->cObj->currentRecord,'tt_content'))	{
			$conf['pidList'] = $this->cObj->data['pages'];
			$conf['recursive'] = $this->cObj->data['recursive'];
		}

		$GLOBALS["TSFE"]->set_no_cache();  

		$this->conf=$conf;		// Setting the TypoScript passed to this function in $this->conf
		$this->pi_setPiVarDefaults();
		$this->pi_loadLL();		// Loading the LOCAL_LANG values
		//$this->pi_USER_INT_obj=1;	// Configuring so caching is not expected. This value means that no cHash params are ever set. We do this, because it's a USER_INT object!
		
		// Gets params from the pi1 plugin
		//$p1Vars = \TYPO3\CMS\Core\Utility\GeneralUtility::GPMerged('tx_nG6_pi1');
		$p1Vars = \TYPO3\CMS\Core\Utility\GeneralUtility::_GPmerged('tx_nG6_pi1');
		
		if (tx_nG6_db::user_is_authorized($user_id, $this->piVars['project_id'], $this->piVars['run_id'])) {
		
			if ($p1Vars['project_id'] && $p1Vars['run_id'] && $p1Vars['analyze_id']) {
				$content = '<p '.$this->pi_classParam('path').'>'. $this->pi_list_linkSingle($this->pi_getLL('Projets','Projets'),0, 1, array());
				// First select the project it belongs to
				$queryParts = array(
					'SELECT' => 'name',
					'FROM' => 'tx_nG6_project',
					'WHERE' => 'uid='.$p1Vars['project_id'],
					'GROUPBY' => '',
					'ORDERBY' => '',
					'LIMIT' => ''
				);
				$project_res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
				$project_vals = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($project_res);
				$mergeArr = array('project_id'=>$p1Vars['project_id']);
				$content .= ' > ' . $this->pi_list_linkSingle($this->pi_getLL($project_vals['name'],$project_vals['name']),0, 1, $mergeArr);
				$mergeArr = array('project_id'=>$p1Vars['project_id'], 'run_id'=>$p1Vars['run_id']);
				// Then the run
				$queryParts = array(
					'SELECT' => 'name',
					'FROM' => 'tx_nG6_run',
					'WHERE' => 'uid='.$p1Vars['run_id'],
					'GROUPBY' => '',
					'ORDERBY' => '',
					'LIMIT' => ''
				);
				$run_res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
				$run_vals = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($run_res);
				$content .= ' > ' . $this->pi_list_linkSingle($this->pi_getLL($run_vals['name'],$run_vals['name']),0, 1, $mergeArr);
				// And finaly the analyze
				$queryParts = array(
					'SELECT' => 'name',
					'FROM' => 'tx_nG6_analyze',
					'WHERE' => 'uid='.$p1Vars['analyze_id'],
					'GROUPBY' => '',
					'ORDERBY' => '',
					'LIMIT' => ''
				);
				$analyze_res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
				$analyze_vals = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($analyze_res);				
				$content .= ' > ' .$analyze_vals['name'];
				$content .= '</p>';
				return $content;
			} elseif ($p1Vars['run_id'] && $p1Vars['analyze_id']) {
				// First select the project it belongs to
				$queryParts = array(
					'SELECT' => 'name',
					'FROM' => 'tx_nG6_run',
					'WHERE' => 'uid='.$p1Vars['run_id'],
					'GROUPBY' => '',
					'ORDERBY' => '',
					'LIMIT' => ''
				);
				$run_res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
				$run_vals = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($run_res);
				$content = '<p '.$this->pi_classParam('path').'>'. $this->pi_list_linkSingle($this->pi_getLL('Runs','Runs'),0, 1, array());
				$mergeArr = array('project_id'=>$p1Vars['project_id'], 'run_id'=>$p1Vars['run_id']);
				$content .= ' > ' . $this->pi_list_linkSingle($this->pi_getLL($run_vals['name'],$run_vals['name']),0, 1, $mergeArr);
				// And finaly the analyze
				$queryParts = array(
					'SELECT' => 'name',
					'FROM' => 'tx_nG6_analyze',
					'WHERE' => 'uid='.$p1Vars['analyze_id'],
					'GROUPBY' => '',
					'ORDERBY' => '',
					'LIMIT' => ''
				);
				$analyze_res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
				$analyze_vals = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($analyze_res);	
				$content .= ' > ' .$analyze_vals['name'];
				$content .= '</p>';
				return $content;
			} elseif ($p1Vars['project_id'] && $p1Vars['analyze_id']) {
				// First select the project it belongs to
				$queryParts = array(
					'SELECT' => 'name',
					'FROM' => 'tx_nG6_project',
					'WHERE' => 'uid='.$p1Vars['project_id'],
					'GROUPBY' => '',
					'ORDERBY' => '',
					'LIMIT' => ''
				);
				$project_res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
				$project_vals = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($project_res);
				$mergeArr = array('project_id'=>$p1Vars['project_id']);
				$content = '<p '.$this->pi_classParam('path').'>'. $this->pi_list_linkSingle($this->pi_getLL('Projets','Projets'),0, 1, array());
				$content .= ' > ' . $this->pi_list_linkSingle($this->pi_getLL($project_vals['name'],$project_vals['name']),0, 1, $mergeArr);
				// And finaly the analyze
				$queryParts = array(
					'SELECT' => 'name',
					'FROM' => 'tx_nG6_analyze',
					'WHERE' => 'uid='.$p1Vars['analyze_id'],
					'GROUPBY' => '',
					'ORDERBY' => '',
					'LIMIT' => ''
				);
				$analyze_res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
				$analyze_vals = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($analyze_res);	
				$content .= ' > '.$analyze_vals['name'];
				$content .= '</p>';
				return $content;
			} elseif ($p1Vars['project_id'] && $p1Vars['run_id']) {
				// First let's get the run's project name
				$queryParts = array(
					'SELECT' => 'name',
					'FROM' => 'tx_nG6_project',
					'WHERE' => 'uid='.$p1Vars['project_id'],
					'GROUPBY' => '',
					'ORDERBY' => '',
					'LIMIT' => ''
				);
				$project_res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
				$project_vals = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($project_res);
				$project_name = $project_vals['name'];
				// The the run
				$queryParts = array(
					'SELECT' => 'name',
					'FROM' => 'tx_nG6_run',
					'WHERE' => 'uid='.$p1Vars['run_id'],
					'GROUPBY' => '',
					'ORDERBY' => '',
					'LIMIT' => ''
				);
				$run_res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
				$run_vals = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($run_res);
				$mergeArr = array('project_id'=>$p1Vars['project_id']);
				$content = '<p '.$this->pi_classParam('path').'>'. $this->pi_list_linkSingle($this->pi_getLL('Projets','Projets'),0, 1, array());
				$content .= ' > ' . $this->pi_list_linkSingle($this->pi_getLL($project_name,$project_name),0, 1, $mergeArr);
				$content .= ' > ' . $run_vals['name'] . '</p>';
				return $content;
			} elseif ($p1Vars['run_id']) {
				// The run
				$queryParts = array(
					'SELECT' => 'name',
					'FROM' => 'tx_nG6_run',
					'WHERE' => 'uid='.$p1Vars['run_id'],
					'GROUPBY' => '',
					'ORDERBY' => '',
					'LIMIT' => ''
				);
				$run_res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
				$run_vals = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($run_res);
				return '<p '.$this->pi_classParam('path').'>'.$this->pi_list_linkSingle($this->pi_getLL('Runs','Runs'),0, 1, array()).' > ' . $run_vals['name'] . '</p>';
			} elseif ($p1Vars['project_id']) {
				// The project
				$queryParts = array(
					'SELECT' => 'name',
					'FROM' => 'tx_nG6_project',
					'WHERE' => 'uid='.$p1Vars['project_id'],
					'GROUPBY' => '',
					'ORDERBY' => '',
					'LIMIT' => ''
				);
				$project_res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
				$project_vals = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($project_res);
				return '<p '.$this->pi_classParam('path').'> '.$this->pi_list_linkSingle($this->pi_getLL('Projets','Projets'), 0, 1, array()).' > '.$project_vals['name'].'</p>';
			} else {
				return '';
			}
		} else {
			return '';
		}
	}
	
	/**
	 * Overloading of the tslib_pibase->pi_list_linkSingle function so piVars[showUid] is not set.
	 * Uses pi_linkTP for the linking
	 *
	 * @param	string		The content string to wrap in <a> tags
	 * @param	integer		UID of the record for which to display details (basically this will become the value of [showUid]
	 * @param	boolean		See pi_linkTP_keepPIvars
	 * @param	array		Array of values to override in the current piVars. Same as $overrulePIvars in pi_linkTP_keepPIvars
	 * @param	boolean		If true, only the URL is returned, not a full link
	 * @param	integer		Alternative page ID for the link. (By default this function links to the SAME page!)
	 * @return	string		The input string wrapped in <a> tags
	 * @see pi_linkTP(), pi_linkTP_keepPIvars()
	 */
	function pi_list_linkSingle($str,$uid,$cache=FALSE,$mergeArr=array(),$urlOnly=FALSE,$altPageId=0)	{
		if ($this->prefixId)	{
			if ($cache)	{
				$overrulePIvars=(array)$mergeArr;
				$str = $this->pi_linkTP($str,Array($this->prefixId=>$overrulePIvars),$cache,$altPageId);
			} else {
				$overrulePIvars=(array)$mergeArr;
				$str = $this->pi_linkTP_keepPIvars($str,$overrulePIvars,$cache,0,$altPageId);
			}

				// If urlOnly flag, return only URL as it has recently be generated.
			if ($urlOnly)	{
				$str = $this->cObj->lastTypoLinkUrl;
			}
		}
		return $str;
	}
		
}

if (defined('TYPO3') && $TYPO3_CONF_VARS[TYPO3]['XCLASS']['ext/nG6/pi4/class.tx_nG6_pi4.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3]['XCLASS']['ext/nG6/pi4/class.tx_nG6_pi4.php']);
}

?>
