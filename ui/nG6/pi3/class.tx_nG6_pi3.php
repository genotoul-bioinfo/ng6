<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/Classes/Controller/tx_nG6_db.php');
use TYPO3\CMS\Core\Context\Context;
use nG6\Controller\tx_nG6_db;


class tx_nG6_pi3 extends \TYPO3\CMS\Frontend\Plugin\AbstractPlugin {
	var $prefixId = 'tx_nG6_pi3';		// Same as class name
	var $scriptRelPath = 'pi3/class.tx_nG6_pi3.php';	// Path to this script relative to the extension dir.
	var $extKey = 'nG6';	// The extension key.
	//var $pi_checkCHash = TRUE;
	
	/**
	 * Main method of your PlugIn
	 *
	 * @param	string		$content: The content of the PlugIn
	 * @param	array		$conf: The PlugIn Configuration
	 * @return	The content that should be displayed on the website
	 */
	function main($content,$conf)	{
	    $context = \TYPO3\CMS\Core\Utility\GeneralUtility ::makeInstance(Context::class);
	    $user_id = $context->getPropertyFromAspect('frontend.user', 'id');
		if (strstr($this->cObj->currentRecord,'tt_content'))	{
			$conf['pidList'] = $this->cObj->data['pages'];
			$conf['recursive'] = $this->cObj->data['recursive'];
		}
				
		$this->conf=$conf;		// Setting the TypoScript passed to this function in $this->conf
		$this->pi_setPiVarDefaults();
		$this->pi_loadLL();		// Loading the LOCAL_LANG values
		//$this->pi_USER_INT_obj=1;	// Configuring so caching is not expected. This value means that no cHash params are ever set. We do this, because it's a USER_INT object!
				
		// Search into the project table
		$queryParts = array(
			'SELECT' => 'pro.*, rights.right_id ',
			'FROM' => 'fe_rights rights '
					.' INNER JOIN tx_nG6_project pro ON pro.uid=rights.project_id ',
		    'WHERE' => 'rights.fe_user_id='.$user_id
					.' AND (pro.name LIKE "%'.$GLOBALS['_POST']['text-search'].'%" '
					.' OR pro.description LIKE "%'.$GLOBALS['_POST']['text-search'].'%")',
			'GROUPBY' => '',
			'ORDERBY' => '',
			'LIMIT' => ''
		);
		$res_project = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
		$nb_project = $GLOBALS['TYPO3_DB']->sql_num_rows($res_project);
			
		// Search into the run table
		$queryParts = array(
			'SELECT' => 'run.*, rights.right_id ',
			'FROM' => 'fe_rights rights '
					.' INNER JOIN tx_nG6_project pro ON pro.uid=rights.project_id '
					.' INNER JOIN tx_nG6_project_run p_run ON pro.uid=p_run.project_id '
					.' INNER JOIN tx_nG6_run run ON run.uid=p_run.run_id',
		    'WHERE' => 'rights.fe_user_id='.$user_id
					.' AND (run.name LIKE "%'.$GLOBALS['_POST']['text-search'].'%" '
					.' OR run.description LIKE "%'.$GLOBALS['_POST']['text-search'].'%")',
			'GROUPBY' => '',
			'ORDERBY' => '',
			'LIMIT' => ''
		);
		$res_run = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
		$nb_run = $GLOBALS['TYPO3_DB']->sql_num_rows($res_run);
		

		// Search into the analyze table			
		// -- Search in Project analyzes
		$queryParts = array(
			'SELECT' => 'ana.*, rights.right_id ',
			'FROM' => 'fe_rights rights '
				.' INNER JOIN tx_nG6_project pro ON pro.uid=rights.project_id '
				.' INNER JOIN tx_nG6_project_analyze p_ana ON pro.uid=p_ana.project_id '
				.' INNER JOIN tx_nG6_analyze ana ON ana.uid=p_ana.analyze_id',
		    'WHERE' => 'rights.fe_user_id='.$user_id
				.' AND (ana.name LIKE "%'.$GLOBALS['_POST']['text-search'].'%" '
				.' OR ana.description LIKE "%'.$GLOBALS['_POST']['text-search'].'%")',
			'GROUPBY' => '',
			'ORDERBY' => '',
			'LIMIT' => ''
		);
		$res_pa = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
		$nb_analyze = $GLOBALS['TYPO3_DB']->sql_num_rows($res_pa);
		
		// -- Search in Run analyzes
		$queryParts = array(
			'SELECT' => 'ana.*, rights.right_id ',
			'FROM' => 'fe_rights rights '
				.' INNER JOIN tx_nG6_project pro ON pro.uid=rights.project_id '
				.' INNER JOIN tx_nG6_project_run p_run ON pro.uid=p_run.project_id '
				.' INNER JOIN tx_nG6_run run ON run.uid=p_run.run_id'
				.' INNER JOIN tx_nG6_run_analyze r_ana ON run.uid=r_ana.run_id'
				.' INNER JOIN tx_nG6_analyze ana ON ana.uid=r_ana.analyze_id',
		    'WHERE' => 'rights.fe_user_id='.$user_id
				.' AND (ana.name LIKE "%'.$GLOBALS['_POST']['text-search'].'%" '
				.' OR ana.description LIKE "%'.$GLOBALS['_POST']['text-search'].'%")',
			'GROUPBY' => '',
			'ORDERBY' => '',
			'LIMIT' => ''
		);
		$res_ra = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
		$nb_analyze += $GLOBALS['TYPO3_DB']->sql_num_rows($res_ra);
	

		// Finally...
		$content = '<h2>'.str_replace(array('###SPAN_WORD###', '###SPAN_LEN###'), array($GLOBALS['_POST']['text-search'], $nb_project + $nb_run + $nb_analyze), $this->getFieldHeader('search_for_text')).'</h2>';
		if ($nb_project > 0) {
			$content .= $this->pi_project_list_view($res_project);
		}
		if ($nb_run > 0) {
			$content .= $this->pi_run_list_view($res_run);
		}
		if ($nb_analyze > 0) {
			$content .= $this->pi_analyze_list_view($res_analyze);
		}
		return $this->pi_wrapInBaseClass($content);
	}

	/**
	 * Creates a list from a database query
	 *
	 * @param	ressource	$res: A database result ressource
	 * @return	A HTML list if result items
	 */
	function pi_project_list_view($res)	{
		$content = '<table'.$this->pi_classParam('listrow').'>
						<tr>
							<th>'.$this->getFieldHeader('project_name').'</th>
							<th>'.$this->getFieldHeader('description').'</th>
						</tr>';
		while($this->internal['currentRow'] = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res))	{
			$content .= '<tr>
				<td>'.$this->getFieldContent('name', array('project_id'=>$this->internal['currentRow']['uid'])).'</td>
				<td>'.$this->getFieldContent('description').'</td>
				</tr>';
		}
		$content .= '</table>';
		return $content;
	}

	/**
	 * Creates a list from a database query
	 *
	 * @param	ressource	$res: A database result ressource
	 * @return	A HTML list if result items
	 */
	function pi_run_list_view($res)	{
		$content .= '<table'.$this->pi_classParam('listrow').'>
						<tr>
							<th>'.$this->getFieldHeader('run_name').'</td>
							<th>'.$this->getFieldHeader('project_name').'</td>
							<th>'.$this->getFieldHeader('date').'</td>
							<th>'.$this->getFieldHeader('species').'</td>
							<th>'.$this->getFieldHeader('data_nature').'</td>
							<th>'.$this->getFieldHeader('type').'</td>
							<th>'.$this->getFieldHeader('nb_sequences').'</td>
							<th>'.$this->getFieldHeader('full_seq_size').'</td>
							<th>'.$this->getFieldHeader('description').'</td>
							<th>'.$this->getFieldHeader('sequencer').'</td>
						</tr>';
						
		while($this->internal['currentRow'] = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res))	{
			// First let's get the run's project name
			$queryParts = array(
				'SELECT' => 'project_id',
				'FROM' => 'tx_nG6_project_run',
				'WHERE' => 'run_id='.$this->internal['currentRow']['uid'],
				'GROUPBY' => '',
				'ORDERBY' => '',
				'LIMIT' => ''
			);
			$res1 = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
			$val = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res1);
			$project_uid = $val['project_id'];
			$queryParts = array(
				'SELECT' => '*',
				'FROM' => 'tx_nG6_project',
				'WHERE' => 'uid='.$project_uid,
				'GROUPBY' => '',
				'ORDERBY' => '',
				'LIMIT' => ''
			);
			$res2 = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
			$val = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res2);
			$project_name = $val['name'];

			$content .= '<tr>
				<td>'.$this->getFieldContent('name', array('run_id'=>$this->internal['currentRow']['uid'], 'project_id'=>$project_uid)).'</td>
				<td>'.$project_name.'</td>
				<td>'.$this->getFieldContent('date').'</td>
				<td>'.$this->getFieldContent('species').'</td>
				<td>'.$this->getFieldContent('data_nature').'</td>
				<td>'.$this->getFieldContent('type').'</td>
				<td>'.$this->getFieldContent('nb_sequences').'</td>
				<td>'.$this->getFieldContent('full_seq_size').'</td>
				<td>'.$this->getFieldContent('description').'</td>
				<td>'.$this->getFieldContent('sequencer').'</td>
				</tr>';
		}
		$content .= '</table>';
		return $content;
	}

	/**
	 * Creates a list from a database query
	 *
	 * @param	ressource	$res: A database result ressource
	 * @return	A HTML list if result items
	 */
	function pi_analyze_list_view($gres)	{
				
		$content.='<table'.$this->pi_classParam('listrow').'>
					<tr>
						<th>'.$this->getFieldHeader('name').'</td>
						<th>'.$this->getFieldHeader('description').'</td>
						<th>'.$this->getFieldHeader('software').'</td>
						<th>'.$this->getFieldHeader('version').'</td>
					</tr>';

		while($this->internal['currentRow'] = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($gres))	{
		
			// First let's get the project and/or run id(s)
			$queryParts = array(
				'SELECT' => 'project_id',
				'FROM' => 'tx_nG6_project_analyze',
				'WHERE' => 'analyze_id='.$this->internal['currentRow']['uid'],
				'GROUPBY' => '',
				'ORDERBY' => '',
				'LIMIT' => ''
			);
			$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
			// If the analyze is a project analyze
			if ($GLOBALS['TYPO3_DB']->sql_num_rows($res) > 0) {
				$val = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
				$mergarr = array('analyze_id'=>$this->internal['currentRow']['uid'], 'project_id'=>$val['project_id']);
			} else { // the analyze has been done on a run
				$queryParts = array(
					'SELECT' => 'run_id',
					'FROM' => 'tx_nG6_run_analyze',
					'WHERE' => 'analyze_id='.$this->internal['currentRow']['uid'],
					'GROUPBY' => '',
					'ORDERBY' => '',
					'LIMIT' => ''
				);
				$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
				$val = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
				$run_uid = $val['run_id'];
				$queryParts = array(
					'SELECT' => 'project_id',
					'FROM' => 'tx_nG6_project_run',
					'WHERE' => 'run_id='.$run_uid,
					'GROUPBY' => '',
					'ORDERBY' => '',
					'LIMIT' => ''
				);
				$res = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($queryParts);
				$val = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
				$project_uid = $val['project_id'];
				$mergarr = array('analyze_id'=>$this->internal['currentRow']['uid'], 'run_id'=>$run_uid, 'project_id'=>$project_uid);
			}

			$content .= '<tr>
				<td>'.$this->getFieldContent('name', $mergarr).'</td>
				<td>'.$this->getFieldContent('description').'</td>
				<td>'.$this->getFieldContent('software').'</td>
				<td>'.$this->getFieldContent('version').'</td>
				</tr>';

		}
		$content .= '</table>';
		return $content;
	}
	
	/**
	 * Returns the label for a fieldname from local language array
	 *
	 * @param	[type]		$fN: ...
	 * @return	[type]		...
	 */
	function getFieldHeader($fN) {
		switch($fN) {
			default:
				return $this->pi_getLL('listFieldHeader_'.$fN,'['.$fN.']');
			break;
		}
	}
	/**
	 * Returns the content of a given field
	 *
	 * @param	string		$fN: name of table field
	 * @return	Value of the field
	 */
	function getFieldContent($fN, $mergeArr=array())	{
		switch($fN) {
			case 'uid':
				return $this->pi_list_linkSingle($this->internal['currentRow'][$fN],$this->internal['currentRow']['uid'],1, $mergeArr, FALSE, $this->conf["redirection_page"]);	// The "1" means that the display of single items is CACHED! Set to zero to disable caching.
			break;
			case 'name':
				return $this->pi_list_linkSingle($this->internal['currentRow'][$fN],$this->internal['currentRow']['uid'],1, $mergeArr, FALSE, $this->conf["redirection_page"]);	// The "1" means that the display of single items is CACHED! Set to zero to disable caching.
			break;
			case 'date':
					// %A %e. %B %Y
				return strftime('%d-%m-%y',$this->internal['currentRow']['date']);
			break;			
			default:
				return $this->internal['currentRow'][$fN];
			break;
		}
	}
	
	/**
	 * Overload the tslib_pibase class
	 * Wraps the $str in a link to a single display of the record (using piVars[showUid])
	 * Uses pi_linkTP for the linking
	 *
	 * @param	string		The content string to wrap in <a> tags
	 * @param	integer		UID of the record for which to display details (basically this will become the value of [showUid]
	 * @param	boolean		See pi_linkTP_keepPIvars
	 * @param	array		Array of values to override in the current piVars. Same as $overrulePIvars in pi_linkTP_keepPIvars
	 * @param	boolean		If true, only the URL is returned, not a full link
	 * @param	integer		Alternative page ID for the link. (By default this function links to the SAME page!)
	 * @return	string		The input string wrapped in <a> tags
	 * @see pi_linkTP(), pi_linkTP_keepPIvars()
	 */
	function pi_list_linkSingle($str,$uid,$cache=FALSE,$mergeArr=array(),$urlOnly=FALSE,$altPageId=0)	{
		if ($this->prefixId)	{
			if ($cache)	{
				$overrulePIvars=(array)$mergeArr;
				$str = $this->pi_linkTP($str,Array("tx_nG6_pi1"=>$overrulePIvars),$cache,$altPageId);
			} else {
				$overrulePIvars=(array)$mergeArr;
				$str = $this->pi_linkTP_keepPIvars($str,$overrulePIvars,$cache,0,$altPageId);
			}

				// If urlOnly flag, return only URL as it has recently be generated.
			if ($urlOnly)	{
				$str = $this->cObj->lastTypoLinkUrl;
			}
		}
		return $str;
	}	
	
}

if (defined('TYPO3') && $TYPO3_CONF_VARS[TYPO3]['XCLASS']['ext/nG6/pi3/class.tx_nG6_pi3.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3]['XCLASS']['ext/nG6/pi3/class.tx_nG6_pi3.php']);
}

?>
