<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/Classes/Controller/class.tx_nG6_utils.php');
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/Classes/Controller/tx_nG6_db.php');
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/libs/Smarty.class.php'); 
use TYPO3\CMS\Core\Context\Context;
use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use nG6\Controller\tx_nG6_db;
use nG6\Controller\tx_nG6_utils;

class tx_nG6_pi5 extends \TYPO3\CMS\Frontend\Plugin\AbstractPlugin {
	var $prefixId = 'tx_nG6_pi5';		// Same as class name
	var $scriptRelPath = 'pi5/class.tx_nG6_pi5.php';	// Path to this script relative to the extension dir.
	var $extKey = 'nG6';	// The extension key.
	
	//var $pi_checkCHash = TRUE;
	
	/**
	 * Main method of your PlugIn
	 *
	 * @param	string		$content: The content of the PlugIn
	 * @param	array		$conf: The PlugIn Configuration
	 * @return	The content that should be displayed on the website
	 */
	function main($content,$conf)	{
	    $context = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(Context::class);
	    $user_id = $context->getPropertyFromAspect('frontend.user', 'id');
	    $user_name = $context->getPropertyFromAspect('frontend.user', 'username');
		if (strstr($this->cObj->currentRecord,'tt_content'))	{
			$conf['pidList'] = $this->cObj->data['pages'];
			$conf['recursive'] = $this->cObj->data['recursive'];
		}
		$this->pi_loadLL();
		$this->conf=$conf;
		$this->pi_setPiVarDefaults();
		//$this->pi_USER_INT_obj=1;

		// Add the jquery libs + the tree plugins and its css
		$GLOBALS['TSFE']->additionalHeaderData[$this->prefixId] = '
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/jquery.min.js"></script>
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/jquery.jstree.min.js"></script>
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/jflow.min.js"></script>	
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/bootstrap.min.js"></script>
			<script type="text/javascript" src="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/js/tx_nG6_pi5.js"></script>
							
			<link type="text/css" rel="stylesheet" media="screen" href="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/css/bootstrap-theme.min.css"/>
			<link type="text/css" rel="stylesheet" media="screen" href="'.\TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($this->extKey)).'res/css/tx_nG6.css"/>';

		$smarty = new Smarty();
		$smarty->setTemplateDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/pi5');
		$smarty->setCompileDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/templates_c');
		$smarty->setCacheDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/cache');
		$smarty->setConfigDir(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'/res/smarty/configs');
		$smarty->security = true;
		$smarty->security_settings['MODIFIER_FUNCS'] = array('count');

		if (isset($this->LOCAL_LANG[$this->LLkey]) && count($this->LOCAL_LANG[$this->LLkey]) > 0) {
			$smarty->assign('llang', $this->LOCAL_LANG[$this->LLkey]);
		} else {
			$smarty->assign('llang', $this->LOCAL_LANG['default']);
		}
		
		// get server url from pi1
		$configurationManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Configuration\\BackendConfigurationManager');
		//$configurationManager->currentPageId = 1;
		$extbaseFrameworkConfiguration = $configurationManager->getTypoScriptSetup();
		$serverURL = $extbaseFrameworkConfiguration['plugin.']['tx_nG6_pi1.']["server_url"];
		$smarty->assign('server_url', $serverURL);
		$smarty->assign('server_name', $this->conf["server_name"]);
		$smarty->assign('data_folder', $this->conf["data"]);
		$smarty->assign('temp_folder', $this->conf["temp"]);
		$smarty->assign('user_login', $user_name);
		if ($this->conf["directory_prefix"]) {
			$smarty->assign('directory_prefix', $this->conf["directory_prefix"]);
		} else {
			$smarty->assign('directory_prefix', "");
		}
		$smarty->assign('user_id', $user_id);
		$smarty->assign('tmp_url', substr($this->conf["temp"], strpos($this->conf["temp"], "fileadmin")));

		// Build the lists of all elements
		if (!$context->getPropertyFromAspect('frontend.user', 'isLoggedIn')) { $user_id = null; }
		else { $user_id = $user_id; }
		$projects = tx_nG6_db::get_user_projects($user_id, 'tx_nG6_project.name');
		$smarty->assign('projects', $projects);
		
		return $smarty->fetch('download_view.tpl');

	}
		
}

if (defined('TYPO3') && $TYPO3_CONF_VARS[TYPO3]['XCLASS']['ext/nG6/pi5/class.tx_nG6_pi5.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3]['XCLASS']['ext/nG6/pi5/class.tx_nG6_pi5.php']);
}

?>
