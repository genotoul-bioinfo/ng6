{*
Copyright (C) 2009 INRA
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*}

<input type="hidden" id="server_name" value="{$server_name}" />
<input type="hidden" id="data_folder" value="{$data_folder}" />
<input type="hidden" id="temp_folder" value="{$temp_folder}" />
<input type="hidden" id="directory_prefix" value="{$directory_prefix}" />

<input type="hidden" id="user_id" value="{$user_id}" />
<input type="hidden" id="tmp_url" value="{$tmp_url}" />
<input type="hidden" id="server_url" value="{$server_url}" />
<input type="hidden" id="user_login" value="{$user_login}" />

<div class="sub-content sc-top">
	<div class="ng6-content-header-left download">
		<h2>Download <small> center </small> </h2>
	</div>
	<div class="ng6-content-header-right">
	    <h4>How to download your data</h4>
	    <p>
		<strong>(i)</strong> Use the select tree bellow to select the data you want to download. You can expand the tree using the arrows on the left.
		<strong>(ii)</strong> Then select the way you want to download your data using the dropdown menu.<strong>(iii)</strong> Click on the download button 
		to start the download.
		</p>
		
        <form class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-4 control-label">Available downloads</label>
                <div class="col-sm-4">
                    <select id="available-download-types" class="form-control">
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-4"></div>
                <div class="col-sm-4"></div>
                <div class="col-sm-4">
                    <button id="download_btn" class="btn btn-default disabled" type="button"><i class="glyphicon glyphicon-floppy-save"></i> Download</button>
                </div>
            </div>
        </form>
		
		<h4>Downloaded files <span id="total_data_size"></span>: </h4>
		<div id="warning_message" class="alert alert-danger" style="display:none"></div>
		<div id="download_list">
			<p>
				No run neither any analyze selected. <br/>
				Please select below the data you want to download...
			</p>
		</div>
		
	</div>
	<div style="clear:both"></div>
</div>

<div class="sub-content sc-bottom">
	{assign var="tree_values" value=""}
	{foreach from=$projects key=project_id item=project_values}
		{assign var="li_value" value="<li class='jstree-closed' id='$project_id'> <a href = '#'>"}
		{$li_value = $li_value|cat:"Project "|cat:$project_values.name}
		{$li_value = $li_value|cat:"</a></li>"}
		{$tree_values = $tree_values|cat:$li_value}
	{/foreach}
	<input type='hidden' name = 'html_tree' id='html_tree'  value="{$tree_values}" /> 
	<div id="download_tree"></div>
</div>

{include file='../template/modals.tpl'}


