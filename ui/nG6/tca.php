<?php
if (!defined ('TYPO3')) 	die ('Access denied.');


$TCA["fe_rights"] = Array (
	"ctrl" => $TCA["fe_rights_levels"]["ctrl"],
);

$TCA["fe_rights_levels"] = Array (
	"ctrl" => $TCA["fe_rights_levels"]["ctrl"],
	"interface" => Array (
		"showRecordFieldList" => "right_level_id, right_level_label"
	),
	"feInterface" => $TCA["fe_rights_levels"]["feInterface"],
	"columns" => Array (
		"right_level_id" => Array(
			"exclude" => 1,
			"label" => "LLL:EXT:lang/locallang_db.xml:fe_rights_levels.id",
			"config" => Array (
				"type" => "input",	
				"size" => "5",	
				"eval" => "trim",
			)
		),
		"right_level_label" => Array(
			"exclude" => 1,
			"label" => "LLL:EXT:lang/locallang_db.xml:fe_rights_levels.label",
			"config" => Array (
				"type" => "input",	
				"size" => "20",	
				"eval" => "trim",
			)
		)
	)
);


$TCA["tx_nG6_project"] = Array (
	"ctrl" => $TCA["tx_nG6_project"]["ctrl"],
	"interface" => Array (
		"showRecordFieldList" => "hidden,public,name,description"
	),
	"feInterface" => $TCA["tx_nG6_project"]["feInterface"],
	"columns" => Array (
		"hidden" => Array (		
			"exclude" => 1,
			"label" => "LLL:EXT:lang/locallang_general.xml:LGL.hidden",
			"config" => Array (
				"type" => "check",
				"default" => "0"
			)
		),
		"public" => Array (		
			"exclude" => 1,
			"label" => "LLL:EXT:lang/locallang_general.xml:LGL.public",
			"config" => Array (
				"type" => "check",
				"default" => "0"
			)
		),
		"name" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_project.name",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",	
				"eval" => "trim",
			)
		),
		"description" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_project.description",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",	
				"eval" => "trim",
			)
		),
	),
	"types" => Array (
		"0" => Array("showitem" => "hidden;;1;;1-1-1, name, description")
	)
);



$TCA["tx_nG6_run"] = Array (
	"ctrl" => $TCA["tx_nG6_run"]["ctrl"],
	"interface" => Array (
		"showRecordFieldList" => "hidden,name,date,directory,species,data_nature,type,nb_sequences,full_seq_size,description,sequencer"
	),
	"feInterface" => $TCA["tx_nG6_run"]["feInterface"],
	"columns" => Array (
		"hidden" => Array (		
			"exclude" => 1,
			"label" => "LLL:EXT:lang/locallang_general.xml:LGL.hidden",
			"config" => Array (
				"type" => "check",
				"default" => "0"
			)
		),
		"name" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_run.name",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",	
				"eval" => "trim",
			)
		),
		"date" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_run.date",		
			"config" => Array (
				"type" => "input",
				"size" => "8",
				"max" => "20",
				"eval" => "date",
				"checkbox" => "0",
				"default" => "0"
			)
		),
		"directory" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_run.directory",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",	
				"eval" => "trim",
			)
		),
		"species" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_run.species",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",	
				"eval" => "trim",
			)
		),
		"data_nature" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_run.data_nature",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",	
				"eval" => "trim",
			)
		),
		"type" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_run.type",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",	
				"eval" => "trim",
			)
		),
		"nb_sequences" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_run.nb_sequences",		
			"config" => Array (
				"type" => "input",
				"size" => "30",
				"eval" => "trim",
			)
		),
		"full_seq_size" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_run.full_seq_size",		
			"config" => Array (
				"type" => "input",
				"size" => "30",
				"eval" => "trim",
			)
		),
		"description" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_run.description",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",	
				"eval" => "trim",
			)
		),
		"sequencer" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_run.sequencer",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",	
				"eval" => "trim",
			)
		),
	),
	"types" => Array (
		"0" => Array("showitem" => "hidden;;1;;1-1-1, name, date, directory, species, data_nature, type, nb_sequences, full_seq_size, description, sequencer")
	),
	"palettes" => Array (
		"1" => Array("showitem" => "")
	)
);



$TCA["tx_nG6_analyze"] = Array (
	"ctrl" => $TCA["tx_nG6_analyze"]["ctrl"],
	"interface" => Array (
		"showRecordFieldList" => "hidden,parent_uid,class,name,description,date,directory,software,version,params"
	),
	"feInterface" => $TCA["tx_nG6_analyze"]["feInterface"],
	"columns" => Array (
		"hidden" => Array (		
			"exclude" => 1,
			"label" => "LLL:EXT:lang/locallang_general.xml:LGL.hidden",
			"config" => Array (
				"type" => "check",
				"default" => "0"
			)
		),
		"parent_uid" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_analyze.parent_uid",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",	
				"eval" => "trim",
			)
		),
		"class" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_analyze.class",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",	
				"eval" => "trim",
			)
		),
		"name" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_analyze.name",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",	
				"eval" => "trim",
			)
		),
		"description" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_analyze.description",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",	
				"eval" => "trim",
			)
		),
		"date" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_analyze.date",		
			"config" => Array (
				"type" => "input",
				"size" => "8",
				"max" => "20",
				"eval" => "date",
				"checkbox" => "0",
				"default" => "0"
			)
		),
		"directory" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_analyze.directory",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",	
				"eval" => "trim",
			)
		),
		"software" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_analyze.software",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",	
				"eval" => "trim",
			)
		),
		"version" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_analyze.version",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",	
				"eval" => "trim",
			)
		),
		"params" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_analyze.params",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",	
				"eval" => "trim",
			)
		),
	),
	"types" => Array (
		"0" => Array("showitem" => "hidden;;1;;1-1-1, parent_uid, name, class, description, date, directory, software, version, params")
	),
	"palettes" => Array (
		"1" => Array("showitem" => "")
	)
);



$TCA["tx_nG6_sample"] = Array (
	"ctrl" => $TCA["tx_nG6_sample"]["ctrl"],
	"interface" => Array (
		"showRecordFieldList" => "hidden,name,file,mid,description"
	),
	"feInterface" => $TCA["tx_nG6_sample"]["feInterface"],
	"columns" => Array (
		"hidden" => Array (		
			"exclude" => 1,
			"label" => "LLL:EXT:lang/locallang_general.xml:LGL.hidden",
			"config" => Array (
				"type" => "check",
				"default" => "0"
			)
		),
		"run_id" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_result.run_id",		
			"config" => Array (
				"type" => "input",
				"size" => "4",
				"max" => "4",
				"eval" => "int",
				"checkbox" => "0",
				"range" => Array (
					"upper" => "1000",
					"lower" => "10"
				),
				"default" => 0
			)
		),
		"mid" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_sample.mid",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",	
				"eval" => "trim",
			)
		),
		"description" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_sample.description",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",	
				"eval" => "trim",
			)
		),
	),
	"types" => Array (
		"0" => Array("showitem" => "hidden;;1;;1-1-1, mid, description")
	),
	"palettes" => Array (
		"1" => Array("showitem" => "")
	)
);



$TCA["tx_nG6_result"] = Array (
	"ctrl" => $TCA["tx_nG6_result"]["ctrl"],
	"interface" => Array (
		"showRecordFieldList" => "hidden,analyze_id,sample_id,rkey,rvalue,rgroup"
	),
	"feInterface" => $TCA["tx_nG6_result"]["feInterface"],
	"columns" => Array (
		"hidden" => Array (		
			"exclude" => 1,
			"label" => "LLL:EXT:lang/locallang_general.xml:LGL.hidden",
			"config" => Array (
				"type" => "check",
				"default" => "0"
			)
		),
		"analyze_id" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_result.analyze_id",		
			"config" => Array (
				"type" => "input",
				"size" => "4",
				"max" => "4",
				"eval" => "int",
				"checkbox" => "0",
				"range" => Array (
					"upper" => "1000",
					"lower" => "10"
				),
				"default" => 0
			)
		),
		"file" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_result.file",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",	
				"eval" => "trim",
			)
		),
		"rkey" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_result.rkey",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",	
				"eval" => "trim",
			)
		),
		"rvalue" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_result.rvalue",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",	
				"eval" => "trim",
			)
		),
		"rgroup" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_result.rgroup",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",	
				"eval" => "trim",
			)
		),
	),
	"types" => Array (
		"0" => Array("showitem" => "hidden;;1;;1-1-1, analyze_id, sample_id, rkey, rvalue, rgroup")
	),
	"palettes" => Array (
		"1" => Array("showitem" => "")
	)
);



$TCA["tx_nG6_project_run"] = Array (
	"ctrl" => $TCA["tx_nG6_project_run"]["ctrl"],
	"interface" => Array (
		"showRecordFieldList" => "hidden,project_id,run_id"
	),
	"feInterface" => $TCA["tx_nG6_project_run"]["feInterface"],
	"columns" => Array (
		"hidden" => Array (		
			"exclude" => 1,
			"label" => "LLL:EXT:lang/locallang_general.xml:LGL.hidden",
			"config" => Array (
				"type" => "check",
				"default" => "0"
			)
		),
		"project_id" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_project_run.project_id",		
			"config" => Array (
				"type" => "input",
				"size" => "4",
				"max" => "4",
				"eval" => "int",
				"checkbox" => "0",
				"range" => Array (
					"upper" => "1000",
					"lower" => "10"
				),
				"default" => 0
			)
		),
		"run_id" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_project_run.run_id",		
			"config" => Array (
				"type" => "input",
				"size" => "4",
				"max" => "4",
				"eval" => "int",
				"checkbox" => "0",
				"range" => Array (
					"upper" => "1000",
					"lower" => "10"
				),
				"default" => 0
			)
		),
	),
	"types" => Array (
		"0" => Array("showitem" => "hidden;;1;;1-1-1, project_id, run_id")
	),
	"palettes" => Array (
		"1" => Array("showitem" => "")
	)
);



$TCA["tx_nG6_project_analyze"] = Array (
	"ctrl" => $TCA["tx_nG6_project_analyze"]["ctrl"],
	"interface" => Array (
		"showRecordFieldList" => "hidden,project_id,analyze_id"
	),
	"feInterface" => $TCA["tx_nG6_project_analyze"]["feInterface"],
	"columns" => Array (
		"hidden" => Array (		
			"exclude" => 1,
			"label" => "LLL:EXT:lang/locallang_general.xml:LGL.hidden",
			"config" => Array (
				"type" => "check",
				"default" => "0"
			)
		),
		"project_id" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_project_analyze.project_id",		
			"config" => Array (
				"type" => "input",
				"size" => "4",
				"max" => "4",
				"eval" => "int",
				"checkbox" => "0",
				"range" => Array (
					"upper" => "1000",
					"lower" => "10"
				),
				"default" => 0
			)
		),
		"analyze_id" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_project_analyze.analyze_id",		
			"config" => Array (
				"type" => "input",
				"size" => "4",
				"max" => "4",
				"eval" => "int",
				"checkbox" => "0",
				"range" => Array (
					"upper" => "1000",
					"lower" => "10"
				),
				"default" => 0
			)
		),
	),
	"types" => Array (
		"0" => Array("showitem" => "hidden;;1;;1-1-1, project_id, analyze_id")
	),
	"palettes" => Array (
		"1" => Array("showitem" => "")
	)
);



$TCA["tx_nG6_run_analyze"] = Array (
	"ctrl" => $TCA["tx_nG6_run_analyze"]["ctrl"],
	"interface" => Array (
		"showRecordFieldList" => "hidden,run_id,analyze_id"
	),
	"feInterface" => $TCA["tx_nG6_run_analyze"]["feInterface"],
	"columns" => Array (
		"hidden" => Array (		
			"exclude" => 1,
			"label" => "LLL:EXT:lang/locallang_general.xml:LGL.hidden",
			"config" => Array (
				"type" => "check",
				"default" => "0"
			)
		),
		"run_id" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_run_analyze.run_id",		
			"config" => Array (
				"type" => "input",
				"size" => "4",
				"max" => "4",
				"eval" => "int",
				"checkbox" => "0",
				"range" => Array (
					"upper" => "1000",
					"lower" => "10"
				),
				"default" => 0
			)
		),
		"analyze_id" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:nG6/locallang_db.xml:tx_nG6_run_analyze.analyze_id",		
			"config" => Array (
				"type" => "input",
				"size" => "4",
				"max" => "4",
				"eval" => "int",
				"checkbox" => "0",
				"range" => Array (
					"upper" => "1000",
					"lower" => "10"
				),
				"default" => 0
			)
		),
	),
	"types" => Array (
		"0" => Array("showitem" => "hidden;;1;;1-1-1, run_id, analyze_id")
	),
	"palettes" => Array (
		"1" => Array("showitem" => "")
	)
);
?>
