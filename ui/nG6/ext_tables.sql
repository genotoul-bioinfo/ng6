
/*
 * Table structure for table 'fe_rights_levels'
*/
CREATE TABLE fe_rights_levels (
    uid int(11) NOT NULL AUTO_INCREMENT,
    pid int(11) NOT NULL default 0,
    right_level_id int(11) NOT NULL,
    right_level_label varchar(20) NOT NULL,
    PRIMARY KEY (right_level_id),
);


/*
 * Table structure for table 'fe_rights'
*/
CREATE TABLE fe_rights (
    uid int(11) NOT NULL AUTO_INCREMENT,
    pid int(11) NOT NULL default 0,
    fe_user_id int(11) NOT NULL,
    project_id int(11) NOT NULL,
    right_id int(11) NOT NULL,
    PRIMARY KEY (uid),UNIQUE KEY `uniq` (`fe_user_id`,`project_id`,`right_id`),KEY `parent` (`pid`)
)  ;

/*
 * Table structure for table 'tx_nG6_project'
*/
CREATE TABLE tx_nG6_project (
    uid int(11) NOT NULL auto_increment,
    pid int(11) DEFAULT '0' NOT NULL,
    tstamp int(11) DEFAULT '0' NOT NULL,
    crdate int(11) DEFAULT '0' NOT NULL,
    cruser_id int(11) DEFAULT '0' NOT NULL,
    deleted tinyint(4) DEFAULT '0' NOT NULL,
    hidden tinyint(4) DEFAULT '0' NOT NULL,
    public tinyint(4) DEFAULT '1' NOT NULL,
    name varchar(255) DEFAULT '' NOT NULL,
    description varchar(255) DEFAULT '' NOT NULL,
    
    PRIMARY KEY (uid),
    KEY parent (pid)
)  ;



/*
 * Table structure for table 'tx_nG6_run'
*/
CREATE TABLE tx_nG6_run (
    uid int(11) NOT NULL auto_increment,
    pid int(11) DEFAULT '0' NOT NULL,
    tstamp int(11) DEFAULT '0' NOT NULL,
    crdate int(11) DEFAULT '0' NOT NULL,
    cruser_id int(11) DEFAULT '0' NOT NULL,
    deleted tinyint(4) DEFAULT '0' NOT NULL,
    hidden tinyint(4) DEFAULT '0' NOT NULL,
    name varchar(255) DEFAULT '' NOT NULL,
    date int(11) DEFAULT '0' NOT NULL,
    directory varchar(255) DEFAULT '' NOT NULL,
    species varchar(255) DEFAULT '' NOT NULL,
    data_nature varchar(255) DEFAULT '' NOT NULL,
    type varchar(255) DEFAULT '' NOT NULL,
    nb_sequences varchar(255) DEFAULT '0' NOT NULL,
    full_seq_size varchar(255) DEFAULT '0' NOT NULL,
    description text DEFAULT '' NOT NULL,
    sequencer varchar(255) DEFAULT '' NOT NULL,
    
    PRIMARY KEY (uid),
    KEY parent (pid)
)  ;


/*
 * Table structure for table 'tx_nG6_analyze'
*/

CREATE TABLE tx_nG6_analyze (
    uid int(11) NOT NULL auto_increment,
    pid int(11) DEFAULT '0' NOT NULL,
    tstamp int(11) DEFAULT '0' NOT NULL,
    crdate int(11) DEFAULT '0' NOT NULL,
    cruser_id int(11) DEFAULT '0' NOT NULL,
    deleted tinyint(4) DEFAULT '0' NOT NULL,
    hidden tinyint(4) DEFAULT '0' NOT NULL,
    parent_uid int(11) DEFAULT '0' NOT NULL,
    class varchar(255) DEFAULT '' NOT NULL,
    name varchar(255) DEFAULT '' NOT NULL,
    description varchar(255) DEFAULT '' NOT NULL,
    date int(11) DEFAULT '0' NOT NULL,
    directory varchar(255) DEFAULT '' NOT NULL,
    software varchar(255) DEFAULT '' NOT NULL,
    version varchar(255) DEFAULT '' NOT NULL,
    params text NOT NULL,
    
    PRIMARY KEY (uid),
    KEY parent (pid)
)  ;



/*
 * Table structure for table 'tx_nG6_sample'
*/

CREATE TABLE tx_nG6_sample (
    uid int(11) NOT NULL auto_increment,
    pid int(11) DEFAULT '0' NOT NULL,
    tstamp int(11) DEFAULT '0' NOT NULL,
    crdate int(11) DEFAULT '0' NOT NULL,
    cruser_id int(11) DEFAULT '0' NOT NULL,
    deleted tinyint(4) DEFAULT '0' NOT NULL,
    hidden tinyint(4) DEFAULT '0' NOT NULL,
    run_id int(11) DEFAULT '0' NOT NULL,
    mid varchar(255) DEFAULT '' NOT NULL,
    description varchar(255) DEFAULT '' NOT NULL,
    
    PRIMARY KEY (uid),
    KEY parent (pid)
)  ;


/*
 * Table structure for table 'tx_nG6_result'
*/

CREATE TABLE tx_nG6_result (
    uid int(11) NOT NULL auto_increment,
    pid int(11) DEFAULT '0' NOT NULL,
    tstamp int(11) DEFAULT '0' NOT NULL,
    crdate int(11) DEFAULT '0' NOT NULL,
    cruser_id int(11) DEFAULT '0' NOT NULL,
    deleted tinyint(4) DEFAULT '0' NOT NULL,
    hidden tinyint(4) DEFAULT '0' NOT NULL,
    analyze_id int(11) DEFAULT '0' NOT NULL,
    file varchar(255) DEFAULT '' NOT NULL,
    rkey varchar(255) DEFAULT '' NOT NULL,
    rvalue text DEFAULT '' NOT NULL,
    rgroup varchar(255) DEFAULT '' NOT NULL,
    
    PRIMARY KEY (uid),
    KEY parent (pid)
)  ;


/*
 * Table structure for table 'tx_nG6_project_run'
*/

CREATE TABLE tx_nG6_project_run (
    uid int(11) NOT NULL auto_increment,
    pid int(11) DEFAULT '0' NOT NULL,
    tstamp int(11) DEFAULT '0' NOT NULL,
    crdate int(11) DEFAULT '0' NOT NULL,
    cruser_id int(11) DEFAULT '0' NOT NULL,
    deleted tinyint(4) DEFAULT '0' NOT NULL,
    hidden tinyint(4) DEFAULT '0' NOT NULL,
    project_id int(11) DEFAULT '0' NOT NULL,
    run_id int(11) DEFAULT '0' NOT NULL,
    
    PRIMARY KEY (uid),
    KEY parent (pid)
)  ;


/*
 * Table structure for table 'tx_nG6_project_analyze'
*/

CREATE TABLE tx_nG6_project_analyze (
    uid int(11) NOT NULL auto_increment,
    pid int(11) DEFAULT '0' NOT NULL,
    tstamp int(11) DEFAULT '0' NOT NULL,
    crdate int(11) DEFAULT '0' NOT NULL,
    cruser_id int(11) DEFAULT '0' NOT NULL,
    deleted tinyint(4) DEFAULT '0' NOT NULL,
    hidden tinyint(4) DEFAULT '0' NOT NULL,
    project_id int(11) DEFAULT '0' NOT NULL,
    analyze_id int(11) DEFAULT '0' NOT NULL,
    
    PRIMARY KEY (uid),
    KEY parent (pid)
)  ;


/*
 * Table structure for table 'tx_nG6_run_analyze'
*/

CREATE TABLE tx_nG6_run_analyze (
    uid int(11) NOT NULL auto_increment,
    pid int(11) DEFAULT '0' NOT NULL,
    tstamp int(11) DEFAULT '0' NOT NULL,
    crdate int(11) DEFAULT '0' NOT NULL,
    cruser_id int(11) DEFAULT '0' NOT NULL,
    deleted tinyint(4) DEFAULT '0' NOT NULL,
    hidden tinyint(4) DEFAULT '0' NOT NULL,
    run_id int(11) DEFAULT '0' NOT NULL,
    analyze_id int(11) DEFAULT '0' NOT NULL,
    
    PRIMARY KEY (uid),
    KEY parent (pid)
)  ;
