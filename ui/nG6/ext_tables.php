<?php
use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
if (!defined ('TYPO3')) 	die ('Access denied.');

$TCA["fe_rights_levels"] = Array(
	"ctrl" => Array (
		'title' => 'LLL:EXT:nG6/locallang_db.xml:fe_rights_levels',
		'label' => 'right_level_id',
		"default_sortby" => "ORDER BY right_level_id",

		"dynamicConfigFile" => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6')."tca.php"
	),
	"feInterface" => Array (
		"fe_admin_fieldList" => "right_level_id, right_level_label",
	)
);

$TCA["fe_rights"] = Array(
	"ctrl" => Array (
		'title' => 'LLL:EXT:nG6/locallang_db.xml:fe_rights',
		'label' => 'right_id, project_id, fe_user_id',
		"default_sortby" => "ORDER BY right_id",

		"dynamicConfigFile" => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6')."tca.php"
	),
	"feInterface" => Array (
		"fe_admin_fieldList" => "fe_user_id, right_id, project_id",
	)
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages("tx_nG6_project");




\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages("tx_nG6_run");

$TCA["tx_nG6_run"] = Array (
	"ctrl" => Array (
		'title' => 'LLL:EXT:nG6/locallang_db.xml:tx_nG6_run',		
		'label' => 'uid',	
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		"default_sortby" => "ORDER BY crdate",	
		"delete" => "deleted",	
		"enablecolumns" => Array (		
			"disabled" => "hidden",
		),
	    "dynamicConfigFile" => \TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('nG6'))."tca.php",
	    "iconfile" => \TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('nG6'))."icon_tx_nG6_run.gif",
	),
	"feInterface" => Array (
		"fe_admin_fieldList" => "hidden, name, date, directory, species, data_nature, type, nb_sequences, full_seq_size, description, sequencer",
	)
);


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages("tx_nG6_analyze");

$TCA["tx_nG6_analyze"] = Array (
	"ctrl" => Array (
		'title' => 'LLL:EXT:nG6/locallang_db.xml:tx_nG6_analyze',		
		'label' => 'uid',	
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		"default_sortby" => "ORDER BY crdate",	
		"delete" => "deleted",	
		"enablecolumns" => Array (		
			"disabled" => "hidden",
		),
	    "dynamicConfigFile" => \TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('nG6'))."tca.php",
	    "iconfile" => \TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('nG6'))."icon_tx_nG6_analyze.gif",
	),
	"feInterface" => Array (
		"fe_admin_fieldList" => "hidden, parent_uid, class, name, description, date, directory, software, version, params",
	)
);


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages("tx_nG6_sample");

$TCA["tx_nG6_sample"] = Array (
	"ctrl" => Array (
		'title' => 'LLL:EXT:nG6/locallang_db.xml:tx_nG6_sample',		
		'label' => 'uid',	
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		"default_sortby" => "ORDER BY crdate",	
		"delete" => "deleted",	
		"enablecolumns" => Array (		
			"disabled" => "hidden",
		),
	    "dynamicConfigFile" => \TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('nG6'))."tca.php",
	        "iconfile" => \TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('nG6'))."icon_tx_nG6_sample.gif",
	),
	"feInterface" => Array (
		"fe_admin_fieldList" => "hidden, run_id, mid, description",
	)
);


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages("tx_nG6_result");

$TCA["tx_nG6_result"] = Array (
	"ctrl" => Array (
		'title' => 'LLL:EXT:nG6/locallang_db.xml:tx_nG6_result',		
		'label' => 'uid',	
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		"default_sortby" => "ORDER BY crdate",	
		"delete" => "deleted",	
		"enablecolumns" => Array (		
			"disabled" => "hidden",
		),
	    "dynamicConfigFile" => \TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('nG6'))."tca.php",
    "iconfile" => \TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('nG6'))."icon_tx_nG6_result.gif",
	),
	"feInterface" => Array (
		"fe_admin_fieldList" => "analyze_id, file, rkey, rvalue, rgroup",
	)
);


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages("tx_nG6_project_run");

$TCA["tx_nG6_project_run"] = Array (
	"ctrl" => Array (
		'title' => 'LLL:EXT:nG6/locallang_db.xml:tx_nG6_project_run',		
		'label' => 'uid',	
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		"default_sortby" => "ORDER BY crdate",	
		"delete" => "deleted",	
		"enablecolumns" => Array (		
			"disabled" => "hidden",
		),
	    "dynamicConfigFile" => \TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('nG6'))."tca.php",
    "iconfile" => \TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('nG6'))."icon_tx_nG6_project_run.gif",
	),
	"feInterface" => Array (
		"fe_admin_fieldList" => "hidden, project_id, run_id",
	)
);


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages("tx_nG6_project_analyze");

$TCA["tx_nG6_project_analyze"] = Array (
	"ctrl" => Array (
		'title' => 'LLL:EXT:nG6/locallang_db.xml:tx_nG6_project_analyze',		
		'label' => 'uid',	
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		"default_sortby" => "ORDER BY crdate",	
		"delete" => "deleted",	
		"enablecolumns" => Array (		
			"disabled" => "hidden",
		),
	    "dynamicConfigFile" => \TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('nG6'))."tca.php",
	        "iconfile" => \TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('nG6'))."icon_tx_nG6_project_analyze.gif",
	),
	"feInterface" => Array (
		"fe_admin_fieldList" => "hidden, project_id, analyze_id",
	)
);


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages("tx_nG6_run_analyze");

$TCA["tx_nG6_run_analyze"] = Array (
	"ctrl" => Array (
		'title' => 'LLL:EXT:nG6/locallang_db.xml:tx_nG6_run_analyze',		
		'label' => 'uid',	
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		"default_sortby" => "ORDER BY crdate",	
		"delete" => "deleted",	
		"enablecolumns" => Array (		
			"disabled" => "hidden",
		),
	    "dynamicConfigFile" => \TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('nG6'))."tca.php",
    "iconfile" => \TYPO3\CMS\Core\Utility\PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('nG6'))."icon_tx_nG6_run_analyze.gif",
	),
	"feInterface" => Array (
		"fe_admin_fieldList" => "hidden, run_id, analyze_id",
	)
);



//\TYPO3\CMS\Core\Utility\GeneralUtility::loadTCA('tt_content');
$TCA['tt_content']['types']['list']['subtypes_excludelist']['nG6'.'_pi1']='layout,select_key';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(Array('LLL:EXT:nG6/locallang_db.xml:tt_content.list_type_pi1', 'nG6'.'_pi1'),'list_type', 'nG6');
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms']['db_new_content_el']['wizardItemsHook']["tx_nG6_pi1_wizicon"] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'pi1/class.tx_nG6_pi1_wizicon.php';

//\TYPO3\CMS\Core\Utility\GeneralUtility::loadTCA('tt_content');
$TCA['tt_content']['types']['list']['subtypes_excludelist']['nG6'.'_pi2']='layout,select_key';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(Array('LLL:EXT:nG6/locallang_db.xml:tt_content.list_type_pi2', 'nG6'.'_pi2'),'list_type', 'nG6');
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms']['db_new_content_el']['wizardItemsHook']["tx_nG6_pi2_wizicon"] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'pi2/class.tx_nG6_pi2_wizicon.php';

//\TYPO3\CMS\Core\Utility\GeneralUtility::loadTCA('tt_content');
$TCA['tt_content']['types']['list']['subtypes_excludelist']['nG6'.'_pi3']='layout,select_key';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(Array('LLL:EXT:nG6/locallang_db.xml:tt_content.list_type_pi3', 'nG6'.'_pi3'),'list_type', 'nG6');
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms']['db_new_content_el']['wizardItemsHook']["tx_nG6_pi3_wizicon"] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'pi3/class.tx_nG6_pi3_wizicon.php';

//\TYPO3\CMS\Core\Utility\GeneralUtility::loadTCA('tt_content');
$TCA['tt_content']['types']['list']['subtypes_excludelist']['nG6'.'_pi4']='layout,select_key';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(Array('LLL:EXT:nG6/locallang_db.xml:tt_content.list_type_pi4', 'nG6'.'_pi4'),'list_type', 'nG6');
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms']['db_new_content_el']['wizardItemsHook']["tx_nG6_pi4_wizicon"] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'pi4/class.tx_nG6_pi4_wizicon.php';

//\TYPO3\CMS\Core\Utility\GeneralUtility::loadTCA('tt_content');
$TCA['tt_content']['types']['list']['subtypes_excludelist']['nG6'.'_pi5']='layout,select_key';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(Array('LLL:EXT:nG6/locallang_db.xml:tt_content.list_type_pi5', 'nG6'.'_pi5'),'list_type', 'nG6');
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms']['db_new_content_el']['wizardItemsHook']["tx_nG6_pi5_wizicon"] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'pi5/class.tx_nG6_pi5_wizicon.php';

//\TYPO3\CMS\Core\Utility\GeneralUtility::loadTCA('tt_content');
$TCA['tt_content']['types']['list']['subtypes_excludelist']['nG6'.'_pi6']='layout,select_key';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(Array('LLL:EXT:nG6/locallang_db.xml:tt_content.list_type_pi6', 'nG6'.'_pi6'),'list_type', 'nG6');
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms']['db_new_content_el']['wizardItemsHook']["tx_nG6_pi6_wizicon"] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'pi6/class.tx_nG6_pi6_wizicon.php';
?>
