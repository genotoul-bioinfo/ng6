<?php
if (!defined ('TYPO3')) 	die ('Access denied.');

  ## Chmod the smarty cache and template_c directories
exec('chmod 777 '.\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'res/smarty/cache');
exec('chmod 777 '.\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nG6').'res/smarty/templates_c');

  ## Extending TypoScript from static template uid=43 to set up userdefined tag:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript('nG6','setup','
	tt_content.CSS_editor.ch.tx_nG6_pi1 = < plugin.tx_nG6_pi1.CSS_editor
',43);


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPItoST43('nG6','pi1/class.tx_nG6_pi1.php','_pi1','list_type',1);

  ## Extending TypoScript from static template uid=43 to set up userdefined tag:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript('nG6','setup','
	tt_content.CSS_editor.ch.tx_nG6_pi2 = < plugin.tx_nG6_pi2.CSS_editor
',43);


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPItoST43('nG6','pi2/class.tx_nG6_pi2.php','_pi2','list_type',1);

  ## Extending TypoScript from static template uid=43 to set up userdefined tag:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript('nG6','setup','
	tt_content.CSS_editor.ch.tx_nG6_pi3 = < plugin.tx_nG6_pi3.CSS_editor
',43);


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPItoST43('nG6','pi3/class.tx_nG6_pi3.php','_pi3','list_type',1);

  ## Extending TypoScript from static template uid=43 to set up userdefined tag:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript('nG6','setup','
	tt_content.CSS_editor.ch.tx_nG6_pi4 = < plugin.tx_nG6_pi4.CSS_editor
',43);


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPItoST43('nG6','pi4/class.tx_nG6_pi4.php','_pi4','list_type',1);

  ## Extending TypoScript from static template uid=43 to set up userdefined tag:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript('nG6','setup','
	tt_content.CSS_editor.ch.tx_nG6_pi5 = < plugin.tx_nG6_pi5.CSS_editor
',43);


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPItoST43('nG6','pi5/class.tx_nG6_pi5.php','_pi5','list_type',1);

  ## Extending TypoScript from static template uid=43 to set up userdefined tag:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript('nG6','setup','
	tt_content.CSS_editor.ch.tx_nG6_pi6 = < plugin.tx_nG6_pi6.CSS_editor
',43);


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPItoST43('nG6','pi6/class.tx_nG6_pi6.php','_pi6','list_type',1);


  ## Added to do some ajax
$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['tx_nG62'] = \ng6\Controller\tx_nG6_eid::class. '::processRequest';
$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['tx_nG6'] =\ng6\Controller\NG6::class . '::processRequest';


?>
