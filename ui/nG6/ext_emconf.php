<?php

########################################################################
# Extension Manager/Repository config file for ext: "nG6"
#
# Auto generated 23-06-2009 10:33
#
# Manual updates:
# Only the data in the array - anything else is removed by next write.
# "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF['nG6'] = array(
	'title' => 'nG6',
	'description' => 'Next Generation Sequencing Information System',
	'category' => 'plugin',
	'author' => 'PF bioinformatique de Toulouse',
	'author_email' => '',
	'shy' => '',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 1,
	'lockType' => '',
	'author_company' => '',
	'version' => '1.1.0',
	'constraints' => array(
		'depends' => array('typo3db_legacy' => '1.0.0-1.8.99',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:24:{s:9:"ChangeLog";s:4:"f9b7";s:10:"README.txt";s:4:"9fa9";s:12:"ext_icon.gif";s:4:"1bdc";s:31:"class.tx_nG6_eid.php";s:4:"bb99";s:17:"ext_localconf.php";s:4:"7efc";s:14:"ext_tables.php";s:4:"e84d";s:14:"ext_tables.sql";s:4:"1b60";s:23:"icon_tx_nG6_analyze.gif";s:4:"475a";s:23:"icon_tx_nG6_project.gif";s:4:"475a";s:31:"icon_tx_nG6_project_analyze.gif";s:4:"475a";s:27:"icon_tx_nG6_project_run.gif";s:4:"475a";s:19:"icon_tx_nG6_run.gif";s:4:"475a";s:27:"icon_tx_nG6_run_analyze.gif";s:4:"475a";s:13:"locallang.xml";s:4:"a0c8";s:16:"locallang_db.xml";s:4:"2862";s:7:"tca.php";s:4:"4639";s:19:"doc/wizard_form.dat";s:4:"110b";s:20:"doc/wizard_form.html";s:4:"44cc";s:14:"pi1/ce_wiz.gif";s:4:"02b6";s:24:"pi1/class.tx_nG6_pi1.php";s:4:"e726";s:32:"pi1/class.tx_nG6_pi1_wizicon.php";s:4:"b2ac";s:13:"pi1/clear.gif";s:4:"cc11";s:17:"pi1/locallang.xml";s:4:"3bb2";s:24:"pi1/static/editorcfg.txt";s:4:"4730";s:20:"pi1/static/setup.txt";s:4:"7bca";s:14:"pi2/ce_wiz.gif";s:4:"02b6";s:24:"pi2/class.tx_nG6_pi2.php";s:4:"7164";s:32:"pi2/class.tx_nG6_pi2_wizicon.php";s:4:"ff00";s:13:"pi2/clear.gif";s:4:"cc11";s:17:"pi2/locallang.xml";s:4:"9216";s:24:"pi2/static/editorcfg.txt";s:4:"e404";s:14:"pi3/ce_wiz.gif";s:4:"02b6";s:24:"pi3/class.tx_nG6_pi3.php";s:4:"7164";s:32:"pi3/class.tx_nG6_pi3_wizicon.php";s:4:"ff00";s:13:"pi3/clear.gif";s:4:"cc11";s:17:"pi3/locallang.xml";s:4:"9216";s:24:"pi3/static/editorcfg.txt";s:4:"e404";s:14:"pi4/ce_wiz.gif";s:4:"02b6";s:24:"pi4/class.tx_nG6_pi4.php";s:4:"7164";s:32:"pi4/class.tx_nG6_pi4_wizicon.php";s:4:"ff00";s:13:"pi4/clear.gif";s:4:"cc11";s:17:"pi4/locallang.xml";s:4:"9216";s:24:"pi4/static/editorcfg.txt";s:4:"e404";s:14:"pi5/ce_wiz.gif";s:4:"02b6";s:24:"pi5/class.tx_nG6_pi5.php";s:4:"7164";s:32:"pi5/class.tx_nG6_pi5_wizicon.php";s:4:"ff00";s:13:"pi5/clear.gif";s:4:"cc11";s:17:"pi5/locallang.xml";s:4:"9216";s:24:"pi5/static/editorcfg.txt";s:4:"e404";s:14:"pi6/ce_wiz.gif";s:4:"02b6";s:24:"pi6/class.tx_nG6_pi6.php";s:4:"7164";s:32:"pi6/class.tx_nG6_pi6_wizicon.php";s:4:"ff00";s:13:"pi6/clear.gif";s:4:"cc11";s:17:"pi6/locallang.xml";s:4:"9216";s:24:"pi6/static/editorcfg.txt";s:4:"e404";}',
);

?>