<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2009 PF bioinformatique de Toulouse <>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'nG6' for the 'nG6' extension.
 *
 * @author	PF bioinformatique de Toulouse <>
 */


class tx_nG6_pi2 extends \TYPO3\CMS\Frontend\Plugin\AbstractPlugin {
	var $prefixId = 'tx_nG6_pi2';		// Same as class name
	var $scriptRelPath = 'pi2/class.tx_nG6_pi2.php';	// Path to this script relative to the extension dir.
	var $extKey = 'nG6';	// The extension key.
	//var $pi_checkCHash = TRUE;
	
	/**
	 * Main method of your PlugIn
	 *
	 * @param	string		$content: The content of the PlugIn
	 * @param	array		$conf: The PlugIn Configuration
	 * @return	The content that should be displayed on the website
	 */
	function main($content,$conf)	{
		$this->conf=$conf;		// Setting the TypoScript passed to this function in $this->conf
		$this->pi_setPiVarDefaults();
		$this->pi_loadLL();		// Loading the LOCAL_LANG values
		//$this->pi_USER_INT_obj=1;	// Configuring so caching is not expected. This value means that no cHash params are ever set. We do this, because it's a USER_INT object!
		$content = '<form '.$this->pi_classParam('search-box').' action="/ng6/index.php?id='.$conf['result_page_id'].'&amp;tx_nG6_pi3" method="post">
      				  <div>
		  			    <input '.$this->pi_classParam('text-search').' name="text-search" value="search" onblur="if(value==\'\') this.value=\'search\';" onfocus="if(value==\'search\') this.value=\'\';" type="text" />
		  			    <input '.$this->pi_classParam('submit-search').' value="OK" type="submit" />
				      </div>
     			    </form>';
		return $this->pi_wrapInBaseClass($content);
	}	
}



if (defined('TYPO3') && $TYPO3_CONF_VARS[TYPO3]['XCLASS']['ext/nG6/pi2/class.tx_nG6_pi2.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3]['XCLASS']['ext/nG6/pi2/class.tx_nG6_pi2.php']);
}

?>
