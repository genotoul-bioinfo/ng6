SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE IF NOT EXISTS `backend_layout` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `t3ver_oid` int(11) NOT NULL DEFAULT '0',
  `t3ver_id` int(11) NOT NULL DEFAULT '0',
  `t3ver_wsid` int(11) NOT NULL DEFAULT '0',
  `t3ver_label` varchar(255) NOT NULL DEFAULT '',
  `t3ver_state` tinyint(4) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(11) NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(11) NOT NULL DEFAULT '0',
  `t3ver_move_id` int(11) NOT NULL DEFAULT '0',
  `t3_origuid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `hidden` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `sorting` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `config` text NOT NULL,
  `icon` text NOT NULL,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `be_groups` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(50) NOT NULL DEFAULT '',
  `non_exclude_fields` text,
  `explicit_allowdeny` text,
  `allowed_languages` varchar(255) NOT NULL DEFAULT '',
  `custom_options` text,
  `db_mountpoints` varchar(255) NOT NULL DEFAULT '',
  `pagetypes_select` varchar(255) NOT NULL DEFAULT '',
  `tables_select` text,
  `tables_modify` text,
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `groupMods` text,
  `file_mountpoints` varchar(255) NOT NULL DEFAULT '',
  `fileoper_perms` tinyint(4) NOT NULL DEFAULT '0',
  `hidden` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `inc_access_lists` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `description` text,
  `lockToDomain` varchar(50) NOT NULL DEFAULT '',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `TSconfig` text,
  `subgroup` varchar(255) NOT NULL DEFAULT '',
  `hide_in_lists` tinyint(4) NOT NULL DEFAULT '0',
  `workspace_perms` tinyint(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `be_sessions` (
  `ses_id` varchar(32) NOT NULL DEFAULT '',
  `ses_name` varchar(32) NOT NULL DEFAULT '',
  `ses_iplock` varchar(39) NOT NULL DEFAULT '',
  `ses_hashlock` int(11) NOT NULL DEFAULT '0',
  `ses_userid` int(11) unsigned NOT NULL DEFAULT '0',
  `ses_tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `ses_data` longtext,
  `ses_backuserid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ses_id`,`ses_name`),
  KEY `ses_tstamp` (`ses_tstamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `be_users` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `admin` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `usergroup` varchar(255) NOT NULL DEFAULT '',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `starttime` int(11) unsigned NOT NULL DEFAULT '0',
  `endtime` int(11) unsigned NOT NULL DEFAULT '0',
  `lang` char(2) NOT NULL DEFAULT '',
  `email` varchar(80) NOT NULL DEFAULT '',
  `db_mountpoints` varchar(255) NOT NULL DEFAULT '',
  `options` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `realName` varchar(80) NOT NULL DEFAULT '',
  `userMods` varchar(255) NOT NULL DEFAULT '',
  `allowed_languages` varchar(255) NOT NULL DEFAULT '',
  `uc` mediumtext,
  `file_mountpoints` varchar(255) NOT NULL DEFAULT '',
  `fileoper_perms` tinyint(4) NOT NULL DEFAULT '0',
  `workspace_perms` tinyint(3) NOT NULL DEFAULT '1',
  `lockToDomain` varchar(50) NOT NULL DEFAULT '',
  `disableIPlock` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `TSconfig` text,
  `lastlogin` int(10) unsigned NOT NULL DEFAULT '0',
  `createdByAction` int(11) NOT NULL DEFAULT '0',
  `usergroup_cached_list` varchar(255) NOT NULL DEFAULT '',
  `workspace_id` int(11) NOT NULL DEFAULT '0',
  `workspace_preview` tinyint(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`),
  KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;


CREATE TABLE IF NOT EXISTS `cache_extensions` (
  `extkey` varchar(60) NOT NULL DEFAULT '',
  `repository` int(11) unsigned NOT NULL DEFAULT '1',
  `version` varchar(10) NOT NULL DEFAULT '',
  `alldownloadcounter` int(11) unsigned NOT NULL DEFAULT '0',
  `downloadcounter` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL DEFAULT '',
  `description` mediumtext,
  `state` int(4) NOT NULL DEFAULT '0',
  `reviewstate` int(4) NOT NULL DEFAULT '0',
  `category` int(4) NOT NULL DEFAULT '0',
  `lastuploaddate` int(11) unsigned NOT NULL DEFAULT '0',
  `dependencies` mediumtext,
  `authorname` varchar(100) NOT NULL DEFAULT '',
  `authoremail` varchar(100) NOT NULL DEFAULT '',
  `ownerusername` varchar(50) NOT NULL DEFAULT '',
  `t3xfilemd5` varchar(35) NOT NULL DEFAULT '',
  `uploadcomment` mediumtext,
  `authorcompany` varchar(100) NOT NULL DEFAULT '',
  `intversion` int(11) NOT NULL DEFAULT '0',
  `lastversion` int(3) NOT NULL DEFAULT '0',
  `lastreviewedversion` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`extkey`,`version`,`repository`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `cache_imagesizes` (
  `md5hash` varchar(32) NOT NULL DEFAULT '',
  `md5filename` varchar(32) NOT NULL DEFAULT '',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `filename` varchar(255) NOT NULL DEFAULT '',
  `imagewidth` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `imageheight` mediumint(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`md5filename`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `cache_md5params` (
  `md5hash` varchar(20) NOT NULL DEFAULT '',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(3) NOT NULL DEFAULT '0',
  `params` text,
  PRIMARY KEY (`md5hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `cache_treelist` (
  `md5hash` char(32) NOT NULL DEFAULT '',
  `pid` int(11) NOT NULL DEFAULT '0',
  `treelist` text,
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `expires` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`md5hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `cache_typo3temp_log` (
  `md5hash` varchar(32) NOT NULL DEFAULT '',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `filename` varchar(255) NOT NULL DEFAULT '',
  `orig_filename` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`md5hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `cf_cache_hash` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `expires` int(11) unsigned NOT NULL DEFAULT '0',
  `content` mediumblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`,`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `cf_cache_hash_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `tag` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`),
  KEY `cache_tag` (`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `cf_cache_pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `expires` int(11) unsigned NOT NULL DEFAULT '0',
  `content` mediumblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`,`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `cf_cache_pagesection` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `expires` int(11) unsigned NOT NULL DEFAULT '0',
  `content` mediumblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`,`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `cf_cache_pagesection_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `tag` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`),
  KEY `cache_tag` (`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `cf_cache_pages_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `tag` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`),
  KEY `cache_tag` (`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `cf_extbase_object` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `expires` int(11) unsigned NOT NULL DEFAULT '0',
  `content` mediumblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`,`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `cf_extbase_object_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `tag` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`),
  KEY `cache_tag` (`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `cf_extbase_reflection` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `expires` int(11) unsigned NOT NULL DEFAULT '0',
  `content` mediumblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`,`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `cf_extbase_reflection_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) NOT NULL DEFAULT '',
  `tag` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`),
  KEY `cache_tag` (`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `fe_groups` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(50) NOT NULL DEFAULT '',
  `tx_nG6_organism` varchar(50) NOT NULL DEFAULT '',
  `tx_nG6_location` varchar(50) NOT NULL DEFAULT '',
  `hidden` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `lockToDomain` varchar(50) NOT NULL DEFAULT '',
  `deleted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `description` text,
  `subgroup` tinytext,
  `TSconfig` text,
  `tx_extbase_type` varchar(255) NOT NULL DEFAULT '',
  `felogin_redirectPid` tinytext,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;


CREATE TABLE IF NOT EXISTS `fe_rights` (
  `fe_user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `right_id` int(11) NOT NULL,
  PRIMARY KEY (`fe_user_id`,`project_id`,`right_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `fe_rights_levels` (
  `right_level_id` int(11) NOT NULL,
  `right_level_label` varchar(20) NOT NULL,
  PRIMARY KEY (`right_level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `fe_sessions` (
  `ses_id` varchar(32) NOT NULL DEFAULT '',
  `ses_name` varchar(32) NOT NULL DEFAULT '',
  `ses_iplock` varchar(39) NOT NULL DEFAULT '',
  `ses_hashlock` int(11) NOT NULL DEFAULT '0',
  `ses_userid` int(11) unsigned NOT NULL DEFAULT '0',
  `ses_tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `ses_data` blob,
  `ses_permanent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ses_id`,`ses_name`),
  KEY `ses_tstamp` (`ses_tstamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `fe_session_data` (
  `hash` varchar(32) NOT NULL DEFAULT '',
  `content` mediumblob,
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`hash`),
  KEY `tstamp` (`tstamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `fe_users` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `usergroup` tinytext,
  `disable` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `starttime` int(11) unsigned NOT NULL DEFAULT '0',
  `endtime` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(80) NOT NULL DEFAULT '',
  `first_name` varchar(50) NOT NULL DEFAULT '',
  `middle_name` varchar(50) NOT NULL DEFAULT '',
  `last_name` varchar(50) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  `telephone` varchar(20) NOT NULL DEFAULT '',
  `fax` varchar(20) NOT NULL DEFAULT '',
  `email` varchar(80) NOT NULL DEFAULT '',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `lockToDomain` varchar(50) NOT NULL DEFAULT '',
  `deleted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `uc` blob,
  `title` varchar(40) NOT NULL DEFAULT '',
  `zip` varchar(10) NOT NULL DEFAULT '',
  `city` varchar(50) NOT NULL DEFAULT '',
  `country` varchar(40) NOT NULL DEFAULT '',
  `www` varchar(80) NOT NULL DEFAULT '',
  `company` varchar(80) NOT NULL DEFAULT '',
  `image` tinytext,
  `TSconfig` text,
  `fe_cruser_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastlogin` int(10) unsigned NOT NULL DEFAULT '0',
  `is_online` int(10) unsigned NOT NULL DEFAULT '0',
  `tx_extbase_type` varchar(255) NOT NULL DEFAULT '',
  `felogin_redirectPid` tinytext,
  `felogin_forgotHash` varchar(80) DEFAULT '',
  `tx_nG6_updatable_password` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`username`),
  KEY `username` (`username`),
  KEY `is_online` (`is_online`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;


CREATE TABLE IF NOT EXISTS `pages` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `t3ver_oid` int(11) NOT NULL DEFAULT '0',
  `t3ver_id` int(11) NOT NULL DEFAULT '0',
  `t3ver_wsid` int(11) NOT NULL DEFAULT '0',
  `t3ver_label` varchar(255) NOT NULL DEFAULT '',
  `t3ver_state` tinyint(4) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(11) NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(11) NOT NULL DEFAULT '0',
  `t3ver_move_id` int(11) NOT NULL DEFAULT '0',
  `t3_origuid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `sorting` int(11) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `perms_userid` int(11) unsigned NOT NULL DEFAULT '0',
  `perms_groupid` int(11) unsigned NOT NULL DEFAULT '0',
  `perms_user` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `perms_group` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `perms_everybody` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `editlock` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `hidden` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `doktype` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `TSconfig` text,
  `storage_pid` int(11) NOT NULL DEFAULT '0',
  `is_siteroot` tinyint(4) NOT NULL DEFAULT '0',
  `php_tree_stop` tinyint(4) NOT NULL DEFAULT '0',
  `tx_impexp_origuid` int(11) NOT NULL DEFAULT '0',
  `url` varchar(255) NOT NULL DEFAULT '',
  `starttime` int(11) unsigned NOT NULL DEFAULT '0',
  `endtime` int(11) unsigned NOT NULL DEFAULT '0',
  `urltype` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `shortcut` int(10) unsigned NOT NULL DEFAULT '0',
  `shortcut_mode` int(10) unsigned NOT NULL DEFAULT '0',
  `no_cache` int(10) unsigned NOT NULL DEFAULT '0',
  `fe_group` varchar(100) NOT NULL DEFAULT '0',
  `subtitle` varchar(255) NOT NULL DEFAULT '',
  `layout` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `url_scheme` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `target` varchar(80) NOT NULL DEFAULT '',
  `media` text,
  `lastUpdated` int(10) unsigned NOT NULL DEFAULT '0',
  `keywords` text,
  `cache_timeout` int(10) unsigned NOT NULL DEFAULT '0',
  `newUntil` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text,
  `no_search` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `SYS_LASTCHANGED` int(10) unsigned NOT NULL DEFAULT '0',
  `abstract` text,
  `module` varchar(10) NOT NULL DEFAULT '',
  `extendToSubpages` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `author` varchar(255) NOT NULL DEFAULT '',
  `author_email` varchar(80) NOT NULL DEFAULT '',
  `nav_title` varchar(255) NOT NULL DEFAULT '',
  `nav_hide` tinyint(4) NOT NULL DEFAULT '0',
  `content_from_pid` int(10) unsigned NOT NULL DEFAULT '0',
  `mount_pid` int(10) unsigned NOT NULL DEFAULT '0',
  `mount_pid_ol` tinyint(4) NOT NULL DEFAULT '0',
  `alias` varchar(32) NOT NULL DEFAULT '',
  `l18n_cfg` tinyint(4) NOT NULL DEFAULT '0',
  `fe_login_mode` tinyint(4) NOT NULL DEFAULT '0',
  `backend_layout` int(10) NOT NULL DEFAULT '0',
  `backend_layout_next_level` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`),
  KEY `parent` (`pid`,`deleted`,`sorting`),
  KEY `alias` (`alias`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;


CREATE TABLE IF NOT EXISTS `pages_language_overlay` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `doktype` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `t3ver_oid` int(11) NOT NULL DEFAULT '0',
  `t3ver_id` int(11) NOT NULL DEFAULT '0',
  `t3ver_wsid` int(11) NOT NULL DEFAULT '0',
  `t3ver_label` varchar(255) NOT NULL DEFAULT '',
  `t3ver_state` tinyint(4) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(11) NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(11) NOT NULL DEFAULT '0',
  `t3_origuid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `sys_language_uid` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `hidden` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `starttime` int(11) unsigned NOT NULL DEFAULT '0',
  `endtime` int(11) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `subtitle` varchar(255) NOT NULL DEFAULT '',
  `nav_title` varchar(255) NOT NULL DEFAULT '',
  `media` text,
  `keywords` text,
  `description` text,
  `abstract` text,
  `author` varchar(255) NOT NULL DEFAULT '',
  `author_email` varchar(80) NOT NULL DEFAULT '',
  `tx_impexp_origuid` int(11) NOT NULL DEFAULT '0',
  `l18n_diffsource` mediumblob,
  `url` varchar(255) NOT NULL DEFAULT '',
  `urltype` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `shortcut` int(10) unsigned NOT NULL DEFAULT '0',
  `shortcut_mode` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`),
  KEY `parent` (`pid`,`sys_language_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `static_tsconfig_help` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `guide` int(11) NOT NULL DEFAULT '0',
  `md5hash` varchar(32) NOT NULL DEFAULT '',
  `description` text,
  `obj_string` varchar(255) NOT NULL DEFAULT '',
  `appdata` blob,
  `title` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `guide` (`guide`,`md5hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `sys_be_shortcuts` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL DEFAULT '0',
  `module_name` varchar(255) NOT NULL DEFAULT '',
  `url` text,
  `description` varchar(255) NOT NULL DEFAULT '',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `sc_group` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `event` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `sys_collection` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `cruser_id` int(11) NOT NULL DEFAULT '0',
  `t3ver_oid` int(11) NOT NULL DEFAULT '0',
  `t3ver_id` int(11) NOT NULL DEFAULT '0',
  `t3ver_wsid` int(11) NOT NULL DEFAULT '0',
  `t3ver_label` varchar(30) NOT NULL DEFAULT '',
  `t3ver_state` tinyint(4) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(11) NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(11) NOT NULL DEFAULT '0',
  `t3ver_move_id` int(11) NOT NULL DEFAULT '0',
  `t3_origuid` int(11) NOT NULL DEFAULT '0',
  `sys_language_uid` int(11) NOT NULL DEFAULT '0',
  `l10n_parent` int(11) NOT NULL DEFAULT '0',
  `l10n_diffsource` mediumtext,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `hidden` tinyint(4) NOT NULL DEFAULT '0',
  `starttime` int(11) NOT NULL DEFAULT '0',
  `endtime` int(11) NOT NULL DEFAULT '0',
  `fe_group` int(11) NOT NULL DEFAULT '0',
  `title` tinytext,
  `description` text,
  `type` varchar(32) NOT NULL DEFAULT 'static',
  `table_name` tinytext,
  `items` int(11) NOT NULL DEFAULT '0',
  `criteria` text NOT NULL,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `sys_collection_entries` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `uid_local` int(11) NOT NULL DEFAULT '0',
  `uid_foreign` int(11) NOT NULL DEFAULT '0',
  `tablenames` varchar(30) NOT NULL DEFAULT '',
  `sorting` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `uid_local` (`uid_local`),
  KEY `uid_foreign` (`uid_foreign`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `sys_domain` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `hidden` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `domainName` varchar(80) NOT NULL DEFAULT '',
  `redirectTo` varchar(255) NOT NULL DEFAULT '',
  `redirectHttpStatusCode` int(4) unsigned NOT NULL DEFAULT '301',
  `sorting` int(10) unsigned NOT NULL DEFAULT '0',
  `prepend_params` int(10) NOT NULL DEFAULT '0',
  `forced` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `sys_filemounts` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(30) NOT NULL DEFAULT '',
  `path` varchar(120) NOT NULL DEFAULT '',
  `base` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `hidden` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sorting` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `sys_history` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sys_log_uid` int(11) NOT NULL DEFAULT '0',
  `history_data` mediumtext,
  `fieldlist` text,
  `recuid` int(11) NOT NULL DEFAULT '0',
  `tablename` varchar(255) NOT NULL DEFAULT '',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `history_files` mediumtext,
  `snapshot` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `recordident_1` (`tablename`,`recuid`),
  KEY `recordident_2` (`tablename`,`tstamp`),
  KEY `sys_log_uid` (`sys_log_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `sys_language` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `hidden` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `title` varchar(80) NOT NULL DEFAULT '',
  `flag` varchar(20) NOT NULL DEFAULT '',
  `static_lang_isocode` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `sys_lockedrecords` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `record_table` varchar(255) NOT NULL DEFAULT '',
  `record_uid` int(11) NOT NULL DEFAULT '0',
  `record_pid` int(11) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '',
  `feuserid` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `event` (`userid`,`tstamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `sys_log` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL DEFAULT '0',
  `action` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `recuid` int(11) unsigned NOT NULL DEFAULT '0',
  `tablename` varchar(255) NOT NULL DEFAULT '',
  `recpid` int(11) NOT NULL DEFAULT '0',
  `error` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `details` text NOT NULL,
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `details_nr` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `IP` varchar(39) NOT NULL DEFAULT '',
  `log_data` varchar(255) NOT NULL DEFAULT '',
  `event_pid` int(11) NOT NULL DEFAULT '-1',
  `workspace` int(11) NOT NULL DEFAULT '0',
  `NEWid` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `event` (`userid`,`event_pid`),
  KEY `recuidIdx` (`recuid`,`uid`),
  KEY `user_auth` (`type`,`action`,`tstamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `sys_news` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `hidden` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `starttime` int(11) unsigned NOT NULL DEFAULT '0',
  `endtime` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `content` mediumtext,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `sys_note` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser` int(11) unsigned NOT NULL DEFAULT '0',
  `author` varchar(80) NOT NULL DEFAULT '',
  `email` varchar(80) NOT NULL DEFAULT '',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `message` text,
  `personal` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `category` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `sys_preview` (
  `keyword` varchar(32) NOT NULL DEFAULT '',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `endtime` int(11) NOT NULL DEFAULT '0',
  `config` text,
  PRIMARY KEY (`keyword`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `sys_refindex` (
  `hash` varchar(32) NOT NULL DEFAULT '',
  `tablename` varchar(255) NOT NULL DEFAULT '',
  `recuid` int(11) NOT NULL DEFAULT '0',
  `field` varchar(40) NOT NULL DEFAULT '',
  `flexpointer` varchar(255) NOT NULL DEFAULT '',
  `softref_key` varchar(30) NOT NULL DEFAULT '',
  `softref_id` varchar(40) NOT NULL DEFAULT '',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `ref_table` varchar(255) NOT NULL DEFAULT '',
  `ref_uid` int(11) NOT NULL DEFAULT '0',
  `ref_string` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`hash`),
  KEY `lookup_rec` (`tablename`,`recuid`),
  KEY `lookup_uid` (`ref_table`,`ref_uid`),
  KEY `lookup_string` (`ref_string`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `sys_registry` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `entry_namespace` varchar(128) NOT NULL DEFAULT '',
  `entry_key` varchar(128) NOT NULL DEFAULT '',
  `entry_value` blob,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `entry_identifier` (`entry_namespace`,`entry_key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

CREATE TABLE IF NOT EXISTS `sys_template` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `t3ver_oid` int(11) NOT NULL DEFAULT '0',
  `t3ver_id` int(11) NOT NULL DEFAULT '0',
  `t3ver_wsid` int(11) NOT NULL DEFAULT '0',
  `t3ver_label` varchar(255) NOT NULL DEFAULT '',
  `t3ver_state` tinyint(4) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(11) NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(11) NOT NULL DEFAULT '0',
  `t3_origuid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `sorting` int(11) unsigned NOT NULL DEFAULT '0',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `sitetitle` varchar(255) NOT NULL DEFAULT '',
  `hidden` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `starttime` int(11) unsigned NOT NULL DEFAULT '0',
  `endtime` int(11) unsigned NOT NULL DEFAULT '0',
  `root` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `clear` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `include_static_file` text,
  `constants` text,
  `config` text,
  `resources` text,
  `nextLevel` varchar(5) NOT NULL DEFAULT '',
  `description` text,
  `basedOn` tinytext,
  `deleted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `includeStaticAfterBasedOn` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `static_file_mode` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `tx_impexp_origuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`),
  KEY `parent` (`pid`,`deleted`,`hidden`,`sorting`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;


CREATE TABLE IF NOT EXISTS `sys_ter` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL DEFAULT '',
  `description` mediumtext,
  `wsdl_url` varchar(100) NOT NULL DEFAULT '',
  `mirror_url` varchar(100) NOT NULL DEFAULT '',
  `lastUpdated` int(11) unsigned NOT NULL DEFAULT '0',
  `extCount` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;


CREATE TABLE IF NOT EXISTS `tt_content` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `t3ver_oid` int(11) NOT NULL DEFAULT '0',
  `t3ver_id` int(11) NOT NULL DEFAULT '0',
  `t3ver_wsid` int(11) NOT NULL DEFAULT '0',
  `t3ver_label` varchar(255) NOT NULL DEFAULT '',
  `t3ver_state` tinyint(4) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(11) NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(11) NOT NULL DEFAULT '0',
  `t3ver_move_id` int(11) NOT NULL DEFAULT '0',
  `t3_origuid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `hidden` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `sorting` int(11) unsigned NOT NULL DEFAULT '0',
  `CType` varchar(30) NOT NULL DEFAULT '',
  `header` varchar(255) NOT NULL DEFAULT '',
  `header_position` varchar(6) NOT NULL DEFAULT '',
  `bodytext` mediumtext,
  `image` text,
  `imagewidth` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `imageorient` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `imagecaption` text,
  `imagecols` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `imageborder` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `media` text,
  `layout` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `cols` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `records` text,
  `pages` tinytext,
  `starttime` int(11) unsigned NOT NULL DEFAULT '0',
  `endtime` int(11) unsigned NOT NULL DEFAULT '0',
  `colPos` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `subheader` varchar(255) NOT NULL DEFAULT '',
  `spaceBefore` smallint(5) unsigned NOT NULL DEFAULT '0',
  `spaceAfter` smallint(5) unsigned NOT NULL DEFAULT '0',
  `fe_group` varchar(100) NOT NULL DEFAULT '0',
  `header_link` varchar(255) NOT NULL DEFAULT '',
  `imagecaption_position` varchar(6) NOT NULL DEFAULT '',
  `image_link` text,
  `image_zoom` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `image_noRows` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `image_effects` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `image_compression` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `altText` text,
  `titleText` text,
  `longdescURL` text,
  `header_layout` varchar(30) NOT NULL DEFAULT '0',
  `text_align` varchar(6) NOT NULL DEFAULT '',
  `text_face` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `text_size` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `text_color` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `text_properties` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `menu_type` varchar(30) NOT NULL DEFAULT '0',
  `list_type` varchar(255) NOT NULL DEFAULT '0',
  `table_border` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `table_cellspacing` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `table_cellpadding` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `table_bgColor` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `select_key` varchar(80) NOT NULL DEFAULT '',
  `sectionIndex` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `linkToTop` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `filelink_size` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `target` varchar(30) NOT NULL DEFAULT '',
  `section_frame` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `splash_layout` varchar(30) NOT NULL DEFAULT '0',
  `multimedia` tinytext,
  `image_frames` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `recursive` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `imageheight` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `rte_enabled` tinyint(4) NOT NULL DEFAULT '0',
  `sys_language_uid` int(11) NOT NULL DEFAULT '0',
  `tx_impexp_origuid` int(11) NOT NULL DEFAULT '0',
  `pi_flexform` mediumtext,
  `accessibility_title` varchar(30) NOT NULL DEFAULT '',
  `accessibility_bypass` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `accessibility_bypass_text` varchar(30) NOT NULL DEFAULT '',
  `l18n_parent` int(11) NOT NULL DEFAULT '0',
  `l18n_diffsource` mediumblob,
  PRIMARY KEY (`uid`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`),
  KEY `parent` (`pid`,`sorting`),
  KEY `language` (`l18n_parent`,`sys_language_uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;


CREATE TABLE IF NOT EXISTS `tx_impexp_presets` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `user_uid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `public` tinyint(3) NOT NULL DEFAULT '0',
  `item_uid` int(11) NOT NULL DEFAULT '0',
  `preset_data` blob,
  PRIMARY KEY (`uid`),
  KEY `lookup` (`item_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tx_nG6_analyze` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `cruser_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `hidden` tinyint(4) NOT NULL DEFAULT '0',
  `parent_uid` int(11) NOT NULL DEFAULT '0',
  `class` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `date` int(11) NOT NULL DEFAULT '0',
  `directory` varchar(255) NOT NULL DEFAULT '',
  `software` varchar(255) NOT NULL DEFAULT '',
  `version` varchar(255) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `is_editable` boolean NOT NULL DEFAULT FALSE,
  `storage_size` bigint(20) NOT NULL DEFAULT '0',
  `data_state` varchar(255) NOT NULL DEFAULT 'stored',
  `retention_date` INT(11) NOT NULL ,
  `purged_date` INT(11) NOT NULL , 
  `purged_size` BIGINT NOT NULL ,
  `mail_sent_date` INT(11) NOT NULL ,
  `purge_demand_id` INT NULL DEFAULT NULL,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tx_nG6_project` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `cruser_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `hidden` tinyint(4) NOT NULL DEFAULT '0',
  `public` tinyint(4) NOT NULL DEFAULT '1',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tx_nG6_project_analyze` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `cruser_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `hidden` tinyint(4) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `analyze_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tx_nG6_project_run` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `cruser_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `hidden` tinyint(4) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `run_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tx_nG6_result` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `cruser_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `hidden` tinyint(4) NOT NULL DEFAULT '0',
  `analyze_id` int(11) NOT NULL DEFAULT '0',
  `file` varchar(255) NOT NULL DEFAULT '',
  `rkey` varchar(255) NOT NULL DEFAULT '',
  `rvalue` text NOT NULL,
  `rgroup` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tx_nG6_run` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `cruser_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `hidden` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `date` int(11) NOT NULL DEFAULT '0',
  `directory` varchar(255) NOT NULL DEFAULT '',
  `species` varchar(255) NOT NULL DEFAULT '',
  `data_nature` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(255) NOT NULL DEFAULT '',
  `nb_sequences` varchar(255) NOT NULL DEFAULT '0',
  `full_seq_size` varchar(255) NOT NULL DEFAULT '0',
  `description` text ,
  `sequencer` varchar(255) NOT NULL DEFAULT '',
  `storage_size` bigint(20) NOT NULL DEFAULT '0',
  `data_state` VARCHAR( 250 ) NOT NULL DEFAULT 'stored' ,
  `retention_date` INT NOT NULL ,
  `purged_date` INT NOT NULL ,
  `purged_size` BIGINT NOT NULL ,
  `mail_sent_date` INT NOT NULL ,
  `purge_demand_id` INT NULL DEFAULT NULL,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tx_nG6_run_analyze` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `cruser_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `hidden` tinyint(4) NOT NULL DEFAULT '0',
  `run_id` int(11) NOT NULL DEFAULT '0',
  `analyze_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tx_nG6_sample` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `cruser_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `hidden` tinyint(4) NOT NULL DEFAULT '0',
  `run_id` int(11) NOT NULL DEFAULT '0',
  `sample_id` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `reads1` text ,
  `reads2` text ,
  `type` varchar(255) NOT NULL DEFAULT '',
  `insert_size` int(11) NOT NULL DEFAULT '0',
  `species` varchar(255) NOT NULL DEFAULT '',
  `nb_sequences` int(11) NOT NULL DEFAULT '0',
  `full_seq_size` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tx_nG6_comment` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `cruser_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `hidden` tinyint(4) NOT NULL DEFAULT '0',
  `comment` text ,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tx_nG6_project_comment` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `cruser_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `hidden` tinyint(4) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `comment_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tx_nG6_run_comment` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `cruser_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `hidden` tinyint(4) NOT NULL DEFAULT '0',
  `run_id` int(11) NOT NULL DEFAULT '0',
  `comment_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tx_nG6_analyze_comment` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `cruser_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `hidden` tinyint(4) NOT NULL DEFAULT '0',
  `analyze_id` int(11) NOT NULL DEFAULT '0',
  `comment_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tx_rsaauth_keys` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `key_value` text,
  PRIMARY KEY (`uid`),
  KEY `crdate` (`crdate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tx_rtehtmlarea_acronym` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `hidden` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `starttime` int(11) unsigned NOT NULL DEFAULT '0',
  `endtime` int(11) unsigned NOT NULL DEFAULT '0',
  `sorting` int(11) unsigned NOT NULL DEFAULT '0',
  `sys_language_uid` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `term` varchar(255) NOT NULL DEFAULT '',
  `acronym` varchar(255) NOT NULL DEFAULT '',
  `static_lang_isocode` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tx_nG6_purge_demand` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `cruser_id` int(11) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL,
  `purge_size` bigint(20),
  `mail_sent_date` int(11),
  `processed_date` int(11),
  `demand_state` varchar(255),
  `analyze_ids` TEXT NOT NULL ,
  `run_ids` TEXT NOT NULL ,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30060 ;


CREATE TABLE IF NOT EXISTS `tx_nG6_purge_demand_fe_users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) NOT NULL DEFAULT '0',
  `crdate` int(11) NOT NULL DEFAULT '0',
  `cruser_id` int(11) NOT NULL DEFAULT '0',
  `purge_demand_id` int(11),
  `fe_users_id` int(11),
  `message` text, 
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30060 ;



INSERT INTO `be_users` (`uid`, `pid`, `tstamp`, `username`, `password`, `admin`, `usergroup`, `disable`, `starttime`, `endtime`, `lang`, `email`, `db_mountpoints`, `options`, `crdate`, `cruser_id`, `realName`, `userMods`, `allowed_languages`, `uc`, `file_mountpoints`, `fileoper_perms`, `workspace_perms`, `lockToDomain`, `disableIPlock`, `deleted`, `TSconfig`, `lastlogin`, `createdByAction`, `usergroup_cached_list`, `workspace_id`, `workspace_preview`) VALUES
(1, 0, 1381239486, 'admin_install', '$P$CbNAUHFOYcMbg2y5aE7yfWl/sAOqMF0', 1, '', 0, 0, 0, '', '', '', 0, 1381239486, 0, '', '', '', 'a:27:{s:14:"interfaceSetup";s:7:"backend";s:10:"moduleData";a:11:{s:8:"tools_em";a:5:{s:8:"function";s:11:"loaded_list";s:9:"listOrder";s:3:"cat";s:15:"display_details";s:1:"1";s:13:"singleDetails";s:4:"info";s:13:"extensionInfo";s:0:"";}s:10:"web_layout";a:2:{s:8:"function";s:1:"1";s:8:"language";s:1:"0";}s:12:"tools_config";a:2:{s:8:"function";s:1:"0";s:6:"node_0";a:1:{s:3:"SYS";i:1;}}s:16:"xMOD_alt_doc.php";a:0:{}s:6:"web_ts";a:2:{s:8:"function";s:17:"tx_tstemplateinfo";s:19:"constant_editor_cat";s:0:"";}s:8:"web_list";a:0:{}s:11:"alt_doc.php";a:2:{i:0;a:1:{s:32:"b2a52a64aeabdfa079e0aca938cff05a";a:4:{i:0;s:4:"Home";i:1;a:7:{s:4:"edit";a:1:{s:5:"pages";a:1:{i:1;s:4:"edit";}}s:7:"defVals";N;s:12:"overrideVals";N;s:11:"columnsOnly";N;s:7:"disHelp";N;s:6:"noView";N;s:24:"editRegularContentFromId";N;}i:2;s:99:"&edit[pages][1]=edit&defVals=&overrideVals=&columnsOnly=&disHelp=&noView=&editRegularContentFromId=";i:3;a:5:{s:5:"table";s:5:"pages";s:3:"uid";s:1:"1";s:3:"pid";s:1:"0";s:3:"cmd";s:4:"edit";s:12:"deleteAccess";i:1;}}}i:1;s:32:"fb7ba7367770a71d70f46f6c0b9f6e0d";}s:29:"t3lib_BEfunc::getUpdateSignal";a:0:{}s:16:"browse_links.php";a:1:{s:10:"expandPage";s:1:"5";}s:16:"opendocs::recent";a:5:{s:32:"fb7ba7367770a71d70f46f6c0b9f6e0d";a:4:{i:0;s:4:"+ext";i:1;a:7:{s:4:"edit";a:1:{s:12:"sys_template";a:1:{i:4;s:4:"edit";}}s:7:"defVals";N;s:12:"overrideVals";N;s:11:"columnsOnly";N;s:7:"disHelp";N;s:6:"noView";N;s:24:"editRegularContentFromId";N;}i:2;s:106:"&edit[sys_template][4]=edit&defVals=&overrideVals=&columnsOnly=&disHelp=&noView=&editRegularContentFromId=";i:3;a:5:{s:5:"table";s:12:"sys_template";s:3:"uid";s:1:"4";s:3:"pid";s:1:"4";s:3:"cmd";s:4:"edit";s:12:"deleteAccess";i:1;}}s:32:"c4af6f757b6bce2e8bff9a85ee10acd3";a:4:{i:0;s:4:"+ext";i:1;a:7:{s:4:"edit";a:1:{s:12:"sys_template";a:1:{i:3;s:4:"edit";}}s:7:"defVals";N;s:12:"overrideVals";N;s:11:"columnsOnly";N;s:7:"disHelp";N;s:6:"noView";N;s:24:"editRegularContentFromId";N;}i:2;s:106:"&edit[sys_template][3]=edit&defVals=&overrideVals=&columnsOnly=&disHelp=&noView=&editRegularContentFromId=";i:3;a:5:{s:5:"table";s:12:"sys_template";s:3:"uid";s:1:"3";s:3:"pid";s:1:"3";s:3:"cmd";s:4:"edit";s:12:"deleteAccess";i:1;}}s:32:"481f0e0c68eef987ca4a90100b2fde9d";a:4:{i:0;s:4:"+ext";i:1;a:7:{s:4:"edit";a:1:{s:12:"sys_template";a:1:{i:2;s:4:"edit";}}s:7:"defVals";N;s:12:"overrideVals";N;s:11:"columnsOnly";N;s:7:"disHelp";N;s:6:"noView";N;s:24:"editRegularContentFromId";N;}i:2;s:106:"&edit[sys_template][2]=edit&defVals=&overrideVals=&columnsOnly=&disHelp=&noView=&editRegularContentFromId=";i:3;a:5:{s:5:"table";s:12:"sys_template";s:3:"uid";s:1:"2";s:3:"pid";s:1:"2";s:3:"cmd";s:4:"edit";s:12:"deleteAccess";i:1;}}s:32:"99eb84e187fc30eeba3ec311ec622692";a:4:{i:0;s:8:"NEW SITE";i:1;a:7:{s:4:"edit";a:1:{s:12:"sys_template";a:1:{i:1;s:4:"edit";}}s:7:"defVals";N;s:12:"overrideVals";N;s:11:"columnsOnly";N;s:7:"disHelp";N;s:6:"noView";N;s:24:"editRegularContentFromId";N;}i:2;s:106:"&edit[sys_template][1]=edit&defVals=&overrideVals=&columnsOnly=&disHelp=&noView=&editRegularContentFromId=";i:3;a:5:{s:5:"table";s:12:"sys_template";s:3:"uid";s:1:"1";s:3:"pid";s:1:"1";s:3:"cmd";s:4:"edit";s:12:"deleteAccess";i:1;}}s:32:"b2a52a64aeabdfa079e0aca938cff05a";a:4:{i:0;s:4:"Home";i:1;a:7:{s:4:"edit";a:1:{s:5:"pages";a:1:{i:1;s:4:"edit";}}s:7:"defVals";N;s:12:"overrideVals";N;s:11:"columnsOnly";N;s:7:"disHelp";N;s:6:"noView";N;s:24:"editRegularContentFromId";N;}i:2;s:99:"&edit[pages][1]=edit&defVals=&overrideVals=&columnsOnly=&disHelp=&noView=&editRegularContentFromId=";i:3;a:5:{s:5:"table";s:5:"pages";s:3:"uid";s:1:"1";s:3:"pid";s:1:"0";s:3:"cmd";s:4:"edit";s:12:"deleteAccess";i:1;}}}s:13:"tools_install";a:1:{s:8:"function";s:0:"";}}s:19:"thumbnailsByDefault";i:1;s:14:"emailMeAtLogin";i:0;s:13:"condensedMode";i:0;s:10:"noMenuMode";i:0;s:11:"startModule";s:17:"help_aboutmodules";s:18:"hideSubmoduleIcons";i:0;s:8:"helpText";i:1;s:8:"titleLen";i:50;s:17:"edit_wideDocument";s:1:"0";s:18:"edit_showFieldHelp";s:4:"icon";s:8:"edit_RTE";s:1:"1";s:20:"edit_docModuleUpload";s:1:"1";s:19:"enableFlashUploader";s:1:"1";s:15:"disableCMlayers";i:0;s:13:"navFrameWidth";s:0:"";s:17:"navFrameResizable";i:0;s:15:"resizeTextareas";i:1;s:25:"resizeTextareas_MaxHeight";i:500;s:24:"resizeTextareas_Flexible";i:0;s:4:"lang";s:0:"";s:19:"firstLoginTimeStamp";i:1381239660;s:15:"moduleSessionID";a:11:{s:8:"tools_em";s:32:"bb989b13a3145bdfbe178325051c277f";s:10:"web_layout";s:32:"42ed621d6dfd7cff37981d8d584bcbf4";s:12:"tools_config";s:32:"bb989b13a3145bdfbe178325051c277f";s:16:"xMOD_alt_doc.php";s:32:"bb989b13a3145bdfbe178325051c277f";s:6:"web_ts";s:32:"bb989b13a3145bdfbe178325051c277f";s:8:"web_list";s:32:"bb989b13a3145bdfbe178325051c277f";s:11:"alt_doc.php";s:32:"bb989b13a3145bdfbe178325051c277f";s:29:"t3lib_BEfunc::getUpdateSignal";s:32:"bb989b13a3145bdfbe178325051c277f";s:16:"browse_links.php";s:32:"bb989b13a3145bdfbe178325051c277f";s:16:"opendocs::recent";s:32:"bb989b13a3145bdfbe178325051c277f";s:13:"tools_install";s:32:"bb989b13a3145bdfbe178325051c277f";}s:17:"BackendComponents";a:1:{s:6:"States";a:2:{s:19:"typo3-debug-console";O:8:"stdClass":1:{s:9:"collapsed";b:1;}s:8:"Pagetree";O:8:"stdClass":1:{s:9:"stateHash";O:8:"stdClass":8:{s:1:"0";i:1;s:1:"1";i:1;s:1:"2";i:1;s:1:"3";i:1;s:1:"4";i:1;s:1:"5";i:1;s:4:"root";i:1;s:16:"lastSelectedNode";s:2:"p6";}}}}s:18:"disablePMKTextarea";i:1;s:11:"browseTrees";a:1:{s:11:"browsePages";s:24:"a:1:{i:0;a:1:{i:0;i:1;}}";}}', '', 0, 1, '', 0, 0, NULL, 1382432509, 0, '', 0, 1);


INSERT INTO `fe_groups` (`uid`, `pid`, `tstamp`, `crdate`, `cruser_id`, `title`, `hidden`, `lockToDomain`, `deleted`, `description`, `subgroup`, `TSconfig`, `tx_extbase_type`, `felogin_redirectPid`) VALUES
(1, 5, 1381241108, 1381241108, 1, 'ng6_admin', 0, '', 0, '', '', '', '0', '');
INSERT INTO `fe_groups` (`uid`, `pid`, `tstamp`, `crdate`, `cruser_id`, `title`, `hidden`, `lockToDomain`, `deleted`, `description`, `subgroup`, `TSconfig`, `tx_extbase_type`, `felogin_redirectPid`) VALUES
(2, 5, 1381241108, 1381241108, 1, 'ng6_superadmin', 0, '', 0, '', '', '', '0', '');

INSERT INTO `fe_rights` (`fe_user_id`, `project_id`, `right_id`) VALUES
(1, 1, 2);


INSERT INTO `fe_rights_levels` (`right_level_id`, `right_level_label`) VALUES
(0, 'member'),
(1, 'manager'),
(2, 'administrator');

INSERT INTO `fe_users` (`uid`, `pid`, `tstamp`, `username`, `password`, `usergroup`, `disable`, `starttime`, `endtime`, `name`, `first_name`, `middle_name`, `last_name`, `address`, `telephone`, `fax`, `email`, `crdate`, `cruser_id`, `lockToDomain`, `deleted`, `uc`, `title`, `zip`, `city`, `country`, `www`, `company`, `image`, `TSconfig`, `fe_cruser_id`, `lastlogin`, `is_online`, `tx_extbase_type`, `felogin_redirectPid`, `felogin_forgotHash`) VALUES
(1, 5, 1381241178, 'admin_install', '$P$CbNAUHFOYcMbg2y5aE7yfWl/sAOqMF0', '1', 0, 0, 0, '', '', '', '', '', '', '', '', 1381241178, 1, '', 0, NULL, '', '', '', '', '', '', '', '', 0, 1382434160, 1382434160, '0', '', '');


INSERT INTO `pages` (`uid`, `pid`, `t3ver_oid`, `t3ver_id`, `t3ver_wsid`, `t3ver_label`, `t3ver_state`, `t3ver_stage`, `t3ver_count`, `t3ver_tstamp`, `t3ver_move_id`, `t3_origuid`, `tstamp`, `sorting`, `deleted`, `perms_userid`, `perms_groupid`, `perms_user`, `perms_group`, `perms_everybody`, `editlock`, `crdate`, `cruser_id`, `hidden`, `title`, `doktype`, `TSconfig`, `storage_pid`, `is_siteroot`, `php_tree_stop`, `tx_impexp_origuid`, `url`, `starttime`, `endtime`, `urltype`, `shortcut`, `shortcut_mode`, `no_cache`, `fe_group`, `subtitle`, `layout`, `url_scheme`, `target`, `media`, `lastUpdated`, `keywords`, `cache_timeout`, `newUntil`, `description`, `no_search`, `SYS_LASTCHANGED`, `abstract`, `module`, `extendToSubpages`, `author`, `author_email`, `nav_title`, `nav_hide`, `content_from_pid`, `mount_pid`, `mount_pid_ol`, `alias`, `l18n_cfg`, `fe_login_mode`, `backend_layout`, `backend_layout_next_level`) VALUES
(1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 1381241294, 256, 0, 1, 0, 31, 27, 0, 0, 1381240389, 1, 0, 'Home', 1, NULL, 5, 0, 0, 0, '', 0, 0, 1, 0, 0, 0, '', '', 0, 0, '', NULL, 0, NULL, 0, 0, NULL, 0, 0, NULL, '', 0, '', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0),
(2, 1, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 1381240477, 256, 0, 1, 0, 31, 27, 0, 0, 1381240426, 1, 1, 'Runs', 1, NULL, 0, 0, 0, 0, '', 0, 0, 1, 0, 0, 0, '0', '', 0, 0, '', NULL, 0, NULL, 0, 0, NULL, 0, 0, NULL, '', 0, '', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0),
(3, 1, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 1381240475, 128, 0, 1, 0, 31, 27, 0, 0, 1381240435, 1, 0, 'Install', 1, NULL, 0, 0, 0, 0, '', 0, 0, 1, 0, 0, 0, '0', '', 0, 0, '', NULL, 0, NULL, 0, 0, NULL, 0, 0, NULL, '', 0, '', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0),
(4, 1, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 1381240479, 768, 0, 1, 0, 31, 27, 0, 0, 1381240445, 1, 1, 'Downloads', 1, NULL, 0, 0, 0, 0, '', 0, 0, 1, 0, 0, 0, '0', '', 0, 0, '', NULL, 0, NULL, 0, 0, NULL, 0, 0, NULL, '', 0, '', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0),
(5, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 1381240471, 512, 0, 1, 0, 31, 27, 0, 0, 1381240455, 1, 0, 'StorageFolder', 254, NULL, 0, 0, 0, 0, '', 0, 0, 1, 0, 0, 0, '0', '', 0, 0, '', NULL, 0, NULL, 0, 0, NULL, 0, 0, NULL, '', 0, '', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0),
(6, 1, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 1387292516, 1024, 0, 1, 0, 31, 27, 0, 0, 1382434107, 1, 0, 'Administration', 1, NULL, 0, 0, 0, 0, '', 0, 0, 1, 0, 0, 0, '1', '', 0, 0, '', NULL, 0, NULL, 0, 0, NULL, 0, 1387292193, NULL, '', 0, '', '', '', 0, 0, 0, 0, '', 0, 0, 0, 0);

INSERT INTO `sys_refindex` (`hash`, `tablename`, `recuid`, `field`, `flexpointer`, `softref_key`, `softref_id`, `sorting`, `deleted`, `ref_table`, `ref_uid`, `ref_string`) VALUES
('001cd2e4390e0152274a1a31cabbf067', 'fe_users', 1, 'usergroup', '', '', '', 0, 0, 'fe_groups', 1, ''),
('06c2d1e556139351a2048e219a283ca2', 'sys_template', 1, 'config', '', 'url', '7', -1, 0, '_STRING', 0, 'http://127.0.0.1:8080/'),
('07470037e2e141d30d7e781a4b93cfb0', 'sys_template', 1, 'config', '', 'url', '2', -1, 0, '_STRING', 0, 'http://127.0.0.1:8080/'),
('29b1a8fdc9dbdabe47fab9fa901556bd', 'sys_template', 1, 'config', '', 'TStemplate', 'fileadminReferences.8', -1, 0, '_FILE', 0, 'typo3conf/ext/nG6/template/css/ie6.css'),
('34583d1c5fe009b1b48fe1fb78e56aa3', 'sys_template', 1, 'config', '', 'TStemplate', 'fileadminReferences.11', -1, 0, '_FILE', 0, 'typo3conf/ext/nG6/template/images/picto-home.png'),
('3a9b81b1e8820bc91e4af1be815c8e10', 'sys_template', 1, 'config', '', 'TStemplate', 'fileadminReferences.14', -1, 0, '_FILE', 0, 'typo3conf/ext/nG6/template/index.html'),
('5c083489c1307a61d2dff0174f4874ca', 'pages', 1, 'storage_pid', '', '', '', 0, 0, 'pages', 5, ''),
('62f3521aab7d0c50a47bec684ec3bb69', 'sys_template', 1, 'config', '', 'TStemplate', 'fileadminReferences.2', -1, 0, '_FILE', 0, 'typo3conf/ext/nG6/template/css/style.css'),
('780391944819cf5056371e32c4b414ef', 'sys_template', 1, 'config', '', 'TStemplate', 'fileadminReferences.5', -1, 0, '_FILE', 0, 'typo3conf/ext/nG6/res/css/bootstrap.min.css'),
('96cdb5c15937a29094996fb7485c0f04', 'sys_template', 1, 'config', '', 'TStemplate', 'fileadminReferences.20', -1, 0, '_FILE', 0, 'fileadmin/data'),
('bb0426f5c6f9400d2e5b8de75142ebaf', 'sys_template', 1, 'config', '', 'TStemplate', 'fileadminReferences.17', -1, 0, '_FILE', 0, 'fileadmin/tmp'),
('f5aef573c5ff741002a58cea70b3b09e', 'sys_template', 1, 'config', '', 'email', '2', -1, 0, '_STRING', 0, 'support@ng6.toulouse.inra.fr');

INSERT INTO `sys_template` (`uid`, `pid`, `t3ver_oid`, `t3ver_id`, `t3ver_wsid`, `t3ver_label`, `t3ver_state`, `t3ver_stage`, `t3ver_count`, `t3ver_tstamp`, `t3_origuid`, `tstamp`, `sorting`, `crdate`, `cruser_id`, `title`, `sitetitle`, `hidden`, `starttime`, `endtime`, `root`, `clear`, `include_static_file`, `constants`, `config`, `resources`, `nextLevel`, `description`, `basedOn`, `deleted`, `includeStaticAfterBasedOn`, `static_file_mode`, `tx_impexp_origuid`) VALUES
(1, 1, 0, 0, 0, '', 0, 0, 0, 0, 0, 1381241401, 256, 1381240575, 1, 'NEW SITE', '', 0, 0, 0, 1, 3, 'EXT:css_styled_content/static/', NULL, 'page = PAGE\r\npage.typeNum = 0\r\n\r\npage.meta.DESCRIPTION = nG6\r\npage.meta.KEYWORDS = Sequencing\r\n\r\npage.includeCSS.file1 = typo3conf/ext/nG6/res/css/bootstrap.min.css\r\npage.includeCSS.file2 = typo3conf/ext/nG6/res/css/bootstrap-theme.min.css\r\npage.includeCSS.file3 = typo3conf/ext/nG6/template/css/style.css\r\n[browser = msie] && [version = <7]\r\npage.includeCSS.file4 = typo3conf/ext/nG6/template/css/ie6.css\r\n[GLOBAL]\r\n\r\ntemp.MenuRechts = HMENU\r\ntemp.MenuRechts.maxItems = 5\r\ntemp.MenuRechts {\r\n        1 = TMENU\r\n        1 {\r\n                expAll =   1\r\n                wrap = <ul><li class="homeitem"><a href="/ng6"><img alt="homeitem" src="typo3conf/ext/nG6/template/images/picto-home.png"/></a></li>|</ul>\r\n                noBlur = 1\r\n                NO {\r\n                  wrapItemAndSub = <li>|</li>\r\n                  ATagParams = accesskey="1"||accesskey="2"||accesskey="3"||accesskey="4"||accesskey="5"||accesskey="6"||\r\n                  ATagTitle.field = description // title  \r\n                }\r\n                ACT=1\r\n                ACT {\r\n                  wrapItemAndSub = <li class="act">|</li>\r\n                  ATagParams = accesskey="1"||accesskey="2"||accesskey="3"||accesskey="4"||accesskey="5"||accesskey="6"||\r\n                  ATagTitle.field = description // title\r\n                }\r\n        }\r\n}\r\n\r\ntemp.MenuUnten=HMENU\r\ntemp.MenuUnten.special=directory\r\ntemp.MenuUnten.special.value=17\r\ntemp.MenuUnten.1=TMENU\r\ntemp.MenuUnten {\r\n        1 = TMENU\r\n        1 {\r\n                expAll = 1\r\n                wrap = <ul>|</ul>\r\n                noBlur = 1\r\n                NO {\r\n                        wrapItemAndSub = <li>|</li>|*|<li>&#124;|</li>|*||*|\r\n                        ATagTitle.field = description // title\r\n                }\r\n                ACT=1\r\n                ACT {\r\n                        wrapItemAndSub = <li>|</li>|*|<li>&#124;|</li>|*||*|\r\n                        ATagTitle.field = description // title\r\n                }\r\n        }\r\n}\r\n\r\npage.bodyTag = <body>\r\npage.1 = TEMPLATE\r\npage.1 {\r\n        template = FILE\r\n        template.file = typo3conf/ext/nG6/template/index.html\r\n        workOnSubpart = DOCUMENT\r\n        subparts.BROWSER < plugin.tx_nG6_pi4\r\n        subparts.CONTENT < styles.content.get\r\n        subparts.MENU < temp.MenuRechts\r\n        subparts.FOOTER_LINKS < temp.MenuUnten\r\n}\r\npage.config.doctype (\r\n<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\r\n)\r\npage.config.htmlTag_setParams = xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr"\r\n\r\n\r\n# nG6 config\r\nplugin.tx_nG6_pi1=USER_INT\r\nplugin.tx_nG6_pi1.userpidList=5\r\nplugin.tx_nG6_pi1.server_url=http://127.0.0.1:8080/\r\nplugin.tx_nG6_pi1.FromEmail=celine.noirot@toulouse.inra.Fr\r\nplugin.tx_nG6_pi1.data=/var/www/ng6/fileadmin\r\nplugin.tx_nG6_pi5.temp=/var/www/ng6/fileadmin/tmp\r\nplugin.tx_nG6_pi5.data=/var/www/ng6/fileadmin/data\r\nplugin.tx_nG6_pi5.server_name=127.0.0.1\r\nplugin.tx_nG6_pi5.directory_prefix=/home/jmariette/scratch/work\r\nplugin.tx_nG6_pi6.data=/var/www/ng6/fileadmin\r\nplugin.tx_nG6_pi6.server_url=http://127.0.0.1:8080/\r\nplugin.tx_felogin_pi1.redirectMode = login, logout\r\nplugin.tx_felogin_pi1.redirectPageLogin = 3\r\nplugin.tx_felogin_pi1.redirectPageLogout = 1\r\n', NULL, '', NULL, '', 0, 0, 0, 0),
(2, 2, 0, 0, 0, '', 0, 0, 0, 0, 0, 1381241411, 256, 1381240779, 1, '+ext', '', 0, 0, 0, 0, 0, 'EXT:css_styled_content/static/', NULL, 'plugin.tx_nG6_pi1.view = run', NULL, '', NULL, '', 0, 0, 0, 0),
(3, 3, 0, 0, 0, '', 0, 0, 0, 0, 0, 1381241424, 256, 1381240857, 1, '+ext', '', 0, 0, 0, 0, 0, 'EXT:css_styled_content/static/', NULL, NULL, NULL, '', NULL, '', 0, 0, 0, 0),
(4, 4, 0, 0, 0, '', 0, 0, 0, 0, 0, 1381241436, 256, 1381240888, 1, '+ext', '', 0, 0, 0, 0, 0, 'EXT:css_styled_content/static/', NULL, NULL, NULL, '', NULL, '', 0, 0, 0, 0),
(5, 6, 0, 0, 0, '', 0, 0, 0, 0, 0, 1382434121, 256, 1382434121, 1, '+ext', '', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, '', NULL, '', 0, 0, 0, 0);


INSERT INTO `sys_ter` (`uid`, `title`, `description`, `wsdl_url`, `mirror_url`, `lastUpdated`, `extCount`) VALUES
(1, 'TYPO3.org Main Repository', 'Main repository on typo3.org. This repository has some mirrors configured which are available with the mirror url.', 'http://typo3.org/wsdl/tx_ter_wsdl.php', 'http://repositories.typo3.org/mirrors.xml.gz', 0, 0);


INSERT INTO `tt_content` (`uid`, `pid`, `t3ver_oid`, `t3ver_id`, `t3ver_wsid`, `t3ver_label`, `t3ver_state`, `t3ver_stage`, `t3ver_count`, `t3ver_tstamp`, `t3ver_move_id`, `t3_origuid`, `tstamp`, `crdate`, `cruser_id`, `hidden`, `sorting`, `CType`, `header`, `header_position`, `bodytext`, `image`, `imagewidth`, `imageorient`, `imagecaption`, `imagecols`, `imageborder`, `media`, `layout`, `deleted`, `cols`, `records`, `pages`, `starttime`, `endtime`, `colPos`, `subheader`, `spaceBefore`, `spaceAfter`, `fe_group`, `header_link`, `imagecaption_position`, `image_link`, `image_zoom`, `image_noRows`, `image_effects`, `image_compression`, `altText`, `titleText`, `longdescURL`, `header_layout`, `text_align`, `text_face`, `text_size`, `text_color`, `text_properties`, `menu_type`, `list_type`, `table_border`, `table_cellspacing`, `table_cellpadding`, `table_bgColor`, `select_key`, `sectionIndex`, `linkToTop`, `filelink_size`, `target`, `section_frame`, `date`, `splash_layout`, `multimedia`, `image_frames`, `recursive`, `imageheight`, `rte_enabled`, `sys_language_uid`, `tx_impexp_origuid`, `pi_flexform`, `accessibility_title`, `accessibility_bypass`, `accessibility_bypass_text`, `l18n_parent`, `l18n_diffsource`) VALUES
(1, 1, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 1381240981, 1381240981, 1, 0, 256, 'login', '', '', NULL, NULL, 0, 0, NULL, 1, 0, NULL, 0, 0, 0, NULL, NULL, 0, 0, 0, '', 0, 0, '', '', '', NULL, 0, 0, 0, 0, NULL, NULL, NULL, '0', '', 0, 0, 0, 0, '0', '', 0, 0, 0, 0, '', 1, 0, 0, '', 0, 0, '0', NULL, 0, 0, 0, 0, 0, 0, '<?xml version="1.0" encoding="utf-8" standalone="yes" ?>\n<T3FlexForms>\n    <data>\n        <sheet index="sDEF">\n            <language index="lDEF">\n                <field index="showForgotPassword">\n                    <value index="vDEF">0</value>\n                </field>\n                <field index="showPermaLogin">\n                    <value index="vDEF">1</value>\n                </field>\n                <field index="showLogoutFormAfterLogin">\n                    <value index="vDEF">0</value>\n                </field>\n                <field index="pages">\n                    <value index="vDEF"></value>\n                </field>\n                <field index="recursive">\n                    <value index="vDEF"></value>\n                </field>\n                <field index="templateFile">\n                    <value index="vDEF"></value>\n                </field>\n            </language>\n        </sheet>\n        <sheet index="s_redirect">\n            <language index="lDEF">\n                <field index="redirectMode">\n                    <value index="vDEF"></value>\n                </field>\n                <field index="redirectFirstMethod">\n                    <value index="vDEF">0</value>\n                </field>\n                <field index="redirectPageLogin">\n                    <value index="vDEF"></value>\n                </field>\n                <field index="redirectPageLoginError">\n                    <value index="vDEF"></value>\n                </field>\n                <field index="redirectPageLogout">\n                    <value index="vDEF"></value>\n                </field>\n                <field index="redirectDisable">\n                    <value index="vDEF">0</value>\n                </field>\n            </language>\n        </sheet>\n        <sheet index="s_messages">\n            <language index="lDEF">\n                <field index="welcome_header">\n                    <value index="vDEF"></value>\n                </field>\n                <field index="welcome_message">\n                    <value index="vDEF"></value>\n                </field>\n                <field index="success_header">\n                    <value index="vDEF"></value>\n                </field>\n                <field index="success_message">\n                    <value index="vDEF"></value>\n                </field>\n                <field index="error_header">\n                    <value index="vDEF"></value>\n                </field>\n                <field index="error_message">\n                    <value index="vDEF"></value>\n                </field>\n                <field index="status_header">\n                    <value index="vDEF"></value>\n                </field>\n                <field index="status_message">\n                    <value index="vDEF"></value>\n                </field>\n                <field index="logout_header">\n                    <value index="vDEF"></value>\n                </field>\n                <field index="logout_message">\n                    <value index="vDEF"></value>\n                </field>\n                <field index="forgot_header">\n                    <value index="vDEF"></value>\n                </field>\n                <field index="forgot_reset_message">\n                    <value index="vDEF"></value>\n                </field>\n            </language>\n        </sheet>\n    </data>\n</T3FlexForms>', '', 0, '', 0, 0x613a31393a7b733a353a224354797065223b4e3b733a363a22636f6c506f73223b4e3b733a31363a227379735f6c616e67756167655f756964223b4e3b733a363a22686561646572223b4e3b733a31333a226865616465725f6c61796f7574223b4e3b733a31353a226865616465725f706f736974696f6e223b4e3b733a343a2264617465223b4e3b733a31313a226865616465725f6c696e6b223b4e3b733a31313a2270695f666c6578666f726d223b4e3b733a363a2268696464656e223b4e3b733a31323a2273656374696f6e496e646578223b4e3b733a393a226c696e6b546f546f70223b4e3b733a393a22737461727474696d65223b4e3b733a373a22656e6474696d65223b4e3b733a383a2266655f67726f7570223b4e3b733a363a226c61796f7574223b4e3b733a31313a2273706163654265666f7265223b4e3b733a31303a2273706163654166746572223b4e3b733a31333a2273656374696f6e5f6672616d65223b4e3b7d),
(2, 2, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 1381241001, 1381241001, 1, 0, 256, 'list', '', '', NULL, NULL, 0, 0, NULL, 1, 0, NULL, 0, 0, 0, NULL, '', 0, 0, 0, '', 0, 0, '', '', '', NULL, 0, 0, 0, 0, NULL, NULL, NULL, '0', '', 0, 0, 0, 0, '0', 'nG6_pi1', 0, 0, 0, 0, '', 1, 0, 0, '', 0, 0, '0', NULL, 0, 0, 0, 0, 0, 0, NULL, '', 0, '', 0, 0x613a32303a7b733a353a224354797065223b4e3b733a363a22636f6c506f73223b4e3b733a31363a227379735f6c616e67756167655f756964223b4e3b733a363a22686561646572223b4e3b733a31333a226865616465725f6c61796f7574223b4e3b733a31353a226865616465725f706f736974696f6e223b4e3b733a343a2264617465223b4e3b733a31313a226865616465725f6c696e6b223b4e3b733a393a226c6973745f74797065223b4e3b733a31313a2273706163654265666f7265223b4e3b733a31303a2273706163654166746572223b4e3b733a31333a2273656374696f6e5f6672616d65223b4e3b733a363a2268696464656e223b4e3b733a31323a2273656374696f6e496e646578223b4e3b733a393a226c696e6b546f546f70223b4e3b733a393a22737461727474696d65223b4e3b733a373a22656e6474696d65223b4e3b733a383a2266655f67726f7570223b4e3b733a353a227061676573223b4e3b733a393a22726563757273697665223b4e3b7d),
(3, 3, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 1381241011, 1381241011, 1, 0, 256, 'list', '', '', NULL, NULL, 0, 0, NULL, 1, 0, NULL, 0, 0, 0, NULL, '', 0, 0, 0, '', 0, 0, '', '', '', NULL, 0, 0, 0, 0, NULL, NULL, NULL, '0', '', 0, 0, 0, 0, '0', 'nG6_pi1', 0, 0, 0, 0, '', 1, 0, 0, '', 0, 0, '0', NULL, 0, 0, 0, 0, 0, 0, NULL, '', 0, '', 0, 0x613a32303a7b733a353a224354797065223b4e3b733a363a22636f6c506f73223b4e3b733a31363a227379735f6c616e67756167655f756964223b4e3b733a363a22686561646572223b4e3b733a31333a226865616465725f6c61796f7574223b4e3b733a31353a226865616465725f706f736974696f6e223b4e3b733a343a2264617465223b4e3b733a31313a226865616465725f6c696e6b223b4e3b733a393a226c6973745f74797065223b4e3b733a31313a2273706163654265666f7265223b4e3b733a31303a2273706163654166746572223b4e3b733a31333a2273656374696f6e5f6672616d65223b4e3b733a363a2268696464656e223b4e3b733a31323a2273656374696f6e496e646578223b4e3b733a393a226c696e6b546f546f70223b4e3b733a393a22737461727474696d65223b4e3b733a373a22656e6474696d65223b4e3b733a383a2266655f67726f7570223b4e3b733a353a227061676573223b4e3b733a393a22726563757273697665223b4e3b7d),
(4, 4, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 1381241023, 1381241023, 1, 0, 256, 'list', '', '', NULL, NULL, 0, 0, NULL, 1, 0, NULL, 0, 0, 0, NULL, '', 0, 0, 0, '', 0, 0, '', '', '', NULL, 0, 0, 0, 0, NULL, NULL, NULL, '0', '', 0, 0, 0, 0, '0', 'nG6_pi5', 0, 0, 0, 0, '', 1, 0, 0, '', 0, 0, '0', NULL, 0, 0, 0, 0, 0, 0, NULL, '', 0, '', 0, 0x613a32303a7b733a353a224354797065223b4e3b733a363a22636f6c506f73223b4e3b733a31363a227379735f6c616e67756167655f756964223b4e3b733a363a22686561646572223b4e3b733a31333a226865616465725f6c61796f7574223b4e3b733a31353a226865616465725f706f736974696f6e223b4e3b733a343a2264617465223b4e3b733a31313a226865616465725f6c696e6b223b4e3b733a393a226c6973745f74797065223b4e3b733a31313a2273706163654265666f7265223b4e3b733a31303a2273706163654166746572223b4e3b733a31333a2273656374696f6e5f6672616d65223b4e3b733a363a2268696464656e223b4e3b733a31323a2273656374696f6e496e646578223b4e3b733a393a226c696e6b546f546f70223b4e3b733a393a22737461727474696d65223b4e3b733a373a22656e6474696d65223b4e3b733a383a2266655f67726f7570223b4e3b733a353a227061676573223b4e3b733a393a22726563757273697665223b4e3b7d),
(5, 6, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 1382434137, 1382434137, 1, 0, 256, 'list', '', '', NULL, NULL, 0, 0, NULL, 1, 0, NULL, 0, 0, 0, NULL, '', 0, 0, 0, '', 0, 0, '', '', '', NULL, 0, 0, 0, 0, NULL, NULL, NULL, '0', '', 0, 0, 0, 0, '0', 'nG6_pi6', 0, 0, 0, 0, '', 1, 0, 0, '', 0, 0, '0', NULL, 0, 0, 0, 0, 0, 0, NULL, '', 0, '', 0, 0x613a32303a7b733a353a224354797065223b4e3b733a363a22636f6c506f73223b4e3b733a31363a227379735f6c616e67756167655f756964223b4e3b733a363a22686561646572223b4e3b733a31333a226865616465725f6c61796f7574223b4e3b733a31353a226865616465725f706f736974696f6e223b4e3b733a343a2264617465223b4e3b733a31313a226865616465725f6c696e6b223b4e3b733a393a226c6973745f74797065223b4e3b733a31313a2273706163654265666f7265223b4e3b733a31303a2273706163654166746572223b4e3b733a31333a2273656374696f6e5f6672616d65223b4e3b733a363a2268696464656e223b4e3b733a31323a2273656374696f6e496e646578223b4e3b733a393a226c696e6b546f546f70223b4e3b733a393a22737461727474696d65223b4e3b733a373a22656e6474696d65223b4e3b733a383a2266655f67726f7570223b4e3b733a353a227061676573223b4e3b733a393a22726563757273697665223b4e3b7d);

INSERT INTO `tx_nG6_project` (`uid`, `pid`, `tstamp`, `crdate`, `cruser_id`, `deleted`, `hidden`, `public`, `name`, `description`) VALUES
(1, 8, 1386341206, 1386341206, 1, 0, 0, 0, 'project_install', 'project_install');

--
-- Structure de la vue `tx_nG6_view_project_analyze`
--
CREATE ALGORITHM=UNDEFINED DEFINER=`ng6`@`localhost` SQL SECURITY DEFINER VIEW `tx_nG6_view_project_analyze` AS select `tx_nG6_project`.`uid` AS `project_id`,`tx_nG6_project`.`name` AS `project_name`,`tx_nG6_project`.`space_id` AS `space_id`,`tx_nG6_analyze`.`uid` AS `analyze_id`,`tx_nG6_analyze`.`storage_size` AS `storage_size`,`tx_nG6_analyze`.`purged_size` AS `purged_size`,`tx_nG6_analyze`.`data_state` AS `state`,`tx_nG6_analyze`.`retention_date` AS `retention_date`,`tx_nG6_analyze`.`purge_demand_id` AS `purge_demand_id`,`tx_nG6_analyze`.`hidden` AS `hidden` from ((`tx_nG6_project` join `tx_nG6_project_analyze` on((`tx_nG6_project_analyze`.`project_id` = `tx_nG6_project`.`uid`))) join `tx_nG6_analyze` on((`tx_nG6_project_analyze`.`analyze_id` = `tx_nG6_analyze`.`uid`)));

--
-- Structure de la vue `tx_nG6_view_project_run`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`ng6`@`localhost` SQL SECURITY DEFINER VIEW `tx_nG6_view_project_run` AS select `tx_nG6_project`.`uid` AS `project_id`,`tx_nG6_project`.`name` AS `project_name`,`tx_nG6_project`.`space_id` AS `space_id`,`tx_nG6_run`.`uid` AS `run_id`,`tx_nG6_run`.`storage_size` AS `storage_size`,`tx_nG6_run`.`purged_size` AS `purged_size`,`tx_nG6_run`.`data_state` AS `state`,`tx_nG6_run`.`retention_date` AS `retention_date`,`tx_nG6_run`.`purge_demand_id` AS `purge_demand_id`,`tx_nG6_run`.`hidden` AS `hidden` from ((`tx_nG6_run` join `tx_nG6_project_run` on((`tx_nG6_project_run`.`run_id` = `tx_nG6_run`.`uid`))) join `tx_nG6_project` on((`tx_nG6_project`.`uid` = `tx_nG6_project_run`.`project_id`)));

--
-- Structure de la vue `tx_nG6_view_project_run_analyze`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`ng6`@`localhost` SQL SECURITY DEFINER VIEW `tx_nG6_view_project_run_analyze` AS select `tx_nG6_project`.`uid` AS `project_id`,`tx_nG6_project`.`name` AS `project_name`,`tx_nG6_project`.`space_id` AS `space_id`,`tx_nG6_analyze`.`uid` AS `analyze_id`,`tx_nG6_analyze`.`storage_size` AS `storage_size`,`tx_nG6_analyze`.`purged_size` AS `purged_size`,`tx_nG6_analyze`.`data_state` AS `state`,`tx_nG6_analyze`.`retention_date` AS `retention_date`,`tx_nG6_analyze`.`purge_demand_id` AS `purge_demand_id`,`tx_nG6_analyze`.`hidden` AS `hidden` from ((((`tx_nG6_run` join `tx_nG6_project_run` on((`tx_nG6_project_run`.`run_id` = `tx_nG6_run`.`uid`))) join `tx_nG6_project` on((`tx_nG6_project`.`uid` = `tx_nG6_project_run`.`project_id`))) join `tx_nG6_run_analyze` on((`tx_nG6_run_analyze`.`run_id` = `tx_nG6_run`.`uid`))) join `tx_nG6_analyze` on((`tx_nG6_run_analyze`.`analyze_id` = `tx_nG6_analyze`.`uid`)));

--
-- Structure de la vue `tx_nG6_view_project_user`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`ng6`@`localhost` SQL SECURITY DEFINER VIEW `tx_nG6_view_project_user` AS select `tx_nG6_project`.`uid` AS `project_id`,`tx_nG6_project`.`space_id` AS `space_id`,`fe_users`.`uid` AS `user_id`, `fe_users`.`username` AS `user_name`,`fe_users`.`email` AS `email`,`fe_users`.`usergroup` AS `user_group`,`fe_rights_levels`.`right_level_label` AS `right_level_label`,`fe_groups`.`title` AS `user_group_title` from ((((`tx_nG6_project` join `fe_rights` on((`fe_rights`.`project_id` = `tx_nG6_project`.`uid`))) join `fe_users` on((`fe_rights`.`fe_user_id` = `fe_users`.`uid`))) join `fe_groups` on((`fe_users`.`usergroup` = `fe_groups`.`uid`))) join `fe_rights_levels` on((`fe_rights_levels`.`right_level_id` = `fe_rights`.`right_id`)));

--
-- Trigger check_demand_insert to prevent the insertion of a second purge demand for a single project
--

DELIMITER ;;

CREATE TRIGGER check_demand_insert BEFORE INSERT ON `tx_nG6_purge_demand`
FOR EACH ROW
BEGIN

DECLARE var_demand_state, var_project_id, var_count int;

SELECT demand_state, project_id, count(*) as count
INTO var_demand_state, var_project_id, var_count
FROM tx_nG6_purge_demand
WHERE demand_state = 'sent' AND project_id = new.project_id 
GROUP BY demand_state, project_id;

IF var_count > 0 THEN
set @message_text = concat('A purge demand already exists for project ', new.project_id);
signal sqlstate '45000' 
set message_text = @message_text;
END IF;

END;
;; 



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
