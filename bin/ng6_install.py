#
# Copyright (C) 2012 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import sys
import argparse
import time
from configparser import ConfigParser
import tempfile, os, sys, re, hashlib, urllib.request, urllib.parse, urllib.error, zipfile
import distutils.dir_util as dirutil
import shutil
import uuid
    
try:
    import _preamble
except ImportError:
    sys.exc_clear()


from ng6.t3MySQLdb import t3MySQLdb


if __name__ == '__main__':

    # Create the top-level parser
    parser = argparse.ArgumentParser()
    parser.add_argument("--web-path", type=str, help="Where should be stored the ng6 web site (recommended within your apache directory)",
                        default="/var/www/ng6", dest="web_path")
    parser.add_argument("--email", type=str, help="",
                        required=True, dest="email")
    args = vars(parser.parse_args())
    args["web_path"] = re.sub( r"\/$", "", args["web_path"] )
    
    print("Downloading web site source code ...")
    dummy_tmp_zipfile = tempfile.NamedTemporaryFile(prefix = 'dummy_', suffix = '.zip').name
    dummy_tmp_dir = tempfile.mkdtemp(suffix='_typo3', prefix='dummy_')
    ng6reader = ConfigParser()
    ng6reader.read(os.path.join(os.path.dirname(__file__), "..", "application.properties")) 
    db_host, db_user, db_password, db_name = ng6reader.get('database', 'host'), ng6reader.get('database', 'user'), ng6reader.get('database', 'passwd'), ng6reader.get('database', 'dbname') 
    server_parameters = [ng6reader.get('global', 'server_socket_host'), ng6reader.get('global', 'server_socket_port')]
    
    ng6_path = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
    db_path = os.path.join(ng6_path, "bin", "ng6_database.sql")
    fileadmin = os.path.join(args["web_path"], "fileadmin")
    
    # Downloading typo3 package
    urllib.request.urlretrieve(ng6reader.get('resources', 'typo3_src'), dummy_tmp_zipfile)
     
    print("Installing web site ...")
    # Testing and Unzipping typo3 package 
    with zipfile.ZipFile(dummy_tmp_zipfile, 'r') as zipf:
        if zipf.testzip() is None:
            zipf.extractall(dummy_tmp_dir)
     
    try:
        # Copy typo3 folder to web folder
        if os.path.exists(args["web_path"]) is False:
            dirutil.copy_tree(os.path.join(dummy_tmp_dir, os.path.basename(ng6reader.get('resources', 'typo3_src'))[:-4]), args["web_path"], preserve_mode=1)
        else: 
            sys.stderr.write("Error: The given www-path already exists ! (" + args["web_path"] + ")\n")
            sys.exit()
    except:
        sys.stderr.write("Error: sudo privileges are required to install the software in '" + args["web_path"] + "'!\n")
        sys.exit()
        
    # Copy nG6 folders
    dirutil.copy_tree(os.path.join(ng6_path, "ui", "nG6"), os.path.join(args["web_path"], "typo3conf", "ext", "nG6"))
    
    os.symlink(os.path.join(ng6reader.get("storage", "save_directory"), "data"), 
               os.path.join(args["web_path"], "fileadmin", "data"))
    
    shutil.copyfile(os.path.join(ng6_path, "ui", "felogin.html"), 
                    os.path.join(args["web_path"], "typo3", "sysext", "felogin", "template.html"))
    
    # Create config file
    with open(os.path.join(args["web_path"],"typo3conf", "localconf.php")) as f:
        config_lines= ''.join(f.readlines()[:-1])
    with open(os.path.join(args["web_path"], "typo3conf", "localconf.php"),"w") as f:
        f.write(config_lines)
        # Set config variables for typo3 localconf.php
        f.write("$TYPO3_CONF_VARS['EXT']['extList'] = 'extbase,css_styled_content,info,perm,func,filelist,fluid,about,version,tsconfig_help,context_help,extra_page_cm_options,impexp,sys_note,tstemplate,tstemplate_ceditor,tstemplate_info,tstemplate_objbrowser,tstemplate_analyzer,func_wizards,wizard_crpages,wizard_sortpages,lowlevel,install,belog,beuser,aboutmodules,setup,taskcenter,info_pagetsconfig,viewpage,rtehtmlarea,t3skin,t3editor,reports,felogin,form,rsaauth,saltedpasswords,nG6';\n")
        f.write("$TYPO3_CONF_VARS['SYS']['compat_version'] = '4.7';\n")
        f.write("$typo_db_username = '" + db_user + "';\n")
        f.write("$typo_db_password = '" + db_password + "';\n")
        f.write("$typo_db_host = '" + db_host + "';\n")
        f.write("$typo_db = '" + db_name + "';\n") # It's risky to use this with the typo3 installer
        f.write("$TYPO3_CONF_VARS['BE']['loginSecurityLevel']  = 'rsa';\n")
        f.write("$TYPO3_CONF_VARS['FE']['loginSecurityLevel']  = 'rsa';\n")
        f.write("$TYPO3_CONF_VARS['EXT']['extList_FE'] = 'extbase,css_styled_content,fluid,version,install,rtehtmlarea,t3skin,felogin,form,rsaauth,saltedpasswords,nG6';\n")
        f.write("""$TYPO3_CONF_VARS['EXT']['extConf']['saltedpasswords'] = 'a:2:{s:3:"FE.";a:2:{s:7:"enabled";s:1:"1";s:21:"saltedPWHashingMethod";s:33:"tx_saltedpasswords_salts_blowfish";}s:3:"BE.";a:2:{s:7:"enabled";s:1:"1";s:21:"saltedPWHashingMethod";s:33:"tx_saltedpasswords_salts_blowfish";}}';\n""")
        f.write("$TYPO3_CONF_VARS['BE']['disable_exec_function'] = '0';\n")
        f.write("$TYPO3_CONF_VARS['GFX']['gdlib_png'] = '0';\n")
        f.write("$TYPO3_CONF_VARS['GFX']['im_combine_filename'] = 'composite';\n")
        f.write("$TYPO3_CONF_VARS['GFX']['im_combine_filename'] = 'composite';\n")
        f.write("$TYPO3_CONF_VARS['EXT']['extConf']['ng6']['email_from'] = '" + args["email"] + "';\n")
        f.write("$TYPO3_CONF_VARS['EXT']['extConf']['ng6']['delay_purge'] = '" + ng6reader.get('email', 'delay_purge') + "';\n")
        f.write("$TYPO3_CONF_VARS['EXT']['extConf']['ng6']['min_extension_duration'] = '" + ng6reader.get('email', 'min_extension_duration') + "';\n")
        f.write("$TYPO3_CONF_VARS['EXT']['extConf']['ng6']['min_extension_size'] = '" + ng6reader.get('email', 'min_extension_size') + "';\n")
        f.write("$TYPO3_CONF_VARS['EXT']['extConf']['ng6']['min_extension_price'] = '" + ng6reader.get('email', 'min_extension_price') + "';\n")
        f.write("$TYPO3_CONF_VARS['EXT']['extConf']['ng6']['url_extension_price'] = '" + ng6reader.get('email', 'url_extension_price') + "';\n")

        # Typo3 password hash
        random=uuid.uuid4().hex[:10]
        install_tool_password_hash = hashlib.md5(random.encode('utf-8')).hexdigest()
        f.write("# password : "+random+";\n")
        f.write("$TYPO3_CONF_VARS['BE']['installToolPassword'] = '" + install_tool_password_hash + "';\n")
        # Testing unzip binary path :
        f.write("$TYPO3_CONF_VARS['BE']['unzip_path'] = 'unzip';\n")
        f.write("?>\n")
        
    #template html
    with open( os.path.join(args["web_path"], "typo3conf", "ext", "nG6", "template", "index.html")) as f:
        template = f.readlines()
    with open(os.path.join(args["web_path"],  "typo3conf", "ext", "nG6", "template", "index.html"),"w") as f:
        for line in template:
            line = re.sub("<li><a href=\"mailto:@\">Contact us</a></li>", "<li><a href=\"mailto:"+args["email"]+"\">Contact us</a></li>" + "\">",line)
            f.write(line)
        # Set config variables for typo3 localconf.php
        f.write("?>\n")
    with open(db_path,"r") as f:
        database = f.readlines()
    for iline in range(0, len(database)):
        database[iline] = re.sub("class=\"homeitem\"><a href=\"([\w/]+)\">", "class=\"homeitem\"><a href=\"/" + os.path.basename(args["web_path"]) + "\">",database[iline])
        database[iline] = re.sub("plugin.tx_nG6_pi1.data=([\w/.-]+)", "plugin.tx_nG6_pi1.data=" + fileadmin, database[iline])
        database[iline] = re.sub("plugin.tx_nG6_pi1.FromEmail=[\w\.]+\@[\w\.]+", "plugin.tx_nG6_pi1.FromEmail=" + args["email"], database[iline])
        database[iline] = re.sub("plugin.tx_nG6_pi5.temp=([\w/.-]+)", "plugin.tx_nG6_pi5.temp=" + fileadmin + "/tmp", database[iline])
        database[iline] = re.sub("plugin.tx_nG6_pi5.data=([\w/.-]+)", "plugin.tx_nG6_pi5.data=" + fileadmin + "/data", database[iline])
        database[iline] = re.sub("plugin.tx_nG6_pi5.directory_prefix=([\w/.-]+)", "plugin.tx_nG6_pi5.directory_prefix=" + ng6reader.get("storage", "work_directory"), database[iline])
        database[iline] = re.sub("plugin.tx_nG6_pi6.data=([\w/.-]+)", "plugin.tx_nG6_pi6.data=" + fileadmin, database[iline])
        database[iline] = re.sub("http://([\w\.]+):(\d+)", "http://" +  server_parameters[0] + ":" + server_parameters[1], database[iline])
        database[iline] = re.sub("plugin.tx_nG6_pi5.server_name=([\w\.]+)", "plugin.tx_nG6_pi5.server_name=" + server_parameters[0], database[iline])
            
    # Inserting tables in typo3 database
    t3m = t3MySQLdb()
    connection = t3m.get_connection()
    try :
        with connection.cursor() as curs:
            curs.execute("show tables;")
            tables = []
            for i in curs.fetchall() : tables.append(i[0])
            erase = "n"
            if len(tables) > 0 and "tx_nG6_project" in tables :
                erase = input('The database already contains a ng6 web site, do you want to DROP it (y/n) ? ')
            elif len(tables) > 0 and ("tt_content" in tables and "pages" in tables and "be_users" in tables ): 
                erase = input('The database already contains a ng6 web site, do you want to DROP it (y/n) ? ')
            if erase == "y" or erase == "Y" :
                curs.execute("Drop table "+ ",".join(tables) +";")
            curs.execute("".join(database))
        connection.commit()
    except :
        raise
    finally:
        connection.close()
    
    # Fix permissions typo3 folder
    for dir in [os.path.join(args["web_path"], "typo3conf"), 
                os.path.join(args["web_path"],"fileadmin"), 
                os.path.join(args["web_path"], "typo3temp"), 
                os.path.join(args["web_path"], "uploads")]:
        os.chmod(dir, 0o777)
    
    for r,d,f in os.walk(os.path.join(args["web_path"], "typo3conf", "ext", "nG6", "res")):
        os.chmod( r , 0o777)
    
    web_name = args["web_path"].split("/var/www/")
    print("The Web site is available at http://"+server_parameters[0]+"/"+web_name[1]) 
    print("Go to http://"+server_parameters[0]+"/"+web_name[1]+"/index.php?id=3 to configure the installation")
     
