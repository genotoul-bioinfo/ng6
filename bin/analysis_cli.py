#
# Copyright (C) 2015 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys
import argparse
import time

try:
    import _preamble
except ImportError:
    sys.exc_clear()

from ng6.analyzes_manager import AnalyzesManager
from ng6_cli import JflowArgumentParser
from jflow.workflow import Workflow
import jflow.utils as utils

if __name__ == '__main__':

    # Create a workflow manager to get access to our workflows
    wfmanager = AnalyzesManager()
    
    # Create the top-level parser
    parser = JflowArgumentParser()
    subparsers = parser.add_subparsers(title='Available sub commands')
    
    # Add available pipelines
    wf_instances, wf_methodes = wfmanager.get_available_workflows()
    wf_classes = []
    for instance in wf_instances:
        wf_classes.append(instance.get_classname())
        # create the subparser for each applications
        sub_parser = subparsers.add_parser(instance.name, help=instance.description, fromfile_prefix_chars='@')
        sub_parser.convert_arg_line_to_args = instance.__class__.config_parser
        [parameters_groups, parameters_order] = instance.get_parameters_per_groups()
        for group in parameters_order:
            if group == "default":
                for param in parameters_groups[group]:
                    sub_parser.add_argument(param.flag, **param.export_to_argparse())
            elif group.startswith("exclude-"):
                is_required = False
                for param in parameters_groups[group]:
                    if param.required:
                        is_required = True
                        # an exlcusive parameter cannot be required, the require is at the group level
                        param.required = False
                pgroup = sub_parser.add_mutually_exclusive_group(required=is_required)
                for param in parameters_groups[group]:
                    pgroup.add_argument(param.flag, **param.export_to_argparse())
            else:
                pgroup = sub_parser.add_argument_group(group)
                for param in parameters_groups[group]:
                    pgroup.add_argument(param.flag, **param.export_to_argparse())
        sub_parser.set_defaults(cmd_object=instance.get_classname())
    args = vars(parser.parse_args())
    
    if args["cmd_object"] in wf_classes:
        wfmanager.run_workflow(args["cmd_object"], args)
    