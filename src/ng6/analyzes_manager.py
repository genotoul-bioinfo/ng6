#
# Copyright (C) 2015 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import pkgutil
import inspect
import workflows.components
import sys

from . import analysis
from . import ng6workflow

from jflow.workflows_manager import ComponentsManager

class AnalyzesManager(ComponentsManager):
    
    def __init__(self):
        ComponentsManager.__init__(self)
    
    def get_available_workflows(self, function="process"):
        instances, methodes = ComponentsManager.get_available_workflows(self)
        wf_instances, wf_methodes = [], []
        
        for i, wfinstance in enumerate(instances) :
            if issubclass(wfinstance.get_comp_object(), analysis.Analysis):
                wf_instances.append(ng6workflow.AnalysisWorkflow(wfinstance.get_comp_object(), function=methodes[i]))
                wf_methodes.append(methodes[i])

        return [wf_instances, wf_methodes]
        
    def run_workflow(self, workflow_class, args, function="process"):
        # load all analyzes from workflows.components
        for importer, modname, ispkg in pkgutil.iter_modules(workflows.components.__path__, workflows.components.__name__ + "."):
            __import__(modname)
            for class_name, comp_obj in inspect.getmembers(sys.modules[modname], inspect.isclass):
                if issubclass(comp_obj, analysis.Analysis) and comp_obj.__name__ !=  analysis.Analysis.__name__:
                    if class_name == workflow_class:
                        workflow = ng6workflow.AnalysisWorkflow(comp_obj, args, self.get_next_id(), function)
        workflow.start()
        # Add the workflow dump path to the workflows dump
        self._dump_workflows([workflow])
        return workflow
    