#
# Copyright (C) 2009 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Plateforme bioinformatique Midi Pyrenees'
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'beta'


class UnsavedRunError(Exception):
    """
    Exception to be raised when an operation is performed and the run has not been saved yet
    """
    def __init__(self, message=None):
        if message is not None:
            self.message = message
        else:
            self.message = "The run has to be saved before to call this method."

    def __str__(self):
        return repr(self.message)
    
    
class UnsavedAnalysisError(Exception):
    """
    Exception to be raised when an operation is performed and the analysis has not been saved yet
    """
    def __init__(self, message=None):
        if message is not None:
            self.message = message
        else:
            self.message = "The analysis has to be saved before to call this method."

    def __str__(self):
        return repr(self.message)