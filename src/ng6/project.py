#
# Copyright (C) 2009 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Plateforme bioinformatique Midi Pyrenees'
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'beta'

import logging, tempfile
from configparser import ConfigParser, RawConfigParser

from ng6.t3MySQLdb import t3MySQLdb
from ng6.run import Run
from ng6.analysis import Analysis


class Project(object):
    """
    Class Project: Define a nG6 Project object
    """

    def __init__(self, name, description, admin_login=None, id=None, space_id="default"):
        """
        Build a Project object
          @param name                  : the project name
          @param description           : the project description
          @param admin_login           : the administrator login
          @param id                    : the project id
        """
        if id == None : # If brand new run
            if admin_login != None:
                t3mysql = t3MySQLdb()
                self.id = t3mysql.add_project(name, description, admin_login, space_id)
            else:
                raise ValueError("An admin login is required to create a new project!")
        else :
            self.id = id
        self.name = name
        self.description = description
        self.space_id=space_id


    def get_name(self):
        """
        Return the project name
        """
        return self.name

    def get_runs(self):
        """
        Return a table of Run object that belongs to the project
        """
        t3mysql = t3MySQLdb()
        runs = []
        run_ids = t3mysql.get_project_runs_ids(self.id)
        # For each analysis
        for run_id in run_ids:
            runs.append(Run.get_from_id(run_id))
        return runs

    def add_analysis(self, my_analysis):
        """
        Add the analysis to the project
          @param my_analysis  : the analysis to add
        """
        analysis_id = my_analysis.save()
        t3mysql = t3MySQLdb()
        t3mysql.add_analysis_to_project(self.id, analysis_id)

    def add_run(self, my_run):
        """
        Add and save the run to the project
          @param my_run  : the run to add
        """
        run_id = my_run.save()
        my_run.id = run_id
        t3mysql = t3MySQLdb()
        t3mysql.add_run_to_project(self.id, run_id)

    def get_analysis(self):
        """
        Return a table of Analysis object that belongs to the project
        """
        t3mysql = t3MySQLdb()
        analysis = []
        analysis_ids = t3mysql.get_project_analysis_ids(self.id)
        # For each analysis
        for analysis_id in analysis_ids:
            analysis.append(Analysis.get_from_id(analysis_id))
        return analysis

    def sync(self):
        """
        Synchronyze data related analysis belonging to the Project between temporary folder and long term storage folder.
        """
        logging.getLogger("Project.sync").debug("Synchronizing project id=" + str(self.id))
        try :
            for my_analysis in self.get_analysis():
                my_analysis.sync()
        except Exception as err:
            logging.getLogger("Project.sync").error("Error when synchronizing analysis that belongs to project id=" + str(self.id))
            raise Exception(str(err))

    def get_config_file(self):
        """
        Return a Project config file
          @return : path the the config file
        """
        config = RawConfigParser()
        config.add_section("Project")
        config.set("Project", "project_id", self.id)
        config.set("Project", "name", self.name)
        config.set("Project", "description", self.description)

        config_path = tempfile.NamedTemporaryFile(suffix=".cfg").name
        config.write(open(config_path,'w'))
        return config_path

    def is_admin(self, admin_login):
        """
        Return true/false
          @param admin_user: login of the user
          @return : true is admin_login is in admin user of the project
        """
        t3mysql = t3MySQLdb()
        user_ids = t3mysql.get_users(self.id,"administrator")
        my_user_id = t3mysql.get_user_id(admin_login)
        if my_user_id in user_ids :
            return True
        else :
            return False

    @staticmethod
    def get_from_id (id):
        """
        Return a project object specified by its id
          @param id : the project id
        """
        try:
            t3mysql = t3MySQLdb()
            [name, description, space_id] = t3mysql.select_project(id)
            return Project(name, description, None, id, space_id)
        except :
            return None

    @staticmethod
    def get_from_name (name):
        """
        Return a project object specified by its name
          @param name : the project name
        """
        try:
            t3mysql = t3MySQLdb()
            [id, description, space_id] = t3mysql.select_project_from_name(name)
            return Project(name, description, None, id, space_id)
        except :
            return None

    @staticmethod
    def get_from_run_id (id):
        """
        Return a project object specified by run id
          @param id : the run id
        """
        try:
            t3mysql = t3MySQLdb()
            id = t3mysql.select_project_id_from_run_id(id)
            return Project.get_from_id(id)
        except :
            return None
    
    def update_space_id (self, space_id):
        """
        Updates a project's space_id field
          @param space_id : the new project space_id
        """  
        if self.id != None :
            t3mysql = t3MySQLdb()
            t3mysql.update_fields('tx_nG6_project', str(self.id), ['space_id'], [str(space_id)] )
    
    def get_project_run_ids (self):
        """
        Retrieve a project's runs ids
          @return : a table of run ids
        """
        if self.id != None :
            t3mysql = t3MySQLdb()
            return t3mysql.get_project_runs_ids(self.id)
        else:
            return None
    
    def get_project_analyzes_ids (self):
        """
        Retrieve a project's analyzes ids
          @return : a table of analyzes ids
        """
        if self.id != None :
            t3mysql = t3MySQLdb()
            return t3mysql.get_project_analysis_ids(self.id)
        else:
            return None
        
    def get_project_runs_analyzes_ids (self):
        """
        Retrieve a project's runs's analyzes ids
          @return : a table of analyzes ids
        """
        if self.id != None :
            t3mysql = t3MySQLdb()
            return t3mysql.get_run_analyzes_id_from_project(self.id)
        else:
            return None    