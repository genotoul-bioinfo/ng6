#
# Copyright (C) 2009 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Plateforme bioinformatique Midi Pyrenees'
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'beta'

import os
from configparser import ConfigParser
from .utils import *
import logging

from dateutil.relativedelta import relativedelta
import datetime
import time

class NG6ConfigReader(object):
    """
    Class NG6ConfigReader: this object read the config file and return the different configuration values
    """

    # The ng6 config file path
    CONFIG_FILE = os.path.join(os.path.dirname(__file__), "../../application.properties")
    PID = 5

    def __init__(self):
        """
        Build a ConfigReader
        """
        self.reader = ConfigParser()
        self.reader.read(self.CONFIG_FILE)


    def get_typo3_db_params(self):
        """
        return params needed to connect to the typo3 database
          @return: [host, user, passwd, dbname]
        """
        try:
            db_params = []
            db_params.append(self.reader.get('database', 'host'))
            db_params.append(self.reader.get('database', 'user'))
            db_params.append(self.reader.get('database', 'passwd'))
            db_params.append(self.reader.get('database', 'dbname'))
            return db_params
        except :
            raise Exception("Failed when parsing the config file - get_typo3_db_params !")


    def get_pid(self):
        """
        return the pid where to links data to
          @return: pid
        """
        return str(self.PID)


    def get_work_directory(self):
        """
        return the work directory
          @return: work_dir
        """
        try:
            return self.reader.get('storage', 'work_directory')
        except :
            raise Exception("Failed when parsing the config file - get_work_directory! ")


    def get_tmp_directory(self):
        """
        return the tmp directory
          @return: tmp_dir
        """
        try:
            return self.reader.get('storage', 'tmp_directory')
        except :
            raise Exception("Failed when parsing the config file get_tmp_directory !")


    def get_save_directory(self):
        """
        return the save directory
          @return: save_dir
        """
        try:
            return self.reader.get('storage', 'save_directory')
        except :
            raise Exception("Failed when parsing the config file get_save_directory !")

    def get_space_directory(self, space_id="default"):
        """
        return the directory corresponding to space_id
          @return: space_dir
        """
        try:
            return self.reader.get('space_'+space_id, 'dir_name').strip("/")
        except:
            raise Exception("Failed when parsing the config file !")

    def get_retention_date(self, space_id="default", creation_date = None):
        """
        return the retention corresponding to space_id
          @return: space_dir
        """
        
        date = None
        start_date= datetime.datetime.today()
        if creation_date is not None :
            start_date=datetime.datetime.fromtimestamp(creation_date)
            
        try :
            nb_month=self.reader.getint('space_'+space_id,"retention_period")
            retention_date = time.mktime((start_date+ relativedelta(months=nb_month)).timetuple())
            return retention_date
        except:
            try :
                (d,m,y)=self.reader.get('space_'+space_id,"retention_date").split('/')
                date = time.mktime(datetime.date(int(y),int(m),int(d)).timetuple())
                return date
            except:
                raise Exception("Failed when parsing the config file !")
        raise ValueError("Failed while generating retention date!")

    
    def get_available_spaces(self):
        """
        return a list of space_ids
          @return: a list containing the spaces strings 
        """
        available_spaces=[]
        for each_section in self.reader.sections():
            if each_section.startswith("space_") :
                available_spaces.append(each_section.replace('space_',''))        
        return available_spaces
    
    def get_log_file_path(self):
        """
        return the log file path
          @return:    the path to the log file
        """
        try:
            return self.reader.get('storage', 'log_file')
        except :
            raise Exception("Failed when parsing the config file !")
    def get_log_level(self):
        """
        return the level of log
          @return:    the level of logging (logging.DEBUG, logging.WARNING....
        """
        try:
            return self.reader.get('storage', 'log_level')
        except :
            raise Exception("Failed when parsing the config file !")


    def get_454_mids(self):
        """
        return the 454 mids list
          @return:    hash table with mids desriptions
        """
        try:
            mid_array = {}
            mids = self.reader.items("454_mids")
            for mid in mids:
                mid_array[mid[0]] = mid[1]
            return mid_array
        except :
            raise Exception("Failed when parsing the config file get_454_mids !")

    def get_10X_indexs(self):
        """
        return the 10X indexs list
            @return : hash table with 10X barcode list of index 
        """
        try:
            barcode_array = {}
            barcodes = self.reader.items("10X_barcodes")
            for barcode in barcodes:
                barcode_array[barcode[0].upper()] = barcode[1].upper()
            return barcode_array
        except : 
            raise Exception("Failed when parsin the config file for 10X barcodes !")

    def get_workflow_filters(self):
        """
            Return a list of workflow class names
            @return: list
        """
        filters = self.reader.get("local", 'runwf_filter')
        if filters :
            return filters.split()
        return []


    def get_user_base_directory(self):
        """
            Return an absolute path of a directory mounted on the web server
            @return: string
        """
        try:
            return self.reader.get('ui', 'user_base_directory')
        except :
            raise Exception("Could not retrieve server directory get_user_base_directory !")


    def get_server_parameters(self):
        """
        return the server parameters
          @return:    server_socket_host, server_socket_port, server_socket_protocol
        """
        try:
            server_params = []
            server_params.append(self.reader.get('global', 'server_socket_host'))
            server_params.append(self.reader.get('global', 'server_socket_port'))
            server_params.append(self.reader.get("global", 'server_socket_protocol'))
            return server_params
        except :
            raise Exception("Failed when parsing the config file get_server_parameters !")
        
