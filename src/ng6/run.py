#
# Copyright (C) 2009 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Plateforme bioinformatique Midi Pyrenees'
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'beta'

import os, uuid, logging, tempfile, re
import datetime, time
from subprocess import call
from subprocess import Popen, PIPE
from shutil import copyfile, rmtree
from configparser import ConfigParser, RawConfigParser

from jflow import seqio
from ng6.t3MySQLdb import t3MySQLdb
from ng6.config_reader import NG6ConfigReader
from ng6.utils import Utils
from ng6.exceptions import UnsavedRunError

class Run(object):
    """
    Class Run: Define a nG6 run object
    """

    # The directories structure into ng6
    DIRECTORIES_STRUCTURE = "run"
    SEQUENCERS = ["454", "454 GS FLX Titanium", "454 GS FLX+", "454 GS FLX XL+", "HiSeq 2000", "MiSeq"]

    def __init__(self, name, date, species, data_nature, type, description, sequencer, space_id="default",nglbi_run_code="", id = None ):
        """
        Build a Run object
          @param name            : the run name
          @param date            : the run data in a date format
          @param species         : the species used in the run
          @param data_nature     : the data nature used in the run
          @param type            : the type of run
          @param description     : the run description
          @param sequencer       : the sequencer used
          @param run_id          : the id of the run, if None a new one will be created
          @param nglbi_run_code  : the NGL-Bi run code and lane corresponding to the current NG6 run
        """
        self.name = name
        self.date = date
        self.species = species
        self.data_nature = data_nature
        self.type = type
        self.description = description
        self.sequencer = sequencer
        self.__samples = None
        self.raw_files = []
        self.admin_login = None
        self.space_id = space_id
        self.retention_date = None
        self.nglbi_run_code = nglbi_run_code
        logging.getLogger("Run").debug("__init, nglbi_run_code = "+str(nglbi_run_code))
        # Set the temp folder to the ng6 temp folder
        ng6conf = NG6ConfigReader()
        tempfile.tempdir = ng6conf.get_tmp_directory()
        if not os.path.isdir(tempfile.tempdir):
            os.makedirs(tempfile.tempdir, 0o751)

        if id != None : # If not a new run
            self.id = id
            t3mysql = t3MySQLdb()
            self.directory = t3mysql.select_run_directory(id)
        else :
            # First find out a uniq id
            uniq = False
            directory_name = uuid.uuid4().hex[:9]
            while True:
                save_dir = os.path.join(ng6conf.get_save_directory(), \
                            ng6conf.get_space_directory(self.space_id), self.DIRECTORIES_STRUCTURE, directory_name)
                work_dir = os.path.join(ng6conf.get_work_directory(), ng6conf.get_space_directory(self.space_id), \
                        self.DIRECTORIES_STRUCTURE, directory_name)

                if not os.path.isdir(save_dir) and not os.path.isdir(work_dir):
                    break
                directory_name = uuid.uuid4().hex[:9]

            # Then make directories
            os.makedirs(work_dir, 0o751)
            self.directory = "/" + os.path.join(ng6conf.get_space_directory(self.space_id),self.DIRECTORIES_STRUCTURE, directory_name)
            self.retention_date = ng6conf.get_retention_date(self.space_id)

    def process_raw_files(self, total_size, total_seq):
        """
        Copy, archive and compress the files list to the run. And add info to the run
          @param mode: can be none, gz, bz2, tar.gz and tar.bz2
        """
        logging.getLogger("Run").debug("process_raw_files. set_full_size()")
        self.set_nb_sequences(total_seq)
        self.set_full_size(total_size)
        logging.getLogger("Run").debug("process_raw_files. set_full_size("+str(total_size) + ")")
        logging.getLogger("Run").debug("process_raw_files. set_nb_sequences("+str(total_seq) + ")")


    def archive_files(self, files, mode, archive_name="ng6_archive.tar", delete=False):

        """
        Copy, archive and compress the files list to the run. Files can then be downloaded and gave back by
        the script ng6run2ergatis.
          @param files: the files to archive
          @param mode: can be none, gz, bz2, tar.gz and tar.bz2
          @param archive_name: the archive name if tar is requested
          @param delete: delete files
        """
        # create workdir if it does not exists
        if not os.path.isdir(self.__get_work_directory()) :
            os.makedirs(self.__get_work_directory(), 0o751)

        # First handle if only one file
        if len(files) == 1 and mode == "tar.gz":
            mode = "gz"
        elif len(files) == 1 and mode == "tar.bz2":
            mode = "bz2"

        if mode == "none":
            for file in files:
                if os.path.isfile(file):
                    copyfile(file, os.path.join(self.__get_work_directory(), os.path.basename(file)))
                # Finaly try to delete the original file if asked to do so
                try:
                    if delete:
                        os.remove(file)
                except:
                    pass

        elif mode == "gz":
            for file in files:
                Utils.gzip(file, self.__get_work_directory(), delete)

        elif mode == "bz2":
            for file in files:
                Utils.bz2(file, self.__get_work_directory(), delete)

        elif mode == "tar.gz":
            Utils.tar_files(files, os.path.join(self.__get_work_directory(), archive_name), delete)
            Utils.gzip(os.path.join(self.__get_work_directory(), archive_name), self.__get_work_directory(), True)

        elif mode == "tar.bz2":
            Utils.tar_files(files, os.path.join(self.__get_work_directory(), archive_name), delete)
            Utils.bz2(os.path.join(self.__get_work_directory(), archive_name), self.__get_work_directory(), True)



    def set_nb_sequences(self, nb_seqs):
        """
        Sets the number of sequences to nb_seqs
          @param nb_seqs : the run number of sequences
        """
        if self.id != None:
            t3mysql = t3MySQLdb()
            t3mysql.update_run_info(self.id, nb_sequences=nb_seqs)
        else :
            raise UnsavedRunError()

    def set_full_size(self, full_size):
        """
        Sets the number of nucleotides in all sequences
          @param full_size : the number of nucleotides
        """
        if self.id != None :
            t3mysql = t3MySQLdb()
            t3mysql.update_run_info(self.id, full_seq_size=full_size)
        else :
            raise UnsavedRunError()

    def get_analysis(self):
        """
        Return a table of Analysis object that belongs to the run
        """
        from ng6.analysis import Analysis
        t3mysql = t3MySQLdb()
        analysis = []
        analysis_ids = t3mysql.get_run_analysis_ids(self.id)
        # For each analysis
        for analysis_id in analysis_ids:
            analysis.append(Analysis.get_from_id(analysis_id))
        return analysis

    def sync(self):
        """
        Synchronyze data related to the Run between temporary folder and long term storage folder.
        """
        logging.getLogger("Run.sync").debug("Synchronizing run id=" + str(self.id) + " from " + self.__get_work_directory() + " to " + self.__get_save_directory())
        if self.id == None :
            raise UnsavedRunError()
        # First sync all analysis
        try :
            for my_analysis in self.get_analysis():
                my_analysis.sync()
        except Exception as err:
            logging.getLogger("Run.sync").error("Error when synchronizing analysis that belongs to run id=" + str(self.id))
            raise Exception(str(err))
        # If a sync is required
        if os.path.exists(self.__get_work_directory()) and len(os.listdir(self.__get_work_directory())):
            # Finally the Run itself
            try :
                # not created for updates
                if not  os.path.isdir(self.__get_save_directory()) :
                    # First make the save directory
                    os.makedirs(self.__get_save_directory(), 0o751)
                if os.path.exists(self.__get_work_directory()) and self.__get_work_directory() != self.__get_save_directory():
                    cmd = "rsync -avh --remove-sent-files "+ self.__get_work_directory() + "/ " + self.__get_save_directory()
                    logging.getLogger("Run.sync").debug(cmd)
                    retcode = call(["rsync", "-avh", "--remove-sent-files", self.__get_work_directory() + "/", self.__get_save_directory()], shell=False)
                    if retcode < 0:
                        logging.getLogger("Run.sync").error("Error when synchronizing run id=" + str(self.id) + "(retcode=" + str(retcode) + ")")
                    else:
                        try: os.rmdir(self.__get_work_directory())
                        except: pass
                        logging.getLogger("Run.sync").debug("Synchronization done for run id=" + str(self.id) + "(retcode=" + str(retcode) + ")")
                # update the storage size
                t3mysql = t3MySQLdb()
                t3mysql.update_run_info(self.id, storage_size=Utils.get_storage_size(self.__get_save_directory()))
            except Exception as err:
                raise Exception(str(err))

    def add_analysis(self, my_analysis):
        """
        Add the analysis to the run
          @param my_analysis   : the analysis to add
        """
        if self.id != None :
            # First define the run
            analysis_id = my_analysis.save()
            t3mysql = t3MySQLdb()
            t3mysql.add_analysis_to_run(self.id, analysis_id)
        else :
            raise UnsavedRunError()

    def get_directory_path(self):
        """
        Return the full path to the Run directory
        """
        return self.__get_save_directory()

    def get_samples(self):
        '''
            return a list of samples object
        '''
        if self.__samples :
            return self.__samples
        else :
            t3mysql = t3MySQLdb()
            results = t3mysql.select_run_samples(self.id)
            samples = []
            for res in results :
                t = dict(reads1 = [], reads2 = [])
                for k in t.keys() :
                    for f in res[k].split(',') :
                        fpath = f
                        if os.path.isdir(self.__get_work_directory()) :
                            fpath = self.__get_work_directory() + "/" + fpath
                        elif os.path.isdir(self.__get_save_directory()) :
                            fpath = self.__get_save_directory() + "/" + fpath
                        if not os.path.isfile(fpath):
                            raise Exception("The sample read file %s cannot be retrieved either in the work or save directory for the run %s " % fpath, self.id)
                        if k == 'reads1' :
                            reads1.append(fpath)
                        else :
                            reads2.append(fapth)

                samples.append(Sample( res['sample_id'], t['reads1'], reads2 = t['reads2'], name = res['name'], description = res['description'],
                                       type = res['type'], insert_size = res['insert_size'], species = res['species'], nb_sequence = res['nb_sequence'] ))
            return samples


    def add_samples(self, samples):
        '''
            Add a list of samples to the run
            @param samples: the list of samples object to add
        '''
        self.__samples = samples
        if self.id != None :
            # Update database
            t3mysql = t3MySQLdb()
            for spo in self.__samples:
                reads1 = []
                reads2 = []
                if spo.reads1:
                    reads1 = [ os.path.basename(ff) for ff in spo.reads1 ]
                if spo.reads2 :
                    reads2 = [ os.path.basename(ff) for ff in spo.reads2 ]

                uid = t3mysql.add_sample_to_run(self.id, sample_id = spo.sample_id, reads1 = reads1, reads2 = reads2, name=spo.name,
                                          description = spo.description, type = spo.type, insert_size = spo.insert_size,
                                          species = spo.species, nb_sequences = spo.nb_sequences)
                spo.id = uid

    def get_config_file(self):
        """
        Return a Run config file
          @return : path the the config file
        """
        t3mysql = t3MySQLdb()

        # First select run
        [name, date, species, data_nature, type, description, sequencer] = t3mysql.select_run(self.id)
        config = RawConfigParser()
        config.add_section("Run")
        config.set("Run", "run_id", self.id)
        config.set("Run", "name", name)
        config.set("Run", "date", date)
        config.set("Run", "species", species)
        config.set("Run", "data_nature", data_nature)
        config.set("Run", "type", type)
        config.set("Run", "description", description)
        config.set("Run", "sequencer", sequencer)

        # Then run samples
        samples = t3mysql.select_run_samples(self.id)
        if len(samples.keys()) > 0 : config.add_section("MIDS")
        for sample in samples.keys():
            config.set("MIDS", sample, samples[sample])

        config_path = tempfile.NamedTemporaryFile(suffix=".cfg").name
        config.write(open(config_path,'w'))
        return config_path

    def get_run_files(self, pattern = "", unzip=True):
        """
        Return list of file corresponding to the pattern
          @param file  : the pattern of files to return
          @param unzip : true for unzip files returned with pattern
          @return      : list of file.
        """
        list = []
        for file in os.listdir(self.__get_save_directory()) :
            # If it fits criteria
            if pattern != "":
                if unzip:
                    if re.search("^" + pattern + "\.gz$", file) or re.search("^" + pattern + "\.zip$", file):
                        tmp_dir = tempfile.mkdtemp()
                        list.append(Utils.gunzip(os.path.join(self.__get_save_directory(), file), tmp_dir))
                    elif re.search("^" + pattern + "\.bz2$", file) or re.search("^" + pattern + "\.bz$", file):
                        tmp_dir = tempfile.mkdtemp()
                        list.append(Utils.uncompress_bz2(os.path.join(self.__get_save_directory(), file), tmp_dir))
                    elif re.search("^" + pattern + "$", file):
                        list.append(os.path.join(self.__get_save_directory(), file))
                else:
                    if re.search("^" + pattern + "$", file):
                        list.append(os.path.join(self.__get_save_directory(), file))
            else:
                list.append(file)
        return list

    def save(self):
        """
        Creates the run config file and save it in the run directory
        """
        # First add the run into the database
        t3mysql = t3MySQLdb()
        self.id = t3mysql.add_run(self.name, self.date, self.directory, self.species, self.data_nature,
                                  self.type, 0, 0, self.description, self.sequencer, self.admin_login, self.retention_date, 1,self.nglbi_run_code)
        return self.id

    def __get_work_directory(self):
        """
        Return the full path of the run directory into the work dir
        """
        ng6conf = NG6ConfigReader()
        return ng6conf.get_work_directory() + self.directory

    def get_work_directory(self):
        """
        Return the full path of the run directory into the work dir
        """
        ng6conf = NG6ConfigReader()
        return self.__get_work_directory()

    def __get_save_directory(self):
        """
        Return the full path of the run directory into the save dir
        """
        ng6conf = NG6ConfigReader()
        return ng6conf.get_save_directory() + self.directory

    @staticmethod
    def get_from_id (id):
        """
        Return a run object specified by its id
          @param id : the run id
        """
        my_run=None
        try:
            t3mysql = t3MySQLdb()
            [name, date, species, data_nature, type, description, sequencer] = t3mysql.select_run(id)
            my_run = Run(name, date, species, data_nature, type, description, sequencer, id=id)
            # reload samples
        except :
            logging.getLogger("Run.get_from_id").debug("Impossible to build run id=" + str(id))
        try :
            my_run.__samples = None
            my_run.__samples = my_run.get_samples()
        except :
            logging.getLogger("Run.get_from_id").debug("Impossible to get samples for run id=" + str(id))
        return my_run


    @staticmethod
    def get_from_config (config_file):
        """
        Return a run object specified by its config file
          @param config_file : the run config file
        """
        try:
            reader = ConfigParser()
            reader.read(config_file)
            run_id = reader.get('Run', 'run_id')
            my_run = Run.get_from_id(run_id)
            return my_run
        except :
            pass
    
    def set_retention(self, retention):
        """
        Updates a run's retention field
          @param retention : the new retention value, in (unix) epoch timestamp format
        """
        if self.id != None :
            t3mysql = t3MySQLdb()
            t3mysql.set_run_retention_from_epoch_timestamp(self.id, retention)
        else :
            raise UnsavedRunError()
    
    def set_directory(self, directory):
        """
        Updates a run's directory field
          @param directory : the new directory path
        """
        if self.id != None :
            t3mysql = t3MySQLdb()
            t3mysql.update_fields('tx_nG6_run', str(self.id), ['directory'], [str(directory)] )
        else :
            raise UnsavedRunError()
        
    def get_creation_date(self):
        """
        Returns a run's crdate value
        """
        if self.id != None :
            t3mysql = t3MySQLdb()
            result = t3mysql.get_run_creation_date(self.id)
            return next(iter(result))
        else :
            raise UnsavedRunError()
    
    def get_directory(self):
        """
        Returns a run's directory
        """
        if self.id != None :
            t3mysql = t3MySQLdb()
            return t3mysql.select_run_directory(self.id)
        else :
            raise UnsavedRunError()
        
    def change_space (self, space_id ):
        ng6conf = NG6ConfigReader()
        
        old_path = ng6conf.get_save_directory() + self.directory
        directory_name = os.path.split(old_path)[-1]
        new_relative_path = os.path.join(ng6conf.get_space_directory(space_id), self.DIRECTORIES_STRUCTURE, directory_name)
        
        new_absolute_path =  os.path.join(ng6conf.get_save_directory(), new_relative_path )
        
        #We create the /run directory if it's missing
        path_to_run_dir = os.path.join(ng6conf.get_save_directory(),ng6conf.get_space_directory(space_id),self.DIRECTORIES_STRUCTURE)
        if not os.path.isdir(path_to_run_dir):
            os.mkdir(path_to_run_dir,0o755)
        
        str_cmd = ""
        retcode = -1
        if str(old_path) != str(new_absolute_path):
            [retcode, str_cmd] = Utils.rsync_getcmd(old_path,new_absolute_path)
            if retcode != 0 :   
                raise Exception("Error while trying to rsync " + old_path + " to " + new_absolute_path + "\n" +
                                "Command : " + str_cmd + "\n" + "Error code : " + str(retcode) + "\n")
        else:
            str_cmd = "Source and destination directories are the same : " + old_path + " , ignored.\n"
        
        new_retention = ng6conf.get_retention_date(space_id, self.date)
        
        t3mysql = t3MySQLdb()
        t3mysql.update_fields('tx_nG6_run', str(self.id), ['directory','retention_date'], [str("/"+new_relative_path),new_retention] )
            
        return [retcode, str_cmd]
        
