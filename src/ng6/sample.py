#
# Copyright (C) 2009 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from ng6.exceptions import UnsavedRunError
from ng6.t3MySQLdb import t3MySQLdb

class Sample(object):
    
    AVAILABLE_TYPES = ["pe", "se", "ose", "ope", "mp","10X"]
    
    def __init__(self, sample_id, reads1, reads2 = None, readsi = None, name = None, description = None, type = None, 
                 insert_size = None, species = None, nb_sequences = None, full_size = None, id = None ):
        self.sample_id = sample_id
        self.name = name
        self.description = description
        self.reads1 = reads1
        self.reads2 = reads2
        self.readsi = readsi
        self.insert_size = insert_size
        self.nb_sequences = nb_sequences
        self.full_size = full_size
        self.species = species
        self.type = type
        self.id = id
        
        if isinstance(reads1, str) :
            self.reads1 = [reads1]
        
        if isinstance(reads2, str) :
            self.reads2 = [reads2]
            
        if isinstance(readsi, str) :
            self.readsi = [readsi]
        
        if self.type is None:
            if self.reads2 :
                self.type = self.AVAILABLE_TYPES[0]
            else :
                self.type = self.AVAILABLE_TYPES[1]
        
        if self.nb_sequences and isinstance(self.nb_sequences, str) :
            self.nb_sequences = int(self.nb_sequences)
        
        self.metadata = {}
        
    def add_metadata(self, key, value):
        self.metadata[key] = value
    
    def get_metadata(self, key):
        if self.has_metadata(key):
            return self.metadata[key]

    def has_metadata(self, key):
        return key in self.metadata
    
    def get_all_reads(self):
        allreads = self.reads1
        if self.reads2 :
            allreads += self.reads2
        return allreads

    def set_nb_sequences(self, nb_seqs):
        """
        Sets the number of sequences to nb_seqs
          @param nb_seqs : the sample number of sequences
        """
        if self.id != None:
            t3mysql = t3MySQLdb()
            t3mysql.update_sample_info(self.id, nb_sequences=nb_seqs)
            self.nb_sequences = nb_seqs
        else :
            raise UnsavedRunError()
    
    def set_full_size(self, full_size):
        """
        Sets the number of nucleotides in all sequences
          @param full_size : the number of nucleotides
        """
        if self.id != None :
            t3mysql = t3MySQLdb()
            t3mysql.update_sample_info(self.id, full_seq_size=full_size)
            self.full_size = full_size
        else :
            raise UnsavedRunError()
    
    def __str__(self, *args, **kwargs):
        return "sid={sid}; name={name}; desc={desc}; r1={r1}; r2={r2}; i={i}; insize={insize}; nbs={nbs}; fsize={fsize}; spec={spec}; t={t}".format(
        sid = self.sample_id or '',
        name = self.name or '',
        desc = self.description or '',
        r1 = self.reads1 or [],
        r2 = self.reads2 or [],
        i = self.readsi or [],
        insize = self.insert_size or '',
        nbs = self.nb_sequences or '',
        fsize = self.full_size or '',
        spec = self.species or '',
        t = self.type  or '')
